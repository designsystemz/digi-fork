## Sammanfattning

- **Miljö:** React / Angular / Webbkomponenter

- **Version på Digi:** x.y.z

Skriv en kort förklaring vad som är fel

## Återskapande (valfritt)

Dela om möjligt en url till en sida där problemet befinner sig och där det går att testa

**⚠️ Observera:** _Dela inte känslig information eller personuppgifter. Allting i vårt repo är tillgängligt för alla besökare_

/label ~"Bugg 🐛" ~"Behöver utredas"
