import type { Config } from 'jest';

const config: Config = {
  projects: [
    'digi',
    'digi/apps/arbetsformedlingen/docs/*',
    'digi/libs/arbetsformedlingen/*'
  ]
};

export default config;
