import { componentCategoryMap } from './categoryData';
import { Category } from '../taxonomies';
// eslint-disable-next-line @nx/enforce-module-boundaries
import _componentData from '../../../../../dist/libs/arbetsformedlingen/docs.json';

export const componentData = _componentData;

const mapComponentsToCategory = (componentData) => {
  return componentData.components.map((component) => {
    return {
      ...component,
      category: component.tag.includes('digi-icon')
        ? Category.ICON
        : componentCategoryMap[component.tag] || Category.UNCATEGORIZED
    };
  });
};

const sortByName = (a, b) => {
  const firstComponentName = a.docsTags.find((obj) => {
    return obj.name === 'swedishName';
  });
  const secondComponentName = b.docsTags.find((obj) => {
    return obj.name === 'swedishName';
  });
  if (
    firstComponentName &&
    firstComponentName.text &&
    secondComponentName &&
    secondComponentName.text
  ) {
    return firstComponentName.text.localeCompare(
      secondComponentName.text,
      'sv',
      {
        sensitivity: 'base'
      }
    );
  }
  return 0;
};

const sortComponentsByName = (componentData) => {
  const sortedData = {
    components: [],
    icons: []
  };
  componentData.forEach((component) => {
    if (component.tag.includes('digi-icon-')) {
      sortedData.icons.push(component);
    } else {
      sortedData.components.push(component);
    }
  });
  sortedData.components.sort(sortByName);
  return sortedData;
};

const mapAndSortDataByCategoriesAndName = (componentData) => {
  const mappedComponentsToCategory = mapComponentsToCategory(componentData);
  return sortComponentsByName(mappedComponentsToCategory);
};

const data = mapAndSortDataByCategoriesAndName(componentData);

export const components = data.components;
export const icons = data.icons;
