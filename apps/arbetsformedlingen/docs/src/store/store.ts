import { createStore } from '@stencil/store';
import { components, icons } from '../data/coreData';
import { LayoutColumnsVariation } from '@digi/arbetsformedlingen';

const { state, onChange } = createStore({
  components: components,
  icons: icons,
  routerRoot: '/',
  activeComponent: null,
  activeNav: false,
  router: {
    activePath: '',
    oldPaths: [],
    newPaths: []
  },
  hideHeader: false,
  hideFooter: false,
  hideNavigation: false,
  responsiveTwoColumns: LayoutColumnsVariation.TWO,
  responsiveThreeColumns: LayoutColumnsVariation.THREE,
  responsive321Columns: LayoutColumnsVariation.THREE
});

onChange('routerRoot', (value) => {
  state.routerRoot = value;
});

export default state;
