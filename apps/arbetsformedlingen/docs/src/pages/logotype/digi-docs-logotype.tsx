import { Component, getAssetPath, h } from '@stencil/core';
import {
  InfoCardVariation,
  InfoCardHeadingLevel
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-logotype',
  styleUrl: 'digi-docs-logotype.scss',
  scoped: true
})
export class DigiDocsLogotype {
  render() {
    return (
      <div class="digi-docs__about-logotype">
        <digi-docs-page-layout af-edit-href="pages/digi-docs-logotype/digi-docs-logotype.tsx">
          <digi-typography>
            <digi-layout-block>
              <digi-typography-heading-jumbo af-text="Logotyp"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                Arbetsförmedlingens logotyp består av en ordbild och en symbol.
                Logotypen är bärare av de värden Arbetsförmedlingen står för.
              </digi-typography-preamble>
            </digi-layout-block>
            <br />
            <digi-layout-block>
              <p>
                Mittcirkeln i symbolen står för människan i centrum. Den yttre
                delen av en omringande cirkel för det som Arbetsförmedlingen
                erbjuder i form av möjligheter och stöd. Den gröna färgen
                symboliserar växtkraft och den blå färgen signalerar en
                förtroendeingivande och trygg myndighet. Ordbildens typsnitt
                Funkis ritades 2016 av Fredrik Gruber och Göran Söderström med
                inspiration av Sigurds Lewerentz klassiska poster till
                Stockholmsutställningen ”Stockholm Exhibition 1930”.
              </p>
              <h2>Användning</h2>
              <p>
                Du får använda logotypen i sammanhang där vi är avsändare och i
                redaktionella sammanhang, men inte i reklam eller
                marknadsföring. Vår logotyp finns i två varianter, primär och
                sekundär. Vilken du ska använda beror på vilken typ av bakgrund
                logotypen ska placeras på.
              </p>
              <p>
                <strong>Primär logotyp</strong> positiv, används endast på
                vit/ljus bakgrund. <br />
                <strong>Sekundär logotyp</strong> negativ, används när
                bakgrunden har annan färg än vit/ljus bakgrund.
              </p>
              <div class="digi-docs__about-logotype--container">
                <digi-media-image
                  afUnlazy
                  af-src={getAssetPath('/assets/images/logo-primary.png')}
                  afAlt="Arbetsförmedlingens primär logotyp "></digi-media-image>
                <digi-media-image
                  afUnlazy
                  af-src={getAssetPath('/assets/images/logo-secondary.png')}
                  afAlt="Arbetsförmedlingens sekundär logotyp"></digi-media-image>
              </div>
              <br />
              <h3>Frizon</h3>
              <p>
                För att logotypen ska bli tydlig behöver den alltid omges av en
                frizon. Frizonen är minimiavståndet till annan grafik, text
                eller bild, eller till ytterkanten på det material där logotypen
                ska placeras.
              </p>
              <digi-media-image
                afUnlazy
                class="digi-docs__about-logotype--image--frizon"
                af-src={getAssetPath('/assets/images/logo-frizon.png')}
                afAlt="Arbetsförmedlingens logotyp i frizon läge "></digi-media-image>
              <h3>Storlek</h3>
              <p>
                För att logotypen ska bli tydlig behöver den också vara
                tillräckligt stor (256px eller 45mm i bredd).
              </p>
              <digi-media-image
                afUnlazy
                class="digi-docs__about-logotype--image--size"
                af-src={getAssetPath('/assets/images/logotype-image-size.png')}
                afAlt="Arbetsförmedlingens logotyp storlek "></digi-media-image>

              <br />

              <div class="digi-docs__about-logotype--info-card-container">
                <digi-info-card
                  afHeading="Gör så här…"
                  afHeadingLevel={InfoCardHeadingLevel.H3}>
                  <digi-list>
                    <li> Behåll logotypens proportioner.</li>
                    <li>Se till att skapa god kontrast mot bakgrunden.</li>
                    <li>Använd rätt filformat anpassad för ändamålet.</li>
                    <li>
                      Skriv ”Arbetsförmedlingen”, istället för att använda
                      logotypen, i löptext.
                    </li>
                  </digi-list>
                </digi-info-card>
                <br />
                <digi-info-card
                  afHeading="…men inte så här"
                  afHeadingLevel={InfoCardHeadingLevel.H3}
                  afVariation={InfoCardVariation.SECONDARY}>
                  <digi-list>
                    <li> Ändra logotypens utformning eller proportioner.</li>
                    <li>Addera effekter eller annan grafik på logotypen.</li>
                    <li>Använd logotypen i löpande text.</li>
                    <li>
                      Lägg logotypen på en bakgrund med dålig kontrast mot
                      texten i logotypen.
                    </li>
                    <li>
                      Använd webbfiler för tryckproduktion eller tvärtom då de
                      är optimerade efter användningsområde.
                    </li>
                  </digi-list>
                </digi-info-card>
              </div>
              <br />
              <p>
                Är du osäker på hur du får använda logotypen eller har frågor.
                Kontakta då Kommunikationsavdelningen för vidare hjälp.
              </p>
              <p>
                <digi-link
                  af-variation="small"
                  afHref="mailto:kommunikationsavdelningen@arbetsformedlingen.se?body=Beskriv%20ditt%20ärende%20så%20återkommer%20vi%20så%20snart%20vi%20kan">
                  <digi-icon-envelope slot="icon"></digi-icon-envelope>
                  Kontakta kommunikationsavdelningen
                </digi-link>
              </p>
              <p>
                <digi-link
                  afHref={getAssetPath(
                    '/assets/brand-logo/arbetsformedlingens-logotyp-2024.zip'
                  )}
                  afDownload={'Arbetsformedlingens-logotyper'}
                  afTarget={'_blank'}>
                  <digi-icon-download></digi-icon-download>
                  Ladda ner Arbetsförmedlingens logotyp (Zip, 1 MB)
                </digi-link>
              </p>
              <p>
                Arbetsförmedlingens logotyp får endast användas av
                Arbetsförmedlingen och där Arbetsförmedlingen är avsändare. I de
                fall där vi har ett nära samarbete med extern part kan annan
                användning godkännas. Vår logotyp ska inte finnas på andra
                webbplatser än våra egna. Undantag är kampanjsajter eller
                liknande där Arbetsförmedlingen är en aktiv part.
              </p>
              <p>
                Kompletterande aktörer får inte använda Arbetsförmedlingens
                logotyp eller formulera texter så att det kan framstå som en
                gemensam marknadsföring mellan aktören och Arbetsförmedlingen.
              </p>
            </digi-layout-block>
            <br />
            <digi-layout-block>
              <h2>Varianter</h2>
              <p>
                Vi ska primärt använda hela logotypen. Om ytan är för liten
                använder vi endast symbolen. Exempel när det är tillåtet att
                använda endast symbolen är för interna tjänster, mobilappar,
                sociala medier och pins. Arbetsförmedlingen behöver då vara
                tydlig som avsändare på annat sätt.
              </p>
              <p>
                Logotypen får endast användas i vertikalt format om den inte går
                att använda horisontellt. Till exempel vid tryck på en flagga. I
                vissa fall kan du använda logotypen i svart eller vitt men bara
                när det inte går att visa den i färg. Exempelvis på
                Arbetsförmedlingens pennor och våra tjänstekort.
              </p>
              <br />
              <div class="digi-docs__about-logotype--variation-container">
                <div class="digi-docs__about-logotype--variation-wrapper">
                  <digi-media-image
                    afUnlazy
                    af-src={getAssetPath('/assets/images/logotype-sign.svg')}
                    afAlt="Arbetsförmedlingens logotyp som en fasadskylt"></digi-media-image>
                  <digi-media-image
                    afUnlazy
                    af-src={getAssetPath(
                      '/assets/images/logotype-facebook.svg'
                    )}
                    afAlt="Arbetsförmedlingens logotyp i facebook"></digi-media-image>
                </div>
                <div class="digi-docs__about-logotype--variation-wrapper">
                  <digi-media-image
                    afUnlazy
                    af-src={getAssetPath('/assets/images/logotype-black.svg')}
                    afAlt="Arbetsförmedlingens logotyp i svart färg"></digi-media-image>
                </div>
                <div class="digi-docs__about-logotype--variation-wrapper">
                  <digi-media-image
                    afUnlazy
                    af-src={getAssetPath('/assets/images/logotype-blue.svg')}
                    afAlt="Arbetsförmedlingens logotyp med blå bakgrund"></digi-media-image>
                  <digi-media-image
                    afUnlazy
                    af-src={getAssetPath('/assets/images/logotype-green.svg')}
                    afAlt="Arbetsförmedlingens logotyp i grön färg"></digi-media-image>
                </div>
              </div>

              <br />
              <h2>Titellogotyp</h2>
              <p>
                För interna tjänster använder vi en så kallad titellogotyp för
                att enkelt och snabbt identifiera vilken tjänst eller vilket
                verktyg vi jobbar i. Exempel där titellogotypen används är
                Intranätet, Designsystem och Digitalportalen.
              </p>
              <p>
                Utöver titellogotypen är det viktigt att även på annat sätt
                tydligt visa att det är en tjänst eller verktyg som är kopplat
                till Arbetsförmedlingen. Exempelvis genom profilfärger, grafik
                och/eller genom att ha Arbetsförmedlingens logotyp i sidfoten.
              </p>
              <br />

              <div class="digi-docs__about-logotype--titlelogotype-wrapper">
                <digi-logo af-system-name="Intranätet"></digi-logo>
                <digi-logo af-system-name="Designsystem"></digi-logo>
              </div>
              <br />
              <h2>Favicon</h2>
              <p>
                För snabb igenkänning i webbläsarens flikar använder vi oss utav
                en så kallad favicon. Det är symbolen liggandes på en platta.
                För externa webbsidor och tjänster använder vi symbolen mot en
                profilblå bakgrund. För interna tjänster och webbsidor använder
                vi symbolen mot en vit bakgrund.
              </p>
              <br />
              <div class="digi-docs__about-logotype--favicon-wrapper">
                <digi-media-image
                  afUnlazy
                  af-src={getAssetPath('/assets/images/logotype-favicon.svg')}
                  afAlt="Arbetsförmedlingens favicon logotyp "></digi-media-image>
              </div>

              <br />
              <p>
                <digi-link
                  afHref={getAssetPath('/assets/brand-logo/favicons.zip')}
                  afDownload={'Arbetsformedlingens-favicons'}
                  afTarget={'_blank'}>
                  <digi-icon-download></digi-icon-download>
                  Ladda ner Arbetsförmedlingens favicons (Zip, 572 kB)
                </digi-link>
              </p>
              <br />
              <h2>Logotypen i samarbeten</h2>
              <p>
                Namn på tjänster/webbplatser har inte egna logotyper utan
                hanteras typografiskt. I vissa fall går det att addera
                infografik eller illustration från Arbetsförmedlingens mediabank
                för att förstärka budskapet. Tänk på att alltid välja vår
                logotyp med grön symbol och blå text i första hand. Endast när
                de andra samarbetspartners logotyper är enfärgade är det
                tillåtet att ha logotypen i endast en färg.{' '}
              </p>
              <p>
                För att tydliggöra vårt samarbete så bra som möjligt för våra
                olika målgrupper får Arbetsförmedlingens logotyp inte användas
                av leverantörer i den egna kommunikationen. Däremot ska texten
                ”på uppdrag av Arbetsförmedlingen" användas i kommunikation för
                att tydliggöra samarbetet. Detta får endast göras under
                avtalsperioden.
              </p>
              <br />
            </digi-layout-block>

            <br />
          </digi-typography>
        </digi-docs-page-layout>
      </div>
    );
  }
}
