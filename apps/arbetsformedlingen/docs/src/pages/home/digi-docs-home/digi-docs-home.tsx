import { Component, h } from '@stencil/core';
import { router } from '../../../global/router';

@Component({
  tag: 'digi-docs-home',
  styleUrl: 'digi-docs-home.scss',
  assetsDirs: ['assets']
})
export class DigiDocsHome {
  Router = router;

  linkHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <div class="digi-docs-home">
        <digi-docs-page-layout af-edit-href="pages/home/digi-docs-home/digi-docs-home.tsx">
          <digi-layout-block>
            <digi-docs-home-hero-alt></digi-docs-home-hero-alt>
          </digi-layout-block>

          <digi-docs-home-four-arguments />
          <digi-docs-home-link-blocks></digi-docs-home-link-blocks>
          <digi-docs-home-architecture />
          <digi-docs-code-examples></digi-docs-code-examples>
          <digi-docs-home-content-blocks></digi-docs-home-content-blocks>
          {/*<digi-docs-home-open-source />*/}
        </digi-docs-page-layout>
      </div>
    );
  }
}
