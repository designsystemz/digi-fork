import { Component, getAssetPath, h, State } from '@stencil/core';
import { UtilBreakpointObserverBreakpoints } from '@digi/arbetsformedlingen';
import { router } from '../../../global/router';

@Component({
  tag: 'digi-docs-home-hero',
  styleUrl: 'digi-docs-home-hero.scss',
  scoped: true
})
export class HomeHero {
  Router = router;

  @State() showMobileVersion = false;

  breakpointHandler(e: CustomEvent) {
    if (e.detail.value !== undefined) {
      this.showMobileVersion =
        e.detail.value === UtilBreakpointObserverBreakpoints.SMALL ||
        e.detail.value === UtilBreakpointObserverBreakpoints.MEDIUM ||
        e.detail.value === UtilBreakpointObserverBreakpoints.LARGE
          ? true
          : false;
    }
  }

  render() {
    return (
      <host>
        <digi-util-breakpoint-observer
          onAfOnChange={(e) => this.breakpointHandler(e)}>
          {this.showMobileVersion && (
            <digi-media-image
              class="digi-docs-home-hero__media-image"
              afUnlazy
              afSrc={getAssetPath('/assets/images/designsystem-hero-team.jpg')}
              afAlt=""></digi-media-image>
          )}
          <digi-layout-block class="digi-docs-home-hero__layout">
            <div class="digi-docs-home-hero">
              <div class="digi-docs-home-hero__text">
                <digi-typography-heading-jumbo afText="Arbetsförmedlingens designsystem" />
                <digi-typography-preamble class="digi-docs-home-hero__preamble">
                  Resurser, verktyg och riktlinjer för att skapa sveriges
                  främsta myndighetstjänster.
                </digi-typography-preamble>
                <slot></slot>
              </div>
              {!this.showMobileVersion && (
                <div class="digi-docs-home-hero__media">
                  <div
                    style={{
                      'background-image': `url(${getAssetPath('/assets/images/designsystem-hero-team.jpg')})`
                    }}></div>
                </div>
              )}
            </div>
          </digi-layout-block>
        </digi-util-breakpoint-observer>
      </host>
    );
  }
}
