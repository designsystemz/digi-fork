import { Component, h } from '@stencil/core';
import { router } from '../../../global/router';
import figmaLogo from '../../../assets/images/startpage/figmaLogo.svg';
import gitlabLogo from '../../../assets/images/startpage/gitlabLogo.svg';
import openSourceImage from '../../../assets/images/startpage/website-side-by-side-illustration.svg';

@Component({
  tag: 'digi-docs-home-open-source',
  styleUrl: 'digi-docs-home-open-source.scss',
  scoped: true
})
export class OpenSource {
  Router = router;

  linkHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-layout-container style={{ 'padding-top': '4rem' }}>
          <div class="text-side-image-container">
            <div>
              <img class="text-side-image-container--logos" src={figmaLogo} alt="Figma Logotyp" />
              <img
                class="text-side-image-container--gitlab-logo text-side-image-container--logos"
                src={gitlabLogo}
                alt="Gitlab Logotyp"
              />
              <digi-typography>
                <h2 class="title-container">
                  Vem som helst kan vara med och bidra i öppna system
                </h2>
                <p class="paragraph-section">
                  Arbetsförmedlingen arbetar med Figma och Gitlab som
                  plattformar för öppen källkod, vilket innebär att vem som
                  helst kan delta i skapandet och förbättringen av
                  designsystemet. Vår licens Apache 2.0 möjliggör användning
                  inom både offentlig och privat sektor.
                </p>
              </digi-typography>
              <div class="links-container">
                <digi-link-external
                  afHref="https://gitlab.com/arbetsformedlingen/designsystem"
                  af-target="_blank"
                  af-variation="primary">
                  Utvecklarplattformen (gitlab.com, öppnas i egen flik)
                </digi-link-external>
                <digi-link-external
                  afHref="https://www.figma.com/@sweden"
                  af-target="_blank"
                  af-variation="primary">
                  UI kit (Figma.com, öppnas i egen flik)
                </digi-link-external>
              </div>
            </div>

            <div>
              <img
                class="open-source-side-image"
                src={openSourceImage}
                alt="Illustration med sju olika figurer i varsin rektangel"
              />
            </div>
          </div>
        </digi-layout-container>
      </host>
    );
  }
}
