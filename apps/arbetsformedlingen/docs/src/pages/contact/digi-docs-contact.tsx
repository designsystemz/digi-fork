import { Component, h, State } from '@stencil/core';

@Component({
  tag: 'digi-docs-contact',
  styleUrl: 'digi-docs-contact.scss'
})
export class DigiDocsContact {
  @State() pageName = 'Kontakta oss gällande Arbetsförmedlingens designsystem';

  render() {
    return (
      <div class="digi-docs-contact">
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-digi/digi-docs-contact/digi-docs-contact.tsx">
          <span slot="preamble">
            Designsystemet består av designers, arkitekter, utvecklare,
            tillgänglighetsspecialister och produktledare och handlägger inte
            ärenden kopplat till dig som arbetssökande.
          </span>
          <digi-layout-container af-margin-bottom af-margin-top>
            <article>
              <p>
                Vi välkomnar alla frågor och bidrag för att förbättras inom
                följande områden:
              </p>
              <digi-list>
                <li>Innehåll på denna webbplats</li>
                <li>Brister avseende digital tillgänglighet</li>
                <li>Frågor om arbetet med öppen källkod</li>
                <li>
                  Erfarenhetsutbyten gällande designsystem, organisation och
                  arbetssätt
                </li>
                <li>Gästföreläsningar hos utbildningsanordnare</li>
              </digi-list>
              <p>
                Vi samarbetar gärna med andra myndigheter, företag och
                organisationer. Vi bedriver vårt arbete som{' '}
                <a
                  href="https://gitlab.com/arbetsformedlingen/designsystem/digi"
                  target="_blank"
                  rel="noopener noreferrer">
                  öppen källkod på Gitlab (öppnas i egen flik).
                </a>
              </p>
              <digi-link
                af-variation="small"
                afHref={`mailto:designsystem@arbetsformedlingen.se?body=Beskriv%20ditt%20ärende%20så%20återkommer%20vi%20så%20snart%20vi%20kan`}
                class="digi-docs__accessibility-link">
                <digi-icon-envelope slot="icon"></digi-icon-envelope>
                Mejla designsystem@arbetsformedlingen.se
              </digi-link>
            </article>
          </digi-layout-container>
        </digi-docs-page-layout>
      </div>
    );
  }
}
