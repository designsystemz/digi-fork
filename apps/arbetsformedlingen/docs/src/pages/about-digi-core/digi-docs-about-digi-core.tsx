import { Component, h, State } from '@stencil/core';
import { InfoCardHeadingLevel } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-about-digi-core',
  styleUrl: 'digi-docs-about-digi-core.scss',
  scoped: true
})
export class DigiDocsAboutDigiCore {
  @State() pageName = 'Digi Core';

  render() {
    return (
      <div class="digi-docs-about-digi-core">
        <digi-docs-page-layout
          af-edit-href="pages/digi-docs-about-digi-core/digi-docs-about-digi-core.tsx"
          af-page-heading={this.pageName}>
          <span slot="preamble">
            Digi Core är ett komponentbibliotek med webbkomponenter. Detta
            innebär att komponenterna i Digi Core går att använda överallt där
            HTML finns, till exempel i Angular, React, SiteVision och ren
            HTML/CSS. Stencil.js är den kompilator vi använder för att hantera
            webbkomponenter. Arbetsförmedlingens Designsystem arbetar med detta
            just för att möta olika behov på myndigheten.
          </span>
          <digi-typography>
            <digi-layout-block>
              <br />
              <p>
                Digi Core innehåller gemensam CSS och SASS, som är byggt på ett
                modernt sätt. Designtokens (till exempel marginaler,
                typsnittsstorlekar, etc) representeras med variabler i både CSS
                och SASS. Detta kopplas ihop med UI-kittet och andra
                designverktyg så att det sker ett sömlöst utbyte av designtokens
                däremellan.
              </p>
              <h2>Målsättning och syfte</h2>
              <p>
                Målsättningen med Digi Core är att all grundläggande kod
                relaterad till designsystem ska finnas där. Sedan kan andra, mer
                specifika paket använda sig av detta (Digi Ng kommer till
                exempel att använda sig av komponenter härifrån). Syftet är att
                ha en källa för allt, så att vi inte behöver ha flera versioner
                av samma sak. Vi vill att den allt för långa listan med paket
                ovan ska kunna bantas ner till bara ett eller två.
              </p>
              <p>
                <digi-info-card
                  afHeading="Fördelar med Digi Core"
                  afHeadingLevel={InfoCardHeadingLevel.H2}>
                  <digi-list>
                    <li>
                      Teknikagnostiskt, vilket innebär att det går att använda
                      överallt där HTML finns.
                    </li>
                    <li>
                      En källa för allt, vi slipper olika versioner av samma
                      sak.
                    </li>
                    <li>Enkelt att implementera och underhålla.</li>
                  </digi-list>
                  {/* <digi-link-internal
                afHref=""
                af-target="_blank"
              > Lär dig mer om webbkomponenter</digi-link-internal> */}
                </digi-info-card>
              </p>
              <br />
            </digi-layout-block>
          </digi-typography>
        </digi-docs-page-layout>
      </div>
    );
  }
}
