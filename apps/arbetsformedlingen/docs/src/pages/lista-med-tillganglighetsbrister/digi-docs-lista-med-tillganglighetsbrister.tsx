import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';

@Component({
  tag: 'digi-docs-lista-med-tillganglighetsbrister',
  styleUrl: 'digi-docs-lista-med-tillganglighetsbrister.scss',
  scoped: true
})
export class ListaMedTillganglighetsbrister {
  Router = router;

  @State() pageName = 'Instruktioner för lista med tillgänglighetsbrister';

  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/lista-med-tillganglighetsbrister/digi-docs-lista-med-tillganglighetsbrister.tsx">
          <span slot="preamble">
            Team som är ansvariga för en sida eller en tjänst på webbplatsen
            arbetsformedlingen.se sammanställer en text om vilka brister som
            finns. Texten ska uppdateras i samband med release och laddas upp på
            angiven plats på SharePoint. Webbredaktionen publicerar texterna i
            redogörelsen regelbundet och går igenom uppladdade listor på
            SharePoint varje månad. Här följer en instruktion om hur processen
            går till.
          </span>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Publicering och ansvar</h2>
              <p>
                Bristtexterna publiceras i tillgänglighetsredogörelsen på
                Arbetsförmedlingens webbplats, på respektive undersida:
                arbetssökande, arbetsgivare och generella brister på webbplatsen
                i övrigt. Texten visas alltså publikt och målgruppen är samtliga
                besökare på webbplatsen.
              </p>
              <digi-link-external
                af-target="_blank"
                af-variation="small"
                afHref="https://arbetsformedlingen.se/om-webbplatsen/tillganglighet">
                Tillgänglighetsredogörelse på Arbetsförmedlingens webbplats
                (öppnas i egen flik)
              </digi-link-external>{' '}
              {''}
              <br />
              <br />
              <p>
                Texterna ska vara{' '}
                <strong>
                  aktuella och spegla tjänsten eller sidans faktiska status
                </strong>
                . Den som är ansvarig för en sida eller tjänst säkerställer att
                texten som beskriver bristerna är korrekt. Texten kan komma att
                användas juridiskt. <strong>Varje team äger sin text</strong>{' '}
                och ansvarar för innehållets kvalitet.
              </p>
              <h2>Uppdatera bristerna vid release</h2>
              <p>
                Varje team ska uppdatera sina texter vid release, om det har
                skett förändringar. Om det inte har skett några förändringar i
                tjänsten vad gäller tillgänglighet, exempelvis att brister har
                åtgärdats eller att nya brister har tillkommit, behöver inte
                texterna uppdateras.
              </p>
              <p>
                Vid förändringar ska ett nytt dokument laddas upp på SharePoint{' '}
                <strong>i samband med teamets release</strong>.
                Tillgänglighetsredogörelsen på Arbetsförmedlingens webbplats
                uppdateras regelbundet. För att få med uppdateringar om brister
                så snabbt som möjligt behöver en ny lista med brister ha laddats
                upp på SharePoint
                <strong>
                  senast den 23:e den månad som releasen har genomförts
                </strong>
                .
              </p>
              <p>
                Exempel: Ett team genomför en release av sin tjänst den 15
                oktober. För att tillgänglighetsbrister i tjänsten ska komma med
                vid nästa uppdatering av tillgänglighetsredogörelsen, behöver en
                lista över bristerna i tjänsten laddas upp till SharePoint
                senast den 23:e oktober.
              </p>
              <p>
                Ta bort brister som ni har åtgärdat och lägg till eventuella nya
                brister som ni har hittat. Kom ihåg att det vid varje
                tillgänglighetsbrist behöver finnas ett datum för när teamet
                planerar att åtgärda den aktuella bristen. Om flera brister
                planeras att åtgärdas samtidigt går det bra att gruppera
                bristerna under samma datum istället för att ange sama datum
                flera gånger. Undvik att exempelvis skriva: "Följande brister
                planerar att åtgärdas innan Q4 är slut". Skriv istället att
                bristerna ska åtgärdas före den 31 december 2022, eller det
                sista datumet i ett kvartal. Det går också bra att ange sista
                datum i ett inkrement eller en sprint. Det är alltså viktigt att
                det finns ett datum.
              </p>
              <p>
                Kom också ihåg att uppdatera dokumentets namn med ett nytt datum
                för när det har uppdaterats och laddats upp till SharePoint.
              </p>
              <p>
                Webbredaktionen uppdaterar tillgänglihgetsredogörelsen med de
                nya texterna efter den 23:e varje månad. Hela texten i det nya
                dokumentet kommer kopieras och ersätta den text som tidigare har
                publicerats. Kom därför ihåg att även ta med tidigare
                publicerade brister som inte har åtgärdats i det nya dokumentet.
              </p>
              <h2>Vilka brister ska ingå?</h2>
              <p>
                Samtliga brister som finns på sidan eller i tjänsten ska
                beskrivas i era bristtexter. Om det finns brister som beror på
                felaktigheter i designsystemets komponenter eller riktlinjer ska
                dessa brister ändå redovisas i er text. Stäm gärna av med
                designsystemet om hur bristen ska hanteras eftersom det kan
                finnas en enkel lösning som gör att bristen snabbt kan åtgärdas.
              </p>
              <p>
                Kriterierna 3.2.3 Konsekvent navigering och 3.2.4 Konsekvent
                identifiering,{' '}
                <strong>ska inte finnas med i er lista med brister</strong>{' '}
                (totalt fyra påståenden). Dessa kriterier kommer att beskrivas
                separat under rubriken Generella brister.
              </p>
              <digi-link-internal
                afHref="/tillganglighet-checklista/wcag-levels"
                af-variation="small"
                onAfOnClick={(e) => this.linkClickHandler(e)}>
                Förklaring av A*, A och AA
              </digi-link-internal>
              <h2>Tillvägagångssätt</h2>
              <h3>Steg 1: Identifiera era brister</h3>
              <p>
                Ta reda på vilka brister ni har. Gå igenom och testa enligt
                punkterna i designsystemets tillgänglighetslista. Fyll i
                resultatet i kommentarsfälten.
              </p>
              <digi-link-internal
                afHref="/tillganglighet-och-design/tillganglighet-checklista"
                af-variation="small"
                onAfOnClick={(e) => this.linkClickHandler(e)}>
                Designsystemets tillgänglighetslista
              </digi-link-internal>
              <br />
              <br />
              <p>
                Filtrera tillgänglighetslistan på svaren ”Kravet uppfylls inte”
                och ”Kravet uppfylls delvis” i filtret "Krav". Gå sedan till
                filtret "Kategori" och kryssa för alla kategorier utom kategorin
                "Konsekvent". Fyll sedan kommentarsfälten och exportera de
                punkter som visas i listan. Samtliga punkter där det finns
                brister ska ingå och beskrivas i era bristtexter.
              </p>
              <h3>Steg 2: Fyll i mallen</h3>
              <p>
                Ladda ner mallen för tillgänglighetsbrister. Fyll i uppgifterna
                under ”Information till redaktören”. Det är bara för
                verksamheten och kommer inte att publiceras publikt.
              </p>
              <h4>Mall för tillgänglighetsbrister</h4>
              <p>
                Intern länk endast för medarbetare på Arbetsförmedlingen.
                <digi-link-external
                  afHref="https://space.arbetsformedlingen.se/sites/tillganglighet/Delade%20dokument/Tillg%C3%A4nglighetsredog%C3%B6relser/Mallar/Mall%20lista%20p%C3%A5%20brister%20f%C3%B6r%20af-se.docx?d=w137e50d10c2441dd92adb80fac8e9bab"
                  af-variation="small"
                  af-target="_blank">
                  Mall för brister till tillgänglighetsredogörelse på SharePoint
                  (öppnas i egen flik)
                </digi-link-external>
              </p>
              <h3>Steg 3: Sammanfatta era brister</h3>
              <p>
                Fyll i uppgifterna under ”Information som ska publiceras
                publikt”. Denna text kommer att publiceras i redogörelsen.
              </p>
              <h4>Inga brister</h4>
              <p>
                Om det inte finns några brister så skriver ni följande: ”Den här
                sidan/tjänsten är helt förenlig med lagen om tillgänglighet till
                digital offentlig service.”.
              </p>
              <h4>Om ni har brister</h4>
              <p>
                Beskriv era brister på ett enkelt sätt så att de blir lätta att
                förstå för användarna.
              </p>
              <p>
                Lista bristerna i punktform. Det går bra att beskriva flera
                rader i tillgänglighetslistan i samma punkt, om de orsakar samma
                typ av problem. Om de orsakar olika typer av problem, undvik
                helst att blanda olika kriterier i samma punkt.
              </p>
              <p>För varje brist skriver ni:</p>
              <digi-list>
                <li>
                  En beskrivning av bristen, exempelvis var den finns och vilka
                  problem som bristen kan orsaka för användaren. Exempel: På
                  tjänstens startsida finns texter som inte är uppmärkta som
                  rubriker. Det gör det svårt för användare som navigerar med
                  uppläsande hjälpmedel att navigera utifrån rubriker och lista
                  rubrikerna på sidan.
                </li>
                <li>
                  Skäl till varför bristen finns, om det finns goda skäl.
                  Exempel: Begränsningar i publiceringsverktyget,
                  säkerhetsaspekter, upphandlad plattform... Formuleringar som
                  ”Inte hunnit", "visste inte” och liknande är inte goda skäl.
                </li>
                <li>
                  Ett datum för när bristen planeras att åtgärdas. Här går det
                  bra att gruppera flera brister under ett gemensamt datum.
                  Exempel: att ett övergripande arbete med digitala riktlinjer
                  kommer göras.
                </li>
              </digi-list>
              <h4>Steg 4: Ladda upp dokumentet</h4>
              <p>
                Ladda upp dokumentet på SharePoint i mappen för innevarande
                månad. Kom ihåg att ta bort det gamla dokumentet för er tjänst.
                Det ska bara finnas ett dokument per tjänst.
              </p>
              <p>
                Namnge dokumentet med tjänstens namn och datum för när
                dokumentet har uppdaterats och laddats upp, exempelvis
                ”Platsbanken 2026-10-23”.
              </p>
              <h5>Plats på SharePoint</h5>
              <p>
                Intern länk endast för medarbetare på Arbetsförmedlingen.
                <digi-link-external
                  afHref="https://space.arbetsformedlingen.se/sites/tillganglighet/Delade%20dokument/Forms/AllItems.aspx?csf=1&e=bq5ihn%2F&FolderCTID=0x0120005C2E125F40DFDF4CA509E09B8112068C&id=%2Fsites%2Ftillganglighet%2FDelade%20dokument%2FTillg%C3%A4nglighetsredog%C3%B6relser%2FRedog%C3%B6relser%20och%20checklistor"
                  af-variation="small"
                  af-target="_blank">
                  Tillgänglighetsredogörelser på SharePoint (öppnas i egen flik)
                </digi-link-external>
              </p>
            </article>
          </digi-layout-container>
        </digi-docs-page-layout>
      </host>
    );
  }
}
