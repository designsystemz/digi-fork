import { Component, h, State } from '@stencil/core';
import state from '../../../store/store';
import { router } from '../../../global/router';
import { match, Route } from 'stencil-router-v2';

import { setDocumentTitle } from '../../../global/documentTitle';

function formatMesage(message: string) {
  return message.split(' ').map((word) => {
    if (word[0] === '<' && word.slice(-2) === '/>') {
      const componentName = word.slice(1, -2).trim();
      return (
        <span>
          <digi-link afHref={`/komponenter/${componentName}/oversikt`}>
            {componentName}
          </digi-link>{' '}
        </span>
      );
    }
    return <span>{word} </span>;
  });
}

@Component({
  tag: 'digi-docs-component',
  styleUrl: 'digi-docs-component.scss',
  scoped: true
})
export class DigiDocsComponent {
  Router = router;

  @State() componentName: string;
  @State() componentTag: string;
  @State() activeTab: string;
  @State() deprecated?: {
    name: string;
    text: string;
  };

  componentWillLoad() {
    this.setActiveComponent();
  }

  componentWillUpdate() {
    this.setActiveComponent();
  }

  componentDidLoad() {
    setDocumentTitle(this.Router.activePath);
  }

  componentDidUpdate() {
    setDocumentTitle(this.Router.activePath);
  }

  getTab(): string {
    switch (state.router.newPaths[state.router.newPaths.length - 1]) {
      case 'api':
        return 'api';
      case 'oversikt':
        return 'oversikt'
    }
  }

  setActiveComponent() {
    if (state.router.newPaths[0] == 'komponenter') {
      this.activeTab = this.getTab();

      state.activeComponent = state.components.find(
        (c) => c.tag === state.router.newPaths[state.router.newPaths.length - 2]
      );

      this.componentName =
        state.activeComponent.docsTags?.find(
          (tag) => tag.name === 'swedishName'
        )?.text || state.activeComponent.tag;
      this.deprecated = state.activeComponent.docsTags.find(
        ({ name }) => name === 'deprecated'
      );
      this.componentTag = state.activeComponent.tag;
    }
  }

  toggleNavTabHandler(e) {
    const tag = e.target.dataset.tag;
    // Nested tabs in documentation page triggers this function
    // Need to check if tag exists before changing route
    if (tag) {
      this.Router.push(
        `${state.routerRoot}komponenter/${this.componentTag}/${tag}`
      );
    }
  }

  navigateTo(tabId: string) {
    this.Router.push(
      `${state.routerRoot}komponenter/${this.componentTag}/${tabId}`
    );
  }

  render() {
    return (
      <div class="digi-docs-component">
        <digi-docs-page-layout afPageHeading={this.componentName}>
          {this.deprecated != null && (
            <digi-layout-block>
              <digi-notification-alert
                af-size="large"
                af-variation="warning"
                af-heading="Utgår">
                {formatMesage(this.deprecated.text)}
              </digi-notification-alert>
            </digi-layout-block>
          )}
          <digi-layout-block>
            <p class="digi-subheading">{this.componentTag}</p>
          </digi-layout-block>
          <digi-layout-block>
            <digi-tablist
              afAriaLabel="Innehåll för komponent"
              afActiveTab={this.activeTab}
              afTabs={[
                { id: 'oversikt', title: 'Översikt' },
                { id: 'api', title: 'Kod' }
              ]}
              onAfTabChange={(event) => this.navigateTo(event.detail.id)}>
              <digi-tablist-panel tab="oversikt" />
              <digi-tablist-panel tab="api" />
            </digi-tablist>

            <this.Router.Switch>
              <Route
                path={match('/komponenter/:tag/oversikt')}
                render={({ tag }) => (
                  <digi-docs-component-details componentTag={tag} />
                )}
              />
              <Route
                path={match('/komponenter/:tag/api')}
                render={({ tag }) => (
                  <digi-docs-component-api componentTag={tag} />
                )}
              />
            </this.Router.Switch>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
