import { Component, h, State } from '@stencil/core';
import state from '../../../store/store';
import { ComponentStatus, statusForComponent } from '../../../helpers';

@Component({
  tag: 'digi-docs-component-list',
  styleUrl: 'digi-docs-component-list.scss',
  scoped: true
})
export class ComponentList {
  @State() pageName = 'Alla komponenter';
  @State() searchFilter = '';
  @State() fullWidthTagNames: string[] = [
    'digi-bar-chart',
    'digi-chart-line',
    'digi-footer',
    'digi-form-input-search',
    'digi-header',
    'digi-tools-feedback',
    'digi-table',
    'digi-quote-multi-container',
    'digi-notification-error-page'
  ];

  searchFilterChangeHandler(event) {
    const inputValue = event.detail.target.value;
    this.searchFilter = inputValue;
  }

  adjustComponentPositions(components, fullWidthTagNames) {
    let firstColumn = false;

    for (let i = 0; i < components.length - 1; i++) {
      if (fullWidthTagNames.includes(components[i].tag)) {
        if (firstColumn) {
          [components[i], components[i + 1]] = [
            components[i + 1],
            components[i]
          ];
          i++;
          firstColumn = false;
        }
      } else {
        firstColumn = !firstColumn;
      }
    }
    return components;
  }

  render() {
    const adjustedComponents = this.adjustComponentPositions(
      [...state.components],
      this.fullWidthTagNames
    );

    return (
      <host>
        <span slot="preamble">
          Här listar vi alla våra komponenter utom ikoner
        </span>
        <div class="digi-docs-component-list__filters">
          <div class="digi-docs-component-list__search-filter">
            <digi-form-input-search
              af-label={`Filtrera komponenter`}
              afLabel={`Filtrera komponenter`}
              af-hide-button="true"
              onAfOnInput={(e) =>
                this.searchFilterChangeHandler(e)
              }></digi-form-input-search>
          </div>
        </div>
        <div class="digi-docs-component-list__list">
          {adjustedComponents
            .filter((component) => !component.tag.includes('digi-icon'))
            .filter((component) => {
              if (!component.docsTags?.find((tag) => tag.name === 'private')) {
                return component;
              }
            })
            .filter((component) => {
              if (
                !component.docsTags
                  ?.find((tag) => tag.name === 'componentType')
                  ?.text.toLowerCase()
                  .includes('utility')
              ) {
                return component;
              }
            })
            .filter((component) => {
              if (this.searchFilter !== '') {
                if (
                  component.tag
                    .toLowerCase()
                    .includes(this.searchFilter.toLowerCase()) ||
                  component.docsTags
                    ?.find((tag) => tag.name === 'swedishName')
                    ?.text.toLowerCase()
                    .includes(this.searchFilter.toLowerCase())
                ) {
                  return true;
                }
                return false;
              }
              return true;
            })
            .map((component) => {
              const TagName = component.tag + '-details';
              const swedishName: string = component.docsTags?.find(
                (tag) => tag.name === 'swedishName'
              )?.text;
              const status = statusForComponent(component);
              return (
                <div
                  style={
                    this.fullWidthTagNames.includes(component.tag)
                      ? { gridColumn: 'span 2' }
                      : {}
                  }>
                  <digi-link afHref={`/komponenter/${component.tag}/oversikt`}>
                    <h2 class="digi-docs-component-list__item-heading">
                      {swedishName} <span>({component.tag})</span>
                      &nbsp;
                    </h2>
                    {status != null && (
                      <span style={{ display: 'flex', alignSelf: 'center' }}>
                        <digi-badge-status
                          af-type={
                            status === ComponentStatus.Deprecated
                              ? 'missing'
                              : 'prompt'
                          }
                          af-size="large"
                          af-variation="secondary"
                          afText={
                            status === ComponentStatus.Deprecated
                              ? 'Utgår'
                              : 'Ny'
                          }
                          class="badge"></digi-badge-status>
                      </span>
                    )}
                  </digi-link>
                  <TagName
                    afShowOnlyExample
                    afHideControls
                    afHideCode></TagName>
                </div>
              );
            })}
        </div>
      </host>
    );
  }
}
