/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, h, Prop, State, Watch } from '@stencil/core';
import { LayoutBlockContainer } from '@digi/arbetsformedlingen';

import state from '../../../store/store';
import { href } from 'stencil-router-v2';

@Component({
  tag: 'digi-docs-component-api',
  styleUrl: 'digi-docs-component-api.scss',
  scoped: true
})
export class DigiDocsComponentApi {
  @State() _componentTag: string;

  @Prop() componentTag: string;

  @Watch('componentTag')
  setComponentTag() {
    this._componentTag = this.componentTag;
  }

  protected get componentClassName() {
    const clearAndUpper = (text) => text.replace(/-/, '').toUpperCase();
    return state.activeComponent.tag.replace(/(^\w|-\w)/g, clearAndUpper);
  }

  componentWillLoad() {
    this.setComponentTag();
  }

  showSection(items): boolean {
    const arr = [];

    items.forEach((item) => {
      const tags = Object.values(item.docsTags).map((tag: any) => tag?.name);
      if (!tags.includes('ignore')) {
        tags.forEach((tag: string) => arr.push(tag));
      }
    });
    return arr.length > 0 ? true : false;
  }

  ignoreProperty(item): boolean {
    const arr = Object.values(item?.docsTags).map((tag: any) => tag?.name);

    return arr.includes('ignore');
  }

  render() {
    return (
      <div class="digi-docs-component-api">
        {this.showSection(state.activeComponent.props) && (
          <digi-layout-block
            class="digi-docs-component-api__block"
            af-container={LayoutBlockContainer.NONE}>
            <h2>Meta</h2>
            <digi-table af-variation="tertiary">
              <table>
                <caption>Meta</caption>
                <thead>
                  <tr>
                    <th>Attribut</th>
                    <th>Värde</th>
                    <th>Beskrivning</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>DOM element type</td>
                    <td>HTML{this.componentClassName}Element</td>
                    <td>
                      Interface i Typescript som kan användas för att sätta typ
                      på ett element-objekt, t.ex.{' '}
                      <digi-code
                        af-variation="light"
                        af-language="typescript"
                        afCode={'myButton: HTMLDigiButtonElement'}></digi-code>
                      .
                    </td>
                  </tr>
                </tbody>
              </table>
            </digi-table>
            <br />
            <h2>Attribut</h2>
            <digi-table af-variation="tertiary">
              <table>
                <caption>Attribut</caption>
                <thead>
                  <tr>
                    <th>Attribut</th>
                    <th>Property</th>
                    <th>Beskrivning</th>
                    <th>Typ</th>
                    <th>Förvalt</th>
                  </tr>
                </thead>
                <tbody>
                  {state.activeComponent.props.map((prop) => {
                    if (this.ignoreProperty(prop)) return;

                    return (
                      <tr>
                        <td>
                          <digi-code
                            af-variation="light"
                            afCode={`${prop.attr}${
                              prop.required ? ' (Obligatorisk)' : ''
                            }`}></digi-code>
                        </td>
                        <td>
                          <digi-code
                            af-variation="light"
                            af-language="typescript"
                            afCode={prop.name}></digi-code>
                        </td>
                        <td>
                          {prop.docs || ''}
                          {prop.deprecation && (
                            <div class="digi-docs-component-api__deprecated">
                              <span class="digi-docs-component-api__deprecated-badge">
                                Utfasad
                              </span>{' '}
                              {prop.deprecation}
                            </div>
                          )}
                        </td>
                        <td>
                          {prop.type && (
                            <digi-code
                              af-variation="light"
                              af-language="typescript"
                              afCode={prop.type}></digi-code>
                          )}
                        </td>
                        <td>
                          {prop.default && (
                            <digi-code
                              af-variation="light"
                              af-language="typescript"
                              afCode={prop.default}></digi-code>
                          )}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </digi-table>
          </digi-layout-block>
        )}
        {this.showSection(state.activeComponent.events) && (
          <digi-layout-block
            class="digi-docs-component-api__block"
            af-container={LayoutBlockContainer.NONE}>
            <h2>Events</h2>
            <digi-table af-variation="tertiary">
              <table>
                <caption>Events</caption>
                <thead>
                  <tr>
                    <th>Namn</th>
                    <th>Beskrivning</th>
                    <th>Detail</th>
                  </tr>
                </thead>
                <tbody>
                  {state.activeComponent.events.map((event) => {
                    if (this.ignoreProperty(event)) return;

                    return (
                      <tr>
                        <td>
                          <digi-code
                            af-variation="light"
                            afCode={event.event}></digi-code>
                        </td>
                        <td>{event.docs}</td>
                        <td>
                          <digi-code
                            afCode={event.detail || 'any'}
                            af-language="typescript"
                            af-variation="light"></digi-code>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </digi-table>
          </digi-layout-block>
        )}
        {this.showSection(state.activeComponent.methods) && (
          <digi-layout-block
            class="digi-docs-component-api__block"
            af-container={LayoutBlockContainer.NONE}>
            <h2>Metoder</h2>
            <digi-table af-variation="tertiary">
              <table>
                <caption>Metoder</caption>
                <thead>
                  <tr>
                    <th>Namn</th>
                    <th>Beskrivning</th>
                    <th>Returtyp</th>
                    <th>Signatur</th>
                  </tr>
                </thead>
                <tbody>
                  {state.activeComponent.methods.map((method) => {
                    if (this.ignoreProperty(method)) return;

                    return (
                      <tr>
                        <td>
                          <digi-code
                            af-variation="light"
                            afCode={method.name}></digi-code>
                        </td>
                        <td>{method.docs}</td>
                        <td>
                          <digi-code
                            afCode={method.returns?.type || 'void'}
                            af-language="typescript"
                            af-variation="light"></digi-code>
                        </td>
                        <td>
                          <digi-code
                            afCode={method.signature}
                            af-language="typescript"
                            af-variation="light"></digi-code>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </digi-table>
          </digi-layout-block>
        )}
        {state.activeComponent.styles?.length > 0 && (
          <digi-layout-block
            class="digi-docs-component-api__block"
            af-container={LayoutBlockContainer.NONE}>
            <h2>Tokens</h2>
            <digi-table af-variation="tertiary">
              <table>
                <caption>Tokens</caption>
                <thead>
                  <tr>
                    <th>Namn</th>
                    <th>Värde</th>
                  </tr>
                </thead>
                <tbody>
                  {state.activeComponent.styles.map((token) => {
                    return (
                      <tr>
                        <td>
                          <digi-code
                            afCode={token.name}
                            af-variation="light"
                            af-language="css"></digi-code>
                        </td>
                        <td>
                          <digi-code
                            afCode={token.docs}
                            af-variation="light"
                            af-language="css"></digi-code>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </digi-table>
          </digi-layout-block>
        )}
        {state.activeComponent.docsTags?.filter((tag) => tag.name === 'enums')
          .length > 0 && (
          <digi-layout-block
            class="digi-docs-component-api__block"
            af-container={LayoutBlockContainer.NONE}>
            <h2>Enums</h2>
            <digi-table af-variation="tertiary">
              <table>
                <caption>Enums</caption>
                <thead>
                  <tr>
                    <th>Namn</th>
                    <th>Filnamn</th>
                  </tr>
                </thead>
                <tbody>
                  {state.activeComponent.docsTags
                    .filter((tag) => tag.name === 'enums')
                    .map((enumb) => {
                      return (
                        <tr>
                          <td>
                            <digi-code
                              afCode={`enum ${enumb.text.split(' - ')[0]}`}
                              af-variation="light"
                              af-language="typescript"></digi-code>
                          </td>
                          <td>
                            <digi-code
                              afCode={enumb.text.split(' - ')[1]}
                              af-variation="light"
                              af-language="bash"></digi-code>
                          </td>
                        </tr>
                      );
                    })}
                </tbody>
              </table>
            </digi-table>
          </digi-layout-block>
        )}
        {state.activeComponent.dependents?.length > 0 && (
          <digi-layout-block
            class="digi-docs-component-api__block"
            af-container={LayoutBlockContainer.NONE}>
            <h2>Används av</h2>
            <ul>
              {state.activeComponent.dependents.map((dependent) => {
                return (
                  <li>
                    <a {...href(`komponenter/${dependent}/oversikt`)}>
                      {dependent}
                    </a>
                  </li>
                );
              })}
            </ul>
          </digi-layout-block>
        )}
        {state.activeComponent.dependencies?.length > 0 && (
          <digi-layout-block
            class="digi-docs-component-api__block"
            af-container={LayoutBlockContainer.NONE}>
            <h2>Använder sig av</h2>
            <ul>
              {state.activeComponent.dependencies.map((dependency) => {
                return (
                  <li>
                    <a {...href(`komponenter/${dependency}/oversikt`)}>
                      {dependency}
                    </a>
                  </li>
                );
              })}
            </ul>
          </digi-layout-block>
        )}
      </div>
    );
  }
}
