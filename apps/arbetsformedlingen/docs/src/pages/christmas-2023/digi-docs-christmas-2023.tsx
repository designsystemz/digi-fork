import { Fragment, Prop } from '@stencil/core';
import { getAssetPath } from '@stencil/core';
import { Component, Host, h } from '@stencil/core';
import { router } from '../../global/router';

@Component({
  tag: 'digi-docs-christmas-2023',
  styleUrl: 'digi-docs-christmas-2023.scss',
  scoped: true
})
export class Christmas2023 {
  Router = router;

  @Prop() afHideGrid: boolean;

  linkHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <Host>
        <digi-layout-container af-margin-bottom af-margin-top>
          <div class="christmas-wrapper">
            <div class="christmas-header">
              <digi-media-image
                afUnlazy
                afSrc={getAssetPath('/assets/images/christmas/kulor.svg')}
                afWidth="130"
                afAlt=""></digi-media-image>
              <h1>
                God jul och
                <br /> gott nytt år
              </h1>
              <p>
                önskar teamet bakom{' '}
                <span>Arbetsförmedlingens designsystem</span>
              </p>

              {this.afHideGrid ? (
                <digi-link-internal
                  afHref="/god-jul"
                  af-variation="small"
                  onAfOnClick={(e) => this.linkHandler(e)}>
                  Lite av vad som hände under 2023
                </digi-link-internal>
              ) : (
                <h2>Lite av vad som hände under 2023...</h2>
              )}
            </div>

            {!this.afHideGrid && (
              <Fragment>
                <div class="christmas-grid">
                  <div>
                    <digi-media-image
                      afUnlazy
                      afSrc={getAssetPath(
                        '/assets/images/christmas/pusselbitar.svg'
                      )}
                      afWidth="200"
                      afAlt=""></digi-media-image>
                    <div>
                      <h3>
                        fler än 1500 <span>uppdateringar i UI-kitet</span>
                      </h3>
                    </div>
                  </div>
                  <div>
                    <digi-media-image
                      afUnlazy
                      afSrc={getAssetPath('/assets/images/christmas/lampa.svg')}
                      afWidth="200"
                      afAlt=""></digi-media-image>
                    <div>
                      <h3>
                        mer än 100 <span>teamworkshops</span>
                      </h3>
                    </div>
                  </div>
                  <div>
                    <digi-media-image
                      afUnlazy
                      afSrc={getAssetPath('/assets/images/christmas/mugg.svg')}
                      afWidth="200"
                      afAlt=""></digi-media-image>
                    <div>
                      <h3>
                        minst 1 <span>release per vecka</span>
                      </h3>
                    </div>
                  </div>
                  <div>
                    <digi-media-image
                      afUnlazy
                      afSrc={getAssetPath('/assets/images/christmas/barn.svg')}
                      afWidth="200"
                      afAlt=""></digi-media-image>
                    <div>
                      <h3>
                        6 st <span>nya team-medlemmar</span>
                      </h3>
                    </div>
                  </div>
                  <div>
                    <digi-media-image
                      afUnlazy
                      afSrc={getAssetPath('/assets/images/christmas/blad.svg')}
                      afWidth="200"
                      afAlt=""></digi-media-image>
                    <div>
                      <h3>
                        11 st <span>nya designmönster</span>
                      </h3>
                    </div>
                  </div>
                  <div>
                    <digi-media-image
                      afUnlazy
                      afSrc={getAssetPath(
                        '/assets/images/christmas/hjarta.svg'
                      )}
                      afWidth="200"
                      afAlt=""></digi-media-image>
                    <div>
                      <h3>
                        minst 15 <span>tillgänglighetsgranskningar</span>
                      </h3>
                    </div>
                  </div>
                  <div>
                    <digi-media-image
                      afUnlazy
                      afSrc={getAssetPath(
                        '/assets/images/christmas/jultomte.svg'
                      )}
                      afWidth="200"
                      afAlt=""></digi-media-image>
                    <div>
                      <h3>
                        open source{' '}
                        <span>
                          vi delar med oss av vår kod till fler myndigheter
                        </span>
                      </h3>
                    </div>
                  </div>
                  <div>
                    <digi-media-image
                      afUnlazy
                      afSrc={getAssetPath(
                        '/assets/images/christmas/dator-lista.svg'
                      )}
                      afWidth="200"
                      afAlt=""></digi-media-image>
                    <div>
                      <h3>
                        typ 2000{' '}
                        <span>uppdateringar av vår dokumentationssite</span>
                      </h3>
                    </div>
                  </div>
                  <div>
                    <digi-media-image
                      afUnlazy
                      afSrc={getAssetPath(
                        '/assets/images/christmas/planta.svg'
                      )}
                      afWidth="200"
                      afAlt=""></digi-media-image>
                    <div>
                      <h3>
                        5 st <span>onboardingtillfällen</span>
                      </h3>
                    </div>
                  </div>
                  <div>
                    <digi-media-image
                      afUnlazy
                      afSrc={getAssetPath(
                        '/assets/images/christmas/julgran.svg'
                      )}
                      afWidth="200"
                      afAlt=""></digi-media-image>
                    <div>
                      <h3>
                        <span>... och mycket mycket mer!</span>
                      </h3>
                    </div>
                  </div>
                </div>
                <h2 class="christmas-greeting">Vi ses nästa år!</h2>
              </Fragment>
            )}
          </div>
        </digi-layout-container>
      </Host>
    );
  }
}
