import { Component, getAssetPath, h, State } from '@stencil/core';
import { GraphicsExampleType } from '../graphics-example/graphics-example-type.enum';

@Component({
  tag: 'digi-docs-about-graphics',
  styleUrl: 'digi-docs-about-graphics.scss',
  scoped: true
})
export class AboutGraphics {
  @State() pageName = 'Vår grafiska profil';

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-graphics/digi-docs-about-graphics.tsx">
          <span slot="preamble">
            Här samlar vi all den information som är relevant för dig som
            producerar grafiskt material och tjänster under Arbetsförmedlingens
            varumärke.
            <br />
            <br />
            <b>Detta är en kort sammanfattning.</b> Vill du fördjupa dina
            kunskaper ytterligare kan du läsa vidare under respektive avdelning
            i menyn.
          </span>
          <digi-layout-container af-margin-bottom afMarginTop>
            <article class="digi-docs-about-graphics">
              <h2>Logotyp</h2>
              <p>
                Logotypen är en symbol för allt det som Arbetsförmedlingen
                representerar. Därför skall alltid logotypen finnas med i någon
                form där Arbetsförmedlingen står som avsändare. Den finns i två
                varianter anpassad för ljus eller mörk bakgrund. Logotypen får
                inte ändras i färg, storleksförhållande eller på något annat
                sätt förvanskas. Se till att den har god kontrast till
                bakomliggande färg eller foto. Ge logotypen utrymme i form av
                marginaler och en tillräcklig frizon i förhållande till andra
                objekt.
              </p>
              <div class="examples examples--two-columns">
                <digi-docs-graphics-example>
                  <h3 slot="heading">Rätt användning av logotypen</h3>
                  <digi-media-image
                    afSrc="/assets/images/graphics-example/logo-good.jpg"
                    af-unlazy
                    af-fullwidth
                    afAlt="Exempel bild på rätt användning av logotypen"></digi-media-image>
                  <p slot="footer">
                    Primär och sekundär logotyp och förslag på frizon.
                  </p>
                </digi-docs-graphics-example>
                <digi-docs-graphics-example
                  exampleType={GraphicsExampleType.BAD}>
                  <h3 slot="heading">Felaktig användning av logotypen</h3>
                  <digi-media-image
                    afSrc="/assets/images/graphics-example/logo-bad.jpg"
                    af-unlazy
                    af-fullwidth
                    afAlt="Exempel bild på en dålig användning av logotypen"></digi-media-image>
                  <p slot="footer">
                    <b>Felaktiga exempel</b> på användning av logotypen i bilden
                    ovan. Det är dålig kontrast, det saknas en frizon och
                    logotypen förvanskas.
                  </p>
                </digi-docs-graphics-example>
              </div>

              <h2>Färger</h2>
              <p>
                Vi har en definierad färgpalett med profilfärger som bygger
                Arbetsförmedlingens grafiska identitet. Färgpaletten är indelad
                i olika kategorier som används för olika syften. Vissa färger är
                endast tänkta för dekoration och andra anpassade för att uppnå
                godkända kontrastförhållanden. Tex. för text och
                informationsbärande grafik. Exempelvis så är den profilgröna som
                är en dekorationsfärg inte lämplig som textfärg, eftersom den
                inte uppfyller kontrastkraven.
              </p>

              <div class="examples examples--two-columns">
                <digi-docs-graphics-example>
                  <h3 slot="heading">Rätt användning av färger</h3>
                  <digi-media-image
                    afSrc="/assets/images/graphics-example/colors-good.svg"
                    af-unlazy
                    af-fullwidth
                    afAlt="Exempel bild på en rätt användning av färger"></digi-media-image>
                  <p slot="footer">
                    Cirkeln visar en ungefärlig volym i hur vi arbetar med de
                    profilbärande färgerna. Utklipp från diagram med fullgod
                    kontrast och olika mönster för att underlätta vid
                    färgblindhet.
                  </p>
                </digi-docs-graphics-example>
                <digi-docs-graphics-example
                  exampleType={GraphicsExampleType.BAD}>
                  <h3 slot="heading">Felaktig användning av färger</h3>
                  <digi-typography-preamble style={{ color: '#95C23E' }}>
                    <b style={{ color: '#95C23E' }}>
                      Denna profilgröna färg på denna mening uppfyller inte
                      kontrastkraven
                    </b>
                  </digi-typography-preamble>
                  <p style={{ color: '#9F9F9F' }}>
                    <p>
                      Om du använder en grå text, tänk då också på att den
                      uppfyller kontrastkraven. Det gör inte denna text.
                    </p>
                  </p>
                  <digi-media-image
                    afSrc="/assets/images/graphics-example/colors-bad.svg"
                    af-unlazy
                    af-fullwidth
                    afAlt="Exempel bild som visar en dålig användning av färger i ett diagram"></digi-media-image>
                  <p slot="footer">
                    <b>Felaktiga exempel</b> med en profilgrön text på vit
                    bakgrund. Samt vit text på profilgrön bakgrund. Utklipp från
                    diagram som saknar tillräcklig kontrast.
                  </p>
                </digi-docs-graphics-example>
              </div>

              <h2>Typografi</h2>
              <p>
                Vi använder typsnittet OpenSans som Arbetsförmedlingens
                standardtypsnitt i den grafiska profilen. I vissa situationer
                behövs alternativ, till exempel i system som inte stöder
                OpenSans. Då används främst Arial. För att maximera läsbarheten,
                har vi uteslutit den tunnaste varianten och kursiv text i
                löpande text. Skriv aldrig text med versaler. Det signalerar
                höjt tonläge (skrikigt) och sänker läsbarheten. Vi ser också
                till att ha ett generöst radavstånd.
              </p>

              <div class="examples examples--two-columns">
                <digi-docs-graphics-example>
                  <h3 slot="heading">Rätt användning av typografin</h3>
                  <div>
                    <h2 class="heading--no-margin-top">Logotyp</h2>
                    <h4 class="heading--no-margin-top">
                      Arbetsförmedlingens logotyp består av en ordbild och en
                      symbol. Logotypen är bärare av de värden
                      Arbetsförmedlingen står för.
                    </h4>
                    <p>
                      Mittcirkeln i symbolen står för människan i centrum. Den
                      yttre delen av en omringande cirkel för det som
                      Arbetsförmedlingen erbjuder i form av möjligheter och
                      stöd. Den gröna färgen står för växtkraft och den blåa
                      färgen signalerar en förtroendegivande och trygg
                      myndighet.
                    </p>
                    <h1 class="heading--no-margin-top">H1 Sidorubrik</h1>
                    <h2 class="heading--no-margin-top">H2 Underrubrik</h2>
                    <h3 class="heading--no-margin-top">
                      H3 Sekundär underrubrik
                    </h3>
                    <h4 class="heading--no-margin-top">H4 Rubrik & ingress</h4>
                    <p>16px Löpande text (brödtext)</p>
                  </div>
                  <p slot="footer">
                    Här visas en rubrik med ingress och brödtext enligt de
                    typografimallar vi har för webben. Samt vilka typer av
                    rubrik och brödtext som finns tillgängliga.
                  </p>
                </digi-docs-graphics-example>
                <digi-docs-graphics-example
                  exampleType={GraphicsExampleType.BAD}>
                  <h3 slot="heading">Felaktig användning av typografin</h3>
                  <div>
                    <h2 class="heading--no-margin-top">LOGOTYP</h2>
                    <h4 class="heading--no-margin-top">
                      ARBETSFÖRMEDLINGENS LOGOTYP BESTÅR AV EN ORDBILD OCH EN
                      SYMBOL. LOGOTYPEN ÄR BÄRARE AV DE VÄRDEN
                      ARBETSFÖRMEDLINGEN STÅR FÖR.
                    </h4>
                    <p style={{ 'font-size': '0.75rem' }}>
                      Mittcirkeln i symbolen står för människan i centrum. Den
                      yttre delen av en omringande cirkel för det som
                      Arbetsförmedlingen erbjuder i form av möjligheter och
                      stöd. Den gröna färgen står för växtkraft och den blåa
                      färgen signalerar en förtroendegivande och trygg
                      myndighet.
                    </p>
                    <h1>Logotyp</h1>
                    <p>
                      <b>
                        <i>
                          Arbetsförmedlingens logotyp består av en ordbild och
                          en symbol. Logotypen är bärare av de värden
                          Arbetsförmedlingen står för.
                        </i>
                      </b>
                    </p>
                    <p style={{ 'font-weight': '100' }}>
                      Mittcirkeln i symbolen står för människan i centrum. Den
                      yttre delen av en omringande cirkel för det som
                      Arbetsförmedlingen erbjuder i form av möjligheter och
                      stöd. Den gröna färgen står för växtkraft och den blåa
                      färgen signalerar en förtroendegivande och trygg
                      myndighet.
                    </p>
                  </div>
                  <p slot="footer">
                    <b>Felaktiga exempel</b> med en rubrik och ingress skriven
                    med versaler. Exempel två visar en tunn variant av
                    typsnittet, samt kursiv ingress som försvårar läsbarheten.
                  </p>
                </digi-docs-graphics-example>
              </div>

              <h2>Grafik</h2>
              <p>
                Vi har tre större kategorier av grafik; infografik,
                funktionsikoner och diagram.
                <br />
                <br />
                Diagram är en visualisering av statistisk data. Dessa skall
                alltid presenteras ihop med en tabell för att kunna tillgodose
                olika behov. Det är ett tillgänglighetskrav. Likaså är det
                viktigt att tänka på val av färger för fullgod kontrast och tex.
                hur någon med färgblindhet kan läsa av grafiken. Det finns
                specifika färgpaletter för dessa ändamål.
                <br />
                <br />
                Funktionsikoner använder vi för att förstärka knappar och
                komponenter på webben. De kan ses lite som ett språk i
                symbolform. Dessa skall inte användas som utsmyckande grafik. En
                knapp skall t.ex. ha en text och ikon som har ett logiskt
                samband.
                <br />
                <br />
                Infografik finns som ett stöd för att förstärka texten eller
                beskriva ett flöde och är oftast endast utsmyckande. Dessa går
                att bygga upp själv utefter behov. Men se till att de skapar ett
                tydligt samband och att linjer och storlek harmoniserar.
              </p>
              <div class="examples examples--two-columns">
                <digi-docs-graphics-example>
                  <h3 slot="heading">Rätt användning av grafik</h3>
                  <digi-media-image
                    afSrc="/assets/images/graphics-example/graphics-good.svg"
                    af-unlazy
                    af-fullwidth
                    afAlt="Illustration på bra användning av grafik"></digi-media-image>
                  <p slot="footer">
                    Ett diagram med linjer för att tillgodose kontrast mot
                    bakgrund. Knappexemel samt sammansatt infografik.
                  </p>
                </digi-docs-graphics-example>
                <digi-docs-graphics-example
                  exampleType={GraphicsExampleType.BAD}>
                  <h3 slot="heading">Felaktig användning av grafik</h3>
                  <digi-media-image
                    afSrc="/assets/images/graphics-example/graphics-bad.svg"
                    af-unlazy
                    af-fullwidth
                    afAlt="Illustration på dålig användning av grafik"></digi-media-image>
                  <p slot="footer">
                    <b>Felaktiga exempel</b> där diagramet har dålig kontrast
                    mot bakgrund. Knappexempel med ologisk koppling mellan
                    symbol och text. Samt infografik sammansatt på ett olämpligt
                    sätt.
                  </p>
                </digi-docs-graphics-example>
              </div>

              <h2>Illustrationer</h2>
              <p>
                Illustrationer används sparsamt. Framförallt är det för extern
                kommunikation på webbplatsen eller i trycksaker. Dessa tas fram
                efter behov av kommunikationsavdelningen. De beskriver oftast en
                händelse eller känslor. Illustrationerna är vektorbaserade och
                går att justera lite efter behov. Undvik olämpliga placeringar
                av “färgklickarna”, som t.ex. en röd fläck på ett huvud.
              </p>
              <div class="examples">
                <digi-docs-graphics-example>
                  <img
                    src={getAssetPath(
                      '/assets/images/graphics-example/illustrations.jpg'
                    )}
                    style={{ padding: '0 15%' }}
                    alt="Illustration på olika typer av bra illustrationer"
                  />
                  <p slot="footer">
                    Exempel på tre olika typer av illustrationer.
                  </p>
                </digi-docs-graphics-example>
              </div>

              <h2>Bildspråk</h2>
              <p>
                Vi vill påverka genom engagemang och därför är människor en
                naturlig del av vårt bildspråk. Bilden representerar ett
                tvärsnitt av befolkningen och speglar verkligheten. Vi undviker
                uppställda och poserande uttryck och väljer bilder som känns
                äkta och fångar ögonblick i vardagen.
                <br />
                <br />
                Använd inte endast en bild för att förmedla en text. En så
                kallad "alt-text" bör beskriva innehållet och eventuella budskap
                i en bild.
                <br />
                <br />I Arbetsförmedlingens mediebank finner du bilder för alla
                typer av ändamål och det är huvudkällan för all typ av
                produktion inom Arbetsförmedlingen.
              </p>
              <div class="examples">
                <digi-docs-graphics-example>
                  <digi-media-image
                    afSrc="/assets/images/graphics-example/image-language.jpg"
                    af-unlazy
                    af-fullwidth
                    afAlt="Fyra bild exempel på Arbetsförmedlingens bildspråk"></digi-media-image>
                  <p slot="footer">
                    Några exempel på hur Arbetsförmedlingens bildspråk ser ut.
                    Hämtade ur Mediebanken.
                  </p>
                </digi-docs-graphics-example>
              </div>

              <h2>Namngivning</h2>
              <p>
                Det är viktigt att vi använder ett begripligt klarspråk. Det
                gäller allt ifrån löpande text, till hur vi namnger tjänster och
                webbadresser. Använd inte egennamn, arbetsnamn, förkortningar
                eller engelska uttryck. Blanda inte gemener och versaler. Vid
                namn på tjänster skall det finnas en tydlig koppling till vad
                tjänsten gör eller vad den fyller för behov. Undvik också
                komplexa webbadresser som inte har en direkt koppling till
                tjänsten.
              </p>
              <div class="examples examples--two-columns">
                <digi-docs-graphics-example>
                  <h3 slot="heading">Exempel på bra namngivningar</h3>
                  <img
                    src={getAssetPath(
                      '/assets/images/graphics-example/naming-good-1.svg'
                    )}
                    class="image-list"
                    alt="Sjuk och frisk"
                  />
                  <img
                    src={getAssetPath(
                      '/assets/images/graphics-example/naming-good-2.svg'
                    )}
                    class="image-list"
                    alt="Mediebanken"
                  />
                  <p class="text-ellipsis">
                    tajma.arbetsformedlingen.se
                    <br />
                    designsystem.arbetsformedlingen.se
                    <br />
                    informationshanteringsplan.arbetsformedli…
                  </p>
                  <p slot="footer">
                    Exempel på webbadress som säger något om tjänsten. Titel på
                    tjänsten som förklarar dess funktion.
                  </p>
                </digi-docs-graphics-example>
                <digi-docs-graphics-example
                  exampleType={GraphicsExampleType.BAD}>
                  <h3 slot="heading">Exempel på dåliga namngivningar</h3>
                  <img
                    src={getAssetPath(
                      '/assets/images/graphics-example/naming-bad-1.svg'
                    )}
                    class="image-list"
                    alt="Äpple"
                  />
                  <img
                    src={getAssetPath(
                      '/assets/images/graphics-example/naming-bad-2.svg'
                    )}
                    class="image-list"
                    alt="DS Direkt"
                  />
                  <img
                    src={getAssetPath(
                      '/assets/images/graphics-example/naming-bad-3.svg'
                    )}
                    class="image-list"
                    alt="Ester"
                  />
                  <p>
                    cmssystem.af.gate.io
                    <br />
                    www.ams.se/arbetsformedlingen
                  </p>
                  <p slot="footer">
                    <b>Felaktiga exempel</b> med en webbadress som inte är
                    kopplat till tjänstens namn. Tjänstenamn skrivet med
                    versaler, på engelska och med förkortning. Eller egennamn.
                  </p>
                </digi-docs-graphics-example>
              </div>
            </article>
          </digi-layout-container>
        </digi-docs-page-layout>
      </host>
    );
  }
}
