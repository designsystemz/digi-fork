import { Component, h, State } from '@stencil/core';
import state from '../../../src/store/store';
// eslint-disable-next-line @nx/enforce-module-boundaries
import designTokens from '../../../../../../dist/libs/arbetsformedlingen/dist/digi-arbetsformedlingen/design-tokens/styleguide/web.json';
import { TokenFormat } from '../../components/design-tokens/token-format.enum';
import { router } from '../../global/router';
import { Route } from 'stencil-router-v2';
import { LayoutBlockContainer } from '@digi/arbetsformedlingen';

type Tokens = typeof designTokens;

@Component({
  tag: 'digi-docs-design-tokens',
  styleUrl: 'digi-docs-design-tokens.scss',
  scoped: true
})
export class DesignTokens {
  Router = router;
  @State() pageName = 'Design Tokens';
  @State() pageNameLanguage = 'en';

  @State() tokenFormat: TokenFormat = TokenFormat.CUSTOM_PROPERTY;
  @State() searchTerm = '';

  @State() colorTokens: Tokens;
  @State() borderTokens: Tokens;
  @State() layoutTokens: Tokens;
  @State() animationTokens: Tokens;

  @State() activeTab: string;

  componentWillLoad() {
    this.activeTab = this.getTabId();
    this.colorTokens = [
      ...designTokens.filter((token) =>
        token.filePath.includes('src/styles/color/color.brand')
      )
    ];

    this.borderTokens = [
      ...designTokens.filter((token) =>
        token.filePath.includes('src/styles/border/border.brand')
      )
    ];

    this.layoutTokens = [
      ...designTokens.filter((token) =>
        token.filePath.includes('src/styles/layout/layout.brand')
      )
    ];

    this.animationTokens = [
      ...designTokens.filter((token) =>
        token.filePath.includes('src/styles/animation/animation.brand')
      )
    ];
  }

  getTabId(): string {
    const tabName = state.router.activePath;
    switch (tabName) {
      case '/design-tokens/bibliotek':
        return 'bibliotek';
      case '/design-tokens/beskrivning':
        return 'beskrivning';
    }
  }

  toggleNavTabHandler(e) {
    const tag = e.target.dataset.tag;
    this.Router.push(`${state.routerRoot}design-tokens/${tag}`);
  }

  navigateTo(tabId: string) {
    this.Router.push(`${state.routerRoot}design-tokens/${tabId}`);
  }

  componentDidLoad() {
    const pageNameLang = document.getElementsByClassName(
      'digi-typography-heading-jumbo'
    )[0];
    pageNameLang.setAttribute('lang', this.pageNameLanguage);
  }

  get tabs() {
    return [
      { id: 'bibliotek', title: 'Bibliotek' },
      { id: 'beskrivning', title: 'Beskrivning' }
    ];
  }

  render() {
    return (
      <digi-docs-page-layout
        af-page-heading={this.pageName}
        af-edit-href="pages/design-tokens/digi-docs-design-tokens.tsx">
        <digi-layout-block afVerticalPadding>
          <digi-typography-preamble>
            <span lang="en">Design tokens</span> representerar alla designbeslut
            och är basen till all design i designsystemet. Det kan vara allt
            från färger, typografi, kantlinjer, avstånd med mera. Dessa skapas
            centralt och uppdateras från ett och samma ställe.
          </digi-typography-preamble>
        </digi-layout-block>
        <br />
        <br />
        <digi-layout-block afVerticalPadding>
          <digi-tablist
            afAriaLabel="Innehåll för design tokens"
            afTabs={this.tabs}
            afActiveTab={this.activeTab}
            onAfTabChange={(event) => this.navigateTo(event.detail.id)}>
            {this.tabs.map((tab) => (
              <digi-tablist-panel tab={tab.id} />
            ))}
          </digi-tablist>
          <digi-layout-block af-container={LayoutBlockContainer.NONE}>
            <this.Router.Switch>
              <Route path="/design-tokens/bibliotek">
                <digi-docs-design-tokens-library />
              </Route>
              <Route path="/design-tokens/beskrivning">
                <digi-docs-design-tokens-description />
              </Route>
            </this.Router.Switch>
          </digi-layout-block>
        </digi-layout-block>
      </digi-docs-page-layout>
    );
  }
}
