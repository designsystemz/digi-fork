import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';

@Component({
  tag: 'digi-docs-about-design-pattern-grids',
  styleUrl: 'digi-docs-about-design-pattern-grids.scss',
  scoped: true
})
export class AboutDesignPatternGrids {
  @State() pageName = 'Grid och brytpunkter';
  Router = router;

  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  isSubset(superObj, subObj) {
    return Object.keys(subObj).every((ele) => {
      if (typeof subObj[ele] == 'object') {
        return this.isSubset(superObj[ele], subObj[ele]);
      }
      return subObj[ele] === superObj[ele];
    });
  }

  groupTokens(arr = []) {
    const res = [];
    const map = {};
    let i, j, curr;

    for (i = 0, j = arr.length; i < j; i++) {
      curr = arr[i];
      if (!(curr.attributes.subitem in map)) {
        map[curr.attributes.subitem] = {
          name: curr.attributes.subitem,
          variations: []
        };
        res.push(map[curr.attributes.subitem]);
      }
      map[curr.attributes.subitem].variations.push(curr);
    }
    return res;
  }

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-design-pattern-grids/digi-docs-about-design-pattern-grids.tsx">
          <span slot="preamble">
            Här beskriver vi brytpunkter och responsiv bakgrundsgrid som vi
            rekommenderar att använda för att få ett homogent uttryck på våra
            webbar och tjänster. Gridden ska hjälpa oss att använda konsekventa
            yttermarginaler, kolumnmellanrum och brytpunkter. Gridden är
            skelettet som vi sedan bygger en mängd olika layouter på, för att
            passa med det specifika innehåll och budskap som ska visas.
          </span>
          <br />
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            afMarginTop>
            <digi-layout-columns af-variation="two">
              <div>
                <digi-media-image
                  afUnlazy
                  afHeight="289"
                  afWidth="646"
                  afSrc="/assets/images/pattern/Rityta-Satsyta.svg"
                  afAlt="Illustration som betonar vad en rityta på en sida"></digi-media-image>
                <h3>Rityta</h3>
                <p>
                  Ritytan är den maximala bredden på sidans innehåll, det är
                  ytan som vi lägger upp vår design på. Yttermarginaler
                  tillkommer och ligger utanför ritytan. (Om vi inte gör
                  utfallande block, som till exempel herobilder på startsidor).
                  Maximala bredden förändras vid varje responsiv brytpunkt
                </p>
              </div>

              <div>
                <digi-media-image
                  afUnlazy
                  afHeight="289"
                  afWidth="629"
                  afSrc="/assets/images/pattern/Yttermarginaler.svg"
                  afAlt="Illustration som betonar yttermarginalerna på en sida"></digi-media-image>
                <h3>Yttermarginaler</h3>
                <p>
                  Yttermarginaler är de yttre marginalerna på gridden. Dessa
                  ligger utanför ritytans bredd.
                </p>
              </div>

              <div>
                <digi-media-image
                  afUnlazy
                  afHeight="289"
                  afWidth="646"
                  afSrc="/assets/images/pattern/kolumner.svg"
                  afAlt="Illustration på exempel kolumner som används för att designa"></digi-media-image>
                <h3>Kolumner</h3>
                <p>
                  Vid 1200px ner till ≥ 768 px har gridden 12 responsiva
                  kolumner. Kolumnernas bredd är elastisk och förändras beroende
                  på bredden av webbsidan. Och kolumnerna blir färre desto
                  mindre skärm som designen och visas på.
                </p>
              </div>

              <div>
                <digi-media-image
                  afUnlazy
                  afHeight="289"
                  afWidth="646"
                  afSrc="/assets/images/pattern/Gutters.svg"
                  afAlt="Illustration som betonar kolumnmellanrum"></digi-media-image>
                <h3> Gutters (kolumnmellanrum)</h3>
                <p>
                  Gutters är mellanrummet mellan griddens kolumner. Gutter bredd
                  har fasta värden (16 px, 24 px, etc.) beroende på skärmbredd
                  och vald brytpunkt.
                </p>
              </div>

              <div>
                <digi-media-image
                  afUnlazy
                  afHeight="320"
                  afWidth="630"
                  afSrc="/assets/images/pattern/Layouter.svg"
                  afAlt="Illustration som visar var som menas med olika layouter som går från fullbred till fyrspalt"></digi-media-image>
                <h3>Layouter</h3>
                <p>
                  En mängd olika layouter är möjliga på samma bakgrundgrid, som
                  exemplet ovan visar. Fullbredd eller fyrspalt, eller något
                  däremellan är möjligt att designa på en responsiv grid.
                  Konekventa kolumnmellanrum och förutbestämda brytpunkter och
                  yttermarginaler hjälper till att skapa ett homogent uttryck.
                </p>
                <br />
              </div>

              <div>
                <digi-layout-block af-variation="secondary" afVerticalPadding>
                  <h3>POC och liveexempel på responsiv grid</h3>
                  <p>
                    Här har vi ett exempel på hur den responsiva gridden beter
                    sig vid olika brytpunkter
                  </p>
                  <digi-link-internal
                    afHref="/designmonster/grid-och-brytpunkter/grid-poc"
                    af-target="_blank"
                    af-variation="small">
                    Se liveexempel på responsiv grid/poc
                  </digi-link-internal>
                </digi-layout-block>
              </div>
            </digi-layout-columns>
            <div>
              <h3>Grundläggande brytpunkter för Arbetsförmedlingens webbar</h3>
              <p>
                Dessa brytpunkter är det primära valet för att skapa en
                enhetlighet och ett sammanhängande visuellt uttryck. Undantag är
                tillåtna för specifika tjänster eller behov.
              </p>
            </div>
            <digi-table af-size="medium" af-variation="primary">
              <table>
                <caption>
                  Grundläggande brytpunkter för Arbetsförmedlingens webbar
                </caption>
                <thead>
                  <tr>
                    <th scope="col">Brytpunkt</th>
                    <th scope="col">Rityta</th>
                    <th scope="col">Yttermarginal</th>
                    <th scope="col">Kolumner</th>
                    <th scope="col">Gutter</th>
                    <th scope="col">Sass-variabel</th>
                    <th scope="col">
                      <span lang="en">Variable mode</span> i Figma
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">{`≥ 1400px`}</th>
                    <td>1320px</td>
                    <td>40px</td>
                    <td>12</td>
                    <td>24 px (1.5 rem)</td>
                    <td>$digi--breakpoint--xx-large</td>
                    <td>
                      <span lang="en">Desktop</span>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">{`≥ 1200px`}</th>
                    <td>1140px</td>
                    <td>30px</td>
                    <td>12</td>
                    <td>24 px (1.5 rem)</td>
                    <td>$digi--breakpoint--x-large</td>
                    <td>
                      <span lang="en">Desktop</span>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">{`≥ 992px`}</th>
                    <td>960px</td>
                    <td>16px</td>
                    <td>12</td>
                    <td>24 px (1.5 rem)</td>
                    <td>$digi--breakpoint--large</td>
                    <td>
                      <span lang="en">Tablet</span>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">{`≥ 768px`}</th>
                    <td>720px</td>
                    <td>24px</td>
                    <td>12</td>
                    <td>24 px (1.5 rem)</td>
                    <td>$digi--breakpoint--medium</td>
                    <td>
                      <span lang="en">Tablet</span>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">{`≥ 576px`}</th>
                    <td>540px</td>
                    <td>18px</td>
                    <td>6</td>
                    <td>24 px (1.5 rem)</td>
                    <td>$digi--breakpoint--small</td>
                    <td>
                      <span lang="en">Mobile</span>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">{`≤ 575px`}</th>
                    <td>Auto</td>
                    <td>16px</td>
                    <td>3</td>
                    <td>24 px (1.5 rem)</td>
                    <td>$digi--breakpoint--small-below</td>
                    <td>
                      <span lang="en">Mobile</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </digi-table>
          </digi-layout-block>

          <digi-layout-block afVerticalPadding>
            <digi-layout-columns af-variation="two">
              <div>
                <h3>Designunderlag</h3>
                <p>
                  Vi gör först och främst designunderlag för desktop-bredden
                  1200 px (med rityta 1140 px) och mobilskärmar (375 px). Stötta
                  även utvecklaren med tablet-design om resurser finnes.
                  Specifika tjänster kan ha användardata som visar på andra
                  skärmbredder, var agil och stötta eventuella specialfall.
                </p>
                <digi-media-image
                  afUnlazy
                  afHeight="289"
                  afWidth="500"
                  afSrc="/assets/images/pattern/Designunderlag.svg"
                  afAlt="..."></digi-media-image>
              </div>

              <div>
                <h3>Statistik</h3>
                <p></p>
                <digi-link-external
                  afHref="https://gs.statcounter.com/os-market-share/mobile/sweden/#monthly-202105-202205"
                  af-target="_blank"
                  af-variation="small">
                  Läs på om skärmstorlekar och andel användare på svenska
                  marknaden (gs.statcounter.com, öppnas i egen flik)
                </digi-link-external>
              </div>
            </digi-layout-columns>
          </digi-layout-block>

          <digi-layout-block af-variation="secondary" afVerticalPadding>
            <digi-layout-columns af-variation="two">
              <div>
                <h4>Formulär</h4>
                <p>Se relaterade designmönster och komponenter</p>
                <digi-docs-related-links>
                  <digi-link-button
                    afHref="/designmonster/formular"
                    af-target="_blank"
                    af-size="medium"
                    af-variation="secondary"
                    onAfOnClick={(e) => this.linkClickHandler(e)}>
                    Formulär
                  </digi-link-button>
                </digi-docs-related-links>
              </div>
              <div>
                <h4>Spacing</h4>
                <p>Se relaterade designmönster och komponenter</p>
                <digi-docs-related-links>
                  <digi-link-button
                    afHref="/designmonster/spacing"
                    af-target="_blank"
                    af-size="medium"
                    af-variation="secondary"
                    onAfOnClick={(e) => this.linkClickHandler(e)}>
                    Spacing
                  </digi-link-button>
                </digi-docs-related-links>
              </div>
            </digi-layout-columns>
          </digi-layout-block>
        </digi-docs-page-layout>
      </host>
    );
  }
}
