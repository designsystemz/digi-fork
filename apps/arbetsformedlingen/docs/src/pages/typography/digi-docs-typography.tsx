import { Component, h, getAssetPath, State } from '@stencil/core';
import state from '../../store/store';

@Component({
  tag: 'digi-docs-typography',
  styleUrl: 'digi-docs-typography.scss',
  scoped: true
})
export class DigiDocsTypography {
  @State() pageName = 'Typografi';

  render() {
    return (
      <div class="digi-docs-about-typography">
        <digi-typography>
          <digi-docs-page-layout
            af-edit-href="pages/digi-docs-about-typography/digi-docs-about-typography.tsx"
            af-page-heading={this.pageName}>
            {/* <digi-layout-block af-no-gutter afMarginBottom> */}
            {/* <digi-typography-heading-jumbo af-text="Typografi"></digi-typography-heading-jumbo> */}
            <span slot="preamble">
              Open Sans är Arbetsförmedlingens identitetsbärande
              typsnittsfamilj. Den kompletteras av Arial och Georgia.
              Typsnittsfamiljerna är valda för att göra våra texter lättlästa
              och tillgängliga.
            </span>
            <digi-layout-block
              af-variation="secondary"
              af-no-gutter
              afVerticalPadding
              afMarginTop>
              <digi-layout-columns
                af-variation={state.responsive321Columns}
                class="digi-docs-typography__columns">
                <div class="digi-docs-typography__block">
                  <digi-media-image
                    afUnlazy
                    afHeight="275"
                    afWidth="455"
                    afSrc={getAssetPath('/assets/images/typography/webben.svg')}
                    afAlt="Illustration med text, på webben använder vi Open Sans"></digi-media-image>
                  <h2>Webb och digitala tjänster</h2>
                  <p>
                    På webben och för alla egenproducerade digitala tjänster
                    använder vi enbart Open Sans. Det gäller rubriker, ingress
                    och brödtext, samt komponenter och övriga funktioner där det
                    finns text.
                  </p>
                  <p>Vi använder tre olika teckenstorlekar</p>
                  <digi-list>
                    <li>
                      <span lang="en">“Small”</span> är standard. Den passar bra
                      för tjänster där det finns mycket komponenter, till
                      exempel för formulär och interna verktyg med många
                      interaktionsmöjligheter.
                    </li>
                    <li>
                      <span lang="en">“Large”</span> används framför allt för
                      Arbetsförmedlingens huvudwebbplats. Du kan även använda
                      large för landningssidor och artikelsidor, om det finns
                      gott om utrymme.
                    </li>
                    <li>
                      <span lang="en">“Mobile”</span> är anpassad för mobila
                      enheter med mindre skärmar.
                    </li>
                  </digi-list>
                </div>
                <div class="digi-docs-typography__block">
                  <digi-media-image
                    afUnlazy
                    afHeight="275"
                    afWidth="455"
                    afSrc={getAssetPath('/assets/images/typography/epost.svg')}
                    afAlt="Illustration med text, i e-post använder vi Arial"></digi-media-image>
                  <h2>E-post och externa system </h2>
                  <p>
                    Till e-post använder vi Arial. Typsnittet är standard i
                    många operativsystem och kan därför visas av de flesta
                    e-postprogram. För externa system och tjänster som
                    Arbetsförmedlingen köpt in eller nyttjar använder vi Open
                    Sans om det är möjligt. I annat fall Arial.
                  </p>
                  <h3>Microsoft Office</h3>
                  <p>
                    I Office-mallar använder vi Arial som vårt huvudtypsnitt.
                    Det överensstämmer bäst med vårt arbete i myndigheten. I
                    löpande brödtext använder vi dock Georgia.
                  </p>
                </div>
                <div class="digi-docs-typography__block">
                  <digi-media-image
                    afUnlazy
                    afHeight="275"
                    afWidth="455"
                    afSrc={getAssetPath(
                      '/assets/images/typography/trycksak.svg'
                    )}
                    afAlt="Illustration med text, rubriker använder Open Sans och Georgia används till brödtext i office-mallar och trycksaker"></digi-media-image>
                  <h2>Trycksaker</h2>
                  <p>
                    I tryckt material använder vi Open Sans till rubriker och
                    ingresser, men Georgia till brödtext. Undantag är tryckt
                    eventmaterial där vi använder Open Sans även till
                    brödtexten.
                  </p>
                </div>
              </digi-layout-columns>
              <digi-link-button
                afHref="/komponenter/digi-typography/oversikt"
                af-size="medium"
                af-variation="secondary">
                Här hittar du typografikomponenten
              </digi-link-button>
              <br />
            </digi-layout-block>
            <digi-layout-block af-variation="primary" af-no-gutter afMarginTop>
              <h2>Om våra typsnitt och textvarianter</h2>
              <p>
                Open Sans är vårt identitetsbärande typsnitt. Det kompletteras
                av Arial och Georgia. För att göra texten lättare att ta till
                sig använder vi text-varianter såsom rubriker, ingress och
                brödtext. Vi undviker <span lang="en">light</span>
                (tunn) och <span lang="en">italic</span> (kursiv) i brödtext. Vi
                skriver inte heller rubriker eller längre stycken med versaler
                (stora bokstäver).
              </p>
              <p>Nedan kan du läsa mer om de olika typsnitten.</p>
              <digi-layout-columns
                af-variation={state.responsive321Columns}
                class="digi-docs-typography__columns">
                <div class="digi-docs-typography__columns-examples">
                  <digi-layout-block af-variation="symbol" af-vertical-padding>
                    <h3>Open Sans</h3>
                    <digi-typography-preamble>
                      Så här ser en ingress ut i typsnittsfamiljen Open Sans.
                      Det är ett användarvänligt och mångsidigt typsnitt.{' '}
                    </digi-typography-preamble>
                    <p>
                      En brödtext i Open Sans är lätt att läsa. Fonten tillhör
                      typsnittsgruppen sans-serif som saknar klackar (serifer) i
                      bokstävernas ändar. Det anses ge ökad tillgänglighet i
                      framför allt digital text.
                    </p>
                    <p>
                      Open Sans är öppen källkod. Fonten har ett omfattande
                      språkstöd och kan presentera text på många olika språk.
                    </p>
                    <p>Open Sans designades år 2011 av Steve Matteson. </p>
                  </digi-layout-block>
                </div>
                <div class="digi-docs-typography__columns-examples-two">
                  <digi-layout-block af-variation="symbol" af-vertical-padding>
                    <h3>Arial</h3>
                    <digi-typography-preamble>
                      Här ser du en ingress i Arial. Det är ett tidlöst och
                      vanligt typsnitt.{' '}
                    </digi-typography-preamble>
                    <p>
                      En brödtext i Arial är tydlig. Arial är standard i många
                      operativsystem och program för textredigering. Precis som
                      Open Sans tillhör Arial typsnittsgruppen sans-serif utan
                      klackar.
                    </p>
                    <p>
                      Arial designades år 1982 av Robin Nicholas och Patricia
                      Saunders.
                    </p>
                  </digi-layout-block>
                </div>
                <div class="digi-docs-typography__columns-examples-three">
                  <digi-layout-block af-variation="symbol" af-vertical-padding>
                    <h3>(Georgia som rubrik)</h3>
                    <digi-typography-preamble>
                      (Som regel använder vi inte Georgia som ingress eller
                      rubrik.)
                    </digi-typography-preamble>
                    <p>
                      En tryckt brödtext i Georgia är lättläst. Det är ett
                      serif-typsnitt med klackar på bokstäverna. Klackarna
                      skapar en tydlig linje att följa med blicken, vilket
                      underlättar läsning av längre text i liten storlek. Vi
                      använder Georgia för tryckt material.
                    </p>
                    <p>Georgia designades år 1993 av Matthew Carter.</p>
                  </digi-layout-block>
                </div>
              </digi-layout-columns>
            </digi-layout-block>
          </digi-docs-page-layout>
        </digi-typography>
      </div>
    );
  }
}
