import { Component, h, State, getAssetPath } from '@stencil/core';
import { router } from '../../global/router';

@Component({
  tag: 'digi-docs-collaboration',
  styleUrl: 'digi-docs-collaboration.scss',
  scoped: true
})
export class DigiDocsCollaboration {
  @State() pageName = 'Samarbeten';
  Router = router;
  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/digi-docs-collaboration/digi-docs-collaboration.tsx">
          <span slot="preamble">
            Vid samarbeten med andra aktörer är det viktigt att klargöra vilken
            roll Arbetsförmedlingen har i samarbetet.
          </span>
          <br />
          <digi-layout-container af-margin-bottom>
            <p>
              Arbetsförmedlingen samarbetar på flera olika sätt. Det kan till
              exempel vara att Arbetsförmedlingen är huvudansvarig eller att
              alla samarbetspartner är likvärdiga. Det är viktigt att följa vår
              grafiska profil och använda vår logotyp i alla former av
              samarbeten. Ibland kan det vara önskvärt att använda logotypen i
              en monokrom variant. Tex. med endast en färg. Det är ett avsteg
              som ska godkännas av kommunikationsavdelningen på
              Arbetsförmedlingen. Det kan också finnas tillfällen där ingen av
              de medverkandes logotyp används.
              <br />
              <br />
              <digi-link
                af-variation="small"
                afHref={`mailto:kommunikationsavdelningen@arbetsformedlingen.se`}
                class="digi-docs__contact-link">
                <digi-icon-envelope slot="icon"></digi-icon-envelope>
                Kontakta kommunikationsavdelningen
              </digi-link>
              <br />
              <br />
            </p>
          </digi-layout-container>
          <digi-layout-container>
            <h3>Externa leverantörer och upphandlade tjänster</h3>
            <p>
              Arbetsförmedlingens logotyp får inte användas av den externa
              leverantören i den egna kommunikationen. Texten ”På uppdrag av
              Arbetsförmedlingen” skall användas i all kommunikation för att
              tydliggöra samarbetet. I stället ska texten ”På uppdrag av
              Arbetsförmedlingen” användas i all kommunikation för att
              tydliggöra samarbetet. Detta får endast göras under
              avtalsperioden.
            </p>
            <br />
            <h3>Exempel där Arbetsförmedligen är huvudansvarig</h3>
            <p>
              Arbetsförmedlingens logotyp är överordnad de andra logotyperna
            </p>
            <div class="digi-docs__graphics-collaboration-images">
              <digi-media-image
                afUnlazy
                class="digi-docs__graphics-logotype-image"
                afSrc={getAssetPath(
                  '/assets/images/collaboration/samarbeten1.png'
                )}
                afAlt="Exempel på där Arbetsförmedlingens logotyp är överordnad de andra logotyperna"></digi-media-image>
            </div>
            <br />
            <br />
            <h3>Exempel där Arbetsförmedligens roll är likvärdig med andra</h3>
            <p>Logotyperna eller aktörernas namn är på samma nivå.</p>
            <div class="digi-docs__graphics-collaboration-images">
              <digi-media-image
                afUnlazy
                afSrc={getAssetPath(
                  '/assets/images/collaboration/samarbeten2.png'
                )}
                afAlt="Exempel där logotyperna eller aktörernas namn är på samma nivå"></digi-media-image>
            </div>
            <br />
            <br />
            <h3>Exempel på likvärdigt samarbete med undantag</h3>
            <p>
              Endast när de andra sammarbetslogotyperna är helt vita är det
              tillåtet att låta Arbetsförmedlingens logotyp vara vit. Samma sak
              gäller också om alla logotyper skulle vara enfärgade i en mörk
              färg mot ljus bakgrund. Då skall alla ha samma färg.
            </p>
            <div class="digi-docs__graphics-collaboration-images">
              <digi-media-image
                afUnlazy
                afSrc={getAssetPath(
                  '/assets/images/collaboration/samarbeten3.png'
                )}
                afAlt="Exempel på där Arbetsförmedlingens logotyp är vit på en mörk bakgrund"></digi-media-image>
            </div>
            <div class="digi-docs__graphics-collaboration-images">
              <digi-media-image
                afUnlazy
                afSrc={getAssetPath(
                  '/assets/images/collaboration/samarbeten4.png'
                )}
                afAlt="Exempel på där Arbetsförmedlingens logotyp är överordnad de andra logotyperna"></digi-media-image>
            </div>
            <br />

            <h3>Neutralt formspråk</h3>
            <p>
              Exempelvis ett projekt mellan organisationer och myndigheter. Som
              har ett neutralt formspråk men ett gemensamt mål.
            </p>
            <div class="digi-docs__graphics-collaboration-images">
              <digi-media-image
                afUnlazy
                afSrc={getAssetPath(
                  '/assets/images/collaboration/samarbeten5.png'
                )}
                afAlt="Exempel på ett projekt mellan organisationer och myndigheter"></digi-media-image>
            </div>
            <br />
            <br />
          </digi-layout-container>
        </digi-docs-page-layout>
      </host>
    );
  }
}
