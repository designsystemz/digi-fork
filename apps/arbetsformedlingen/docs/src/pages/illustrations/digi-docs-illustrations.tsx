import { Component, getAssetPath, h, State } from '@stencil/core';
import { router } from '../../global/router';
import state from '../../store/store';

@Component({
  tag: 'digi-docs-illustrations',
  styleUrl: 'digi-docs-illustrations.scss',
  scoped: true
})
export class DigiDocsIllustrations {
  Router = router;
  @State() pageName = 'Illustrationer';

  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <div class="digi-docs-illustrations">
        <digi-docs-page-layout af-edit-href="pages/digi-docs-logotype/digi-docs-illustrations.tsx">
          <digi-typography>
            <digi-layout-block>
              <digi-typography-heading-jumbo af-text="Illustrationer"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                Ibland vill vi förstärka en känsla eller beskriva en händelse.
                De gånger det inte är möjligt eller passar att använda foton så
                använder vi våra egna illustrationer. Det är lika viktigt här
                precis som för vårt bildspråk att de representerar ett tvärsnitt
                av befolkningen och kommunicerar verkligheten utan klichéer och
                inte speglar kön eller klasstillhörighet.
              </digi-typography-preamble>
              <p>
                Vi använder oss av ett eget manér där illustrationerna kan
                användas på fler olika sätt. Antingen i sin ursprungsform med
                endast de tecknade linjerna eller ihop med “färgklickar” som
                signalerar eller harmoniserar med övrigt innehåll i dess
                kontext. För “färgklickarna” använder vi våra profilfärger samt
                komplementfärger i dess original eller ljusare variant. Vi är
                sparsamma hur vi använder dessa illustrationer och de är
                framtagna för arbetsformedlingen.se och tryck material/formgivna
                PDF:er.
              </p>
              <h2>Illustrationer som beskriver en händelse</h2>
              <p>
                Används för att förstärka texten rent visuellt. Exempel kan vara
                en text om hur du deltar i Arbetsförmedlingens webinarier.
              </p>
              <div class="digi-docs__webinar-image">
                <digi-media-image
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/people-in-webinar-illustrations.png'
                  )}
                  afAlt="Illustrationer på hur man kan deltar i Arbetsförmedlingens webinarium"></digi-media-image>
              </div>

              <h2>Illustrationer som beskriver en känsla</h2>
              <p>
                Används för att inspirera och komplettera innehåll. Med
                människan i centrum som även kan omges av intryck och verktyg
                som symboliserar en helhet.
              </p>
              <div class="digi-docs__people-image">
                <digi-media-image
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/people-illustrations.png'
                  )}
                  afAlt="Illustrationer på människor med olika intryck"></digi-media-image>
              </div>
              <br />
              <br />
              <hr></hr>
              <br />
              <digi-layout-columns af-variation={state.responsiveTwoColumns}>
                <div>
                  <h4>Bilder</h4>
                  <p>Se relaterad sida inom grafiska profilen</p>
                  <digi-docs-related-links>
                    <digi-link-button
                      afHref="/grafisk-profil/bilder"
                      af-target="_blank"
                      af-size="medium"
                      af-variation="secondary"
                      onAfOnClick={(e) => this.linkClickHandler(e)}>
                      Bilder
                    </digi-link-button>
                  </digi-docs-related-links>
                  {''}
                  <br />
                </div>
                <div>
                  <h4>Animationer</h4>
                  <p>Se relaterad sida inom grafiska profilen</p>
                  <digi-docs-related-links>
                    <digi-link-button
                      afHref="/grafisk-profil/animationer"
                      af-target="_blank"
                      af-size="medium"
                      af-variation="secondary"
                      onAfOnClick={(e) => this.linkClickHandler(e)}>
                      Animationer
                    </digi-link-button>
                  </digi-docs-related-links>
                </div>
              </digi-layout-columns>
            </digi-layout-block>
            <br />
          </digi-typography>
        </digi-docs-page-layout>
      </div>
    );
  }
}
