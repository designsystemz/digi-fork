import { Component, h } from '@stencil/core';

@Component({
  tag: 'digi-docs-about-language',
  styleUrl: 'digi-docs-about-language.scss',
  scoped: true
})
export class DigiDocsAboutLanguage {
  render() {
    return (
      <div class="digi-docs--about-language">
        <digi-docs-page-layout af-edit-href="pages/digi-docs--about-language/digi-docs--about-language.tsx">
          <digi-typography>
            <digi-layout-block>
              <digi-typography-heading-jumbo af-text="Klarspråk"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                Klarspråkslagen innebär att vi som myndighet är skyldiga att
                skriva begripliga texter ur mottagarens perspektiv. Det vi
                skriver och kommunicerar ska gå att förstå och hjälpa mottagaren
                att använda våra tjänster och utföra sitt ärende på sina
                villkor. I punktlistan här nedanför hittar du konkreta tips för
                hur du kan få dina texter att bli tydliga, effektiva och enkla
                att ta till sig.
              </digi-typography-preamble>
              <digi-list>
                <li>Tänk på läsaren.</li>
                <li>Bestäm syftet med texten.</li>
                <li>Välj ett relevant innehåll.</li>
                <li>Börja med det viktigaste.</li>
                <li>Skriv enkla meningar.</li>
                <li>Välj ord som läsaren förstår.</li>
                <li>Följ skrivreglerna.</li>
              </digi-list>
              <p>
                <br />
                Exempel på hur du kan strukturera din text:
              </p>
              <digi-list>
                <li>Börja med det viktigaste.</li>
                <li>Gör en kort sammanfattning i början av texten.</li>
                <li>Låt varje stycke bestå av endast ett ämne.</li>
                <li>Använd gärna punktlistor, faktarutor och diagram.</li>
              </digi-list>
              <br />
              <p>
                Du kan skriva tydliga och informativa rubriker på flera sätt.
                Här är några olika exempel:
              </p>
              <digi-list>
                <li>Tydliga rapporter lockar till läsning (påstående).</li>
                <li>Skriv tydliga rapporter (uppmaning).</li>
                <li>Skriva tydliga rapporter (infinitiv-fras).</li>
                <li>Hur skriver jag tydliga rapporter? (fråga).</li>
              </digi-list>
              <br />
            </digi-layout-block>
          </digi-typography>
        </digi-docs-page-layout>
      </div>
    );
  }
}
