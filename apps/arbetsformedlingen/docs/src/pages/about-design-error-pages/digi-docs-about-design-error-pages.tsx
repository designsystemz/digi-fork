import { Component, getAssetPath, h, Prop } from '@stencil/core';
import Helmet from '@stencil/helmet';
import { router } from '../../global/router';
import { href } from 'stencil-router-v2';
import state from '../../store/store';

@Component({
  tag: 'digi-docs-about-design-error-pages',
  styleUrl: 'digi-docs-about-design-error-pages.scss',
  scoped: true
})
export class AboutDesignErrorPages {
  @Prop() pageName = 'Felmeddelandesidor';
  Router = router;
  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-design-error-pages/digi-docs-about-design-error-pages.tsx">
          <Helmet>
            <meta
              property="rek:pubdate"
              content="Wed May 24 2023 11:44:56 GMT+0200 (centraleuropeisk sommartid)"
            />
          </Helmet>

          <span slot="preamble">
            Vi strävar efter att erbjuda många olika felmeddelandesidor, för att
            hjälpa våra användare med riktad information om vad som har
            inträffat och visa på specifika vägar vidare beroende på det
            enskilda felet. Detta gör vi för att vi vill hålla en så hög
            servicenivå som möjligt till våra besökare i en situation då vi av
            många olika anledningar inte kan visa rätt sida. <br /> <br />
          </span>

          <digi-layout-block
            af-variation="secondary"
            af-vertical-padding
            af-margin-bottom>
            <h2>Felmeddelandesidor</h2>
            <p>
              Felmeddelandesidor är en viktig del av vår webbplats, vilket
              betyder att de behöver vara så optimerade som möjligt för att
              bidra till ökad konvertering och minskning av ”studsfrekvens”. Vi
              önskar att säkerställa att sidan erbjuder användaren ett eller
              fler sätt att gå vidare ifrån. Därefter vill vi ta hand om
              användaren på bästa sätt.
            </p>
            <p>
              Vi behöver också, som ett led i detta, samla på oss data kring
              vilket fel som uppstått. I längden kan vi därmed minska antalet
              visningar av dessa felsidor. Dessa mätningar blir indirekt ett
              verktyg för kontroll och samtidigt får vi en hälsoindikator på
              våra webbplatser.
            </p>
            <h2>Mätning</h2>
            <p>
              Ha koll på dina felsidor! Se till att mäta dessa sidor från
              början. Mätningarna kommer att ge information om hur väl din
              webbplats presterar och hur eventuella åtgärder resulterar i
              förbättrade siffror.
            </p>
            <p>
              Det kommer även att ge indikation på hur åtgärderna relaterar till
              andra produkter och andra beroenden. Dessa mätningar borde även
              kunna hjälpa till att identifiera eventuellt okända
              användarbeteenden kopplade till enskilda KPI:er.
            </p>
            <h3>Några exempel på generella KPI:er</h3>
            <digi-list af-list-type="bullet">
              <li>Mät var användaren kommer ifrån.</li>
              <li>Mät vart användaren försökte ta sig.</li>
              <li>
                Mät så specifika fel (-koder) som möjligt, undvik generella fel
                då de ger sämre data.
              </li>
            </digi-list>
          </digi-layout-block>

          <digi-layout-block
            af-variation="primary"
            af-vertical-padding
            af-margin-bottom>
            <h2>Anatomi och byggstenar</h2>
            <digi-layout-columns
              class="digi-docs-about-design-error-pages__columns"
              af-element="div"
              af-variation={state.responsiveTwoColumns}>
              <div>
                <p>
                  Se befintliga felmeddelandesidor i{' '}
                  <a href="#error-examples-table">tabellen</a> nedan. I händelse
                  av att du behöver samma felsidor optimerade för din enskilda
                  tjänst finns denna verktyglåda att ta till, då{' '}
                  <a
                    {...href(
                      '/komponenter/digi-notification-error-page/oversikt'
                    )}>
                    komponenten
                  </a>{' '}
                  för felsidor är uppbyggd med ett antal tänkta{' '}
                  <span lang="en">features</span>.
                </p>
                <ol class="digi-docs-about-design-error-pages__list">
                  <li>Rubrikyta, med fördefinierad texter</li>
                  <li>Brödtextyta, med fördefinierad texter</li>
                  <li>
                    Sökruta (länkar sökningen till den aktuella tjänstens sök)
                  </li>
                  <li>
                    Yta som erbjuder vägar vidare (max tre länkar per felsida).
                    Några exempel på länkar som vi föreslår: <br />
                    - Länk till startsida, (bör läggas som den sista länken)
                    <br />
                    - Länk till tjänstens startläge
                    <br />
                    - Länk till underliggande felrapporteringsformulär
                    <br />
                    - Länk till att kunna dela med sig av t.ex tidpunkt,
                    webbläsarversion, med mera
                    <br />
                    - Att om tjänsten har möjlighet; kunna kontakta en
                    administratör eller funktionsbrevlåda
                    <br />- En hjälplänk för den ovane webbandvändaren, som
                    laddar om sidan
                  </li>
                  <li>Bildyta, med fördefinierad illustration</li>
                </ol>
              </div>

              <div>
                <digi-media-image
                  class="digi-docs-about-design-error-pages__media"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/pattern/error-pages/error-numbers-list.png'
                  )}
                  afAlt=""></digi-media-image>
              </div>
            </digi-layout-columns>
          </digi-layout-block>

          <digi-layout-block af-variation="secondary" af-vertical-padding>
            <h2>Din tjänsts egna felmeddelandesidor</h2>
            <p>
              Observera att användaren av komponenten måste sätta upp både
              funktionalitet och mätningar själva. Använd felsideskomponenten
              för att skapa egna versioner till din tjänst, samt att för att
              skräddarsy dessa efter era behov och er unika användarkunskap.
              Använd max tre vägar vidare, utöver sökmöjlighet för att inte göra
              sidan för komplex för användaren att avkoda, och därigenom öka
              studsfrekvens.
            </p>

            <h2>En helt ny felmeddelandesida?</h2>
            <p>
              Behöver du en sida med felkod som inte finns med här? Hör av dig
              till Designsystemet så hjälper vi till med grafik och
              tillgängliggör sidan som komponent för fler team. Mejla
              funktionsbrevlådan{' '}
              <a href="mailto:designsystem@arbetsformedlingen.se">
                designsystem@arbetsformedlingen.se
              </a>{' '}
            </p>

            <h2>Språk och anslag</h2>
            <p>
              Vi strävar efter att använda ett språk och en ton som är
              lättförståelig och tillgänglig för alla användare. Teknisk jargong
              som <span lang="en">”404 - Not found”</span>
              eller <span lang="en">”500 - Internal server error”</span> ska
              undvikas, istället beskriver vi problemen med vanliga ord. Vi
              önskar erbjuda högupplöst information om problemen och hjälpa
              användarna att förstå vad som har hänt.
            </p>
            <p>
              Vi förklarar problemen tydligt och erbjuder relevanta vägar vidare
              för att hålla användarna engagerade, även när de upplever problem
              på vår plattform. Genom att använda ett tillgängligt språk kan vi
              hjälpa våra användare att känna sig trygga och ha en positiv
              upplevelse av vår tjänst.
            </p>
          </digi-layout-block>

          <digi-layout-block af-variation="secondary" af-vertical-padding>
            <h2 id="error-examples-table">
              Översikt av befintliga felmeddelandesidor
            </h2>
            <hr />
            <section class="digi-docs-about-design-error-pages__grid">
              <div class="digi-docs-about-design-error-pages__grid-300">
                <h3>
                  <span lang="en">301 Moved permanently</span>
                </h3>
                <p>
                  Vid statuskod 301 Moved permanently kommer användaren direkt
                  omdirigeras till en ny URL och kommer därför inte att hinna
                  hamna på en felsida.
                </p>
              </div>

              <div class="digi-docs-about-design-error-pages__grid-400">
                <h3>
                  <span lang="en">401 Unauthorized</span>
                </h3>
                <digi-media-image
                  class="digi-docs-home-link-blocks__media"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/pattern/error-pages/unauthorized.png'
                  )}
                  afAlt="Ett låst hänglås med sur mun och ett olåst hänglås med glad mun"></digi-media-image>
                <span>
                  När användaren behöver autentisera sig för att få åtkomst till
                  sidan.
                </span>
              </div>

              <div class="digi-docs-about-design-error-pages__grid-400">
                <h3>
                  <span lang="en">403 Forbidden</span>
                </h3>
                <digi-media-image
                  class="digi-docs-home-link-blocks__media"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/pattern/error-pages/forbidden.png'
                  )}
                  afAlt="Nyckelknippa med tre nycklar och en etikett"></digi-media-image>
                <span>
                  När användaren inte har behörighet till att se sidan.
                </span>
              </div>
              <div class="digi-docs-about-design-error-pages__grid-400">
                <h3>
                  <span lang="en">404 Not found</span>
                </h3>
                <digi-media-image
                  class="digi-docs-home-link-blocks__media"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/pattern/error-pages/notfound.png'
                  )}
                  afAlt="En person som tittar i en kikare"></digi-media-image>
                <span>När URL:en är felaktig eller sidan inte finns kvar.</span>
              </div>
              <div class="digi-docs-about-design-error-pages__grid-400">
                <h3>
                  <span lang="en">410 Gone permanently</span>
                </h3>
                <digi-media-image
                  class="digi-docs-home-link-blocks__media"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/pattern/error-pages/gone.png'
                  )}
                  afAlt="Bil som blir bärgad av en bärgningsbil"></digi-media-image>
                <span>
                  När sidan är permanent borttagen och det inte finns en ny URL
                  att omdirigeras till.
                </span>
              </div>

              <div class="digi-docs-about-design-error-pages__grid-500 digi-docs-about-design-error-pages__grid-500--1">
                <h3>
                  <span lang="en">500 Internal error</span>
                </h3>
                <digi-media-image
                  class="digi-docs-home-link-blocks__media"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/pattern/error-pages/servererror.png'
                  )}
                  afAlt="Två sladdar som ligger okopplade"></digi-media-image>
                <span>
                  När det är ett generellt anropsproblem och det inte finns
                  någon mer lämplig 5xx-statuskod.
                </span>
              </div>
              <div class="digi-docs-about-design-error-pages__grid-500 digi-docs-about-design-error-pages__grid-500--2">
                <h3>
                  <span lang="en">504 Timeout</span>
                </h3>
                <digi-media-image
                  class="digi-docs-home-link-blocks__media"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/pattern/error-pages/timeout.png'
                  )}
                  afAlt="Två sladdar som ligger okopplade och ett tidtagarur"></digi-media-image>
                <span>
                  När servern agerar som en proxy och inte fått respons i tid
                  från uppströmsservern.
                </span>
              </div>
            </section>
          </digi-layout-block>

          <digi-layout-block af-variation="secondary" afVerticalPadding>
            <hr></hr>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <h4>Formulär</h4>
                <p>Se relaterade designmönster och komponenter</p>
                <digi-docs-related-links>
                  <digi-link-button
                    afHref="/designmonster/formular"
                    af-target="_blank"
                    af-size="medium"
                    af-variation="secondary"
                    onAfOnClick={(e) => this.linkClickHandler(e)}>
                    Formulär
                  </digi-link-button>
                </digi-docs-related-links>
              </div>
              <div>
                <h4>Spacing</h4>
                <p>Se relaterade designmönster och komponenter</p>
                <digi-docs-related-links>
                  <digi-link-button
                    afHref="/designmonster/spacing"
                    af-target="_blank"
                    af-size="medium"
                    af-variation="secondary"
                    onAfOnClick={(e) => this.linkClickHandler(e)}>
                    Spacing
                  </digi-link-button>
                </digi-docs-related-links>
              </div>
            </digi-layout-columns>
          </digi-layout-block>
        </digi-docs-page-layout>
      </host>
    );
  }
}
