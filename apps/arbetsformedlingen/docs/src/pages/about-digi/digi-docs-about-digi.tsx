import { Component, h } from '@stencil/core';

@Component({
  tag: 'digi-docs-about-digi',
  styleUrl: 'digi-docs-about-digi.scss'
})
export class DigiDocsAboutDigi {
  render() {
    return (
      <div class="digi-docs-about-digi">
        <digi-docs-page-layout af-edit-href="pages/about-digi/digi-docs-about-digi/digi-docs-about-digi.tsx">
          <digi-layout-block></digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
