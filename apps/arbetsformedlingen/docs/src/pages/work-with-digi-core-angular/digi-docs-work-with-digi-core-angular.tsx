import { Component, h, State } from '@stencil/core';

import {
  CodeLanguage,
  CodeBlockLanguage,
  CodeVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-work-with-digi-core-angular',
  styleUrl: 'digi-docs-work-with-digi-core-angular.scss',
  scoped: true
})
export class DigiDocsWorkWithDigiCoreAngular {
  @State() pageName = 'Digi Core Angular';

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/digi-docs-work-with-digi-core-angular/digi-docs-work-with-digi-core-angular.tsx">
          <digi-layout-container af-margin-top af-margin-bottom>
            <article>
              <h2>Installation</h2>
              <p>
                Skapa filen{' '}
                <digi-code afVariation={CodeVariation.LIGHT} afCode=".npmrc" />{' '}
                i roten av ditt projekt och lägg till följande rad
              </p>
              <digi-code-block
                afCode={`@digi:registry=https://nexus.jobtechdev.se/repository/arbetsformedlingen-npm/
								`}
              />
              <br />
              <p>Kör följande kommando i roten av ditt projekt</p>
              <digi-code-block
                afLang={CodeLanguage.BASH}
                afCode={`npm i @digi/arbetsformedlingen @digi/arbetsformedlingen-angular
							`}
              />
              <br />
              <p>
                Lägg till följande rader i <digi-code afCode="styles.css" />{' '}
                eller liknande
              </p>
              <digi-code-block
                afLanguage={CodeBlockLanguage.CSS}
                afCode={`@import '@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css';
@import '@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/fonts.css';\n`}
              />
              <br />
              <p>
                Lägg till raderna märkta med <digi-code afCode="// digi" /> på
                motsvarande ställe i ditt projekt
              </p>
              <digi-tablist
                afTabs={[
                  { id: 'angular-component', title: 'Version 17+' },
                  { id: 'angular-module', title: 'Äldre versioner' }
                ]}>
                <digi-tablist-panel tab="angular-component">
                  <digi-code-block
                    afLanguage={CodeBlockLanguage.TYPESCRIPT}
                    afCode={`/* app.component.ts */
import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

// digi
import { DigiArbetsformedlingenAngularModule } from '@digi/arbetsformedlingen-angular';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
		RouterOutlet, 
		DigiArbetsformedlingenAngularModule // digi
	],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {}`}></digi-code-block>
                </digi-tablist-panel>
                <digi-tablist-panel tab="angular-module">
                  <digi-code-block
                    afLanguage={CodeBlockLanguage.TYPESCRIPT}
                    afCode={`/* app.module.ts */
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MyPageComponent } from './pages/my-page/my-page.component'

// digi
import { DigiArbetsformedlingenAngularModule } from '@digi/arbetsformedlingen-angular';

@NgModule({
  declarations: [AppComponent, MyPageComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: "my-page", component: MyPageComponent}
    ], { initialNavigation: 'enabledBlocking' }),
    ReactiveFormsModule,
    DigiArbetsformedlingenAngularModule // digi
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}`}></digi-code-block>
                </digi-tablist-panel>
              </digi-tablist>
              <br />
              <p>
                Det var allt!{' '}
                <span role="img" aria-label="Emoji: Raket">
                  🚀
                </span>
              </p>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Använda Digi Core Angular</h2>
              <h3>Använda enums</h3>
              <p>
                Användande av enums sker via{' '}
                <digi-code afCode="@digi/arbetsformedlingen" /> och ser ut som
                följande
              </p>
              <digi-code-block
                afLanguage={CodeBlockLanguage.TYPESCRIPT}
                afCode="import { EnumName1, EnumName2 } from '@digi/arbetsformedlingen';"
              />
              <h3>Reactive Forms</h3>
              <p>
                Du behöver inte tänka på något särskilt när det kommer till{' '}
                <span lang="en">Reactive Forms</span> och Digi Core Angular.
                Komponenterna använder value och checked för input och via en{' '}
                <span lang="en">ValueAccessor</span> i Digi Core Angular så
                lyssnar även
                <span lang="en">Reactive Forms</span> korrekt på komponenternas
                change-event. Här följer ett exempel på ett enkelt formulär som
                använder Digi Core Angular:
              </p>
              <digi-tablist
                afTabs={[
                  { id: 'fil1', title: 'my-form.component.html' },
                  { id: 'fil2', title: 'my-form.component.ts' }
                ]}
                afAriaLabel="Formulär-exempel">
                <digi-tablist-panel tab="fil1">
                  <digi-code-block
                    afLanguage={CodeBlockLanguage.HTML}
                    afCode={`<form [formGroup]="myForm" class="my-form">
  <digi-form-input
    afId="myEmail"
    afLabel="Epost-adress"
    [afType]="FormInputType.EMAIL"
    afRequired="true"
    afAutocomplete="on"
    formControlName="myEmail"
  ></digi-form-input>

  <digi-form-input
    afId="myAge"
    afLabel="Min ålder"
    [afType]="FormInputType.NUMBER"
    afRequired="true"
    afAutocomplete="on"
    formControlName="myAge"
  ></digi-form-input>

  <digi-form-textarea
    afId="myDescription"
    afLabel="Beskrivning"
    afRequired="true"
    formControlName="myDescription"
  ></digi-form-textarea>

  <digi-form-select
    afId="myJob"
    afLabel="Yrke"
    afRequired="true"
    formControlName="myJob"
  >
    <option name="Välj yrke" value="">Välj yrke</option>
    <option name="handlaggare" value="handlaggare">Handläggare</option>
    <option name="konsult" value="konsult">Konsult</option>
  </digi-form-select>

  <digi-form-fieldset afLegend="Svara på frågan" afId="myChoice">
    <digi-form-radiobutton
      formControlName="myChoice"
      afName="myChoice"
      afLabel="Yes"
      afId="Yes"
      afValue="Yes"
    ></digi-form-radiobutton>
    <digi-form-radiobutton
      formControlName="myChoice"
      afName="myChoice"
      afLabel="Maybe"
      afId="Maybe"
      afValue="Maybe"
    ></digi-form-radiobutton>
    <digi-form-radiobutton
      formControlName="myChoice"
      afName="myChoice"
      afLabel="No"
      afId="No"
      afValue="No"
    ></digi-form-radiobutton>
  </digi-form-fieldset>

  <br />

  <digi-form-fieldset afLegend="Klicka i" afId="myBox" formArrayName="myBox">
    <digi-form-checkbox
      formControlName="0"
      afName="myBox"
      afLabel="Val 1"
      afId="val-1"
    ></digi-form-checkbox>
    <digi-form-checkbox
      formControlName="1"
      afName="myBox"
      afLabel="Val 2"
      afId="val-2"
    ></digi-form-checkbox>
    <digi-form-checkbox
      formControlName="2"
      afName="myBox"
      afLabel="Val 3"
      afId="val-3"
    ></digi-form-checkbox>
  </digi-form-fieldset>

  <br />

  <digi-button (afOnClick)="clickButtonHandler()">Skicka</digi-button>
</form>`}></digi-code-block>
                </digi-tablist-panel>
                <digi-tablist-panel tab="fil2">
                  <digi-code-block
                    afLanguage={CodeBlockLanguage.TYPESCRIPT}
                    afCode={`import { Component, HostListener, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { FormInputValidation, FormInputType } from '@digi/arbetsformedlingen';

@Component({
  selector: 'my-form',
  templateUrl: './my-form.component.html',
  styleUrls: ['./my-form.component.scss']
})
export class MyFormComponent implements OnInit {

  FormInputValidation = FormInputValidation;
  FormInputType = FormInputType;

  myForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.myForm = this.fb.group({});
  }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      myEmail: ['', [Validators.email, Validators.required]], 
      myAge: [null, [Validators.required]], 
      myDescription: ['', [Validators.required, Validators.maxLength(60)]],
      myJob: ['', Validators.required],
      myChoice: [null, Validators.required],
      myBox: new FormArray([
        new FormControl(false),
        new FormControl(false),
        new FormControl(false)
      ])
    });
  }

  clickButtonHandler() {
    console.log(this.myForm.value);
    alert('click!');
  }
}`}></digi-code-block>
                </digi-tablist-panel>
              </digi-tablist>
            </article>
          </digi-layout-container>
        </digi-docs-page-layout>
      </host>
    );
  }
}
