import { Component, h, State } from '@stencil/core';
import {
  CodeBlockLanguage,
  CodeLanguage,
  CodeVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-work-with-digi-core-react',
  styleUrl: 'digi-docs-work-with-digi-core-react.scss',
  scoped: true
})
export class WorkWithDigiCoreReact {
  @State() pageName = 'Digi Core React';

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/work-with-digi-core-react/digi-docs-work-with-digi-core-react.tsx">
          <digi-layout-container af-margin-top af-margin-bottom>
            <article>
              <h2>Installation</h2>
              <p>
                Skapa filen{' '}
                <digi-code afVariation={CodeVariation.LIGHT} afCode=".npmrc" />{' '}
                i roten av ditt projekt och lägg till följande rad
              </p>
              <digi-code-block
                afCode={`@digi:registry=https://nexus.jobtechdev.se/repository/arbetsformedlingen-npm/
								`}
              />
              <br />
              <p>Kör följande kommando i roten av ditt projekt</p>
              <digi-code-block
                afLang={CodeLanguage.BASH}
                afCode={`npm i @digi/arbetsformedlingen @digi/arbetsformedlingen-react
							`}
              />
              <br />
              <p>Lägg till följande rader i din globala css-fil</p>
              <digi-code-block
                afLanguage={CodeBlockLanguage.CSS}
                afCode={`@import '@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css';
@import '@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/fonts.css';\n`}
              />
              <br />
              <p>
                Det var allt!{' '}
                <span role="img" aria-label="Emoji: Raket">
                  🚀
                </span>
              </p>
              <h2>Användning</h2>
              <p>
                Nedan följer ett exempel på hur du kan använda{' '}
                <digi-code afCode="digi-button" /> i ditt projekt
              </p>
              <digi-code-block
                afLanguage={CodeBlockLanguage.TYPESCRIPT}
                afCode={`import { DigiButton } from '@digi/arbetsformedlingen-react';
import { 
	ButtonVariation 
} from '@digi/arbetsformedlingen'; // observera att import av enums sker via @digi/arbetsformedlingen

export function App() {
	return (
		<DigiButton 
			afVariation={ButtonVariation.PRIMARY}
			onAfOnClick={() => console.log('Hallå världen!')}
		>Skicka</DigiButton>
	);
}
export default App;
								`}></digi-code-block>
              <br />
            </article>
          </digi-layout-container>
        </digi-docs-page-layout>
      </host>
    );
  }
}
