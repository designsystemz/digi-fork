import { Component, h, Prop } from '@stencil/core';
import Helmet from '@stencil/helmet';
import state from '../../store/store';
import { router } from '../../global/router';

@Component({
  tag: 'digi-docs-cognitive-walkthrough',
  styleUrl: 'digi-docs-cognitive-walkthrough.scss',
  scoped: true
})
export class CognitiveWalkthrough {
  @Prop() pageName = 'Kognitiv genomgång';
  Router = router;
  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/cognitive-walkthrough/digi-docs-cognitive-walkthrough.tsx">
          <Helmet>
            <meta
              property="rek:pubdate"
              content="Thu Feb 15 2024 10:48:30 GMT+0100 (GMT+01:00)"
            />
          </Helmet>
          <span slot="preamble">
            Designsystemsteamet erbjuder er som arbetar i ett utvecklingsteam en
            kognitiv genomgång av era digitala tjänster. Kognitiv genomgång är
            en effektiv metod för att utvärdera användbarhet och tillgänglighet.
            Jämfört med andra metoder är den lätt, effektiv och billig att
            utföra.
          </span>
          <digi-layout-container af-margin-bottom af-margin-top>
            <article>
              <h2>Resultatet</h2>
              <p>Den kognitiva genomgången ger er:</p>

              <digi-list af-list-type="bullet">
                <li>
                  Konkret återkoppling på användbarhet, tillgänglighet och
                  design
                </li>
                <li>Förslag på hur ni kan göra er tjänst bättre</li>
                <li>
                  Hjälp att identifiera eventuella problem i
                  utvecklingsprocessens alla faser
                </li>
                <li>
                  Insikter i hur man kan bedriva arbete med tillgänglighet och
                  användbarhet på ett strukturerat sätt
                </li>
              </digi-list>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Så här går genomgången till</h2>
              <p>
                Genomgången görs med fördel tidigt i utvecklingsprocessen, med
                en prototyp eller annat designunderlag inför utveckling. Det går
                också bra att göra genomgången på en redan utvecklad tjänst i
                testmiljö, eller på en lanserad tjänst.
              </p>
            </article>
            <br />
            <digi-progress-list
              role="list"
              af-current-step="4"
              af-variation="primary"
              af-heading-level="h3"
              af-type="circle"
              af-expandable="false">
              <digi-progress-list-item
                afHeading="1. Utvecklingsteamets förberedelse"
                afAriaLabel="1. Utvecklingsteamets förberedelse">
                <digi-list af-list-type="bullet">
                  <li>
                    Gå igenom{' '}
                    <digi-link
                      afHref="/tillganglighet-och-design/tillganglighet-checklista"
                      af-href="/tillganglighet-och-design/tillganglighet-checklista"
                      afVariation="small"
                      onAfOnClick={(e) => this.linkClickHandler(e)}>
                      tillgänglighetslistan
                    </digi-link>{' '}
                    och säkerställ att tjänsten följer de riktlinjer som finns.
                  </li>
                  <li>
                    Skicka en kort beskrivning om tjänsten och en länk till
                    prototypen eller testmiljön till Designsystemets{' '}
                    <digi-link
                      afHref="mailto:designsystem@arbetsformedlingen.se"
                      af-href="mailto:designsystem@arbetsformedlingen.se"
                      af-variation="small">
                      mejllåda
                    </digi-link>
                    .
                  </li>
                  <li>
                    Boka en tid för en genomgång, på plats/hybrid/distans.
                  </li>
                </digi-list>
              </digi-progress-list-item>
              <digi-progress-list-item
                afHeading="2. Kognitiv genomgång"
                afAriaLabel="2. Kognitiv genomgång">
                <p>
                  Utvecklingsteamet och Designsystemsteamet träffas och går
                  tillsammans igenom tjänsten steg för steg.
                </p>
                <p>
                  Varje steg granskas ur användbarhets- och
                  tillgänglighetsperspektiv och utifrån gällande krav,
                  riktlinjer och rekommendationer.
                </p>
                <p>En rapport med åtgärdsplan sammanställs.</p>
              </digi-progress-list-item>
              <digi-progress-list-item
                afHeading="3. Uppföljande möte"
                afAriaLabel="3. Uppföljande möte">
                <p>
                  Utvecklingsteamet bokar en uppföljning med Designsystemsteamet
                  för att gå igenom åtgärdade punkter.
                </p>
                <p>
                  Under uppföljningsmötet använder vi rapporten som underlag. Om
                  vi hittar fler brister, görs tillägg till rapporten.
                </p>
              </digi-progress-list-item>
            </digi-progress-list>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Extra möte vid behov</h2>
              <p>
                Ibland bokar vi extra möten. Det kan vara då utvecklingsteamet
                behöver hjälp med att gå igenom konceptskisser eller annat
                designunderlag inför utveckling. Eller då teamet vill diskutera
                en specifik fråga som är viktig att besvara innan eller under
                själva utvecklingen.
              </p>
            </article>
          </digi-layout-container>
          <digi-layout-block
            af-variation="secondary"
            af-vertical-padding
            af-margin-bottom>
            <h2>Varför testa?</h2>
            <p>
              <strong>
                16% svenskar har någon typ av funktionsnedsättning.
              </strong>
            </p>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <p>
                  Funktionsnedsättningar kan påverka personen möjligheter att
                  hantera digitiala tjänster och dess interaktioner.
                </p>
                <p>
                  Bristande digital tillgänglighet och dålig användbarhet kostar
                  mycket på grund av ineffektiv användning, utbildnings- och
                  supportkostnader. Det är inte heller kostnadseffektivt att
                  rätta bristerna när tjänsten är lanserad.{' '}
                </p>
              </div>
              <div>
                <digi-list af-list-type="bullet">
                  <li>
                    Över en miljon svenskar har en permanent nedsatt kognitiv
                    förmåga.
                  </li>
                  <li>
                    Över en halv miljon människor har en rörelsenedsättning.
                  </li>
                  <li>
                    Nästan en och en halv miljon har läs-och skrivsvårigheter.
                  </li>
                  <li>
                    {' '}
                    Över hundra tusen personen har synnedsättning, varav ca
                    trettio tusen gravt synnedsatta eller blinda.
                  </li>
                </digi-list>
                <p> Källor: Hjärnfonden, SCB och OECD</p>
              </div>
            </digi-layout-columns>
            <br />
            <digi-link-internal
              afHref="/tillganglighet-och-design/om-tillganglighetsmatrisen"
              af-variation="small">
              Tillgänglighetsmatrisen om funktionsnedsättningar
            </digi-link-internal>
          </digi-layout-block>
        </digi-docs-page-layout>
      </host>
    );
  }
}
