import { Component, h, getAssetPath } from '@stencil/core';
import { router } from '../../global/router';
import state from '../../store/store';
import { GoodBadExampleType } from './good-bad-example/good-bad-example-type.enum';
//import { InfoCardHeadingLevel } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-image-policy',
  styleUrl: 'digi-docs-image-policy.scss',
  scoped: true
})
export class DigiDocsImagePolicy {
  Router = router;
  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }
  render() {
    return (
      <div class="digi-docs-image-policy">
        <digi-docs-page-layout af-edit-href="pages/digi-docs-image-policy/digi-docs-image-policy.tsx">
          <digi-typography>
            <digi-layout-block>
              <digi-typography-heading-jumbo af-text="Bilder"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                Bilder är en viktig del i kommunikationen. De engagerar, skapar
                känslor och förstärker både intryck och uttryck. Våra bilder ska
                visualisera våra uppdrag och skapa tydlighet för våra
                målgrupper.
              </digi-typography-preamble>
              <h2>Bildspråk</h2>
              <p>
                På Arbetsförmedlingen jobbar vi med människors utveckling. Vi
                vill att det ska speglas i våra bilder. Bilderna ska innehålla
                ett naturligt blickfång och lyfta fram olika människor i olika
                situationer, kopplat till arbete och aktivt arbetssökande.
              </p>
              <p class="digi-docs-image-policy__smallermargin">
                Människor står i fokus och avbildas ärligt och äkta. Bilderna:
              </p>
              <digi-list af-list-type="bullet">
                <li>upplevs som spontana och tagna i stunden.</li>
                <li>speglar verkligheten och är inkluderande.</li>
                <li>visar på samspel och aktivitet i relevanta sammanhang.</li>
                <li>
                  har naturliga färgtoner och ljussättning som upplevs som äkta.
                </li>
              </digi-list>
              <div class="examples">
                <digi-docs-good-bad-example>
                  <h2>Rätt bild</h2>
                  <digi-list af-list-style="bullet">
                    <li>Riktiga människor i riktiga situationer.</li>
                    <li>Autentisk känsla och naturliga miljöer.</li>
                    <li>Ärlighet, gemenskap, mångfald och engagemang.</li>
                  </digi-list>
                  <img
                    src={getAssetPath(
                      '/assets/images/ImagePolicy/image-language.jpg'
                    )}
                    alt="Rätt användning av logotypen"
                  />
                </digi-docs-good-bad-example>
                <br />
                <digi-docs-good-bad-example
                  exampleType={GoodBadExampleType.BAD}>
                  <h2>Fel bild</h2>
                  <digi-list af-list-style="bullet">
                    <li>Poserande ”fotomodeller” i onaturliga miljöer.</li>
                    <li>
                      Effekter/grafik/text/illustrationer integrerat i bilden.
                    </li>
                    <li>
                      Onaturliga filter och ljus (overkligt och retuscherat).
                    </li>
                    <li>
                      Obefogade negativa känslor (till exempel ledsamhet, ilska,
                      tomhet).
                    </li>
                    <li>Dekorationsbilder utan koppling till innehåll.</li>
                    <li>
                      Undvik bilder med alkohol, tobak och vapen om det inte är
                      yrkesrelaterat.
                    </li>
                  </digi-list>
                  <img
                    src={getAssetPath(
                      '/assets/images/ImagePolicy/image-language-fel.jpg'
                    )}
                    alt="Dålig användning av logotypen"
                  />
                </digi-docs-good-bad-example>
              </div>
              <h2>Andra bilder</h2>
              <digi-typography>
                <digi-list af-list-type="bullet">
                  <li>
                    <b>Dekorationsbilder</b>, som också finns i mediebanken, ska
                    du endast använda i interna sammanhang. Använd dem sparsamt
                    (max 20 procent av den totala bildmängden) och se till att
                    de har en stark koppling till texten.
                  </li>
                  <li>
                    <b>Egna bilder</b> får du använda om de är av tillräckligt
                    god kvalitet. Personerna på bilderna måste godkänna att de
                    är med, helst skriftligen.
                  </li>
                  <li>
                    <b>Gratisbilder</b> ska du inte använda av upphovsrättsliga
                    skäl. De flesta gratisbyråerna har ofta avancerade och
                    svårtolkade avtal, där man frånsäger sig ansvaret genom så
                    kallade ”disclaimers”. Att en fotograf tillåter dig att
                    använda en bild betyder inte att modellerna har godkänt sin
                    medverkan. Den som publicerar bilden ansvarar för att följa
                    reglerna.
                  </li>
                </digi-list>
              </digi-typography>
              <h2>Mediebank</h2>
              <p>
                I Arbetsförmedlingens mediebank ligger myndighetens
                kvalitetssäkrade bilder. Det är fritt för medarbetare att
                använda bilderna i arbetet. Förutom porträttbilderna får andra
                aktörer (som till exempel myndigheter, media eller företag) inte
                använda bilderna som finns i mediebanken. Myndighetens
                leverantörer som kommunicerar i myndighetens namn har tillgång
                till ett begränsat innehåll i en separat portal.
              </p>
              <p>
                Trots att det finns avtal och samtycken knutet till samtligt
                innehåll i mediebanken, kan publiceringar kopplade till
                exempelvis psykisk ohälsa eller arbetslivskriminalitet upplevas
                som extra känsligt. Vid så kallad känslig publicering är det
                viktigt att respektera människors integritet. Kontakta
                bildansvarig via självbetjäningsportalen för att få reda på vad
                som behöver göras inför känslig publicering.
              </p>
              <h2>Hjälp?</h2>
              <p>
                En bild kan tolkas på många olika sätt. Om något känns fel så är
                sannolikheten stor att mottagaren känner likadant. Bolla med en
                kollega eller hör av dig till kommunikationsavdelningen om du
                känner dig osäker.
              </p>
              <hr></hr>
              <br />
              <digi-layout-columns af-variation={state.responsiveTwoColumns}>
                <div>
                  <h4>Mediebanken</h4>
                  <p>
                    Mediebank för myndighetens medarbetare med bland annat
                    bilder, infografik och illustrationer.
                  </p>
                  <digi-docs-related-links>
                    <digi-link-button
                      afHref="https://arbetsformedlingen.mediaflowportal.com/"
                      af-target="_blank"
                      af-size="medium"
                      af-variation="secondary">
                      Mediebank
                    </digi-link-button>
                  </digi-docs-related-links>
                  {''}
                  <br />
                </div>
                <div>
                  <h4>Illustrationer</h4>
                  <p>Se relaterad sida inom grafiska profilen</p>
                  <br />
                  <digi-docs-related-links>
                    <digi-link-button
                      afHref="/grafisk-profil/illustrationer"
                      af-target="_blank"
                      af-size="medium"
                      af-variation="secondary"
                      onAfOnClick={(e) => this.linkClickHandler(e)}>
                      Illustrationer
                    </digi-link-button>
                  </digi-docs-related-links>
                </div>
              </digi-layout-columns>
            </digi-layout-block>
          </digi-typography>
        </digi-docs-page-layout>
      </div>
    );
  }
}
