import { Component, h, State } from '@stencil/core';

@Component({
  tag: 'digi-docs-about-accessibility-and-language',
  styleUrl: 'digi-docs-about-accessibility-and-language.scss',
  scoped: true
})
export class AboutAccessibilityAndLanguage {
  @State() pageName = 'Om tillgänglighet';

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-accessibility-and-language/digi-docs-about-accessibility-and-language.tsx">
          <span slot="preamble">
            Vi vill vara tillgängliga för alla våra användare. Därför arbetar vi
            mycket med att våra tjänster ska vara enkla att använda oavsett
            funktionsnedsättning eller förmåga. Vi vet att det som är nödvändigt
            för vissa användare, är väldigt bra för alla våra användare.
          </span>
          <digi-layout-container af-margin-bottom af-margin-top>
            <article>
              <p>
                Här kan du också läsa mer om hur vi arbetar med tillgänglighet.
                Hur vi tänker kring universell utformning, responsiv design,
                semantisk kod och andra viktiga delar inom området.
              </p>
            </article>
          </digi-layout-container>
        </digi-docs-page-layout>
      </host>
    );
  }
}
