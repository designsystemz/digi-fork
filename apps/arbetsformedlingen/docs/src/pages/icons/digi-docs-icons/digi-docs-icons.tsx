import { Component, h, State } from '@stencil/core';
import state from '../../../store/store';
import { router } from '../../../global/router';
import { Route } from 'stencil-router-v2';

import { LayoutBlockContainer } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-icons',
  styleUrl: 'digi-docs-icons.scss',
  scoped: true
})
export class DigiDocsIcons {
  Router = router;

  @State() isMobile: boolean;
  @State() isOpen: boolean;
  @State() activeTab: string;
  @State() pageName = 'Ikonbibliotek';

  componentWillLoad() {
    this.activeTab = this.getTabId()
  }

  getTabId(): string {
    const tabName: string = state.router.activePath;

    switch (tabName) {
      case '/ikoner/bibliotek':
        return 'bibliotek'
      case '/ikoner/beskrivning':
        return 'beskrivning';
      case '/ikoner/api':
        return 'api';
    }
  }

  toggleNavTabHandler(e) {
    const tag = e.target.dataset.tag;
    this.Router.push(`${state.routerRoot}ikoner/${tag}`);
  }

  abstractLinkHandler(e) {
    e.detail.preventDefault();
    window.open(e.target.afHref, '_blank');
  }

  navigateTo(tabId: string) {
    this.Router.push(`${state.routerRoot}ikoner/${tabId}`);
  }
  get tabs() {
    return [
      { id: 'bibliotek', title: 'Bibliotek' },
      { id: 'beskrivning', title: 'Beskrivning' },
      { id: 'api', title: 'Kod' }
    ];
  }

  render() {
    return (
      <div class="digi-docs-icons">
        <digi-docs-page-layout af-page-heading={this.pageName}>
          <span slot="preamble">
            Ikonerna på den här sidan är framtagna för att vara tillgängliga,
            användbara och att erbjuda en enhetlig upplevelse i våra tjänster.
          </span>
          <digi-layout-block af-container={LayoutBlockContainer.STATIC}>
            <digi-layout-container af-no-gutter af-margin-bottom af-margin-top>
              <p class="digi-docs-icons__link-abstract">
                <digi-link-button
                  onAfOnClick={(e) => this.abstractLinkHandler(e)}
                  afHref="https://www.figma.com/community/file/1415694540311931262/af-icons"
                  af-href="https://www.figma.com/community/file/1415694540311931262/af-icons">
                  Ikonbiblioteket i Figma
                </digi-link-button>
              </p>

              <p>
                Om du behöver en ikon som inte finns idag så kan du höra av dig
                till oss så hjälper vi till med att skapa den och lägga upp den
                i designsystemets utvecklingsmiljö.
              </p>
              <digi-link
                af-variation="small"
                afHref={`mailto:designsystem@arbetsformedlingen.se`}
                class="digi-docs__accessibility-link">
                <digi-icon-envelope slot="icon"></digi-icon-envelope>
                Mejla designsystem@arbetsformedlingen.se
              </digi-link>
            </digi-layout-container>
            <digi-tablist
              afAriaLabel="Innehåll för ikoner"
              afTabs={this.tabs}
              afActiveTab={this.activeTab}
              onAfTabChange={(event) => this.navigateTo(event.detail.id)}>
              {this.tabs.map((tab) => (
                <digi-tablist-panel tab={tab.id} />
              ))}
            </digi-tablist>
          </digi-layout-block>
          <digi-layout-block af-container={LayoutBlockContainer.NONE}>
            <this.Router.Switch>
              <Route path="/ikoner/bibliotek">
                <digi-docs-icons-library />
              </Route>
              <Route path="/ikoner/beskrivning">
                <digi-docs-icons-description />
              </Route>
              <Route path="/ikoner/api">
                <digi-docs-icons-api />
              </Route>
            </this.Router.Switch>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
