import { Component, h, Listen, Prop, State } from '@stencil/core';
import {
  ButtonSize,
  ButtonVariation,
  LayoutBlockContainer
} from '@digi/arbetsformedlingen';
import state from '../../../store/store';

@Component({
  tag: 'digi-docs-icons-library',
  styleUrl: 'digi-docs-icons-library.scss',
  scoped: true
})
export class DigiDocsIconsLibrary {
  @State() isMobile: boolean;
  @State() isOpen: boolean;
  @State() searchFilter = '';
  @Prop() component: string;

  private inverted: boolean;
  private primary: boolean;

  copyButtonClickHandler(e) {
    navigator.clipboard.writeText(`<${e}></${e}>`).then(
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      () => {},
      (err) => {
        console.error(err);
      }
    );
  }

  setName(iconName) {
    const name = iconName.split('digi-icon-').join('');
    return name;
  }

  @Listen('afOnInput')
  searchFilterChangeHandler(event) {
    if (event.target.matches('.digi-form-input-search__input')) {
      const inputValue = event.detail.target.value;
      this.searchFilter = inputValue;
    }
  }

  render() {
    return (
      <div class="digi-docs-icons-library">
        <digi-docs-page-layout>
          <digi-layout-block af-container={LayoutBlockContainer.STATIC}>
            <div class="digi-docs-icons__filters">
              <div class="digi-docs-icons__search-filter">
                <digi-form-input-search
                  af-label={`Filtrera ikoner`}
                  afLabel={`Filtrera ikoner`}
                  af-hide-button="true"></digi-form-input-search>
              </div>
            </div>
          </digi-layout-block>
          <digi-layout-block
            af-container={LayoutBlockContainer.FLUID}
            afMarginBottom>
            <div class="digi-docs-icons__list-wrapper">
              <ul class="digi-docs-icons__list">
                {state.icons
                  .filter((component) => {
                    if (this.searchFilter !== '') {
                      return component.tag.includes(this.searchFilter);
                    } else {
                      return component;
                    }
                  })
                  .map((component) => {
                    return (
                      <li class="digi-docs-icons__item">
                        <div
                          class={{
                            'digi-docs-icons__main': true,
                            'digi-docs-icons__main--inverted': this.inverted,
                            'digi-docs-icons__main--primary': this.primary
                          }}>
                          <digi-media-figure
                            af-figcaption={this.setName(component.tag)}
                            af-alignment="center"
                            class="digi-docs-icons__figure">
                            <component.tag class="digi-docs-icons__icon"></component.tag>
                          </digi-media-figure>
                          <div class="digi-docs-icons__footer">
                            <digi-button
                              onAfOnClick={() =>
                                this.copyButtonClickHandler(component.tag)
                              }
                              af-variation={ButtonVariation.FUNCTION}
                              af-size={ButtonSize.SMALL}
                              af-aria-label={`Kopiera kod för ikonen ${this.setName(
                                component.tag
                              )}`}>
                              <digi-icon-copy
                                aria-hidden="true"
                                slot="icon"></digi-icon-copy>
                              Kopiera kod
                            </digi-button>
                          </div>
                        </div>
                      </li>
                    );
                  })}
              </ul>
            </div>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
