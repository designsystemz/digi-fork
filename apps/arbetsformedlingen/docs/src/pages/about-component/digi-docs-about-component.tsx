import { Component, h, State } from '@stencil/core';

@Component({
  tag: 'digi-docs-about-component',
  styleUrl: 'digi-docs-about-component.scss',
  scoped: true
})
export class AboutComponent {
  @State() pageName = 'Om vårt komponentbibliotek';

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-component/digi-docs-about-component.tsx">
          <span slot="preamble">
            Komponenterna är fundamentet i designsystemet. De är byggstenarna
            som skapar visuella och funktionella kopplingar mellan produkter och
            tjänster.
          </span>
          <digi-layout-container af-margin-bottom af-margin-top>
            <article>
              <p>
                Komponenterna är uppdelade i olika kategorier beroende på
                funktionalitet. I varje kategori finns det en demonstration av
                komponenten som visar hur den fungerar och interagerar.
              </p>
            </article>
          </digi-layout-container>

          <digi-layout-container>
            <digi-docs-component-list></digi-docs-component-list>
          </digi-layout-container>
        </digi-docs-page-layout>
      </host>
    );
  }
}
