import { Component, getAssetPath, h } from '@stencil/core';
import { router } from '../../../global/router';

@Component({
  tag: 'digi-pattern-agentive-intro',
  styleUrl: 'digi-pattern-agentive-intro.scss',
  scoped: true
})
export class AgentiveIntro {
  Router = router;

  linkHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-layout-container>
          <div class="digi-pattern-agentive-intro">
            <div class="digi-pattern-agentive-intro__text">
              <digi-typography-heading-jumbo
                af-text="Agentiva tjänster och artificiell intelligens"
                af-level="h1"></digi-typography-heading-jumbo>
              <digi-typography-preamble class="digi-pattern-agentive-intro__preamble">
                Agentiva tjänster är digitala tjänster som använder artificiell
                intelligens för att utföra arbete efter användarens behov och
                förutsättningar. Dessa tjänster kan integreras i digitala
                produkter såsom röstassistenter och uppgiftsutförande tjänster.
              </digi-typography-preamble>
              <p>
                Detta designmönster ger rekommendationer för att se till att
                tjänsten inte blir för teknikdriven och missar de mänskliga
                aspekterna. I Arbetsförmedlingens kundundersökning 2022 fanns
                indikationer på att kunder generellt är skeptiska till "robotar"
                eftersom de inte riktigt upplevs förstå vad användarna vill.
              </p>
            </div>
            <div class="digi-pattern-agentive-intro__media">
              <digi-media-image
                class="digi-pattern-agentive-intro__media-image"
                afUnlazy
                afSrc={getAssetPath(
                  '/assets/images/pattern/agentive-services/ai-man-objects.jpg'
                )}
                afAlt="Person i centrum med vissa geometriska former runtomkring, illustration."></digi-media-image>
            </div>
          </div>
        </digi-layout-container>
      </host>
    );
  }
}
