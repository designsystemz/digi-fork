import { Component, getAssetPath, h } from '@stencil/core';

@Component({
  tag: 'digi-docs-forskning-och-samarbeten',
  styleUrl: 'digi-docs-forskning-och-samarbeten.scss',
  scoped: true
})
export class ForskningOchSamarbeten {
  render() {
    return (
      <div>
        <digi-docs-page-layout af-edit-href="pages/introduktion/digi-docs-forskning-och-samarbeten.tsx">
          <digi-layout-block afMarginBottom>
            <digi-typography-heading-jumbo af-text="Forskning och samarbeten"></digi-typography-heading-jumbo>
            <digi-typography-preamble>
              Arbetsförmedlingens designsystem baseras på gemensamma insikter,
              research och användarbehov. Vi vill vara ledande genom att både
              använda och bidra till forskning, samarbeta och följa de bästa
              metoderna.
            </digi-typography-preamble>
            <digi-typography>
              <h2>Öppen källkod och öppen data</h2>
              <p>
                Vi har valt att göra designsystemet öppet och fritt för alla att
                använda, eftersom det skapar bättre möjligheter för samarbete,
                även med näringslivet.
              </p>
              <h3>Samarbeten med utbildningsväsendet</h3>
              <digi-list>
                <li>
                  Elever från olika skolor har hjälpt oss att förbättra och
                  vidareutveckla våra rekommendationer och processer för digital
                  tillgänglighet.
                </li>
                <li>
                  Yrkeshögskolor har använt vårt designsystem i sina
                  utbildningar och skapat många varianter på både befintliga och
                  nya tjänster.
                </li>
                <li>
                  Studenterna har fått en chans att visa sina färdigheter, och
                  mycket av det de levererat har blivit en uppskattad del av
                  dokumentationswebben.
                </li>
                <li>
                  KTH och Dena Hussein Al-Omran valde att forska på våra
                  arbetssätt kring digital inkludering i sin doktorsavhandling
                  ”A Framework for Digital Inclusion”, KTH 2024.
                </li>
              </digi-list>
              <digi-link-external
                af-target="_blank"
                afHref="https://kth.diva-portal.org/smash/get/diva2:1857125/SUMMARY01.pdf">
                ”A Framework for Digital Inclusion”, KTH 2024
                (kth.diva-portal.org, öppnas i egen flik)
              </digi-link-external>
              <h3>Samarbete med myndigheter och företag</h3>
              <digi-list>
                <li>
                  Vi har haft flera samarbeten med andra myndigheter, vilket har
                  resulterat i den första versionen av ett designsystem som kan
                  användas av flera myndigheter.
                </li>
                <li>
                  Vi har även träffat representanter från både offentlig och
                  privat sektor som varit nyfikna på vårt arbete med
                  designsystemet och hur de kan inspireras av eller använda det
                  i sin egen verksamhet.
                </li>
              </digi-list>
              <div class="digi-docs-forskning--image">
                <digi-media-image
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/digital-tillganglighet.png'
                  )}
                  afAlt="Illustration över digital tillgänglighet"></digi-media-image>
              </div>
              <h2>Forskning i digital inkludering</h2>
              <p>
                Tyvärr finns det inte mycket forskning om hur organisationer kan
                skapa rätt förutsättningar för att utvecklingsteam ska kunna
                bygga tillgängliga tjänster.
              </p>
              <h3>Designsystemet i forskningen</h3>
              <digi-quote-single
                af-variation="primary"
                af-quote-author-name="Dena Hussein Al-Omran"
                af-quote-author-title="Doctor of Engineering">
                This approach ensures that accessibility is not just a legal
                obligation but also a fundamental aspect of software
                development, fostering a culture of inclusion and awareness
                within digital environments.
              </digi-quote-single>
              <p>
                År 2024 kom Dena Hussein Al-Omrans doktorsavhandling “A
                Framework for Digital Inclusion” där hen beskriver den digitala
                tillgänglighetens komplexitet och ser i sin forskning hur ett
                väletablerat designsystem kan skapa förutsättningar för
                organisationen att uppnå god användbarhet och tillgänglighet
                utöver att uppfylla lagkrav.
              </p>
              <p>
                Vi i designsystemet är stolta att vårt arbete med digital
                inkludering finns med i Hussain Al-Omrans doktorsavhandling som
                ett bra exempel på effektivt tillgänglighetsarbete i en
                organisation.
              </p>
              <p>
                I citatet nedan nämns en “ICT platform”. ICT står för
                Information and Communication Technology och är i denna text
                detsamma som designsystemet.
              </p>
              <p>
                “… this specific public organisation utilises a customised
                in-house ICT platform within the organisation. This ICT platform
                aims to create a knowledge base where all stakeholders involved
                in the development of accessible web services could utilise this
                platform during the development process.” (s.56, kapitel 4.2.2.
                Gaining A Deeper Understanding of ICT Tools Developed)
              </p>
              <p>
                Hussein Al-Omrans ser att vårt tillgänglighetsarbete som går ut
                på att;
              </p>
              <digi-list>
                <li>
                  involverar alla yrkesgrupper i ett agilt utvecklingsteam,
                </li>
                <li>involvera produktansvariga,</li>
                <li>skapa en gemensam plats för kunskapsdelning,</li>
              </digi-list>
              <p>
                <strong>
                  är bevisligen den bästa formen för att bedriva digital
                  inkludering i stor skala.
                </strong>
              </p>
              <p>
                Hen skriver; “Thus, by recognising stakeholders' diverse
                concerns and fostering effective collaboration through
                customised ICT tools, projects can better navigate the
                complexities of developing inclusive digital services and
                ultimately achieve their goals of digital accessibility and
                inclusion.” (s.62, kapitel 4.2.2 Gaining a Deeper understanding
                of ICT Tools Developed)
              </p>
              <p>
                Hussein Al-Omrans avslutar kapitlet genom att skriva ”By
                adopting inclusive design practices and involving stakeholders
                with diverse expertise, software teams can create more inclusive
                and accessible products. This approach ensures that
                accessibility is not just a legal obligation but also a
                fundamental aspect of software development, fostering a culture
                of inclusion and awareness within digital environments. By
                incorporating these insights, software teams can develop a
                comprehensive approach to integrating accessibility into agile
                development processes, ultimately creating more inclusive and
                accessible digital products and services” (s.70, kapitel 4.3
                Synergising Accessibility and Agile Development: Facilitating
                Knowledge Sharing)
              </p>
              <div class="digi-docs-forskning--image">
                <digi-media-image
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/samarbete-illustration.png'
                  )}
                  afAlt="Illustration över digital problemlösning"></digi-media-image>
              </div>

              <h2>Framtida samarbeten</h2>
              <p>
                Vi ser fram emot fler samarbeten i framtiden, när vi har tid. Vi
                kommer alltid att prioritera dom samarbeten som använder öppen
                data och öppen källkod, eftersom vi tror att det ger mest värde
                och stämmer överens med regeringens digitaliseringsstrategi för
                ett hållbart digitalt Sverige.
              </p>
              <digi-link-external
                af-target="_blank"
                af-variation="primary"
                afHref="https://www.regeringen.se/contentassets/c9bc0cd3a4374f9388e714ae7fb1ec1d/for-ett-hallbart-digitaliserat-sverige-en-digitaliseringsstrategi.pdf">
                För ett hållbart digitaliserat Sverige – en
                digitaliseringsstrategi (regeringen.se, öppnas i egen flik).
              </digi-link-external>
            </digi-typography>
          </digi-layout-block>
          <digi-layout-block
            af-variation="secondary"
            style={{ 'padding-top': '3rem' }}>
            <div class="digi-docs-home-content-blocks digi-docs-home-content-blocks--media-right">
              <div class="digi-docs-home-content-blocks__media ">
                <digi-media-image
                  class="digi-docs-home-content-blocks__media-image"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/Startsida-oppenkallkod.png'
                  )}
                  afAlt="Kollegor på kontor jobbar tillsammans"></digi-media-image>
              </div>
              <div class="digi-docs-home-content-blocks__text">
                <h2>Samarbete genom öppen källkod</h2>
                <p class="digi-docs-home-content-blocks__preamble">
                  Arbetsförmedlingen och andra myndigheter samarbetar kring
                  designsystem. Följ det gemensamma arbetet och bidra till
                  utvecklingen på GitLab.
                </p>
                <digi-link-external
                  afHref="https://gitlab.com/arbetsformedlingen/designsystem"
                  af-target="_blank"
                  af-variation="primary">
                  Utvecklarplattformen (gitlab.com, öppnas i egen flik)
                </digi-link-external>
              </div>
            </div>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
