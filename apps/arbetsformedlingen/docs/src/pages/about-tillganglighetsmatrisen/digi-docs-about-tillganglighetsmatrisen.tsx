import { Component, h, Prop } from '@stencil/core';
import Helmet from '@stencil/helmet';
import state from '../../store/store';

@Component({
  tag: 'digi-docs-about-tillganglighetsmatrisen',
  styleUrl: 'digi-docs-about-tillganglighetsmatrisen.scss',
  scoped: true
})
export class DigiDocsAboutTillganglighetsmatrisen {
  @Prop() pageName = 'Tillgänglighetsmatrisen';

  render() {
    return (
      <host>
        <Helmet>
          <meta
            property="rek:pubdate"
            content="Tue Dec 05 2023 08:08:09 GMT+0100 (Central European Standard Time)"
          />
        </Helmet>
        <digi-typography>
          <digi-docs-page-layout
            af-page-heading={this.pageName}
            af-edit-href="pages/digi-docs-about-tillganglighetsmatrisen/digi-docs-about-tillganglighetsmatrisen.tsx">
            <digi-layout-container af-margin-bottom af-margin-top>
              <digi-layout-columns af-variation={state.responsiveTwoColumns}>
                <div>
                  <article>
                    <p>
                      God digital tillgänglighet bidrar till ökad
                      sysselsättning. På Arbetsförmedlingen arbetar vi enligt
                      principen att det som är bra för en del är bra för alla. I
                      tillgänglighetsmatrisen möter du olika användare med
                      funktionsnedsättningar. En del funktionsnedsättningar
                      syns, andra inte. Vissa är permanenta, andra tillfälliga
                      och en del beror på situationen. En person kan ha flera
                      olika funktionsnedsättningar samtidigt.
                    </p>
                    <p>
                      Exemplen i tabellen hjälper oss att få en större
                      förståelse för våra användare, utifrån deras behov. Så att
                      vi kan skapa bästa tänkbara tjänster, för både kunder och
                      medarbetare.
                    </p>
                  </article>
                </div>
                <div>
                  <digi-quote-single
                    af-variation="primary"
                    af-quote-author-name="Arbetsförmedlingen">
                    Vi motiverar, utvecklar och vägleder arbetssökande i
                    allmänhet och de som står långt från arbetsmarknaden i
                    synnerhet.
                  </digi-quote-single>
                </div>
              </digi-layout-columns>
            </digi-layout-container>
            <digi-layout-container af-margin-bottom>
              <digi-docs-accessibility-matrix></digi-docs-accessibility-matrix>
            </digi-layout-container>
            <digi-layout-block
              af-variation="symbol"
              af-vertical-padding
              af-margin-bottom>
              <h2>Tips Användbara sidor om tillgänglighet i designsystemet</h2>
              <digi-layout-columns af-variation={state.responsiveThreeColumns}>
                <div>
                  <h3>Digital tillgänglighet för alla</h3>
                  <p>
                    Varför är digital tillgänglighet så viktigt? Och hur arbetar
                    vi med det? Läs mer om vårt mål att skapa en professionell,
                    inspirerande och förtroendeingivande upplevelse för alla.
                  </p>
                  <digi-link-internal
                    afHref="/tillganglighet-och-design/om-digital-tillganglighet"
                    af-variation="small">
                    Om digital tillgänglighet
                  </digi-link-internal>
                </div>
                <div>
                  <h3>Lagkrav och riktlinjer</h3>
                  <p>
                    Tillgänglighet till digital offentlig service - vad innebär
                    det? Diskrimineringslagen och arbetsmiljölagen? Lär dig mer
                    om hur våra olika lagar stärker inkluderande design.
                  </p>
                  <digi-link-internal
                    afHref="/tillganglighet-och-design/lagkrav-och-riktlinjer"
                    af-variation="small">
                    Lagar och riktlinjer
                  </digi-link-internal>
                </div>
                <div>
                  <h3>Checklista för självskattning</h3>
                  <p>
                    Använd tillgänglighetslistan för att pröva dina digitala
                    tjänster. Eller för att lära dig mer om vad lagkraven
                    betyder i praktiken.
                  </p>
                  <digi-link-internal
                    afHref="/tillganglighet-och-design/tillganglighet-checklista"
                    af-variation="small">
                    Tillgänglighetslistan
                  </digi-link-internal>
                </div>
              </digi-layout-columns>
            </digi-layout-block>
          </digi-docs-page-layout>
        </digi-typography>
      </host>
    );
  }
}
