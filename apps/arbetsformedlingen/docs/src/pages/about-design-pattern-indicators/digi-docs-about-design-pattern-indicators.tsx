import { Component, h, Prop, getAssetPath } from '@stencil/core';
import Helmet from '@stencil/helmet';
import state from '../../store/store';

@Component({
  tag: 'digi-docs-about-design-pattern-indicators',
  styleUrl: 'digi-docs-about-design-pattern-indicators.scss',
  scoped: true
})
export class AboutDesignPatternIndicators {
  @Prop() pageName = 'Indikatorer';

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-design-pattern-indicators/digi-docs-about-design-pattern-indicators.tsx">
          <Helmet>
            <meta
              property="rek:pubdate"
              content="Wed Jan 03 2024 09:52:45 GMT+0100 (Central European Standard Time)"
            />
          </Helmet>
          <span slot="preamble">
            I detta designmönster visar vi notifikationsindikator,
            statusindikator och mediaindikator. Notifikationsgrafik påvisar ny
            händelse av någon sort. Statusindikator hjälper användaren visuellt
            att förstå status på till exempel ett kort. Mediaindikator är till
            för att kommunicera och skapa förförståelse för olika mediaformat.
          </span>
          <br />
          <digi-layout-block
            af-variation="secondary"
            af-vertical-padding
            //af-margin-bottom
            af-no-gutter>
            <h2>Notifikationsindikator</h2>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <p>
                  Indikatorer används för att uppmärksamma en specifik post
                  eller inlägg eller en eller flera händelse genom att visa en
                  märkning för att påvisa att något nytt har hänt. Denna
                  funktion är bland annat användbar där det önskas
                  användaråtgärder. Till exempel antalet olästa inlägg, eller
                  för att kommunicera att något är nytt, till exempel en ny sida
                  eller en ny händelse i ett kort. Indikatorn är utformad för
                  att var tydlig men inte påträngande.
                </p>
                <h3>Andvändning av notifikationsindikatorn</h3>
                <p>
                  Notifikationindikator meddelar användare när det finns en
                  väntande avisering som de ännu inte har agerat på.
                </p>
                <p>
                  Notifikationindikator visas <strong>alltid</strong> ihop med
                  en annan komponent som till exempel en länkknapp, ett
                  menyalternativ eller annan relevant gränssnittselement.
                </p>
                <p>
                  Notifikationen kan ha siffra eller ett ord som tex Ny, Nytt,
                  eller Ny sida. Undvik använd långa ord, ett eller två ord är
                  att föredra.
                </p>
                <p>
                  Vi visar max två siffror. Om antal av notiser är över 99, ska
                  det visa med ”+99” i indikatorn. Om antalet av händelser är
                  lika med 0, visas inte indikator. Konsekvent standardplacering
                  är topp- och högerjusterad uppepå komponenten den ligger på.
                </p>
                <p>
                  I händelse av att notifikationsindikatorn används på en knapp
                  med till exempel ”Ny” bestäm i förväg hur länge ett objekt är
                  ”nytt”. Undersök om det är motiverat att sätta en automatisk
                  tidsgräns på indikatorn. Till exempel 30 dagar.
                </p>
                <p>
                  Överbelasta inte användaren med för många indikatorer.
                  Prioritera och visa endast de mest relevanta meddelandena.
                </p>
                <p>Säkerställ tillgänglighet och tangentbordsnavigering.</p>
              </div>
              <div>
                <digi-layout-block af-variation="primary" afVerticalPadding>
                  <h4>Appliceringsexempel för notifikationsindikatorn</h4>
                  <p>
                    Notifikationsindikator finns i två olika utförande, en
                    första för antal och en andra för ord.
                  </p>
                  <br />
                  <div class="example-header-notif__1">
                    <digi-header-notification af-notification-amount="1">
                      <a href="#">
                        <digi-icon-user-alt></digi-icon-user-alt>
                        Mina sidor
                      </a>
                    </digi-header-notification>
                    <digi-header-notification af-notification-amount="12">
                      <a href="#">
                        <digi-icon-user-alt></digi-icon-user-alt>
                        Mina sidor
                      </a>
                    </digi-header-notification>
                    <digi-header-notification af-notification-amount="100">
                      <a href="#">
                        <digi-icon-bell-filled></digi-icon-bell-filled>
                        Notiser
                      </a>
                    </digi-header-notification>
                  </div>
                  <br />
                  <br />
                  <br />
                  <div class="example-header-notif__2">
                    <digi-link-button
                      afHref="#"
                      af-size="medium-large"
                      af-variation="primary">
                      Sök statistik
                      <digi-badge-notification af-value="Ny"></digi-badge-notification>
                    </digi-link-button>
                    <digi-link-button
                      afHref="#"
                      af-size="medium-large"
                      af-variation="secondary">
                      Insatser och program
                      <digi-badge-notification af-value="Ny sida"></digi-badge-notification>
                    </digi-link-button>
                  </div>
                  <br />
                </digi-layout-block>
              </div>
            </digi-layout-columns>
            <br />
            <br />
            <hr></hr>
            <h2>Statusindikator</h2>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <p>
                  Statusindikator är enbart en visuell indikator som används för
                  att indikera ett ärendes status.
                </p>
                <h3>Andvändning av statusindikatorn</h3>
                <p>
                  Statusindikator är en fristående komponent, den ska{' '}
                  <strong>inte</strong> användas som element på andra
                  komponenter, (till skillnad mot notifikationsindikatorn).
                </p>
                <p>
                  Vi har två varianter av statusindikator. Primär och sekundär.
                  Välj en variant som funkar tillsammans med din övriga design
                  och behov av visuell styrka i din sidas hierarki.
                </p>
                <p>
                  "Grön" kan användas för att framhäva en åtgärd som användaren
                  har utfört framgångsrikt.
                </p>
                <p>
                  "Grå" varianten kan användas för hjälpa till med
                  beslutsfattande men kräver inte omedelbar uppmärksamhet.
                </p>
                <p>
                  "Gul" Kan användas för detalj som kan påverka användarens
                  beslut.
                </p>
                <p>
                  "Röd" Kan användas för information som hindrar användaren från
                  att slutföra en åtgärd
                </p>
                <p>
                  "Rosa" Används för utvalda produkter eller tjänster som är
                  under beta eller i tidigt process och håller på testas.
                </p>
              </div>
              <div>
                <digi-layout-block af-variation="primary" afVerticalPadding>
                  <digi-media-image
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/pattern/indikators/status-badge-exampel.png'
                    )}
                    afAlt="Bild, exempel på hur en status tag kan användas i ett sammanhang"></digi-media-image>
                  <br />
                  <br />
                  <div class="example-grid__statusbadge">
                    <digi-badge-status
                      af-type="approved"
                      af-variation="primary"
                      afText="Godkänd"></digi-badge-status>
                    <digi-badge-status
                      af-type="prompt"
                      af-variation="primary"
                      afText="Info"></digi-badge-status>
                    <digi-badge-status
                      af-type="neutral"
                      af-variation="primary"
                      afText="Neutral"></digi-badge-status>
                    <digi-badge-status
                      af-type="missing"
                      af-variation="primary"
                      afText="Varning"></digi-badge-status>
                    <digi-badge-status
                      af-type="denied"
                      af-variation="primary"
                      afText="Stopp"></digi-badge-status>
                    <digi-badge-status
                      af-type="beta"
                      af-variation="primary"
                      afText="Profilrosa"></digi-badge-status>
                    <digi-badge-status
                      af-type="approved"
                      af-variation="secondary"
                      afText="Godkänd"></digi-badge-status>
                    <digi-badge-status
                      af-type="prompt"
                      af-variation="secondary"
                      afText="Info"></digi-badge-status>
                    <digi-badge-status
                      af-type="neutral"
                      af-variation="secondary"
                      afText="Neutral"></digi-badge-status>
                    <digi-badge-status
                      af-type="missing"
                      af-variation="secondary"
                      afText="Varning"></digi-badge-status>
                    <digi-badge-status
                      af-type="denied"
                      af-variation="secondary"
                      afText="Stopp"></digi-badge-status>
                    <digi-badge-status
                      af-type="beta"
                      af-variation="secondary"
                      afText="Profilrosa"></digi-badge-status>
                  </div>
                  <br />
                  <h4>Primära och sekundära statusindikatorer</h4>
                  <p>
                    Statusindikator finns i två olika varianter. Välj variant
                    som funkar tillsammans med din övriga design och behov av
                    visuell styrka.
                  </p>
                  <br />
                  <br />
                  <digi-badge-status
                    af-type="denied"
                    af-variation="primary"
                    afText="Avvecklas"></digi-badge-status>
                  <digi-badge-status
                    style={{ marginLeft: '15px' }}
                    af-type="denied"
                    af-variation="primary"
                    afText="Avvecklas"
                    af-size="small"></digi-badge-status>
                  <h4>Två storlekar</h4>
                  <p>
                    Statusindikator finns även i två olika storlekar. Använd den
                    stora som default. Använd den lilla när den stora inte får
                    plats, till exempel om en statusindikator behöver ligga inne
                    i en meny, eller inne i en filtervy.
                  </p>
                  <br />
                </digi-layout-block>
              </div>
            </digi-layout-columns>
            <br />
            <br />
            <hr></hr>
            <h2>Mediaindikator</h2>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <p>
                  Mediaindikator är ett objekt som används för att särskilja
                  olika mediaformat, och skapa förförståelse hos användaren.
                </p>
                <h3>Andvändning av mediaindikatorn</h3>
                <p>
                  Mediaindikatorn använd med fördel som element i puffar som
                  leder till innehåll med respektive mediatyp.
                </p>
                <p>
                  Indikatorer finns med dessa rubriker: Nyheter, Spellistor,
                  Podcast, Film, Webbinarium, och Webb-Tv.
                </p>
              </div>
              <div>
                <digi-layout-block af-variation="primary" afVerticalPadding>
                  <h4>Mediaindikatorer</h4>
                  <div class="example-grid__mediatag">
                    <digi-tag-media
                      afText="Nyheter"
                      af-icon="news"></digi-tag-media>
                    <digi-tag-media
                      afText="Spellista"
                      af-icon="playlist"></digi-tag-media>
                    <digi-tag-media
                      afText="Podcast"
                      af-icon="podcast"></digi-tag-media>
                    <digi-tag-media
                      afText="Film"
                      af-icon="film"></digi-tag-media>
                    <digi-tag-media
                      afText="Webb-TV"
                      af-icon="webtv"></digi-tag-media>
                    <digi-tag-media
                      afText="Webbinarium"
                      af-icon="webinar"></digi-tag-media>
                  </div>
                </digi-layout-block>
              </div>
            </digi-layout-columns>
          </digi-layout-block>
        </digi-docs-page-layout>
      </host>
    );
  }
}
