import { Component, h, State } from '@stencil/core';
import { CodeVariation, CodeLanguage } from '@digi/arbetsformedlingen';

import state from '../../store/store';

@Component({
  tag: 'digi-docs-work-with-digi-core',
  styleUrl: 'digi-docs-work-with-digi-core.scss',
  scoped: true
})
export class DigiDocsWorkWithDigiCore {
  @State() pageName = 'Jobba med Digi Core';

  linkClickHandler(e) {
    const href = e.detail.target.getAttribute('href');
    if (e.target.matches('digi-link') && href[0] === '/') {
      e.detail.preventDefault();
    }
  }

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/digi-docs-work-with-digi-core/digi-docs-work-with-digi-core.tsx">
          <span slot="preamble">
            Som utvecklare på Arbetsförmedlingen så utgår vi från vårt
            gemensamma komponentbibliotek Digi Core. Digi Core är ett
            komponentbibliotek, design tokens, funktionalitet, api:er och annat
            som hjälper dig att bygga digitala tjänster som följer våra
            gemensamma riktlinjer.
          </span>
          <digi-layout-container af-margin-bottom af-margin-top>
            <article>
              <h2>Vad finns i Digi Core?</h2>
              <p>
                Digi Core består av några olika paket, och listan växer hela
                tiden. De mest centrala är{' '}
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  afCode="@digi/arbetsformedlingen"></digi-code>{' '}
                som innehåller alla komponenter, css/scss, typsnitten etc., och{' '}
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  afCode="@digi/arbetsformedlingen-angular"></digi-code>{' '}
                och{' '}
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  afCode="@digi/arbetsformedlingen-react"></digi-code>{' '}
                som används i Angular respektive React för att förenkla
                utvecklingen med Core.
              </p>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Installera Digi Core</h2>
              <p>
                Börja med att konfigurera Nexus. Lägg till en fil som heter{' '}
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  afCode=".npmrc"></digi-code>{' '}
                i samma mapp som din{' '}
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  afCode="package.json"></digi-code>{' '}
                och lägg till den här raden:
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  afCode="@digi:registry=https://nexus.jobtechdev.se/repository/arbetsformedlingen-npm/"></digi-code>
              </p>
              <h3>NPM </h3>
              <p>
                Installera Digi Core genom att köra:
                <br />
                <digi-code
                  afLanguage={CodeLanguage.BASH}
                  afVariation={CodeVariation.LIGHT}
                  afCode="npm i @digi/arbetsformedlingen"></digi-code>
              </p>
              <h3>Läs in skriptfilen</h3>
              <p>
                <digi-code
                  afLanguage={CodeLanguage.HTML}
                  afVariation={CodeVariation.LIGHT}
                  af-code={
                    '<script type="module" src="node_modules/@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.esm.js"></script>'
                  }
                />
              </p>
              <h3>
                Lägg till <span lang="en">styles</span>
              </h3>
              <p>
                Lägg till <span lang="en">stylesheets</span> till din
                applikation.{' '}
              </p>
              <p>
                <digi-code
                  afLanguage={CodeLanguage.HTML}
                  afVariation={CodeVariation.LIGHT}
                  af-code={
                    '<link href="node_modules/@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css" rel="stylesheet" />'
                  }
                />
              </p>
              <h3>Lägg till typsnitt</h3>
              <p>
                För att använda sig av typsnitt så behöver man inkludera filerna
                från denna mapp i sin applikation:
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  af-code="@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/assets/fonts"
                />
              </p>
              <p>
                Nästa steg är att läsa in CSS/SCSS-filen som pekar mot dessa
                typsnitt:
                <digi-code
                  afLanguage={CodeLanguage.SCSS}
                  afVariation={CodeVariation.LIGHT}
                  af-code="@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/fonts.css"
                />
              </p>
              <h3>Enums</h3>
              <p>
                Alla eventuella enums hittar du direkt under
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  afCode="@digi/arbetsformedlingen"></digi-code>
              </p>
              <h4>Lägg till i Angular eller React</h4>
              <p>
                Att lägga till Digi Core i ditt Angular- eller React-projekt är
                inte så svårt. Det finns en Angular-modul att importera som du
                kan läsa mer om på sidan:{' '}
                <digi-link
                  af-variation="small"
                  afHref={`${state.routerRoot}kom-i-gang/jobba-med-digi-core/digi-core-angular`}
                  onAfOnClick={(e) => this.linkClickHandler(e)}>
                  Digi Core Angular
                </digi-link>{' '}
                och det finns även ett React-paket att importera som du kan läsa
                mer om på sidan:{' '}
                <digi-link
                  af-variation="small"
                  afHref={`${state.routerRoot}kom-i-gang/jobba-med-digi-core/digi-core-react`}
                  onAfOnClick={(e) => this.linkClickHandler(e)}>
                  Digi Core React
                </digi-link>
                .
              </p>
              <h3>Intellisense i HTML för VSCode</h3>
              <p>
                För att få intellisense i HTML för VSCode ska du lägga till{' '}
                <digi-code
                  afLanguage={CodeLanguage.JSON}
                  afVariation={CodeVariation.LIGHT}
                  afCode='"html.customData": ["./node_modules/@digi/arbetsformedlingen/custom-elements.json"]'></digi-code>{' '}
                i{' '}
                <digi-code
                  afLanguage={CodeLanguage.BASH}
                  afVariation={CodeVariation.LIGHT}
                  afCode=".vscode/settings.json"></digi-code>
                .
              </p>
            </article>
          </digi-layout-container>
        </digi-docs-page-layout>
      </host>
    );
  }
}
