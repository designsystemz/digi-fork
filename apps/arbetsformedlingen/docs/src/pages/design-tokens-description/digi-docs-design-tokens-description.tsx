import { Component, h, State } from '@stencil/core';
import { CodeBlockLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-design-tokens-description',
  styleUrl: 'digi-docs-design-tokens-description.scss',
  scoped: true
})
export class DesignTokensDescription {
  @State() pageName = 'Design Tokens Description';

  render() {
    return (
      <div class="digi-docs-design-tokens-description">
        <digi-docs-page-layout>
          <digi-docs-page-layout>
            <digi-layout-block afMarginTop afMarginBottom>
              <p>
                <span lang="en">Design tokens</span> knyter ihop designbeslut
                som annars skulle sakna en tydlig relation. Om en designers
                skiss och en utvecklares implementation båda refererar till
                samma <span lang="en">design token</span> så kan man känna sig
                trygg i att t.ex. samma färg används på båda ställena. Skulle
                värdet på färgen ändras så kan man också känna sig trygg i att
                det uppdateras för båda. Man skulle kunna säga att det är ett
                språk för designbeslut och att vi alla kan prata samma språk
                oavsett roll.
              </p>
              <h2>
                Globala <span lang="en">tokens</span>
              </h2>
              <p>
                Genom att använda CSS-filen{' '}
                <digi-code afCode="@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css"></digi-code>{' '}
                får du tillgång till en handfull anpassade CSS-egenskaper på{' '}
                <span lang="en">:root-scope.</span>
                Dessa följer denna namngivningskonvention:{' '}
                <digi-code afCode="--digi--global--category--token-name--variation"></digi-code>
                <br />
                <br />
                De flesta <span lang="en">tokens</span> kan grupperas i en av
                följande kategorier:
              </p>
              <digi-list>
                <span lang="en">
                  <li>Animation</li>
                  <li>Border</li>
                  <li>Color</li>
                  <li>Layout</li>
                  <li>Typography</li>
                </span>
              </digi-list>
              Exempelvis ser några grundläggande <span lang="en">tokens</span>{' '}
              för teckenstorlek ut så här:
              <br />
              <br />
              <digi-code af-code="--digi--global--typography--font-size--base"></digi-code>{' '}
              <br />
              <digi-code af-code="--digi--global--typography--font-size--small"></digi-code>{' '}
              <br />
              <digi-code af-code="--digi--global--typography--font-size--large"></digi-code>{' '}
              <br />
              <br />
              Och färger kan se ut så här:
              <br />
              <br />
              <digi-code afCode="--digi--global--color--profile--white--base"></digi-code>
              <br />
              <digi-code afCode="--digi--global--color--function--info--base"></digi-code>
              <br />
              <digi-code afCode="--digi--global--color--neutral--grayscale--base"></digi-code>
              <br />
              <br />
              Dessa variabler är alltid tillgängliga att använda eftersom de är
              en del av :root (det vill säga på HTML-elementet).
              <h2>
                Alias <span lang="en">tokens</span>
              </h2>
              <p>
                Som namnet antyder så är dessa <span lang="en">tokens</span> ett
                alias för de globala <span lang="en">tokens</span>
                som finns. Dessa hjälper till med att förtydliga sammanhang och
                intention. Alla globala <span lang="en">tokens</span> är unika
                men alias <span lang="en">tokens</span> kan ha samma värde -
                till exempel så kan vit färg användas både som inverterad
                textfärg eller som primär bakgrundsfärg. Dessa{' '}
                <span lang="en">tokens</span> hänvisar då till namnet på det
                globala <span lang="en">token</span> som används, inte till dess
                värde direkt. <br />
                <br />
                <digi-code af-code="--digi--color--background--primary: var(--digi--global--color--profile--white--base)"></digi-code>{' '}
                <br />
                <digi-code af-code="--digi--color--text--inverted: var(--digi--global--color--profile--white--base)"></digi-code>{' '}
                <br />
              </p>
              <h2>Tokens för komponenter</h2>
              <p>
                Varje enskild komponent har vanligtvis också en uppsättning av
                komponentspecifika <span lang="en">tokens</span>. Dessa följer
                denna namnkonvention:
                <br />
                <br />
                <digi-code afCode="--component--name--token-name--variation"></digi-code>
                <br />
                <br />
                Dessa är inte tillgängliga globalt, men definieras i själva
                komponenten (och finns därför inte i dokumentet förrän
                komponenten används). Dessa bör i de flesta fall baseras på
                alias <span lang="en">tokens</span>.
                <br />
                <br />
                Till exempel ser några <span lang="en">tokens</span> från
                digi-knappen ut så här:
                <br />
                <br />
                <digi-code afCode="--digi--button--color--text--primary--default: var(--digi--color--text--inverted)"></digi-code>
                <br />
                <digi-code afCode="--digi--button--color--background--primary--default: var(--digi--color--background--inverted-1)"></digi-code>
                <br />
                <br />
                Se varje komponents individuella dokumentation för att se vilka
                variabler som är tillgängliga.
              </p>
              <h2>
                Använda <span lang="en">tokens</span>
              </h2>
              <p>
                Eftersom dessa är anpassade CSS-egenskaper, fungerar de som
                förväntat. De tillåter också flexibilitet när systemet
                implementeras. Genom att kombinera globala variabler och
                komponentvariabler på smarta sätt kan du ändra standardutseendet
                med bara några få ändringar.
              </p>
              <digi-list>
                <li>
                  Undvik att läsa in stora CSS-filer om enbart någon enstaka
                  variabel behövs genom att sätta ett klassnamn på
                  HTML-elementet och använd dig av variabler för att få önskat
                  utseende. Exempel nedan för att sätta en marginal nedåt med en
                  variabel:
                  <br />
                  <digi-code-block
                    af-code="
.my-class {
  margin-bottom: var(--digi--margin--medium);
}
                    "
                    afLanguage={CodeBlockLanguage.CSS}
                    af-language={CodeBlockLanguage.CSS}></digi-code-block>
                </li>
                <li>
                  Använd CSS-variabler i första hand och undvik sass-variabler i
                  den mån det går.
                </li>
                <li>
                  Undvik användning av pixelvärden i CSS:en, använd istället
                  rem-värden via importering av rem-funktionen som kan användas
                  enligt exempel nedan:
                  <digi-code-block
                    af-code='
@use "@digi/styles/src/functions/rem" as *;

.my-class {
  margin-bottom: #{rem(16)}
}
                    '
                    afLanguage={CodeBlockLanguage.CSS}
                    af-language={CodeBlockLanguage.CSS}></digi-code-block>
                  16 motsvarar 16px. Detta är endast ett exempel för att visa
                  hur funktionen fungerar. Bättre lösning i detta fall hade
                  varit att använda en <span lang="en">token</span> för
                  marginalen:{' '}
                  <digi-code af-code="margin-bottom: var(--digi--margin--medium);"></digi-code>
                </li>
              </digi-list>
            </digi-layout-block>
          </digi-docs-page-layout>
        </digi-docs-page-layout>
      </div>
    );
  }
}
