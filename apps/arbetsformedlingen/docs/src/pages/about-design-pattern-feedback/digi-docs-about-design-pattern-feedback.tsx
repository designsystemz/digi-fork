import { Component, h, Prop, Listen, getAssetPath } from '@stencil/core';
import Helmet from '@stencil/helmet';
import state from '../../store/store';
import { Element } from '@stencil/core';
import { router } from '../../global/router';
import type { HTMLStencilElement } from '@stencil/core/internal';

@Component({
  tag: 'digi-docs-about-design-pattern-feedback',
  styleUrl: 'digi-docs-about-design-pattern-feedback.scss',
  scoped: true
})
export class AboutDesignPatternFeedback {
  @Element() hostElement: HTMLStencilElement;
  Router = router;
  @Prop() pageName = 'Enkäter och feedbackfunktioner';

  private _feedback;
  private _feedback2;
  private _feedback3;
  private _button;

  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  componentDidRender() {
    if (this._feedback || this._feedback2) {
      this._feedback.afMSetStep(2);
      this._feedback2.afMSetStep(3);
    }

    this._button = this.hostElement.querySelectorAll('button');
    if (this._button) {
      this._button.forEach(
        (element) => ((element as HTMLButtonElement).disabled = true)
      );
    }
  }

  //Används inte just nu
  connectedCallback() {
    customElements.whenDefined('digi-tools-feedback-rating').then(() => {
      this._feedback3.afMSetStep(2);
    });
  }

  @Listen('afOnClick')
  clickHandler(e) {
    if (
      e.target.matches('.digi-tools-feedback-banner__links digi-link-external')
    ) {
      e.detail.preventDefault();
    }
  }
  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-design-pattern-feedback/digi-docs-about-design-pattern-feedback.tsx">
          <Helmet>
            <meta
              property="rek:pubdate"
              content="Wed Aug 30 2023 12:55:14 GMT+0200 (Central European Summer Time)"
            />
          </Helmet>
          <span slot="preamble">
            Feedbackkomponenter är viktiga för att förbättra
            användarupplevelsen, minska risk, öka insikterna i användarnas behov
            och preferenser samt öka användarengagemang och lojalitet till din
            produkt eller tjänst. Genom att samla in feedback från användarna
            kan du identifiera problem och förbättringsområden tidigt i
            utvecklingsprocessen och undvika kostsamma ombyggnader.
          </span>
          <br />
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            af-no-gutter
            afMarginTop>
            <h2>Moduler för återkoppling</h2>
            <p>
              Det finns idag två olika feedbackmoduler för att göra det enkelt
              för användare att skicka in synpunkter till oss. Först har vi en
              banner som direkt länkar till underliggande enkätformulär. Utöver
              bannern har vi en enklare enkätmodul som kan placeras direkt i
              gränssnittet, vilket gör att användare snabbt och enkelt kan
              skriva sina reaktioner och ge feedback.
            </p>
            <br />
          </digi-layout-block>
          <digi-layout-block afVerticalPadding af-nogutter afMarginTop>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <h2>Banner till dina enkätundersökningar</h2>
                <p>
                  Med en standardiserad modul med enhetligt utseende önskar vi
                  skapa igenkänning och en enkel väg för användaren att skicka
                  in feedback till oss. I modulen kan du antingen använda den
                  befintliga copyn, eller ändra efter tjänstens behov och lägga
                  till länkar till dina underliggande enkäter eller exempelvis
                  egna sidor med frågeformulär. I exemplet ser du två olika
                  länkar för att rikta feedbacken till olika kanaler, beroende
                  på återkopplingens typ och önskad segmentering av enkätsvaren.
                  Defaultplaceringen av feedback-banner är i slutet av sidan,
                  direkt ovanför sidfoten. Här hittar du komponenten för{' '}
                  <digi-link
                    afHref="/komponenter/digi-tools-feedback-banner/oversikt"
                    afTarget="_blank"
                    af-variation="small"
                    onAfOnClick={(e) => this.linkClickHandler(e)}>
                    feedback-banner-modul
                  </digi-link>
                  .
                </p>
              </div>
              <div style={{ 'margin-top': 'var(--digi--margin--larger)' }}>
                <digi-tools-feedback-banner>
                  <a id="link1" href=" ">
                    Tyck till om tjänsten
                  </a>
                  <a id="link2" href=" ">
                    Rapportera tekniskt fel
                  </a>
                </digi-tools-feedback-banner>
                <p class="smaller">
                  * Använd befintlig copy, eller anpassa till din tjänsts
                  önskade budskap. Ta bort eller lägg till länkar. Det går även
                  att lägga in egna objekt i komponenten om det t.ex. behöver
                  läggas till disclaimer eller liknande info.
                </p>
              </div>
            </digi-layout-columns>
          </digi-layout-block>
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            af-no-gutter
            afMarginTop></digi-layout-block>
          <digi-layout-block afVerticalPadding af-nogutter afMarginTop>
            <h2>Feedback-modul</h2>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <p>
                  Vi vill alltid förbättra vår webbplats och ge besökarna den
                  bästa upplevelsen. Feedback hjälper oss att identifiera vad
                  som fungerar bra och vad som kan utvecklas. Vår feedbackmodul
                  kan populeras med olika funktioner, beroende på
                  frågeställarens behov. Tex kan modulens innehållssteg
                  innehålla fritextfält, radioknappar, epostfält eller andra
                  komponenter efter eget önskemål. Komponenten finns även i en
                  mer tillbakadragen (sekundär) version rent visuellt, då den
                  inte har den identitetsskapande gröna bakgrundsfärgen. Detta
                  för att göra det enklare att lägga den i anslutning till
                  specifika interaktions-objekt.
                </p>
                <p>
                  Läs allt användaren skickar in oavsett om det är kommentarer,
                  idéer eller förbättringsförslag. Detta är det ett
                  grundläggande beslutsmaterial, vi tar alltid användarens
                  feedback på allvar och håller den konfidentiell. Läs mer om
                  hur vi hanterar personuppgifter.
                </p>
                <p>
                  Feedbackkomponenten bör användas regelbundet för att samla in
                  feedback från användarna, så att eventuella problem eller
                  förbättringsförslag kan upptäckas och hanteras snabbt. Det är
                  viktigt att ge återkoppling till användarna om vilka åtgärder
                  som vidtas som svar på deras feedback. Detta kan bidra till
                  att bygga förtroende och öka användarnas engagemang.
                </p>
              </div>
              <div>
                <h3>Introduktionsvy</h3>
                {/* <digi-tools-feedback
									af-text="Hjälpte informationen dig?"
									af-variation="primary"
									af-type="fullwidth"
									af-is-done="false"
								>
								</digi-tools-feedback> */}

                <digi-tools-feedback
                  af-text="Hjälpte informationen dig?"
                  af-variation="primary"
                  af-type="fullwidth"
                  af-is-done="false"></digi-tools-feedback>
                <h3>Innehållsvy</h3>
                <digi-tools-feedback
                  ref={(el) => (this._feedback = el)}
                  af-text="Hjälpte informationen dig?"
                  af-variation="primary"
                  af-type="fullwidth"
                  af-is-done="false">
                  <form slot="form">
                    <digi-form-textarea
                      afLabel="Skriv din feedback här"
                      af-variation="medium"
                      af-validation="neutral"></digi-form-textarea>
                    <digi-button
                      af-size="small"
                      af-variation="secondary"
                      af-full-width="false">
                      Skicka
                    </digi-button>
                  </form>
                </digi-tools-feedback>
                <br />
              </div>
            </digi-layout-columns>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <h3>Feedbackmodulen består av flera steg</h3>
                <h4>Introduktionsvy</h4>
                <p>
                  Först inleds modulen med en fråga om att ge feedback på
                  tjänsten eller sidans innehåll, användaren får två val. "Ja"
                  eller "Nej".{' '}
                </p>
                <h4>Innehållsvy</h4>
                <p>
                  Därefter kan enkätkonstruktören ställa sina frågor och har ett
                  antal olika funktioner tillgängliga, såsom fritextfält,
                  radioknappar, e-postfält och andra komponenter, för att göra
                  det enklare för deltagarna att ge detaljerade och användbara
                  svar.
                  <br />
                  (Tänk på att inte göra denna enkät för komplex. Behövs det en
                  mer djuplodande enkät med många olika svarsalternativ så är
                  det bättre att bygga en underliggande enkät får det enskilda
                  ändamålet)
                </p>
                <h4>Kvittensvy</h4>
                <p>
                  Vi tackar användaren för deras medverkan, med en visuell och
                  textmässig kvittens.
                </p>
              </div>
              <div>
                <digi-form-fieldset
                  af-form="radioknappsexempel"
                  af-name="radioknappsexempel">
                  <div class="radioButtonBox">
                    <digi-form-radiobutton
                      afLabel="Kategori 1"
                      af-variation="primary"
                      af-value="Kategori 1"
                      afName="Myradiobuttons"></digi-form-radiobutton>
                    <digi-form-radiobutton
                      afLabel="Kategori 2"
                      af-variation="primary"
                      af-value="Kategori 2"
                      afName="Myradiobuttons"></digi-form-radiobutton>
                    <digi-form-radiobutton
                      afLabel="Annan information"
                      af-variation="primary"
                      af-value="Annan information"
                      afName="Myradiobuttons"></digi-form-radiobutton>
                  </div>
                  <br />
                  <div class="textBox">
                    <digi-form-input
                      afLabel="Lämna din e-post här om du vill ha återkoppling via mail"
                      af-variation="small"
                      af-type="text"
                      af-validation="neutral"
                      af-inputmode="text"></digi-form-input>
                  </div>
                  <p class="smaller">
                    * Innehållssteget går att populera med önskade komponenter
                    såsom fritextfält, radioknappar, e-postfält osv. Använd
                    kategorier för att strukturera inkommande feedback för att
                    förenkla hanteringen av data.
                  </p>
                  <br />
                  <div>
                    <h3>Kvittensvy på utförd återkoppling</h3>
                    <digi-tools-feedback
                      ref={(el) => (this._feedback2 = el)}
                      af-text="Hjälpte informationen dig?"
                      af-variation="primary"
                      af-type="fullwidth"></digi-tools-feedback>
                  </div>
                </digi-form-fieldset>
              </div>
            </digi-layout-columns>
          </digi-layout-block>
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            af-no-gutter
            afMarginTop></digi-layout-block>
          <digi-layout-block afVerticalPadding af-nogutter afMarginTop>
            <h2>Betygskomponent</h2>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <p>
                  Betygskomponenten kan hjälpa dit team att snabbt få
                  återkoppling på funktioner, flöden och tjänster med mera.
                  Betygsmodulen är förberedd att skicka statistik till vårat
                  statistikverktyg som är uppsatt med ett antal förutbestämda
                  standard-punkter, för att du och organisationen som helhet ska
                  kunna se positiva och negativa förändringar över tid på ett
                  samordnat sätt. Fördjupad information om{' '}
                  <digi-link
                    afHref="/komponenter/digi-tools-feedback-rating/oversikt"
                    af-href="/komponenter/digi-tools-feedback-rating/oversikt"
                    af-variation="small">
                    teknisk implemenation
                  </digi-link>{' '}
                  hittar du på komponentsidan.
                </p>
                <p>
                  I komponenten går det att byta rubrikfråga. Fältet tillåter
                  upp till 70-tecken men försök att hålla frågan så kort och
                  koncis som möjligt. Komponenten finns som en färdig banner,
                  men även som en bas-variant utan rubrik som kan vara enklare
                  att implementera i mer komplicerad layouter.
                </p>
                <h3>Fem steg</h3>
                <p>
                  Användaren har fem standard-steg med betygsstjärnor att välja
                  bland, från mycket missnöjd, missnöjd, varken eller, nöjd och
                  mycket nöjd. Om du använder baskomponenten och ändrar i
                  etiketten så måste det framgå av annan tillhörande text vad
                  det är användaren bedömmer och vilka ytterligheter i
                  betygsskalan som används (innan användaren interagerat med
                  stjärnorna). Detta med tanke på tillgänglighet och
                  användarkontroll.
                </p>
                <digi-tools-feedback-rating
                  af-id="rating3"
                  af-question="Hur hjälpsam var tjänsten?"
                  af-type="minimal"
                  af-ratings="Mycket missnöjd;Ganska nöjd;Helt okej;Ganska nöjd;Mycket nöjd"></digi-tools-feedback-rating>
                <h3>Underliggande enkät</h3>
                <p>
                  Komponenten har också en funktion i kvittens-steget, där man
                  kan länka vidare till ett enkätverktyg för att erbjuda
                  användaren ett sätt att skriva mer fördjupad feedback, eller
                  på annat sätt förmedla mer detaljerad information om vad hen
                  ger betyg på.
                </p>
              </div>
              <div>
                <h3>Introduktionsvy</h3>
                <digi-tools-feedback-rating
                  af-id="rating1"
                  af-question="Hur hjälpsam var tjänsten?"
                  af-variation="primary"
                  af-type="grid"
                  af-ratings="Mycket missnöjd;Ganska nöjd;Helt okej;Ganska nöjd;Mycket nöjd"></digi-tools-feedback-rating>
                <h3>Kvittensvy på utförd återkoppling</h3>
                <digi-media-image
                  afUnlazy
                  af-src={getAssetPath('/assets/images/feedback-rating.png')}
                  afAlt="Betygskomponenten visar tack-meddelandet"></digi-media-image>
              </div>
            </digi-layout-columns>
          </digi-layout-block>
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            af-no-gutter
            afMarginTop>
            <h2>Fråga, mät och iterera</h2>
            <p>
              Få värdefull feedback från användarna om deras upplevelse på sidan
              och öka svarsfrekvensen genom att erbjuda enkla och tydliga
              alternativ som passar webbplatsens syfte och placera dem på
              lämpliga ställen på sidan.
            </p>
            <p>
              Att mäta och strukturera statistiken från enkätundersökningar är
              avgörande för att kunna identifiera mönster och trender, och
              därmed göra informerade beslut om hur man kan förbättra
              användarupplevelsen på sin webbplats. Genom att följa upp tidigare
              åtgärder baserade på ny feedback kan man se hur användarnas
              synpunkter och behov förändras över tid.
            </p>
            <p>
              Förutom att samla in feedback från användarna, kan den också
              hjälpa till att skapa ett engagemang genom att uppmuntra
              användarna att interagera och ge sin åsikt.
            </p>
            <p>
              För att upprätthålla användarnas förtroende och se till att
              återkopplingen inte missbrukas är det viktigt att ha en tydlig och
              transparent policy kring hur feedbacken kommer att användas. Detta
              kan inkludera att endast använda anonymiserad feedback för att
              skydda användarnas integritet.
              <br />
              <br />
            </p>
          </digi-layout-block>
        </digi-docs-page-layout>
      </host>
    );
  }
}
