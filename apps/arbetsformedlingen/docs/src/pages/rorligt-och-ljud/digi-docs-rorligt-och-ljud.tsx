import { Component, h, getAssetPath, Prop } from '@stencil/core';
import Helmet from '@stencil/helmet';
import { InfoCardHeadingLevel } from '@digi/arbetsformedlingen';
import { router } from '../../global/router';
import { RorligtLjudExampleType } from './rorligt-ljud-example/rorligt-ljud-example-type.enum';
import state from '../../store/store';

@Component({
  tag: 'digi-docs-rorligt-och-ljud',
  styleUrl: 'digi-docs-rorligt-och-ljud.scss',
  scoped: true
})
export class RorligtOchLjud {
  @Prop() pageName = 'Rörligt och ljud';
  Router = router;
  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/rorligt-och-ljud/digi-docs-rorligt-och-ljud.tsx">
          <Helmet>
            <meta
              property="rek:pubdate"
              content="Wed Jun 28 2023 11:04:22 GMT+0200 (Central European Summer Time)"
            />
          </Helmet>
          <span slot="preamble">
            Film är ett effektivt men resurskrävande sätt att kommunicera.
            Därför är det kommunikationsavdelningen som avgör om film är rätt
            verktyg för att lösa ditt kommunikationsbehov.
          </span>
          <digi-layout-container af-margin-bottom af-margin-top>
            <h2>När filmar vi?</h2>
            <digi-list af-list-type="bullet">
              <li>När din historia förstärks av att berättas med bilder.</li>
              <li>När du vill förmedla en känsla mer än fakta.</li>
              <li>När du vill ge tittaren en autentisk upplevelse.</li>
              <li>
                När du vill visa tittaren i hur en tjänst eller funktion
                fungerar.
              </li>
            </digi-list>
            <h2>När filmar vi inte?</h2>
            <p>
              Fakta gör sig bättre i text. Om din film bara är en uppläst
              version av en text så fyller den ingen funktion. Vi undviker också
              rent dokumenterande, som att filma seminarier, föreläsningar och
              liknande. Det blir sällan bra, eftersom ljus och ljud ofta är för
              dåligt för att fungera på film.
            </p>
            <h2>Filmer ska vara tillgängliga</h2>
            <p>
              All film som myndigheten publicerar ska vara undertextad och i
              vissa fall även syntolkad. Detta gäller även internt och oavsett
              plattform (Sharepoint, intranätet med mera). På webbplatsen
              Vägledning för webbutveckling finns information om hur du
              praktiskt går till väga för att skapa tillgängliga filmer.
            </p>
            <h2>Lagring</h2>
            <p>
              Vi har ett verktyg för att hantera film och ljudfiler som heter
              Quickchannel.
            </p>
            <digi-info-card
              afHeading="Checklista"
              afHeadingLevel={InfoCardHeadingLevel.H2}>
              <digi-list af-list-type="numbered">
                <li>
                  Inled med text och grafik som tydligt visar filmens syfte och
                  uppdrag.
                </li>
                <li>
                  Presentera dig själv och samtliga medverkande med namnskylt
                  (namn och titel).
                </li>
                <li>
                  Använd helst en extern mikrofon eftersom kamerans eller
                  mobilens inbyggda mikrofon har begränsad upptagningsförmåga om
                  inte personen är väldigt nära kameran eller telefonen. Tänk
                  också på att tala tydligt och att manuset är på klarspråk.
                </li>
                <li>
                  Se till att personer som syns framför kameran är på en väl
                  upplyst plats. Undvik motljus för att slippa effekten av att
                  subjekten blir ”silhuetter”.
                </li>
                <li>Avsluta tydligt med logotypskylt.</li>
              </digi-list>
            </digi-info-card>
            <br />
            <h2>Exempel på enheter</h2>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <h3>Grafiska element</h3>
                <p>
                  Namnskylten är en gemensam nämnare i vår rörliga
                  kommunikation. Använd typsnittet Open sans.
                </p>
                <p>
                  Namn: 28 pixlar <br /> Befattning: 23 pixlar{' '}
                </p>
                <digi-media-image
                  class="digi-docs-rorligt-och-ljud__media-image-shadow"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/rorligt-ljud-image/namnskylt-exempel.png'
                  )}
                  afAlt="Exempel på namnskylt"></digi-media-image>
              </div>
              <div>
                <h3>
                  Visualisering av färgschema och mängdvolym av respektive färg
                </h3>
                <p>
                  Vid framtagningen av Grafik bör man tänka på denna palett och
                  färgmängd. Våra primära färger ska dominera.
                </p>
                <br />
                <digi-media-image
                  class="digi-docs-rorligt-och-ljud__media-image"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/rorligt-ljud-image/visualisering-fargschema.png'
                  )}
                  afAlt="Visualisering av färgschema och mängdvolym av respektive färg"></digi-media-image>
              </div>
            </digi-layout-columns>
            <br />
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <h3>Exempel på grafikplattor, 100% av ytan</h3>
                <digi-media-image
                  class="digi-docs-rorligt-och-ljud__media-image"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/rorligt-ljud-image/grafikplattor-100-bla.png'
                  )}
                  afAlt="Exempel på en blå grafisk platta som tar 100% av ytan"></digi-media-image>
                <digi-media-image
                  class="digi-docs-rorligt-och-ljud__media-image"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/rorligt-ljud-image/grafikplattor-100-vit.png'
                  )}
                  afAlt="Exempel på en vit grafisk platta som tar 100% av ytan"></digi-media-image>
              </div>
              <div>
                <h3>Exempel på grafikplattor, 80% av ytan</h3>
                <digi-media-image
                  class="digi-docs-rorligt-och-ljud__media-image"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/rorligt-ljud-image/grafikplattor-80-bla.png'
                  )}
                  afAlt="Exempel på en blå grafisk platta som tar 80% av ytan"></digi-media-image>
                <digi-media-image
                  class="digi-docs-rorligt-och-ljud__media-image"
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/rorligt-ljud-image/grafikplattor-80-vit.png'
                  )}
                  afAlt="Exempel på en vit grafisk platta som tar 80% av ytan"></digi-media-image>
              </div>
            </digi-layout-columns>
            <p>
              Grafikplattor mot profilblå och vit bakgrund ser ut så här. Skriv
              en kort och enkel text, helst endast en rad. Du kan förminska
              grafikplattan till 80 % av skärmen.
            </p>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <digi-media-image
                class="digi-docs-rorligt-och-ljud__media-image"
                afUnlazy
                afSrc={getAssetPath(
                  '/assets/images/rorligt-ljud-image/grafikplattor-bla.png'
                )}
                afAlt="Exempel på en blå grafisk platta"></digi-media-image>
              <digi-media-image
                class="digi-docs-rorligt-och-ljud__media-image"
                afUnlazy
                afSrc={getAssetPath(
                  '/assets/images/rorligt-ljud-image/grafikplattor-vit.png'
                )}
                afAlt="Exempel på en blå grafisk platta"></digi-media-image>
            </digi-layout-columns>
            <br />
            <br />
            <digi-layout-block af-variation="secondary" af-vertical-padding>
              <digi-typography>
                <h2>Länkar till mer information och hjälpmedel</h2>
                <h3>Rörligt</h3>
                <p>
                  Frågor om behörigheter till Quickchannel besvaras av
                  kommunikationsavdelningen.
                </p>
                <h3>Tillgänglighet</h3>
                <p>
                  Checklista för tillgänglighet:{' '}
                  <digi-link
                    afHref="/tillganglighet-och-design/tillganglighet-checklista"
                    af-variation="small"
                    onAfOnClick={(e) => this.linkClickHandler(e)}>
                    Tillgänglighetslistan{' '}
                  </digi-link>
                </p>
                <h3>Undertextning</h3>
                <p>
                  Mejla frågor om undertextning till:{' '}
                  <digi-link
                    af-variation="small"
                    afHref="mailto:undertextning@arbetsformedlingen.se?body=Beskriv%20ditt%20ärende%20så%20återkommer%20vi%20så%20snart%20vi%20kan">
                    <digi-icon-envelope slot="icon"></digi-icon-envelope>
                    Kontakta undertextning
                  </digi-link>
                </p>
                <h3>Kontrastvärden</h3>
                <p>
                  Checka av kontrastvärde:{' '}
                  <digi-link-external
                    afHref="https://contrastchecker.com"
                    af-target="_blank"
                    af-variation="small">
                    Öppna contrastchecker (contrastchecker.com, öppnas i egen
                    flik)
                  </digi-link-external>
                </p>
                <h3>Mer om riktlinjer och produktioner</h3>
                <p>
                  Vill du veta mer om riktlinjer för film och grafik? Kontakta:{' '}
                  <digi-link
                    af-variation="small"
                    afHref="mailto:kommunikationsavdelningen@arbetsformedlingen.se?body=Beskriv%20ditt%20ärende%20så%20återkommer%20vi%20så%20snart%20vi%20kan">
                    <digi-icon-envelope slot="icon"></digi-icon-envelope>
                    kommunikationsavdelningen@arbetsformedlingen.se
                  </digi-link>
                </p>
                <p>
                  Mer information om produktioner:{' '}
                  <digi-link-external
                    afHref="https://space.arbetsformedlingen.se/sites/streamingpaarbetsformedlingenscreen9/SitePages/Home.aspx"
                    af-target="_blank"
                    af-variation="small">
                    Öppna Quickchannel (öppnas i egen flik){' '}
                  </digi-link-external>{' '}
                  Intern länk endast för medarbetare på Arbetsförmedlingen.
                </p>
              </digi-typography>
            </digi-layout-block>
            <br />
            <br />
            <h3>Bra exempel på film och rörlig media</h3>
            <p>
              Exempel på produktioner som tagits fram. Filmer ska alltid följa
              vårt bildspråk och kännas äkta.
            </p>
            <digi-docs-rorligt-ljud-example>
              <digi-layout-columns af-variation={state.responsiveTwoColumns}>
                <div>
                  <p>
                    <b>Webbinarium</b>
                  </p>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/webbinarium-exempel1.png'
                    )}
                    afAlt="Utdrag från ett webbinarium, bild från video"></digi-media-image>
                  <br />
                </div>
                <div>
                  <p>
                    <b>Externt program "Tema"</b>
                  </p>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/externt-program-exempel.png'
                    )}
                    afAlt="Uttdrag från externa programmet 'Tema', bild från video"></digi-media-image>
                  <br />
                </div>
                <div>
                  <p>
                    <b>Fråga oss live</b>
                  </p>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/fraga-live-exempel.png'
                    )}
                    afAlt="Grafikplatta från 'Fråga oss live', bild från video"></digi-media-image>
                  <br />
                </div>
                <div>
                  <p>
                    <b>Internt program "På jobbet"</b>
                  </p>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/internt-program-exempel.png'
                    )}
                    afAlt="Utdrag från interna programmet 'På jobbet', bild från video"></digi-media-image>
                  <br />
                </div>
                <div>
                  <p>
                    <b>Webbinarium</b>
                  </p>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/webbinarium-exempel2.png'
                    )}
                    afAlt="Utdrag från ett webbinarium, bild från video"></digi-media-image>
                  <br />
                </div>
                <div>
                  <p>
                    <b>Film</b>
                  </p>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/film-exempel.png'
                    )}
                    afAlt="Utdrag från en film, bild från video"></digi-media-image>
                  <br />
                </div>
                <div>
                  <p>
                    <b>Animation</b>
                  </p>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/animation-exempel1.png'
                    )}
                    afAlt="Utdrag från en arbetsförmedlingen animation, bild från video"></digi-media-image>
                  <br />
                </div>
                <div>
                  <p>
                    <b>Animation</b>
                  </p>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/animation-exempel2.png'
                    )}
                    afAlt="Utdrag från en arbetsförmedlingen animation, bild från video"></digi-media-image>
                  <br />
                </div>
              </digi-layout-columns>
              <p>
                <b>Animering</b> <br />
                Vi ska använda vår infografik vid animeringar. Detta för att
                underlätta rent pedagogiskt, tekniskt och för att hålla det
                enhetligt.{' '}
              </p>
              <digi-layout-columns af-variation={state.responsiveTwoColumns}>
                <div>
                  <p>
                    <b>Slutplatta vit</b>
                  </p>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/slutplatta-profilvit.png'
                    )}
                    afAlt="Exempel på vit slutplatta, bild från video"></digi-media-image>
                </div>
                <div>
                  <p>
                    <b>Slutplatta profilblå</b>
                  </p>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/slutplatta-profilbla.png'
                    )}
                    afAlt="Exempel på blå slutplatta, bild från video"></digi-media-image>
                </div>
              </digi-layout-columns>
            </digi-docs-rorligt-ljud-example>
            <h3>Dåliga exempel</h3>
            <p>
              Här är några exempel på hur vi <strong>inte</strong> ska hantera
              plattor och logotyp i kontext.
            </p>
            <digi-docs-rorligt-ljud-example
              exampleType={RorligtLjudExampleType.BAD}>
              <digi-layout-columns af-variation={state.responsiveTwoColumns}>
                <div>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/genomskinligplatta-exempel1.png'
                    )}
                    afAlt="Exempel på hur genomskinlig platta inte ska användas, bild från video"></digi-media-image>
                  <p>
                    Använd <strong>inte</strong> en genomskinlig platta
                  </p>
                  <br />
                </div>
                <div>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/genomskinligplatta-exempel2.png'
                    )}
                    afAlt="Exempel på hur genomskinlig platta inte ska användas, bild från video"></digi-media-image>
                  <p>
                    Använd <strong>inte</strong> en genomskinlig platta
                  </p>
                  <br />
                </div>
                <div>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/plattasida-exempel.png'
                    )}
                    afAlt="Exempel på hur en platta inte ska användas på sidan, bild från video"></digi-media-image>
                  <p>
                    Använd <strong>inte</strong> platta på sidan
                  </p>
                  <br />
                </div>
                <div>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/genomskinligplatta-exempel3.png'
                    )}
                    afAlt="Exempel på hur genomskinlig platta inte ska användas på sidan, bild från video"></digi-media-image>
                  <p>
                    Använd <strong>inte</strong> en genomskinlig platta på sidan
                  </p>
                  <br />
                </div>
                <div>
                  <digi-media-image
                    class="digi-docs-rorligt-och-ljud__media-image"
                    afUnlazy
                    afSrc={getAssetPath(
                      '/assets/images/rorligt-ljud-image/logokontrast-exempel.png'
                    )}
                    afAlt="Exempel på hur arbetsförmedlingen logotypen inte ska användas med låg kontrast, bild från video"></digi-media-image>
                  <p>
                    Logotypen har <strong>inte</strong> tillräcklig kontrast mot
                    bakgrunden
                  </p>
                </div>
              </digi-layout-columns>
            </digi-docs-rorligt-ljud-example>
            <br />
            <h1>Ljud</h1>
            <digi-typography-preamble>
              Arbetsförmedlingen producerar stora mängder material som i en
              eller annan form omfattar ljud. Det rör sig om allt från film,
              webbinarier och poddar till kompetensförsörjning, intervjuer,
              debatter och yrkesporträtt inom intern och extern tv-produktion.
            </digi-typography-preamble>
            <p>
              För att få enhetligt ljud i våra kanaler så ska vi förmedla att vi
              är Arbetsförmedlingen. Detta genom bland annat valda ljudfiler som
              tagits fram. Även så det känns äkta.
              <br />
              Produktionen ska kopplas till produktionens karaktär. Valet av
              VO-röster också mellan manligt respektive kvinnligt samt ålder och
              härkomst. Vi ska undvika olika ljudeffekter om inte produktionen
              verkligen kräver effekt.
            </p>
            <h2>Att tänka på</h2>
            <digi-list af-list-type="bullet">
              <li>
                Kön, ålder, eventuell dialekt eller brytning – vad passar bäst?{' '}
              </li>
              <li>En eller flera röster – är de samspelta och tydliga?</li>
              <li>Dynamik</li>
              <li>Effekter, frekvenser och volym.</li>
              <li>Enkla melodier, lätt att komma ihåg.</li>
              <li>Skillnader i antal toner.</li>
              <li>
                Rytm, tonart, harmoni. håll det enkelt. När och hur musik
                används – är det i kontext?
              </li>
              <li>
                Instrumentering, harmonier och tempo. Rätt frekvenser till rätt
                kanal?
              </li>
              <li>Äkthet och seriösitet.</li>
            </digi-list>
            <digi-info-card
              afHeading="Checklista för vad vi vill förmedla"
              afHeadingLevel={InfoCardHeadingLevel.H2}>
              <p>
                Proffsighet, förtroende, enkelt och okomplicerat och mänsklighet
              </p>
              <digi-list af-list-type="numbered">
                <li>
                  Återspegla de olika känslor som finns i till exempel det
                  filmade materialet, förstärk det genom ljudsättning. På ett
                  sätt som får det att hänga ihop.
                </li>
                <li>
                  En variation av tempon för att motsvara olika typer av
                  handling.
                </li>
                <li>
                  Neutrala ljudspår som kan kombineras med många olika typer av
                  innehåll som till exempel information, yrkesporträtt och
                  intervjuer. En allt för dramatisk musik skulle kännas märklig.
                </li>
                <li>
                  Undvika högfrekventa ljud eller annat som tar över på oönskat
                  sätt eller krockar med voice overn.
                </li>
                <li>
                  Håll det enkelt eftersom komplicerad musik skapar ett
                  komplicerat intryck.
                </li>
                <li>
                  Bearbeta musiken för att passa digitala kanaler eller
                  ljudkällor i mobil och dator.
                </li>
              </digi-list>
            </digi-info-card>
            <br />
            <br />
            <digi-layout-block af-variation="secondary" af-vertical-padding>
              <digi-typography>
                <h3>Hjälp</h3>
                <p>
                  Vill du veta mer om riktlinjer för ljud, och få access till
                  ljudfiler?
                  <br /> Kontakta:{' '}
                  <digi-link
                    af-variation="small"
                    afHref="mailto:kommunikationsavdelningen@arbetsformedlingen.se?body=Beskriv%20ditt%20ärende%20så%20återkommer%20vi%20så%20snart%20vi%20kan">
                    <digi-icon-envelope slot="icon"></digi-icon-envelope>
                    kommunikationsavdelningen@arbetsformedlingen.se
                  </digi-link>
                </p>
              </digi-typography>
            </digi-layout-block>
          </digi-layout-container>
        </digi-docs-page-layout>
      </host>
    );
  }
}
