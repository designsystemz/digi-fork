import { Component, h, getAssetPath } from '@stencil/core';

@Component({
  tag: 'digi-docs-the-brand-employment-service',
  styleUrl: 'digi-docs-the-brand-employment-service.scss',
  scoped: true
})
export class DigiDocsTheBrandEmploymentService {
  render() {
    return (
      <div class="digi-docs-the-brand-employment-service">
        <digi-docs-page-layout af-edit-href="pages/home/digi-docs-the-brand-employment-service/digi-docs-the-brand-employment-service.tsx">
          <digi-typography>
            <digi-layout-block afMarginBottom>
              <digi-typography-heading-jumbo af-text="Varumärket Arbetsförmedlingen"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                Varumärket Arbetsförmedlingen manifesterar kärnan av vilka vi
                är, nyttan vi skapar, tonaliteten vi har och hur vi vill
                uppfattas. Varumärket ska hjälpa till att sätta rätt
                förväntningar hos användarna.
              </digi-typography-preamble>
              <br />
              <p>
                Allt Arbetsförmedlingen producerar bidrar till hur varumärket
                Arbetsförmedlingen upplevs. Det är viktigt att det framgår vem
                som står bakom det producerade materialet oavsett om det är i
                form av webbtjänster, presentationsmaterial, video eller annat
                medium. Det är viktigt att förhålla sig till den värdegrund som
                Arbetsförmedlingen har samt de riktlinjer som vi kommunicerar i
                vår grafiska profil och i designsystemet. Det är lika relevant
                för det som produceras och kommuniceras både internt samt
                externt.
              </p>
              <h2>Det här är Arbetsförmedlingen </h2>

              <div class="digi-docs__brand-sign-image">
                <digi-media-image
                  afUnlazy
                  afSrc={getAssetPath(
                    '/assets/images/employment-service-sign.jpg'
                  )}
                  afAlt="Fasadskylt med Arbetsförmedlingens logotyp."></digi-media-image>
              </div>
              <h2>Professionell </h2>
              <p>
                Allt vi gör skapar nytta för användarna och vi känner stolthet över
                det vi gör. Vi har ordning och reda i vår verksamhet.{' '}
              </p>
              <h2>Inspirerande </h2>
              <p>
                Vår drivkraft är att se människor växa och utvecklas. Vi
                inspireras av varandra och möter människor där de är.{' '}
              </p>
              <h2>Förtroendeingivande </h2>
              <p>
                Vi tror på människans inneboende förmåga och har förtroende för
                varandra. Vi är tydliga och skapar rätt förväntningar.{' '}
              </p>
              <h2>Vision </h2>
              <p>
                Vi gör Sverige rikare genom att få människor och företag att
                växa. Ett jobb lägger grunden för människans oberoende,
                självständighet och frihet. Ett jobb ger kraft och
                förutsättningar till skapande och växande. Företag och
                organisationer som har arbetskraft med rätt kompetens kan agera
                på bättre villkor. Det lägger grunden för tillväxt och välfärd.
                När människor och företag möts uppstår utveckling och
                innovation. På så sätt växer människorna, företagen och
                samhället. Tillsammans skapar vi ett rikare Sverige.
              </p>
              <h2>Uppdraget</h2>
              <p>
                Vi motiverar, utvecklar och vägleder arbetssökande i allmänhet
                och dem som står långt från arbetsmarknaden i synnerhet. Vi
                finns för alla arbetsgivare men särskilt för dem som behöver
                växtkraft.
              </p>

              <h2>Bakgrund</h2>
              <p>
                Arbetsförmedlingen är Sveriges största förmedlare av arbeten.
                Att sammanföra arbetsgivare med arbetssökande är vår viktigaste
                uppgift. Vi är en nationell myndighet som finns i hela landet.
                Huvudkontoret finns i Solna och Arbetsförmedlingens organisation
                leds av generaldirektören. Alla våra tjänster är avgiftsfria.
              </p>
              <p>
                Arbetsförmedlingens uppdrag är att bidra till en väl fungerande
                arbetsmarknad både för arbetssökande och för arbetsgivare. Vi
                gör det genom att hjälpa de arbetssökande att hitta jobb och
                företag att hitta rätt kompetens. En viktig uppgift för oss är
                att hjälpa dem som har svårare att hitta jobb. På så sätt kan vi
                bidra till att förhindra utslagning och öka sysselsättningen på
                lång sikt.
              </p>
              <p>
                {' '}
                En förutsättning för att Arbetsförmedlingen ska lyckas med
                uppdraget är att människor och företag har förtroende för oss
                och de tjänster vi levererar. Därför arbetar vi hårt för att
                utveckla vår service och möta behoven hos arbetsgivare och
                arbetssökande. Vårt uppdrag, de långsiktiga målen och
                uppgifterna kommer från riksdag och regering. Vi ansvarar också
                för att se till att arbetslöshetsförsäkringen fungerar som en
                omställningsförsäkring. Arbetsförmedlingen har ett samordnande
                ansvar för vissa nyanländas etablering på arbetsmarknaden.
              </p>
              <p>
                Uppdraget är att ge nyanlända rätt förutsättningar för att så
                snabbt som möjligt lära sig svenska, få ett arbete och kunna
                försörja sig. I vårt uppdrag ingår även arbetslivsinriktad
                rehabilitering i samarbete med Försäkringskassan. Verksamheten
                bidrar till att fler människor med nedsatt arbetsförmåga på
                grund av funktionsnedsättningar eller ohälsa kan börja arbeta
                igen.
              </p>
            </digi-layout-block>
          </digi-typography>
        </digi-docs-page-layout>
      </div>
    );
  }
}
