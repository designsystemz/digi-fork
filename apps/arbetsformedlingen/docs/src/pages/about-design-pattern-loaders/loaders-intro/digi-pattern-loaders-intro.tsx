import { Component, h } from '@stencil/core';
import { router } from '../../../global/router';

@Component({
  tag: 'digi-pattern-loaders-intro',
  styleUrl: 'digi-pattern-loaders-intro.scss',
  scoped: true
})
export class LoadersIntro {
  Router = router;

  linkHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-layout-container>
          <div class="digi-pattern-loaders-intro">
            <div class="digi-pattern-loaders-intro__text">
              <digi-typography-heading-jumbo
                af-text="Laddningsindikatorer"
                af-level="h1"></digi-typography-heading-jumbo>
              <digi-typography-preamble class="digi-pattern-loaders-intro__preamble">
                <span lang="en">Skeleton loader</span> och{' '}
                <span lang="en">Spinner</span> är visuella platshållare för
                information medan data fortfarande laddas. Det här
                designmönstret är till för att med gemensam teknisk grund och
                design åstadkomma en enhetlig upplevelse för användarna, vilket
                kan förbättra både prestanda, tillgänglighet och upplevelsen av
                våra digitala tjänster.
              </digi-typography-preamble>
            </div>
          </div>
        </digi-layout-container>
      </host>
    );
  }
}
