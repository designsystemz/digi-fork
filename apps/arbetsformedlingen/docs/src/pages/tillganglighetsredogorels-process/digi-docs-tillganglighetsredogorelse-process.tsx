import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';

@Component({
  tag: 'digi-docs-tillganglighetsredogorelse-process',
  styleUrl: 'digi-docs-tillganglighetsredogorelse-process.scss',
  scoped: true
})
export class DigiDocsTillganglighetsredogorelseProcess {
  Router = router;
  @State() pageName = 'Process för tillgänglighetsredogörelse';

  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }
  render() {
    return (
      <digi-docs-page-layout
        af-page-heading={this.pageName}
        af-edit-href="pages/tillganglighetsredogorels-process/digi-docs-tillganglighetsredogorelse-process.tsx">
        <span slot="preamble">
          Vi arbetar kontinuerligt med att förtydliga processen för att skapa
          tillgänglighetsredogörelser. Därför kan denna och underliggande sidor
          uppdateras ofta med små ändringar och förtydliganden.
        </span>
        <digi-layout-container af-margin-bottom af-margin-top>
          <article>
            <p>
              Samtliga webbplatser som Arbetsförmedlingen står bakom ska ha en
              tillgänglighetsredogörelse som beskriver status och de
              tillgänglighetsbrister som finns. En tillgänglighetsredogörelse
              kan se olika ut beroende på om den aktuella webbplatsen omfattas
              av lagkrav eller inte. På denna sida finner ni instruktioner och
              mallar som beskriver hur ni skriver era bristtexter och
              redogörelser.
            </p>
            <h2>
              Redogörelse för arbetsformedlingen.se med tillhörande tjänster
            </h2>
            <p>
              Alla team som är ansvariga för en sida eller tjänst på webbplatsen
              arbetsformedlingen.se ska vid varje release uppdatera sina listor
              på tillgänglighetsbrister så att Arbetsförmedlingens
              tillgänglighetsredogörelse alltid är uppdaterad. Här följer
              instruktioner om hur den processen går till:
            </p>

            <digi-link-internal
              afHref="/tillganglighetsredogorelse/lista-med-tillganglighetsbrister"
              af-variation="small"
              onAfOnClick={(e) => this.linkClickHandler(e)}>
              Instruktioner för lista med tillgänglighetsbrister
            </digi-link-internal>
            <h2>Redogörelse för externa webbplatser</h2>
            <p>
              Alla externa webbplatser som Arbetsförmedlingen står bakom ska ha
              en egen tillgänglighetsredogörelse. Den som är ansvarig för en
              webbplats säkerställer att redogörelsen publiceras på webbplatsen
              och att redogörelsen med bristtexter hålls uppdaterad.
            </p>

            <digi-link-internal
              afHref="/tillganglighetsredogorelse/externa-webbplatser"
              af-variation="small"
              onAfOnClick={(e) => this.linkClickHandler(e)}>
              Instruktioner för externa webbplatser
            </digi-link-internal>

            <h2>Redogörelse för interna webbplatser</h2>
            <p>
              Även interna webbplatser som Arbetsförmedlingen står bakom ska ha
              en egen tillgänglighetsredogörelse. Den som är ansvarig för en
              webbplats säkerställer att redogörelsen publiceras på den aktuella
              webbplatsen och att redogörelsen med bristtexter hålls uppdaterad.
            </p>
            <digi-link-internal
              afHref="/tillganglighetsredogorelse/interna-webbplatser"
              af-variation="small"
              onAfOnClick={(e) => this.linkClickHandler(e)}>
              Instruktioner för interna webbplatser
            </digi-link-internal>

            <h2>Redogörelse för designsystemet</h2>
            <digi-link-internal
              afHref="/tillganglighetsredogorelse"
              af-variation="small"
              onAfOnClick={(e) => this.linkClickHandler(e)}>
              Tillgänglighetsredogörelse
            </digi-link-internal>
          </article>
        </digi-layout-container>
      </digi-docs-page-layout>
    );
  }
}
