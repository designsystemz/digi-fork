import { Component, h, State } from '@stencil/core';

// eslint-disable-next-line @nx/enforce-module-boundaries
import designTokens from '../../../../../../../dist/libs/arbetsformedlingen/dist/digi-arbetsformedlingen/design-tokens/styleguide/web.json';
import { TokenFormat } from '../../../components/design-tokens/token-format.enum';

import { LayoutBlockVariation } from '@digi/arbetsformedlingen';

type Tokens = typeof designTokens;

@Component({
  tag: 'digi-docs-colors-tokens',
  styleUrl: 'digi-docs-colors-tokens.scss',
  scoped: true
})
export class DigiDocsColorsTokens {
  @State() colorTokens: Tokens;
  @State() tokenFormat: TokenFormat = TokenFormat.CUSTOM_PROPERTY;

  private _profileColors: Tokens;
  private _profileColorsGroup: Tokens;

  private _complementaryColors: Tokens;
  private _complementaryColorsGroup: Tokens;

  private _ctaColors: Tokens;
  private _ctaColorsGroup: Tokens;

  private _neutralColors: Tokens;
  private _neutralColorsGroup: Tokens;

  private _functionColors: Tokens;
  private _functionColorsGroup: Tokens;

  componentWillLoad() {
    this.colorTokens = [
      ...designTokens.filter((token) =>
        token.filePath.includes('src/styles/color')
      )
    ];
    this.setColorTokens();
  }

  setColorTokens() {
    this._profileColors = this.colorTokens.filter((token) =>
      this.isSubset(token.attributes, {
        category: 'global',
        type: 'color',
        item: 'profile'
      })
    );
    this._profileColorsGroup = this.groupTokens(
      this._profileColors.filter((color) => !color.attributes.hidden)
    );

    this._complementaryColors = this.colorTokens.filter((token) =>
      this.isSubset(token.attributes, {
        category: 'global',
        type: 'color',
        item: 'complementary'
      })
    );
    this._complementaryColorsGroup = this.groupTokens(
      this._complementaryColors.filter((color) => !color.attributes.hidden)
    );

    this._ctaColors = this.colorTokens.filter((token) =>
      this.isSubset(token.attributes, {
        category: 'global',
        type: 'color',
        item: 'cta'
      })
    );
    this._ctaColorsGroup = this.groupTokens(
      this._ctaColors.filter((color) => !color.attributes.hidden)
    );

    this._neutralColors = this.colorTokens.filter((token) =>
      this.isSubset(token.attributes, {
        category: 'global',
        type: 'color',
        item: 'neutral'
      })
    );
    this._neutralColorsGroup = this.groupTokens(
      this._neutralColors.filter((color) => !color.attributes.hidden)
    );

    this._functionColors = this.colorTokens.filter((token) =>
      this.isSubset(token.attributes, {
        category: 'global',
        type: 'color',
        item: 'function'
      })
    );
    this._functionColorsGroup = this.groupTokens(
      this._functionColors.filter((color) => !color.attributes.hidden)
    );
  }

  isSubset(superObj, subObj) {
    return Object.keys(subObj).every((ele) => {
      if (typeof subObj[ele] == 'object') {
        return this.isSubset(superObj[ele], subObj[ele]);
      }
      return subObj[ele] === superObj[ele];
    });
  }

  groupTokens(arr = []) {
    const res = [];
    const map = {};
    let i, j, curr;

    for (i = 0, j = arr.length; i < j; i++) {
      curr = arr[i];
      if (!(curr.attributes.subitem in map)) {
        map[curr.attributes.subitem] = {
          name: curr.attributes.subitem,
          variations: []
        };
        res.push(map[curr.attributes.subitem]);
      }
      map[curr.attributes.subitem].variations.push(curr);
    }
    return res;
  }

  tokenFormatChangeHandler(e) {
    this.tokenFormat = e.detail.target.value;
  }

  render() {
    return (
      <div class="digi-docs-colors-tokens">
        <digi-layout-block afMarginTop afMarginBottom>
          <digi-notification-alert
            af-size="large"
            af-variation="warning"
            af-heading="Informationsmeddelande">
            Denna sida är temporär och kommer att tas bort i Februari 2025. Vi
            arbetar med ett designmönster som förklarar hur färger ska användas
            på ett tydligare sätt.
            <br />
            <br />
            Länk till nya sidan kommer finnas tillgänglig här vid lansering.
          </digi-notification-alert>

          <br />

          <digi-typography-preamble>
            Tokens är ett gemensamt språk för att beskriva struktur, kod och
            design. Här hittar du ett urval av färger som är kopplade till den
            grafiska profilen. Det vill säga färger som förekommer i
            komponenter, grafik, illustrationer och text.
          </digi-typography-preamble>
          <br />
          <p>
            Det är viktigt att färgerna används på rätt sätt. Om du är osäker på
            hur du skall använda en färg diskutera gärna det med en insatt
            kollega eller hör av dig till oss i designsystemet så kan vi guida
            er.
          </p>
          {/* <p>
						Har ni kopplat designsystemet Digi UI-kit till ert projekt, så har ni
						motsvarande färgpalett speglad direkt i Figma.
					</p> */}
          <div class="digi-docs-colors-tokens__filter-wrapper">
            <div class="digi-docs-colors-tokens__filter-format">
              <digi-form-select
                afLabel="Format"
                afId="tokenFormat"
                afDisableValidation={true}
                onAfOnChange={(e) => this.tokenFormatChangeHandler(e)}>
                <option value="custom-property">CSS-variabel </option>
                <option value="sass">Sass </option>
              </digi-form-select>
            </div>
          </div>
        </digi-layout-block>

        <digi-layout-block
          class="digi-docs-colors-tokens__bottom"
          afVariation={LayoutBlockVariation.SECONDARY}>
          <div class="digi-docs-colors-tokens__tokens-wrapper">
            <h2>Profilfärger</h2>
            {this._profileColorsGroup.map((colors) => {
              return (
                <digi-docs-color-token-cards
                  colorTokens={colors['variations']}
                  tokenFormat={this.tokenFormat}></digi-docs-color-token-cards>
              );
            })}

            <h2>Komplementfärger</h2>
            {this._complementaryColorsGroup.map((colors) => {
              return (
                <digi-docs-color-token-cards
                  colorTokens={colors['variations']}
                  tokenFormat={this.tokenFormat}></digi-docs-color-token-cards>
              );
            })}

            <h2>Markeringsfärger</h2>
            {this._ctaColorsGroup.map((colors) => {
              return (
                <digi-docs-color-token-cards
                  colorTokens={colors['variations']}
                  tokenFormat={this.tokenFormat}></digi-docs-color-token-cards>
              );
            })}

            <h2>Neutral skala</h2>
            {this._neutralColorsGroup.map((colors) => {
              return (
                <digi-docs-color-token-cards
                  colorTokens={colors['variations']}
                  tokenFormat={this.tokenFormat}></digi-docs-color-token-cards>
              );
            })}

            <h2>Funktionsfärger</h2>
            {this._functionColorsGroup.map((colors) => {
              return (
                <digi-docs-color-token-cards
                  colorTokens={colors['variations']}
                  tokenFormat={this.tokenFormat}></digi-docs-color-token-cards>
              );
            })}
          </div>
        </digi-layout-block>
      </div>
    );
  }
}
