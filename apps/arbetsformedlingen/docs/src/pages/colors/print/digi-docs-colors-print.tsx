import { Component, h, State } from '@stencil/core';

// eslint-disable-next-line @nx/enforce-module-boundaries
import designTokens from '../../../../../../../dist/libs/arbetsformedlingen/dist/digi-arbetsformedlingen/design-tokens/print/print.json';
import { TokenFormat } from '../../../components/design-tokens/token-format.enum';

import { LayoutBlockVariation } from '@digi/arbetsformedlingen';

type Tokens = typeof designTokens;

@Component({
  tag: 'digi-docs-colors-print',
  styleUrl: 'digi-docs-colors-print.scss',
  scoped: true
})
export class DigiDocsColorsPrint {
  @State() colorTokens: Tokens;
  @State() tokenFormat: TokenFormat = TokenFormat.RGB;

  private _profileColors: Tokens;
  private _profileColorsGroup: Tokens;

  private _complementaryColors: Tokens;
  private _complementaryColorsGroup: Tokens;

  private _backgroundColors: Tokens;
  private _backgroundColorsGroup: Tokens;

  private _neutralColors: Tokens;
  private _neutralColorsGroup: Tokens;

  componentWillLoad() {
    this.colorTokens = [
      ...designTokens.filter((token) => token.filePath.includes('src/print'))
    ];
    this.setColorTokens();
  }

  setColorTokens() {
    this._profileColors = this.colorTokens.filter((token) =>
      this.isSubset(token.attributes, {
        category: 'print',
        type: 'color',
        item: 'profile'
      })
    );
    this._profileColorsGroup = this.groupTokens(this._profileColors);

    this._complementaryColors = this.colorTokens.filter((token) =>
      this.isSubset(token.attributes, {
        category: 'print',
        type: 'color',
        item: 'complementary'
      })
    );
    this._complementaryColorsGroup = this.groupTokens(
      this._complementaryColors
    );

    this._backgroundColors = this.colorTokens.filter((token) =>
      this.isSubset(token.attributes, {
        category: 'print',
        type: 'color',
        item: 'background'
      })
    );
    this._backgroundColorsGroup = this.groupTokens(this._backgroundColors);

    this._neutralColors = this.colorTokens.filter((token) =>
      this.isSubset(token.attributes, {
        category: 'print',
        type: 'color',
        item: 'neutral'
      })
    );
    this._neutralColorsGroup = this.groupTokens(this._neutralColors);
  }

  isSubset(superObj, subObj) {
    return Object.keys(subObj).every((ele) => {
      if (typeof subObj[ele] == 'object') {
        return this.isSubset(superObj[ele], subObj[ele]);
      }
      return subObj[ele] === superObj[ele];
    });
  }

  groupTokens(arr = []) {
    const res = [];
    const map = {};
    let i, j, curr;

    for (i = 0, j = arr.length; i < j; i++) {
      curr = arr[i];
      if (!(curr.attributes.subitem in map)) {
        map[curr.attributes.subitem] = {
          name: curr.attributes.subitem,
          variations: []
        };
        res.push(map[curr.attributes.subitem]);
      }
      map[curr.attributes.subitem].variations.push(curr);
    }
    return res;
  }

  render() {
    return (
      <div class="digi-docs-colors-print">
        <digi-layout-block afMarginTop afMarginBottom>
          <digi-typography-preamble digi-typography-preamble>
            För tryck använder vi färre varianter av färger än för webben. Det
            är framförallt profilfärgerna (med toningar), komplementfärger och
            en gråskala.
          </digi-typography-preamble>
          <br />
          <p>
            Dessa beskrivs på olika sätt beroende på vilken tryckteknik som
            används.
          </p>
          <p>
            CMYK står för cyan (blått), magenta (rött), yellow (gult) och key
            (svart). CMYK använd vid fyrfärgstryck i annonser, broschyrer,
            affischer med mera. Toningar anges som procentangivelse för varje
            färg.
          </p>
          <p>
            PMS står för Pantone Matching System och används vid
            dekorfärgstryck.
          </p>
          <p>
            RGB står för Röd, Grön och Blå. Det är RGB-färger som återges på en
            dataskärm och anges med ett värde mellan 0 och 255.
          </p>
          <p>
            Viktigt att tänka på är att den profilblå innehåller mer än 100
            procent färg och kan ibland upplevas som svart. Beroende på underlag
            så kan den behövas justeras något.
          </p>
        </digi-layout-block>
        <digi-layout-block afVariation={LayoutBlockVariation.SECONDARY}>
          <div class="digi-docs-colors-print__colors-wrapper">
            <h2>Profilfärger</h2>
            {this._profileColorsGroup.map((colors) => {
              return (
                <digi-docs-color-token-cards
                  colorTokens={colors['variations']}
                  tokenFormat={this.tokenFormat}></digi-docs-color-token-cards>
              );
            })}

            <h2>Komplementfärger</h2>
            {this._complementaryColorsGroup.map((colors) => {
              return (
                <digi-docs-color-token-cards
                  colorTokens={colors['variations']}
                  tokenFormat={this.tokenFormat}></digi-docs-color-token-cards>
              );
            })}

            <h2>Bakgrundsplattor</h2>
            {this._backgroundColorsGroup.map((colors) => {
              return (
                <digi-docs-color-token-cards
                  colorTokens={colors['variations']}
                  tokenFormat={this.tokenFormat}></digi-docs-color-token-cards>
              );
            })}

            <h2>Gråskala</h2>
            {this._neutralColorsGroup.map((colors) => {
              return (
                <digi-docs-color-token-cards
                  colorTokens={colors['variations']}
                  tokenFormat={this.tokenFormat}></digi-docs-color-token-cards>
              );
            })}
          </div>
        </digi-layout-block>
      </div>
    );
  }
}
