importScripts('workbox-v4.3.1/workbox-sw.js');

/**
 * Enable offline routing.
 */

self.workbox.routing.registerNavigationRoute('/index.html');

self.workbox.routing.registerRoute(
  /\/assets\//,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'assets-cache',
    plugins: [
      new workbox.expiration.Plugin({
        // Only cache requests for a week
        maxAgeSeconds: 7 * 24 * 60 * 60,
        // Only cache 10 requests.
        maxEntries: 10
      })
    ]
  })
);

// Cache the Google Fonts stylesheets with a stale-while-revalidate strategy.
self.workbox.routing.registerRoute(
  /^https:\/\/fonts\.googleapis\.com/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'google-fonts-stylesheets'
  })
);

// Cache the underlying font files with a cache-first strategy for 1 year.
self.workbox.routing.registerRoute(
  /^https:\/\/fonts\.gstatic\.com/,
  new workbox.strategies.CacheFirst({
    cacheName: 'google-fonts-webfonts',
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      }),
      new workbox.expiration.Plugin({
        maxAgeSeconds: 60 * 60 * 24 * 365,
        maxEntries: 30
      })
    ]
  })
);

self.workbox.precaching.cleanupOutdatedCaches();

/**
 * Gets populated by sw-config.js
 */
self.workbox.precaching.precacheAndRoute([]);
// self.workbox.precaching.precacheAndRoute(self.__WB_MANIFEST);

self.skipWaiting();
