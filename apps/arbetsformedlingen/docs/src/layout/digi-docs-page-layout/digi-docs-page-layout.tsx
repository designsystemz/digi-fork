import { Component, Element, h, Prop, State } from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import Helmet from '@stencil/helmet';
import * as analytics from '../../services/analytics.service';

@Component({
  tag: 'digi-docs-page-layout',
  styleUrl: 'digi-docs-page-layout.scss'
})
export class DigiDocsPageLayout {
  @Element() hostElement: HTMLStencilElement;

  @Prop() afEditBaseUrl =
    'https://bitbucket.arbetsformedlingen.se/projects/DIGI/repos/digi-monorepo/browse/apps/docs/src/';
  @Prop() afEditHref: string;

  @Prop() afPageHeading: string; // @Prop() afPageHeading !: string;
  @State() hasPreamble: boolean;
  @State() preamble: string;
  @State() showFeedback = false;
  @State() currentUrl: string;

  private _feedback;

  setHasPreamble() {
    this.hasPreamble = !!this.hostElement.querySelector('[slot="preamble"]');

    if (this.hasPreamble) {
      this.preamble =
        this.hostElement.querySelector('[slot="preamble"]').innerHTML;
    } else {
      this.preamble =
        'Välkommen till Arbetsförmedlingen Designsystem! Här finner du komponenter i både kod och design, designmönster, riktlinjer och mer som gäller för hela Arbetsförmedlingen.';
    }
  }

  setCheckPage() {
    this.currentUrl = window.location.pathname;
    if (
      this.currentUrl.includes('/komponenter') &&
      !this.currentUrl.includes('/komponenter/om-komponenter')
    ) {
      this.showFeedback = true;
    } else {
      this.showFeedback = false;
    }
  }

  componentWillLoad() {
    this.setHasPreamble();
    this.setCheckPage();
  }

  componentDidRender() {
    this.initFeedback();
  }

  componentWillUpdate() {
    this.setHasPreamble();
  }

  async initFeedback() {
    await customElements.whenDefined('digi-tools-feedback');
    await this._feedback?.afMReset();

    setTimeout(() => {
      const buttons = this.hostElement.querySelectorAll(
        '.digi-tools-feedback__container-btns digi-button'
      );
      buttons.forEach((fbutton) => {
        fbutton.addEventListener('afOnClick', (event) => {
          const b = event.target as HTMLButtonElement;
          const answer = b.textContent;
          analytics.analyticsClickHandler(answer);
          this._feedback.afMSetStep(3);
        });
      });
    }, 100);
  }

  render() {
    return (
      <host>
        <digi-typography>
          <Helmet>
            <meta name="description" content={this.preamble} />
          </Helmet>
          <section class="digi-docs-page-layout__content">
            <digi-layout-container>
              {this.afPageHeading && (
                <digi-typography-heading-jumbo af-text={this.afPageHeading} />
              )}
              {this.hasPreamble && (
                <digi-typography-preamble>
                  <slot name="preamble"></slot>
                </digi-typography-preamble>
              )}
            </digi-layout-container>
            <slot></slot>
            {this.showFeedback && (
              <digi-tools-feedback
                ref={(el) => (this._feedback = el)}
                afText="Var denna artikel hjälpsam?"
                af-type="fullwidth"></digi-tools-feedback>
            )}
          </section>
        </digi-typography>
      </host>
    );
  }
}
