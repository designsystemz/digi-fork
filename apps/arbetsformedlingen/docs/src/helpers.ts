/* eslint-disable @typescript-eslint/no-explicit-any */

export enum ComponentStatus {
  Deprecated = 'deprecated',
  New = 'new'
}

export function statusForComponent(component: any): ComponentStatus | null {
  const componentIsDeprecated =
    component.docsTags?.find(
      (tag) => tag.name === ComponentStatus.Deprecated
    ) !== undefined;
  const componentIsNew =
    component.docsTags?.find((tag) => tag.name === ComponentStatus.New) !==
    undefined;

  if (componentIsDeprecated) {
    return ComponentStatus.Deprecated;
  }
  if (componentIsNew) {
    return ComponentStatus.New;
  }
  return null;
}
