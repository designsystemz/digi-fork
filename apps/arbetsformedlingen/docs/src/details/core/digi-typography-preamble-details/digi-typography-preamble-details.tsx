import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-typography-preamble-details',
  styleUrl: 'digi-typography-preamble-details.scss'
})
export class DigiTypographyPeambleDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  get typographyPeamble() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-typography-preamble>
	Jag är en ingress. Använd mig direkt efter huvudrubrik.
</digi-typography-preamble>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-typography-preamble>
	Jag är en ingress. Använd mig direkt efter huvudrubrik.
</digi-typography-preamble>`,
      [CodeExampleLanguage.REACT]: `\
<DigiTypographyPreamble>
	Jag är en ingress. Använd mig direkt efter huvudrubrik.
</DigiTypographyPreamble>`
    };
  }

  render() {
    return (
      <div class="digi-typography-preamble-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används för att skapa en ingress direkt efter
              en huvudrubrik. Det rekommenderas att använda denna komponent runt
              textnoder eftersom den omsluter allt med en{' '}
              <digi-code af-code="<p>" />
              -tagg.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.typographyPeamble)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <digi-typography-preamble>
                  Jag är en ingress. Använd mig direkt efter huvudrubrik.
                </digi-typography-preamble>
              </digi-code-example>
            </article>
          </digi-layout-container>
        </digi-typography>
      </div>
    );
  }
}
