import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  FormRadiobuttonVariation,
  LinkVariation
} from '@digi/arbetsformedlingen';
import state from '../../../store/store';
import { router } from '../../../global/router';

@Component({
  tag: 'digi-form-radiobutton-details',
  styleUrl: 'digi-form-radiobutton-details.scss'
})
export class DigiFormRadiobuttontDetails {
  Router = router;
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() FormRadiobuttonVariation: FormRadiobuttonVariation =
    FormRadiobuttonVariation.PRIMARY;
  @State() validation = false;

  radiogroupClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  get radiobuttonCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-form-radiobutton 
	af-label="Radioknapp"
	af-variation="${this.FormRadiobuttonVariation}"\
	${this.validation ? `\n\taf-validation="error"` : ''}
>
</digi-form-radiobutton>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-form-radiobutton
	[attr.af-label]="'Radioknapp'"
	[attr.af-variation]="FormRadiobuttonVariation.${Object.keys(FormRadiobuttonVariation).find((key) => FormRadiobuttonVariation[key] === this.FormRadiobuttonVariation)}"\
	${this.validation ? `\n\t[attr.af-validation]="FormRadiobuttonValidation.ERROR"` : ''}
>
</digi-form-radiobutton>`,
      [CodeExampleLanguage.REACT]: `\
<DigiFormRadiobutton
	afLabel="Radioknapp"
	afVariation={FormRadiobuttonVariation.${Object.keys(FormRadiobuttonVariation).find((key) => FormRadiobuttonVariation[key] === this.FormRadiobuttonVariation)}}\
	${this.validation ? `\n\tafValidation={FormRadiobuttonValidation.ERROR}` : ''}
>
</DigiFormRadiobutton>`
    };
  }
  render() {
    return (
      <div class="digi-form-radiobutton-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Används för att välja ett enda alternativ av flera fördefinierade
              val.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.radiobuttonCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variation"
                  afLegend="Variation"
                  onChange={(e) =>
                    (this.FormRadiobuttonVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Primär"
                    afValue={FormRadiobuttonVariation.PRIMARY}
                    afChecked={
                      this.FormRadiobuttonVariation ===
                      FormRadiobuttonVariation.PRIMARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Sekundär"
                    afValue={FormRadiobuttonVariation.SECONDARY}
                    afChecked={
                      this.FormRadiobuttonVariation ===
                      FormRadiobuttonVariation.SECONDARY
                    }
                  />
                </digi-form-fieldset>

                <digi-form-fieldset afLegend="Validering">
                  <digi-form-checkbox
                    afLabel="Error"
                    afChecked={this.validation}
                    onAfOnChange={() =>
                      this.validation
                        ? (this.validation = false)
                        : (this.validation = true)
                    }></digi-form-checkbox>
                </digi-form-fieldset>
              </div>
              <digi-form-radiobutton
                afLabel="Radioknapp"
                afVariation={this.FormRadiobuttonVariation}
                af-validation={this.validation ? 'error' : ''}
                afChecked={!this.validation}
              />
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <p>
                Komponenten finns i en primär och sekundär variant.
                Typescript-användare bör importera och använda{' '}
                <digi-code af-code="FormRadiobuttonVariation" /> för att ändra
                variation.
              </p>
              <h3>Validering</h3>
              <p>
                Det går att sätta valideringsstatus{' '}
                <digi-code af-code="error" />. Typescript-användare bör
                importera och använda{' '}
                <digi-code af-code="FormRadiobuttonVariation" /> för att sätta
                valideringsstatus till error.
              </p>
              <h3>ControlValueAccessor</h3>
              <p>
                För att kunna knyta radioknappar mot{' '}
                <digi-code afCode="ControlValueAccessor"></digi-code> i Angular
                så behöver du använda denna komponent tillsammans med{' '}
                <digi-link
                  afVariation={LinkVariation.SMALL}
                  afHref={`${state.routerRoot}komponenter/digi-form-radiogroup/oversikt`}
                  onAfOnClick={(e) => this.radiogroupClickHandler(e)}>
                  digi-form-radiogroup
                </digi-link>
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
