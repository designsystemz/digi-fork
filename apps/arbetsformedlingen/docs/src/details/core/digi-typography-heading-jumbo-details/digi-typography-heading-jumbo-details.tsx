import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-typography-heading-jumbo-details',
  styleUrl: 'digi-typography-heading-jumbo-details.scss'
})
export class DigiTypographyHeadingJumboDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  get jumboHeadingCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-typography-heading-jumbo
	af-text="Det här är en jumborubrik"
	af-level="h1"
></digi-typography-heading-jumbo>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-typography-heading-jumbo
	[attr.af-text]="Det här är en jumborubrik"
	[attr.af-level]="TypographyHeadingJumboLevel.H1"
>
</digi-typography-heading-jumbo>`,
      [CodeExampleLanguage.REACT]: `\
<DigiTypographyHeadingJumbo
	afText="Det här är en jumborubrik"
	afLevel={TypographyHeadingJumboLevel.H1}
>
</DigiTypographyHeadingJumbo>`
    };
  }
  render() {
    return (
      <div class="digi-typography-heading-jumbo-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Det här är en jumborubrik med en linje under.
            </digi-typography-preamble>
          )}

          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.jumboHeadingCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <digi-typography-heading-jumbo
                  af-text="Det här är en jumborubrik"
                  af-level="h1"></digi-typography-heading-jumbo>
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <p>
                Rubriktexten läggs in med <digi-code af-code="af-text" /> och
                rubriknivån är <digi-code af-code="h1" /> som standard. Vill du
                ändra rubriknivå kan du använda <digi-code af-code="af-level" />{' '}
                och lägg in den som du vill ha en property. Typescript-användare
                bör importera och använda
                <digi-code af-code="TypographyHeadingJumboLevel" />. Stylingen
                på jumborubriken kommer se likadan ut även om du ändrar till en
                lägre rubriknivå.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
