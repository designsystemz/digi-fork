import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'digi-util-detect-focus-outside-details',
  styleUrl: 'digi-util-detect-focus-outside-details.scss'
})
export class DigiUtilDetectFocusOutSideDetails {
  @Prop() component: string;

  render() {
    return (
      <div class="digi-util-detect-focus-outside-details">
        <digi-typography>
          <digi-layout-block af-variation="primary">
            <digi-typography-preamble>
              <digi-code af-code={`<digi-util-detect-focus-outside>`} />
              övervakar fokushändelser och skapar events när fokus sätts utanför
              eller inuti ett element.
            </digi-typography-preamble>
          </digi-layout-block>
        </digi-typography>
      </div>
    );
  }
}
