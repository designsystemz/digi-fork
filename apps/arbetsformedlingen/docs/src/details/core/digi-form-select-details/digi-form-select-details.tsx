/* eslint-disable no-useless-escape */
import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  FormSelectValidation,
  FormSelectVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-form-select-details',
  styleUrl: 'digi-form-select-details.scss'
})
export class DigiFormSelectDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() formSelectValidation: FormSelectValidation =
    FormSelectValidation.NEUTRAL;
  @State() formSelectVariation: FormSelectVariation =
    FormSelectVariation.MEDIUM;
  @State() intialText = 'placeholder';
  @State() hasDescription = false;

  get inputCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-form-select
	af-label="Etikett"
	af-variation="${this.formSelectVariation}"
	af-validation="${this.formSelectValidation}"\
	${this.hasDescription ? '\n\taf-description="Beskrivande text"' : ' '}\
	${this.intialText === 'placeholder' ? '\n\taf-placeholder="Platshållare"' : ' '}\
	${this.intialText === 'startSelected' ? '\n\taf-value="val1"' : ' '}
	${this.formSelectValidation !== FormSelectValidation.NEUTRAL ? 'af-validation-text="Det här är ett valideringsmeddelande" \n\t' : ''}\
\>
	<option value="val1">Val 1</option>
	<option value="val2">Val 2</option>
</digi-form-select>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-form-select
	[attr.af-label]="'Etikett'"
	[attr.af-variation]="FormSelectVariation.${this.formSelectVariation.toUpperCase()}"
	[attr.af-validation]="FormSelectValidation.${this.formSelectValidation.toUpperCase()}"\
	${this.hasDescription ? `\n\t[attr.af-description]="'Beskrivande text'"` : ' '}\
	${this.intialText === 'placeholder' ? `\n\t[attr.af-placeholder]="'Platshållare'"` : ' '}\
	${this.intialText === 'startSelected' ? `\n\t[attr.af-value="'val1'"` : ' '}
	${this.formSelectValidation !== FormSelectValidation.NEUTRAL ? '[attr.af-validation-text]="Det här är ett valideringsmeddelande"\n\t' : ''}\
\>
	<option value="val1">Val 1</option>
	<option value="val2">Val 2</option>
</digi-form-select>`,
      [CodeExampleLanguage.REACT]: `\
<DigiFormSelect
	afLabel="Etikett"
	afVariation={FormSelectVariation.${this.formSelectVariation.toUpperCase()}} 
	afValidation={FormSelectValidation.${this.formSelectValidation.toUpperCase()}}\
	${this.hasDescription ? `\n\tafDescription="Beskrivande text"` : ' '}\
	${this.intialText === 'placeholder' ? `\n\tafPlaceholder="Platshållare"` : ' '}\
	${this.intialText === 'startSelected' ? `\n\tafValue="val1"` : ' '} 
	${this.formSelectValidation !== FormSelectValidation.NEUTRAL ? 'afValidationText="Det här är ett valideringsmeddelande"\n\t' : ''}\
\>
	<option value="val1">Val 1</option>
	<option value="val2">Val 2</option>
</DigiFormSelect>`
    };
  }

  render() {
    return (
      <div class="digi-form-select-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Väljaren används för att möjliggöra val i ett formulär.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.inputCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div
                style={{
                  width: '300px'
                }}>
                {this.intialText == 'none' && !this.hasDescription && (
                  <digi-form-select
                    afLabel="Etikett"
                    af-validation-text="Det här är ett valideringsmeddelande"
                    afVariation={this.formSelectVariation}
                    afValidation={this.formSelectValidation}>
                    <option value="val1">Val 1</option>
                    <option value="val2">Val 2</option>
                  </digi-form-select>
                )}
                {this.intialText == 'placeholder' && !this.hasDescription && (
                  <digi-form-select
                    afLabel="Etikett"
                    af-placeholder="Platshållare  "
                    af-validation-text="Det här är ett valideringsmeddelande"
                    afVariation={this.formSelectVariation}
                    afValidation={this.formSelectValidation}>
                    <option value="val1">Val 1</option>
                    <option value="val2">Val 2</option>
                  </digi-form-select>
                )}
                {this.intialText == 'startSelected' && !this.hasDescription && (
                  <digi-form-select
                    afLabel="Etikett"
                    af-value="val1"
                    af-validation-text="Det här är ett valideringsmeddelande"
                    afVariation={this.formSelectVariation}
                    afValidation={this.formSelectValidation}>
                    <option value="val1">Val 1</option>
                    <option value="val2">Val 2</option>
                  </digi-form-select>
                )}
                {this.intialText == 'none' && this.hasDescription && (
                  <digi-form-select
                    afLabel="Etikett"
                    af-description="Beskrivande text"
                    af-validation-text="Det här är ett valideringsmeddelande"
                    afVariation={this.formSelectVariation}
                    afValidation={this.formSelectValidation}>
                    <option value="val1">Val 1</option>
                    <option value="val2">Val 2</option>
                  </digi-form-select>
                )}
                {this.intialText == 'placeholder' && this.hasDescription && (
                  <digi-form-select
                    afLabel="Etikett"
                    af-description="Beskrivande text"
                    af-placeholder="Platshållare  "
                    af-validation-text="Det här är ett valideringsmeddelande"
                    afVariation={this.formSelectVariation}
                    afValidation={this.formSelectValidation}>
                    <option value="val1">Val 1</option>
                    <option value="val2">Val 2</option>
                  </digi-form-select>
                )}
                {this.intialText == 'startSelected' && this.hasDescription && (
                  <digi-form-select
                    afLabel="Etikett"
                    af-description="Beskrivande text"
                    af-value="val1"
                    af-validation-text="Det här är ett valideringsmeddelande"
                    afVariation={this.formSelectVariation}
                    afValidation={this.formSelectValidation}>
                    <option value="val1">Val 1</option>
                    <option value="val2">Val 2</option>
                  </digi-form-select>
                )}
              </div>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Storlek"
                  afLegend="Storlek"
                  onChange={(e) =>
                    (this.formSelectVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Liten"
                    afValue={FormSelectVariation.SMALL}
                    afChecked={
                      this.formSelectVariation === FormSelectVariation.SMALL
                    }
                  />
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Mellan"
                    afValue={FormSelectVariation.MEDIUM}
                    afChecked={
                      this.formSelectVariation === FormSelectVariation.MEDIUM
                    }
                  />
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Stor"
                    afValue={FormSelectVariation.LARGE}
                    afChecked={
                      this.formSelectVariation === FormSelectVariation.LARGE
                    }
                  />
                </digi-form-fieldset>
                <digi-form-fieldset afLegend="Beskrivande text">
                  <digi-form-checkbox
                    afLabel="Ja"
                    afChecked={this.hasDescription}
                    onAfOnChange={() =>
                      this.hasDescription
                        ? (this.hasDescription = false)
                        : (this.hasDescription = true)
                    }></digi-form-checkbox>
                </digi-form-fieldset>
                <digi-form-fieldset
                  afName="placeholder"
                  afLegend="Initial text"
                  onChange={(e) =>
                    (this.intialText = (e.target as HTMLFormElement).value)
                  }>
                  <digi-form-radiobutton
                    afName="placeholder"
                    afLabel="Platshållare"
                    afValue="placeholder"
                    afChecked={this.intialText === 'placeholder'}
                  />
                  <digi-form-radiobutton
                    afName="placeholder"
                    afLabel="Förvalt värde"
                    afValue="startSelected"
                    afChecked={this.intialText === 'startSelected'}
                  />
                  <digi-form-radiobutton
                    afName="placeholder"
                    afLabel="Ingen text"
                    afValue="none"
                    afChecked={this.intialText === 'none'}
                  />
                </digi-form-fieldset>
                <digi-form-select
                  afLabel="Validering"
                  onAfOnChange={(e) =>
                    (this.formSelectValidation = (
                      e.target as HTMLDigiFormSelectElement
                    ).value as FormSelectValidation)
                  }
                  af-variation="small">
                  <option value="neutral">Ingen</option>
                  <option value="error">Felaktig</option>
                  <option value="success">Godkänd</option>
                  <option value="warning">Varning</option>
                </digi-form-select>
              </div>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Storlekar</h3>
              <p>
                Väljaren finns i tre olika storleksvarianter: stor, mellan och
                liten. Det går att ställas in med hjälp av{' '}
                <digi-code af-code="af-variation" />.
              </p>
              <h3>Validering</h3>
              <p>
                Väljaren kan få fyra olika valideringsstatusar: Felaktig,
                godkänd, varning eller ingen alls. Attributet{' '}
                <digi-code af-code="af-validation" /> sätter väljarens
                validering och <digi-code af-code="af-validation-text" /> ger
                väljaren ett valideringsmeddelande. Du kan också avaktivera
                valideringen med attributet
                <digi-code af-code="af-disable-validation" />.
              </p>
              <h3>
                <span lang="en">Dropdown</span>
              </h3>
              <p>
                Utseendet av själva <span lang="en">dropdown</span>-ytan styrs
                av operativsystem och webbläsare. Därför kan utseendet skilja
                sig i jämförelse med andra komponenters 
                <span lang="en">dropdown</span>. Det är även av den
                anledningen som komponenten i UI-kittet saknar en 
                <span lang="en">dropdown</span>.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
