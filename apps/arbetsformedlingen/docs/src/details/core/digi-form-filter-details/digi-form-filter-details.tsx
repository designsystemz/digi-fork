import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

const LIST_ITEMS = [
  { id: 'omr1', label: 'Område 1' },
  { id: 'omr2', label: 'Område 2' },
  { id: 'omr3', label: 'Område 3' }
];

const EVENTS = [
  {
    name: 'afChangeFilter',
    body: `(e) => console.log('Användaren interagerade med filterval', 'ID:', e.detail.id, 'Är valt:', e.detail.isChecked)`
  },
  {
    name: 'afResetFilter',
    body: '() => console.log("Användaren återställer")'
  },
  {
    name: 'afSubmitFilter',
    body: '(e) => console.log("Användaren skickar in", e.detail.listItems, e.detail.checked))'
  },
  {
    name: 'afCloseFilter',
    body: '(e) => console.log("Användaren stängde", e.detail.listItems, e.detail.checked)'
  }
];

@Component({
  tag: 'digi-form-filter-details',
  styleUrl: 'digi-form-filter-details.scss'
})
export class DigiFormFilterDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  get filterCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-form-filter
	af-filter-button-text="Yrkesområde"
	af-submit-button-text="Filtrera"
	af-name="Legend text"
>
</digi-form-filter>

<script>
	const element = document.querySelector('digi-form-filter')

	element.afListItems = ${JSON.stringify(LIST_ITEMS)};
</script>`,
      [CodeExampleLanguage.ANGULAR]: `\
<!--component.ts-->
listItems = ${JSON.stringify(LIST_ITEMS)}

<!--template.html-->
<digi-form-filter
	[attr.af-filter-button-text]="Yrkesområde"
	[attr.af-submit-button-text]="Filtrera"
	[attr.af-name]="Legend text"
	[afListItems]='listItems'
> 
</digi-form-filter>`,
      [CodeExampleLanguage.REACT]: `\
<DigiFormFilter
	afFilterButtonText="Yrkesområde"
	afSubmitButtonText="Filtrera"
	afName="Legend text"
	afListItems={${JSON.stringify(LIST_ITEMS)}}
/>`
    };
  }

  render() {
    return (
      <div class="digi-form-filter-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Multifilterkomponenten används för att filtrera resultat med hjälp
              av andra formulärkomponenter, till exempel kryssrutor.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.filterCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls"></div>
              <div style={{ height: '250px' }}>
                <digi-form-filter
                  afFilterButtonText="Yrkesområde"
                  afSubmitButtonText="Filtrera"
                  afName="Legend text"
                  onAfSubmitFilter={(e) =>
                    console.log(
                      'submit filter',
                      e.detail.listItems,
                      e.detail.checked
                    )
                  }>
                  {LIST_ITEMS.map((item) => {
                    return <digi-form-checkbox afLabel={item.label} />;
                  })}
                </digi-form-filter>
              </div>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <article>
                <h2>Användningsexempel</h2>
                <h3>Lyssna på Events</h3>
								<digi-code-block
									af-language="html"
									af-variation="dark"
									af-code={`<script>
	const formFilter = document.querySelector('digi-form-filter')
	${EVENTS.map((event) => `formFilter.addEventListener('${event.name}', ${event.body})`).join('\n\t')}
</script>`}></digi-code-block>
                <h2>Datastruktur</h2>
                <digi-code-block
                  af-language="typescript"
                  af-code={`interface FormFilterItem {
	id: string;
	label: string;
}`}
                  af-variation="dark"></digi-code-block>
              </article>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
