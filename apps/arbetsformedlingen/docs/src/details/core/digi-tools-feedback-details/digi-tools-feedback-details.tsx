import { Component, h, Prop, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  FeedbackVariation,
  ToolsFeedbackType
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-tools-feedback-details',
  styleUrl: 'digi-tools-feedback-details.scss'
})
export class DigiToolsFeedback {
  private _feedback;
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() showExample = true;
  @State() feedbackVariation: FeedbackVariation = FeedbackVariation.PRIMARY;
  @State() toolsFeedbackType: ToolsFeedbackType = ToolsFeedbackType.FULLWIDTH;

  get toolsFeedbackCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
			<digi-tools-feedback   
				af-text="Hjälpte informationen dig?"
				af-variation="${this.feedbackVariation}"
				af-type="${this.toolsFeedbackType}"
				af-is-done="false">
			</digi-tools-feedback>`,
      [CodeExampleLanguage.ANGULAR]: `\
			<digi-tools-feedback 
				[attr.af-text]="Hjälpte informationen dig?"
				[attr.af-variation]="FeedbackVariation.${Object.keys(FeedbackVariation).find(
          (key) => FeedbackVariation[key] === this.feedbackVariation
        )}"
				[attr.af-Type]="toolsFeedbackType.${Object.keys(ToolsFeedbackType).find(
          (key) => ToolsFeedbackType[key] === this.toolsFeedbackType
        )}"
				[attr.af-is-done]="false">
			</digi-tools-feedback>`,
      [CodeExampleLanguage.REACT]: `\
			<DigiToolsFeedback
				afText="Hjälpte informationen dig?"	
				afVariation="${Object.keys(FeedbackVariation).find(
          (key) => FeedbackVariation[key] === this.feedbackVariation
        )}"
				afType="${Object.keys(ToolsFeedbackType).find(
          (key) => ToolsFeedbackType[key] === this.toolsFeedbackType
        )}"
				afIsDone="false">
			</DigiToolsFeedback>`
    };
  }

  handleResetClick(e) {
    if (e.target.matches('#reset')) {
      this._feedback.afMReset();
    }
  }

  render() {
    return (
      <div class="digi-tools-feedback-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Feedback-komponenten möjliggör insamling av användarfeedback genom
              att ställa en fråga som användaren kan svara på. Genom att lägga
              till
              <digi-code af-code="digi-tools-feedback" /> kan enkelt integreras
              och visas på sidan. Detta är ett användbart verktyg för att samla
              in feedback och förbättra användarupplevelsen.
            </digi-typography-preamble>
          )}
        </digi-typography>
        <digi-layout-container af-no-gutter af-margin-bottom>
          {!this.afShowOnlyExample && <h2>Exempel</h2>}
          <digi-code-example
            af-code={JSON.stringify(this.toolsFeedbackCode)}
            af-hide-controls={this.afHideControls ? 'true' : 'false'}
            af-hide-code={this.afHideCode ? 'true' : 'false'}>
            <div class="slot__controls" slot="controls">
              <digi-form-fieldset
                af-legend="Varianter"
                af-name="Varianter"
                onChange={(e) =>
                  (this.feedbackVariation = (e.target as HTMLFormElement).value)
                }>
                <digi-form-radiobutton
                  afName="Varianter"
                  afLabel="Primär"
                  afValue={FeedbackVariation.PRIMARY}
                  afChecked={
                    this.feedbackVariation === FeedbackVariation.PRIMARY
                  }
                />
                <digi-form-radiobutton
                  afName="Varianter"
                  afLabel="Sekundär"
                  afValue={FeedbackVariation.SECONDARY}
                  afChecked={
                    this.feedbackVariation === FeedbackVariation.SECONDARY
                  }
                />
              </digi-form-fieldset>

              <br />
              <digi-form-fieldset
                af-legend="Type"
                af-name="Typer"
                onChange={(e) =>
                  (this.toolsFeedbackType = (e.target as HTMLFormElement).value)
                }>
                <digi-form-radiobutton
                  afName="Type"
                  afLabel="Fullbredd"
                  afValue={ToolsFeedbackType.FULLWIDTH}
                  afChecked={
                    this.toolsFeedbackType === ToolsFeedbackType.FULLWIDTH
                  }
                />
                <digi-form-radiobutton
                  afName="Type"
                  afLabel="Grid"
                  afValue={ToolsFeedbackType.GRID}
                  afChecked={this.toolsFeedbackType === ToolsFeedbackType.GRID}
                />
              </digi-form-fieldset>

              <br />

              <div class="slot__controls" slot="controls">
                <digi-form-fieldset afLegend="Återställ komponent">
                  <digi-button
                    af-variation="function"
                    af-size="medium"
                    af-full-width="false"
                    id="reset"
                    onAfOnClick={(e) => this.handleResetClick(e)}>
                    <digi-icon-redo slot="icon" />
                    Återställ
                  </digi-button>

                  <br />
                </digi-form-fieldset>
              </div>
            </div>

            <div>
              <digi-tools-feedback
                af-text="Hjälpte informationen dig?"
                afVariation={this.feedbackVariation}
                id="feedback"
                af-is-done="false"
                af-type={this.toolsFeedbackType}
                ref={(e) => (this._feedback = e)}></digi-tools-feedback>
            </div>
          </digi-code-example>
        </digi-layout-container>
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-margin-bottom afNoGutter>
              <h2>Beskrivning</h2>
              <p>
                <digi-code af-code="af-is-done" /> används för att återställa
                komponentens värden när användaren har skickat feedback. I detta
                fall är den satt till "false", vilket innebär att inga värden
                kommer att återställas.
              </p>
              <p>
                Texten på frågan kan anges genom att använda taggen{' '}
                <digi-code af-code="af-text" /> .
              </p>
              <br />
              <p>
                <digi-code af-code="af-type" /> används för att ställa in typen
                av komponenten. I detta fall är det satt till fullbredd, vilket
                innebär att komponenten kommer att ta upp hela bredden av dess
                container.
              </p>
              <digi-layout-block
                af-variation="symbol"
                af-vertical-padding
                af-margin-bottom>
                <h2>Riktlinjer</h2>
                <digi-list>
                  <li>
                    Det finns två typer av komponenten för feedbackformulär:
                    Fullbredd och Grid. Fullbredd skapar en egen layout-block
                    som fyller ut utrymmet horisontellt, medan Grid lägger sig i
                    en annan layout-block och ärver samma stilsättning som
                    omgivande element.
                  </li>
                  <li>
                    För att lägga till formuläret efter att användaren har
                    klickat på "nej"-knappen, använd{' '}
                    <digi-code af-code="slot=form" />.
                  </li>
                  <li>
                    Se till att justeringen av feedback komponenten matchar
                    justeringen av tillhörande innehåll.
                  </li>
                </digi-list>
              </digi-layout-block>
            </digi-layout-container>
          )}{' '}
        </digi-typography>
      </div>
    );
  }
}
