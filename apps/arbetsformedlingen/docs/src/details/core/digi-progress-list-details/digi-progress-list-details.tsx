import { Component, h, State, Prop } from '@stencil/core';
import {
  CodeExampleLanguage,
  ProgressListVariation,
  ProgressListHeadingLevel,
  ProgressListType
} from '@digi/arbetsformedlingen';
@Component({
  tag: 'digi-progress-list-details',
  styleUrl: 'digi-progress-list-details.scss'
})
export class DigiProgressList {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() progressListVariation: ProgressListVariation =
    ProgressListVariation.PRIMARY;
  @State() progressListHeadingLevel: ProgressListHeadingLevel =
    ProgressListHeadingLevel.H2;
  @State() progressListType: ProgressListType = ProgressListType.CIRCLE;
  @State() currentProgressSteps = 2;
  @State() expandable = false;

  get progressListCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-progress-list
	af-current-step="${this.currentProgressSteps}"
	af-variation="${this.progressListVariation}"
	af-heading="Så går det till"
	af-heading-level="${this.progressListHeadingLevel}"
	af-type="${this.progressListType}"
	af-expandable="${this.expandable}"
>
	<digi-progress-list-item af-heading="Ansökan är mottagen">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem.
	</digi-progress-list-item>
	<digi-progress-list-item af-heading="Ärendet handläggs">
		<div>
			<h4>Underrubrik</h4>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			Etiam magna neque, interdum vel massa eget, condimentum
			rutrum velit. Sed vitae ullamcorper sem.
		</div>
	</digi-progress-list-item>
	<digi-progress-list-item af-heading="Ärendet granskas">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem.
	</digi-progress-list-item>
	<digi-progress-list-item af-heading="Beslut fattas ">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem.
	</digi-progress-list-item>
</digi-progress-list>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-progress-list
	[attr.af-current-step]="${this.currentProgressSteps}"
	[attr.af-variation]="ProgressListVariation.${Object.keys(ProgressListVariation).find((key) => ProgressListVariation[key] === this.progressListVariation)}"
	[attr.af-heading]="'Så går det till'"
	[attr.af-heading-level]="ProgressListHeadingLevel.${Object.keys(ProgressListHeadingLevel).find((key) => ProgressListHeadingLevel[key] === this.progressListHeadingLevel)}"
	[attr.af-type]="ProgressListType.${Object.keys(ProgressListType).find((key) => ProgressListType[key] === this.progressListType)}"
	[attr.af-expandable]="${this.expandable}"
>
	<digi-progress-list-item af-heading="Ansökan är mottagen">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem.
	</digi-progress-list-item>
	<digi-progress-list-item af-heading="Ärendet handläggs">
		<div>
			<h4>Underrubrik</h4>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			Etiam magna neque, interdum vel massa eget, condimentum
			rutrum velit. Sed vitae ullamcorper sem.
		</div>
	</digi-progress-list-item>
	<digi-progress-list-item af-heading="Ärendet granskas">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem.
	</digi-progress-list-item>
	<digi-progress-list-item af-heading="Beslut fattas">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem.
	</digi-progress-list-item>
</digi-progress-list>`,
      [CodeExampleLanguage.REACT]: `\
<DigiProgressList
	afCurrentStep={${this.currentProgressSteps}}
	afVariation={ProgressListVariation.${Object.keys(ProgressListVariation).find((key) => ProgressListVariation[key] === this.progressListVariation)}}
	afHeading="Så går det till"
	afHeadingLevel={ProgressListHeadingLevel.${Object.keys(ProgressListHeadingLevel).find((key) => ProgressListHeadingLevel[key] === this.progressListHeadingLevel)}}
	afType={ProgressListType.${Object.keys(ProgressListType).find((key) => ProgressListType[key] === this.progressListType)}}
	afExpandable={${this.expandable}}
>
	<DigiProgressListItem afHeading="Ansökan är mottagen">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem.
	</DigiProgressListItem>
	<DigiProgressListItem afHeading="Ärdendet handläggs">
		<div>
			<h4>Underrubrik</h4>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			Etiam magna neque, interdum vel massa eget, condimentum
			rutrum velit. Sed vitae ullamcorper sem.
		</div>
	</DigiProgressListItem>
	<DigiProgressListItem afHeading="Ärendet granskas">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem.
	</DigiProgressListItem>
		<DigiProgressListItem afHeading="Beslut fattas">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem.
	</DigiProgressListItem>
</DigiProgressList>`
    };
  }

  render() {
    return (
      <div class="digi-progress-list-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används tillsammans med{' '}
              <digi-code af-code="<digi-progress-list-item>" /> för att
              visualisera ett användarflöde i vertikal riktning.
            </digi-typography-preamble>
          )}
        </digi-typography>

        <digi-layout-container af-no-gutter af-margin-bottom>
          <article>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.progressListCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <digi-progress-list
                af-heading="Så går det till"
                af-current-step={this.currentProgressSteps}
                af-variation={this.progressListVariation}
                af-heading-level={this.progressListHeadingLevel}
                af-type={this.progressListType}
                af-expandable={this.expandable}>
                <digi-progress-list-item afHeading="Ansökan är mottagen">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                  magna neque, interdum vel massa eget, condimentum rutrum
                  velit. Sed vitae ullamcorper sem.
                </digi-progress-list-item>
                <digi-progress-list-item afHeading="Ärendet handläggs">
                  <h4>Underrubrik</h4>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                  magna neque, interdum vel massa eget, condimentum rutrum
                  velit. Sed vitae ullamcorper sem.
                </digi-progress-list-item>
                <digi-progress-list-item afHeading="Ärendet granskas">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                  magna neque, interdum vel massa eget, condimentum rutrum
                  velit. Sed vitae ullamcorper sem.
                </digi-progress-list-item>
                <digi-progress-list-item afHeading="Beslut fattas">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                  magna neque, interdum vel massa eget, condimentum rutrum
                  velit. Sed vitae ullamcorper sem.
                </digi-progress-list-item>
              </digi-progress-list>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variation"
                  afLegend="Variation"
                  onChange={(e) =>
                    (this.progressListVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Primär"
                    afValue={ProgressListVariation.PRIMARY}
                    afChecked={
                      this.progressListVariation ===
                      ProgressListVariation.PRIMARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Sekundär"
                    afValue={ProgressListVariation.SECONDARY}
                    afChecked={
                      this.progressListVariation ===
                      ProgressListVariation.SECONDARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Tertiär"
                    afValue={ProgressListVariation.TERTIARY}
                    afChecked={
                      this.progressListVariation ===
                      ProgressListVariation.TERTIARY
                    }
                  />
                </digi-form-fieldset>

                <digi-form-fieldset
                  afName="Typ"
                  afLegend="Typ"
                  onChange={(e) =>
                    (this.progressListType = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Typ"
                    afLabel="Cirkel"
                    afValue={ProgressListType.CIRCLE}
                    afChecked={
                      this.progressListType === ProgressListType.CIRCLE
                    }
                  />
                  <digi-form-radiobutton
                    afName="Typ"
                    afLabel="Ikon"
                    afValue={ProgressListType.ICON}
                    afChecked={this.progressListType === ProgressListType.ICON}
                  />
                </digi-form-fieldset>

                <digi-form-fieldset afName="Expanderbar" afLegend="Expanderbar">
                  <digi-form-checkbox
                    afLabel="Ja"
                    afChecked={this.expandable}
                    onAfOnChange={() =>
                      this.expandable
                        ? (this.expandable = false)
                        : (this.expandable = true)
                    }></digi-form-checkbox>
                </digi-form-fieldset>
                <digi-form-select
                  afLabel="Nuvarande steg"
                  onAfOnChange={(e) =>
                    (this.currentProgressSteps = parseInt(
                      (e.target as HTMLDigiFormSelectElement).value
                    ))
                  }
                  af-variation="small"
                  af-start-selected={this.currentProgressSteps - 1}>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </digi-form-select>
                <digi-form-select
                  afLabel="Rubriknivå"
                  onAfOnChange={(e) =>
                    (this.progressListHeadingLevel = (
                      e.target as HTMLDigiFormSelectElement
                    ).value as ProgressListHeadingLevel)
                  }
                  af-variation="small"
                  af-start-selected={1}>
                  <option value={ProgressListHeadingLevel.H1}>
                    {ProgressListHeadingLevel.H1}
                  </option>
                  <option value={ProgressListHeadingLevel.H2}>
                    {ProgressListHeadingLevel.H2}
                  </option>
                  <option value={ProgressListHeadingLevel.H3}>
                    {ProgressListHeadingLevel.H3}
                  </option>
                  <option value={ProgressListHeadingLevel.H4}>
                    {ProgressListHeadingLevel.H4}
                  </option>
                  <option value={ProgressListHeadingLevel.H5}>
                    {ProgressListHeadingLevel.H5}
                  </option>
                  <option value={ProgressListHeadingLevel.H6}>
                    {ProgressListHeadingLevel.H6}
                  </option>
                </digi-form-select>
              </div>
            </digi-code-example>
          </article>
        </digi-layout-container>
        {!this.afShowOnlyExample && (
          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              <h2>Beskrivning</h2>
              <p>
                Rubriknivån för alla förloppsteg kan ändras med &nbsp;
                <digi-code af-code="af-heading-level" />, standard är H2.
                Typescript-användare bör importera och använda &nbsp;
                <digi-code af-code="ProgressListHeadingLevel" />. Stylingen på
                kommer se likadan ut även om du ändrar till en annan rubriknivå.
              </p>

              <h3>Variationer</h3>
              <p>
                Komponenten finns i tre varianter, en primär (
                <span lang="en">primary</span>) i grönt som är standard; en
                sekundär (<span lang="en">secondary</span>) i blått och en
                tertiär (<span lang="en">tertiary</span>) som är i grått.
                Variation sätts med hjälp av &nbsp;
                <digi-code af-code="af-variation" />.
              </p>
              <h3>Typ</h3>
              <p>
                Indikatorn kan vara en cirkel (<span lang="en">circle</span>)
                eller ikon (<span lang="en">icon</span>). Standard är cirkel och
                sätts med hjälp av &nbsp;
                <digi-code af-code="af-type" />.
              </p>
              <h3>Expanderbar</h3>
              <p>
                Komponenten kan väljas att vara expanderbar och sätts i så fall
                med attributet &nbsp;
                <digi-code af-code="af-expandable" />. Det går även att välja
                egna ikoner till knappen som expanderar och kollapsar varje
                item. Detta görs genom &nbsp;
                <digi-code af-code="af-expand-icon" />
                &nbsp; och &nbsp;
                <digi-code af-code="af-collapse-icon" />. Exempel för att sätta
                ett plustecken: &nbsp;
                <digi-code af-code='af-expand-icon="plus"' />.
              </p>
            </article>
          </digi-layout-container>
        )}
      </div>
    );
  }
}
