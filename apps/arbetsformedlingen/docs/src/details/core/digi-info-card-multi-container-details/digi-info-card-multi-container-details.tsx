import { Component, Prop, h, State } from '@stencil/core';
import {
  InfoCardVariation,
  InfoCardMultiType,
  InfoCardMultiHeadingLevel,
  InfoCardMultiContainerHeadingLevel,
  CodeExampleLanguage,
  FormCheckboxVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-info-card-multi-container-details',
  styleUrl: 'digi-info-card-multi-container-details.scss'
})
export class DigiInfoCardMultiContainerDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() heading = false;

  get infoCardContainerCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-info-card-multi-container ${this.heading ? '\n\t af-heading = "Rubrik till infokortbehållare">' : '>'}
	<digi-info-card-multi
		af-heading="Jag är ett infokort av typen multi"
		af-heading-level="h2"
		af-type="related"
		af-link-text="Beskrivande länktext"
		af-link-href="/"
>
		<p>
			Det här är bara ord för att illustrera hur det ser ut med text
			inuti. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			Suspendisse commodo egestas elit in consequat. Proin in ex
			consectetur, laoreet augue sit amet, malesuada tellus.
		</p>
	</digi-info-card-multi>
	<digi-info-card-multi>
		...
	</digi-info-card-multi>
	<digi-info-card-multi>
		...
	</digi-info-card-multi>
</digi-info-card-multi-container>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-info-card-multi-container ${this.heading ? '\n\t [attr.af-heading] = "Rubrik till infokortbehållare">' : '>'}
	<digi-info-card-multi
		[attr.af-heading]="'Jag är ett infokort av typen multi'"
		[attr.af-heading-level]="InfoCardMultiHeadingLevel.H2"
		[attr.af-type]="InfoCardMultiType.RELATED"
		[attr.af-link-text]="'Beskrivande länktext'"
		[attr.af-link-href]="/"
>
		<p>
			Det här är bara ord för att illustrera hur det ser ut med text
			inuti. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			Suspendisse commodo egestas elit in consequat. Proin in ex
			consectetur, laoreet augue sit amet, malesuada tellus.
		</p>
	</digi-info-card-multi>
	<digi-info-card-multi>
		...
	</digi-info-card-multi>
	<digi-info-card-multi>
		...
	</digi-info-card-multi>
</digi-info-card-multi-container>`,
      [CodeExampleLanguage.REACT]: `\
<DigiInfoCardMultiContainer ${this.heading ? '\n\t afHeading="Rubrik till infokortbehållare">' : '>'}
	<DigiInfoCardMulti
		afHeading="Jag är ett infokort av typen multi"
		afHeadingLevel={InfoCardMultiHeadingLevel.H2}
		afType={InfoCardMultiType.RELATED}
		afLinkText="Beskrivande länktext"
		afLinkHref="/"
>
		<p>
			Det här är bara ord för att illustrera hur det ser ut med text
			inuti. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			Suspendisse commodo egestas elit in consequat. Proin in ex
			consectetur, laoreet augue sit amet, malesuada tellus.
		</p>
	</DigiInfoCardMulti>
	<DigiInfoCardMulti>
		...
	</DigiInfoCardMulti>
	<DigiInfoCardMulti>
		...
	</DigiInfoCardMulti>
</DigiInfoCardMultiContainer>`
    };
  }
  render() {
    return (
      <div class="digi-info-card-multi-container-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Infokortbehållaren används av infokortkomponenten för att visa upp
              flera infokort bredvid varandra.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.infoCardContainerCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <div style={{ width: '100%' }}>
                  {this.heading && (
                    <digi-info-card-multi-container
                      afHeading="Rubrik till infokortbehållare"
                      afHeadingLevel={InfoCardMultiContainerHeadingLevel.H2}>
                      <digi-info-card-multi
                        afHeading="Jag är ett infokort av typen multi"
                        afHeadingLevel={InfoCardMultiHeadingLevel.H3}
                        af-variation={InfoCardVariation.PRIMARY}
                        afType={InfoCardMultiType.RELATED}
                        afLinkHref="/">
                        <p>
                          Det här är bara ord för att illustrera hur det ser ut
                          med text inuti. Lorem ipsum dolor sit amet,
                          consectetur adipiscing elit. Suspendisse commodo
                          egestas elit in consequat. Proin in ex consectetur,
                          laoreet augue sit amet, malesuada tellus.
                        </p>
                      </digi-info-card-multi>
                      <digi-info-card-multi
                        afHeading="Jag är ett infokort av typen multi"
                        afHeadingLevel={InfoCardMultiHeadingLevel.H3}
                        af-variation={InfoCardVariation.PRIMARY}
                        afType={InfoCardMultiType.RELATED}
                        afLinkHref="/">
                        <p>
                          Det här är bara ord för att illustrera hur det ser ut
                          med text inuti. Lorem ipsum dolor sit amet,
                          consectetur adipiscing elit. Suspendisse commodo
                          egestas elit in consequat. Proin in ex consectetur,
                          laoreet augue sit amet, malesuada tellus.
                        </p>
                      </digi-info-card-multi>
                      <digi-info-card-multi
                        afHeading="Jag är ett infokort av typen multi"
                        afHeadingLevel={InfoCardMultiHeadingLevel.H3}
                        af-variation={InfoCardVariation.PRIMARY}
                        afType={InfoCardMultiType.RELATED}
                        afLinkHref="/">
                        <p>
                          Det här är bara ord för att illustrera hur det ser ut
                          med text inuti. Lorem ipsum dolor sit amet,
                          consectetur adipiscing elit. Suspendisse commodo
                          egestas elit in consequat. Proin in ex consectetur,
                          laoreet augue sit amet, malesuada tellus.
                        </p>
                      </digi-info-card-multi>
                    </digi-info-card-multi-container>
                  )}
                  {!this.heading && (
                    <digi-info-card-multi-container>
                      <digi-info-card-multi
                        afHeading="Jag är ett infokort av typen multi"
                        afHeadingLevel={InfoCardMultiHeadingLevel.H3}
                        af-variation={InfoCardVariation.PRIMARY}
                        afType={InfoCardMultiType.RELATED}
                        afLinkHref="/">
                        <p>
                          Det här är bara ord för att illustrera hur det ser ut
                          med text inuti. Lorem ipsum dolor sit amet,
                          consectetur adipiscing elit. Suspendisse commodo
                          egestas elit in consequat. Proin in ex consectetur,
                          laoreet augue sit amet, malesuada tellus.
                        </p>
                      </digi-info-card-multi>
                      <digi-info-card-multi
                        afHeading="Jag är ett infokort av typen multi"
                        afHeadingLevel={InfoCardMultiHeadingLevel.H3}
                        af-variation={InfoCardVariation.PRIMARY}
                        afType={InfoCardMultiType.RELATED}
                        afLinkHref="/">
                        <p>
                          Det här är bara ord för att illustrera hur det ser ut
                          med text inuti. Lorem ipsum dolor sit amet,
                          consectetur adipiscing elit. Suspendisse commodo
                          egestas elit in consequat. Proin in ex consectetur,
                          laoreet augue sit amet, malesuada tellus.
                        </p>
                      </digi-info-card-multi>
                      {!this.afShowOnlyExample && (
                        <digi-info-card-multi
                          afHeading="Jag är ett infokort av typen multi"
                          afHeadingLevel={InfoCardMultiHeadingLevel.H3}
                          af-variation={InfoCardVariation.PRIMARY}
                          afType={InfoCardMultiType.RELATED}
                          afLinkHref="/">
                          <p>
                            Det här är bara ord för att illustrera hur det ser
                            ut med text inuti. Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit. Suspendisse commodo
                            egestas elit in consequat. Proin in ex consectetur,
                            laoreet augue sit amet, malesuada tellus.
                          </p>
                        </digi-info-card-multi>
                      )}
                    </digi-info-card-multi-container>
                  )}
                </div>
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset afLegend="Rubrik">
                    <digi-form-checkbox
                      afLabel="Ja"
                      afVariation={FormCheckboxVariation.PRIMARY}
                      afChecked={this.heading}
                      onAfOnChange={() =>
                        this.heading
                          ? (this.heading = false)
                          : (this.heading = true)
                      }></digi-form-checkbox>
                  </digi-form-fieldset>
                </div>
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <p>
                Infokortbehållarkomponenten är avsedd att linda in 1-3{' '}
                <digi-code af-code="<digi-info-card-multi />" /> komponenter.
              </p>
              <h3>Varianter</h3>
              <p>
                <digi-code af-code="af-heading" /> ger behållaren en frivillig
                rubrik och
                <digi-code af-code="af-heading-level" /> sätter storleken på
                behållarens rubrik.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
