/* eslint-disable no-constant-condition */
import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  FormFileUploadValidation,
  FormFileUploadVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-form-file-upload-details',
  styleUrl: 'digi-form-file-upload-details.scss'
})
export class DigiFormFileUpload {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() showThumbnail = false;
  @State() enablePreview = false;
  @State() showValidationText = false;
  @State() fileUploadExampleData: { [key: string]: unknown } = {
    variation: FormFileUploadVariation.SMALL,
    validation: FormFileUploadValidation.ENABLED
  };
  errorMessage = 'Du behöver bifoga ditt CV innan du kan skicka in formuläret';

  resp = [
    {
      file_id: '44d88612fea8a8f36de82e1278abb02f',
      status: 'error',
      details: {
        confidence: 5,
        malware_family: 54768315,
        malware_type: 114,
        severity: 4,
        signature_name: 'Trojan.Win32.Mitaka.TC.a'
      }
    },
    {
      file_id: '3e74f8be1ca4e3acaf843c8b4f5ef6cc',
      status: 'OK',
      details: {
        confidence: 0,
        severity: 0,
        signature_name: ''
      }
    }
  ];

  get fileUploadExampleCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-form-file-upload 
    af-variation="${this.fileUploadExampleData.variation}"
    af-validation="${this.fileUploadExampleData.validation}"
    af-file-types="*"\
    ${this.showThumbnail ? `\n\taf-show-thumbnail="true"\t` : ''}\
    ${this.enablePreview ? `\n\taf-enable-preview="true"\t` : ''}\
    ${this.showValidationText ? `\n\taf-validation-text="${this.errorMessage}"\t` : ''}
></digi-form-file-upload>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-form-file-upload 
    [attr.af-variation]="FormFileUpload.${Object.keys(FormFileUploadVariation).find((key) => FormFileUploadVariation[key] === this.fileUploadExampleData.variation)}"
    [attr.af-validation]="FormFileUpload.${Object.keys(FormFileUploadValidation).find((key) => FormFileUploadValidation[key] === this.fileUploadExampleData.validation)}"
    [attr.af-file-types]="*"\
    ${this.showThumbnail ? `\n\t[attr.af-show-thumbnail]="'true'"\t` : ''}\
    ${this.enablePreview ? `\n\t[attr.af-enable-Preview]="'true'"\t` : ''}\
    ${this.showValidationText ? `\n\t[attr.af-validation-text]="${this.errorMessage}"\t` : ''}
></digi-form-file-upload>`,
      [CodeExampleLanguage.REACT]: `\
<DigiFormFileUpload 
    afVariation={FormFileUploadVariation.${Object.keys(FormFileUploadVariation).find((key) => FormFileUploadVariation[key] === this.fileUploadExampleData.variation)}}
    afValidation={FormFileUploadValidation.${Object.keys(FormFileUploadValidation).find((key) => FormFileUploadValidation[key] === this.fileUploadExampleData.validation)}}
    afFileTypes="*"\
    ${this.showThumbnail ? `\n\tafShowThumbnail="true"\t` : ''}\
    ${this.enablePreview ? `\n\tafEnablePreview="true"\t` : ''}\
    ${this.showValidationText ? `\n\tafValidationText="${this.errorMessage}"\t` : ''}
></DigiFormFileUpload>`
    };
  }

  @State() showExample = true;

  changeFileUploadExampleData(newData) {
    this.showExample = false;
    setTimeout(() => (this.showExample = true), 0);
    this.fileUploadExampleData = newData;
  }

  changeVariationHandler(e) {
    this.changeFileUploadExampleData({
      ...this.fileUploadExampleData,
      variation: e.target.value
    });
  }

  changeValidationHandler(e) {
    this.changeFileUploadExampleData({
      ...this.fileUploadExampleData,
      validation: e.target.value
    });
  }

  async validateFile(scanResp) {
    await customElements.whenDefined('digi-form-file-upload');
    const paginationElement = document.querySelector('digi-form-file-upload');
    await paginationElement?.afMValidateFile(scanResp);
  }

  onRemove(e) {
    console.log(e.detail);
  }

  onCancel(e) {
    console.log(e.detail);
  }

  onUpload(e) {
    setTimeout(() => {
      this.mockValidation(e.detail);
    }, 2000);
  }

  mockValidation(file) {
    const copiedFile = new File([file], file.name, { type: file.type });
    copiedFile['id'] = file.id;
    copiedFile['status'] = 'OK';
    setTimeout(() => {
      this.sendValidatedFileToComponent(copiedFile);
    }, 1000);
  }

  sendValidatedFileToComponent(file) {
    const $fileUploadElement = document.querySelector(
      '#digi-file-upload-example'
    ) as any;
    $fileUploadElement.afMValidateFile(file);
  }

  render() {
    return (
      <div class="digi-form-file-upload-details">
        {!this.afShowOnlyExample && (
          <digi-typography-preamble>
            Filuppladdaren används till att ladda upp en eller flera filer. Vi
            rekommenderar alla att skanna filerna efter säkerhetsrisker via
            Byggstenen Skyddas API innan filerna används i ert system eller
            sparas. Exemplet nedan är inte anslutet till antivirustjänsten och
            visar därför inte den funktionaliteten till fullo.
          </digi-typography-preamble>
        )}
        <digi-layout-container af-no-gutter af-margin-bottom>
          {!this.afShowOnlyExample && <h2>Exempel</h2>}
          <digi-code-example
            af-code={JSON.stringify(this.fileUploadExampleCode)}
            af-hide-controls={this.afHideControls ? 'true' : 'false'}
            af-hide-code={this.afHideCode ? 'true' : 'false'}
            af-controls-position="end">
            <div class="slot__controls" slot="controls">
              <digi-form-fieldset
                af-legend="Variant"
                af-name="Variant"
                onChange={(e) => this.changeVariationHandler(e)}>
                <digi-form-radiobutton
                  af-name="Variant"
                  afLabel="Liten"
                  afValue={FormFileUploadVariation.SMALL}
                  afChecked={true}
                />
                <digi-form-radiobutton
                  af-name="Variant"
                  afLabel="Mellan"
                  afValue={FormFileUploadVariation.MEDIUM}
                />
                <digi-form-radiobutton
                  af-name="Variant"
                  afLabel="Stor"
                  afValue={FormFileUploadVariation.LARGE}
                />
              </digi-form-fieldset>

              <digi-form-fieldset afLegend="Tumnagelvy">
                <digi-form-checkbox
                  afChecked={false}
                  afLabel={'this.aFchecked' ? 'Ja' : 'Nej'}
                  onAfOnChange={() =>
                    this.showThumbnail
                      ? (this.showThumbnail = false)
                      : (this.showThumbnail = true)
                  }></digi-form-checkbox>
              </digi-form-fieldset>

              <digi-form-fieldset afLegend="Förhandsgranskning">
                <digi-form-checkbox
                  afChecked={false}
                  afLabel={'this.aFchecked' ? 'Ja' : 'Nej'}
                  onAfOnChange={() =>
                    this.enablePreview
                      ? (this.enablePreview = false)
                      : (this.enablePreview = true)
                  }></digi-form-checkbox>
              </digi-form-fieldset>

              <digi-form-fieldset afLegend="Felmeddelande">
                <digi-form-checkbox
                  afChecked={false}
                  afLabel={'this.showValidationText' ? 'Visa' : 'Dölj'}
                  onAfOnChange={() =>
                    this.showValidationText
                      ? (this.showValidationText = false)
                      : (this.showValidationText = true)
                  }></digi-form-checkbox>
              </digi-form-fieldset>

              <digi-form-fieldset
                af-legend="Validering"
                af-name="Validering"
                onChange={(e) => this.changeValidationHandler(e)}>
                <digi-form-radiobutton
                  af-name="Validering"
                  afLabel="På"
                  afValue={FormFileUploadValidation.ENABLED}
                  afChecked={true}
                />
                <digi-form-radiobutton
                  af-name="Validering"
                  afLabel="Av"
                  afValue={FormFileUploadValidation.DISABLED}
                />
              </digi-form-fieldset>
            </div>
            {this.showExample && (
              <digi-form-file-upload
                id="digi-file-upload-example"
                onAfOnCancelFile={(e) => this.onCancel(e)}
                onAfOnRemoveFile={(e) => this.onRemove(e)}
                onAfOnUploadFile={(e) => this.onUpload(e)}
                af-variation={this.fileUploadExampleData.variation}
                af-validation={this.fileUploadExampleData.validation}
                afShowThumbnail={this.showThumbnail}
                afEnablePreview={this.enablePreview}
                afValidationText={
                  this.showValidationText ? this.errorMessage : ''
                }
                afFileTypes={'*'}></digi-form-file-upload>
            )}
          </digi-code-example>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom afMarginTop>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <p>
                Filuppladdaren finns i tre varianter, liten (small), mellan
                (medium) och stor (large), som väljs med{' '}
                <digi-code af-variation="light" af-code="af-variation" />. Tänk
                på att använda en lämplig variant beroende på var komponenten
                placeras och utrymmet som ges till den.
              </p>
              <p>
                Varianten <span lang="en">"small"</span> passar för mobila
                enheter och filuppladdning sker via tryck på knapp.
              </p>
              <p>
                Varianterna <span lang="en">"medium"</span> och{' '}
                <span lang="en">"large"</span> passar större skärmar och har
                drag-och-släpp-funktionalitet. Komponenten blir en{' '}
                <span lang="en">"small"</span> variant per automatik på mindre
                skärmar.
              </p>
              <h3>Tumnagelvy</h3>
              <p>
                Tumnagelvyn stödjer endast pdf och bilder i formaten .jpg .jpeg
                och .png.
              </p>
              <h3>Validering och säkerhetsskanning av filer</h3>
              <h4>Validera filernas format, storlek och antal</h4>
              <p>
                Filuppladdaren validerar alla filer den tar emot utifrån hur
                komponenten konfigurerats. Exempelvis om du specificerat att den
                bara ska ta emot filer i formatet .pdf så accepterar den bara
                filer i formatet .pdf. Uppfyller uppladdad fil inte kravet så
                visas ett felmeddelande.
              </p>
              <h4>Skanna filer efter säkerhetsbrister och virus</h4>
              <p>
                Som standard är ytterligare validering påslagen. Den innebär att
                efter att komponenten validerat filerna så skickar den dessa för
                ytterligare säkerhetsskanning. Ni som team behöver ta emot
                filerna från komponenten och sedan skicka dom vidare för
                virusskanning.
              </p>
              <p>
                Berätta sedan för komponenten om filen blivit godkänd eller inte
                med hjälp av metoden
                <span lang="en">
                  {' '}
                  <digi-code af-variation="light" af-code=" afMValidateFile" />
                </span>
                .{' '}
                <span lang="en">
                  {' '}
                  <digi-code af-variation="light" af-code=" afMValidateFile" />
                </span>{' '}
                förväntar sig ett filobjekt med filens id, namn och status samt
                filen själv. Filens status kan antigen vara "OK" eller{' '}
                <span lang="en">"error"</span>. Har filen status{' '}
                <span lang="en">"error"</span> så beskriv felet via propertyn{' '}
                <span lang="en">"error"</span> på fil-objektet, se kodexempel
                nedan.
              </p>
              <digi-code-block
                af-language="typescript"
                af-code="
const $uploadComponent = document.getElementByTagName('digi-form-file-upload');

function handleFile(file: File) {
  const hasErrors = validateFile(file); // Validera filen via API eller själva
  file['status'] = hasErrors ? 'error': 'OK';

  /* Om filen har fel så skicka med ett bra meddelande som beskriver felet och vad användaren
   kan göra för att rätta till det. Är filen osäker så kan du skriva som följer:  */
  file['error'] = hasErrors ? 'Filen ' + file.name + ' har bedömts som osäker' : '';
  $uploadComponent.afMValidateFile(file)
}

$uploadComponent.addEventListener('afOnUploadFile', (e: CustomEvent) => {
  handleFile(e.detail);
});

"
                af-variation="light"></digi-code-block>
              <h4>Sammanfattning</h4>
              <digi-list>
                <li>Komponenten validerar filerna utifrån er konfiguration.</li>
                <li>
                  Efter det skickar den dom filer som klarat valideringen vidare
                  till applikationen som skannar filerna efter säkerhetsbrister.
                </li>
                <li>
                  När filen kommer tillbaks från skanning så skickas den
                  tillbaks till komponenten som visar godkänd fil eller ett
                  felmeddelande.
                </li>
              </digi-list>

              <h3>Konfigurera komponenten</h3>
              <digi-list>
                <li>
                  Du kan ge inmatningsfältet i komponenten ett id genom att
                  använda <digi-code af-variation="light" af-code=" af-id" />.
                  Anges inget kommer ett genereras automatiskt.
                </li>
                <li>
                  Attributet{' '}
                  <digi-code af-variation="light" af-code=" af-max-files" />{' '}
                  anger det maximala antalet filer som kan laddas upp.
                </li>
                <li>
                  Med attributet{' '}
                  <digi-code af-variation="light" af-code=" af-file-max-size" />{' '}
                  anges den maximala filstorlek som kan laddas upp.{' '}
                </li>
                <li>
                  Attributet{' '}
                  <digi-code af-variation="light" af-code=" af-file-types" />{' '}
                  anger vilka filtyper som är accepterade, formatet skrivs i
                  samma form som ett accept-attribut för{' '}
                  <digi-code af-code="<input [type=file] />"></digi-code>.
                  Obligatoriskt!{' '}
                </li>
                <li>
                  Sätt rubrik för uppladdade filer genom att sätta attributet{' '}
                  <digi-code af-variation="light" af-code="af-heading-files" />.
                </li>
                <li>
                  Sätt rubiknivå genom att sätta attributet{' '}
                  <digi-code af-variation="light" af-code="af-heading-level" />.
                </li>
                <li>
                  Ange filuppladdarens etikett genom att använda{' '}
                  <digi-code af-variation="light" af-code=" af-label" />.
                </li>
                <li>
                  Med attributet{' '}
                  <digi-code
                    af-variation="light"
                    af-code=" af-label-description"
                  />{' '}
                  anger du en beskrivningstext. Här informerar du användaren om
                  vilka filtyper som är godkända, max antal filer, max storlek
                  på filer och eventuell annan information som gör det lätt för
                  användaren att göra rätt. <br />
                  Exempel: "Här bifogar du ditt CV, personliga brev och
                  eventuella referenser. Du kan bifoga filer av typerna .pdf och
                  .doc. Filerna får inte vara större än 10 Megabyte eller fler
                  än fyra.".
                </li>
                <li>
                  Ändra{' '}
                  <digi-code af-variation="light" af-code=" af-file-types" />{' '}
                  till{' '}
                  <digi-code
                    af-variation="light"
                    af-code="af-file-types='.jpg, .jpeg, .png, .pdf'"
                  />{' '}
                  om du ska ha tumnagelvy.
                </li>
                <li>
                  Med attributet{' '}
                  <digi-code
                    af-variation="light"
                    af-code=" af-upload-btn-text"
                  />{' '}
                  anger du text på uppladdningsknappen.{' '}
                </li>
                <li>
                  Kalla på metoden{' '}
                  <digi-code af-variation="light" af-code=" afMGetAllFiles " />{' '}
                  så returnerar metoden alla uppladdade godkända filer.
                </li>
                <li>
                  Du kan importera en array med fil-objekt med metoden
                  <digi-code af-variation="light" af-code=" afMImportFiles " />.
                  Filerna kommer inte trigga uppladdningsevent, de kommer endast
                  visas upp med den informationen som de importades med. Ett
                  fil-objekt måste innehålla id, status och filen själv. Har
                  filen ett felmeddelade kopplat till sig ska det skickas med,
                  under <span lang="en">"error"</span>. Status kan ha värdet
                  "OK", <span lang="en">"pending"</span>, eller{' '}
                  <span lang="en">"error"</span>. Bara filer som har status OK
                  och <span lang="en">pending</span> visas i komponenten.
                  Fil-dubbletter läggs inte till i komponenten.
                </li>
              </digi-list>
            </digi-layout-container>
          )}
        </digi-layout-container>
      </div>
    );
  }
}
