import { Component, Prop, h, State } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-navigation-pagination-details',
  styleUrl: 'digi-navigation-pagination-details.scss'
})
export class DigiNavigationPaginationDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() totalPages = 6;
  @State() showResultName = false;
  @State() hasMorePages = false;
  @State() hasLessPages = false;

  get navigationPaginationCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-navigation-pagination
${
  this.showResultName
    ? `\
	af-total-pages="${this.hasMorePages ? 10 : 6}"
	af-init-active-page="1"
	af-current-result-start="1"
	af-current-result-end="25"
	af-total-results="${this.hasMorePages ? 10 : 6}"
	af-result-name="annonser"`
    : `\
	af-total-pages="${this.hasMorePages ? 10 : 6}"
	af-init-active-page="1"`
}
>
</digi-navigation-pagination>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-pagination
${
  this.showResultName
    ? `\
	[attr.af-total-pages]="6"
	[attr.af-init-active-page]="1"
	[attr.af-current-result-start]="1"
	[attr.af-current-result-end]="25"
	[attr.af-total-results]="10"
	[attr.af-result-name]="'annonser'"`
    : `\
  [attr.af-total-pages]="${this.hasMorePages ? 10 : 6}"
  [attr.af-init-active-page]="1"`
}
>
</digi-navigation-pagination>`,
      [CodeExampleLanguage.REACT]: `\
<DigiNavigationPagination
${
  this.showResultName
    ? `\
	afTotalPages={6}
	afInitActive-page={1}
	afCurrentResultStart={1}
	afCurrentResultEnd={25}
	afTotalResults={10}
	afResultName="annonser"`
    : ` afTotalPages={${this.hasMorePages ? 10 : 6}}
	afInitActivePage={1}`
}
>
</DigiNavigationPagination>`
    };
  }

  render() {
    return (
      <div class="digi-navigation-pagination-details">
        <digi-typography>
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.navigationPaginationCode)}
              af-controls-position="end"
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset>
                  <digi-form-checkbox
                    afLabel="Resultatblock"
                    afChecked={this.showResultName}
                    onAfOnChange={() =>
                      this.showResultName
                        ? (this.showResultName = false)
                        : (this.showResultName = true)
                    }
                  />
                </digi-form-fieldset>
                <digi-form-fieldset>
                  <digi-form-checkbox
                    afLabel="Över 9 sidor"
                    afChecked={this.hasMorePages}
                    onAfOnChange={() =>
                      this.hasMorePages
                        ? (this.hasMorePages = false)
                        : (this.hasMorePages = true)
                    }
                  />
                </digi-form-fieldset>
              </div>
              {this.showResultName && (
                <digi-navigation-pagination
                  afTotalPages={this.hasMorePages ? 10 : 6}
                  afInitActivePage={1}
                  afCurrentResultStart={1}
                  afCurrentResultEnd={25}
                  afTotalResults={this.hasMorePages ? 10 : 6}
                  afResultName={'annonser'}></digi-navigation-pagination>
              )}
              {!this.showResultName && (
                <digi-navigation-pagination
                  afTotalPages={this.hasMorePages ? 10 : 6}
                  afInitActivePage={1}></digi-navigation-pagination>
              )}
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <h4>Med resultatblock</h4>
              <p>
                <digi-code af-code="af-total-pages" /> anger hur många sidor
                pagineringen innehåller och
                <digi-code af-code="af-init-active-page" /> anger vilken sida
                som ska visas som förvald. Om inget anges är första (1) sidan
                vald.
              </p>
              <h4>Utan resultatblock</h4>
              <p>
                Genom att inte använda <digi-code af-code="af-total-results" />{' '}
                renderas inte resultatfältet och då behövs inte heller
                <digi-code af-code="af-result-name" />,
                <digi-code af-code="af-current-result-start" /> eller
                <digi-code af-code="af-current-result-end" /> skickas med.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
