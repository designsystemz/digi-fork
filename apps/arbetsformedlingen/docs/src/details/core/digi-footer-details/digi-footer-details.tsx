import { Component, Fragment, h, Host, Prop, State } from '@stencil/core';
import { CodeExampleLanguage, FooterVariation } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-footer-details',
  styleUrl: 'digi-footer-details.scss'
})
export class DigiFooter {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() afVariation: FooterVariation = FooterVariation.LARGE;
  @State() showExample = true;

  changeVariationHandler(e) {
    this.afVariation = (e.target as HTMLFormElement).value;
    this.showExample = false;
    setTimeout(() => (this.showExample = true), 0);
  }

  get badgeStatusCode() {
    return {
      [CodeExampleLanguage.HTML]: `<digi-footer af-variation="${this.afVariation}">
  <div slot="content-top">
    <div>
      <digi-footer-card af-type="icon">
        <ul>
          <li>
            <a href="#">
              <digi-icon-accessibility-universal></digi-icon-accessibility-universal>
              Tillgänglighetsredogörelse
            </a>
          </li>
          <li>
            <a href="#">
              <digi-icon-sign></digi-icon-sign>
              Teckenspråk
            </a>
          </li>
          <li>
            <a href="#">
              <digi-icon-globe></digi-icon-globe>
              Other languages
            </a>
          </li>
          <li>
            <a href="#">
              <digi-icon-envelope></digi-icon-envelope>
              Mejla vår funktionbrevlåda
            </a>
          </li>
        </ul>
      </digi-footer-card>
    </div>
${
  this.afVariation === FooterVariation.LARGE
    ? `    <div>
      <digi-footer-card>
        <ul>
          <li>
            <a href="#">Tillgänglighetsredogörelse</a>
          </li>
          <li>
            <a href="#">Teckenspråk</a>
          </li>
          <li>
            <a href="#">Other languages</a>
            <br />
          </li>
          <li>
            <a href="#">Mejla vår funktionbrevlåda</a>
          </li>
          <li>
            <a href="#">Mejla vår funktionbrevlåda</a>
          </li>
          <li>
            <a href="#">Mejla vår funktionbrevlåda</a>
          </li>
        </ul>
      </digi-footer-card>
    </div>
    <div>
      <digi-footer-card af-type="icon">
        <h2>Fokus abacus dolores</h2>
        <p>
        Vivamus feugiat nunc vel enim fermentum dolores ab. Nuj vel enim fermentum dolores abacus. Vivanna helam conkista
        </p>
        <a href="#"><digi-icon-external-link-alt></digi-icon-external-link-alt>
          Mejla vår funktionbrevlåda
        </a>
      </digi-footer-card>
    </div>
    <div>
      <digi-footer-card af-type="border">
        <a href="#">Forium dolores</a>
        <p>Vivamus feugiat nunc vel enim fermentum, ac aliquet tortor.</p>
      </digi-footer-card>
      <digi-footer-card af-type="border">
        <a href="#">Leveante lipsum</a>
        <p>Vivamus feugiat nunc vel enim.</p>
      </digi-footer-card>
      <digi-footer-card af-type="border">
        <a href="#">Acta estium</a>
        <p>fermentum, ac aliquet tortor cursus. Curabitur cursus eros.</p>
      </digi-footer-card>
    </div>`
    : `   <div>
      <digi-footer-card af-type="border">
        <a href="#">Om tjänsten dolores</a>
        <p>Systemversion: 1.4.0 <br/> Ansvarig: Jenny Svensson</p>
      </digi-footer-card>
    </div>
    <div>
      <digi-footer-card af-type="border">
        <a href="#">Kontakta servicdolores</a>
        <p>Telefon: 0771-60 0001 <br/> Öppettider: Vardagar 08:00-16:30</p>
      </digi-footer-card>
    </div>`
}
  </div>
  <div slot="content-bottom-left">
    <digi-link af-href="#">
      <digi-logo af-variation="large" af-color="secondary"></digi-logo>
    </digi-link>
  </div>
  <div slot="content-bottom-right">
    <p>Följ oss på</p>
    <a href="#">Facebook</a>
    <a href="#">Youtube</a>
    <a href="#">Linkedin</a>
    <a href="#">Instagram</a>
  </div>
</digi-footer>`,
      [CodeExampleLanguage.ANGULAR]: `<digi-footer [attr.af-variation]="FooterVariation.${Object.keys(FooterVariation).find((key) => FooterVariation[key] === this.afVariation)}">
  <div slot="content-top">
    <div>
      <digi-footer-card [attr.af-type]="FooterCardVariation.ICON">
        <ul>
          <li>
            <a href="#">
              <digi-icon-accessibility-universal></digi-icon-accessibility-universal>
              Tillgänglighetsredogörelse
            </a>
          </li>
          <li>
            <a href="#">
              <digi-icon-sign></digi-icon-sign>
              Teckenspråk
            </a>
          </li>
          <li>
            <a href="#">
              <digi-icon-globe></digi-icon-globe>
              Other languages
            </a>
          </li>
          <li>
            <a href="#">
              <digi-icon-envelope></digi-icon-envelope>
              Mejla vår funktionbrevlåda
            </a>
          </li>
        </ul>
      </digi-footer-card>
    </div>
  ${
    this.afVariation === FooterVariation.LARGE
      ? `
    <div>
      <digi-footer-card>
        <ul>
          <li>
            <a href="#">Tillgänglighetsredogörelse</a>
          </li>
          <li>
            <a href="#">Teckenspråk</a>
          </li>
          <li>
            <a href="#">Other languages</a>
            <br />
          </li>
          <li>
            <a href="#">Mejla vår funktionbrevlåda</a>
          </li>
          <li>
            <a href="#">Mejla vår funktionbrevlåda</a>
          </li>
          <li>
            <a href="#">Mejla vår funktionbrevlåda</a>
          </li>
        </ul>
      </digi-footer-card>
    </div>
    <div>
      <digi-footer-card [attr.af-type]="FooterCardVariation.ICON">
        <h2>Fokus abacus dolores</h2>
        <p>Vivamus feugiat nunc vel enim fermentum dolores ab. Nuj vel enim fermentum dolores abacus. Vivanna helam conkista</p>
        <a href="#"><digi-icon-external-link-alt></digi-icon-external-link-alt>
          Mejla vår funktionbrevlåda
        </a>
      </digi-footer-card>
    </div>
    <div>
      <digi-footer-card [attr.af-type]="FooterCardVariation.BORDER">
        <a href="#">Forium dolores</a>
        <p>Vivamus feugiat nunc vel enim fermentum, ac aliquet tortor.</p>
      </digi-footer-card>
      <digi-footer-card [attr.af-type]="FooterCardVariation.BORDER">
        <a href="#">Leveante lipsum</a>
        <p>Vivamus feugiat nunc vel enim.</p>
      </digi-footer-card>
      <digi-footer-card [attr.af-type]="FooterCardVariation.BORDER">
        <a href="#">Acta estium</a>
        <p>fermentum, ac aliquet tortor cursus. Curabitur cursus eros.</p>
      </digi-footer-card>
    </div>`
      : `<div>
      <digi-footer-card [attr.af-type]="FooterCardVariation.BORDER">
        <a href="#">Om tjänsten dolores</a>
        <p>Systemversion: 1.4.0 <br/> Ansvarig: Jenny Svensson</p>
      </digi-footer-card>
    </div>
    <div>
      <digi-footer-card [attr.af-type]="FooterCardVariation.BORDER">
        <a href="#">Kontakta servicdolores</a>
        <p>Telefon: 0771-60 0001 <br/> Öppettider: Vardagar 08:00-16:30</p>
      </digi-footer-card>
    </div>`
  }
  </div>
  <div slot="content-bottom-left">
    <a [routerLink]="['/']">
      <digi-logo [attr.af-variation]="LogoVariation.LARGE" [attr.af-color]="LogoColor.SECONDARY"></digi-logo>
    </a>
	</div>
	<div slot="content-bottom-right">
		<p>Följ oss på</p>
		<a href="#">Facebook</a>
		<a href="#">Youtube</a>
		<a href="#">Linkedin</a>
		<a href="#">Instagram</a>
	</div>
</digi-footer>`,
      [CodeExampleLanguage.REACT]: `<DigiFooter afVariation={FooterVariation.${Object.keys(FooterVariation).find((key) => FooterVariation[key] === this.afVariation)}}>
  <div slot="content-top">
    <div>
      <DigiFooterCard afType={FooterCardVariation.ICON}>
        <ul>
          <li>
            <a href="#">
              <DigiIconAccessibilityUniversal></DigiIconAccessibilityUniversal>
              Tillgänglighetsredogörelse
            </a>
          </li>
          <li>
            <a href="#">
              <DigiIconSign></DigiIconSign>
              Teckenspråk
            </a>
          </li>
          <li>
            <a href="#">
              <DigiIconGlobe></DigiIconGlobe>
              Other languages
            </a>
          </li>
          <li>
            <a href="#">
              <DigiIconEnvelope></DigiIconEnvelope>
              Mejla vår funktionbrevlåda
            </a>
          </li>
        </ul>
      </DigiFooterCard>
	  </div>
${
  this.afVariation === FooterVariation.LARGE
    ? `    <div>
      <DigiFooterCard>
        <ul>
          <li>
            <a href="#">Tillgänglighetsredogörelse</a>
          </li>
          <li>
            <a href="#">Teckenspråk</a>
          </li>
          <li>
            <a href="#">Other languages</a>
            <br />
          </li>
          <li>
            <a href="#">Mejla vår funktionbrevlåda</a>
          </li>
          <li>
            <a href="#">Mejla vår funktionbrevlåda</a>
          </li>
          <li>
            <a href="#">Mejla vår funktionbrevlåda</a>
          </li>
        </ul>
      </DigiFooterCard>
    </div>
    <div>
      <DigiFooterCard afType={FooterCardVariation.ICON}>
        <h2>Fokus abacus dolores</h2>
        <p>
        Vivamus feugiat nunc vel enim fermentum dolores ab. Nuj vel enim fermentum dolores abacus. Vivanna helam conkista
        </p>
        <a href="#"><DigiIconExternalLinkAlt></DigiIconExternalLinkAlt>
          Mejla vår funktionbrevlåda
        </a>
      </DigiFooterCard>
    </div>
    <div>
      <DigiFooterCard afType={FooterCardVariation.BORDER}>
        <a href="#">Forium dolores</a>
        <p>Vivamus feugiat nunc vel enim fermentum, ac aliquet tortor.</p>
      </DigiFooterCard>
      <DigiFooterCard afType={FooterCardVariation.BORDER}>
        <a href="#">Leveante lipsum</a>
        <p>Vivamus feugiat nunc vel enim.</p>
      </DigiFooterCard>
      <DigiFooterCard afType={FooterCardVariation.BORDER}>
        <a href="#">Acta estium</a>
        <p>fermentum, ac aliquet tortor cursus. Curabitur cursus eros.</p>
      </DigiFooterCard>
    </div>`
    : `   <div>
      <DigiFooterCard afType={FooterCardVariation.BORDER}>
        <a href="#">Om tjänsten dolores</a>
        <p>Systemversion: 1.4.0 <br/> Ansvarig: Jenny Svensson</p>
      </DigiFooterCard>
    </div>
    <div>
      <DigiFooterCard afType={FooterCardVariation.BORDER}>
        <a href="#">Kontakta servicdolores</a>
        <p>Telefon: 0771-60 0001 <br/> Öppettider: Vardagar 08:00-16:30</p>
      </DigiFooterCard>
    </div>`
}
  </div>
  <div slot="content-bottom-left">
    <Link to="/">
      <DigiLogo afVariation={LogoVariation.LARGE} afColor={LogoColor.SECONDARY}></DigiLogo>
    </Link>
	</div>
	<div slot="content-bottom-right">
		<p>Följ oss på</p>
		<a href="#">Facebook</a>
		<a href="#">Youtube</a>
		<a href="#">Linkedin</a>
		<a href="#">Instagram</a>
	</div>
</DigiFooter>`
    };
  }

  render() {
    return (
      <Host>
        <div class="digi-footer-details">
          <div class="block-spacer">
            <digi-typography-preamble>
              Den här komponenten är en samlingskomponent som kan innehålla
              flera olika underkomponenter som tillsammmans bygger upp sidfoten.
              Sidfoten finns i två varianter, där den första varianten kan ha
              max fyra columner och där den andra varianten är nedskalad till
              tre kolumner.
            </digi-typography-preamble>
          </div>

          <digi-layout-container af-no-gutter af-margin-bottom af-margin-top>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.badgeStatusCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}
              style={{ '--digi--code-example--controls--width': '140px' }}>
              {this.showExample && (
                <digi-footer af-variation={this.afVariation}>
                  <div slot="content-top">
                    <div>
                      <digi-footer-card af-type="icon">
                        <ul>
                          <li>
                            <a href="#">
                              <digi-icon-accessibility-universal></digi-icon-accessibility-universal>
                              Tillgänglighetsredogörelse
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <digi-icon-sign></digi-icon-sign>
                              Teckenspråk
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <digi-icon-globe></digi-icon-globe>
                              Other languages
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <digi-icon-envelope></digi-icon-envelope>
                              Mejla vår funktionbrevlåda
                            </a>
                          </li>
                        </ul>
                      </digi-footer-card>
                    </div>

                    {this.afVariation === FooterVariation.LARGE && (
                      <Fragment>
                        <div>
                          <digi-footer-card>
                            <ul>
                              <li>
                                <a href="#">Tillgänglighetsredogörelse</a>
                              </li>
                              <li>
                                <a href="#">Teckenspråk</a>
                              </li>
                              <li>
                                <a href="#">Other languages</a>
                                <br />
                              </li>
                              <li>
                                <a href="#">Mejla vår funktionbrevlåda</a>
                              </li>
                              <li>
                                <a href="#">Mejla vår funktionbrevlåda</a>
                              </li>
                              <li>
                                <a href="#">Mejla vår funktionbrevlåda</a>
                              </li>
                            </ul>
                          </digi-footer-card>
                        </div>
                        <div>
                          <digi-footer-card af-type="icon">
                            <h2>Fokus abacus dolores</h2>
                            <p>
                              Vivamus feugiat nunc vel enim fermentum dolores
                              ab. Nuj vel enim fermentum dolores abacus. Vivanna
                              helam conkista
                            </p>
                            <a href="#">
                              <digi-icon-external-link-alt></digi-icon-external-link-alt>
                              Mejla vår funktionbrevlåda
                            </a>
                          </digi-footer-card>
                        </div>
                        <div>
                          <digi-footer-card af-type="border">
                            <a href="#">Forium dolores</a>
                            <p>
                              Vivamus feugiat nunc vel enim fermentum, ac
                              aliquet tortor.
                            </p>
                          </digi-footer-card>
                          <digi-footer-card af-type="border">
                            <a href="#">Leveante lipsum</a>
                            <p>Vivamus feugiat nunc vel enim.</p>
                          </digi-footer-card>
                          <digi-footer-card af-type="border">
                            <a href="#">Acta estium</a>
                            <p>
                              fermentum, ac aliquet tortor cursus. Curabitur
                              cursus eros.
                            </p>
                          </digi-footer-card>
                        </div>
                      </Fragment>
                    )}

                    {this.afVariation === FooterVariation.SMALL && (
                      <Fragment>
                        <div>
                          <digi-footer-card af-type="border">
                            <a href="#">Om tjänsten dolores</a>
                            <p>
                              Systemversion: 1.4.0 <br /> Ansvarig: Jenny
                              Svensson
                            </p>
                          </digi-footer-card>
                        </div>
                        <div>
                          <digi-footer-card af-type="border">
                            <a href="#">Kontakta servicdolores</a>
                            <p>
                              Telefon: 0771-60 0001 <br /> Öppettider: Vardagar
                              08:00-16:30
                            </p>
                          </digi-footer-card>
                        </div>
                      </Fragment>
                    )}
                  </div>

                  <div slot="content-bottom-left">
                    <digi-link afHref="#">
                      <digi-logo
                        af-variation="large"
                        af-color="secondary"></digi-logo>
                    </digi-link>
                  </div>
                  <div slot="content-bottom-right">
                    <p>Följ oss på</p>
                    <a href="#">Facebook</a>
                    <a href="#">Youtube</a>
                    <a href="#">Linkedin</a>
                    <a href="#">Instagram</a>
                  </div>
                </digi-footer>
              )}

              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Varianter"
                  afLegend="Varianter"
                  onChange={(e) => this.changeVariationHandler(e)}>
                  <digi-form-radiobutton
                    afName="Stor"
                    afLabel="Stor"
                    afValue={FooterVariation.LARGE}
                    afChecked={this.afVariation === FooterVariation.LARGE}
                  />
                  <digi-form-radiobutton
                    afName="Liten"
                    afLabel="Liten"
                    afValue={FooterVariation.SMALL}
                    afChecked={this.afVariation === FooterVariation.SMALL}
                  />
                </digi-form-fieldset>
              </div>
            </digi-code-example>
          </digi-layout-container>

          <div>
            {!this.afShowOnlyExample && (
              <digi-layout-container af-no-gutter af-margin-bottom>
                <h2>Beskrivning</h2>
                <h3>Varianter</h3>
                <p>
                  Footern finns i två varianter Stor och Liten, vilket skickas
                  in via följande prop: <digi-code af-code="af-variation" />.
                </p>
              </digi-layout-container>
            )}
          </div>
        </div>
      </Host>
    );
  }
}
