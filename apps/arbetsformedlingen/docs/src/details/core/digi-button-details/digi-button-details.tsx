/* eslint-disable no-useless-escape */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, Prop, h, State } from '@stencil/core';
import {
  ButtonSize,
  ButtonVariation,
  CodeExampleLanguage,
  LayoutBlockVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-button-details',
  styleUrl: 'digi-button-details.scss'
})
export class DigiButtonDetails {
  @Prop() component: string;

  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() buttonExampleData: { [key: string]: any } = {
    size: ButtonSize.MEDIUM,
    variation: ButtonVariation.PRIMARY,
    icon: false,
    iconSecondary: false,
    fullWidth: false
  };

  get buttonExampleCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-button
	af-size="${this.buttonExampleData.size}"
	af-variation="${this.buttonExampleData.variation}"
	af-full-width="${this.buttonExampleData.fullWidth}"\>${
    this.buttonExampleData.variation === ButtonVariation.FUNCTION
      ? `\n  <digi-icon-copy slot="icon" />`
      : ''
  }\
${this.buttonExampleData.icon ? `\n  <digi-icon-copy slot="icon" />` : ''}
	En knapp\
${
  this.buttonExampleData.iconSecondary
    ? `\n  <digi-icon-arrow-down slot="icon-secondary" />`
    : ''
}\n</digi-button>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-button
	[attr.af-size]="ButtonSize.${Object.keys(ButtonSize).find((key) => ButtonSize[key] === this.buttonExampleData.size)}"
	[attr.af-variation]="ButtonVariation.${Object.keys(ButtonVariation).find((key) => ButtonVariation[key] === this.buttonExampleData.variation)}"
	[attr.af-full-width]="${this.buttonExampleData.fullWidth}">${
    this.buttonExampleData.variation === ButtonVariation.FUNCTION
      ? `\n  <digi-icon-copy slot="icon" />`
      : ''
  }\
${this.buttonExampleData.icon ? `\n  <digi-icon-copy slot="icon" />` : ''}
	En knapp\
${
  this.buttonExampleData.iconSecondary
    ? `\n  <digi-icon-arrow-down slot="icon-secondary" />`
    : ''
}
</digi-button>`,
      [CodeExampleLanguage.REACT]: `\
<DigiButton
	afSize={ButtonSize.${Object.keys(ButtonSize).find((key) => ButtonSize[key] === this.buttonExampleData.size)}}
	afVariation={ButtonVariation.${Object.keys(ButtonVariation).find((key) => ButtonVariation[key] === this.buttonExampleData.variation)}}
	afFullWidth={${this.buttonExampleData.fullWidth}}>${
    this.buttonExampleData.variation === ButtonVariation.FUNCTION
      ? `\n  <DigiIconCopy slot="icon" />`
      : ''
  }\
${this.buttonExampleData.icon ? `\n  <DigiIconCopy slot="icon" />` : ''}
	En knapp\
${
  this.buttonExampleData.iconSecondary
    ? `\n  <DigiIconArrowDown slot="icon-secondary" />`
    : ''
}
</DigiButton>`
    };
  }

  @State() showExample = true;

  changeButtonExampleData(newData) {
    this.showExample = false;
    setTimeout(() => (this.showExample = true), 0);

    this.buttonExampleData = newData;
  }

  changeSizeHandler(e) {
    this.changeButtonExampleData({
      ...this.buttonExampleData,
      size: e.target.value
    });
  }

  changeVariationHandler(e) {
    let newData: any = {
      ...this.buttonExampleData,
      variation: e.target.value
    };

    if (e.target.value === ButtonVariation.FUNCTION) {
      newData = {
        ...newData,
        icon: false,
        iconSecondary: false
      };
    }

    this.changeButtonExampleData(newData);
  }

  changeIconHandler(e) {
    const pos = e.target.value;

    let newData: any = {
      ...this.buttonExampleData,
      icon: false,
      iconSecondary: false
    };

    if (pos === 'left') {
      newData = {
        ...newData,
        icon: true
      };
    } else if (pos === 'right') {
      newData = {
        ...newData,
        iconSecondary: true
      };
    }

    this.changeButtonExampleData(newData);
  }

  changeFullWidthHandler(e) {
    this.changeButtonExampleData({
      ...this.buttonExampleData,
      fullWidth: e.target.checked
    });
  }

  render() {
    return (
      <div class="digi-button-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Knappar används för att utföra en handling eller exekvera en
              funktion. Till skillnad från länkar så används de ej för att
              navigera inom eller mellan sidor.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.buttonExampleCode)}
                af-controls-position="end"
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset
                    af-legend="Varianter"
                    af-name="Varianter"
                    onChange={(e) => this.changeVariationHandler(e)}>
                    <digi-form-radiobutton
                      afName="Varianter"
                      afLabel="Primär"
                      afValue={ButtonVariation.PRIMARY}
                      afChecked={true}
                    />
                    <digi-form-radiobutton
                      afName="Varianter"
                      afLabel="Sekundär"
                      afValue={ButtonVariation.SECONDARY}
                    />
                    <digi-form-radiobutton
                      afName="Varianter"
                      afLabel="Funktion"
                      afValue={ButtonVariation.FUNCTION}
                    />
                  </digi-form-fieldset>

                  <digi-form-fieldset
                    af-legend="Storlekar"
                    af-name="Storlekar"
                    onChange={(e) => this.changeSizeHandler(e)}>
                    <digi-form-radiobutton
                      afName="Storlekar"
                      afLabel="Stor"
                      afValue={ButtonSize.LARGE}
                    />
                    <digi-form-radiobutton
                      afName="Storlekar"
                      afLabel="Mellan"
                      afValue={ButtonSize.MEDIUM}
                      afChecked={true}
                    />
                    <digi-form-radiobutton
                      afName="Storlekar"
                      afLabel="Liten"
                      afValue={ButtonSize.SMALL}
                    />
                  </digi-form-fieldset>

                  <digi-form-fieldset
                    af-legend="Ikon"
                    af-name="Ikon"
                    onChange={(e) => this.changeIconHandler(e)}>
                    {this.buttonExampleData.variation !==
                      ButtonVariation.FUNCTION && (
                      <span>
                        <digi-form-radiobutton
                          afName="Ikon"
                          afLabel="Ingen ikon"
                          afChecked={
                            !this.buttonExampleData.icon &&
                            !this.buttonExampleData.iconSecondary
                          }
                          afValue={''}
                        />
                        <digi-form-radiobutton
                          afName="Ikon"
                          afLabel="Ikon (vänster)"
                          afChecked={this.buttonExampleData.icon}
                          afValue={'left'}
                        />
                        <digi-form-radiobutton
                          afName="Ikon"
                          afLabel="Ikon (höger)"
                          afChecked={this.buttonExampleData.iconSecondary}
                          afValue={'right'}
                        />
                      </span>
                    )}
                    {this.buttonExampleData.variation ===
                      ButtonVariation.FUNCTION && (
                      <p>
                        <em>
                          Ikon-alternativen är inte tillgängliga för
                          funktionsknappen.
                        </em>
                      </p>
                    )}
                  </digi-form-fieldset>

                  <digi-form-checkbox
                    afLabel="100% i bredd"
                    onChange={(e) =>
                      this.changeFullWidthHandler(e)
                    }></digi-form-checkbox>
                </div>
                {this.showExample && (
                  <digi-button
                    af-size={this.buttonExampleData.size}
                    af-variation={this.buttonExampleData.variation}
                    af-full-width={this.buttonExampleData.fullWidth}>
                    {this.buttonExampleData.variation ===
                      ButtonVariation.FUNCTION && (
                      <digi-icon-copy slot="icon" />
                    )}
                    {this.buttonExampleData.icon && (
                      <digi-icon-copy slot="icon" />
                    )}
                    En knapp
                    {this.buttonExampleData.iconSecondary && (
                      <digi-icon-arrow-down slot="icon-secondary" />
                    )}
                  </digi-button>
                )}
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2 id="description">Beskrivning</h2>
              <h3>Storlekar</h3>
              <p>
                Knapparna finns även i flera storlekar: stor, mellan och liten.
                Mellanstorleken är förvald. Storlek ställs in med{' '}
                <digi-code af-code="af-size" />.
              </p>
              <h3>Varianter</h3>
              <p>
                Knappar finns i tre olika varianter: Primära, sekundära och
                funktionsknappar. De sistnämnda måste ha en ikon. Variant ställs
                in med <digi-code af-code="af-variation" />.{' '}
              </p>
              <h4>Primär</h4>
              <digi-list>
                <li>Används alltid om det endast finns en knapp på sidan.</li>
                <li>
                  Ska alltid vara mest framträdande i relation till andra
                  knappar.
                </li>
              </digi-list>

              <h4>Sekundär</h4>
              <digi-list>
                <li>
                  En sekundär knapp får endast användas om det finns mer än en
                  knapp på sidan.
                </li>
                <li>
                  En sekundär knapp används för den sekundära interaktionen, som
                  till exempel att avbryta ett flöde, säga nej tack eller
                  liknande.
                </li>
                <li>
                  En sekundär knapp får i vissa fall ersättas med en
                  funktionsknapp, men endast om det gör interaktionen tydligare
                  för användare.
                </li>
              </digi-list>
              <h4>Funktionsknapp</h4>
              <digi-list>
                <li>
                  Funktionsknappar är knappar men ser ut som länkar. Dessa
                  används för att utföra en handling, till exempel Rensa eller
                  Ändra.
                </li>
              </digi-list>
              <h3>Knappar i grupp</h3>
              <p>
                När du ska ha flera knappar bredvid varandra tänk på att
                gruppera dessa i en container som har stilsättningen:{' '}
                <digi-code af-code="display: inline-flex;" /> och{' '}
                <digi-code af-code="flex-wrap: wrap;" />
              </p>
              <p>
                {' '}
                Detta för att knappar ska kunna lägga sig under varandra om
                utrymmet inte skulle räcka om till exempel en fönster blir
                mindre.
              </p>
            </digi-layout-container>
          )}
          {!this.afShowOnlyExample && (
            <digi-layout-block
              af-variation={LayoutBlockVariation.SYMBOL}
              af-vertical-padding
              af-margin-bottom>
              <h2 id="guidelines">Riktlinjer</h2>
              <digi-list>
                <li>
                  Inaktiv knapp ska inte användas. Alla knappar ska alltid gå
                  att interagera med.
                </li>
              </digi-list>
            </digi-layout-block>
          )}
        </digi-typography>
      </div>
    );
  }
}
