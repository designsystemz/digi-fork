import { Component, Prop, h, getAssetPath } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-media-image-details',
  styleUrl: 'digi-media-image-details.scss'
})
export class DigiMediaImageDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  get mediaFigureCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-media-image
	af-unlazy
	af-height="300"
	af-width="300"
	af-src='/assets/images/logotype-sign.svg'
	af-alt="Arbetsförmedlingens logotyp som en fasadskyld"
>
</digi-media-image>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-media-image
	[attr.af-unlazy]
	[attr.af-height]="300"
	[attr.af-width]="300"
	[attr.af-src]='/assets/images/logotype-sign.svg'
	[attr.af-alt]="Arbetsförmedlingens logotyp som en fasadskyld"
>
</digi-media-image>`,
      [CodeExampleLanguage.REACT]: `\
<DigiMediaImage
	afUnlazy
	afHeight="300"
	afWidth="300"
	afSrc='/assets/images/logotype-sign.svg'
	afAlt="Arbetsförmedlingens logotyp som en fasadskyld"
>
</DigiMediaImage>`
    };
  }

  render() {
    return (
      <div class="digi-media-image-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Det här är Arbetsförmedlingens komponent för bilder.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.mediaFigureCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <digi-media-image
                afUnlazy
                afHeight="300"
                afWidth="300"
                af-src={getAssetPath('/assets/images/logotype-sign.svg')}
                afAlt="Arbetsförmedlingens logotyp som en fasadskyld"></digi-media-image>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <h3>Alt-text</h3>
              <p>
                Alt-texten är obligatorisk och ställs in med{' '}
                <digi-code af-code="af-alt" />.
              </p>
              <h3>Bredd och höjd</h3>
              <p>
                Med hjälp av <digi-code af-code="af-height" /> och{' '}
                <digi-code af-code="af-width" /> går det att ställa in bildens
                storlek. Det går också att använda{' '}
                <digi-code af-code="af-fullwidth" /> så kommer bilden sättas
                till 10% och anpassar höjden automatiskt efter bredden.
              </p>
              <h3>Lady loading</h3>
              <p>
                Bilden har inbyggd lazy loadning. För att ta bort det kan man
                sätta attributet <digi-code af-code="af-unlazy" /> till true. Om
                komponenten får mått för bredd och höjd så kan en platshållare
                se till så att sidan inte hoppar till när bilden har laddat
                färdigt.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
