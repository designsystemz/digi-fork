import { Component, h, Host, Prop, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  InfoCardHeadingLevel,
  InfoCardVariation,
  LoaderSkeletonVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-loader-skeleton-details',
  styleUrl: 'digi-loader-skeleton-details.scss'
})
export class DigiLoaderSkeleton {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() loaderSkeletonVariation: LoaderSkeletonVariation =
    LoaderSkeletonVariation.LINE;
  @State() showExample = true;
  @State() afCountShow = true;

  get skeletonCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-loader-skeleton 
	af-variation="${this.loaderSkeletonVariation}"
	${
    this.afCountShow &&
    (this.loaderSkeletonVariation === LoaderSkeletonVariation.TEXT ||
      this.loaderSkeletonVariation === LoaderSkeletonVariation.SECTION)
      ? 'af-count="4"'
      : ''
  }
>
</digi-loader-skeleton>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-loader-skeleton
	[attr.af-variation]="LoaderSkeletonVariation.${Object.keys(
    LoaderSkeletonVariation
  ).find(
    (key) => LoaderSkeletonVariation[key] === this.loaderSkeletonVariation
  )}"
	${
    this.afCountShow &&
    (this.loaderSkeletonVariation === LoaderSkeletonVariation.TEXT ||
      this.loaderSkeletonVariation === LoaderSkeletonVariation.SECTION)
      ? '[attr.af-variation]="4"'
      : ''
  }
>
</digi-loader-skeleton>`,
      [CodeExampleLanguage.REACT]: `\
<DigiLoaderSkeleton
	afVariation={LoaderSkeletonVariation.${Object.keys(
    LoaderSkeletonVariation
  ).find(
    (key) => LoaderSkeletonVariation[key] === this.loaderSkeletonVariation
  )}}
	${
    this.afCountShow &&
    (this.loaderSkeletonVariation === LoaderSkeletonVariation.TEXT ||
      this.loaderSkeletonVariation === LoaderSkeletonVariation.SECTION)
      ? 'afCount="4"'
      : ''
  }
>
</DigiLoaderSkeleton>`
    };
  }

  startExampleSkeletonAnimation() {
    requestAnimationFrame(() => {
      this.showExample = false;
      requestAnimationFrame(() => {
        this.showExample = true;
      });
    });
  }

  changeVariationHandler(e) {
    // eslint-disable-next-line
    this.loaderSkeletonVariation = (e.target as any).value;
    this.startExampleSkeletonAnimation();
  }

  render() {
    return (
      <Host>
        <div class="digi-loader-skeleton-details">
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Platshållare (eller skeleton loaders) används för att visa den
              generella strukturen av en sida eller en del av en sida när den
              laddas in.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom af-margin-top>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.skeletonCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Varianter"
                  afLegend="Varianter"
                  onChange={(e) => this.changeVariationHandler(e)}>
                  <digi-form-radiobutton
                    afName="Varianter"
                    afLabel="Linje"
                    afValue={LoaderSkeletonVariation.LINE}
                    afChecked={true}
                  />
                  <digi-form-radiobutton
                    afName="Varianter"
                    afLabel="Rubrik"
                    afValue={LoaderSkeletonVariation.HEADER}
                  />
                  <digi-form-radiobutton
                    afName="Varianter"
                    afLabel="Text"
                    afValue={LoaderSkeletonVariation.TEXT}
                  />
                  <digi-form-radiobutton
                    afName="Varianter"
                    afLabel="Cirkel"
                    afValue={LoaderSkeletonVariation.CIRCLE}
                  />
                  <digi-form-radiobutton
                    afName="Varianter"
                    afLabel="Rundad"
                    afValue={LoaderSkeletonVariation.ROUNDED}
                  />
                  <digi-form-radiobutton
                    afName="Varianter"
                    afLabel="Sektion"
                    afValue={LoaderSkeletonVariation.SECTION}
                  />
                </digi-form-fieldset>
              </div>
              {this.showExample && (
                <digi-loader-skeleton
                  afVariation={this.loaderSkeletonVariation}
                  af-count="4"></digi-loader-skeleton>
              )}
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2 id="description">Beskrivning</h2>
              <h3>Variationer</h3>
              <p>
                För varianterna text och sektion så kan man ändra antalet rader
                med <digi-code af-variation="light" af-code=" af-count" />. Som
                standard är{' '}
                <digi-code af-variation="light" af-code=" af-count" /> lika med
                4.
              </p>
              <digi-info-card
                afHeading="Bra att veta"
                afHeadingLevel={`h3` as InfoCardHeadingLevel}
                afVariation={InfoCardVariation.SECONDARY}>
                <p>
                  Aria attributen
                  <digi-code
                    af-variation="light"
                    af-code='aria-busy="true"'
                  />{' '}
                  och{' '}
                  <digi-code
                    af-variation="light"
                    af-code='aria-hidden="true"'
                  />{' '}
                  ingår i komponenten.
                </p>
              </digi-info-card>
            </digi-layout-container>
          )}
        </div>
      </Host>
    );
  }
}
