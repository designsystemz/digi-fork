import { Component, h, Prop, State } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-tools-languagepicker-details',
  styleUrl: 'digi-tools-languagepicker-details.scss'
})
export class DigiToolsLanguagepicker {
  @Prop() component: string;

  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() showExample = true;

  @State() languagepickerData: { [key: string]: boolean } = {
    hideLanguagePicker: false,
    showListenButton: false,
    hideSignlangButton: false
  };
  changeLanguagepickerData(newData) {
    this.showExample = false;
    setTimeout(() => (this.showExample = true), 0);

    this.languagepickerData = newData;
  }
  changeLanguagePickerVisibility(e) {
    this.changeLanguagepickerData({
      ...this.languagepickerData,
      hideLanguagePicker: e.target.checked
    });
  }

  changeListenButtonVisibility(e) {
    this.changeLanguagepickerData({
      ...this.languagepickerData,
      showListenButton: e.target.checked
    });
  }

  changeSignlangButtonVisibility(e) {
    this.changeLanguagepickerData({
      ...this.languagepickerData,
      hideSignlangButton: e.target.checked
    });
  }

  get LanguagePickerCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-tools-languagepicker
\taf-languagepicker-text="Språk"
${this.languagepickerData.hideLanguagePicker ? '\taf-languagepicker-hide="true"\n' : ''}\
${this.languagepickerData.showListenButton ? '\taf-listen-show="true"\n' : ''}\
${this.languagepickerData.hideSignlangButton ? '\taf-signlang-hide="true"\n' : ''}\
\taf-languagepicker-items='[{"index":0,"type":"button","text":"العربية (Arabiska)","lang":"ar","value":"ar","dir":"rtl"},{"index":1,"type":"button","text":"دری (Dari)","lang":"prs","value":"prs","dir":"rtl"},{"index":2,"type":"button","text":"به پارسی (Persiska)","lang":"fa","value":"fa","dir":"rtl"},{"index":3,"type":"button","text":"English (Engelska)","lang":"en","value":"en","dir":"ltr"},{"index":4,"type":"button","text":"Русский (Ryska)","lang":"ru","value":"ru","dir":"ltr"},{"index":5,"type":"button","text":"Af soomaali (Somaliska)","lang":"so","value":"so","dir":"ltr"},{"index":6,"type":"button","text":"Svenska","lang":"sv","value":"sv","dir":"ltr"},{"index":7,"type":"button","text":"ትግርኛ (Tigrinska)","lang":"ti","value":"ti","dir":"ltr"}]'
>
</digi-tools-languagepicker>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-tools-languagepicker
\t[attr.af-languagepicker-text]="'Språk'"
${this.languagepickerData.hideLanguagePicker ? '\t[attr.af-languagepicker-hide]="true"\n' : ''}\
${this.languagepickerData.showListenButton ? '\t[attr.af-listen-show]="true"\n' : ''}\
${this.languagepickerData.hideSignlangButton ? '\t[attr.af-signlang-hide]="true"\n' : ''}\
\t[attr.af-languagepicker-items]='[{"index":0,"type":"button","text":"العربية (Arabiska)","lang":"ar","value":"ar","dir":"rtl"},{"index":1,"type":"button","text":"دری (Dari)","lang":"prs","value":"prs","dir":"rtl"},{"index":2,"type":"button","text":"به پارسی (Persiska)","lang":"fa","value":"fa","dir":"rtl"},{"index":3,"type":"button","text":"English (Engelska)","lang":"en","value":"en","dir":"ltr"},{"index":4,"type":"button","text":"Русский (Ryska)","lang":"ru","value":"ru","dir":"ltr"},{"index":5,"type":"button","text":"Af soomaali (Somaliska)","lang":"so","value":"so","dir":"ltr"},{"index":6,"type":"button","text":"Svenska","lang":"sv","value":"sv","dir":"ltr"},{"index":7,"type":"button","text":"ትግርኛ (Tigrinska)","lang":"ti","value":"ti","dir":"ltr"}]'
/>`,
      [CodeExampleLanguage.REACT]: `\
<DigiToolsLanguagepicker
\tafLanguagepickerText="Språk"
${this.languagepickerData.hideLanguagePicker ? '\tafLanguagepickerHide={true}\n' : ''}\
${this.languagepickerData.showListenButton ? '\tafListenShow={true}\n' : ''}\
${this.languagepickerData.hideSignlangButton ? '\tafSignlangHide={true}\n' : ''}\
\tafLanguagepickerItems='[{"index":0,"type":"button","text":"العربية (Arabiska)","lang":"ar","value":"ar","dir":"rtl"},{"index":1,"type":"button","text":"دری (Dari)","lang":"prs","value":"prs","dir":"rtl"},{"index":2,"type":"button","text":"به پارسی (Persiska)","lang":"fa","value":"fa","dir":"rtl"},{"index":3,"type":"button","text":"English (Engelska)","lang":"en","value":"en","dir":"ltr"},{"index":4,"type":"button","text":"Русский (Ryska)","lang":"ru","value":"ru","dir":"ltr"},{"index":5,"type":"button","text":"Af soomaali (Somaliska)","lang":"so","value":"so","dir":"ltr"},{"index":6,"type":"button","text":"Svenska","lang":"sv","value":"sv","dir":"ltr"},{"index":7,"type":"button","text":"ትግርኛ (Tigrinska)","lang":"ti","value":"ti","dir":"ltr"}]'
/>`
    };
  }

  render() {
    return (
      <div class="digi-tools-languagepicker-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Språkväljaren tillåter dig ge användaren möjlighet att ändra
              språk. Den kommer tillsammans med två andra knappar som går att
              använda för teckenspråk och syntolkning.
            </digi-typography-preamble>
          )}
        </digi-typography>
        <digi-layout-container af-no-gutter af-margin-bottom>
          {!this.afShowOnlyExample && <h2>Exempel</h2>}
          <digi-code-example
            af-code={JSON.stringify(this.LanguagePickerCode)}
            af-hide-controls={this.afHideControls ? 'true' : 'false'}
            af-hide-code={this.afHideCode ? 'true' : 'false'}>
            <div class="slot__controls" slot="controls">
              <digi-form-checkbox
                afLabel="Dölj språkväljaren"
                onChange={(e) =>
                  this.changeLanguagePickerVisibility(e)
                }></digi-form-checkbox>
              <digi-form-checkbox
                class="divider"
                afLabel='Dölj "Teckenspråk"-knapp'
                onChange={(e) =>
                  this.changeSignlangButtonVisibility(e)
                }></digi-form-checkbox>
              <digi-form-checkbox
                afLabel='Visa "Lyssna"-knapp'
                onChange={(e) =>
                  this.changeListenButtonVisibility(e)
                }></digi-form-checkbox>
              <br />
            </div>
            <div style={{ height: '120px' }}>
              <digi-tools-languagepicker
                afLanguagepickerText="Språk"
                afLanguagepickerStartSelected={0}
                afLanguagepickerItems='[{"index":0,"type":"button","text":"العربية (Arabiska)","lang":"ar","value":"ar","dir":"rtl"},{"index":1,"type":"button","text":"دری (Dari)","lang":"prs","value":"prs","dir":"rtl"},{"index":2,"type":"button","text":"به پارسی (Persiska)","lang":"fa","value":"fa","dir":"rtl"},{"index":3,"type":"button","text":"English (Engelska)","lang":"en","value":"en","dir":"ltr"},{"index":4,"type":"button","text":"Русский (Ryska)","lang":"ru","value":"ru","dir":"ltr"},{"index":5,"type":"button","text":"Af soomaali (Somaliska)","lang":"so","value":"so","dir":"ltr"},{"index":6,"type":"button","text":"Svenska","lang":"sv","value":"sv","dir":"ltr"},{"index":7,"type":"button","text":"ትግርኛ (Tigrinska)","lang":"ti","value":"ti","dir":"ltr"}]'
                afLanguagepickerHide={
                  this.languagepickerData.hideLanguagePicker
                }
                afListenShow={this.languagepickerData.showListenButton}
                afSignlangHide={
                  this.languagepickerData.hideSignlangButton
                }></digi-tools-languagepicker>
            </div>
          </digi-code-example>
        </digi-layout-container>
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-margin-bottom afNoGutter>
              <h2>Beskrivning</h2>
              <p>
                Visningsnamnet på rullgardinsmenyn anges via{' '}
                <digi-code af-code="af-languagepicker-text" />. Texten på
                knapparna anges via <digi-code af-code="af-listen-text" /> för
                "Lyssna"-knappen och <digi-code af-code="af-signlang-text" />{' '}
                för "Teckenspråk"-knappen.
              </p>
              <p>
                Du kan välja att dölja språkväljaren eller teckenspråks-knappen
                med <digi-code af-code="af-languagepicker-hide" /> eller{' '}
                <digi-code af-code="af-signlang-hide" />.
              </p>
              <p>
                Lyssna-knappen är dold som förval men du kan välja att visa den
                med <digi-code af-code="af-listen-show" />.
              </p>
              <p>
                Du sätter alla valbara språk genom att skicka in en sträng eller
                matris in i <digi-code af-code="af-languagepicker-items" /> och
                det redan valda språket genom att skicka in ett index i{' '}
                <digi-code af-code="af-languagepicker-start-selected" />.
              </p>
              <h2>Funktion för att få text uppläst</h2>
              <p>
                Att funktionen för "Lyssna"-knapp inte är förvald beror på att
                myndigheten 2023 valde att inte förlänga avtal med den
                leverantör som tillhandahållit detta. Är du intresserad av att t
                ex nyttja operativsystemets inbyggda talsyntes så kontakta
                teamet för designsystemet.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
