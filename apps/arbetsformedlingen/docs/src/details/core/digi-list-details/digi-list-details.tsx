import { Component, h, Prop, State } from '@stencil/core';
import { CodeExampleLanguage, ListType } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-list-details',
  styleUrl: 'digi-list-details.scss'
})
export class DigiListDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() hasIcon = false;

  @State() listType: ListType = ListType.BULLET;

  get listCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-list
	af-list-type="${this.listType}"
>
<li>Specifikt</li>
<li>Realistiskt</li>
<li>Mätbart</li>
</digi-list>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-list
	[attr.af-list-type]="ListType.${this.listType.toUpperCase()}"
>
<li>Specifikt</li>
<li>Realistiskt</li>
<li>Mätbart</li>
</digi-list>`,
      [CodeExampleLanguage.REACT]: `\
<DigiList
	afListType={ListType.${this.listType.toUpperCase()}}
>
<li>Specifikt</li>
<li>Realistiskt</li>
<li>Mätbart</li>
</DigiList>`
    };
  }

  render() {
    return (
      <div class="digi-list-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Använd listan när du har ett stycke av text som kan brytas ut i
              flera rader. Komponenten tillåter dig att visa en lista med olika
              stilar som punktlistor, numrerade eller med ikonen för check.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.listCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div>
                <digi-list afListType={this.listType}>
                  <li>Specifikt</li>
                  <li>Realistiskt</li>
                  <li>Mätbart</li>
                </digi-list>
              </div>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Typ av lista"
                  afLegend="Typ av lista"
                  onChange={(e) =>
                    (this.listType = (e.target as HTMLFormElement).value)
                  }>
                  <digi-form-radiobutton
                    afName="Typ av lista"
                    afLabel="Punktlista"
                    afValue={ListType.BULLET}
                    afChecked={this.listType === ListType.BULLET}
                  />
                  <digi-form-radiobutton
                    afName="Typ av lista"
                    afLabel="Numrerad"
                    afValue={ListType.NUMBERED}
                    afChecked={this.listType === ListType.NUMBERED}
                  />
                  <digi-form-radiobutton
                    afName="Typ av lista"
                    afLabel="Check"
                    afValue={ListType.CHECKED}
                    afChecked={this.listType === ListType.CHECKED}
                  />
                </digi-form-fieldset>
              </div>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Typ av lista</h3>
              <p>
                Listan finns i fyra olika varianter: som punktlista, numrerad,
                eller med en check-ikon. Det ställs in med hjälp av{' '}
                <digi-code af-code="af-list-type" />.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
