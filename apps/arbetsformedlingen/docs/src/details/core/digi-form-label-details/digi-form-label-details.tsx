import { Component, Prop, h, State } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-form-label-details',
  styleUrl: 'digi-form-label-details.scss'
})
export class DigiFormLabelDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() hasDescription = false;

  get labelCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-form-label
	af-label="Jag är en etikett"
	af-for="ett-element-id"${this.hasDescription ? `\n\taf-description="Beskrivande text"` : ''}
>\
</digi-form-label>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-form-label
	[attr.af-label]="Jag är en etikett"
	[attr.af-for]="ett-element-id"${this.hasDescription ? `\n\t[attr.af-description]="Beskrivande text"` : ''}
>\
</digi-form-label>`,
      [CodeExampleLanguage.REACT]: `\
<DigiFormLabel
	afLabel="Jag är en etikett"
	afFor="ett-element-id"${this.hasDescription ? `\n\tafDescription="Beskrivande text"` : ''}
>\
</DigiFormLabel>`
    };
  }

  render() {
    return (
      <div class="digi-form-label-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Etikettkomponenten är kompatibel med alla typer av formelement och
              kan till exempel användas för att ge ett formulärselement en
              etikett.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.labelCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset afLegend="Beskrivande text">
                    <digi-form-checkbox
                      afLabel="Ja"
                      afChecked={this.hasDescription}
                      onAfOnChange={() =>
                        this.hasDescription
                          ? (this.hasDescription = false)
                          : (this.hasDescription = true)
                      }></digi-form-checkbox>
                  </digi-form-fieldset>
                </div>
                {!this.hasDescription && (
                  <digi-form-label
                    af-label="Jag är en etikett"
                    afLabel="Jag är en etikett"
                    afFor="some-element-id"></digi-form-label>
                )}
                {this.hasDescription && (
                  <digi-form-label
                    af-label="Jag är en etikett"
                    afLabel="Jag är en etikett"
                    afFor="some-element-id"
                    af-description="Beskrivande text"></digi-form-label>
                )}
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <p>
                Med hjälp av <digi-code af-code="af-label" /> så anges
                etikettexten. Attributet <digi-code af-code="af-for" /> är
                obligatoriskt och ska vara identiskt med id-attributet på
                inmatningselementet. Etiketten har också stöd för en
                beskrivningstext som anges med{' '}
                <digi-code af-code="af-description" />.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
