import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  FormCheckboxVariation,
  InfoCardHeadingLevel,
  LinkButtonSize,
  LinkButtonVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-link-button-details',
  styleUrl: 'digi-link-button-details.scss'
})
export class DigiFormCheckboxDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() linkButtonVariation: LinkButtonVariation =
    LinkButtonVariation.PRIMARY;
  @State() linkButtonSize: LinkButtonSize = LinkButtonSize.MEDIUM;
  @State() linkButtonHideIcon = false;
  @State() linkButtonFullwidth = false;

  get linkButtonCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-link-button
	af-href="#"
	af-size="${this.linkButtonSize}"
	af-variation="${this.linkButtonVariation}"${
    this.linkButtonHideIcon ? '\n	af-hide-icon="true"' : ''
  }${this.linkButtonFullwidth ? '\n	af-fullwidth="true"' : ''}
>
	Jag är en länkknapp
</digi-link-button>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-link-button
  [attr.af-link-container]="true"
  [attr.af-size]="LinkButtonSize.${Object.keys(LinkButtonSize).find(
    (key) => LinkButtonSize[key] === this.linkButtonSize
  )}"
  [attr.af-variation]="LinkButtonVariation.${Object.keys(
    LinkButtonVariation
  ).find((key) => LinkButtonVariation[key] === this.linkButtonVariation)}"${
    this.linkButtonHideIcon ? '\n  [attr.af-hide-icon]="true"' : ''
  }${this.linkButtonFullwidth ? '\n  [attr.af-fullwidth]="true"' : ''}
>
  <a href="/" [routerLink]="['/']">
    <digi-icon-chevron-right></digi-icon-chevron-right>
    Jag är en länkknapp
  </a>
</digi-link-button>`,
      [CodeExampleLanguage.REACT]: `\
<DigiLinkButton
	afHref="#"
	afSize={LinkButtonSize.${Object.keys(LinkButtonSize).find(
    (key) => LinkButtonSize[key] === this.linkButtonSize
  )}}
	afVariation={LinkButtonVariation.${Object.keys(LinkButtonVariation).find(
    (key) => LinkButtonVariation[key] === this.linkButtonVariation
  )}}${this.linkButtonHideIcon ? '\n	af-hide-icon={true}' : ''}${
    this.linkButtonFullwidth ? '\n	af-fullwidth={true}' : ''
  }
>
	Jag är en länkknapp
</DigiLinkButton>`
    };
  }

  render() {
    return (
      <div class="digi-link-button-details">
        {!this.afShowOnlyExample && (
          <digi-typography-preamble>
            Länkknapp är en länk som är stylad som en knapp. Den används för att
            framhäva en länk extra tydligt, som till exempel vid ingångar till
            digitala tjänster.
          </digi-typography-preamble>
        )}
        <digi-layout-container af-no-gutter af-margin-bottom>
          <article>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.linkButtonCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variation"
                  afLegend="Variant"
                  onChange={(e) =>
                    (this.linkButtonVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Primär"
                    afValue={LinkButtonVariation.PRIMARY}
                    afChecked={
                      this.linkButtonVariation === LinkButtonVariation.PRIMARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Primär alt"
                    afValue={LinkButtonVariation.PRIMARY_ALT}
                    afChecked={
                      this.linkButtonVariation ===
                      LinkButtonVariation.PRIMARY_ALT
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Sekundär"
                    afValue={LinkButtonVariation.SECONDARY}
                    afChecked={
                      this.linkButtonVariation === LinkButtonVariation.SECONDARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Sekundär alt"
                    afValue={LinkButtonVariation.SECONDARY_ALT}
                    afChecked={
                      this.linkButtonVariation ===
                      LinkButtonVariation.SECONDARY_ALT
                    }
                  />
                </digi-form-fieldset>
                <digi-form-fieldset
                  afName="Storlek"
                  afLegend="Storlek"
                  onChange={(e) =>
                    (this.linkButtonSize = (e.target as HTMLFormElement).value)
                  }>
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Mellan"
                    afValue={LinkButtonSize.MEDIUM}
                    afChecked={this.linkButtonSize === LinkButtonSize.MEDIUM}
                  />
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Större"
                    afValue={LinkButtonSize.MEDIUMLARGE}
                    afChecked={
                      this.linkButtonSize === LinkButtonSize.MEDIUMLARGE
                    }
                  />
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Störst"
                    afValue={LinkButtonSize.LARGE}
                    afChecked={this.linkButtonSize === LinkButtonSize.LARGE}
                  />
                </digi-form-fieldset>
                <digi-form-fieldset afLegend="Egenskaper">
                  <digi-form-checkbox
                    afLabel="Dölj ikon"
                    afVariation={FormCheckboxVariation.PRIMARY}
                    afChecked={this.linkButtonHideIcon}
                    onAfOnChange={() =>
                      (this.linkButtonHideIcon = !this.linkButtonHideIcon)
                    }></digi-form-checkbox>
                  <digi-form-checkbox
                    afLabel="Fullbredd"
                    afVariation={FormCheckboxVariation.PRIMARY}
                    afChecked={this.linkButtonFullwidth}
                    onAfOnChange={() =>
                      (this.linkButtonFullwidth = !this.linkButtonFullwidth)
                    }></digi-form-checkbox>
                </digi-form-fieldset>
              </div>
              <digi-link-button
                afHref="#"
                af-target="_blank"
                afVariation={this.linkButtonVariation}
                afSize={this.linkButtonSize}
                afHideIcon={this.linkButtonHideIcon}
                afFullwidth={this.linkButtonFullwidth}>
                Jag är en länkknapp
              </digi-link-button>
            </digi-code-example>
          </article>
        </digi-layout-container>
        {!this.afShowOnlyExample && (
          <digi-layout-container af-no-gutter af-margin-bottom>
            <digi-info-card
              afHeading="Riktlinjer och tillgänglighet"
              afHeadingLevel={InfoCardHeadingLevel.H3}>
              <div class="digi-typography digi-typography--s">
                <digi-list>
                  <li>
                    Mer utförliga riktlinjer finns i vårt{' '}
                    <a href="/designmonster/lankar">designmönster för länkar</a>
                    .
                  </li>
                  <li>
                    För att kunna använda Angulars{' '}
                    <digi-code af-code="routerLink"></digi-code> med vår
                    komponent behöver man använd attributet{' '}
                    <digi-code af-code="af-container-link"></digi-code> och
                    istället använda komponenten som en hållare av ett vanligt
                    länkelement.
                    <br />
                    <br />
                    Använder man Angular RouterLink direkt på vårt element så
                    får man tillgänglighetsproblem då Angular lägger till
                    tabindex i koden som gör att man får fokus på fel element
                    när man navigerar med tangentbord. Använder man
                    @digi/arbetsformedlingen-angular i tidigare version än
                    19.0.0 så rekommenderar vi att använda{' '}
                    <digi-code af-code="af-override-link"></digi-code> och
                    istället sköta routing programmatisk.
                  </li>
                </digi-list>
              </div>
            </digi-info-card>
          </digi-layout-container>
        )}
      </div>
    );
  }
}
