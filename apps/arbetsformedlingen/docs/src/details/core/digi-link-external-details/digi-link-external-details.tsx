import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  InfoCardHeadingLevel,
  LinkVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-link-external-details',
  styleUrl: 'digi-link-external-details.scss'
})
export class DigiLinkExternalDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() linkVariation: LinkVariation = LinkVariation.SMALL;

  get linkCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-link-external
	af-href="#"
	af-target="_blank"
	af-variation="${Object.keys(LinkVariation)
    .find((key) => LinkVariation[key] === this.linkVariation)
    .toLocaleLowerCase()}"
>
	Jag är en extern länk
</digi-link-external>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-link-external
	[attr.af-link-container]="true"
	[attr.af-variation]="LinkVariation.${Object.keys(LinkVariation).find(
    (key) => LinkVariation[key] === this.linkVariation
  )}"
	[attr.af-href]="'#'"
	[attr.af-target]="'_blank'"
>
	Jag är en extern länk
</digi-link-external>`,
      [CodeExampleLanguage.REACT]: `\
<DigiLinkExternal
	afHref="#"
	afTarget="_blank"
	afVariation={LinkVariation.${Object.keys(LinkVariation).find(
    (key) => LinkVariation[key] === this.linkVariation
  )}}
>
	Jag är en extern länk
</DigiLinkExternal>`
    };
  }

  render() {
    return (
      <div class="digi-link-external-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används för att länka till externa webbsidor.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.linkCode)}
              // afCodeBlockVariation={CodeExampleVariation.LIGHT}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Storlek"
                  afLegend="Storlek"
                  onChange={(e) =>
                    (this.linkVariation = (e.target as HTMLFormElement).value)
                  }>
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Liten"
                    afValue={LinkVariation.SMALL}
                    afChecked={this.linkVariation === LinkVariation.SMALL}
                  />
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Stor"
                    afValue={LinkVariation.LARGE}
                    afChecked={this.linkVariation === LinkVariation.LARGE}
                  />
                </digi-form-fieldset>
              </div>
              <digi-link-external
                afHref="#"
                af-target="_blank"
                af-variation={this.linkVariation}>
                Jag är en extern länk
              </digi-link-external>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <p>
                Externa länkar finns i två storlekar, varav den lilla är
                standard. Använd <digi-code af-code="af-variation"></digi-code>{' '}
                för att välja vilken du vill ha, t.ex.{' '}
                <digi-code af-code="af-variation='large'"></digi-code>.
              </p>
              <digi-info-card
                afHeading="Riktlinjer och tillgänglighet"
                afHeadingLevel={InfoCardHeadingLevel.H3}>
                <div class="digi-typography digi-typography--s">
                  <digi-list>
                    <li>
                      Mer utförliga riktlinjer finns i vårt{' '}
                      <a href="/designmonster/lankar">
                        designmönster för länkar
                      </a>
                      .
                    </li>
                    <li>
                      En länktext ska vara tydlig så att användaren förstår vart
                      en länk leder även om länken lyfts ur sitt sammanhang.
                      Undvik länkar som endast heter "Tillbaka" eller "Läs mer".
                    </li>
                    <li>Skriv länktexten så kort som möjligt.</li>
                    <li>
                      Navigering på webbplatsen och inom tjänsterna sker primärt
                      via brödsmulor. Tillbakalänkar används endast i
                      undantagsfall.
                    </li>
                    <li>
                      Alla ikoner som utgör en länk och som definieras inom
                      &lt;i&gt; skall ha attributet{' '}
                      <digi-code af-code='aria-hidden="true"'></digi-code>.
                    </li>
                    <li>
                      Behöver du förtydliga länkens syfte eller mål så kan
                      attributet <digi-code af-code="aria-label"></digi-code>{' '}
                      användas. Det är svårt att säga när attributet behövs, men
                      om flera länkar i t.ex. ett sökresultat är identiska så
                      kan aria-label användas.
                      <br />
                      <br />
                      Kom ihåg att attributet aria-label:
                      <br />
                      - endast används av skärmläsare och andra liknande
                      verktyg.
                      <br />- skriver över länktexten för användare av dessa
                      verktyg.
                    </li>
                  </digi-list>
                </div>
              </digi-info-card>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
