import { Component, h, Host, Prop, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  QuoteSingleVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-quote-single-details',
  styleUrl: 'digi-quote-single-details.scss'
})
export class DigiQuoteSingle {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() QuoteSingleVariation: QuoteSingleVariation =
    QuoteSingleVariation.PRIMARY;

  get quoteSingleCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-quote-single
af-variation="${this.QuoteSingleVariation}"
af-quote-author-name="Namn"
af-quote-author-title="Roll"	
>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</digi-quote-single>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-quote-single
[attr.af-variation]="QuoteSingleVariation.${Object.keys(QuoteSingleVariation).find((key) => QuoteSingleVariation[key] === this.QuoteSingleVariation)}"
[attr.af-quote-author-name]="'Namn'"
[attr.af-quote-author-title]="'Roll'"	
>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</digi-quote-single>`,
      [CodeExampleLanguage.REACT]: `\
<digi-quote-single
afVariation={QuoteSingleVariation.${Object.keys(QuoteSingleVariation).find((key) => QuoteSingleVariation[key] === this.QuoteSingleVariation)}}
afQuoteAuthorName="Namn"
afQuoteAuthorTitle="Roll"	
>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</digi-quote-single>`
    };
  }

  render() {
    return (
      <Host>
        <div class="digi-quote-single-details">
          <digi-typography>
            {!this.afShowOnlyExample && (
              <digi-typography-preamble>
                Det här är en citatkomponent som är avsedd för ha blockcitat.
                Ett blockcitat är ett citat som är utformad för att förskjutas
                från huvudtexten på en sida.
              </digi-typography-preamble>
            )}
            <digi-layout-container af-no-gutter af-margin-bottom>
              <article>
                {!this.afShowOnlyExample && <h2>Exempel</h2>}
                <digi-code-example
                  af-code={JSON.stringify(this.quoteSingleCode)}
                  af-hide-controls={this.afHideControls ? 'true' : 'false'}
                  af-hide-code={this.afHideCode ? 'true' : 'false'}>
                  <div class="slot__controls" slot="controls">
                    <digi-form-select
                      afLabel="Variant"
                      onAfOnChange={(e) =>
                        (this.QuoteSingleVariation = (
                          e.target as HTMLDigiFormSelectElement
                        ).value as QuoteSingleVariation)
                      }
                      af-variation="small"
                      af-start-selected="0">
                      <option value="primary">Primär</option>
                      <option value="secondary">Sekundär</option>
                      <option value="tertiary">Tertiär</option>
                    </digi-form-select>
                  </div>
                  <digi-quote-single
                    afVariation={this.QuoteSingleVariation}
                    afQuoteAuthorName="Namn"
                    afQuoteAuthorTitle="Roll">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.
                  </digi-quote-single>
                </digi-code-example>
              </article>
            </digi-layout-container>
            {!this.afShowOnlyExample && (
              <digi-layout-block
                af-variation="symbol"
                af-vertical-padding
                af-margin-bottom>
                <h2>Riktlinjer</h2>
                <digi-list af-list-type="bullet">
                  <li>
                    Använd gärna citat-komponenten istället för andra
                    komponenter när ni ska visa citat på er sida.
                  </li>
                  <li>
                    Detta är en komponent som endast ska användas som citat,
                    därför ska inte ikonen bytas ut.
                  </li>
                  <li>
                    En bra längd på ett citat är på runt 280 tecken inklusive
                    mellanslag. Men kortare kan vara att föredra.
                  </li>
                </digi-list>
              </digi-layout-block>
            )}
          </digi-typography>
        </div>
      </Host>
    );
  }
}
