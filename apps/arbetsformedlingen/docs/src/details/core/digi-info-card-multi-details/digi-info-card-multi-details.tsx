import { Component, h, Host, Prop, State, getAssetPath } from '@stencil/core';
import {
  InfoCardMultiType,
  InfoCardHeadingLevel,
  InfoCardMultiHeadingLevel,
  CodeExampleLanguage,
  TagMediaIcon
} from '@digi/arbetsformedlingen';

enum SelectCardAmount {
  SINGLE = 'single',
  MULTI = 'multi'
}

@Component({
  tag: 'digi-info-card-multi-details',
  styleUrl: 'digi-info-card-multi-details.scss'
})
export class DigiInfoCardMultiDetails {
  @Prop() selectCardAmount: SelectCardAmount = SelectCardAmount.SINGLE;
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @Prop() tagMediaIcon: TagMediaIcon = TagMediaIcon.NEWS;

  @State() infoCardMultiType: InfoCardMultiType = InfoCardMultiType.MEDIA;
  @State() infoCardMultiHeadingLevel: InfoCardMultiHeadingLevel =
    InfoCardMultiHeadingLevel.H2;

  get infoCardMultiCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
${this.selectCardAmount === SelectCardAmount.MULTI ? '<digi-info-card-multi-container>' : ''}
  <digi-info-card-multi
    af-heading="Jag är ett multikort"
    af-heading-level="h2"
    af-type="${this.infoCardMultiType}"
    af-link-href="länk"\
    ${this.infoCardMultiType === InfoCardMultiType.MEDIA ? `\n \taf-tag-text="Taggtext"` : ''}\
    ${this.infoCardMultiType === InfoCardMultiType.MEDIA ? `\n \taf-tag-icon="${this.tagMediaIcon}"` : ''}
  >
  ${
    this.infoCardMultiType !== InfoCardMultiType.MEDIA
      ? `<p>
    Det här är bara ord för att illustrera hur det ser ut med text inuti.
  </p> \n`
      : ''
  }\
</digi-info-card-multi>
  ${this.selectCardAmount === SelectCardAmount.MULTI ? '<digi-info-card-multi>...</digi-info-card-multi>' : ' '}
  ${this.selectCardAmount === SelectCardAmount.MULTI ? '<digi-info-card-multi>...</digi-info-card-multi>' : ' '}
${this.selectCardAmount === SelectCardAmount.MULTI ? '</digi-info-card-multi-container>' : ''}`,
      [CodeExampleLanguage.ANGULAR]: `\
${this.selectCardAmount === SelectCardAmount.MULTI ? '<digi-info-card-multi-container>' : ''}
  <digi-info-card-multi
    [attr.af-heading]="'Jag är ett multikort'"
    [attr.af-heading-level]="InfoCardMultiHeadingLevel.${Object.keys(InfoCardMultiHeadingLevel).find((key) => InfoCardMultiHeadingLevel[key] === this.infoCardMultiHeadingLevel)}"
    [attr.af-type]="InfoCardMultiType.${Object.keys(InfoCardMultiType).find((key) => InfoCardMultiType[key] === this.infoCardMultiType)}"
    [attr.af-link-href]="'länk'"\
    ${this.infoCardMultiType === InfoCardMultiType.MEDIA ? `\n \t[attr.af-tag-text]="'Taggtext'"` : ''}\
    ${this.infoCardMultiType === InfoCardMultiType.MEDIA ? `\n \t[attr.af-tag-icon]="TagMediaIcon.${Object.keys(TagMediaIcon).find((key) => TagMediaIcon[key] === this.tagMediaIcon)}"` : ''}
  >
  ${
    this.infoCardMultiType !== InfoCardMultiType.MEDIA
      ? `<p>
    Det här är bara ord för att illustrera hur det ser ut med text inuti.
  </p>`
      : ''
  }\
</digi-info-card-multi>
  ${this.selectCardAmount === SelectCardAmount.MULTI ? '<digi-info-card-multi>...</digi-info-card-multi>' : ' '}
  ${this.selectCardAmount === SelectCardAmount.MULTI ? '<digi-info-card-multi>...</digi-info-card-multi>' : ' '}
${this.selectCardAmount === SelectCardAmount.MULTI ? '</digi-info-card-multi-container>' : ''}`,
      [CodeExampleLanguage.REACT]: `\
${this.selectCardAmount === SelectCardAmount.MULTI ? '<digi-info-card-multi-container>' : ''}
  <digi-info-card-multi
    afHeading="Jag är ett multikort"
    afHeadingLevel={InfoCardMultiHeadingLevel.${Object.keys(InfoCardMultiHeadingLevel).find((key) => InfoCardMultiHeadingLevel[key] === this.infoCardMultiHeadingLevel)}}
    afType={InfoCardMultiType.${Object.keys(InfoCardMultiType).find((key) => InfoCardMultiType[key] === this.infoCardMultiType)}}
    afLinkHref="länk"\
    ${this.infoCardMultiType === InfoCardMultiType.MEDIA ? `\n \tafTagText="Taggtext"` : ''}\
    ${this.infoCardMultiType === InfoCardMultiType.MEDIA ? `\n \tafTagIcon={TagMediaIcon.${Object.keys(TagMediaIcon).find((key) => TagMediaIcon[key] === this.tagMediaIcon)}}` : ''}
  >
  ${
    this.infoCardMultiType !== InfoCardMultiType.MEDIA
      ? `<p>
    Det här är bara ord för att illustrera hur det ser ut med text inuti.
  </p>`
      : ''
  }\
</digi-info-card-multi>
  ${this.selectCardAmount === SelectCardAmount.MULTI ? '<digi-info-card-multi>...</digi-info-card-multi>' : ' '}
  ${this.selectCardAmount === SelectCardAmount.MULTI ? '<digi-info-card-multi>...</digi-info-card-multi>' : ' '}
${this.selectCardAmount === SelectCardAmount.MULTI ? '</digi-info-card-multi-container>' : ''}`
    };
  }

  render() {
    return (
      <Host>
        <div class="digi-info-card-multi-details">
          <digi-typography>
            {!this.afShowOnlyExample && (
              <digi-typography-preamble>
                Multikort med länk som kan innehålla bild med tag eller bara
                text.
              </digi-typography-preamble>
            )}
            <digi-layout-container af-no-gutter af-margin-bottom>
              <article>
                {!this.afShowOnlyExample && <h2>Exempel</h2>}
                <digi-code-example
                  af-code={JSON.stringify(this.infoCardMultiCode)}
                  af-hide-controls={this.afHideControls ? 'true' : 'false'}
                  af-hide-code={this.afHideCode ? 'true' : 'false'}>
                  {this.selectCardAmount === SelectCardAmount.SINGLE && (
                    <div>
                      {this.infoCardMultiType == InfoCardMultiType.MEDIA && (
                        <digi-info-card-multi
                          afHeading="Jag är ett multikort"
                          afHeadingLevel={InfoCardMultiHeadingLevel.H2}
                          afType={this.infoCardMultiType}
                          af-src={getAssetPath(
                            '/assets/images/employment-service-sign.jpg'
                          )}
                          afAlt="Alt Text"
                          af-tag-icon={this.tagMediaIcon}
                          af-tag-text="Taggtext"
                          af-link-href="/"></digi-info-card-multi>
                      )}

                      {this.infoCardMultiType == InfoCardMultiType.RELATED && (
                        <digi-info-card-multi
                          afHeading="Jag är ett multikort"
                          afHeadingLevel={InfoCardMultiHeadingLevel.H2}
                          afType={this.infoCardMultiType}
                          af-link-href="/">
                          <p>
                            Det här är bara ord för att illustrera hur det ser
                            ut med text inuti.
                          </p>
                        </digi-info-card-multi>
                      )}

                      {this.infoCardMultiType == InfoCardMultiType.ENTRY && (
                        <digi-info-card-multi
                          afHeading="Jag är ett multikort"
                          afHeadingLevel={InfoCardMultiHeadingLevel.H2}
                          afType={this.infoCardMultiType}
                          af-link-href="/">
                          <p>
                            Det här är bara ord för att illustrera hur det ser
                            ut med text inuti.
                          </p>
                        </digi-info-card-multi>
                      )}
                    </div>
                  )}

                  {this.selectCardAmount === SelectCardAmount.MULTI && (
                    <div>
                      <digi-info-card-multi-container>
                        {this.infoCardMultiType == InfoCardMultiType.MEDIA &&
                          Array.from({ length: 3 }, () => (
                            <digi-info-card-multi
                              afHeading="Jag är ett multikort"
                              afHeadingLevel={InfoCardMultiHeadingLevel.H2}
                              afType={this.infoCardMultiType}
                              af-src={getAssetPath(
                                '/assets/images/employment-service-sign.jpg'
                              )}
                              afAlt="Alt Text"
                              af-tag-icon={this.tagMediaIcon}
                              af-tag-text="Taggtext"
                              af-link-href="/"></digi-info-card-multi>
                          ))}

                        {this.infoCardMultiType == InfoCardMultiType.RELATED &&
                          Array.from({ length: 3 }, () => (
                            <digi-info-card-multi
                              afHeading="Jag är ett multikort"
                              afHeadingLevel={InfoCardMultiHeadingLevel.H2}
                              afType={this.infoCardMultiType}
                              af-link-href="/">
                              <p>
                                Det här är bara ord för att illustrera hur det
                                ser ut med text inuti.
                              </p>
                            </digi-info-card-multi>
                          ))}

                        {this.infoCardMultiType == InfoCardMultiType.ENTRY &&
                          Array.from({ length: 3 }, () => (
                            <digi-info-card-multi
                              afHeading="Jag är ett multikort"
                              afHeadingLevel={InfoCardMultiHeadingLevel.H2}
                              afType={this.infoCardMultiType}
                              af-link-href="/">
                              <p>
                                Det här är bara ord för att illustrera hur det
                                ser ut med text inuti.
                              </p>
                            </digi-info-card-multi>
                          ))}
                      </digi-info-card-multi-container>
                    </div>
                  )}

                  <div class="slot__controls" slot="controls">
                    <digi-form-fieldset
                      afName="Type"
                      afLegend="Typ"
                      onChange={(e) =>
                        (this.infoCardMultiType = (
                          e.target as HTMLFormElement
                        ).value)
                      }>
                      <digi-form-radiobutton
                        afName="Type"
                        afLabel="Media multi"
                        afValue={InfoCardMultiType.MEDIA}
                        afChecked={
                          this.infoCardMultiType === InfoCardMultiType.MEDIA
                        }
                      />
                      <digi-form-radiobutton
                        afName="Type"
                        afLabel="Relaterad multi"
                        afValue={InfoCardMultiType.RELATED}
                        afChecked={
                          this.infoCardMultiType === InfoCardMultiType.RELATED
                        }
                      />
                      <digi-form-radiobutton
                        afName="Type"
                        afLabel="Behovsingång multi"
                        afValue={InfoCardMultiType.ENTRY}
                        afChecked={
                          this.infoCardMultiType === InfoCardMultiType.ENTRY
                        }
                      />
                    </digi-form-fieldset>

                    {this.infoCardMultiType == InfoCardMultiType.MEDIA && (
                      <digi-form-select
                        afLabel="Ikoner"
                        onAfOnChange={(e) =>
                          (this.tagMediaIcon = (
                            e.target as HTMLDigiFormSelectElement
                          ).value as TagMediaIcon)
                        }
                        af-variation="small"
                        af-start-selected="0">
                        <option value="news">Nyheter</option>
                        <option value="playlist">Spellista</option>
                        <option value="podcast">Podcast</option>
                        <option value="film">Film</option>
                        <option value="webtv">Webb-Tv</option>
                        <option value="webinar">Webbinar</option>
                      </digi-form-select>
                    )}

                    <digi-form-fieldset
                      afName="Varianter"
                      afLegend="Varianter"
                      onChange={(e) =>
                        (this.selectCardAmount = (
                          e.target as HTMLFormElement
                        ).value)
                      }>
                      <digi-form-radiobutton
                        afName="Varianter"
                        afLabel="Singel"
                        afValue={SelectCardAmount.SINGLE}
                        afChecked={
                          this.selectCardAmount === SelectCardAmount.SINGLE
                        }
                      />
                      <digi-form-radiobutton
                        afName="Varianter"
                        afLabel="Multi"
                        afValue={SelectCardAmount.MULTI}
                        afChecked={
                          this.selectCardAmount === SelectCardAmount.MULTI
                        }
                      />
                    </digi-form-fieldset>
                  </div>
                </digi-code-example>
              </article>
            </digi-layout-container>
            {!this.afShowOnlyExample && (
              <digi-layout-container afNoGutter afMarginBottom>
                <digi-info-card
                  afHeading="Riktlinjer"
                  afHeadingLevel={InfoCardHeadingLevel.H3}>
                  <div class="digi-typography digi-typography--s">
                    <digi-list>
                      <li>
                        Multikort är kort som är tänkta att användas ihop i
                        layouter med samma sorts kort på en rad, med kurerat
                        innehåll och längder på texter.
                      </li>
                      <li>
                        I mobil staplas dessa kort på och under varandra i en
                        spalt.
                      </li>
                      <li>
                        Multikort media har en tagg i sig som hjälper användaren
                        att förstå vilken sorts media kortet förväntas leda
                        till.
                      </li>
                      <li>
                        Alla multikort är vita och är tänkta att ligga på en
                        ljust grå bakgrund.
                      </li>
                    </digi-list>
                  </div>
                </digi-info-card>
              </digi-layout-container>
            )}
          </digi-typography>
        </div>
      </Host>
    );
  }
}
