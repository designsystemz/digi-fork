import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  CodeVariation,
  CodeLanguage,
  FormSelectVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-code-details',
  styleUrl: 'digi-code-details.scss'
})
export class DigiCodeDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() codeLanguage: CodeLanguage = CodeLanguage.HTML;
  @State() language = 'html';
  @State() codeVariation: CodeVariation = CodeVariation.LIGHT;
  @State() codeLanguages = {
    html: '<digi-button>HTML-exempel</digi-button>',
    javascript: 'const foo = bar',
    typescript: "const foo: User = new User('Tesla')",
    css: '--foo: var(--bar)',
    scss: "@import 'globals/mixins/my-mixin'",
    json: '{ foo: bar }',
    bash: 'npm run start core'
  };

  get codeLanguageCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-code
	af-code="${this.codeLanguages[this.language]}"
	af-variation="${Object.keys(CodeVariation)
    .find((key) => CodeVariation[key] === this.codeVariation)
    .toLocaleLowerCase()}"
>
</digi-code>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-code
	[attr.af-code]="${this.codeLanguages[this.language]}"
	[attr.af-variation]="CodeVariation.${Object.keys(CodeVariation).find((key) => CodeVariation[key] === this.codeVariation)}"
>
</digi-code>`,
      [CodeExampleLanguage.REACT]: `\
<DigiCode
	afCode="${this.codeLanguages[this.language]}"
	afVariation={CodeVariation.${Object.keys(CodeVariation).find((key) => CodeVariation[key] === this.codeVariation)}}
/>`
    };
  }

  render() {
    return (
      <div class="digi-code-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Kodkomponenten kan användas för att skapa syntaxmarkerade
              kodexempel.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.codeLanguageCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              {Object.keys(this.codeLanguages).map((key) => {
                if (key === this.language) {
                  return (
                    <digi-code
                      afVariation={this.codeVariation}
                      af-language={key}
                      afCode={this.codeLanguages[`${key}`]}></digi-code>
                  );
                }
              })}
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variant"
                  afLegend="Variant"
                  onChange={(e) =>
                    (this.codeVariation = (e.target as HTMLFormElement).value)
                  }>
                  <digi-form-radiobutton
                    afName="Variant"
                    afLabel="Ljus"
                    afValue={CodeVariation.LIGHT}
                    afChecked={this.codeVariation === CodeVariation.LIGHT}
                  />
                  <digi-form-radiobutton
                    afName="Variant"
                    afLabel="Mörk"
                    afValue={CodeVariation.DARK}
                    afChecked={this.codeVariation === CodeVariation.DARK}
                  />
                </digi-form-fieldset>
                <digi-form-select
                  afLabel="Kodspråk"
                  af-start-selected="0"
                  afVariation={FormSelectVariation.SMALL}
                  onAfOnChange={(e) =>
                    (this.language = (
                      e.target as HTMLDigiFormSelectElement
                    ).value)
                  }>
                  <option value="html">HTML</option>
                  <option value="javascript">JavaScript</option>
                  <option value="typescript">TypeScript</option>
                  <option value="css">CSS</option>
                  <option value="scss">SCSS</option>
                  <option value="json">JSON</option>
                  <option value="bash">Bash</option>
                </digi-form-select>
              </div>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <p>
                Kodkomponenten finns i en mörk eller en ljus variant, varav ljus
                är standard. Använd{' '}
                <digi-code af-code="af-variation"></digi-code> för att ange
                variant.
              </p>
              <h3>Språk</h3>
              <p>
                Kodkomponenten stödjer olika språk såsom HTML (standard),
                JavaScript, CSS, SCSS, TypeScript, JSON och Bash. Använd{' '}
                <digi-code af-code="af-language"></digi-code>för att ange språk.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
