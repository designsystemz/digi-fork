import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  ProgressbarVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-progressbar-details',
  styleUrl: 'digi-progressbar-details.scss'
})
export class DigiProgressbarDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() progressbarVariation: ProgressbarVariation =
    ProgressbarVariation.PRIMARY;
  @State() completedSteps = 2;

  get progressbarCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-progressbar
	af-total-steps="4"
	af-completed-steps="${this.completedSteps}"
	af-variation="${this.progressbarVariation}"
>
</digi-progressbar>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-progressbar
	[attr.af-total-steps]="4"
	[attr.af-completed-steps]="${this.completedSteps}"
	[attr.af-variation]="ProgressbarVariation.${Object.keys(ProgressbarVariation).find((key) => ProgressbarVariation[key] === this.progressbarVariation)}"
>
</digi-progressbar>`,
      [CodeExampleLanguage.REACT]: `\
<DigiProgressbar
	afTotalSteps={4}
	afCompletedSteps={${this.completedSteps}}
	afVariation={ProgressbarVariation.${Object.keys(ProgressbarVariation).find((key) => ProgressbarVariation[key] === this.progressbarVariation)}}
>
</DigiProgressbar>`
    };
  }

  render() {
    return (
      <div class="digi-progressbar-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten visar hur mycket man hunnit göra klart i ett
              uttänkt flöde, som t.ex. olika formulärdelar på en sida. Behöver
              man istället påvisa i steg vart man är i ett förlopp bör man
              använda sig av komponenten{' '}
              <digi-code af-code="<digi-progress-steps>"></digi-code>
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.progressbarCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <digi-progressbar
                af-total-steps={4}
                af-variation={this.progressbarVariation}
                af-completed-steps={this.completedSteps}></digi-progressbar>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variation"
                  afLegend="Variation"
                  onChange={(e) =>
                    (this.progressbarVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Primär"
                    afValue={ProgressbarVariation.PRIMARY}
                    afChecked={
                      this.progressbarVariation === ProgressbarVariation.PRIMARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Sekundär"
                    afValue={ProgressbarVariation.SECONDARY}
                    afChecked={
                      this.progressbarVariation ===
                      ProgressbarVariation.SECONDARY
                    }
                  />
                </digi-form-fieldset>
                <digi-form-select
                  afLabel="Färdiga steg"
                  onAfOnChange={(e) =>
                    (this.completedSteps = parseInt(
                      (e.target as HTMLDigiFormSelectElement).value
                    ))
                  }
                  af-variation="small"
                  af-start-selected={this.completedSteps - 1}>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </digi-form-select>
              </div>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <p>
                Det finns en primär (förvald) och en sekundär variant av
                komponenten som ställs in med hjälp av{' '}
                <digi-code af-code="af-variation"></digi-code>.
              </p>

              <h4>Icke-linjär variant</h4>
              <p>
                Det går även att anpassa komponenten så att stegen kan markeras
                som färdiga i en icke-linjär ordning. Då utelämnar man
                attributen <digi-code af-code="af-total-steps"></digi-code> och
                <digi-code af-code="af-completed-steps"></digi-code> och lägger
                istället in stegen manuellt inuti komponenten samt anger vilka
                steg som är klara genom att lägga till klassen{' '}
                <digi-code af-code="completed"></digi-code> på dem.
              </p>
              <digi-code-example
                af-code={`<digi-progressbar
  af-total-steps="4"
  af-completed-steps="2"
>
  <li class="completed"></li>
  <li></li>
  <li class="completed"></li>
  <li></li>
</digi-progressbar>
            `}>
                <digi-progressbar
                  af-total-steps="4"
                  af-completed-steps="2"
                  af-steps-label="frågor besvarade">
                  <li class="completed"></li>
                  <li></li>
                  <li class="completed"></li>
                  <li></li>
                </digi-progressbar>
              </digi-code-example>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
