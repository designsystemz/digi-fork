import { Component, h, Prop } from '@stencil/core';
import {
  CodeExampleLanguage,
  InfoCardHeadingLevel,
  InfoCardVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-header-navigation-item-details',
  styleUrl: 'digi-header-navigation-item-details.scss'
})
export class DigiHeaderNavigationItem {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @Prop() isCurrentPage: boolean;

  get headerNavigationItemCode() {
    return {
      [CodeExampleLanguage.HTML]: `		 
<digi-header-navigation-item af-current-page="${!this.isCurrentPage ? 'true' : 'false'}">
	<a href="/">Mina bokningar</a>
</digi-header-navigation-item>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-header-navigation-item [attr.af-current-page]="${!this.isCurrentPage ? 'true' : 'false'}">
	<a href="'/'" [routerLink]="['/']">Mina bokningar</a>
</digi-header-navigation-item>`,
      [CodeExampleLanguage.REACT]: `\
<DigiHeaderNavigationItem afCurrentPage="${!this.isCurrentPage ? 'true' : 'false'}">
	<a href="/">Mina bokningar</a>
</DigiHeaderNavigationItem>`
    };
  }

  render() {
    return (
      <div>
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används tillsammans med{' '}
              <a href="/komponenter/digi-header-navigation/oversikt">
                <digi-code af-code="<digi-header-navigation>"></digi-code>{' '}
              </a>
              för att skapa menyer i header komponenten.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.headerNavigationItemCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset afLegend="Nuvarande sida">
                  <digi-form-checkbox
                    afLabel="Ja"
                    afChecked={!this.isCurrentPage}
                    onAfOnChange={() =>
                      this.isCurrentPage
                        ? (this.isCurrentPage = false)
                        : (this.isCurrentPage = true)
                    }></digi-form-checkbox>
                </digi-form-fieldset>
              </div>
              <digi-header-navigation>
                <digi-header-navigation-item
                  af-current-page={!this.isCurrentPage}>
                  <a href="https://designsystem.arbetsformedlingen.se/">
                    Mina bokningar
                  </a>
                </digi-header-navigation-item>
              </digi-header-navigation>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <p>
                Komponenten måste användas tillsammans med{' '}
                <a href="/komponenter/digi-header-navigation/oversikt">
                  <digi-code af-code="<digi-header-navigation>"></digi-code>
                </a>
                .
                <p>
                  Attributet <digi-code af-code="af-current-page" /> används för
                  att markera ett menyval som nuvarande sida.
                </p>
              </p>
            </digi-layout-container>
          )}
          {!this.afShowOnlyExample && (
            <digi-layout-block af-container="none" af-margin-bottom>
              <digi-info-card
                afHeading="Bra att veta"
                afHeadingLevel={`h3` as InfoCardHeadingLevel}
                afVariation={InfoCardVariation.PRIMARY}>
                <p>
                  Komponenten kan enbart användas inuti komponenten{' '}
                  <a href="/komponenter/digi-header-navigation/oversikt">
                    <digi-code af-code="<digi-header-navigation>"></digi-code>
                  </a>
                  .
                </p>
              </digi-info-card>
              <br />
            </digi-layout-block>
          )}
        </digi-typography>
      </div>
    );
  }
}
