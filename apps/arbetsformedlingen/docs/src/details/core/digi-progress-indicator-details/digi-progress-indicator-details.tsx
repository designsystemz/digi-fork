import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  ProgressIndicatorVariation
} from '@digi/arbetsformedlingen';

enum ProgressIndicatorValidation {
  NONE = '0',
  WARNING = '1',
  ERROR = '2'
}

@Component({
  tag: 'digi-progress-indicator-details',
  styleUrl: 'digi-progress-indicator-details.scss'
})
export class DigiProgressIndicatorDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() progressIndicatorVariation: ProgressIndicatorVariation =
    ProgressIndicatorVariation.PRIMARY;
  @State() completedSteps = 2;
  @State() warningStep = 0;
  @State() errorStep = 0;
  @State() validationLabel: string;

  @State() progressIndicatorValidation: ProgressIndicatorValidation =
    ProgressIndicatorValidation.NONE;

  get progressIndicatorCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-progress-indicator
	af-step-heading="Nuvarande steg"
	af-step-next-label="Kommande steg"
	af-total-steps="4"
	af-completed-steps="${this.completedSteps}"
	af-variation="${this.progressIndicatorVariation}"\
	${this.validationLabel ? `\n \taf-validation-label="${this.validationLabel}"` : ''}\
	${this.warningStep != 0 ? `\n \taf-warning-step="${this.warningStep}"` : ''}\
	${this.errorStep != 0 ? `\n \taf-error-step="${this.errorStep}"` : ''}\
\n>
</digi-progress-indicator>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-progress-indicator
	[attr.af-step-heading]="Nuvarande steg"
	[attr.af-step-next-label]="Kommande steg"
	[attr.af-total-steps]="4"
	[attr.af-completed-steps]="${this.completedSteps}"
	[attr.af-variation]="ProgressIndicatorVariation.${Object.keys(ProgressIndicatorVariation).find((key) => ProgressIndicatorVariation[key] === this.progressIndicatorVariation)}"\
	${this.validationLabel ? `\n \t[attr.af-validation-label]="${this.validationLabel}"` : ''}\
	${this.warningStep != 0 ? `\n \t[attr.af-warning-step]="${this.warningStep}"` : ''}\
	${this.errorStep != 0 ? `\n \t[attr.af-error-step]="${this.errorStep}"` : ''}\
\n>
</digi-progress-indicator>`,
      [CodeExampleLanguage.REACT]: `\
<DigiProgressIndicator
	afStepHeading="Nuvarande steg"
	afStepNext-label="Kommande steg"
	afTotalSteps={4}
	afCompletedSteps={${this.completedSteps}}
	afVariation={ProgressIndicatorVariation.${Object.keys(ProgressIndicatorVariation).find((key) => ProgressIndicatorVariation[key] === this.progressIndicatorVariation)}}\
	${this.validationLabel ? `\n \tafValidationLabel="${this.validationLabel}"` : ''}\
	${this.warningStep != 0 ? `\n \tafWarningStep="${this.warningStep}"` : ''}\
	${this.errorStep != 0 ? `\n \tafErrorStep="${this.errorStep}"` : ''}\
\n>
</DigiProgressIndicator>`
    };
  }

  validationChangeHandler() {
    if (this.progressIndicatorValidation === ProgressIndicatorValidation.NONE) {
      this.warningStep = 0;
      this.errorStep = 0;
      this.validationLabel = '';
    }
    if (
      this.progressIndicatorValidation === ProgressIndicatorValidation.WARNING
    ) {
      this.warningStep = this.completedSteps;
      this.errorStep = 0;
      this.validationLabel = 'Åtgärder krävs!';
    }
    if (
      this.progressIndicatorValidation === ProgressIndicatorValidation.ERROR
    ) {
      this.warningStep = 0;
      this.errorStep = this.completedSteps;
      this.validationLabel = 'Stopp i processen';
    }
  }

  render() {
    return (
      <div class="digi-progress-indicator-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten visar hur mycket man hunnit göra klart i ett
              uttänkt flöde, som t.ex. olika formulärdelar på en sida. Behöver
              man istället påvisa i steg vart man är i ett förlopp bör man
              använda sig av komponenten{' '}
              <digi-code af-code="<digi-progress-list>"></digi-code>
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.progressIndicatorCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <digi-progress-indicator
                af-step-heading="Nuvarande steg"
                af-step-next-label="Kommande steg"
                af-total-steps={4}
                af-validation-label={this.validationLabel}
                af-warning-step={this.warningStep}
                af-error-step={this.errorStep}
                af-variation={this.progressIndicatorVariation}
                af-completed-steps={
                  this.completedSteps
                }></digi-progress-indicator>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variation"
                  afLegend="Variation"
                  onChange={(e) =>
                    (this.progressIndicatorVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Primär"
                    afValue={ProgressIndicatorVariation.PRIMARY}
                    afChecked={
                      this.progressIndicatorVariation ===
                      ProgressIndicatorVariation.PRIMARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Sekundär"
                    afValue={ProgressIndicatorVariation.SECONDARY}
                    afChecked={
                      this.progressIndicatorVariation ===
                      ProgressIndicatorVariation.SECONDARY
                    }
                  />
                </digi-form-fieldset>

                <digi-form-fieldset
                  afName="Status"
                  afLegend="Status"
                  onChange={(e) => (
                    (this.progressIndicatorValidation = (
                      e.target as HTMLFormElement
                    ).value),
                    this.validationChangeHandler()
                  )}>
                  <digi-form-radiobutton
                    afName="Status"
                    afLabel="Ingen"
                    afValue={ProgressIndicatorValidation.NONE}
                    afChecked={
                      this.progressIndicatorValidation ===
                      ProgressIndicatorValidation.NONE
                    }
                  />
                  <digi-form-radiobutton
                    afName="Status"
                    afLabel="Åtgärd"
                    afValue={ProgressIndicatorValidation.WARNING}
                    afChecked={
                      this.progressIndicatorValidation ===
                      ProgressIndicatorValidation.WARNING
                    }
                  />
                  <digi-form-radiobutton
                    afName="Status"
                    afLabel="Fel"
                    afValue={ProgressIndicatorValidation.ERROR}
                    afChecked={
                      this.progressIndicatorValidation ===
                      ProgressIndicatorValidation.ERROR
                    }
                  />
                </digi-form-fieldset>

                <digi-form-select
                  afLabel="Färdiga steg"
                  onAfOnChange={(e) => (
                    (this.completedSteps = parseInt(
                      (e.target as HTMLDigiFormSelectElement).value
                    )),
                    this.validationChangeHandler()
                  )}
                  af-variation="small"
                  af-start-selected={this.completedSteps - 1}>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </digi-form-select>
              </div>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <p>
                Det finns en primär (förvald) och en sekundär variant av
                komponenten som ställs in med hjälp av{' '}
                <digi-code af-code="af-variation"></digi-code>.
              </p>

              <h3>Status</h3>
              <p>
                Det finns två olika statusar för validgering av förloppsmätaren.
                Som standard är statusen dold. Det går att markera vilket steg
                som statusen ska visas på genom att ange stegnumret. Stegnumret
                kan antingen anges på{' '}
                <digi-code af-code="af-error-step"></digi-code> eller{' '}
                <digi-code af-code="af-warning-step"></digi-code> Det finns även
                en tillhörande statusrubrik som visas om{' '}
                <digi-code af-code="af-validation-label"></digi-code> är ifylld.
              </p>

              <h4>Icke-linjär variant</h4>
              <p>
                Det går även att anpassa komponenten så att stegen kan markeras
                som färdiga i en icke-linjär ordning. Då man lägger istället in
                stegen manuellt inuti komponenten samt anger vilka steg som är
                klara genom att lägga till klassen{' '}
                <digi-code af-code="completed"></digi-code> på dem.
              </p>
              <digi-code-example
                af-code={`<digi-progress-indicator
	af-step-heading="Nuvarande steg"
	af-step-next-label="Kommande steg"
	af-total-steps="4"
	af-completed-steps="2"
	af-steps-label="frågor besvarade"
>
  <li class="completed"></li>
  <li></li>
  <li class="completed"></li>
  <li></li>
</digi-progress-indicator>
            `}>
                <digi-progress-indicator
                  af-step-heading="Nuvarande steg"
                  af-step-next-label="Kommande steg"
                  af-total-steps="4"
                  af-completed-steps="2"
                  af-steps-label="frågor besvarade">
                  <li class="completed"></li>
                  <li></li>
                  <li class="completed"></li>
                  <li></li>
                </digi-progress-indicator>
              </digi-code-example>
              <h3>Beteende</h3>
              <p>
                När användaren kommit till sista steget och det finns ingen
                nästkommande steg så blir{' '}
                <digi-code af-code="af-step-next-label"></digi-code> dold.
                Värdet kan antigen vara tomt eller vara en blanksteg, så länge
                man är på sista steget så är etiketten dold. Men om man skulle
                vilja va själva etiketten för steg räkning under rubriken så kan
                man det genom attt inte ha med{' '}
                <digi-code af-code="af-step-next-label"></digi-code>. Tänk dock
                på att inte blanda dessa två olika utseenden i ett och samma
                flöde.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
