import { Component, Prop, h, State } from '@stencil/core';
import {
  FormInputType,
  FormInputSearchVariation,
  CodeExampleLanguage,
  InfoCardHeadingLevel,
  InfoCardVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-form-input-search-details',
  styleUrl: 'digi-form-input-search-details.scss'
})
export class DigiFormInputSearchDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() formInputType: FormInputType = FormInputType.SEARCH;
  @State() formInputSearchVariation: FormInputSearchVariation =
    FormInputSearchVariation.MEDIUM;
  @State() hasDescription = false;

  get searchInputCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-form-input-search
	af-label="Etikett"
	af-variation="${this.formInputSearchVariation}"
	af-type="${this.formInputType}"\
	${this.hasDescription ? `\n\taf-label-description="Beskrivande text"\t ` : ''}
	af-button-text="Knapptext" 
>
</digi-form-input-search>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-form-input-search
	[attr.af-label]="'Etikett'"
	[attr.af-variation]="FormInputSearchVariation.${Object.keys(FormInputSearchVariation).find((key) => FormInputSearchVariation[key] === this.formInputSearchVariation)}"
	[attr.af-type]="FormInputType.${Object.keys(FormInputType).find((key) => FormInputType[key] === this.formInputType)}"\
	${this.hasDescription ? `\n\t[attr.af-label-description]="'Beskrivande text'"\t ` : ''}
	[attr.af-button-text]="'Knapptext'"
>
</digi-form-input-search>`,
      [CodeExampleLanguage.REACT]: `\
<DigiFormInputSearch
	afLabel="Etikett"
	afVariation={FormInputSearchVariation.${Object.keys(FormInputSearchVariation).find((key) => FormInputSearchVariation[key] === this.formInputSearchVariation)}}
	afType={FormInputType.${Object.keys(FormInputType).find((key) => FormInputType[key] === this.formInputType)}}\
	${this.hasDescription ? `\n\tafLabelDescription="Beskrivande text"\t ` : ''}
	afButtonText="Knapptext"
>
</DigiFormInputSearch>`
    };
  }
  render() {
    return (
      <div class="digi-form-input-search-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Sökfältskomponenten är utformad så att användaren kan ange sökord
              via ett inmatningsfält. Den består av en etikett, ett
              inmatningsfält och en knapp.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.searchInputCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div style={{ width: '550px' }}>
                {!this.hasDescription && (
                  <digi-form-input-search
                    afLabel="Etikett"
                    af-validation-text="Det här är ett valideringsmeddelande"
                    afVariation={this.formInputSearchVariation}
                    af-type={this.formInputType}
                    afButtonText="Knapptext"></digi-form-input-search>
                )}
                {this.hasDescription && (
                  <digi-form-input-search
                    afLabel="Etikett"
                    afLabelDescription="Beskrivande text"
                    af-validation-text="Det här är ett valideringsmeddelande"
                    afVariation={this.formInputSearchVariation}
                    af-type={this.formInputType}
                    afButtonText="Knapptext"></digi-form-input-search>
                )}
              </div>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Storlek"
                  afLegend="Storlek"
                  onChange={(e) =>
                    (this.formInputSearchVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Liten"
                    afValue={FormInputSearchVariation.SMALL}
                    afChecked={
                      this.formInputSearchVariation ===
                      FormInputSearchVariation.SMALL
                    }
                  />
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Mellan"
                    afValue={FormInputSearchVariation.MEDIUM}
                    afChecked={
                      this.formInputSearchVariation ===
                      FormInputSearchVariation.MEDIUM
                    }
                  />
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Stor"
                    afValue={FormInputSearchVariation.LARGE}
                    afChecked={
                      this.formInputSearchVariation ===
                      FormInputSearchVariation.LARGE
                    }
                  />
                </digi-form-fieldset>

                <digi-form-select
                  afLabel="Typ"
                  onAfOnChange={(e) =>
                    (this.formInputType = (
                      e.target as HTMLDigiFormSelectElement
                    ).value as FormInputType)
                  }
                  af-variation="small"
                  af-value="search">
                  <option value="color">Färg</option>
                  <option value="date">Datum</option>
                  <option value="datetime-local">Lokalt datum</option>
                  <option value="email">Epostadress</option>
                  <option value="month">Månad</option>
                  <option value="number">Nummer</option>
                  <option value="password">Lösenord</option>
                  <option value="search">Sök</option>
                  <option value="tel">Telefonnummer</option>
                  <option value="text">Text</option>
                  <option value="time">Tid</option>
                  <option value="url">URL</option>
                  <option value="week">Vecka</option>
                </digi-form-select>
                <digi-form-fieldset afLegend="Beskrivande text">
                  <digi-form-checkbox
                    afLabel="Ja"
                    afChecked={this.hasDescription}
                    onAfOnChange={() =>
                      this.hasDescription
                        ? (this.hasDescription = false)
                        : (this.hasDescription = true)
                    }></digi-form-checkbox>
                </digi-form-fieldset>
              </div>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Storlekar</h3>
              <p>
                Sökfältskomponenten finns i tre olika storlekar som sätts med{' '}
                <digi-code af-code="af-variation" />: liten, mellan och stor,
                varav mellan är standard.
              </p>
              <h3>Typer</h3>
              <p>
                Inmatningsfältet stödjer de flesta fälttyper och mappar direkt
                till inputelementets <digi-code af-code="type"></digi-code>
                -attribut.
              </p>
              <h3>Sökknapp</h3>
              <p>
                Sökfältsknappens text sätts via
                <digi-code af-code="afButtonText" />.{' '}
                <digi-code af-code="afButtonType" /> sätter knappelementets
                attribut 'type'. Bör vara 'submit' eller 'button'{' '}
              </p>
              <h3>Resultatlista</h3>
              <p>
                I komponenten ingår idag ingen resultatlista, alltså en yta för
                sökförslag eller resultat från en sökning. Vi gjorde det
                beslutet då dessa resultatlistor skiljer sig väldigt beroende på
                användningsområde.
              </p>
            </digi-layout-container>
          )}
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <digi-info-card
                afHeading="Designmönster om sök"
                afHeadingLevel={`h3` as InfoCardHeadingLevel}
                af-link-href="/designmonster/sok-och-sokfilter"
                af-link-text="Designmönster om sök och sökfilter"
                afVariation={InfoCardVariation.SECONDARY}>
                <p>
                  Lär dig mer om hur du arbetar med sök och sökfilter genom att
                  läsa vårt designmönster.
                </p>
              </digi-info-card>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
