import { Component, Prop, h, State } from '@stencil/core';
import {
  ExpandableFaqItemVariation,
  CodeExampleLanguage
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-expandable-faq-item-details',
  styleUrl: 'digi-expandable-faq-item-details.scss'
})
export class DigiExpandableFAQItemDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() expandableFaqItemVariation: ExpandableFaqItemVariation =
    ExpandableFaqItemVariation.PRIMARY;

  get faqCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-expandable-faq-item 
	af-heading="Fråga" 
	af-variation="${this.expandableFaqItemVariation}">
	<p>
		Svar - Id occaecat id cupidatat dolore esse. 
		Mollit do non fugiat id mollit irure aute nulla id ipsum aute ullamco cupidatat ex. 
		Laborum id sint est pariatur quis.
	</p>
</digi-expandable-faq-item>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-expandable-faq-item 
	[attr.af-heading]="Fråga" 
	[attr.af-variation]="ExpandableFaqItemVariation.${Object.keys(ExpandableFaqItemVariation).find((k) => ExpandableFaqItemVariation[k] == this.expandableFaqItemVariation)}">
	<p>
		Svar - Id occaecat id cupidatat dolore esse. 
		Mollit do non fugiat id mollit irure aute nulla id ipsum aute ullamco cupidatat ex. 
		Laborum id sint est pariatur quis.
	</p>
</digi-expandable-faq-item>`,
      [CodeExampleLanguage.REACT]: `\
<DigiExpandableFaqItem 
	afHeading="Fråga" 
	afVariation={ExpandableFaqItemVariation.${Object.keys(ExpandableFaqItemVariation).find((k) => ExpandableFaqItemVariation[k] == this.expandableFaqItemVariation)}}>
	<p>
		Svar - Id occaecat id cupidatat dolore esse. 
		Mollit do non fugiat id mollit irure aute nulla id ipsum aute ullamco cupidatat ex. 
		Laborum id sint est pariatur quis.
	</p>
</DigiExpandableFaqItem>`
    };
  }

  render() {
    return (
      <div class="digi-expandable-faq-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används tillsammans med{' '}
              <digi-code af-variation="light" af-code="<digi-expandable-faq>" />{' '}
              för att skapa en lista med vanliga frågor och utfällbara svar.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.faqCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variation"
                  afLegend="Variation"
                  onChange={(e) =>
                    (this.expandableFaqItemVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Primär"
                    afValue={ExpandableFaqItemVariation.PRIMARY}
                    afChecked={
                      this.expandableFaqItemVariation ===
                      ExpandableFaqItemVariation.PRIMARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Sekundär"
                    afValue={ExpandableFaqItemVariation.SECONDARY}
                    afChecked={
                      this.expandableFaqItemVariation ===
                      ExpandableFaqItemVariation.SECONDARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Tertiär"
                    afValue={ExpandableFaqItemVariation.TERTIARY}
                    afChecked={
                      this.expandableFaqItemVariation ===
                      ExpandableFaqItemVariation.TERTIARY
                    }
                  />
                </digi-form-fieldset>
              </div>
              <digi-expandable-faq-item
                afHeading="Fråga"
                afVariation={this.expandableFaqItemVariation}>
                <p>
                  Svar - Duis irure cillum elit labore pariatur et occaecat.
                  Aute ad eu nisi pariatur excepteur nisi qui. Elit eu nisi
                  proident velit non consequat eiusmod.
                </p>
              </digi-expandable-faq-item>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <h3>Kodexempel</h3>
              <p>
                Rubriken läggs in med{' '}
                <digi-code af-variation="light" af-code="af-heading" /> och
                rubriknivån är <digi-code af-variation="light" af-code="h3" />{' '}
                som standard. Ligger den inbäddad i{' '}
                <digi-code
                  af-variation="light"
                  af-code="<digi-expandable-faq>"
                />
                -komponenten så blir rubriknivån automatiskt en rubriknivå lägre
                än rubriken till den. Vill du ändra rubriknivå kan du använda
                <digi-code af-variation="light" af-code="af-heading-level" /> .
                Typescript-användare bör importera och använda
                <digi-code
                  af-variation="light"
                  af-code="ExpandableFaqItemHeadingLevel"
                />
                . Stylingen på kommer se likadan ut även om du ändrar till en
                lägre rubriknivå.
              </p>
              <h3>Varianter</h3>
              <p>
                FAQ item-komponenten finns i tre varianter, en primär (primary)
                i ljusgrönt som är standard, en sekundär (secondary) i grått,
                samt en tertiär (tertiary) i vit. Variant sätts med hjälp av
                <digi-code af-variation="light" af-code="af-variation" />.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
