/* eslint-disable no-useless-escape */
import { Component, h, Host, Prop, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  FormSelectFilterValidation
} from '@digi/arbetsformedlingen';

const LIST_ITEMS = [
  { label: 'Danska' },
  { label: 'Tyska' },
  { label: 'العربية (Arabiska)', dir: 'rtl', lang: 'ar' },
  { label: 'Ukrainska' },
  { label: 'Franska' }
];

const EVENTS = [
  {
    name: 'afOnFilterClosed',
    body: `() => console.log('Användaren stängde')`
  },
  {
    name: 'afOnFocusOut',
    body: `() => console.log('Användarens fokus lämnade komponenten')`
  },
  {
    name: 'afOnQueryChanged',
    body: `() => console.log('Användaren söker efter alternativ', e.detail)`
  },
  {
    name: 'afOnResetFilters',
    body: `() => console.log('Användaren återställer')`
  },
  {
    name: 'afOnSelect',
    body: `() => console.log('Användaren väljer ett eller flera alternativ')`
  },
  {
    name: 'afOnSubmitFilters',
    body: `() => console.log('Användaren skickar in')`
  }
];

@Component({
  tag: 'digi-form-select-filter-details',
  styleUrl: 'digi-form-select-filter-details.scss'
})
export class DigiFormSelectFilter {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() formSelectFilterValidation: FormSelectFilterValidation =
    FormSelectFilterValidation.NEUTRAL;
  @State() afMultipleItems = false;

  get formSelectFilterCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-form-select-filter
	af-filter-button-text-label="Välj språk"
	af-description="Beskrivande text"
	af-filter-button-text="Inget språk valt"
	af-name="Sök språk"
	af-submit-button-text="Filtrera"
	af-multiple-items="${this.afMultipleItems}"
	af-validation="${this.formSelectFilterValidation}"
	${this.formSelectFilterValidation !== FormSelectFilterValidation.NEUTRAL
      ? 'af-validation-text="Det här är ett valideringsmeddelande"\t'
      : ''}
>
</digi-form-select-filter>

<script>
  const element = document.querySelector('digi-form-select-filter');

  element.afListItems = ${JSON.stringify(LIST_ITEMS)};
</script>`,
      [CodeExampleLanguage.ANGULAR]: `\

<!--component.ts-->
listItems = ${JSON.stringify(LIST_ITEMS)};
FormSelectFilterValidation = FormSelectFilterValidation;

<!--template.html-->
<digi-form-select-filter
  [attr.af-filter-button-text-label]="'Välj språk'"
  [attr.af-description]="'Beskrivande text'"
  [attr.af-name]="'Sök språk'"
  [attr.af-filter-button-text]="'Inget språk valt'"
  [attr.af-submit-button-text]="'Filtrera'"
  [attr.af-multiple-items]="${this.afMultipleItems}"
  [attr.af-validation]="FormSelectFilterValidation.${this.formSelectFilterValidation.toUpperCase()}"
  [afListItems]="listItems"
	${this.formSelectFilterValidation !== FormSelectFilterValidation.NEUTRAL
      ? '[attr.af-validation-text]="Det här är ett valideringsmeddelande"\n\t'
      : ''}
\>
</digi-form-select-filter>`,
      [CodeExampleLanguage.REACT]: `\
<DigiFormSelectFilter
	afFilterButtonTextLabel="Välj språk"
	afDescription="Beskrivande text"
	afFilterButtonText="Inget språk valt"
	afName="Sök språk"
	afSubmitButtonText="Filtrera"
	afMultipleItems={${this.afMultipleItems}}
	afValidation={FormSelectFilterValidation.${this.formSelectFilterValidation.toUpperCase()}}
	afListItems={${JSON.stringify(LIST_ITEMS)}}
	${this.formSelectFilterValidation !== FormSelectFilterValidation.NEUTRAL
      ? 'afValidationText="Det här är ett valideringsmeddelande"\n\t'
      : ''}
/>`
    };
  }

  render() {
    return (
      <Host>
        <div class="digi-form-select-filter-details">
          <div class="block-spacer">
            {!this.afShowOnlyExample && (
              <digi-typography-preamble>
                En väljare med möjlighet att söka i listan.
              </digi-typography-preamble>
            )}
          </div>
          <digi-layout-container af-no-gutter af-margin-bottom af-margin-top>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.formSelectFilterCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}
                class="digi-form-select-filter-details__code-example">
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset afLegend="Flera val ">
                    <digi-form-checkbox
                      afLabel="Ja"
                      afChecked={this.afMultipleItems}
                      onAfOnChange={() =>
                        this.afMultipleItems
                          ? (this.afMultipleItems = false)
                          : (this.afMultipleItems = true)
                      }></digi-form-checkbox>
                  </digi-form-fieldset>
                  <digi-form-select
                    afLabel="Validering"
                    onAfOnChange={(e) =>
                      (this.formSelectFilterValidation = (
                        e.target as HTMLDigiFormSelectElement
                      ).value as FormSelectFilterValidation)
                    }
                    af-variation="small">
                    <option value="neutral">Ingen</option>
                    <option value="error">Felaktig</option>
                    <option value="success">Godkänd</option>
                    <option value="warning">Varning</option>
                  </digi-form-select>
                </div>

                <div style={{ height: '500px' }}>
                  {this.afMultipleItems && (
                    <digi-form-select-filter
                      afMultipleItems={this.afMultipleItems}
                      afFilterButtonTextMultipleValues="Några val ({count})"
                      afFilterButtonTextLabel="Välj språk"
                      afFilterButtonText="Inget språk valt"
                      afSubmitButtonText="Filtrera"
                      afDescription="Beskrivande text"
                      afName="Sök språk"
                      afListItems={LIST_ITEMS}
                      afRequired={true}
                      afRequiredText="obligatoriskt"
                      afAnnounceIfOptional={true}
                      afAnnounceIfOptionalText="frivilligt"
                      afDisableValidation={false}
                      afValidation={this.formSelectFilterValidation}
                      af-validation-text="Det här är ett valideringsmeddelande"></digi-form-select-filter>
                  )}
                  {!this.afMultipleItems && (
                    <digi-form-select-filter
                      afMultipleItems={this.afMultipleItems}
                      afFilterButtonTextMultipleValues="Några val ({count})"
                      afFilterButtonTextLabel="Välj språk"
                      afFilterButtonText="Inget språk valt"
                      afSubmitButtonText="Filtrera"
                      afDescription="Beskrivande text"
                      afName="Sök språk"
                      afListItems={LIST_ITEMS}
                      afRequired={true}
                      afRequiredText="obligatoriskt"
                      afAnnounceIfOptional={true}
                      afAnnounceIfOptionalText="frivilligt"
                      afDisableValidation={false}
                      afValidation={this.formSelectFilterValidation}
                      af-validation-text="Det här är ett valideringsmeddelande"></digi-form-select-filter>
                  )}
                </div>
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <article>
                <h2>Användningsexempel</h2>
                <h3>Lyssna på Events</h3>
                <digi-code-block
                  af-language="html"
                  af-code={`<script>
  const selectFilter = document.querySelector('digi-form-select-filter')
  ${EVENTS.map((event) => `selectFilter.addEventListener('${event.name}', ${event.body})`).join('\n  ')}
</script>`}
                  af-variation="dark"></digi-code-block>

                <h2>Datastruktur</h2>
                <digi-code-block
                  af-language="typescript"
                  af-code={`interface IListItem {
  selected?: boolean //Är markerad som (för)vald
  label: string //Namn på texten som visas i dropdownen.
  value?: string // value sets type Button
  lang?: string // Vilket språk är ovan text (2-letter isokod)
  dir?: "LTR" | "RTL"
}
`}
                  af-variation="dark"></digi-code-block>
              </article>
            </digi-layout-container>
          )}
        </div>
      </Host>
    );
  }
}
