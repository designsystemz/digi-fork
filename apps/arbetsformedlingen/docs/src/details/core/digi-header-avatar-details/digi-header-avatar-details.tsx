import {
  CodeExampleLanguage,
  InfoCardHeadingLevel,
  InfoCardVariation
} from '@digi/arbetsformedlingen';
import { Component, h, Prop, getAssetPath } from '@stencil/core';

@Component({
  tag: 'digi-header-avatar-details',
  styleUrl: 'digi-header-avatar-details.scss'
})
export class DigiHeaderAvatar {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  get avatarHeaderCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-header-avatar
	af-src="/assets/images/avatar.svg"
	af-alt="Profilbild på Linda Karlsson"
	af-name="Linda Karlsson"
	af-signature="KALIA"
></digi-header-avatar>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-header-avatar
	[attr.af-src]="/assets/images/avatar.svg"
	[attr.af-alt]="'Profilbild på Linda Karlsson'"
	[attr.af-name]="'Linda Karlsson'"
	[attr.af-signature]="'KALIA'"
></digi-header-avatar>`,
      [CodeExampleLanguage.REACT]: `\
<digiHeaderAvatar
	afSrc="/assets/images/avatar.svg"
	afAlt="Profilbild på Linda Karlsson"
	afName="Linda Karlsson"
	afSignature="KALIA"
></digiHeaderAvatar>`
    };
  }

  render() {
    return (
      <div>
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används tillsammans med{' '}
              <a href="/komponenter/digi-header/oversikt">
                <digi-code af-code="<digi-header>"></digi-code>{' '}
              </a>
              för att header komponenten.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.avatarHeaderCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <digi-header-avatar
                af-src={getAssetPath('/assets/images/avatar.svg')}
                afAlt="Profilbild på Linda Karlsson"
                af-name="Linda Karlsson"
                af-signature="KALIA"
                af-is-logged-in="true"></digi-header-avatar>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <p>
                Komponenten måste användas tillsammans med{' '}
                <a href="/komponenter/digi-header/oversikt">
                  <digi-code af-code="<digi-header>"></digi-code>
                </a>
                .
              </p>
              <p>
                Attributet <digi-code af-code="af-alt" /> används för att
                beskriva kort om bilden. Attributet{' '}
                <digi-code af-code="af-name" /> används för att sätta namn.
                Attributet <digi-code af-code="af-signature" /> används för att
                sätta femsiffrig signatur.
              </p>
            </digi-layout-container>
          )}
          {!this.afShowOnlyExample && (
            <digi-layout-block af-container="none" af-margin-bottom>
              <digi-info-card
                afHeading="Bra att veta"
                afHeadingLevel={`h3` as InfoCardHeadingLevel}
                afVariation={InfoCardVariation.PRIMARY}>
                <p>
                  Komponenten kan enbart användas inuti komponenten{' '}
                  <a href="/komponenter/digi-header-navigation/oversikt">
                    <digi-code af-code="<digi-header-navigation>"></digi-code>
                  </a>
                  .
                </p>
              </digi-info-card>
              <br />
            </digi-layout-block>
          )}
        </digi-typography>
      </div>
    );
  }
}
