import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  FormInputType,
  FormInputValidation,
  FormInputMode,
  FormInputVariation,
  InfoCardVariation,
  InfoCardHeadingLevel
} from '@digi/arbetsformedlingen';
import { href } from 'stencil-router-v2';

@Component({
  tag: 'digi-form-input-details',
  styleUrl: 'digi-form-input-details.scss'
})
export class DigiFormInputDetails {
  @State() isSubmitted = false;
  @State() hasSubmitted = false;
  @State() formState = {
    firstName: {
      valid: false,
      errorText: 'Ange ditt förnamn'
    },
    lastName: {
      valid: false,
      errorText: 'Ange ditt efternamn'
    }
  };
  @State() errorList = [];

  @State() formInputType: FormInputType = FormInputType.TEXT;
  @State() formInputMode: FormInputMode = FormInputMode.DEFAULT;
  @State() formInputVariation: FormInputVariation = FormInputVariation.MEDIUM;
  @State() formInputValidation: FormInputValidation =
    FormInputValidation.NEUTRAL;
  @State() hasDescription = false;

  @State() hasInputModeNumeric = false;

  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  submitHandler(e) {
    e.detail.preventDefault();
    this.validateForm();
    this.hasSubmitted = true;
  }

  validateForm() {
    const formElements = [...Object.keys(this.formState)];
    const tempState = this.formState;
    const errorList = [];

    formElements.forEach((element) => {
      const el = document.getElementById(element) as HTMLFormElement;
      tempState[element].valid = el.value === '' ? false : true;
    });

    Object.entries(tempState).forEach(([key, value]) => {
      !!value.valid || errorList.push({ id: key, message: value.errorText });
    });

    this.formState = { ...tempState };
    this.errorList = [...errorList];

    if (this.errorList.length === 0) {
      this.isSubmitted = true;
    }
  }
  get inputCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-form-input
	af-label="Etikett"
	af-variation="${this.formInputVariation}"
	af-type="${this.formInputType}" ${
    this.hasDescription ? '\n\taf-label-description="Beskrivande text"\t ' : ''
  }
	af-validation="${this.formInputValidation}"\
	${this.formInputMode ? `\n\taf-inputmode="${this.formInputMode}"\t ` : ''}\
	${
    this.formInputValidation !== FormInputValidation.NEUTRAL
      ? '\n\taf-validation-text="Det här är ett valideringsmeddelande"\n>'
      : '\n>'
  }
</digi-form-input>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-form-input
	[attr.af-label]="'Etikett'"
	[attr.af-variation]="FormInputVariation.${Object.keys(FormInputVariation).find(
    (key) => FormInputVariation[key] === this.formInputVariation
  )}"
	[attr.af-type]="FormInputType.${Object.keys(FormInputType).find(
    (key) => FormInputType[key] === this.formInputType
  )}"
	[attr.af-validation]="FormInputValidation.${Object.keys(
    FormInputValidation
  ).find((key) => FormInputValidation[key] === this.formInputValidation)}"\
	${
    this.hasDescription
      ? `\n\t[attr.af-label-description]="'Beskrivande text'"\t `
      : ''
  }\
	${
    this.formInputMode
      ? `\n\t[attr.af-inputmode]="FormInputMode.${Object.keys(
          FormInputMode
        ).find((key) => FormInputMode[key] === this.formInputMode)}"\t `
      : ''
  }\
	${
    this.formInputValidation !== FormInputValidation.NEUTRAL
      ? `\n\t[attr.af-validation-text]="'Det här är ett valideringsmeddelande'"\n>`
      : '\n>'
  }
</digi-form-input>`,
      [CodeExampleLanguage.REACT]: `\
<DigiFormInput
	afLabel="Etikett"
	afVariation={FormInputVariation.${Object.keys(FormInputVariation).find(
    (key) => FormInputVariation[key] === this.formInputVariation
  )}}
	afType={FormInputType.${Object.keys(FormInputType).find(
    (key) => FormInputType[key] === this.formInputType
  )}}
	afValidation={FormInputValidation.${Object.keys(FormInputValidation).find(
    (key) => FormInputValidation[key] === this.formInputValidation
  )}}\
	${this.hasDescription ? `\n\tafLabelDescription="Beskrivande text"\t ` : ''}\
	${
    this.formInputMode
      ? `\n\tafInputmode="{FormInputMode.${Object.keys(FormInputMode).find(
          (key) => FormInputMode[key] === this.formInputMode
        )}}"\t `
      : ''
  }\
	${
    this.formInputValidation !== FormInputValidation.NEUTRAL
      ? `\n\tafValidationText="Det här är ett valideringsmeddelande"\n>`
      : '\n>'
  }
</DigiFormInput>`
    };
  }
  render() {
    return (
      <div class="digi-form-input-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Inmatningsfält används för att användaren ska kunna fylla i
              information när det inte finns färdiga alternativ att välja
              mellan.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.inputCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div style={{ width: '400px' }}>
                {this.hasDescription && (
                  <digi-form-input
                    afLabel="Etikett"
                    afLabelDescription="Beskrivande text"
                    af-validation-text="Det här är ett valideringsmeddelande"
                    afVariation={this.formInputVariation}
                    af-type={this.formInputType}
                    afValidation={this.formInputValidation}
                    afInputmode={this.formInputMode}></digi-form-input>
                )}
                {!this.hasDescription && (
                  <digi-form-input
                    afLabel="Etikett"
                    af-validation-text="Det här är ett valideringsmeddelande"
                    afVariation={this.formInputVariation}
                    af-type={this.formInputType}
                    afValidation={this.formInputValidation}
                    afInputmode={this.formInputMode}></digi-form-input>
                )}
              </div>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Storlek"
                  afLegend="Storlek"
                  onChange={(e) =>
                    (this.formInputVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Liten"
                    afValue={FormInputVariation.SMALL}
                    afChecked={
                      this.formInputVariation === FormInputVariation.SMALL
                    }
                  />
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Mellan"
                    afValue={FormInputVariation.MEDIUM}
                    afChecked={
                      this.formInputVariation === FormInputVariation.MEDIUM
                    }
                  />
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Stor"
                    afValue={FormInputVariation.LARGE}
                    afChecked={
                      this.formInputVariation === FormInputVariation.LARGE
                    }
                  />
                </digi-form-fieldset>

                <digi-form-select
                  afLabel="Validering"
                  onAfOnChange={(e) =>
                    (this.formInputValidation = (
                      e.target as HTMLDigiFormSelectElement
                    ).value as FormInputValidation)
                  }
                  af-variation="small">
                  <option value="neutral">Ingen</option>
                  <option value="error">Felaktig</option>
                  <option value="success">Godkänd</option>
                  <option value="warning">Varning</option>
                </digi-form-select>
                <digi-form-select
                  afLabel="Typ"
                  onAfOnChange={(e) =>
                    (this.formInputType = (
                      e.target as HTMLDigiFormSelectElement
                    ).value as FormInputType)
                  }
                  af-variation="small"
                  af-start-selected="10">
                  <option value="color">Färg</option>
                  <option value="email">Epostadress</option>
                  <option value="hidden">Gömd</option>
                  <option value="month">Månad</option>
                  <option value="number">Nummer</option>
                  <option value="password">Lösenord</option>
                  <option value="tel">Telefonnummer</option>
                  <option value="text">Text</option>
                  <option value="time">Tid</option>
                  <option value="url">URL</option>
                  <option value="week">Vecka</option>
                </digi-form-select>

                <digi-form-select
                  afLabel="Inmatningsläge"
                  onAfOnChange={(e) =>
                    (this.formInputMode = (
                      e.target as HTMLDigiFormSelectElement
                    ).value as FormInputMode)
                  }
                  af-variation="small"
                  af-start-selected="10">
                  <option value="">Standardläge</option>
                  <option value="none">Inget</option>
                  <option value="text">Text</option>
                  <option value="decimal">Decimaler</option>
                  <option value="numeric">Numerisk</option>
                  <option value="tel">Telefon</option>
                  <option value="search">Sök</option>
                  <option value="email">E-post</option>
                  <option value="url">Url</option>
                </digi-form-select>

                <digi-form-fieldset afLegend="Beskrivande text">
                  <digi-form-checkbox
                    afLabel="Ja"
                    afChecked={this.hasDescription}
                    onAfOnChange={() =>
                      this.hasDescription
                        ? (this.hasDescription = false)
                        : (this.hasDescription = true)
                    }></digi-form-checkbox>
                </digi-form-fieldset>
              </div>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <p>
                Inmatningsfältet finns i tre olika varianter: liten, mellan
                (standard) och stor.
              </p>

              <h3>Validering</h3>
              <p>
                Inmatningsfältet kan ha fyra olika valideringsstatus: Godkänd,
                felaktig, varning och neutral (som är den förvalda
                valideringstypen). Valideringsmeddelanden kan visas vid alla
                statusar, men kommer endast att synas om användaren har
                interagerat med fältet.
              </p>
              <digi-info-card
                afHeading="Bra att tänka på"
                afHeadingLevel={`h3` as InfoCardHeadingLevel}
                afVariation={InfoCardVariation.SECONDARY}>
                <digi-list>
                  <li>
                    Se{' '}
                    <a {...href('/designmonster/validering')}>designmönster</a>{' '}
                    för hur olika fält är tänkta att validera
                  </li>
                  <li>
                    Tänk på att formulera felmeddelandet kort och koncist.
                    Användaren ska förstå vad som gick fel och vad som behöver
                    göras för att rätta till felet. Var konsekvent i
                    formuleringarna.
                  </li>
                  <li>Rensa inte informationen i fältet vid validering.</li>
                  <li>
                    Inmatningsfält kan också validera bekräftande. Används
                    främst för då datat i fältet hämtar information i ett annat
                    system eller liknande.
                  </li>
                </digi-list>
              </digi-info-card>

              <h3>Typer</h3>
              <p>
                Inmatningsfältet stödjer de flesta fälttyperna och mappar direkt
                till input-elementets <digi-code af-code="type"></digi-code>
                -attribut.
              </p>
              <p>
                Vill du lära dig mer om dom olika typerna kan du göra det på
                <digi-link-external
                  af-target="_blank"
                  afHref="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input">
                  MDN Web Docs (developer.mozilla.org, öppnas i egen flik)
                </digi-link-external>
              </p>
              <h3>Inmatningsläge</h3>
              <p>
                Inmatningsfältet stödjer de flesta inmatningslägena och mappar
                direkt till input-elementets{' '}
                <digi-code af-code="inputmode"></digi-code>-attribut.
              </p>
              <p>
                Inmatningsläge är till för att ändra funktionaliteten för
                enheter med virtuella tangentbord. Detta kan bland annat
                användas till att skapa textfält med inmatning för siffror på
                mobila enheter.
              </p>
              <h3>Etiketter</h3>
              <p>
                Om inmatningsfältet ska vara obligatoriska eller friviliga och
                om man vill att ha en annan text än standard texten så kan man
                specifiera det med{' '}
                <digi-code af-code="af-required-text"></digi-code> och{' '}
                <digi-code af-code="af-announce-if-optional-text"></digi-code>.
                Om dessa specifieras som en tom sträng (till exempel{' '}
                <digi-code af-code='af-required-text=""'></digi-code>) så kommer
                ingen text läggas till i etiketen. Läs mer om obligatoriska fält
                i{' '}
                <a {...href('/designmonster/formular')}>
                  designmönstret för formulär
                </a>
              </p>
              <digi-info-card
                afHeading="Riktlinjer"
                afHeadingLevel={`h3` as InfoCardHeadingLevel}
                afVariation={InfoCardVariation.PRIMARY}>
                <digi-list>
                  <li>
                    Skriv korta och tydliga etiketter. Det ska vara enkelt för
                    användaren att förstå vilken information som fältet ska
                    fyllas med.
                  </li>
                  <li>
                    Är det möjligt ska vi tillåta inmatning i olika format.
                  </li>
                  <li>
                    Visa vilket format som ska användas i de fall det krävs,
                    användaren ska inte behöva gissa vilket format som gäller.
                  </li>
                  <li>
                    Anpassa storleken på inmatningsfältet för att passa för ett
                    normalt svar i sammanhanget. Men ta samtidigt hänsyn till
                    helheten, så att inte formuläret upplevs som rörigt.
                  </li>
                  <li>
                    Fundera på ordningsföljden och grupperingen av
                    inmatningsfält, så att den följer största möjliga logik för
                    användaren.
                  </li>
                  <li>
                    Är det möjligt att förpopulera inmatningsfält med
                    information ska vi göra det.
                  </li>
                  <li>
                    För inmatning av datum använd komponenten{' '}
                    <digi-code af-code="digi-calendar-datepicker"></digi-code>
                  </li>
                  <li>
                    För inmatning av sökningar använd komponenten{' '}
                    <digi-code af-code="digi-form-input-search"></digi-code>
                  </li>
                </digi-list>
              </digi-info-card>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
