/* eslint-disable no-useless-escape */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, h, Host, Prop } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-chart-line-details',
  styleUrl: 'digi-chart-line-details.scss'
})
export class DigiChartLine {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  _chartData: any = {
    data: {
      xValueNames: [
        '2022-09-13',
        '2022-09-14',
        '2022-09-15',
        '2022-09-16',
        '2022-09-17',
        '2022-09-18',
        '2022-09-19',
        '2022-09-20',
        '2022-09-21',
        '2022-09-22',
        '2022-09-23',
        '2022-09-24',
        '2022-09-25',
        '2022-09-26',
        '2022-09-27',
        '2022-09-28',
        '2022-09-29',
        '2022-09-30',
        '2022-10-01',
        '2022-10-02',
        '2022-10-03',
        '2022-10-04',
        '2022-10-05',
        '2022-10-06',
        '2022-10-07',
        '2022-10-08',
        '2022-10-09',
        '2022-10-10',
        '2022-10-11',
        '2022-10-12',
        '2022-10-13',
        '2022-10-14',
        '2022-10-15',
        '2022-10-16',
        '2022-10-17',
        '2022-10-18',
        '2022-10-19',
        '2022-10-20',
        '2022-10-21',
        '2022-10-22',
        '2022-10-23',
        '2022-10-24',
        '2022-10-25',
        '2022-10-26',
        '2022-10-27',
        '2022-10-28',
        '2022-10-29',
        '2022-10-30',
        '2022-10-31',
        '2022-11-01',
        '2022-11-02',
        '2022-11-03',
        '2022-11-04',
        '2022-11-05',
        '2022-11-06',
        '2022-11-07',
        '2022-11-08',
        '2022-11-09',
        '2022-11-10',
        '2022-11-11',
        '2022-11-12',
        '2022-11-13',
        '2022-11-14',
        '2022-11-15',
        '2022-11-16',
        '2022-11-17',
        '2022-11-18',
        '2022-11-19',
        '2022-11-20',
        '2022-11-21',
        '2022-11-22',
        '2022-11-23',
        '2022-11-24',
        '2022-11-25',
        '2022-11-26',
        '2022-11-27',
        '2022-11-28',
        '2022-11-29',
        '2022-11-30',
        '2022-12-01',
        '2022-12-02',
        '2022-12-03',
        '2022-12-04',
        '2022-12-05',
        '2022-12-06',
        '2022-12-07',
        '2022-12-08',
        '2022-12-09',
        '2022-12-10',
        '2022-12-11',
        '2022-12-12',
        '2022-12-13',
        '2022-12-14',
        '2022-12-15',
        '2022-12-16',
        '2022-12-17',
        '2022-12-18',
        '2022-12-19',
        '2022-12-20',
        '2022-12-21',
        '2022-12-22',
        '2022-12-23',
        '2022-12-24',
        '2022-12-25',
        '2022-12-26',
        '2022-12-27',
        '2022-12-28',
        '2022-12-29',
        '2022-12-30',
        '2022-12-31',
        '2023-01-01',
        '2023-01-02',
        '2023-01-03',
        '2023-01-04',
        '2023-01-05',
        '2023-01-06',
        '2023-01-07',
        '2023-01-08',
        '2023-01-09',
        '2023-01-10'
      ],
      xValues: [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
        39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
        57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74,
        75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92,
        93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108,
        109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120
      ],
      series: [
        {
          yValues: [
            1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0,
            0, 2, 1, 2, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 6, 0,
            0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 2,
            0, 0, 0, 2, 2
          ],
          title: 'Commits'
        }
      ]
    },
    title: 'Utvecklingshistorik för detta linjediagram',
    subTitle:
      'Diagrammet visar historiken av commits, additions och tillhörande datum över tiden som detta linjediagram kodades i designsystemet',
    x: 'Datum',
    y: 'Commits'
  };

  get chartData() {
    return JSON.stringify(this._chartData);
  }

  get tagCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
 <digi-chart-line    
 af-chart-data={this.chartData}
 af-heading-level="h2"
>
 </digi-chart-line>`,
      [CodeExampleLanguage.ANGULAR]: `\ 
<digi-chart-line 
[attr.af-chart-data]="chartData" 
[attr.af-heading-level]="'h2'"
>
 </digi-chart-line>
 </div>`,
      [CodeExampleLanguage.REACT]: `\
 <digi-chart-line 
 afChartData={chartData}
 afHeadingLevel={afHeadingLevel.H2}
 >
 </digi-chart-line>
`
    };
  }

  render() {
    return (
      <Host>
        <div class="digi-chart-line-details">
          <digi-typography>
            {!this.afShowOnlyExample && (
              <digi-layout-container afNoGutter afMarginTop>
                <digi-typography-preamble>
                  Linjediagram är ett typ av diagram som lämpar sig bäst för att
                  visa hur en eller flera variabler förändras över tid.
                </digi-typography-preamble>
              </digi-layout-container>
            )}
            <digi-layout-container af-no-gutter af-margin-bottom>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.tagCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset></digi-form-fieldset>
                </div>
                <div style={{ width: '600px', height: '400px' }}>
                  <digi-chart-line
                    af-chart-data={this.chartData}
                    af-heading-level="h2"></digi-chart-line>
                </div>
              </digi-code-example>
            </digi-layout-container>
            <digi-layout-container af-no-gutter af-margin-bottom>
              {!this.afShowOnlyExample && (
                <article>
                  <h2>Beskrivning</h2>
                  <h3>Storlek</h3>
                  <p>
                    Linjediagrammets storlek anpassar sig efter dess
                    förälderelement. Förälderelementen måste ha en storlek,
                    annars så får diagrammet storlek 0. Exempelvis
                    <digi-code afCode="style={{ 'width': '400px', 'height': '200px' }} "></digi-code>
                    <br />
                    <br />
                    <span class="italic">
                      Känd bugg: kan vara problematiskt för linjediagrammet att
                      korrekt uppdatera storlek vid resize om man sätter width
                      till 100%, testa med 95%, eller lite lägre om det är
                      önskvärt med en procentuell storlek
                    </span>
                  </p>
                  <h3>Att skicka in data</h3>
                  <p>
                    För att skicka in data till linjediagrammet så finns det en
                    objektstruktur man måste följa, den ser ut som sådan:
                    <br />
                    <digi-code-block
                      af-language="typescript"
                      af-code="ChartLineData {
	data: { 
		xValues: number[]; 
		series: ChartLineSeries[]; 
		xValueNames?: string[]; 
	}; 
	x:string; 
	y:string; 
	title:string; 
	subTitle?:string; 
	meta?: { 
		numberOfReferenceLines?: number; 
		percentage?:boolean; 
	}; 
}"
                      af-variation="dark"></digi-code-block>
                    <br />
                    <digi-code afCode="data"></digi-code> Innehåller{' '}
                    <digi-code afCode="xValues"></digi-code> som är en array med
                    numeriska värden, från 1 till N antal datapunkter som finns
                    i datan. Om man har ett linjediagram över ett år, med en
                    datapunkt per månad, så skulle alltså{' '}
                    <digi-code afCode="xValues"></digi-code> vara en lista med{' '}
                    <br />{' '}
                    <digi-code afCode="[1,2,3,4,5,6,7,8,9,10,11,12]"></digi-code>
                    , <br /> men för att visa månadsnamn istället för månadens
                    nummer, så kan man använda sig av den valfria nyckeln{' '}
                    <digi-code afCode="xValueNames"></digi-code>, som är en
                    lista på strängar av matchande längd med{' '}
                    <digi-code afCode="xValues"></digi-code>. Som i detta fall
                    skulle kunna vara: <br />{' '}
                    <digi-code afCode="['jan','feb','mar','apr','maj','jun','jul','aug','sep','okt','nov','dec']"></digi-code>
                    <br />
                    <br />
                    <digi-code afCode="series"></digi-code> Innehåller själva
                    datapunkerna och har en liten egen struktur som ser ut
                    såhär:
                    <digi-code-block
                      af-language="typescript"
                      af-code="ChartLineSeries {
 yValues: number[];
 title: string;
 colorToken?: string;
}"
                      af-variation="dark"></digi-code-block>
                    En serie motsvarar en linje, därför har varje serie en{' '}
                    <digi-code afCode="title"></digi-code>-nyckel för att namnge
                    serien, samt en <digi-code afCode="colorToken"></digi-code>
                    -nyckel, men det är rekommenderat att använda de färgerna
                    som sätts per automatik i diagrammet, för att säkerställa
                    tillräckligt hög kontrast.
                    <br />
                    <br />
                    <digi-code afCode="yValues"></digi-code> är en lista på alla
                    numeriska värden per datapunkt,
                    <digi-code afCode="yValues"></digi-code> måste inte vara
                    samma längd som <digi-code afCode="xValues"></digi-code>,
                    men om man har en serie som sträcker sig över 5 värden, men
                    en serie på endast tre punkter, så kommer linjen att ta slut
                    efter tre punkter. Är det så att linjen börjar efter två
                    punkter, så bör <digi-code afCode="yValues"></digi-code>{' '}
                    vara 5 lång, men de två första värdena kan vara null:{' '}
                    <digi-code afCode="[null, null, 1, 2, 3]"></digi-code>
                    <br />
                    <br />
                    <digi-code afCode="x"></digi-code> är namnet på x-axelns
                    värden, bör vara <digi-code afCode="x: 'Månad'"></digi-code>{' '}
                    enligt exemplet ovan.
                    <br />
                    <br />
                    <digi-code afCode="y"></digi-code> är namnet på y-axelns
                    värden, alltså det som mäts vid varje x-värde.
                    <br />
                    <br />
                    <digi-code afCode="title"></digi-code> är namnet diagrammet.
                    <br />
                    <br />
                    <digi-code afCode="subTitle"></digi-code> är en eventuell
                    underrubrik till diagrammet.
                    <br />
                    <br />
                    <digi-code afCode="meta"></digi-code> har just nu enbart två
                    parametrar, <digi-code afCode="percentage"></digi-code> om
                    man vill att diagrammet ska visa procent.
                    <digi-code afCode="numberOfReferenceLines"></digi-code> om
                    man vill öka, eller minska antalet referenslinjer, default
                    är <digi-code afCode="5"></digi-code>
                    <br />
                    <br />
                    Utöver detta, ska komponent ha rubiknivå. Du kan välja
                    rubriknivå för rubriken genom att använda{' '}
                    <digi-code afCode="digi-heading-level"></digi-code>.<br />
                  </p>
                  <h3>Legend </h3>
                  <p>
                    Legenden sätts med hjälp av alla titel-nycklar som är satta
                    i respektive serie. Varje knapp/chip i legenden är klickbar
                    för att kunna se på enbart ett urval av linjerna.
                  </p>

                  <h3>Tillgänglighet</h3>
                  <p>
                    Diagrammet användar aria-hidden för att dölja själva
                    diagram-ytan.
                    <br /> Digrammet erbjuder även möjligheten till användaren
                    att visa informationen i tabellform. Detta uppnås genom att
                    man trycker på visa som tabell, knappen är går att navigera
                    till med tangentbord.
                    <br /> De 8 färgerna som är framtagna för diagrammet
                    erbjuder ett högre kontrastförhållande än 4.5 mot vit
                    bakgrund, vill man justera färgerna, så bör man se över
                    kontrastförhållandet.
                    <br /> Linjerna i diagrammet använder inte bara färg för att
                    skilja linjerna åt, utan det kompletteras med olika
                    linjetyper och 'glyphs'.
                  </p>

                  <h3>Att tänka på</h3>
                  <p>
                    Linjediagramm kan för ovana användare vara lite
                    avskräckande, de kräver uppmärksamhet och bör därför vara
                    sparsamt använda på en sida. Undvik att ha flera diagram i
                    närheten av varandra, en bra tumregel kan vara ett diagram
                    per vy.
                  </p>
                  <digi-layout-block
                    af-variation="symbol"
                    af-vertical-padding
                    af-margin-bottom>
                    <h2>Riktlinjer</h2>
                    <digi-list>
                      <li>
                        Att använda linjediagram lämpar sig bra när man vill se
                        hur data förändras över tid, extra bra om man har två
                        linjer som man vill jämföra över tid. Men var försiktig,
                        använd inte för många linjer samtidigt, då blir det
                        väldigt svårt att kunna utföra någon analys.
                      </li>
                      <li>
                        Linjediagram tar väldigt mycket uppmärksamhet, och bör
                        omfamnas av tillräckligt med White space. Ett
                        linjediagram tar med fördel upp en större del av sidans
                        bredd.
                      </li>
                      <li>
                        Har man andra diagram eller statistiska element på sidan
                        bör de ligga på lämpligt avstånd från diagrammet, såvida
                        de inte är tätt kopplade med datan i diagrammet.
                      </li>
                      <li>
                        Vill man ha flera linjediagram på en och samma sida bör
                        de ligga på separata rader med lämpligt avstånd. Tänk
                        att om linjediagrammet inte speglar olika nyanser av
                        samma sak, så är det ingen poäng att de kan visas
                        samtidigt i samma vy.
                      </li>
                      <li>Undvik att använda fler än 3 linjer samtidigt.</li>
                    </digi-list>
                  </digi-layout-block>
                </article>
              )}
            </digi-layout-container>
          </digi-typography>
        </div>
      </Host>
    );
  }
}
