import { Component, Prop, h } from '@stencil/core';
import {
  CodeExampleLanguage,
  LayoutBlockVariation
} from '@digi/arbetsformedlingen';

const tabs = [
  { id: 'om-yrket', title: 'Om Yrket' },
  { id: 'arbetsuppgifter', title: 'Arbetsuppgifter' },
  { id: 'formagor', title: 'Förmågor' },
  { id: 'lon', title: 'Lön' },
  { id: 'formaner', title: 'Förmåner' },
  { id: 'ovrigt', title: 'Övrigt' }
];

@Component({
  tag: 'digi-tablist-details',
  styleUrl: 'digi-navigation-tablist-details.scss'
})
export class DigiTablistDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  get exampleCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-tablist 
	af-tabs='${JSON.stringify(tabs, null, 1)}'
	af-aria-label="Detaljer om Yrke"
>
${tabs
  .map(
    (tab) => `
	<digi-tablist-panel tab="${tab.id}">
		<p>Innehåll som visas när flik "${tab.title}" är aktiv</p>
	</digi-tablist-panel>
`
  )
  .join('\n')}
</digi-tablist>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-tablist 
	[attr.af-tabs]='${JSON.stringify(tabs)}'
	[attr.af-aria-label]="'Detaljer om Yrke'"
>
${tabs
  .map(
    (tab) => `
	<digi-tablist-panel [attr.tab]="'${tab.id}'">
		<p>Innehåll som visas när flik "${tab.title}" är aktiv</p>
	</digi-tablist-panel>
`
  )
  .join('\n')}
</digi-tablist>`,
      [CodeExampleLanguage.REACT]: `\
<DigiTablist
	afTabs={${JSON.stringify(tabs)}}
	afAriaLabel="Detaljer om Yrke"
>
	${tabs
    .map(
      (tab) => `
	<DigiTablistPanel tab="${tab.id}">
		<p>Innehåll som visas när flik "${tab.title}" är aktiv</p>
	</DigiTablistPanel>
`
    )
    .join('')}
</DigiTablist>`
    };
  }

  render() {
    return (
      <div>
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Tablist tar in en lista av innehåll och visar det som flikar.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.exampleCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <div class="slot__controls" slot="controls"></div>
                <digi-tablist afAriaLabel="Detaljer om Yrke" afTabs={tabs}>
                  {tabs.map((tab) => (
                    <digi-tablist-panel tab={tab.id}>
                      <p>
                        Innehåll som visas när flik <b>{tab.title}</b> är aktiv
                      </p>
                    </digi-tablist-panel>
                  ))}
                </digi-tablist>
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <p>
                Komponenten flikar används när du vill
                gruppera innehåll i olika sektioner utan att behöva ladda om
                hela sidan. Flikar kompletterar huvudnavigeringen och används
                för att förbättra användarupplevelsen, göra den lättare
                överblickbar eller dela upp relaterad information.
              </p>
              <p>
                Den första fliken är alltid förvald och visar det aktuella
                innehållet. Den är markerad som aktiv genom fet stil och en grön
                markör. Vid hover över de andra flikarna så markeras detta med
                att rubriken blir “länkblå” med understrykning.
              </p>
              <p>
                Flikarna kan placeras varsomhelst på en sida. Men tänk på att ge
                den en tillräcklig frizon i förhållande till andra grafiska
                element. Är den återkommande på undersidor så placera flikfältet
                på samma ställe. Komponenten är responsiv och anpassar sig till
                sid- eller kolumnbredd.
              </p>
              <h3>Visuellt beteende</h3>
              <p>
                När innehållet i flikarna överstiger bredden på
                webbläsarfönstret kompletteras komponenten med pilar för att ta
                dig vidare till höger och tillbaka till vänster. Detta är
                vanligt i mobiler då sidbredden är begränsad.
              </p>

              <article>
                <h4>Standardutförande</h4>
                <p>
                  Nedan visas exempel på hur flikkomponenten beter sig på en
                  sida med fullbredd.
                </p>
                <digi-layout-block
                  af-variation="secondary"
                  af-vertical-padding
                  af-margin-bottom>
                  <digi-tablist
                    af-tabs='[
 {
  "id": "om-yrket",
  "title": "Om Yrket"
 },
 {
  "id": "arbetsuppgifter",
  "title": "Arbetsuppgifter"
 },
 {
  "id": "formagor",
  "title": "Förmågor"
 },
 {
  "id": "lon",
  "title": "Lön"
 },
 {
  "id": "formaner",
  "title": "Förmåner"
 },
 {
  "id": "ovrigt",
  "title": "Övrigt"
 }
]'
                    af-aria-label="Detaljer om Yrke">
                    <digi-tablist-panel tab="om-yrket">
                      <div class="digi-docs-about-design-pattern-tabs_example-tab">
                        <p>Innehåll som visas när flik "Om Yrket" är aktiv</p>
                      </div>
                    </digi-tablist-panel>

                    <digi-tablist-panel tab="arbetsuppgifter">
                      <div class="digi-docs-about-design-pattern-tabs_example-tab">
                        <p>
                          Innehåll som visas när flik "Arbetsuppgifter" är aktiv
                        </p>
                      </div>
                    </digi-tablist-panel>

                    <digi-tablist-panel tab="formagor">
                      <div class="digi-docs-about-design-pattern-tabs_example-tab">
                        <p>Innehåll som visas när flik "Förmågor" är aktiv</p>
                      </div>
                    </digi-tablist-panel>

                    <digi-tablist-panel tab="lon">
                      <div class="digi-docs-about-design-pattern-tabs_example-tab">
                        <p>Innehåll som visas när flik "Lön" är aktiv</p>
                      </div>
                    </digi-tablist-panel>

                    <digi-tablist-panel tab="formaner">
                      <div class="digi-docs-about-design-pattern-tabs_example-tab">
                        <p>Innehåll som visas när flik "Förmåner" är aktiv</p>
                      </div>
                    </digi-tablist-panel>

                    <digi-tablist-panel tab="ovrigt">
                      <div class="digi-docs-about-design-pattern-tabs_example-tab">
                        <p>Innehåll som visas när flik "Övrigt" är aktiv</p>
                      </div>
                    </digi-tablist-panel>
                  </digi-tablist>
                </digi-layout-block>
                <br />
                <h4>Mobilbredd eller i kolumn</h4>
                <p>
                  Visar flikkomponenten när den är placerad på en yta som är
                  begränsad till bredden
                </p>
                <digi-layout-block
                  af-variation="secondary"
                  af-vertical-padding
                  af-margin-bottom>
                  <div class="digi-docs-about-design-pattern-tabs_example-forcemobile">
                    <digi-tablist
                      af-tabs='[
 {
  "id": "om-yrket",
  "title": "Om Yrket"
 },
 {
  "id": "arbetsuppgifter",
  "title": "Arbetsuppgifter"
 },
 {
  "id": "formagor",
  "title": "Förmågor"
 },
 {
  "id": "lon",
  "title": "Lön"
 },
 {
  "id": "formaner",
  "title": "Förmåner"
 },
 {
  "id": "ovrigt",
  "title": "Övrigt"
 }
]'
                      af-aria-label="Detaljer om Yrke">
                      <digi-tablist-panel tab="om-yrket">
                        <div class="digi-docs-about-design-pattern-tabs_example-tab">
                          <p>Innehåll som visas när flik "Om Yrket" är aktiv</p>
                        </div>
                      </digi-tablist-panel>

                      <digi-tablist-panel tab="arbetsuppgifter">
                        <div class="digi-docs-about-design-pattern-tabs_example-tab">
                          <p>
                            Innehåll som visas när flik "Arbetsuppgifter" är
                            aktiv
                          </p>
                        </div>
                      </digi-tablist-panel>

                      <digi-tablist-panel tab="formagor">
                        <div class="digi-docs-about-design-pattern-tabs_example-tab">
                          <p>Innehåll som visas när flik "Förmågor" är aktiv</p>
                        </div>
                      </digi-tablist-panel>

                      <digi-tablist-panel tab="lon">
                        <div class="digi-docs-about-design-pattern-tabs_example-tab">
                          <p>Innehåll som visas när flik "Lön" är aktiv</p>
                        </div>
                      </digi-tablist-panel>

                      <digi-tablist-panel tab="formaner">
                        <div class="digi-docs-about-design-pattern-tabs_example-tab">
                          <p>Innehåll som visas när flik "Förmåner" är aktiv</p>
                        </div>
                      </digi-tablist-panel>

                      <digi-tablist-panel tab="ovrigt">
                        <div class="digi-docs-about-design-pattern-tabs_example-tab">
                          <p>Innehåll som visas när flik "Övrigt" är aktiv</p>
                        </div>
                      </digi-tablist-panel>
                    </digi-tablist>
                  </div>
                </digi-layout-block>
              </article>
              <digi-layout-block
                af-variation={LayoutBlockVariation.SYMBOL}
                af-vertical-padding
                af-margin-bottom>
                <h2>Riktlinjer</h2>
                <h3>Grundläggande principer</h3>
                <digi-list>
                  <li>
                    Presentera flikarna i en logisk och naturlig ordning för
                    användaren.
                  </li>
                  <li>
                    Använd flikar för att organisera relaterat innehåll, inte
                    för att komprimera information.
                  </li>
                  <li>
                    Placera viktig information synligt, inte gömd under flikar.
                  </li>
                </digi-list>
                <h3>Användningsområden</h3>
                <digi-list>
                  <li>
                    Flikar är primärt för innehållsorganisering inom en sida,
                    inte för huvudnavigation.
                  </li>
                  <li>
                    Använd flikar sparsamt och överväg alltid andra lösningar
                    först.
                  </li>
                </digi-list>
                <h3>Design och struktur</h3>
                <digi-list>
                  <li>
                    Begränsa antalet flikar för att undvika överväldigande
                    navigation.
                  </li>
                  <li>
                    Använd korta, tydliga rubriker som beskriver flikens
                    innehåll.
                  </li>
                  <li>Se till att den aktiva fliken är tydligt markerad.</li>
                  <li>
                    Gör inaktiva flikar klickbara och synliga för att visa
                    tillgängliga alternativ.
                  </li>
                </digi-list>
                <h3>Tillgänglighet och navigation</h3>
                <digi-list>
                  <li>
                    Implementera ARIA-attribut för att stödja
                    tangentbordsnavigation.
                  </li>
                  <li>
                    Säkerställ att flikordningen är konsekvent med sidans övriga
                    struktur (huvudmeny, rubriker).
                  </li>
                </digi-list>
                <h3>Att undvika</h3>
                <digi-list>
                  <li>
                    Använd inte flikar för att ersätta rubriker eller
                    expanderbara ytor på innehållsrika sidor.
                  </li>
                  <li>Undvik att dölja väsentlig information under flikar.</li>
                </digi-list>
              </digi-layout-block>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
