import { Component, h } from '@stencil/core';

@Component({
  tag: 'digi-docs-related-links',
  styleUrl: 'digi-docs-related-links.scss',
  scoped: true
})
export class RelatedLinks {
  render() {
    return (
      <host>
        <div class="digi-docs-related-links">
          <slot></slot>
        </div>
      </host>
    );
  }
}
