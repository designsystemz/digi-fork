import { Component, Element, h, Listen, State } from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { Category } from '../../taxonomies';
import {
  UtilBreakpointObserverBreakpoints,
  NavigationSidebarVariation,
  NavigationSidebarPosition,
  NavigationVerticalMenuActiveIndicatorSize
} from '@digi/arbetsformedlingen';

import state from '../../store/store';

import { router } from '../../global/router';
import { InternalRouterState } from 'stencil-router-v2/dist/types';
import { ComponentStatus, statusForComponent } from '../../helpers';

@Component({
  tag: 'digi-docs-navigation',
  styleUrl: 'digi-docs-navigation.scss',
  scoped: true
})
export class DigiDocsNavigation {
  Router = router;

  @Element() el: HTMLStencilElement;

  private _initiated: boolean;

  @State() _activePath: string;
  @State() isMobile: boolean;
  @State() isOpen: boolean;

  @Listen('click')
  clickHandler(e: any) {
    if (e.target.matches('.digi-navigation-vertical-menu-item__link')) {
      if (e.target.getAttribute('href')[0] === '/') {
        this.Router.push(e.target.getAttribute('href'));
      }
      // If target is combined link and toggle button,
      // open up subnav when clicking on the link
      if (
        e.target.parentElement.matches(
          '.digi-navigation-vertical-menu-item__inner--combined'
        )
      ) {
        const el = e.target.closest('digi-navigation-vertical-menu-item');
        el.setAttribute('af-active-subnav', 'true');
      }

      if (this.isMobile) {
        state.activeNav = false;
      }
    }
  }

  get wrapperElement() {
    return this.isMobile ? 'digi-navigation-sidebar' : 'div';
  }

  getComponentName(component: any) {
    const componentName =
      component.docsTags?.find((tag) => tag.name === 'swedishName')?.text ||
      component.tag;
    return componentName;
  }

  componentWillLoad() {
    this.Router.onChange(
      'activePath',
      (_activePath: InternalRouterState['activePath']) => {
        this._activePath = _activePath;

        if (this._initiated) {
          this.setActiveSubnavs();
        }
      }
    );
  }

  componentDidLoad() {
    this._initiated = true;
    this.setActiveSubnavs();
  }

  setActiveSubnavs() {
    let activeEl = null;

    if (state.router.newPaths.length > 1) {
      const path = [...state.router.newPaths];
      activeEl = document.querySelector(
        `digi-navigation-vertical-menu-item[af-href="/${path.join('/')}"]`
      );
    }

    if (activeEl) {
      let parentUl = activeEl.closest('ul');
      let parentToggle = parentUl.parentElement.querySelector(
        'digi-navigation-vertical-menu-item'
      );

      function setActiveAttribute() {
        if (
          !parentUl.matches('.digi-docs-main-nav__root') &&
          parentToggle.getAttribute('af-active-subnav')
        ) {
          parentToggle.setAttribute('af-active-subnav', 'true');
        }
        if (activeEl.getAttribute('af-active-subnav') === 'false') {
          activeEl.setAttribute('af-active-subnav', 'true');
        }
      }
      setActiveAttribute();

      let items = 1;
      let i = 0;
      while (i < items) {
        const newParentUl = parentUl.parentElement.closest('ul');
        if (newParentUl) {
          parentUl = newParentUl;
          parentToggle = newParentUl.parentElement.querySelector(
            'digi-navigation-vertical-menu-item'
          );
          setActiveAttribute();
          items++;
        }
        i++;
      }
    }
  }

  checkActive(level: number, linkItem: string, exact = true) {
    return String(
      state.router.newPaths[level] === linkItem &&
        (exact ? state.router.newPaths.length === level + 1 : true)
    );
  }

  render() {
    return (
      <digi-util-breakpoint-observer
        id="sideNavObserver"
        onAfOnChange={(e) =>
          (this.isMobile =
            e.detail.value === UtilBreakpointObserverBreakpoints.SMALL)
        }>
        <digi-navigation-sidebar
          id="sideNav"
          class="digi-docs-navigation__sidebar"
          afActive={this.isMobile ? state.activeNav : true}
          afVariation={NavigationSidebarVariation.STATIC}
          afPosition={NavigationSidebarPosition.END}
          afHideHeader={this.isMobile ? false : true}
          afStickyHeader={true}
          afCloseButtonText="Stäng"
          onAfOnClose={() => (state.activeNav = false)}
          onAfOnEsc={() => (state.activeNav = false)}
          onAfOnBackdropClick={() => (state.activeNav = false)}>
          <digi-navigation-vertical-menu
            afId="digi-docs-main-nav"
            afAriaLabel="Huvudmeny"
            afActiveIndicatorSize={
              NavigationVerticalMenuActiveIndicatorSize.SECONDARY
            }>
            <ul class="digi-docs-main-nav__root">
              <li>
                <digi-navigation-vertical-menu-item
                  af-text="Om designsystemet"
                  af-active-subnav="false"></digi-navigation-vertical-menu-item>
                <ul>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Introduktion"
                      af-href={`${state.routerRoot}om-designsystemet/introduktion`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'introduktion'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Digi Core"
                      af-href={`${state.routerRoot}om-designsystemet/digi-core`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'digi-core'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Digi UI Kit"
                      af-href={`${state.routerRoot}om-designsystemet/digi-ui-kit`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'digi-ui-kit'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Digi Tokens"
                      af-href={`${state.routerRoot}om-designsystemet/digi-tokens`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'digi-tokens'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Forskning och samarbeten"
                      af-href={`${state.routerRoot}om-designsystemet/forskning-och-samarbeten`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'forskning-och-samarbeten'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                </ul>
              </li>
              <li>
                <digi-navigation-vertical-menu-item
                  af-text="Grafisk profil"
                  af-active-subnav="false"></digi-navigation-vertical-menu-item>
                <ul>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Vår grafiska profil"
                      af-href={`${state.routerRoot}grafisk-profil/om-grafisk-profil`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'om-grafisk-profil'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Varumärket Arbetsförmedlingen"
                      af-href={`${state.routerRoot}grafisk-profil/varumarket-arbetsformedlingen`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'varumarket-arbetsformedlingen'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Logotyp"
                      af-href={`${state.routerRoot}grafisk-profil/logotyp`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'logotyp'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Färger"
                      af-href={`${state.routerRoot}grafisk-profil/farger`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'farger',
                        false
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Typografi"
                      af-href={`${state.routerRoot}grafisk-profil/typografi`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'typografi'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Grafik"
                      af-href={`${state.routerRoot}grafisk-profil/grafik`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'grafik'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Bilder"
                      af-href={`${state.routerRoot}grafisk-profil/bilder`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'bilder'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Illustrationer"
                      af-href={`${state.routerRoot}grafisk-profil/illustrationer`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'illustrationer'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Animationer"
                      af-href={`${state.routerRoot}grafisk-profil/animationer`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'animationer'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Rörligt och ljud"
                      af-href={`${state.routerRoot}grafisk-profil/rorligt-och-ljud`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'rorligt-och-ljud'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Namngivning och klarspråk"
                      af-href={`${state.routerRoot}grafisk-profil/klarsprak`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'klarsprak'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Samarbeten"
                      af-href={`${state.routerRoot}grafisk-profil/samarbeten`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'samarbeten'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                </ul>
              </li>
              <li>
                <digi-navigation-vertical-menu-item
                  af-text="Kom igång"
                  af-override-link="true"
                  af-active-subnav="false"></digi-navigation-vertical-menu-item>
                <ul>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Jobba med Digi Core"
                      af-href={`${state.routerRoot}kom-i-gang/jobba-med-digi-core`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'jobba-med-digi-core'
                      )}></digi-navigation-vertical-menu-item>
                    <ul>
                      <li>
                        <digi-navigation-vertical-menu-item
                          af-text="Digi Core Angular"
                          af-href={`${state.routerRoot}kom-i-gang/jobba-med-digi-core/digi-core-angular`}
                          af-override-link="true"
                          af-active={this.checkActive(
                            2,
                            'digi-core-angular'
                          )}></digi-navigation-vertical-menu-item>
                      </li>
                      <li>
                        <digi-navigation-vertical-menu-item
                          af-text="Digi Core React"
                          af-href={`${state.routerRoot}kom-i-gang/jobba-med-digi-core/digi-core-react`}
                          af-override-link="true"
                          af-active={this.checkActive(
                            2,
                            'digi-core-react'
                          )}></digi-navigation-vertical-menu-item>
                      </li>
                    </ul>
                  </li>

                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Jobba med Digi Ui Kit"
                      af-href={`${state.routerRoot}kom-i-gang/jobba-med-digi-ui-kit`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'jobba-med-digi-ui-kit'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                </ul>
              </li>

              <li>
                <digi-navigation-vertical-menu-item
                  af-text="Komponenter"
                  af-active-subnav="false"></digi-navigation-vertical-menu-item>

                <ul>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Om vårt komponentbibliotek"
                      af-href={`${state.routerRoot}komponenter/om-komponenter`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'om-komponenter'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  {Object.values(Category)
                    .sort()
                    .filter(
                      (category) =>
                        category !== Category.ICON &&
                        category !== Category.UNCATEGORIZED &&
                        category !== Category.TEST &&
                        category !== Category.PAGE
                    )
                    .map((category) => {
                      return (
                        <li>
                          <digi-navigation-vertical-menu-item
                            af-text={category}
                            af-active-subnav="false"></digi-navigation-vertical-menu-item>
                          <ul>
                            {state.components
                              .filter(
                                (component) => component.category === category
                              )
                              .filter(
                                (component) =>
                                  !component.tag.includes('digi-icon-')
                              )
                              .map((component) => {
                                const status = statusForComponent(component);
                                return (
                                  <li>
                                    <digi-navigation-vertical-menu-item
                                      id={`komponent-${component.tag}`}
                                      af-href={`${state.routerRoot}komponenter/${component.tag}/oversikt`}
                                      af-override-link="true"
                                      af-text={this.getComponentName(component)}
                                      af-active={this.checkActive(
                                        1,
                                        component.tag,
                                        false
                                      )}>
                                      {status != null && (
                                        <span slot="badge">
                                          <digi-badge-status
                                            af-type={
                                              status ===
                                              ComponentStatus.Deprecated
                                                ? 'missing'
                                                : 'prompt'
                                            }
                                            af-size="small"
                                            af-variation="secondary"
                                            afText={
                                              status ===
                                              ComponentStatus.Deprecated
                                                ? 'Utgår'
                                                : 'Ny'
                                            }
                                            class="badge"></digi-badge-status>
                                        </span>
                                      )}
                                    </digi-navigation-vertical-menu-item>
                                  </li>
                                );
                              })}
                          </ul>
                        </li>
                      );
                    })}
                </ul>
              </li>
              <li>
                <digi-navigation-vertical-menu-item
                  af-text="Design tokens"
                  af-href={`${state.routerRoot}design-tokens`}
                  af-override-link="true"
                  af-active={this.checkActive(
                    0,
                    'design-tokens',
                    false
                  )}></digi-navigation-vertical-menu-item>
              </li>

              <li>
                <digi-navigation-vertical-menu-item
                  af-text="Designmönster"
                  af-active-subnav="false"></digi-navigation-vertical-menu-item>

                <ul>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Introduktion"
                      af-href={`${state.routerRoot}designmonster/introduktion`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'introduktion'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Formulär"
                      af-href={`${state.routerRoot}designmonster/formular`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'formular'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Validering"
                      af-href={`${state.routerRoot}designmonster/validering`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'validering'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Grid och brytpunkter"
                      af-href={`${state.routerRoot}designmonster/grid-och-brytpunkter`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'grid-och-brytpunkter'
                      )}></digi-navigation-vertical-menu-item>
                  </li>

                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Spacing"
                      af-href={`${state.routerRoot}designmonster/spacing`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'spacing'
                      )}></digi-navigation-vertical-menu-item>
                  </li>

                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Länkar"
                      af-href={`${state.routerRoot}designmonster/lankar`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'lankar'
                      )}></digi-navigation-vertical-menu-item>
                  </li>

                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Knappar"
                      af-href={`${state.routerRoot}designmonster/knappar`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'knappar'
                      )}></digi-navigation-vertical-menu-item>
                  </li>

                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Laddningsindikatorer"
                      af-href={`${state.routerRoot}designmonster/laddningsindikatorer`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'laddare'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Felmeddelandesidor"
                      af-href={`${state.routerRoot}designmonster/felmeddelandesidor`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'felmeddelandesidor'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Agentiva tjänster"
                      af-href={`${state.routerRoot}designmonster/agentiva-tjanster`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'agentiva-tjanster'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Piktogram"
                      af-href={`${state.routerRoot}designmonster/pictogram`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'pictogram'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Sök och sökfilter"
                      af-href={`${state.routerRoot}designmonster/sok-och-sokfilter`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'sok-och-sokfilter'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Enkäter och feedback"
                      af-href={`${state.routerRoot}designmonster/enkater-och-feedback`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'enkater-och-feedback'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Indikatorer"
                      af-href={`${state.routerRoot}designmonster/indikatorer`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'indikatorer'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                </ul>
              </li>
              <li>
                <digi-navigation-vertical-menu-item
                  af-text="Ikoner"
                  af-href={`${state.routerRoot}ikoner`}
                  af-override-link="true"
                  af-active={this.checkActive(
                    0,
                    'ikoner',
                    false
                  )}></digi-navigation-vertical-menu-item>
              </li>
              <li>
                <digi-navigation-vertical-menu-item
                  af-text="Tillgänglighet"
                  af-active-subnav="false"></digi-navigation-vertical-menu-item>
                <ul>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Om digital tillgänglighet"
                      af-href={`${state.routerRoot}tillganglighet-och-design/om-digital-tillganglighet`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'om-digital-tillganglighet'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Tillgänglighetslista"
                      af-href={`${state.routerRoot}tillganglighet-och-design/tillganglighet-checklista`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'tillganglighet-checklista'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Kognitiv genomgång"
                      af-href={`${state.routerRoot}tillganglighet-och-design/kognitiv-genomgang`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'kognitiv-genomgang'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Tillgänglighetsmatrisen"
                      af-href={`${state.routerRoot}tillganglighet-och-design/om-tillganglighetsmatrisen`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'om-tillganglighetsmatrisen'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Nivåer i WCAG"
                      af-href={`${state.routerRoot}tillganglighet-checklista/wcag-levels`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'wcag-levels'
                      )}></digi-navigation-vertical-menu-item>
                  </li>

                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Process för tillgänglighetsredogörelse"
                      af-href={`${state.routerRoot}tillganglighet-och-design/tillganglighetsredogorelse`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'tillganglighetsredogorelse'
                      )}
                      af-active-subnav="false"></digi-navigation-vertical-menu-item>
                    <ul>
                      <li>
                        <digi-navigation-vertical-menu-item
                          af-text="Instruktioner för lista med tillgänglighetsbrister"
                          af-href={`${state.routerRoot}tillganglighetsredogorelse/lista-med-tillganglighetsbrister`}
                          af-override-link="true"
                          af-active={this.checkActive(
                            1,
                            'lista-med-tillganglighetsbrister'
                          )}></digi-navigation-vertical-menu-item>
                      </li>
                      <li>
                        <digi-navigation-vertical-menu-item
                          af-text="Instruktioner för externa webbplatser"
                          af-href={`${state.routerRoot}tillganglighetsredogorelse/externa-webbplatser`}
                          af-override-link="true"
                          af-active={this.checkActive(
                            1,
                            'externa-webbplatser'
                          )}></digi-navigation-vertical-menu-item>
                      </li>
                      <li>
                        <digi-navigation-vertical-menu-item
                          af-text="Instruktioner för interna webbplatser"
                          af-href={`${state.routerRoot}tillganglighetsredogorelse/interna-webbplatser`}
                          af-override-link="true"
                          af-active={this.checkActive(
                            1,
                            'interna-webbplatser'
                          )}></digi-navigation-vertical-menu-item>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Lagkrav och riktlinjer"
                      af-href={`${state.routerRoot}tillganglighet-och-design/lagkrav-och-riktlinjer`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'lagkrav-och-riktlinjer'
                      )}></digi-navigation-vertical-menu-item>
                  </li>

                  {/* <li>
										<digi-navigation-vertical-menu-item
											af-text="Hjälpmedel"
											af-href={`${state.routerRoot}tillganglighet-och-design/hjalpmedel`}
											af-override-link="true"
											af-active={this.checkActive(1, 'hjalpmedel')}
										></digi-navigation-vertical-menu-item>
									</li>
*/}
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Användningstester och användarutvärderingar"
                      af-href={`${state.routerRoot}tillganglighet-och-design/anvandningstester`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'anvandningstester'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                </ul>
              </li>
              <li>
                <digi-navigation-vertical-menu-item
                  af-text="Språk"
                  af-active-subnav="false">
                  {' '}
                </digi-navigation-vertical-menu-item>
                <ul>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Om vårt språk"
                      af-href={`${state.routerRoot}sprak/digi-docs-about-copy-languages`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'digi-docs-about-copy-languages'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Klarspråk"
                      af-href={`${state.routerRoot}sprak/klarsprak`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'klarsprak'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Mikrocopy"
                      af-href={`${state.routerRoot}sprak/mikrocopy`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'mikrocopy'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                  <li>
                    <digi-navigation-vertical-menu-item
                      af-text="Översättningar"
                      af-href={`${state.routerRoot}sprak/oversattningar`}
                      af-override-link="true"
                      af-active={this.checkActive(
                        1,
                        'oversattningar'
                      )}></digi-navigation-vertical-menu-item>
                  </li>
                </ul>
              </li>

              <li>
                <digi-navigation-vertical-menu-item
                  af-text="Kontakt"
                  af-href={`${state.routerRoot}kontakt`}
                  af-override-link="true"
                  af-active={this.checkActive(
                    0,
                    'kontakt'
                  )}></digi-navigation-vertical-menu-item>
              </li>
              <li>
                <digi-navigation-vertical-menu-item
                  af-text="Release notes"
                  af-href={`${state.routerRoot}release-notes`}
                  af-override-link="true"
                  af-active={this.checkActive(0, 'release-notes')}
                  af-lang="en"></digi-navigation-vertical-menu-item>
              </li>
            </ul>
          </digi-navigation-vertical-menu>
        </digi-navigation-sidebar>
      </digi-util-breakpoint-observer>
    );
  }
}
