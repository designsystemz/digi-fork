import { Component, Element, h } from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import {
  CodeBlockLanguage,
  CodeBlockVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-code-examples',
  styleUrl: 'digi-docs-code-examples.scss',
  scoped: true
})
export class CodeExamples {
  @Element() hostElement: HTMLStencilElement;

  render() {
    return (
      <host>
        <digi-docs-code-examples-tabs
          af-id="components"
          af-heading="Teknisk plattform"
          af-text={`Med hjälp av <a href="https://stenciljs.com/" rel="noopener noreferrer" target="_blank">Stencil (stenciljs.com, öppnas i egen flik)</a> och <a href="https://nx.dev/" rel="noopener noreferrer" target="_blank">NX (nx.dev, öppnas i egen flik)</a> bygger vi komponenter som fungerar i olika ramverk.`}>
          <div
            id="jsexample"
            tabindex="0"
            role="tabpanel"
            aria-label="Web Components kodexempel"
            data-button-label="JavaScript"
            data-button-icon='<svg
							role="img"
							aria-hidden="true"
							class="logoWebComponents"
							style="width:100%;height:100%;"
							viewBox="0 0 630 630"
							xmlns="http://www.w3.org/2000/svg"
						>
							<rect width="630" height="630" fill="#f7df1e"></rect>
							<path d="m423.2 492.19c12.69 20.72 29.2 35.95 58.4 35.95 24.53 0 40.2-12.26 40.2-29.2 0-20.3-16.1-27.49-43.1-39.3l-14.8-6.35c-42.72-18.2-71.1-41-71.1-89.2 0-44.4 33.83-78.2 86.7-78.2 37.64 0 64.7 13.1 84.2 47.4l-46.1 29.6c-10.15-18.2-21.1-25.37-38.1-25.37-17.34 0-28.33 11-28.33 25.37 0 17.76 11 24.95 36.4 35.95l14.8 6.34c50.3 21.57 78.7 43.56 78.7 93 0 53.3-41.87 82.5-98.1 82.5-54.98 0-90.5-26.2-107.88-60.54zm-209.13 5.13c9.3 16.5 17.76 30.45 38.1 30.45 19.45 0 31.72-7.61 31.72-37.2v-201.3h59.2v202.1c0 61.3-35.94 89.2-88.4 89.2-47.4 0-74.85-24.53-88.81-54.075z"></path>
						</svg>'>
            <digi-docs-code-examples-code-block
              afVariation={CodeBlockVariation.DARK}
              afLanguage={CodeBlockLanguage.TSX}
              afCode={`//main.js
import "@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css"
import "@digi/arbetsformedlingen/components/";
//index.html
<digi-button
	af-size="medium"
	af-variation="primary"
	af-full-width="false">
	En knapp
</digi-button>
<script type="module" src="/src/main.ts"></script>`}></digi-docs-code-examples-code-block>
          </div>
          <div
            id="angularexample"
            tabindex="0"
            role="tabpanel"
            aria-label="Angular kodexempel"
            data-button-label="Angular"
            data-button-icon='<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" viewBox="0 0 186.2 200"><path fill="#dd0031" d="M93.1 0 0 33.2l14.2 123.1L93.1 200l78.9-43.7 14.2-123.1Z"/><path fill="#c3002f" d="M93.1 0v22.2-.1V200l78.9-43.7 14.2-123.1Z"/><path fill="#ffffff" d="M93.1 22.1 34.9 152.6h21.7l11.7-29.2h49.4l11.7 29.2h21.7zm17 83.3h-34l17-40.9z"/></svg>'
            hidden>
            <digi-docs-code-examples-code-block
              afVariation={CodeBlockVariation.DARK}
              afLanguage={CodeBlockLanguage.TSX}
              afCode={`// Importera DigiArbetsformedlingenAngularModule in till app.module
import { DigiArbetsformedlingenAngularModule } from '@digi/arbetsformedlingen-angular';
  // ...
  imports: [... DigiArbetsformedlingenAngularModule],

// Använd i din komponent
<digi-button
  af-size={ButtonSize.MEDIUM}
  af-variation={ButtonVariation.PRIMARY"}
  af-full-width={false}
  onAfOnClick={myFunction}>
  En knapp
</digi-button>
`}></digi-docs-code-examples-code-block>
          </div>
          <div
            id="reactexample"
            tabindex="0"
            role="tabpanel"
            aria-label="React kodexempel"
            data-button-label="React"
            data-button-icon='<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" viewBox="0 0 490.6 436.9"><g fill="#61dafb" transform="translate(-175.7 -78)"><path d="M666.3 296.5c0-32.5-40.7-63.3-103.1-82.4 14.4-63.6 8-114.2-20.2-130.4-6.5-3.8-14.1-5.6-22.4-5.6v22.3c4.6 0 8.3.9 11.4 2.6 13.6 7.8 19.5 37.5 14.9 75.7-1.1 9.4-2.9 19.3-5.1 29.4-19.6-4.8-41-8.5-63.5-10.9-13.5-18.5-27.5-35.3-41.6-50 32.6-30.3 63.2-46.9 84-46.9V78c-27.5 0-63.5 19.6-99.9 53.6-36.4-33.8-72.4-53.2-99.9-53.2v22.3c20.7 0 51.4 16.5 84 46.6-14 14.7-28 31.4-41.3 49.9-22.6 2.4-44 6.1-63.6 11-2.3-10-4-19.7-5.2-29-4.7-38.2 1.1-67.9 14.6-75.8 3-1.8 6.9-2.6 11.5-2.6V78.5c-8.4 0-16 1.8-22.6 5.6-28.1 16.2-34.4 66.7-19.9 130.1-62.2 19.2-102.7 49.9-102.7 82.3 0 32.5 40.7 63.3 103.1 82.4-14.4 63.6-8 114.2 20.2 130.4 6.5 3.8 14.1 5.6 22.5 5.6 27.5 0 63.5-19.6 99.9-53.6 36.4 33.8 72.4 53.2 99.9 53.2 8.4 0 16-1.8 22.6-5.6 28.1-16.2 34.4-66.7 19.9-130.1 62-19.1 102.5-49.9 102.5-82.3zm-130.2-66.7c-3.7 12.9-8.3 26.2-13.5 39.5-4.1-8-8.4-16-13.1-24-4.6-8-9.5-15.8-14.4-23.4 14.2 2.1 27.9 4.7 41 7.9zm-45.8 106.5c-7.8 13.5-15.8 26.3-24.1 38.2-14.9 1.3-30 2-45.2 2-15.1 0-30.2-.7-45-1.9-8.3-11.9-16.4-24.6-24.2-38-7.6-13.1-14.5-26.4-20.8-39.8 6.2-13.4 13.2-26.8 20.7-39.9 7.8-13.5 15.8-26.3 24.1-38.2 14.9-1.3 30-2 45.2-2 15.1 0 30.2.7 45 1.9 8.3 11.9 16.4 24.6 24.2 38 7.6 13.1 14.5 26.4 20.8 39.8-6.3 13.4-13.2 26.8-20.7 39.9zm32.3-13c5.4 13.4 10 26.8 13.8 39.8-13.1 3.2-26.9 5.9-41.2 8 4.9-7.7 9.8-15.6 14.4-23.7 4.6-8 8.9-16.1 13-24.1zM421.2 430c-9.3-9.6-18.6-20.3-27.8-32 9 .4 18.2.7 27.5.7 9.4 0 18.7-.2 27.8-.7-9 11.7-18.3 22.4-27.5 32zm-74.4-58.9c-14.2-2.1-27.9-4.7-41-7.9 3.7-12.9 8.3-26.2 13.5-39.5 4.1 8 8.4 16 13.1 24 4.7 8 9.5 15.8 14.4 23.4zM420.7 163c9.3 9.6 18.6 20.3 27.8 32-9-.4-18.2-.7-27.5-.7-9.4 0-18.7.2-27.8.7 9-11.7 18.3-22.4 27.5-32zm-74 58.9c-4.9 7.7-9.8 15.6-14.4 23.7-4.6 8-8.9 16-13 24-5.4-13.4-10-26.8-13.8-39.8 13.1-3.1 26.9-5.8 41.2-7.9zm-90.5 125.2c-35.4-15.1-58.3-34.9-58.3-50.6 0-15.7 22.9-35.6 58.3-50.6 8.6-3.7 18-7 27.7-10.1 5.7 19.6 13.2 40 22.5 60.9-9.2 20.8-16.6 41.1-22.2 60.6-9.9-3.1-19.3-6.5-28-10.2zM310 490c-13.6-7.8-19.5-37.5-14.9-75.7 1.1-9.4 2.9-19.3 5.1-29.4 19.6 4.8 41 8.5 63.5 10.9 13.5 18.5 27.5 35.3 41.6 50-32.6 30.3-63.2 46.9-84 46.9-4.5-.1-8.3-1-11.3-2.7zm237.2-76.2c4.7 38.2-1.1 67.9-14.6 75.8-3 1.8-6.9 2.6-11.5 2.6-20.7 0-51.4-16.5-84-46.6 14-14.7 28-31.4 41.3-49.9 22.6-2.4 44-6.1 63.6-11 2.3 10.1 4.1 19.8 5.2 29.1zm38.5-66.7c-8.6 3.7-18 7-27.7 10.1-5.7-19.6-13.2-40-22.5-60.9 9.2-20.8 16.6-41.1 22.2-60.6 9.9 3.1 19.3 6.5 28.1 10.2 35.4 15.1 58.3 34.9 58.3 50.6-.1 15.7-23 35.6-58.4 50.6zM320.8 78.4Z"/><circle cx="420.9" cy="296.5" r="45.7"/><path d="M520.5 78.1Z"/></g></svg>'
            hidden>
            <digi-docs-code-examples-code-block
              afVariation={CodeBlockVariation.DARK}
              afLanguage={CodeBlockLanguage.TSX}
              afCode={`//index.tsx
import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import "@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css";
ReactDOM.createRoot(
	document.getElementById('root') as HTMLElement
	).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
//App.tsx
import { DigiButton } from '@digi/arbetsformedlingen-react';
import { ButtonVariation } from '@digi/arbetsformedlingen';

export function App() {

	function myFunction(e) {
		console.log(e)
	}

	return (
		<DigiButton onAfOnClick={myFunction} afVariation={ButtonVariation.PRIMARY}>En knapp</DigiButton>
	);
}
export default App;`}></digi-docs-code-examples-code-block>
          </div>
        </digi-docs-code-examples-tabs>
      </host>
    );
  }
}
