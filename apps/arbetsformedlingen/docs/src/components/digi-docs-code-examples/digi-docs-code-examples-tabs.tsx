import { Component, Element, State, h, Prop } from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { LayoutBlockVariation } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-code-examples-tabs',
  styleUrl: 'digi-docs-code-examples-tabs.scss',
  scoped: true
})
export class CodeExamplesTabs {
  @Element() hostElement: HTMLStencilElement;

  @Prop() afId: string;
  @Prop() afHeading: string;
  @Prop() afText: string;

  @State() tabPanels = [];
  @State() activeTab = 0;
  @State() currentTabIndex: number = this.activeTab;

  setTabFocus(id): void {
    id = this.tabId(this.tabPanels[id].id);
    if (document.getElementById(`${id}`)) {
      document.getElementById(`${id}`).focus({ preventScroll: true });
    }
  }

  clickHandler(i: number) {
    this.setActiveTab(i);
  }

  focusHandler() {
    const isActiveTab =
      this.tabPanels.findIndex(
        (i) => this.tabId(i.id) === document.activeElement.id
      ) === this.activeTab;
    this.currentTabIndex = isActiveTab ? this.activeTab : this.currentTabIndex;
  }

  leftHandler() {
    this.decrementCurrentTabIndex();
    this.setTabFocus(this.currentTabIndex);
  }

  rightHandler() {
    this.incrementCurrentTabIndex();
    this.setTabFocus(this.currentTabIndex);
  }

  homeHandler() {
    this.setTabFocus('0');
  }

  endHandler() {
    this.setTabFocus(this.tabPanels.length - 1);
  }
  tabId(prefix: string): string {
    return `${prefix}-tab`;
  }

  decrementCurrentTabIndex() {
    this.currentTabIndex > 0
      ? (this.currentTabIndex = this.currentTabIndex - 1)
      : (this.currentTabIndex = this.tabPanels.length - 1);
  }

  incrementCurrentTabIndex() {
    this.currentTabIndex < this.tabPanels.length - 1
      ? (this.currentTabIndex = this.currentTabIndex + 1)
      : (this.currentTabIndex = 0);
  }

  componentDidLoad() {
    this.getTabs();
  }

  setActiveTab(newTabIndex: number) {
    this.activeTab = newTabIndex;
    this.currentTabIndex = this.activeTab;

    this.tabPanels.forEach((tab, i: number) => {
      if (i === newTabIndex) {
        tab.hidden = false;
      } else {
        tab.hidden = true;
      }
    });
  }

  getTabs(e = null) {
    const tablist = this.hostElement.querySelectorAll(
      `#${this.afId}-observer > [role="tabpanel"]`
    );

    if (!tablist) {
      return;
    }

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.tabPanels = [...tablist];

    let activeTabIndex = 0;

    // If tabs are added or removed
    if (e) {
      if (!e.detail.addedNodes || !e.detail.removedNodes) {
        return;
      }
      const added = e.detail.addedNodes?.item(0),
        removed = e.detail.removedNodes?.item(0);
      activeTabIndex = this.currentTabIndex;

      if (added) {
        // If added tab is same position or before current tab, jump to the right
        Object.values(e.target.children).indexOf(added) <=
          this.currentTabIndex && (activeTabIndex += 1);
      } else if (removed) {
        // If removed tab is before current tab or is last, jump to the left
        if (
          (removed.dataset.position == e.target.children.length &&
            removed.dataset.position == this.currentTabIndex) ||
          removed.dataset.position < this.currentTabIndex
        ) {
          activeTabIndex -= 1;
        }
      }
    }

    this.setActiveTab(activeTabIndex);
  }

  render() {
    return (
      <host>
        <div class="digi-docs-code-examples-tabs">
          <digi-layout-block
            afVariation={LayoutBlockVariation.TRANSPARENT}
            afVerticalPadding={false}>
            <digi-typography>
              <h2
                id={this.afId + '__heading'}
                class="digi-docs-code-examples-tabs__heading">
                {this.afHeading}
              </h2>
              <div
                class="digi-docs-code-examples-tabs__body"
                innerHTML={this.afText}></div>
            </digi-typography>
            <digi-util-keydown-handler
              onAfOnLeft={() => this.leftHandler()}
              onAfOnRight={() => this.rightHandler()}
              onAfOnHome={() => this.homeHandler()}
              onAfOnEnd={() => this.endHandler()}>
              <div
                role="tablist"
                aria-labelledby={this.afId + '__heading'}
                class="digi-docs-code-examples-tabs__tabs">
                {this.tabPanels.map((tab: HTMLElement, i: number) => {
                  return (
                    <button
                      class="digi-docs-code-examples-tabs__tab"
                      role="tab"
                      type="button"
                      aria-selected={this.activeTab === i ? 'true' : null}
                      aria-controls={tab.id}
                      tabindex={this.activeTab !== i ? '-1' : null}
                      id={`${this.tabId(tab.id)}`}
                      onClick={() => this.clickHandler(i)}
                      onFocus={() => this.focusHandler()}>
                      <span
                        class="digi-docs-code-examples-tabs__tab-icon"
                        innerHTML={tab.dataset.buttonIcon}></span>
                      <span class="digi-docs-code-examples-tabs__tab-text">
                        {tab.dataset.buttonLabel}
                      </span>
                    </button>
                  );
                })}
              </div>
            </digi-util-keydown-handler>
          </digi-layout-block>
          <digi-layout-block afVariation={LayoutBlockVariation.TERTIARY}>
            <digi-util-mutation-observer
              onAfOnChange={(e) => this.getTabs(e)}
              id={`${this.afId}-observer`}>
              <slot></slot>
            </digi-util-mutation-observer>
          </digi-layout-block>
        </div>
      </host>
    );
  }
}
