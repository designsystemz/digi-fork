import {
  Component,
  h,
  State,
  Fragment,
  Prop,
  Watch,
  Element
} from '@stencil/core';
import { ButtonVariation, CodeVariation } from '@digi/arbetsformedlingen';
import { DocsColorSwatchVariation } from '../color-swatch/digi-docs-color-swatch-variation.enum';
import { TokenFormat } from '../../token-format.enum';
import type { HTMLStencilElement } from '@stencil/core/internal';

@Component({
  tag: 'digi-docs-color-token-cards',
  styleUrl: 'digi-docs-color-token-cards.scss',
  scoped: true
})
export class ColorTokenCards {
  @Element() hostElement: HTMLStencilElement;

  private _isPrint = false;

  @State() pageName = 'Color Token Cards';
  @State() _colorTokens = [];
  @State() _tokenFormat: TokenFormat;

  @Prop() colorTokens;
  @Watch('colorTokens')
  colorTokensChangeHandler() {
    this._colorTokens = [...this.colorTokens];
  }

  @Prop() tokenFormat: TokenFormat = TokenFormat.CUSTOM_PROPERTY;
  @Watch('tokenFormat')
  tokenFormatChangeHandler() {
    this._tokenFormat = this.tokenFormat;
  }

  scssValue(code: string) {
    return `$${code}`;
  }

  customPropertyValue(code: string) {
    return `--${code}`;
  }

  componentWillLoad() {
    this._isPrint = this.tokenFormat == TokenFormat.RGB;
    this.colorTokensChangeHandler();
    this.tokenFormatChangeHandler();
  }

  componentDidLoad() {
    this.initToggleButtons();
  }

  initToggleButtons() {
    const toggleButtons = this.hostElement.querySelectorAll(
      'digi-button[data-toggle-button] button'
    );
    toggleButtons.forEach((button: HTMLElement) => {
      const controlledItems = this.getControlledItems();
      button.setAttribute('aria-expanded', 'false');
      button.setAttribute('aria-controls', controlledItems);
    });
  }

  toggleHandler(e) {
    const $this = e.target as HTMLElement;
    const $button = $this.querySelector('button');

    const groupElements = this.hostElement.querySelectorAll(
      `*[data-list-group="tokenListCollapse-${this._colorTokens[0].attributes.subitem}"]`
    );

    const isExpanded = $button.getAttribute('aria-expanded') == 'true';

    $button.setAttribute('aria-expanded', String(!isExpanded));

    groupElements.forEach((item) => {
      item.setAttribute('hidden', String(isExpanded));
      item.setAttribute('aria-hidden', String(isExpanded));
      item.classList.toggle('hidden');
      item
        .closest('digi-card')
        .classList.toggle('digi-docs-color-token-cards__item--expanded');
    });
  }

  getControlledItems() {
    let controlsList = '';
    this._colorTokens.forEach((token, index) => {
      if (index > 0) {
        controlsList += `${index > 1 ? ', ' : ''}#tokenListCollapse-${token.attributes.subitem}-${index - 1}`;
      }
    });
    return controlsList;
  }

  render() {
    return (
      <host>
        <div class="digi-docs-color-token-cards">
          <div class="digi-docs-color-token-cards__list">
            {this._colorTokens.length > 0 && (
              <Fragment>
                {this._colorTokens
                  .filter((token) => token.attributes.state === 'base')
                  .map((token) => {
                    return (
                      <digi-card class="digi-docs-color-token-cards__item">
                        <div class="digi-docs-color-token-cards__content">
                          <div class="digi-docs-color-token-cards__content-start">
                            <div class="digi-docs-color-token-cards__header">
                              <digi-docs-color-swatch
                                token={JSON.stringify(token)}
                                token-variation={
                                  DocsColorSwatchVariation.CIRCLE
                                }
                                token-border={true}
                                token-color-format="HEX"></digi-docs-color-swatch>
                              <div>
                                <h3 class="digi-docs-color-token-cards__heading">
                                  {token.original.name ||
                                    token.path[token.path.length - 2]}
                                </h3>
                                {!this._isPrint && <p>{token.value}</p>}
                              </div>
                            </div>
                            {token.comment && (
                              <p class="digi-docs-color-token-cards__comment">
                                {token.comment}
                              </p>
                            )}
                            <div class="digi-docs-color-token-cards__token">
                              {this._isPrint && (
                                <dl class="digi-docs-color-token-cards__print-data">
                                  {token.attributes.CMYK && (
                                    <Fragment>
                                      <dt>CMYK</dt>
                                      <dd>{token.attributes.CMYK}</dd>
                                    </Fragment>
                                  )}
                                  {token.attributes.PMS && (
                                    <Fragment>
                                      <dt>PMS</dt>
                                      <dd>{token.attributes.PMS}</dd>
                                    </Fragment>
                                  )}
                                  {token.attributes.RGB && (
                                    <Fragment>
                                      <dt>RGB (print)</dt>
                                      <dd>{token.attributes.RGB}</dd>
                                    </Fragment>
                                  )}
                                </dl>
                              )}
                              {this._tokenFormat ==
                                TokenFormat.CUSTOM_PROPERTY && (
                                <digi-code
                                  afVariation={CodeVariation.LIGHT}
                                  afCode={this.customPropertyValue(
                                    token.name
                                  )}></digi-code>
                              )}
                              {this._tokenFormat == TokenFormat.SASS && (
                                <digi-code
                                  afVariation={CodeVariation.LIGHT}
                                  afCode={this.scssValue(
                                    token.name
                                  )}></digi-code>
                              )}
                            </div>
                          </div>
                          <div class="digi-docs-color-token-cards__content-end">
                            <div class="digi-docs-color-token-cards__swatch-list">
                              {this._colorTokens.map((tokenVariation) => {
                                return (
                                  <digi-docs-color-swatch
                                    token={JSON.stringify(tokenVariation)}
                                    token-border={true}
                                    token-color-format="HEX"
                                    token-variation={
                                      DocsColorSwatchVariation.CIRCLE
                                    }
                                    token-selected={
                                      token.name == tokenVariation.name
                                    }></digi-docs-color-swatch>
                                );
                              })}
                            </div>
                            {this._colorTokens.length > 1 && (
                              <digi-button
                                afVariation={ButtonVariation.FUNCTION}
                                class="digi-docs-color-token-cards__toggle-btn"
                                data-toggle-button="true"
                                onAfOnClick={(e) => this.toggleHandler(e)}>
                                <digi-icon-chevron-down slot="icon"></digi-icon-chevron-down>
                                <span class="digi-docs-color-token-cards__toggle-btn-text">
                                  <span>Visa varianter</span>
                                  <span>Dölj varianter</span>
                                </span>
                              </digi-button>
                            )}
                          </div>
                        </div>
                        <div
                          slot="footer"
                          class="digi-docs-color-token-cards__footer"
                          style={{
                            '--TOKEN--FOOTER--COLOR': !this._isPrint
                              ? `var(${this.customPropertyValue(token.name)})`
                              : token.value
                          }}></div>
                      </digi-card>
                    );
                  })}
                {this._colorTokens.length > 1 &&
                  this._colorTokens
                    .filter((token) => token.attributes.state !== 'base')
                    .map((token, index) => (
                      <digi-card class="digi-docs-color-token-cards__item digi-docs-color-token-cards__item--variation">
                        <div
                          id={`tokenListCollapse-${this._colorTokens[0].attributes.subitem}-${index}`}
                          data-list-group={`tokenListCollapse-${this._colorTokens[0].attributes.subitem}`}
                          aria-hidden="true"
                          hidden={true}
                          class="hidden digi-docs-color-token-cards__content digi-docs-color-token-cards__content--variation">
                          <div class="digi-docs-color-token-cards__content-start">
                            <div class="digi-docs-color-token-cards__header">
                              <digi-docs-color-swatch
                                token={JSON.stringify(token)}
                                token-variation={
                                  DocsColorSwatchVariation.CIRCLE
                                }
                                token-border={true}
                                token-color-format="HEX"></digi-docs-color-swatch>
                              <div>
                                <h3 class="digi-docs-color-token-cards__heading">
                                  {token.original.name ||
                                    token.path[token.path.length - 2]}
                                </h3>
                                {!this._isPrint && <p>{token.value}</p>}
                              </div>
                            </div>
                            {token.comment && (
                              <p class="digi-docs-color-token-cards__comment">
                                {token.comment}
                              </p>
                            )}
                            <div class="digi-docs-color-token-cards__token">
                              {this._isPrint && (
                                <dl class="digi-docs-color-token-cards__print-data">
                                  {token.attributes.CMYK && (
                                    <Fragment>
                                      <dt>CMYK</dt>
                                      <dd>{token.attributes.CMYK}</dd>
                                    </Fragment>
                                  )}
                                  {token.attributes.PMS && (
                                    <Fragment>
                                      <dt>PMS</dt>
                                      <dd>{token.attributes.PMS}</dd>
                                    </Fragment>
                                  )}
                                  {token.attributes.RGB && (
                                    <Fragment>
                                      <dt>RGB (print)</dt>
                                      <dd>{token.attributes.RGB}</dd>
                                    </Fragment>
                                  )}
                                </dl>
                              )}
                              {this._tokenFormat ==
                                TokenFormat.CUSTOM_PROPERTY && (
                                <digi-code
                                  afVariation={CodeVariation.LIGHT}
                                  afCode={this.customPropertyValue(
                                    token.name
                                  )}></digi-code>
                              )}
                              {this._tokenFormat == TokenFormat.SASS && (
                                <digi-code
                                  afVariation={CodeVariation.LIGHT}
                                  afCode={this.scssValue(
                                    token.name
                                  )}></digi-code>
                              )}
                            </div>
                          </div>
                          <div class="digi-docs-color-token-cards__content-end">
                            <div class="digi-docs-color-token-cards__swatch-list">
                              {this._colorTokens.map((tokenVariation) => {
                                return (
                                  <digi-docs-color-swatch
                                    token={JSON.stringify(tokenVariation)}
                                    token-border={true}
                                    token-color-format="HEX"
                                    token-variation={
                                      DocsColorSwatchVariation.CIRCLE
                                    }
                                    token-selected={
                                      token.name == tokenVariation.name
                                    }></digi-docs-color-swatch>
                                );
                              })}
                            </div>
                          </div>
                        </div>
                        <div
                          slot="footer"
                          class="digi-docs-color-token-cards__footer"
                          style={{
                            '--TOKEN--FOOTER--COLOR': !this._isPrint
                              ? `var(${this.customPropertyValue(token.name)})`
                              : token.value
                          }}></div>
                      </digi-card>
                    ))}
              </Fragment>
            )}
          </div>
        </div>
      </host>
    );
  }
}
