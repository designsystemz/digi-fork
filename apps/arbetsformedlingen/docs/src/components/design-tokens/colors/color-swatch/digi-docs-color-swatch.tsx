import { Component, h, Prop, State, Watch } from '@stencil/core';
import { DocsColorSwatchVariation } from './digi-docs-color-swatch-variation.enum';
import invert from 'invert-color';
import { DocsColorSwatchSize } from './digi-docs-color-swatch-size.enum';

@Component({
  tag: 'digi-docs-color-swatch',
  styleUrl: 'digi-docs-color-swatch.scss',
  scoped: true
})
export class DigiDocsColorSwatch {
  @State() _token;
  private _textColor: string;
  private _borderColor: string;
  private _selectedColor: string;
  private _useHEX: boolean;

  /**
   * Design token
   */
  @Prop() token: string;
  @Watch('token')
  tokenChangeHandler() {
    this._token = JSON.parse(this.token);
  }

  @Prop() tokenText: string;

  /**
   * Color swatch variation
   */
  @Prop() tokenVariation: DocsColorSwatchVariation =
    DocsColorSwatchVariation.RECTANGLE;

  /**
   * Color swatch size
   */
  @Prop() tokenSize: DocsColorSwatchSize = DocsColorSwatchSize.M;

  /**
   * Show border
   */
  @Prop() tokenBorder = false;

  /**
   * Color swatch selected
   */
  @Prop() tokenSelected: boolean;

  @Prop() tokenColorFormat = '';

  componentWillLoad() {
    this._token = JSON.parse(this.token);
    this._useHEX = this.tokenColorFormat == 'HEX';
    this.setTextColor();
    this.setBorderColor();
    this.setSelectedColor();
  }

  setTextColor() {
    const textColor = invert(this._token.value, true);
    this._textColor =
      textColor === '#000000'
        ? 'var(--digi--color--text--primary)'
        : 'var(--digi--color--text--inverted)';
  }

  setBorderColor() {
    const borderColor = invert(this._token.value, true);
    this._borderColor =
      borderColor === '#000000'
        ? 'var(--digi--color--border--neutral-4)'
        : 'none';
  }

  setSelectedColor() {
    const selectedColor = invert(this._token.value, true);
    this._selectedColor =
      selectedColor === '#000000'
        ? 'var(--digi--color--text--primary)'
        : 'var(--digi--color--text--inverted)';
  }

  get customProperty() {
    return `--${this._token.name}`;
  }

  get scss() {
    return `$${this._token.name}`;
  }

  render() {
    return (
      <div
        class={{
          'digi-docs-color-swatch': true,
          'digi-docs-color-swatch--rectangle':
            this.tokenVariation === DocsColorSwatchVariation.RECTANGLE,
          'digi-docs-color-swatch--circle':
            this.tokenVariation === DocsColorSwatchVariation.CIRCLE,
          'digi-docs-color-swatch--full':
            this.tokenVariation === DocsColorSwatchVariation.FULL,
          'digi-docs-color-swatch--s': this.tokenSize === DocsColorSwatchSize.S,
          'digi-docs-color-swatch--m': this.tokenSize === DocsColorSwatchSize.M,
          'digi-docs-color-swatch--l': this.tokenSize === DocsColorSwatchSize.L,
          'digi-docs-color-swatch--selected': this.tokenSelected
        }}
        style={{
          '--digi-docs-color-swatch--color': this._textColor,
          '--digi-docs-color-swatch--background': !this._useHEX
            ? `var(${this.customProperty})`
            : this._token.value,
          '--digi-docs-color-swatch--border-color': this._borderColor,
          '--digi-docs-color-swatch--selected-color': this._selectedColor
        }}>
        <div class="digi-docs-color-swatch__color">
          <div>
            {this.tokenVariation == DocsColorSwatchVariation.FULL && (
              <span>{this._token.value}</span>
            )}
          </div>
        </div>
      </div>
    );
  }
}
