import { Component, h, Prop, State, Watch } from '@stencil/core';
import { ButtonVariation } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-token-copy',
  styleUrl: 'digi-docs-token-copy.scss',
  shadow: true
})
export class TokenCopy {
  ButtonVariation = ButtonVariation;
  @State() value: string;

  @Prop() afValue: string;
  @Watch('afValue')
  afValueChangeHandler() {
    this.value = this.afValue;
  }

  componentWillLoad() {
    this.afValueChangeHandler();
  }

  copyButtonClickHandler(e) {
    navigator.clipboard.writeText(e).then(
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      () => {},
      (err) => {
        console.error(err);
      }
    );
  }

  render() {
    return (
      <host>
        <div class="digi-docs-token-copy">
          <digi-button
            class="digi-docs-token-copy__button"
            afVariation={ButtonVariation.FUNCTION}
            onAfOnClick={() => this.copyButtonClickHandler(this.value)}>
            <digi-icon-copy aria-hidden="true" slot="icon"></digi-icon-copy>
            <span class="digi-docs-token-copy__a11y-sr-only">
              Kopiera token {this.value}
            </span>
          </digi-button>
        </div>
      </host>
    );
  }
}
