import {
  Component,
  getAssetPath,
  h,
  Prop,
  State,
  Element,
  Event,
  EventEmitter
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { UtilBreakpointObserverBreakpoints } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-accessibility-matrix',
  styleUrl: 'digi-docs-accessibility-matrix.scss',
  scoped: true
})
export class DigiDocsAccessibilityMatrix {
  private _button: HTMLButtonElement;

  @Prop() afExpanded = false;

  @State() isActive: boolean;
  @State() isMobile: boolean;
  @State() accordionArray: string[] = [];
  @Event() afOnClick: EventEmitter<MouseEvent>;
  @Element() hostElement: HTMLStencilElement;

  get cssModifiers() {
    return {
      [`digi-docs-accessibility-matrix__toggle-icon--expanded-${!!this.afExpanded}`]:
        true
    };
  }

  clickToggleHandler(e: MouseEvent, resetFocus = false, id: string) {
    e.preventDefault();
    this.afExpanded = !this.afExpanded;

    if (resetFocus) {
      this._button.focus();
    }

    if (this.accordionArray.includes(id)) {
      const index = this.accordionArray.indexOf(id);
      this.accordionArray.splice(index, 1);
      this.animateContentHeight(id, false);
    } else {
      this.accordionArray.push(id);
      this.animateContentHeight(id, true);
    }
    this.afOnClick.emit(e);
  }

  clickHandler(e) {
    e.target.nextElementSibling.setAttribute('af-show-dialog', true);
  }

  componentDidLoad() {
    const content = this.hostElement.children[0].querySelectorAll(
      '.digi-docs-accessibility-matrix__info'
    );
    content.forEach((item) => {
      item.setAttribute('style', 'visibility: hidden;');
    });
  }

  animateContentHeight(id: string, open: boolean) {
    const content = this.hostElement.children[0].querySelector(
      `#${id} .digi-docs-accessibility-matrix__info`
    ) as HTMLElement;
    const sectionHeight = (content.firstElementChild as HTMLElement)
      .offsetHeight;

    if (open) {
      content.setAttribute(
        'style',
        'visibility: visible; height:' + sectionHeight + 'px'
      );
    } else {
      content.setAttribute('style', 'visibility: hidden; height: ' + 0 + 'px');
    }
  }

  render() {
    return (
      <div class={'digi-docs-accessibility-matrix'}>
        <digi-util-breakpoint-observer
          onAfOnChange={(e) =>
            (this.isMobile =
              e.detail.value === UtilBreakpointObserverBreakpoints.SMALL ||
              e.detail.value === UtilBreakpointObserverBreakpoints.MEDIUM)
          }></digi-util-breakpoint-observer>
        {!this.isMobile && (
          <digi-layout-block afMarginBottom>
            <table class={'digi-docs-accessibility-matrix__table'}>
              <caption>Tillgänglighetsmatris</caption>
              <thead>
                <tr>
                  <th scope="col">
                    <span class={'digi--util--sr-only'}>Bild</span>
                  </th>
                  <th scope="col" id="Permanent">
                    Permanent
                  </th>
                  <th scope="col" id="Tillfällig">
                    Tillfällig
                  </th>
                  <th scope="col" id="Kontextuell">
                    Kontextuell
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row" id="Motorik">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/motorik-hand.svg'
                      )}
                      afAlt="Hand, illustration."></digi-media-image>
                    <button
                      type="button"
                      class="digi-docs-accessibility-matrix__button"
                      onClick={(e) =>
                        this.clickToggleHandler(e, false, 'Motorik-content')
                      }
                      aria-pressed={
                        this.accordionArray.includes('Motorik-content')
                          ? 'true'
                          : 'false'
                      }
                      aria-expanded={
                        this.accordionArray.includes('Motorik-content')
                          ? 'true'
                          : 'false'
                      }
                      aria-controls={`Motorik-content`}
                      ref={(el) => (this._button = el)}>
                      <digi-icon
                        afName={`chevron-down`}
                        class={{
                          'digi-docs-accessibility-matrix__toggle-icon': true,
                          'digi-docs-accessibility-matrix__toggle-icon--expanded-true':
                            this.accordionArray.includes('Motorik-content')
                        }}
                      />
                      <span>Motorik</span>
                    </button>
                  </th>
                  <td headers="Motorik Permanent">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/motorik-en-arm.svg'
                      )}
                      afAlt="Person som saknar en arm, illustration."></digi-media-image>
                    En arm
                  </td>
                  <td headers="Motorik Tillfällig">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/motorik-armbrott.svg'
                      )}
                      afAlt="Person med en arm som är bruten och bandagerad, illustration."></digi-media-image>
                    Armbrott
                  </td>
                  <td headers="Motorik Kontextuell">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/motorik-bar-pa-nagot.svg'
                      )}
                      afAlt="Person som bär en dator, illustration."></digi-media-image>
                    Bär på något
                  </td>
                </tr>

                <tr
                  id={'Motorik-content'}
                  class={
                    this.accordionArray.includes('Motorik-content')
                      ? 'digi-docs-accessibility-matrix__row--expanded'
                      : 'digi-docs-accessibility-matrix__row'
                  }>
                  <td
                    class={'digi-docs-accessibility-matrix__cell'}
                    headers="Motorik Permanent Info"
                    colSpan={4}>
                    <div class={'digi-docs-accessibility-matrix__info'}>
                      <div class={'digi-docs-accessibility-matrix__wrapper'}>
                        <div>
                          <h3 class={'digi-docs-accessibility-matrix__header'}>
                            Calle saknar en arm
                          </h3>
                          <p>
                            Calle är 25 år. Han bor i en mellanstor stad I
                            Sverige. Calle arbetar som optiker på ett medelstort
                            företag. När Calle var liten råkade han ut för en
                            olycka som gjorde att han behövde amputera en arm.
                            För Calle är teknik eller föremål som kräver mer än
                            en hand svår att använda. Som hjälpmedel på sin
                            smartphone använder Calle därför röstkommandon och
                            diktering för att skriva meddelanden och för att
                            söka efter information på Internet.
                          </p>
                          <h3 class={'digi-docs-accessibility-matrix__header'}>
                            Konstnären Lena Maria{' '}
                          </h3>
                          <p>
                            Lena Maria Vendelius är konstnär. Hon saknar armar
                            och hennes vänstra ben är hälften så långt som det
                            högra. Hon målar med mun och fötter. Utöver att vara
                            konstnär har Lena Maria även tävlat i simning och
                            deltagit i det svenska landslaget vid Paralymics i
                            Seoul i Sydkorea 1988.
                          </p>
                          <a
                            href="https://www.youtube.com/watch?v=AR6GPKxg-es"
                            target="_blank">
                            Se en film om Lena Maria Vendelius på Youtube
                          </a>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th scope="row" id="Syn">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/syn-oga.svg'
                      )}
                      afAlt="Öga, illustration."></digi-media-image>
                    <button
                      type="button"
                      class="digi-docs-accessibility-matrix__button"
                      onClick={(e) =>
                        this.clickToggleHandler(e, false, 'Syn-content')
                      }
                      aria-pressed={
                        this.accordionArray.includes('Syn-content')
                          ? 'true'
                          : 'false'
                      }
                      aria-expanded={
                        this.accordionArray.includes('Syn-content')
                          ? 'true'
                          : 'false'
                      }
                      aria-controls={`Syn-content`}
                      ref={(el) => (this._button = el)}>
                      <digi-icon
                        afName={`chevron-down`}
                        class={{
                          'digi-docs-accessibility-matrix__toggle-icon': true,
                          'digi-docs-accessibility-matrix__toggle-icon--expanded-true':
                            this.accordionArray.includes('Syn-content')
                        }}
                      />
                      <span>Syn</span>
                    </button>
                  </th>
                  <td headers="Syn Permanent">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/syn-synskadad.svg'
                      )}
                      afAlt="Person med ett par stora glasögon, illustration."></digi-media-image>
                    Synskadad
                  </td>
                  <td headers="Syn Tillfällig">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/syn-starr.svg'
                      )}
                      afAlt="Person med öppna och lite grumliga ögon, illustration."></digi-media-image>
                    Starr
                  </td>
                  <td headers="Syn Kontextuell">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/syn-sol-pa-skarm.svg'
                      )}
                      afAlt="Person med solen bakom axeln, illustration."></digi-media-image>
                    Sol på skärmen
                  </td>
                </tr>
                <tr
                  id={'Syn-content'}
                  class={
                    this.accordionArray.includes('Syn-content')
                      ? 'digi-docs-accessibility-matrix__row--expanded'
                      : 'digi-docs-accessibility-matrix__row'
                  }>
                  <td headers="Motorik Permanent Info" colSpan={4}>
                    <div class={'digi-docs-accessibility-matrix__info'}>
                      <div class={'digi-docs-accessibility-matrix__wrapper'}>
                        <div>
                          <h3 class={'digi-docs-accessibility-matrix__header'}>
                            Adrian förlorade sin syn som barn
                          </h3>
                          <p>
                            Adrian är 29 år och arbetar som
                            kundtjänstmedarbetare på ett stort företag. Efter en
                            svår ögoninfektion som barn förlorade Adrian sin
                            syn. Han är aktiv och gillar att sporta på sin
                            fritid. På sitt arbete använder Adrian en dator med
                            skärmläsare och punktdisplay för att läsa text som
                            visas på skärmen. Det gör att det tar längre tid för
                            honom än för andra att hitta information på en
                            webbplats.
                          </p>
                          <h3 class={'digi-docs-accessibility-matrix__header'}>
                            Journalisten Anna
                          </h3>
                          <p>
                            Anna Bergholtz förlorade sin syn helt när hon var 24
                            år på grund av en reumatisk sjukdom. Idag är Anna
                            utbildad journalist och har bland annat jobbat för
                            Sveriges Radio. Hon gör även poddar och tar uppdrag
                            som moderator och föreläsare. Anna har en ledarhund
                            som hjälper henne att orientera sig och undvika
                            hinder när hon rör sig i stadsmiljöer eller är ute
                            och reser.
                          </p>
                          <a
                            href="https://www.youtube.com/watch?v=k6kk6urAuS4"
                            target="_blank">
                            Se Anna i filmen Art of Welcoming på Youtube
                          </a>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th scope="row" id="Hörsel">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/horsel-ora.svg'
                      )}
                      afAlt="Öra, illustration."></digi-media-image>
                    <button
                      type="button"
                      class="digi-docs-accessibility-matrix__button"
                      onClick={(e) =>
                        this.clickToggleHandler(e, false, 'Hörsel-content')
                      }
                      aria-pressed={this.afExpanded ? 'true' : 'false'}
                      aria-expanded={this.afExpanded ? 'true' : 'false'}
                      aria-controls={`Hörsel-content`}
                      ref={(el) => (this._button = el)}>
                      <digi-icon
                        afName={`chevron-down`}
                        class={{
                          'digi-docs-accessibility-matrix__toggle-icon': true,
                          'digi-docs-accessibility-matrix__toggle-icon--expanded-true':
                            this.accordionArray.includes('Hörsel-content')
                        }}
                      />
                      <span>Hörsel</span>
                    </button>
                  </th>
                  <td headers="Hörsel Permanent">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/horsel-dov.svg'
                      )}
                      afAlt="Person med dolda öron, illustration."></digi-media-image>
                    Döv
                  </td>
                  <td headers="Hörsel Tillfällig">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/horsel-oroninflammation.svg'
                      )}
                      afAlt="Person med lite ömma öron , illustration."></digi-media-image>
                    Öron&shy;inflammation
                  </td>
                  <td headers="Hörsel Kontextuell">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/horsel-buller.svg'
                      )}
                      afAlt="Person med buller runt öronen, illustration."></digi-media-image>
                    Buller/&shy;Hörlurar
                  </td>
                </tr>
                <tr
                  id={'Hörsel-content'}
                  class={
                    this.accordionArray.includes('Hörsel-content')
                      ? 'digi-docs-accessibility-matrix__row--expanded'
                      : 'digi-docs-accessibility-matrix__row'
                  }>
                  <td headers="Motorik Permanent Info" colSpan={4}>
                    <div class={'digi-docs-accessibility-matrix__info'}>
                      <div class={'digi-docs-accessibility-matrix__wrapper'}>
                        <div>
                          <h3 class={'digi-docs-accessibility-matrix__header'}>
                            Ida kommunicerar med teckenspråk
                          </h3>
                          <p>
                            Ida är 23 år och bor i en medelstor stad i norra
                            Sverige. Ida arbetar som fritidsledare på en
                            mellanstadieskola. Vid större folksamlingar och i
                            digitala möten är det svårt för Ida att uppfatta vad
                            en person säger. Hon kan läsa på läppar men har även
                            hjälpmedel som underlättar i vardagen. Det finns
                            även hörslingor i skolans lokaler som gör det
                            möjligt för Ida att lättare höra när någon talar
                            eller sjunger i en mikrofon.
                          </p>
                          <h3 class={'digi-docs-accessibility-matrix__header'}>
                            Talaren och konsulten Molly
                          </h3>
                          <p>
                            Molly Watt är talare inom tillgänglighet,
                            funktionsnedsättningar och universell design som
                            bland annat har talat inför det brittiska
                            parlamentet och Harvards läkarhögskola. Hon har en
                            ovanlig diagnos som heter Usher Syndrome som
                            påverkar både hörsel och syn. Förutom att hålla
                            föredrag tar Molly även uppdrag som konsult och
                            skriver barnböcker. Hon använder hörapparat och
                            andra hörhjälpmedel som hon kan styra med sin
                            smartphone.
                          </p>
                          <a
                            href="https://www.youtube.com/watch?v=YMQK1uKghcg"
                            target="_blank">
                            Se filmen “Have you?” med Molly Watt på Youtube
                          </a>
                          <br />
                          <a
                            href="https://www.youtube.com/watch?v=6s6REK0bWqw"
                            target="_blank">
                            Se nyhetsinslag om hörhjälpmedel och smartphones på
                            Youtube
                          </a>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th scope="row" id="Tal">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/tal-mun.svg'
                      )}
                      afAlt="Mun, illustration."></digi-media-image>
                    <button
                      type="button"
                      class="digi-docs-accessibility-matrix__button"
                      onClick={(e) =>
                        this.clickToggleHandler(e, false, 'Tal-content')
                      }
                      aria-pressed={this.afExpanded ? 'true' : 'false'}
                      aria-expanded={this.afExpanded ? 'true' : 'false'}
                      aria-controls={`Tal-content`}
                      ref={(el) => (this._button = el)}>
                      <digi-icon
                        afName={`chevron-down`}
                        class={{
                          'digi-docs-accessibility-matrix__toggle-icon': true,
                          'digi-docs-accessibility-matrix__toggle-icon--expanded-true':
                            this.accordionArray.includes('Tal-content')
                        }}
                      />
                      <span>Tal</span>
                    </button>
                  </th>
                  <td headers="Tal Permanent">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/tal-stum.svg'
                      )}
                      afAlt="Person med något dold mun, illustration."></digi-media-image>
                    Språkstörning
                  </td>
                  <td headers="Tal Tillfällig">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/tal-strupkatarr.svg'
                      )}
                      afAlt="Person med öm hals, illustration."></digi-media-image>
                    Strupkatarr
                  </td>
                  <td headers="Tal Kontextuell">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/tal-tandvark.svg'
                      )}
                      afAlt="Person med smärta och ömhet i munnen, illustration."></digi-media-image>
                    Tandvärk
                  </td>
                </tr>
                <tr
                  id={'Tal-content'}
                  class={
                    this.accordionArray.includes('Tal-content')
                      ? 'digi-docs-accessibility-matrix__row--expanded'
                      : 'digi-docs-accessibility-matrix__row'
                  }>
                  <td headers="Motorik Permanent Info" colSpan={4}>
                    <div class={'digi-docs-accessibility-matrix__info'}>
                      <div class={'digi-docs-accessibility-matrix__wrapper'}>
                        <div>
                          <h3 class={'digi-docs-accessibility-matrix__header'}>
                            Henrik
                          </h3>
                          <p>
                            Henrik är 52 år och bor i en mellanstor stad i
                            västra Sverige. Han arbetar som låssmed och är stum
                            sedan födseln. Henrik använder teckenspråk för att
                            kommunicera vilket ofta är svårt om personer i hans
                            omgivning inte förstår vad han försöker säga. Dagens
                            goda uppkopplingsmöjligheter gör att Henrik kan
                            ringa videosamtal på sin smartphone. Vid kontakt med
                            myndigheter och vård använder han texttelefoni eller
                            e-post men det är inte alltid möjligt.
                          </p>
                          <h3 class={'digi-docs-accessibility-matrix__header'}>
                            Fysikern Stephen
                          </h3>
                          <p>
                            Den berömde fysikern Stephen Hawking fick diagnosen
                            ALS när han var 19 år och gick bort 2018. Han var
                            också känd som forskare av svarta hål och som
                            författare i populärvetenskap. I ett tidigt skede
                            förlorade Stephen förmågan att tala. Han hade en
                            dator monterad på sin rullstol för att kunna
                            kommunicera med omvärlden. Med en switch kunde han
                            välja bland hundratusentals ord och fraser. Han ska
                            också ha sagt att datorn gjorde det möjligt för
                            honom att kommunicera bättre än vad han någonsin
                            kunde med tal.
                          </p>
                          <a
                            href="https://www.youtube.com/watch?v=OTmPw4iy0hk"
                            target="_blank">
                            Se filmen "Stephen Hawking's voice and the machine
                            that powers it" på Youtube
                          </a>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th scope="row" id="Kognition">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/kognitiv-hjarna.svg'
                      )}
                      afAlt="Hjärna, illustration"></digi-media-image>
                    <button
                      type="button"
                      class="digi-docs-accessibility-matrix__button"
                      onClick={(e) =>
                        this.clickToggleHandler(e, false, 'Kognition-content')
                      }
                      aria-pressed={
                        this.accordionArray.includes('Kognition-content')
                          ? 'true'
                          : 'false'
                      }
                      aria-expanded={
                        this.accordionArray.includes('Kognition-content')
                          ? 'true'
                          : 'false'
                      }
                      aria-controls={`Kognition-content`}
                      ref={(el) => (this._button = el)}>
                      <digi-icon
                        afName={`chevron-down`}
                        class={{
                          'digi-docs-accessibility-matrix__toggle-icon': true,
                          'digi-docs-accessibility-matrix__toggle-icon--expanded-true':
                            this.accordionArray.includes('Kognition-content')
                        }}
                      />
                      <span>Kognition</span>
                    </button>
                  </th>
                  <td headers="Kognition Permanent">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/kognitiv-adhd.svg'
                      )}
                      afAlt="Person med ett brus runt huvudet, illustration."></digi-media-image>
                    ADHD
                  </td>
                  <td headers="Kognition Tillfällig">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/kognitiv-depression.svg'
                      )}
                      afAlt="Person med ena halvan av ansiktet något dolt, illustration."></digi-media-image>
                    Hjärntrötthet
                  </td>
                  <td headers="Kognition Kontextuell">
                    <digi-media-image
                      af-height="140"
                      af-width="140"
                      afUnlazy
                      af-src={getAssetPath(
                        '/assets/icon/accessibility/kognitiv-kontorslandskap.svg'
                      )}
                      afAlt="Person med stimmigt ljud från ett kontorslandskap, illustration."></digi-media-image>
                    Kontorslandskap
                  </td>
                </tr>
                <tr
                  id={'Kognition-content'}
                  class={
                    this.accordionArray.includes('Kognition-content')
                      ? 'digi-docs-accessibility-matrix__row--expanded'
                      : 'digi-docs-accessibility-matrix__row'
                  }>
                  <td headers="Motorik Permanent Info" colSpan={4}>
                    <div class={'digi-docs-accessibility-matrix__info'}>
                      <div class={'digi-docs-accessibility-matrix__wrapper'}>
                        <div>
                          <h3 class={'digi-docs-accessibility-matrix__header'}>
                            Majken har ADHD
                          </h3>
                          <p>
                            Majken är 32 år och bor i en liten stad i mellersta
                            Sverige. Majken arbetar som personlig tränare och
                            hennes stora intressen är träning och hälsa. Hon har
                            ADHD vilket gör att hon har svårt att koncentrera
                            sig och att avsluta saker. Det är också jobbigt för
                            henne att sitta still långa stunder, exempelvis vid
                            längre föredrag. Majken använder sig av en klocka
                            med en visuell räknare som visar hur lång tid som
                            återstår av en uppgift eller ett föredrag. Hon får
                            också påminnelser i sin smartphone om inbokade
                            händelser i sin kalender och om när hon behöver gå
                            för att komma fram i tid.
                          </p>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </digi-layout-block>
        )}

        {/*   Mobil Content    */}

        {this.isMobile && (
          <digi-layout-block afMarginBottom>
            <div class={'digi-docs-accessibility-matrix__mobile'}>
              <digi-layout-media-object af-alignment="center">
                <digi-media-image
                  af-height="72"
                  af-width="72"
                  afUnlazy
                  af-src={getAssetPath(
                    '/assets/icon/accessibility/motorik-hand.svg'
                  )}
                  afAlt="Hand, illustration."
                  slot="media"></digi-media-image>
                <button
                  type="button"
                  class="digi-docs-accessibility-matrix__button"
                  onClick={(e) =>
                    this.clickToggleHandler(e, false, 'Motorik-mobil-content')
                  }
                  aria-pressed={
                    this.accordionArray.includes('Motorik-mobil-content')
                      ? 'true'
                      : 'false'
                  }
                  aria-expanded={
                    this.accordionArray.includes('Motorik-mobil-content')
                      ? 'true'
                      : 'false'
                  }
                  aria-controls={`Motorik-mobil-content`}
                  ref={(el) => (this._button = el)}>
                  <h2>Motorik</h2>
                  <digi-icon
                    afName={`chevron-down`}
                    class={{
                      'digi-docs-accessibility-matrix__toggle-icon': true,
                      'digi-docs-accessibility-matrix__toggle-icon--expanded-true':
                        this.accordionArray.includes('Motorik-mobil-content')
                    }}
                  />
                </button>
              </digi-layout-media-object>
              <div
                id={'Motorik-mobil-content'}
                class={
                  this.accordionArray.includes('Motorik-mobil-content')
                    ? 'digi-docs-accessibility-matrix__row--expanded'
                    : 'digi-docs-accessibility-matrix__row'
                }>
                <div>
                  <div class={'digi-docs-accessibility-matrix__info'}>
                    <div class={'digi-docs-accessibility-matrix__wrapper'}>
                      <div class={'digi-docs-accessibility-matrix__content'}>
                        <h3 class={'digi-docs-accessibility-matrix__header'}>
                          Calle saknar en arm
                        </h3>
                        <p>
                          Calle är 25 år. Han bor i en mellanstor stad I
                          Sverige. Calle arbetar som optiker på ett medelstort
                          företag. När Calle var liten råkade han ut för en
                          olycka som gjorde att han behövde amputera en arm. För
                          Calle är teknik eller föremål som kräver mer än en
                          hand svår att använda. Som hjälpmedel på sin
                          smartphone använder Calle därför röstkommandon och
                          diktering för att skriva meddelanden och för att söka
                          efter information på Internet.
                        </p>
                        <h3 class={'digi-docs-accessibility-matrix__header'}>
                          Konstnären Lena Maria{' '}
                        </h3>
                        <p>
                          Lena Maria Vendelius är konstnär. Hon saknar armar och
                          hennes vänstra ben är hälften så långt som det högra.
                          Hon målar med mun och fötter. Utöver att vara konstnär
                          har Lena Maria även tävlat i simning och deltagit i
                          det svenska landslaget vid Paralymics i Seoul i
                          Sydkorea 1988.
                        </p>
                        <a
                          href="https://www.youtube.com/watch?v=AR6GPKxg-es"
                          target="_blank">
                          Se en film om Lena Maria Vendelius på Youtube
                        </a>
                      </div>
                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/motorik-en-arm.svg'
                            )}
                            afAlt="Person som saknar en arm, illustration."></digi-media-image>
                          <div>
                            <h3>Permanent</h3>
                            <p>En arm</p>
                          </div>
                        </digi-typography>
                      </div>

                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/motorik-armbrott.svg'
                            )}
                            afAlt="Person med en arm som är bruten och bandagerad, illustration."></digi-media-image>
                          <div>
                            <h3>Tillfällig</h3>
                            <p>Armbrott</p>
                          </div>
                        </digi-typography>
                      </div>

                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/motorik-bar-pa-nagot.svg'
                            )}
                            afAlt="Person som bär en dator, illustration."></digi-media-image>
                          <div>
                            <h3>Kontextuell</h3>
                            <p>Bär på något</p>
                          </div>
                        </digi-typography>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <digi-layout-media-object af-alignment="center">
                <digi-media-image
                  af-height="72"
                  af-width="72"
                  afUnlazy
                  af-src={getAssetPath(
                    '/assets/icon/accessibility/syn-oga.svg'
                  )}
                  afAlt="Öga, illustration."
                  slot="media"></digi-media-image>
                <button
                  type="button"
                  class="digi-docs-accessibility-matrix__button"
                  onClick={(e) =>
                    this.clickToggleHandler(e, false, 'Syn-mobil-content')
                  }
                  aria-pressed={
                    this.accordionArray.includes('Syn-mobil-content')
                      ? 'true'
                      : 'false'
                  }
                  aria-expanded={
                    this.accordionArray.includes('Syn-mobil-content')
                      ? 'true'
                      : 'false'
                  }
                  aria-controls={`Syn-mobil-content`}
                  ref={(el) => (this._button = el)}>
                  <h2>Syn</h2>
                  <digi-icon
                    afName={`chevron-down`}
                    class={{
                      'digi-docs-accessibility-matrix__toggle-icon': true,
                      'digi-docs-accessibility-matrix__toggle-icon--expanded-true':
                        this.accordionArray.includes('Syn-mobil-content')
                    }}
                  />
                </button>
              </digi-layout-media-object>

              <div
                id={'Syn-mobil-content'}
                class={
                  this.accordionArray.includes('Syn-mobil-content')
                    ? 'digi-docs-accessibility-matrix__row--expanded'
                    : 'digi-docs-accessibility-matrix__row'
                }>
                <div>
                  <div class={'digi-docs-accessibility-matrix__info'}>
                    <div class={'digi-docs-accessibility-matrix__wrapper'}>
                      <div class={'digi-docs-accessibility-matrix__content'}>
                        <h3 class={'digi-docs-accessibility-matrix__header'}>
                          Adrian förlorade sin syn som barn
                        </h3>
                        <p>
                          Adrian är 29 år och arbetar som kundtjänstmedarbetare
                          på ett stort företag. Efter en svår ögoninfektion som
                          barn förlorade Adrian sin syn. Han är aktiv och gillar
                          att sporta på sin fritid. På sitt arbete använder
                          Adrian en dator med skärmläsare och punktdisplay för
                          att läsa text som visas på skärmen. Det gör att det
                          tar längre tid för honom än för andra att hitta
                          information på en webbplats.
                        </p>
                        <h3 class={'digi-docs-accessibility-matrix__header'}>
                          Journalisten Anna
                        </h3>
                        <p>
                          Anna Bergholtz förlorade sin syn helt när hon var 24
                          år på grund av en reumatisk sjukdom. Idag är Anna
                          utbildad journalist och har bland annat jobbat för
                          Sveriges Radio. Hon gör även poddar och tar uppdrag
                          som moderator och föreläsare. Anna har en ledarhund
                          som hjälper henne att orientera sig och undvika hinder
                          när hon rör sig i stadsmiljöer eller är ute och reser.
                        </p>
                        <a
                          href="https://www.youtube.com/watch?v=k6kk6urAuS4"
                          target="_blank">
                          Se Anna i filmen Art of Welcoming på Youtube
                        </a>
                      </div>
                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/syn-synskadad.svg'
                            )}
                            afAlt="Person med ett par stora glasögon, illustration."></digi-media-image>
                          <div>
                            <h3>Permanent</h3>
                            <p>Synskadad</p>
                          </div>
                        </digi-typography>
                      </div>

                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/syn-starr.svg'
                            )}
                            afAlt="Person med öppna och lite grumliga ögon, illustration."></digi-media-image>
                          <div>
                            <h3>Tillfällig</h3>
                            <p>Starr</p>
                          </div>
                        </digi-typography>
                      </div>

                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/syn-sol-pa-skarm.svg'
                            )}
                            afAlt="Person med solen bakom axeln, illustration."></digi-media-image>
                          <div>
                            <h3>Kontextuell</h3>
                            <p>Sol på skärmen</p>
                          </div>
                        </digi-typography>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <digi-layout-media-object af-alignment="center">
                <digi-media-image
                  af-height="72"
                  af-width="72"
                  afUnlazy
                  af-src={getAssetPath(
                    '/assets/icon/accessibility/horsel-ora.svg'
                  )}
                  afAlt="Öra, illustration."
                  slot="media"></digi-media-image>
                <button
                  type="button"
                  class="digi-docs-accessibility-matrix__button"
                  onClick={(e) =>
                    this.clickToggleHandler(e, false, 'Horsel-mobil-content')
                  }
                  aria-pressed={
                    this.accordionArray.includes('Horsel-mobil-content')
                      ? 'true'
                      : 'false'
                  }
                  aria-expanded={
                    this.accordionArray.includes('Horsel-mobil-content')
                      ? 'true'
                      : 'false'
                  }
                  aria-controls={`Horsel-mobil-content`}
                  ref={(el) => (this._button = el)}>
                  <h2>Hörsel</h2>
                  <digi-icon
                    afName={`chevron-down`}
                    class={{
                      'digi-docs-accessibility-matrix__toggle-icon': true,
                      'digi-docs-accessibility-matrix__toggle-icon--expanded-true':
                        this.accordionArray.includes('Horsel-mobil-content')
                    }}
                  />
                </button>
              </digi-layout-media-object>

              <div
                id={'Horsel-mobil-content'}
                class={
                  this.accordionArray.includes('Horsel-mobil-content')
                    ? 'digi-docs-accessibility-matrix__row--expanded'
                    : 'digi-docs-accessibility-matrix__row'
                }>
                <div>
                  <div class={'digi-docs-accessibility-matrix__info'}>
                    <div class={'digi-docs-accessibility-matrix__wrapper'}>
                      <div class={'digi-docs-accessibility-matrix__content'}>
                        <h3 class={'digi-docs-accessibility-matrix__header'}>
                          Ida kommunicerar med teckenspråk
                        </h3>
                        <p>
                          Ida är 23 år och bor i en medelstor stad i norra
                          Sverige. Ida arbetar som fritidsledare på en
                          mellanstadieskola. Vid större folksamlingar och i
                          digitala möten är det svårt för Ida att uppfatta vad
                          en person säger. Hon kan läsa på läppar men har även
                          hjälpmedel som underlättar i vardagen. Det finns även
                          hörslingor i skolans lokaler som gör det möjligt för
                          Ida att lättare höra när någon talar eller sjunger i
                          en mikrofon.
                        </p>
                        <h3 class={'digi-docs-accessibility-matrix__header'}>
                          Talaren och konsulten Molly
                        </h3>
                        <p>
                          Molly Watt är talare inom tillgänglighet,
                          funktionsnedsättningar och universell design som bland
                          annat har talat inför det brittiska parlamentet och
                          Harvards läkarhögskola. Hon har en ovanlig diagnos som
                          heter Usher Syndrome som påverkar både hörsel och syn.
                          Förutom att hålla föredrag tar Molly även uppdrag som
                          konsult och skriver barnböcker. Hon använder
                          hörapparat och andra hörhjälpmedel som hon kan styra
                          med sin smartphone.
                        </p>
                        <a
                          href="https://www.youtube.com/watch?v=YMQK1uKghcg"
                          target="_blank">
                          Se filmen “Have you?” med Molly Watt på Youtube
                        </a>
                        <br />
                        <a
                          href="https://www.youtube.com/watch?v=6s6REK0bWqw"
                          target="_blank">
                          Se nyhetsinslag om hörhjälpmedel och smartphones på
                          Youtube
                        </a>
                      </div>
                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/horsel-dov.svg'
                            )}
                            afAlt="Person med dolda öron, illustration."></digi-media-image>
                          <div>
                            <h3>Permanent</h3>
                            <p>Döv</p>
                          </div>
                        </digi-typography>
                      </div>

                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/horsel-oroninflammation.svg'
                            )}
                            afAlt="Person med lite ömma öron , illustration."></digi-media-image>
                          <div>
                            <h3>Tillfällig</h3>
                            <p>Öron­inflammation</p>
                          </div>
                        </digi-typography>
                      </div>

                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/horsel-buller.svg'
                            )}
                            afAlt="Person med buller runt öronen, illustration."></digi-media-image>
                          <div>
                            <h3>Kontextuell</h3>
                            <p>Buller/­Hörlurar</p>
                          </div>
                        </digi-typography>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <digi-layout-media-object af-alignment="center">
                <digi-media-image
                  af-height="72"
                  af-width="72"
                  afUnlazy
                  af-src={getAssetPath(
                    '/assets/icon/accessibility/tal-mun.svg'
                  )}
                  afAlt="Mun, illustration"
                  slot="media"></digi-media-image>
                <button
                  type="button"
                  class="digi-docs-accessibility-matrix__button"
                  onClick={(e) =>
                    this.clickToggleHandler(e, false, 'Tal-mobil-content')
                  }
                  aria-pressed={
                    this.accordionArray.includes('Tal-mobil-content')
                      ? 'true'
                      : 'false'
                  }
                  aria-expanded={
                    this.accordionArray.includes('Tal-mobil-content')
                      ? 'true'
                      : 'false'
                  }
                  aria-controls={`Tal-mobil-content`}
                  ref={(el) => (this._button = el)}>
                  <h2>Tal</h2>
                  <digi-icon
                    afName={`chevron-down`}
                    class={{
                      'digi-docs-accessibility-matrix__toggle-icon': true,
                      'digi-docs-accessibility-matrix__toggle-icon--expanded-true':
                        this.accordionArray.includes('Tal-mobil-content')
                    }}
                  />
                </button>
              </digi-layout-media-object>

              <div
                id={'Tal-mobil-content'}
                class={
                  this.accordionArray.includes('Tal-mobil-content')
                    ? 'digi-docs-accessibility-matrix__row--expanded'
                    : 'digi-docs-accessibility-matrix__row'
                }>
                <div>
                  <div class={'digi-docs-accessibility-matrix__info'}>
                    <div class={'digi-docs-accessibility-matrix__wrapper'}>
                      <div class={'digi-docs-accessibility-matrix__content'}>
                        <h3 class={'digi-docs-accessibility-matrix__header'}>
                          Henrik
                        </h3>
                        <p>
                          Henrik är 52 år och bor i en mellanstor stad i västra
                          Sverige. Han arbetar som låssmed och är stum sedan
                          födseln. Henrik använder teckenspråk för att
                          kommunicera vilket ofta är svårt om personer i hans
                          omgivning inte förstår vad han försöker säga. Dagens
                          goda uppkopplingsmöjligheter gör att Henrik kan ringa
                          videosamtal på sin smartphone. Vid kontakt med
                          myndigheter och vård använder han texttelefoni eller
                          e-post men det är inte alltid möjligt.
                        </p>
                        <h3 class={'digi-docs-accessibility-matrix__header'}>
                          Fysikern Stephen
                        </h3>
                        <p>
                          Den berömde fysikern Stephen Hawking fick diagnosen
                          ALS när han var 19 år och gick bort 2018. Han var
                          också känd som forskare av svarta hål och som
                          författare i populärvetenskap. I ett tidigt skede
                          förlorade Stephen förmågan att tala. Han hade en dator
                          monterad på sin rullstol för att kunna kommunicera med
                          omvärlden. Med en switch kunde han välja bland
                          hundratusentals ord och fraser. Han ska också ha sagt
                          att datorn gjorde det möjligt för honom att
                          kommunicera bättre än vad han någonsin kunde med tal.
                        </p>
                        <a
                          href="https://www.youtube.com/watch?v=OTmPw4iy0hk"
                          target="_blank">
                          Se filmen "Stephen Hawking's voice and the machine
                          that powers it" på Youtube
                        </a>
                      </div>
                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/tal-stum.svg'
                            )}
                            afAlt="Person med något dold mun, illustration."></digi-media-image>
                          <div>
                            <h3>Permanent</h3>
                            <p>Språkstörning</p>
                          </div>
                        </digi-typography>
                      </div>

                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/tal-strupkatarr.svg'
                            )}
                            afAlt="Person med öm hals, illustration."></digi-media-image>
                          <div>
                            <h3>Tillfällig</h3>
                            <p>Strupkatarr</p>
                          </div>
                        </digi-typography>
                      </div>

                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/tal-tandvark.svg'
                            )}
                            afAlt="Person med smärta och ömhet i munnen, illustration."></digi-media-image>
                          <div>
                            <h3>Kontextuell</h3>
                            <p>Tandvärk</p>
                          </div>
                        </digi-typography>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <digi-layout-media-object af-alignment="center">
                <digi-media-image
                  af-height="72"
                  af-width="72"
                  afUnlazy
                  af-src={getAssetPath(
                    '/assets/icon/accessibility/kognitiv-hjarna.svg'
                  )}
                  afAlt="Hjärna, illustration."
                  slot="media"></digi-media-image>
                <button
                  type="button"
                  class="digi-docs-accessibility-matrix__button"
                  onClick={(e) =>
                    this.clickToggleHandler(e, false, 'Kognition-mobil-content')
                  }
                  aria-pressed={
                    this.accordionArray.includes('Kognition-mobil-content')
                      ? 'true'
                      : 'false'
                  }
                  aria-expanded={
                    this.accordionArray.includes('Kognition-mobil-content')
                      ? 'true'
                      : 'false'
                  }
                  aria-controls={`Kognition-mobil-content`}
                  ref={(el) => (this._button = el)}>
                  <h2>Kognition</h2>
                  <digi-icon
                    afName={`chevron-down`}
                    class={{
                      'digi-docs-accessibility-matrix__toggle-icon': true,
                      'digi-docs-accessibility-matrix__toggle-icon--expanded-true':
                        this.accordionArray.includes('Kognition-mobil-content')
                    }}
                  />
                </button>
              </digi-layout-media-object>

              <div
                id={'Kognition-mobil-content'}
                class={
                  this.accordionArray.includes('Kognition-mobil-content')
                    ? 'digi-docs-accessibility-matrix__row--expanded'
                    : 'digi-docs-accessibility-matrix__row'
                }>
                <div>
                  <div class={'digi-docs-accessibility-matrix__info'}>
                    <div class={'digi-docs-accessibility-matrix__wrapper'}>
                      <div class={'digi-docs-accessibility-matrix__content'}>
                        <h3 class={'digi-docs-accessibility-matrix__header'}>
                          Majken har ADHD
                        </h3>
                        <p>
                          Majken är 32 år och bor i en liten stad i mellersta
                          Sverige. Majken arbetar som personlig tränare och
                          hennes stora intressen är träning och hälsa. Hon har
                          ADHD vilket gör att hon har svårt att koncentrera sig
                          och att avsluta saker. Det är också jobbigt för henne
                          att sitta still långa stunder, exempelvis vid längre
                          föredrag. Majken använder sig av en klocka med en
                          visuell räknare som visar hur lång tid som återstår av
                          en uppgift eller ett föredrag. Hon får också
                          påminnelser i sin smartphone om inbokade händelser i
                          sin kalender och om när hon behöver gå för att komma
                          fram i tid.
                        </p>
                      </div>
                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/kognitiv-adhd.svg'
                            )}
                            afAlt="Person med ett brus runt huvudet, illustration."></digi-media-image>
                          <div>
                            <h3>Permanent</h3>
                            <p>ADHD</p>
                          </div>
                        </digi-typography>
                      </div>

                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/kognitiv-depression.svg'
                            )}
                            afAlt="Person med ena halvan av ansiktet något dolt, illustration."></digi-media-image>
                          <div>
                            <h3>Tillfällig</h3>
                            <p>Hjärntrötthet</p>
                          </div>
                        </digi-typography>
                      </div>

                      <div class={'digi-docs-accessibility-matrix__example'}>
                        <digi-typography>
                          <digi-media-image
                            af-height="92"
                            af-width="60"
                            afUnlazy
                            af-src={getAssetPath(
                              '/assets/icon/accessibility/kognitiv-kontorslandskap.svg'
                            )}
                            afAlt="Person med stimmigt ljud från ett kontorslandskap, illustration."></digi-media-image>
                          <div>
                            <h3>Kontextuell</h3>
                            <p>Kontorslandskap</p>
                          </div>
                        </digi-typography>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </digi-layout-block>
        )}
      </div>
    );
  }
}
