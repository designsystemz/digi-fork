import { Route } from '@angular/router';

import { ArticleDemoComponent } from './components/article-demo/article-demo.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { ChartsComponent } from './components/charts/charts.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { FormComponent } from './components/form/form.component';
import { GridComponent } from './components/grid/grid.component';
import { HomeComponent } from './components/home/home.component';
import { LanguagepickerComponent } from './components/languagepicker/languagepicker.component';
import { LinkButtonsComponent } from './components/link-buttons/link-buttons.component';
import { LinksComponent } from './components/links/links.component';
import { MultifilterPocComponent } from './components/multifilter-poc/multifilter-poc.component';
import { NavigationPageComponent } from './components/navigation-demo/navigation-page/navigation-page.component';
import { NyaIkonerComponent } from './components/nya-ikoner/nya-ikoner.component';
import { FormReceiptComponent } from './components/receipt/receipt.component';
import { StepsComponent } from './components/steps/steps.component';
import { TimepickerComponent } from './components/timepicker/timepicker.component';
import { ToolsLanguagepickerComponent } from './components/tools-languagepicker/tools-languagepicker.component';
import { ValidationComponent } from './components/validering/validation.component';
import { WheelOfFortuneComponent } from './components/wheel-of-fortune/wheel-of-fortune.component';
import { ForloppsstegarenComponent } from './components/forloppsstegare/forloppsstegaren.component';
import { DatepickerValueAccessorComponent } from './components/datepicker-value-accessor/datepicker-value-accessor.component';

const appRoutes: Route[] = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'article',
    component: ArticleDemoComponent
  },
  {
    path: 'form',
    component: FormComponent
  },
  {
    path: 'timepicker',
    component: TimepickerComponent,
    data: {
      fullwidth: true,
      hideSidenav: true
    }
  },
  {
    path: 'grid',
    component: GridComponent,
    data: {
      fullwidth: true,
      hideSidenav: true
    }
  },
  {
    path: 'steps',
    component: StepsComponent
  },
  {
    path: 'navigation',
    component: NavigationPageComponent,
    data: {
      sidenavDemo: true
    }
  },
  {
    path: 'fileupload',
    component: FileUploadComponent,
    data: {
      sidenavDemo: false
    }
  },
  {
    path: 'calendar',
    component: CalendarComponent,
    data: {
      sidenavDemo: true
    }
  },
  {
    path: 'links',
    component: LinksComponent,
    data: {
      sidenavDemo: false
    }
  },
  {
    path: 'languagepicker',
    component: LanguagepickerComponent,
    data: {
      fullwidth: true,
      hideSidenav: true
    }
  },
  {
    path: 'receipt',
    component: FormReceiptComponent,
    data: {
      fullwidth: true,
      hideSidenav: true
    }
  },
  {
    path: 'validering',
    component: ValidationComponent,
    data: {
      fullwidth: false,
      hideSidenav: true
    }
  },
  {
    path: 'link-buttons',
    component: LinkButtonsComponent,
    data: {
      fullwidth: false,
      hideSidenav: true
    }
  },
  {
    path: 'charts',
    component: ChartsComponent
  },
  {
    path: 'tools-languagepicker',
    component: ToolsLanguagepickerComponent
  },
  {
    path: 'charts',
    component: ChartsComponent
  },
  {
    path: 'datepicker',
    component: DatepickerComponent
  },
  {
    path: 'multifilter-poc',
    component: MultifilterPocComponent,
    data: {
      fullwidth: true,
      hideSidenav: true
    }
  },
  {
    path: 'datepicker-value-accessor',
    component: DatepickerValueAccessorComponent
  },
  {
    path: 'wheel-of-fortune',
    component: WheelOfFortuneComponent
  },
  {
    path: 'nya-ikoner',
    component: NyaIkonerComponent
  },
  {
    path: 'forloppsstegaren',
    component: ForloppsstegarenComponent
  },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

export default appRoutes;
