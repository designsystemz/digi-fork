import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';

import {
  UtilBreakpointObserverBreakpoints,
  LayoutBlockContainer,
  LayoutContainerVariation,
  LayoutContainerMaxWidth
} from 'arbetsformedlingen-dist';
import { filter, map, mergeMap } from 'rxjs';

@Component({
  selector: 'digi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  tabs = [
    { id: 'om-yrket', title: 'Om Yrket' },
    { id: 'arbetsuppgifter', title: 'Arbetsuppgifter' },
    { id: 'lon', title: 'Lön' },
    { id: 'formagor', title: 'Förmågor' }
  ];

  LayoutContainerVariation = LayoutContainerVariation;
  LayoutBlockContainer = LayoutBlockContainer;

  isMobile: boolean;
  isFullwidth: boolean;
  hideSidenav: boolean;
  sidenavDemo = false;

  navForm: UntypedFormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: UntypedFormBuilder
  ) {}

  pageTemplateSelect = [
    { name: 'Intranät', value: 'intranet' },
    { name: 'Designsystem', value: 'default' },
    { name: 'Arbetsformedlingen.se', value: 'external' }
  ];

  navigationLayoutSelect = [
    { name: 'Variant 1', value: 'primary' },
    { name: 'Variant 2', value: 'secondary' }
  ];

  ngOnInit() {
    this.navForm = this.fb.group({
      pageTemplateSelect: ['default', []],
      navigationLayoutSelect: ['secondary', []]
    });

    this.router.events
      .pipe(
        filter((events) => events instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map((route) => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        })
      )
      .pipe(
        filter((route) => route.outlet === 'primary'),
        mergeMap((route) => route.data)
      )
      .subscribe((x) => {
        if (x.hideSidenav) {
          this.hideSidenav = x.hideSidenav;
        } else {
          this.hideSidenav = false;
        }

        if (x.sidenavDemo) {
          this.sidenavDemo = x.sidenavDemo;
        } else {
          this.sidenavDemo = false;
        }

        if (x.fullwidth) {
          this.isFullwidth = x.fullwidth;
        } else {
          this.isFullwidth = false;
        }
      });
  }

  get asideClass() {
    if (this.hideSidenav) {
      return 'main__aside--hidden';
    }
    let className = '';
    switch (this.navForm.controls.pageTemplateSelect.value) {
      case 'external':
        className = 'main__aside--external';
        break;
      case 'intranet':
        className = 'main__aside--intranet';
        break;
      case 'default':
        className = 'main__aside--default';
    }
    return className;
  }

  @HostListener('document:afOnChange', ['$event'])
  onAfOnClick(event) {
    if (event.target.tagName == 'DIGI-UTIL-BREAKPOINT-OBSERVER') {
      this.isMobile =
        event.detail.value === UtilBreakpointObserverBreakpoints.SMALL;
    }
  }

  protected readonly LayoutContainerMaxWidth = LayoutContainerMaxWidth;
}
