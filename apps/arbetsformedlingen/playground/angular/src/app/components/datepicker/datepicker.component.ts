import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'digi-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent implements OnInit {
  today = new Date();
  multipleDates: Array<Date> = [
    this.today,
    new Date(new Date().setDate(this.today.getDate() - 1)),
    new Date(new Date().setDate(this.today.getDate() + 2)),
    new Date(new Date().setDate(this.today.getDate() + 7))
  ];
  singleDate: Array<Date> = [new Date(Date.now())];
  minDate: Date;
  maxDate: Date;
  weekNumbers: boolean;

  ngOnInit(): void {
    null;
  }

  setDates(evt) {
    this.multipleDates = evt.detail;
  }
  setDate(evt) {
    this.singleDate = evt.detail;
  }
  setMinDate(evt) {
    this.minDate = evt.detail[0];
  }
  setMaxDate(evt) {
    this.maxDate = evt.detail[0];
  }
  removeDate(date) {
    this.multipleDates = this.multipleDates.filter(
      (d) => !this.isSameDate(d, date)
    );
  }
  isSameDate(a: Date, b: Date): boolean {
    return this.getDateString(a) === this.getDateString(b);
  }
  getDateString(date: Date): string {
    return `${date.getDate()}${date.getMonth()}${date.getFullYear()}`;
  }
  toggleWeekNumbers() {
    this.weekNumbers = !this.weekNumbers;
  }

  get _multipleDates() {
    const isValidDates = this.multipleDates.every(
      (date) => date instanceof Date && !isNaN(date.getTime())
    );
    if (isValidDates) {
      return this.multipleDates;
    }
    return [];
  }
}
