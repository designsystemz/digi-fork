import { Component } from '@angular/core';

@Component({
  selector: 'digi-ng-link-buttons',
  templateUrl: './link-buttons.component.html',
  styleUrls: ['./link-buttons.component.scss']
})
export class LinkButtonsComponent {}
