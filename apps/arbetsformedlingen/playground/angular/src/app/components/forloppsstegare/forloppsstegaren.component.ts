import { Component } from '@angular/core';
import {
  ProgressListVariation,
  ProgressListHeadingLevel,
  ProgressListType,
  FormSelectFilterValidation
} from '@digi/arbetsformedlingen';

@Component({
  selector: 'digi-ng-forloppstegare',
  templateUrl: './forloppsstegaren.component.html'
})
export class ForloppsstegarenComponent {
  public ProgressListVariation = ProgressListVariation;
  public ProgressListHeadingLevel = ProgressListHeadingLevel;
  public ProgressListType = ProgressListType;
  public FormSelectFilterValidation = FormSelectFilterValidation;
  public currentStep = 1;

  public listItems = JSON.stringify([
    { label: 'Danska' },
    { label: 'Tyska' },
    { label: 'العربية (Arabiska)', dir: 'rtl', lang: 'ar' },
    { label: 'Ryska' },
    { label: 'Franska' },
    { label: 'Finska' },
    { label: 'Holländska' },
    { label: 'Italienska' },
    { label: 'Spanska' },
    { label: 'Grekiska' },
    { label: 'Romänska' }
  ]);

  changeCurrent() {
    if (this.currentStep >= 3) {
      this.currentStep = 1;
    } else {
      this.currentStep++;
    }
  }
}
