import { Component } from '@angular/core';

@Component({
  selector: 'digi-ng-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})
export class ValidationComponent {
  showDialog = false;
  openDialog() {
    this.showDialog = true;
  }
  clickButton() {
    console.log('clickButton');
  }
}
