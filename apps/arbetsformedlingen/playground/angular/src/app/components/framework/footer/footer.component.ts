import { Component } from '@angular/core';

import {
  LayoutBlockVariation,
  LayoutBlockContainer,
  LayoutContainerVariation,
  LogoVariation,
  LogoColor,
  TypographyVariation,
  FooterCardVariation,
  FooterVariation
} from 'arbetsformedlingen-dist';

@Component({
  selector: 'digi-ng-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  LayoutBlockVariation = LayoutBlockVariation;
  LayoutBlockContainer = LayoutBlockContainer;
  LayoutContainerVariation = LayoutContainerVariation;
  LogoVariation = LogoVariation;
  LogoColor = LogoColor;
  TypographyVariation = TypographyVariation;
  FooterCardVariation = FooterCardVariation;
  FooterVariation = FooterVariation;
}
