import { Component, OnInit, signal } from '@angular/core';
import { FormGroup, UntypedFormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'digi-datepicker-value-accessor',
  templateUrl: './datepicker-value-accessor.component.html',
  styleUrl: './datepicker-value-accessor.component.scss'
})
export class DatepickerValueAccessorComponent implements OnInit {
  constructor(private fb: UntypedFormBuilder) {}

  myForm: FormGroup;

  today = new Date();
  nextWeek = new Date(new Date().setDate(this.today.getDate() + 7));
  minDate = new Date(new Date().setDate(this.today.getDate() - 7));
  maxDate = new Date(new Date().setDate(this.today.getMonth() + 30));

  selectedDates = signal<Date[]>([this.today, this.nextWeek]);

  ngOnInit(): void {
    this.myForm = this.fb.group({
      myDatepicker: [this.selectedDates(), Validators.required]
    });
  }

  get myDates(): Date[] {
    return this.myForm.get('myDatepicker')?.value || [];
  }

  formatDate(date: Date): string | null {
    if (!(date instanceof Date) || isNaN(date.getTime())) {
      console.warn('Invalid date provided:', date);
      return null; // Return null or handle the error
    }

    return date
      .toLocaleDateString('sv-SE', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit'
      })
      .replace(/\./g, '-');
  }

  addDate(): void {
    const updatedDates = [...this.myDates, new Date()];
    this.myForm.get('myDatepicker')?.setValue(updatedDates);
    this.selectedDates.set(updatedDates);
  }

  removeDate(index: number): void {
    const updatedDates = this.myDates.filter((_, i) => i !== index);
    this.myForm.get('myDatepicker')?.setValue(updatedDates);
    this.selectedDates.set(updatedDates);
  }

  tagClickHandler(event: CustomEvent) {
    const el = event.target as HTMLElement;
    if (el.tagName.toLowerCase() === 'digi-tag') {
      const tagDate = el.innerText;
      const dateIndex = this.myDates.findIndex(
        (date) => this.formatDate(date) === tagDate
      );

      this.removeDate(dateIndex);
    }
  }

  dateChangeHandler(event: CustomEvent) {
    const dateList = event.detail;
    const isDates = dateList.every(
      (date) => date instanceof Date && !isNaN(date.getTime())
    );
    const dates: Date[] = isDates ? [...(event.detail as Date[])] : [];
    this.selectedDates.set(dates);
  }
}
