import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DatepickerValueAccessorComponent } from './datepicker-value-accessor.component';

describe('DatepickerValueAccessorComponent', () => {
  let component: DatepickerValueAccessorComponent;
  let fixture: ComponentFixture<DatepickerValueAccessorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DatepickerValueAccessorComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(DatepickerValueAccessorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
