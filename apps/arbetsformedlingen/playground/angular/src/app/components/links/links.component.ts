import { Component, AfterViewInit, OnInit } from '@angular/core';

@Component({
  selector: 'digi-ng-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss']
})
export class LinksComponent implements AfterViewInit, OnInit {
  ngOnInit(): void {
    const test: HTMLDigiLinkElement = document.querySelector(
      'digi-link#digiLinkTest'
    );
    console.log('nginit test');
    console.log(test);
  }

  ngAfterViewInit(): void {
    const test: HTMLDigiLinkElement = document.querySelector(
      'digi-link#digiLinkTest'
    );

    console.log('ngAfterViewInit test');
    console.log(test);

    if (test) {
      test.addEventListener('afOnReady', async (e: CustomEvent) => {
        console.log('CUSTOM LISTENER afOnReady CALLED');
        console.log(e);

        let testLog;
        await test.afMGetLinkElement().then((res) => {
          testLog = res;
        });
        console.log('CUSTOM LISTENER TESTLOG');
        console.log(testLog);
      });
    }
  }

  callback = async (e) => {
    console.log('COMPONENT LISTENER afOnReady CALLED');
    console.log(e);
    let testLog;
    const target: HTMLDigiLinkElement = e.target;
    await target.afMGetLinkElement().then((res) => {
      testLog = res;
    });
    console.log('CALLBACK TESTLOG');
    console.log(testLog);
  };
}
