import { Component } from '@angular/core';
import MyFiles from './demo-file';

@Component({
  selector: 'digi-ng-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent {
  myFiles = [...MyFiles];
  validateAsOK = true;

  public resp = [
    {
      file_id: '44d88612fea8a8f36de82e1278abb02f',
      status: 'OK',
      details: {
        confidence: 5,
        malware_family: 54768315,
        malware_type: 114,
        severity: 4,
        signature_name: 'Trojan.Win32.Mitaka.TC.a'
      }
    },

    {
      file_id: '3e74f8be1ca4e3acaf843c8b4f5ef6cc',
      status: 'OK',
      details: {
        confidence: 0,
        severity: 0,
        signature_name: ''
      }
    }
  ];

  async populateFiles(files) {
    await customElements.whenDefined('digi-form-file-upload');
    const fileUploadElement = document.querySelector('digi-form-file-upload');
    await fileUploadElement?.afMImportFiles(files);
  }

  async validateFile(file) {
    await customElements.whenDefined('digi-form-file-upload');
    const fileUploadElement = document.querySelector('digi-form-file-upload');
    await fileUploadElement?.afMValidateFile(file);
  }

  uploadFiles() {
    this.populateFiles(this.myFiles);
  }

  fakeValidate(file) {
    const copiedFile = new File([file], file.name, { type: file.type });
    copiedFile['id'] = file.id;
    if (this.validateAsOK) {
      copiedFile['status'] = 'OK';
    } else {
      copiedFile['status'] = 'error';
      copiedFile['error'] = 'Filen ' + file.name + ' har bedömts som osäker';
    }

    this.validateAsOK = !this.validateAsOK;
    return copiedFile;
  }

  onUpload(e) {
    setTimeout(() => {
      const fakeValidate = this.fakeValidate(e.detail);
      this.validateFile(fakeValidate);
    }, 1000);
  }

  onRemove(e) {
    console.log('onRemove', e);
  }

  onCancel(e) {
    console.log('onCancel', e);
  }
}
