import { Component, HostListener, Input } from '@angular/core';

import {
  UtilBreakpointObserverBreakpoints,
  NavigationSidebarVariation,
  NavigationVerticalMenuVariation
} from 'arbetsformedlingen-dist';

import ExternalNavigation from '../externalNavigation.json';

@Component({
  selector: 'digi-ng-navigation-external',
  templateUrl: './navigation-external.component.html',
  styleUrls: ['./navigation-external.component.scss']
})
export class NavigationExternalComponent {
  NavigationSidebarVariation = NavigationSidebarVariation;
  NavigationVerticalMenuVariation = NavigationVerticalMenuVariation;

  @Input() navLayout = 'primary';

  private _isMobile: boolean;

  navData = ExternalNavigation;

  get sideNavHeader() {
    return this._isMobile ? 'false' : 'true';
  }

  @HostListener('document:afOnChange', ['$event'])
  onAfOnChange(event) {
    if (event.target.tagName == 'DIGI-UTIL-BREAKPOINT-OBSERVER') {
      this._isMobile =
        event.detail.value === UtilBreakpointObserverBreakpoints.SMALL;
    }
  }
}
