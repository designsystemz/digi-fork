import { Component, HostListener, Input } from '@angular/core';

import {
  UtilBreakpointObserverBreakpoints,
  NavigationSidebarVariation,
  NavigationVerticalMenuVariation
} from 'arbetsformedlingen-dist';

import DesignsystemNavigation from '../designsystemNavigation.json';

@Component({
  selector: 'digi-ng-navigation-designsystem',
  templateUrl: './navigation-designsystem.component.html',
  styleUrls: ['./navigation-designsystem.component.scss']
})
export class NavigationDesignsystemComponent {
  NavigationSidebarVariation = NavigationSidebarVariation;
  NavigationVerticalMenuVariation = NavigationVerticalMenuVariation;

  @Input() navLayout = 'primary';

  private _isMobile: boolean;

  navData = DesignsystemNavigation;

  get sideNavHeader() {
    return this._isMobile ? 'false' : 'true';
  }

  @HostListener('document:afOnChange', ['$event'])
  onAfOnChange(event) {
    if (event.target.tagName == 'DIGI-UTIL-BREAKPOINT-OBSERVER') {
      this._isMobile =
        event.detail.value === UtilBreakpointObserverBreakpoints.SMALL;
    }
  }
}
