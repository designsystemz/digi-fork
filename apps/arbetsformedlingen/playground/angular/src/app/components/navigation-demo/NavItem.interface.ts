export interface NavItem {
  title?: string;
  divider?: string;
  highlight?: boolean;
  children?: NavItemChildren[];
  noLink?: boolean;
}

export interface NavItemChildren {
  title?: string;
  highlight?: boolean;
  children?: [];
  noLink?: boolean;
}
