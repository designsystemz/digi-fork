import { Component } from '@angular/core';

@Component({
  selector: 'digi-ng-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent {
  dates: Date[] = [new Date()];
}
