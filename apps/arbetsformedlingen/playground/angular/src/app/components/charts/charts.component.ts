import { Component } from '@angular/core';
import { ChartLineData } from 'dist/libs/arbetsformedlingen/dist/types';

@Component({
  selector: 'digi-ng-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})
export class ChartsComponent {
  barChartdata: ChartLineData = {
    data: {
      xValues: [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        21
      ],
      series: [
        {
          yValues: [
            708333, 6011283, 469751, 5996719, 17672408, 16894458, 1455858,
            12711914, 722069, 22621904, 2897470, 2203106, 0, 27045369, 4316673,
            0, 18571956, 5700205, 1672010, 0
          ],
          title: 'Megawattimmar'
        }
      ],
      xValueNames: [
        'Blekinge',
        'Dalarna',
        'Gotland',
        'Gävleborg',
        'Halland',
        'Jämtland',
        'Jönköping',
        'Kalmar',
        'Kronoberg',
        'Norrbotten',
        'Skåne',
        'Stockholm',
        'Södermanland',
        'Uppsala',
        'Värmland',
        'Västerbotten',
        'Västernorrland',
        'Västra Götaland',
        'Örebro',
        'Östergötland'
      ]
    },
    x: 'Län',
    y: 'MWh',
    title: 'Elproduktion i Sverige 2021 (MWh)',
    subTitle: 'Uppdelat per län, hämtat från Statens energimyndighet',
    infoText: 'Denna statistik avser hela landet och kan inte filtreras'
  };

  secondSet: ChartLineData = {
    data: {
      xValues: [1, 2, 3, 4],
      series: [
        {
          yValues: [2708, 1856, 575, 1670],
          title: 'Megawattimmar'
        }
      ],
      xValueNames: ['Blekinge', 'Dalarna', 'Gotland', 'Gävleborg']
    },
    x: 'Län',
    y: 'MWh',
    title: 'Någon helt annan data',
    subTitle: 'Lorem ipsum dolor sit amet',
    meta: {
      hideXAxis: true
    }
  };

  currentDataSet = {};

  setOne = {
    data: {
      xValueNames: [
        '2022-09-13',
        '2022-09-14',
        '2022-09-15',
        '2022-09-16',
        '2022-09-17'
      ],
      xValues: [1, 2, 3, 4, 5],
      series: [
        {
          yValues: [1, 6, 7, 2, 4],
          title: 'Commits'
        },
        {
          yValues: [0, 0, 0, 0, 0],
          title: 'deletions'
        },
        {
          yValues: [null, 1, null, 1, 3],
          title: 'n00bs'
        },
        {
          yValues: [7, 0, 2, 0, 0],
          title: 'inactive days'
        }
      ]
    },
    title: 'Utvecklingshistorik för detta linjediagram',
    subTitle:
      'Diagrammet visar historiken av commits, additions och tillhörande datum över tiden som detta linjediagram kodades i designsystemet',
    x: 'Datum',
    y: 'Commits'
  };

  setTwo = {
    data: {
      xValueNames: [
        '2022-09-13',
        '2022-09-14',
        '2022-09-15',
        '2022-09-16',
        '2022-09-17'
      ],
      xValues: [1, 2, 3, 4, 5],
      series: [
        {
          yValues: [1, 6, 7, 2, 4],
          title: 'Commits'
        },
        {
          yValues: [0, 0, 0, 0, 0],
          title: 'deletions'
        },
        {
          yValues: [null, 1, null, 1, 3],
          title: 'n00bs'
        },
        {
          yValues: [2, 3, 4, 5, null],
          title: 'inactive days'
        }
      ]
    },
    title: 'Utvecklingshistorik för detta linjediagram',
    subTitle:
      'Diagrammet visar historiken av commits, additions och tillhörande datum över tiden som detta linjediagram kodades i designsystemet',
    x: 'Datum',
    y: 'Commits'
  };

  _chartData = this.setOne;

  setCurrentData(dataset: string) {
    if (dataset == 'el') {
      this.currentDataSet = {};
      this._chartData = this.setOne;
    } else {
      this.currentDataSet = this.barChartdata;
      this._chartData = this.setTwo;
    }
  }

  get chartData() {
    return JSON.stringify(this._chartData);
  }

  get barChart(): string {
    return JSON.stringify(this.currentDataSet);
  }
}
