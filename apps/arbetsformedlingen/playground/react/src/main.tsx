import * as React from 'react';
import * as ReactDOM from 'react-dom/client';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';

import App from './app/app';
import Home from './app/components/pages/home/Home';

import ButtonTest from './app/components/pages/button-test/ButtonTest';
import FilterTest from './app/components/pages/filter-test/FilterTest';
import ErrorPage from './app/components/pages/error-page/ErrorPage';
import Components from './app/components/pages/components/Components';
import Forms from './app/components/pages/components/Forms';
import DatepickerComponent from './app/components/pages/datepicker/Datepicker';

const routes = [
  {
    path: '/',
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      { path: '/', element: <Home />, label: 'Hem' },
      { path: '/filter-test', element: <FilterTest />, label: 'Multi Filter' },
      { path: '/components', element: <Components />, label: 'Komponenter' },
      { path: '/forms', element: <Forms />, label: 'Formulär' },
      {
        path: '/datepicker',
        element: <DatepickerComponent />,
        label: 'Datepicker'
      },
      {
        path: '/buttontest',
        element: <ButtonTest />,
        label: 'Buggtest knappar'
      }
    ]
  }
];
interface IRoute {
  path: string;
  label: string;
}
export const Routes: IRoute[] = routes
  .map((route) => route.children)
  .flat()
  .map((route) => {
    return {
      path: route.path,
      label: route.label
    };
  });
const router = createBrowserRouter(routes);

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
