import {
  DigiFormErrorList,
  DigiFormCheckbox,
  DigiFormFieldset,
  DigiFormLabel,
  DigiButton,
  DigiFormFileUpload,
  DigiFormRadiobutton,
  DigiFormRadiogroup,
  DigiLayoutBlock,
  DigiTypographyHeadingJumbo
} from 'arbetsformedlingen-react-dist';
import {
  FormFileUploadVariation,
  FormFileUploadValidation,
  FormRadiobuttonVariation
} from 'arbetsformedlingen-dist';

const Forms = () => {
  return (
    <DigiLayoutBlock afVerticalPadding>
      <DigiTypographyHeadingJumbo afText="Formulär"></DigiTypographyHeadingJumbo>
      <p>BUGG</p>
      <p>HIJACKING FOCUS</p>
      <DigiFormErrorList afHeading="Felmeddelandelista">
        <a href="#input_1_id">Felmeddelandelänk 1</a>
        <a href="#input_2_id">Felmeddelandelänk 2</a>
      </DigiFormErrorList>

      <DigiFormFieldset
        afForm="Formulärnamnet"
        afLegend="Det här är en legend"
        afName="Fältgruppnamn">
        <DigiFormCheckbox afLabel="Alternativ 1"></DigiFormCheckbox>
        <DigiFormCheckbox afLabel="Alternativ 2"></DigiFormCheckbox>
        <DigiFormCheckbox afLabel="Alternativ 3"></DigiFormCheckbox>
        <DigiButton>Skicka</DigiButton>
      </DigiFormFieldset>

      <br></br>

      <DigiFormFileUpload
        afVariation={FormFileUploadVariation.LARGE}
        afValidation={FormFileUploadValidation.DISABLED}
        afFileTypes="*"></DigiFormFileUpload>

      <br></br>

      <br></br>

      <DigiFormLabel
        afLabel="Etikett"
        afFor={"<DigiIconInfoCircleSolid slot='actions' />"}
      />

      <br></br>

      <DigiFormRadiobutton
        afLabel="Kryssruta"
        afVariation={FormRadiobuttonVariation.PRIMARY}></DigiFormRadiobutton>

      <br></br>

      <DigiFormRadiogroup afName="myRadiogroupName">
        <DigiFormRadiobutton
          afLabel="Kryssruta 1"
          afValue="val-1"></DigiFormRadiobutton>
        <DigiFormRadiobutton
          afLabel="Kryssruta 2"
          afValue="val-2"></DigiFormRadiobutton>
        <DigiFormRadiobutton
          afLabel="Kryssruta 3"
          afValue="val-3"></DigiFormRadiobutton>
      </DigiFormRadiogroup>
    </DigiLayoutBlock>
  );
};

export default Forms;
