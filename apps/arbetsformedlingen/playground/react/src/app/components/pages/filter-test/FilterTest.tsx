import React, { useCallback, useState } from 'react';
import { DigiButton, DigiFormFilter } from 'arbetsformedlingen-react-dist';

// credits: GPT, fisher-yates algorithm
function shuffleArray<T>(inputArray: T[]): T[] {
  const array = [...inputArray];
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

const initItems = [
  {
    label: 'Arbetsmiljö (2)',
    id: 'Arbetsmiljö'
  },
  {
    label: 'Digitala tjänster (5)',
    id: 'Digitala tjänster'
  },
  {
    label: 'Digitalisering och omvärld (2)',
    id: 'Digitalisering och omvärld'
  },
  {
    label: 'Förändring och utveckling (1)',
    id: 'Förändring och utveckling'
  },
  {
    label: 'Hälsa (1)',
    id: 'Hälsa'
  },
  {
    label: 'It i arbetet (1)',
    id: 'It i arbetet'
  },
  {
    label: 'Livslångt lärande (1)',
    id: 'Livslångt lärande'
  },
  {
    label: 'Tillgänglighet (2)',
    id: 'Tillgänglighet'
  }
];

const initChecks = ['Livslångt lärande'];

export default function () {
  const [items, setItems] = useState(initItems);
  const [checks, setChecks] = useState(initChecks);

  const handleRandomRemove = useCallback(() => {
    const randIdx = Math.floor(Math.random() * items.length);
    setItems((items) => items.filter((_, i) => i !== randIdx));
  }, [items]);
  const handleShuffleItems = useCallback(() => {
    setItems((items) => shuffleArray(items));
  }, [items]);
  const handleFilterChange = useCallback(
    (e) => {
      const item = e.detail;
      if (checks.includes(item.id)) {
        // remove
        setChecks(checks.filter((check) => check !== item.id));
      } else {
        setChecks((checks) => [...checks, item.id]);
      }
    },
    [items, checks]
  );
  return (
    <main
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '1rem',
        flexDirection: 'column',
        gap: '1rem'
      }}>
      <DigiButton onAfOnClick={handleRandomRemove}>Remove random</DigiButton>
      <DigiButton onAfOnClick={handleShuffleItems}>Suffle items</DigiButton>
      <DigiButton onAfOnClick={() => setChecks([])}>
        Reset checkmarks
      </DigiButton>
      <DigiButton onAfOnClick={() => setItems(initItems)}>
        Reset items
      </DigiButton>
      <DigiFormFilter
        afFilterButtonText={`Filtrera (${checks.length}/${items.length})`}
        afSubmitButtonText="Filtrera"
        afResetButtonText="Återställ"
        afListItems={items}
        afCheckItems={checks}
        onAfSubmitFilter={(e) => console.log('submit', e.detail)}
        onAfCloseFilter={(e) => console.log('filter-closed', e.detail)}
        onAfResetFilter={() => {
          setChecks([]);
          setItems(initItems);
        }}
        onAfChangeFilter={handleFilterChange}
      />
    </main>
  );
}
