import {
  DigiNotificationErrorPage,
  DigiTypography
} from 'arbetsformedlingen-react-dist';
import { ErrorPageStatusCodes } from 'arbetsformedlingen-dist';

const ErrorPage = () => {
  return (
    <DigiTypography>
      <DigiNotificationErrorPage
        afHttpStatusCode={
          ErrorPageStatusCodes.NOT_FOUND
        }></DigiNotificationErrorPage>
    </DigiTypography>
  );
};

export default ErrorPage;
