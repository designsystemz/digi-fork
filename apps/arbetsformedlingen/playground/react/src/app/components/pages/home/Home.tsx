import {
  DigiLayoutBlock,
  DigiTypographyHeadingJumbo,
  DigiList
} from 'arbetsformedlingen-react-dist';
import { LayoutBlockContainer } from 'arbetsformedlingen-dist';

const Home = () => {
  return (
    <DigiLayoutBlock afVerticalPadding>
      <DigiTypographyHeadingJumbo afText="Designsystemet - React"></DigiTypographyHeadingJumbo>
      <DigiLayoutBlock
        afMarginTop
        afMarginBottom
        afContainer={LayoutBlockContainer.NONE}>
        <DigiList></DigiList>
      </DigiLayoutBlock>
    </DigiLayoutBlock>
  );
};

export default Home;
