import {
  DigiCalendarDatepicker,
  DigiLayoutBlock
} from 'arbetsformedlingen-react-dist';

const DatepickerComponent = () => {
  const today = new Date();
  let nextWeek = new Date();
  nextWeek.setDate(today.getDate() + 7);

  const gridStyle = {
    display: 'flex',
    gap: 'var(--digi--padding--large)'
  };

  return (
    <DigiLayoutBlock afVerticalPadding={true}>
      <div style={gridStyle}>
        <DigiCalendarDatepicker
          afSelectedDates={[today]}></DigiCalendarDatepicker>
        <DigiCalendarDatepicker
          afSelectedDates={[today, nextWeek]}
          afMultipleDates={true}></DigiCalendarDatepicker>
      </div>
    </DigiLayoutBlock>
  );
};

export default DatepickerComponent;
