import {
  DigiButton,
  DigiFormFilter,
  DigiFormInput,
  DigiLayoutBlock,
  DigiTypographyHeadingJumbo
} from '@digi/arbetsformedlingen-react';
import { FormFilterItem } from 'dist/libs/arbetsformedlingen/dist/types';
import { useState } from 'react';

const initList: FormFilterItem[] = [
  {
    label: 'Arbetssökande',
    id: 'arbetssokande'
  },
  {
    label: 'Arbetsgivare',
    id: 'arbetsgivare'
  },
  {
    label: 'Studerande',
    id: 'studerande'
  }
];

const SlotBug = () => {
  const [inputText, setinputText] = useState('');
  const [checkboxItems, setcheckboxItems] = useState([...initList]);

  return (
    <>
      <DigiLayoutBlock>
        <DigiTypographyHeadingJumbo afText="Slot Bug" />
        <pre>
          {/* {checkboxItems.map((checkbox) => {
          return <span key={checkbox.value}>{checkbox.label} </span>
        })} */}
        </pre>
      </DigiLayoutBlock>
      <DigiLayoutBlock afVerticalPadding>
        <h2>Digi-form-filter with dynamic updates</h2>
        <DigiFormInput
          afLabel="Lägg till val"
          afName="newItem"
          onAfOnInput={(e) => setinputText(e.detail.target.value)}
        />
        <DigiButton
          afVariation="primary"
          onAfOnClick={() => {
            setcheckboxItems([
              ...checkboxItems,
              {
                label: inputText,
                id: inputText.toLowerCase().replace(' ', '-')
              }
            ]);
          }}>
          Spara
        </DigiButton>

        <DigiButton
          afVariation="primary"
          onAfOnClick={() => {
            setcheckboxItems([...checkboxItems.slice(0, -1)]);
          }}>
          Radera sista
        </DigiButton>

        <hr />

        <DigiFormFilter
          afFilterButtonText="Yrkesområde"
          afSubmitButtonText="Filtrera"
          afListItems={checkboxItems}
        />
      </DigiLayoutBlock>
    </>
  );
};

export default SlotBug;
