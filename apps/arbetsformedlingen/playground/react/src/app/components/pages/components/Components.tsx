import {
  ButtonVariation,
  CodeLanguage,
  CodeVariation,
  ExpandableFaqVariation,
  InfoCardHeadingLevel,
  InfoCardType,
  InfoCardVariation,
  LayoutBlockContainer,
  LayoutColumnsElement,
  LinkButtonSize,
  LinkButtonVariation,
  LoaderSpinnerSize,
  LogoColor,
  LogoVariation,
  NavigationContextMenuItemType,
  ProgressbarVariation,
  TagSize,
  TypographyTimeVariation,
  ProgressListVariation,
  ProgressListHeadingLevel,
  ProgressListType
} from 'arbetsformedlingen-dist';

import {
  DigiCalendarWeekView,
  DigiLayoutBlock,
  DigiTypographyHeadingJumbo,
  DigiLink,
  DigiLinkInternal,
  DigiLinkExternal,
  DigiLinkButton,
  DigiButton,
  DigiTag,
  DigiIconArrowDown,
  DigiCalendar,
  DigiCode,
  DigiExpandableAccordion,
  DigiExpandableFaq,
  DigiExpandableFaqItem,
  DigiInfoCard,
  DigiInfoCardMultiContainer,
  DigiLayoutColumns,
  DigiMediaImage,
  DigiLoaderSpinner,
  DigiProgressListItem,
  DigiProgressList,
  DigiLogo,
  DigiNavigationBreadcrumbs,
  DigiNavigationContextMenu,
  DigiNavigationContextMenuItem,
  DigiNavigationPagination,
  DigiProgressbar,
  DigiTypographyTime,
  DigiList
} from 'arbetsformedlingen-react-dist';
import { useEffect, useState } from 'react';

import { useNavigate } from 'react-router-dom';

import styles from './Components.module.scss';

const Components = () => {
  const navigate = useNavigate();
  const dates = ['2022-07-04', '2022-07-11', '2022-07-12', '2022-07-18'];

  const [navItems, setNavItems] = useState([]);

  function clickHandler(e: Event) {
    const element = e.target as HTMLElement;
    const targetUrl = element.getAttribute('af-href');
    navigate(targetUrl);
  }

  useEffect(() => {
    type PageHeading = {
      id: string;
      text: string;
    };
    const pageHeadings: PageHeading[] = [];
    document.querySelectorAll('#pageContent > h2').forEach((h2) => {
      pageHeadings.push({
        id: h2.id,
        text: h2.textContent
      });
    });
    setNavItems(pageHeadings);
  }, []);

  return (
    <DigiLayoutBlock afVerticalPadding>
      <DigiTypographyHeadingJumbo afText="Komponenter"></DigiTypographyHeadingJumbo>
      <DigiLayoutBlock afContainer={LayoutBlockContainer.NONE}>
        <div
          id="pageContent"
          className={styles['page-content']}
          style={{
            position: 'relative'
          }}>
          <DigiLink
            afHref="/filter-test"
            afOverrideLink={true}
            onAfOnClick={clickHandler}>
            DigiLink overridar href navigering (digi-link){' '}
          </DigiLink>
          <div
            className={styles['page-navigation']}
            style={{
              position: 'absolute',
              zIndex: 999,
              top: '0',
              bottom: '0',
              right: 'var(--digi--gutter--medium)'
            }}>
            <div
              style={{
                position: 'sticky',
                top: 'var(--digi--margin--medium)',
                background: 'var(--digi--color--background--primary)',
                boxShadow: '3px 3px 10px 0px rgba(0,0,0,0.15)',
                width: '200px',
                padding: 'var(--digi--padding--medium)',
                overflowY: 'scroll'
              }}>
              <ul className={styles['list']}>
                {navItems.map((item) => {
                  return (
                    <li key={item.id}>
                      <a
                        href={`#${item.id}`}
                        onClick={(e) => {
                          console.log(e);
                          e.preventDefault();
                          const el = document.getElementById(item.id);
                          if (el) {
                            el.scrollIntoView({ behavior: 'smooth' });
                          }
                        }}>
                        {item.text}
                      </a>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>

          <h2 id="calendarAnchor">Kalender</h2>

          <DigiCalendar
            afActive={true}
            afShowWeekNumber={true}
            afMultipleDates={true}>
            <DigiButton slot="calendar-footer">Välj</DigiButton>
          </DigiCalendar>

          <h2 id="calendarWeekViewAnchor">Veckokalender</h2>

          <DigiCalendarWeekView
            afDates={JSON.stringify(dates)}
            onAfOnDateChange={(e: CustomEvent) =>
              console.log('dateChange', e.detail)
            }></DigiCalendarWeekView>

          <h2 id="listAnchor">Listor</h2>
          <DigiList>
            <li>
              <a href="/">startsidan (a-tagg) </a>
            </li>
            <li>
              <DigiLink
                afHref="/article"
                afOverrideLink
                onAfOnClick={clickHandler}>
                startsidan (digi-link)
              </DigiLink>
            </li>
            <li>
              <DigiLinkInternal
                afHref="/article"
                afOverrideLink
                onAfOnClick={clickHandler}>
                startsidan (digi-link-internal)
              </DigiLinkInternal>
            </li>
            <li>
              <DigiLinkExternal afHref="/article" onAfOnClick={clickHandler}>
                startsidan (digi-link-external)
              </DigiLinkExternal>
            </li>
            <li>
              <DigiLinkButton
                afHref="/article"
                afOverrideLink
                onAfOnClick={clickHandler}>
                startsidan (digi-link-button)
              </DigiLinkButton>
            </li>
            <li>
              <DigiButton
                afSize="large"
                afVariation={ButtonVariation.PRIMARY}
                afFullWidth={false}>
                startsidan (digi-link-external)
                <DigiIconArrowDown slot="icon" />
              </DigiButton>
            </li>
            <li>
              <DigiTag
                afText="Jag är en tag"
                afSize={TagSize.SMALL}
                afNoIcon={false}></DigiTag>
            </li>
          </DigiList>

          <h2 id="codeAnchor">Kod</h2>

          <DigiCode
            afLanguage={CodeLanguage.JAVASCRIPT}
            afVariation={CodeVariation.DARK}
            afCode="function foo(bar) {
                if (foo + 2 === 3) {
                  return true // This is true if foo is 1;
                } else {
                  return 'Wrong!';
                }"
          />

          <h2 id="expandableAccordionAnchor">Utfällbar rubrik</h2>

          <DigiExpandableAccordion afHeading="Utfällbar rubrik">
            <p>Ea nulla enim enim voluptate mollit proident.</p>
          </DigiExpandableAccordion>

          <h2 id="expandableFaqAnchor">FAQ</h2>

          <DigiExpandableFaq
            afHeading="Vanliga frågor"
            afVariation={ExpandableFaqVariation.PRIMARY}>
            <DigiExpandableFaqItem afHeading="Fråga 1">
              <p>
                Svar 1 - Reprehenderit ea nulla dolore non fugiat aute.
                Reprehenderit velit velit non eiusmod. Voluptate commodo
                consectetur ullamco velit minim quis. Reprehenderit consectetur
                nisi officia est fugiat id anim incididunt qui eiusmod.
              </p>
            </DigiExpandableFaqItem>
            <DigiExpandableFaqItem afHeading="Fråga 2">
              <p>
                Svar 2 - In veniam nostrud esse velit velit incididunt ullamco
                non adipisicing. Dolore sint quis aute nostrud. Pariatur commodo
                ullamco dolor nulla ut. Consequat amet velit veniam ipsum
                reprehenderit tempor. Eu labore enim officia anim laboris magna
                eu eu ad. Pariatur voluptate sint nisi ut nulla.
              </p>
            </DigiExpandableFaqItem>
            <DigiExpandableFaqItem afHeading="Fråga 3">
              <p>
                Svar 3 - Officia laboris commodo quis ex nisi cupidatat officia
                dolore est. Ad dolore exercitation sunt deserunt Lorem do esse
                reprehenderit ex non. Proident occaecat elit enim ullamco.
              </p>
            </DigiExpandableFaqItem>
          </DigiExpandableFaq>

          <h2 id="infoCardMultiContainerAnchor">Infokortsbehållare</h2>

          <DigiInfoCardMultiContainer afHeading="Rubrik till infokortbehållare">
            <DigiInfoCard
              afHeading="Jag är ett infokort av typen multi"
              afVariation={InfoCardVariation.PRIMARY}
              afType={InfoCardType.RELATED}
              afLinkText="Jag är en obligatorisk länktext"
              afLinkHref="/"
              afHeadingLevel={InfoCardHeadingLevel.H1}>
              <p>
                Det här är bara ord för att illustrera hur det ser ut med text
                inuti. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Suspendisse commodo egestas elit in consequat. Proin in ex
                consectetur, laoreet augue sit amet, malesuada tellus.
              </p>
            </DigiInfoCard>
          </DigiInfoCardMultiContainer>

          <h2 id="mediaAnchor">Media</h2>
          <DigiLayoutColumns afElement={LayoutColumnsElement.DIV}>
            <DigiMediaImage
              afUnlazy
              afSrc="assets/images/employment-service-sign.jpg"
              afAlt="Fasadskylt med Arbetsförmedlingens logotyp."></DigiMediaImage>
            <p>...</p>
          </DigiLayoutColumns>

          <h2 id="linkButtonAnchor">Länkknapp</h2>

          <DigiLinkButton
            afHref="'#'"
            afSize={LinkButtonSize.MEDIUM}
            afVariation={LinkButtonVariation.SECONDARY}>
            Jag är en länk
          </DigiLinkButton>

          <h2 id="loaderSpinnerAnchor">Loading Spinner</h2>

          <DigiLoaderSpinner
            afSize={LoaderSpinnerSize.MEDIUM}></DigiLoaderSpinner>

          <h2 id="logoAnchor">Logo</h2>

          <DigiLogo
            afSystemName="System One"
            afVariation={LogoVariation.LARGE}
            afColor={LogoColor.PRIMARY}></DigiLogo>

          <h2 id="breadcrumbAnchor">Brödsmulor</h2>

          <DigiNavigationBreadcrumbs afCurrentPage="Nuvarande sida">
            <a href="/">Start</a>
            <a href="/contact-us">Undersida 1</a>
            <a href="#">Undersida 2</a>
          </DigiNavigationBreadcrumbs>

          <h2 id="navigationContextMenuAnchor">Språkväljare</h2>

          <DigiNavigationContextMenu
            afText="Rullgardinsmeny"
            afStartSelected={0}>
            <DigiNavigationContextMenuItem
              afText="Menyval 1"
              afType={
                NavigationContextMenuItemType.BUTTON
              }></DigiNavigationContextMenuItem>
            <DigiNavigationContextMenuItem
              afText="Menyval 2"
              afType={
                NavigationContextMenuItemType.BUTTON
              }></DigiNavigationContextMenuItem>
          </DigiNavigationContextMenu>

          <h2 id="paginationAnchor">Paginering</h2>

          <DigiNavigationPagination
            afTotalPages={6}
            afInitActive-page="1"
            afCurrentResultStart={1}
            afCurrentResultEnd={25}
            afTotalResults={10}
            afResultName="annonser"
            af-total-pages={6}
            afInitActivePage={1}></DigiNavigationPagination>

          <h2 id="progressStepAnchor">Förloppsmätare</h2>

          <DigiProgressList
            role="list"
            afCurrentStep={2}
            afVariation={ProgressListVariation.PRIMARY}
            afHeading="Huvudrubrik"
            afAriaLabel="Min huvudrubrik"
            afHeadingLevel={ProgressListHeadingLevel.H2}
            afType={ProgressListType.CIRCLE}
            afExpandable={false}>
            <DigiProgressListItem afHeading="Rubrik 1" afAriaLabel="Rubrik 1">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
              magna neque, interdum vel massa eget, condimentum rutrum velit.
              Sed vitae ullamcorper sem.
            </DigiProgressListItem>
            <DigiProgressListItem afHeading="Rubrik 2" afAriaLabel="Rubrik 2">
              <div>
                <h4>Underrubrik</h4>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                magna neque, interdum vel massa eget, condimentum rutrum velit.
                Sed vitae ullamcorper sem.
              </div>
            </DigiProgressListItem>
            <DigiProgressListItem afHeading="Rubrik 3" afAriaLabel="Rubrik 3">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
              magna neque, interdum vel massa eget, condimentum rutrum velit.
              Sed vitae ullamcorper sem.
            </DigiProgressListItem>
          </DigiProgressList>

          <DigiProgressbar
            afTotalSteps={4}
            afCompletedSteps={2}
            afVariation={ProgressbarVariation.PRIMARY}></DigiProgressbar>

          <h2 id="typographyAnchor">Typografi</h2>

          <DigiTypographyHeadingJumbo afText="Det här är en jumborubrik"></DigiTypographyHeadingJumbo>

          <DigiTypographyTime
            afVariation={TypographyTimeVariation.DISTANCE}
            afDateTime="2022-12-12"></DigiTypographyTime>
        </div>
      </DigiLayoutBlock>
    </DigiLayoutBlock>
  );
};

export default Components;
