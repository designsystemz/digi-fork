# Template Generator 🤸

Genererar nödvändiga filer när du vill skapa:

- Ny komponent
- Ny ikon
- Ny dokumentationssida för komponent
- Ny undersida

## Information

Manual för verktyget
`npx nx start template-generator -- --help`

Manual för specifikt kommando för verktyget
`npx nx start template-generator -- new-component --help`
