export interface ScaffoldNode {
  name: string
  content?: string
  children?: ScaffoldNode[]
}