import { camelCase, capitalize } from '../helpers';
import type { ScaffoldNode } from '../types';

export function scaffold(name: string, displayName: string): ScaffoldNode[] {
  const kebabName = name;
  const camelName = camelCase(kebabName);
  const pascalName = capitalize(camelName);

  const componentSource = `
import { Component, Host, Prop, h } from '@stencil/core';

import { ${pascalName}Variation } from './${kebabName}-variation.enum';

/**
 * @enums ${pascalName}Variation - ${kebabName}-variation.enum.ts
 * @swedishName ${displayName}
 */
@Component({
  tag: 'digi-${kebabName}',
  styleUrl: '${kebabName}.scss',
  scoped: true
})
export class ${pascalName} {

  /**
  * Ändra variant av komponent
  * @en Change variant of component
  */
  @Prop() afVariation = ${pascalName}Variation.Primary

  get cssModifiers() {
    return {
      'digi-${kebabName}--primary': this.afVariation === ${pascalName}Variation.Primary,
      'digi-${kebabName}--secondary': this.afVariation === ${pascalName}Variation.Secondary
    };
  }

  render() {
    return (
      <Host>
        <div class={this.cssModifiers}>
          <p>Hej Världen! 👋</p>
          <p>Jag är <code>digi-${kebabName}</code></p>
        </div>
      </Host>
    );
  }
}
  `;
  const componentStyle = `
@use './styles/${kebabName}.variations' as variations;
@use './styles/${kebabName}.variables' as *;

:host {
  @include digi-${kebabName}--variables;

  .digi-${kebabName} {
  // Exempel:
  // background-color: var(--COLOR--BACKGROUND);
  // border-color: var(--COLOR--BORDER);
  //
  // &--primary {
  //   @include variations.primary;
  // }
  //
  // &--secondary {
  //   @include variations.secondary;
  // }
  }
}
  `;
  const componentTypes = `
export enum ${pascalName}Variation {
  Primary = 'primary'
}
  `;

  const componentStyleVariables = `
@mixin digi-${kebabName}--variables {
  // Namngivning följer en variant av CTI "Category > Type > Item > State"
  // I vårt fall "Component > Category > Type > Variant > State"
  // 
  // Exempel: 
  // --digi--${kebabName}--color--background--primary: black;
  // --digi--${kebabName}--color--border--primary: gray;
  //
  // --digi--${kebabName}--color--background--secondary: black;
  // --digi--${kebabName}--color--border--secondary: white;
}
  `;

  const componentStyleVariations = `
@mixin primary {
  // Exempel:
  // --COLOR--BACKGROUND: var(--digi--${kebabName}--color--background--primary);
  // --COLOR--BORDER: var(--digi--${kebabName}--color--border--primary);
}

@mixin secondary {
  // Exempel:
  // --COLOR--BACKGROUND: var(--digi--${kebabName}--color--background--secondary);
  // --COLOR--BORDER: var(--digi--${kebabName}--color--border--secondary);
} 
  `;

  return [
    {
      name: kebabName,
      children: [
        { name: `${kebabName}.tsx`, content: componentSource },
        { name: `${kebabName}.scss`, content: componentStyle },
        {
          name: `styles`,
          children: [
            {
              name: `_${kebabName}.variables.scss`,
              content: componentStyleVariables
            },
            {
              name: `_${kebabName}.variations.scss`,
              content: componentStyleVariations
            }
          ]
        },
        { name: `${kebabName}-variation.enum.ts`, content: componentTypes }
      ]
    }
  ];
}
