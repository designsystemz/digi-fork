import path from "path";
import fs from 'fs-extra'

import { Command } from 'commander'

import * as stencilComponent from "./template/digi-component";
import * as digiIcon from './template/digi-icon'
import * as docsPage from './template/docs-page'
import * as docsComponentPage from './template/docs-component-page'

import type { ScaffoldNode } from "./types";

async function generateFiles (structure: ScaffoldNode[], currentPath) {
  for (const node of structure) {
    const newPath = path.join(currentPath, node.name)
    if (node.children != null) { // recursive call
      await fs.ensureDir(newPath);
      await generateFiles(node.children, newPath);
    } else { // base case
      if (await fs.pathExists(newPath)) {
        throw new Error('🚨 Aborting, file already exists: ' + newPath);
      }
      await fs.writeFile(newPath, node.content)
    }
  }
}

function trimStart (tag: string, matches: string[]) {
  for (const match of matches) {
    if (tag.startsWith(match)) {
      return tag.slice(match.length)   
    }
  }
  return tag
}

function trimEnd(tag: string, matches: string[]) {
  for (const match of matches) {
    if (tag.endsWith(match)) {
      return tag.slice(0, -1 * match.length);
    }
  }
  return tag;
} 

async function start () {

  const program = new Command();
  program.name('digi-template-generator')

  program
    .command('new-component')
    .description('Create a new component')
    .requiredOption('--tag <tagName>', 'Example: form-filter')
    .requiredOption('--name <displayName>', 'Example: Multifilter')
    .arguments('<outDir>')
    .action(async (outDir, opts) => {
      const componentName = trimStart(opts.tag, ['digi-']);
      const displayName = opts.name;
      await fs.ensureDir(outDir);
      const structure = stencilComponent.scaffold(componentName, displayName);
      await generateFiles(structure, outDir);

      const resultDir = path.join(outDir, structure[0].name)
      console.log(`
  TODO 📝
- Flytta komponent-mappen som skapats i "${resultDir}" till passande kategori-mapp.
  Exempel: Flytta till mappen "_form" om det är en formulärkomponent.
      `);

      console.log(`✨ ${resultDir}`);
    });

  program
    .command('new-icon')
    .description('Create a new icon')
    .requiredOption('--tag <tagName>', 'Example: chevron-up')
    .arguments('<outDir>')
    .action(async (outDir, opts) => {
      const name = trimStart(opts.tag, ['digi-icon-', 'icon-']);
      await fs.ensureDir(outDir);
      const structure = digiIcon.scaffold(name);
      await generateFiles(structure, outDir);

      console.log(`✨ ${path.join(outDir, structure[0].name)}`);
    });

  program
    .command('new-docs-page')
    .description('Create a new documentation page')
    .requiredOption('--id <pageId>', 'Example: about-animations')
    .requiredOption('--title <pageTitle>', 'Example: Animationer och rörelse')
    .arguments('<outDir>')
    .action(async (outDir, opts) => {
      const name = trimStart(opts.id, ['digi-docs', 'digi-']);
      await fs.ensureDir(outDir);
      const structure = docsPage.scaffold(name, opts.title);
      await generateFiles(structure, outDir);

      console.log(`
  TODO 📝
- Lägg in "Route" nedan i "digi-docs.tsx"
- Uppdatera path så att det följer namngivning för relaterade sidor 

  <Route path="/${name}">
    <digi-docs-${name} />
  </Route>
      `);

      console.log(`✨ ${path.join(outDir, structure[0].name)}`);
    });

    program
      .command('new-docs-component-page')
      .description('Create a documentation page with examples and documentation for a component')
      .requiredOption('--tag <componentTag>', 'Example: form-filter')
      .requiredOption('--title <pageTitle>', 'Example: Multifilter')
      .arguments('<outDir>')
      .action(async (outDir, opts) => {
        let name = trimStart(opts.tag, ['digi-docs', 'digi-']);
        name = trimEnd(name, ["-details"])
        await fs.ensureDir(outDir);
        const structure = docsComponentPage.scaffold(name, opts.title);
        await generateFiles(structure, outDir);

        console.log(`

  TODO 📝
- Lägg till komponenten i "categoryData.ts" kopplad till passande kategori.
  Detta gör att komponenten visas i navigation och sök.

  >
  > digi-${name}
  >
        `);

        console.log(`✨ ${path.join(outDir, structure[0].name)}`);
      });

    program.parse()
}

start()