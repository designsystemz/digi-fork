import { Command } from 'commander';
import { marked } from 'marked';
import fs from 'fs/promises';
import fse from 'fs-extra'

function changelogToHtml(markdown: string): string {
  let headingCount = 0 // keep track of headings
  marked.use({
    renderer: {
      heading(text, level) {
        if (level === 2) {
          // add content in between headings in accordions
          // comments for brevity as the solution is a hack
          const expandByDefault = headingCount < 3 ? 'true' : 'false';
          const prevClosingTag = headingCount > 0 ? '</digi-expandable-accordion af-heading>' : '';
          headingCount += 1; // keep track of processed headings
          return (
            prevClosingTag +
            `<digi-expandable-accordion af-heading="${text}" af-expanded="${expandByDefault}">`
          );
        } else {
          return `<h${level}>${text}</h${level}>`
        }
      },
      html(html) {
        if (html.match(/<breaking\s*\/>/)) {
          return '<span class="breaking-tag">Breaking</span>';
        }
        return html;
      },
      codespan(code) {
        return `<digi-code af-code="${code}"></digi-code>`;
      }
    }
  });

  let html = marked(markdown);
  html += '</digi-expandable-accordion af-heading>'; // closing tag for last accordion

  return html

}

async function start() {
  const program = new Command();
  program
    .name('changelog-util')
    .version('0.0.1');

  program
    .command('generate')
    .description('Create release notes json from Changelog using @digi components')
    .arguments('<input>')
    .arguments('<output>')
    .action(async (changelogFile, outputFile) => {
      const markdown = await fs.readFile(changelogFile, { encoding: 'utf8' });
      const html = changelogToHtml(markdown);
      
      const outputDir = outputFile.includes('/')
      ? outputFile.split('/').slice(0, -1).join('/')
      : null
      
      if (outputDir != null) {
        await fse.ensureDir(outputDir);
      }
      
      const jsonContent = JSON.stringify({ html })
      try {
        await fs.writeFile(outputFile, jsonContent, { encoding: 'utf8' });
        console.log('CHANGELOG.md successfully exported to ' + outputFile);
      } catch (error) {
        console.error('An error occured generating changelog data', error);
      }
    });

  program.parse();
}

start();
