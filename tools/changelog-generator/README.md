# Changelog Tool

Generate HTML using our custom components for "Release Notes"

## Compile to JS
`nx build changelog-generator` or `tsc`

## Compile Changelog Markdown to HTML
From workspace root run
`node /dist/tools/changelog-generator/src/index.js generate <markdownFile> <outputJsonFile>`

For more help
`node /dist/tools/changelog-generator/src/index.js --help`
