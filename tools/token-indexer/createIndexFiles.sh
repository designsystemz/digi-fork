#/usr/bin/env bash
function create() {
	echo Creating $1
	node index.js index ../../libs/core/package/src/components --btf ../../libs/arbetsformedlingen/design-tokens/src/styles/$1/$1.brand.json --gtf ../../libs/arbetsformedlingen/design-tokens/src/styles/$1/$1.global.json > target/$1.json
}

mkdir -p target

for a in ../../libs/arbetsformedlingen/design-tokens/src/styles/*; do
	create $(basename $a)
done
