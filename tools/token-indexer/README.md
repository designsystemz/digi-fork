För att skapa upp index för använding av olika brand tokens, gör följande:

```shell
$ nvm use
$ npm ci
$ ./createIndexFiles.sh
```

Index-filerna hamnar i target-katalogen.
