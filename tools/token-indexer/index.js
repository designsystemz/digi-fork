import { Command } from 'commander';
import { readdir, readFile } from 'node:fs/promises';
import { Indexer } from './indexer.js';

const program = new Command();

program
  .name('indexer')
  .description('Indexerar design-tokens och deras användning i komponenter');

program
  .command('index')
  .option('--btf, --brand-tokens-file <file>')
  .option('--gtf, --global-tokens-file <file>')
  .argument('<directory>', 'The directory to index')
  .action(async (arg, options) => {
    let brandFile, globalFile, filesToIndex;
    const brandFilePromise = readFile(options.brandTokensFile);
    const filesPromise = readdir(arg, { recursive: true, withFileTypes: true });
    const globalFilePromise = readFile(options.globalTokensFile);

    await Promise.all([
      brandFilePromise.then((f) => JSON.parse(f)).then((f) => (brandFile = f)),
      filesPromise.then((a) => (filesToIndex = a)),
      globalFilePromise.then((f) => JSON.parse(f)).then((f) => (globalFile = f))
    ]);
    brandFilePromise.then((f) => (brandFile = JSON.parse(f)));

    new Indexer(brandFile, globalFile).index(filesToIndex);
  });

program.parse();
