import { program } from 'commander';

import nestTokens from './commands/nestTokens';
import exportTokens from './commands/exportTokens';
import indexTokens from './commands/indexTokens';
import extractCss from './commands/extractCss';
import transform from './commands/transform';
import validateTokens from './commands/validateTokens';
import flattenTokens from './commands/flattenTokens';

program
  .name('style-dictionary-export')
  .description('Utility to manage, convert and extract design tokens')
  .version('0.0.2')
  .addCommand(nestTokens.name('nest'))
  .addCommand(exportTokens.name('export'))
  .addCommand(indexTokens.name('index'))
  .addCommand(extractCss.name('extract-css'))
  .addCommand(transform.name('transform'))
  .addCommand(validateTokens.name('validate'))
  .addCommand(flattenTokens.name('flatten'));

program.parse(process.argv);
