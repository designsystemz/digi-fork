import SDPackage from 'style-dictionary';
import { Command } from 'commander';
import { format } from 'date-fns';
import { sv } from 'date-fns/locale';
import * as fs from 'fs-extra';
import tinycolor from 'tinycolor2'; // library used by style dictionary
import { namePathToDotNotation } from '../transfomer/name-path-to-dot-notation';
import { namePathToDashNotation } from '../transfomer/name-path-to-dash-notation';
import StyleDictionary from 'style-dictionary';

type FigmaVariableType = 'COLOR' | 'STRING' | 'FLOAT' | 'BOOLEAN';

function generateTokenName(parts: string[]) {
  const filtered = parts.filter((part) => !['value', 'default'].includes(part));
  return filtered.join('/');
}

function generateTokenId(parts: string[]): string {
  const filtered = parts.filter((part) => !['value'].includes(part));
  return filtered.join('--');
}

function toFigmaToken(userValue: string): {
  value: any;
  type: FigmaVariableType;
} {
  const value = userValue.trim();
  if (value.length > 5 && tinycolor(value).isValid()) {
    // color
    const normalized = Object.fromEntries(
      // go from 0 - 255 to 0 - 1
      Object.entries(tinycolor(value).toRgb()).map(
        ([channel, value]) =>
          channel !== 'a'
            ? [channel, parseInt(value as string) / 255]
            : [channel, parseInt(value as string)]
      )
    );
    return { type: 'COLOR', value: normalized };
  } else if (!isNaN(Number(value))) {
    return {
      type: 'FLOAT',
      value: Number(value)
    };
  } else if (value.endsWith('px') && value.indexOf(' ') === -1) {
    return {
      type: 'FLOAT',
      value: parseInt(value)
    };
  } else if (value.endsWith('rem') && value.indexOf(' ') === -1) {
    return {
      type: 'FLOAT',
      value: Number(value.slice(0, -3)) * 16
    };
  }
  return { type: 'STRING', value };
}

function exportTokens(
  inputs: string[],
  platform,
  opts: {
    output: string;
    prefix?: string;
    config?: Record<any, any>;
  }
) {

  const StyleDictionary = SDPackage.extend({
    source: inputs,
    platforms: {
      json: {
        transformGroup: 'tokens-json',
        buildPath: opts.output,
        files: [
          {
            destination: 'tokens.json',
            format: 'json/flat'
          }
        ]
      },
      css: {
        transformGroup: 'css',
        buildPath: opts.output,
        files: [
          {
            destination: 'variables.css',
            format: 'css/variables',
            options: {
              outputReferences: true,
              fileHeader: 'myCustomHeader'
            }
          }
        ]
      },
      scss: {
        transformGroup: 'scss',
        buildPath: opts.output,
        files: [
          {
            destination: 'variables.scss',
            format: 'scss/variables',
            options: {
              fileHeader: 'myCustomHeaderSCSS'
            }
          }
        ]
      },
      print: {
        transformGroup: 'print',
        buildPath: opts.output,
        files: [
          {
            destination: 'print.json',
            format: 'json/flat'
          }
        ]
      },
      styleguide: {
        transformGroup: 'styleguide',
        buildPath: opts.output,
        files: [
          {
            destination: 'web.json',
            format: 'json/flat'
          },
          {
            destination: 'web.scss',
            format: 'scss/variables',
            options: {
              fileHeader: 'myCustomHeaderSCSS'
            }
          }
        ]
      },
      'web/js': {
        transformGroup: 'tokens-js',
        buildPath: opts.output,
        prefix: 'digi',
        files: [
          {
            destination: 'tokens.es6.js',
            format: 'javascript/es6',
            options: {
              fileHeader: 'myCustomHeaderSCSS'
            }
          }
        ]
      },
      figma: {
        transforms: ['name/digiName/dash'],
        buildPath: opts.output,
        files: [
          {
            destination: 'flat.json',
            format: 'figma/export-import-variables/flat'
          }
        ]
      },
      'qlikSense/json': {
        transformGroup: 'qlikSense-json',
        buildPath: opts.output,
        files: [
          {
            destination: 'qlikSense-variables.json',
            format: 'qlikSense/json'
          }
        ]
      },
      'sketch/txt': {
        transformGroup: 'sketch-txt',
        buildPath: opts.output,
        files: [
          {
            destination: 'sketch-colors.txt',
            format: 'sketch/txt',
            filter: function (token) {
              return token.filePath.includes('src/styles/color/color.global');
            }
          }
        ]
      },
      'json/dot': {
        transforms: ['name/pathToDotNotation'],
        buildPath: opts.output,
        files: [
          {
            destination: 'token-dot.json',
            format: 'json/dot'
          }
        ]
      },
      'json/dash': {
        transforms: ['name/pathToDashNotation'],
        buildPath: opts.output,
        files: [
          {
            destination: 'token-dash.json',
            format: 'json/dash'
          }
        ]
      }
    }
  });

  // REGISTER CUSTOM FORMATS + TEMPLATES + TRANSFORMS + TRANSFORM GROUPS

  // ----- Formats

  StyleDictionary.registerFormat({
    name: 'json/flat',
    formatter: function (dictionary: any) {
      return JSON.stringify(dictionary.allProperties, null, 2);
    }
  });

  StyleDictionary.registerFormat({
    name: 'json/dot',
    formatter: function ({ dictionary }) {
      const flattened = dictionary.allProperties.map(({ name, original }) => {
        const value = original.value.replace('.value}', '}');
        return [name, value];
      });
      return JSON.stringify(Object.fromEntries(flattened), null, 2);
    }
  });

  StyleDictionary.registerFormat({
    name: 'json/dash',
    formatter: function ({ dictionary }) {
      const flattened = dictionary.allProperties.map(({ name, original }) => {
        if (original.value.includes('{') && original.value.includes('}')) {
          const value = original.value
            .replace('.value}', '}')
            .replaceAll('.', '--');
          return [name, value];
        }
        return [name, original.value];
      });
      const tokens = Object.fromEntries(flattened);
      const result = {
        tokens
      };
      return JSON.stringify(result, null, 2);
    }
  });

  StyleDictionary.registerFormat({
    name: 'qlikSense/json',
    formatter: function ({ dictionary }) {
      const qsVars = {};
      dictionary.allProperties.forEach(
        (token) => (qsVars[token.name] = token.value)
      );
      return JSON.stringify(qsVars, null, 1);
    }
  });

  StyleDictionary.registerFormat({
    name: 'sketch/txt',
    formatter: function ({ dictionary }) {
      let sketchVars = '';
      dictionary.allProperties.forEach(
        (token) => (sketchVars += `${token.name}: ${token.value}\n`)
      );
      return sketchVars;
    }
  });
  // handcrafted for the figma plugin available at
  // https://www.figma.com/community/plugin/1256972111705530093
  // worth to mention: the naming of properties follows the official Figma API at
  // https://www.figma.com/plugin-docs/api/Variable/
  StyleDictionary.registerFormat({
    name: 'figma/export-import-variables/flat',
    formatter: function ({ dictionary }) {
      const defaultCollections = [
        {
          id: 'VariableCollectionId:1:1',
          variableId: 'VariableId:1:1',
          name: 'Design Tokens',
          modes: [{ id: 'value', name: 'Value', prefix: '' }],
          match: ['.*']
        }
      ];
      const properties = dictionary.allProperties.sort((a, b) =>
        a.name.localeCompare(b.name)
      );
      const config =
        opts.config != null ? opts.config : { collections: defaultCollections };
      const results: Array<{
        id: string,
        name: string,
        modes: { [key: string]: string },
        variableIds: string[],
        variables: any
      }> = [];
      const usedTokens = new Set();
      for (const collection of config.collections) {
        const tokens = collection.match
          .map((_pattern) => {
            const pattern =
              typeof _pattern === 'string' ? { pattern: _pattern } : _pattern;
            return properties.filter(({ name }) => {
              const regex = new RegExp(pattern.pattern, 'gi');
              return name.match(regex) != null && !usedTokens.has(name);
            });
          })
          .reduce((acc, cur) => [...acc, ...cur]);
        for (const token of tokens) {
          usedTokens.add(token.name);
        }
        const variables = tokens.map((token) => {
          const references = dictionary.getReferences(token.original.value);
          const figmaToken = toFigmaToken(token.value);
          let type = figmaToken.type;
          let alias: string;
          let value = figmaToken.value;
          let resolvedValue: { 
            alias?: string | null, 
            aliasName?: string, 
            resolvedValue: any 
          } = {
            resolvedValue: figmaToken.value
          };
          let description = '';
          if (references.length === 1) {
            const referenceParts = token.original.value
              .trim()
              .slice(1, -1)
              .split('.');
            alias = generateTokenId(referenceParts);
            value = { type: 'VARIABLE_ALIAS', id: alias };
            resolvedValue = {
              resolvedValue: figmaToken.value,
              alias,
              aliasName: alias
            };
          } else if (references.length > 1) {
            // figma does not support multi part variables
            value = token.original.value; // save references for manual transforms
            type = 'STRING';
            resolvedValue = {
              resolvedValue: token.original.value,
              alias: null,
              aliasName: undefined
            };
            description = 'TODO: Split into multiple tokens and remove';
          }
          const id = generateTokenId(token.path);
          const name = generateTokenName(token.path);

          return {
            id: id,
            name: name,
            description,
            type,
            valuesByMode: {
              [collection.modes[0].id]: value
            },
            resolvedValuesByMode: {
              [collection.modes[0].id]: resolvedValue
            },
            scopes: ['ALL_SCOPES'],
            hiddenFromPublishing: false,
            codeSyntax: {
              WEB:
                '--digi--' +
                token.path.filter((path) => path !== 'default').join('--')
            }
          };
        });
        results.push({
          id: collection.id,
          name: collection.name,
          modes: Object.fromEntries(
            collection.modes.map(({ id, name }) => [id, name])
          ),
          variableIds: variables.map(({ id }) => id),
          variables: variables
        });
      }
      for (const result of results) {
        console.log(result.name, result.variables.length);
        fs.writeJSONSync(result.name + '.json', result);
      }
      return JSON.stringify(results);
    }
  });

  // ----- Transforms
  StyleDictionary.registerTransform({
    name: 'name/pathToDotNotation',
    ...namePathToDotNotation
  } as StyleDictionary.Named<StyleDictionary.Transform>);

  StyleDictionary.registerTransform({
    name: 'name/pathToDashNotation',
    ...namePathToDashNotation
  } as StyleDictionary.Named<StyleDictionary.Transform>);

  StyleDictionary.registerTransform({
    name: 'size/pxToPt',
    type: 'value',
    matcher: function (prop) {
      return prop.value.match(/^[\d.]+px$/);
    },
    transformer: function (prop) {
      return prop.value.replace(/px$/, 'pt');
    }
  });

  StyleDictionary.registerTransform({
    name: 'size/pxToRem',
    type: 'value',
    matcher: function (prop) {
      if (prop.value == null) {
        console.warn(`⚠️  Undefined value of token: ${prop.path.join('--')}`);
        console.warn(
          `   Referencing to the token: ${prop.original.value
            .replace('.value', '')
            .replace(/\./g, '--')}`
        );
        return false;
      }
      return prop.value.match(/^[\d.]+px$/);
    },
    transformer: function (prop) {
      const REM_IN_PIXELS = 16;
      const pxNumber = Number.parseInt(prop.value.replace(/px$/, ''));
      return `${pxNumber / REM_IN_PIXELS}rem`;
    }
  });

  StyleDictionary.registerTransform({
    name: 'name/digiName/css',
    type: 'name',
    transformer: function (token) {
      return 'digi--' + token.path.join('--');
    }
  });

  StyleDictionary.registerTransform({
    name: 'name/digiName/json',
    type: 'name',
    matcher: function (token) {
      return token.filePath.includes('src/styles/');
    },
    transformer: function (token) {
      return token.path.join('.');
    }
  });

  StyleDictionary.registerTransform({
    name: 'name/digiName/dash',
    type: 'name',
    transformer: (token) => token.path.join('--')
  });

  StyleDictionary.registerTransform({
    name: 'name/digiName/js',
    type: 'name',
    matcher: function (token) {
      return token.filePath.includes('src/styles/');
    },
    transformer: function (token) {
      let digiName = token.path.join('_').replace(/-/g, '_').toUpperCase();
      return digiName;
    }
  });

  StyleDictionary.registerTransform({
    name: 'name/qlikSense/json',
    type: 'name',
    matcher: function (token) {
      return token.filePath.includes('src/styles/');
    },
    transformer: function (token) {
      let qsName = `@${token.path.join('-')}`;
      return qsName;
    }
  });

  StyleDictionary.registerTransform({
    name: 'name/sketch/txt',
    type: 'name',
    matcher: function (token) {
      return token.filePath.includes('src/styles');
    },
    transformer: function (token) {
      return `${token.path.join('/').replace('global/color/', '')}`;
    }
  });

  // ----- TransformGroups
  StyleDictionary.registerTransformGroup({
    name: 'styleguide',
    transforms: [
      'name/digiName/css',
      'time/seconds',
      'color/css',
      'size/pxToRem'
    ]
  });

  StyleDictionary.registerTransformGroup({
    name: 'tokens-js',
    transforms: ['name/digiName/js', 'size/px', 'color/hex']
  });

  StyleDictionary.registerTransformGroup({
    name: 'styleguide',
    transforms: [
      'attribute/cti',
      'name/digiName/css',
      'time/seconds',
      'color/css',
      'size/pxToRem'
    ]
  });

  StyleDictionary.registerTransformGroup({
    name: 'tokens-json',
    transforms: [
      'name/digiName/json',
      'time/seconds',
      'color/css',
      'size/pxToRem'
    ]
  });

  StyleDictionary.registerTransformGroup({
    name: 'qlikSense-json',
    transforms: [
      'name/qlikSense/json',
      'time/seconds',
      'color/css',
      'size/pxToRem'
    ]
  });

  StyleDictionary.registerTransformGroup({
    name: 'sketch-txt',
    transforms: ['name/sketch/txt', 'time/seconds', 'color/css', 'size/pxToRem']
  });

  StyleDictionary.registerTransformGroup({
    name: 'css',
    // to see the pre-defined "css" transformation use: console.log(StyleDictionary.transformGroup['css']);
    transforms: [
      'name/digiName/css',
      'time/seconds',
      'color/css',
      'size/pxToRem'
    ]
  });

  StyleDictionary.registerTransformGroup({
    name: 'scss',
    // to see the pre-defined "scss" transformation use: console.log(StyleDictionary.transformGroup['scss']);
    transforms: [
      'name/digiName/css',
      'time/seconds',
      'color/css',
      'size/pxToRem'
    ]
  });

  StyleDictionary.registerTransformGroup({
    name: 'print',
    transforms: [
      'attribute/cti',
      'name/digiName/css',
      'time/seconds',
      'color/css'
    ]
  });

  StyleDictionary.registerFileHeader({
    name: 'myCustomHeader',
    fileHeader: function () {
      // console.log(defaultMessage);
      const date = format(new Date(), 'yyyy-MM-dd', { locale: sv });
      return [
        `Do not edit directly
 * Autogenerated`
      ];
    }
  });

  StyleDictionary.registerFileHeader({
    name: 'myCustomHeaderSCSS',
    fileHeader: function () {
      // console.log(defaultMessage);
      const date = format(new Date(), 'yyyy-MM-dd', { locale: sv });
      return [
        `Do not edit directly
// Autogenerated`
      ];
    }
  });

  StyleDictionary.buildPlatform(platform);
}
export default new Command()
  .description('Utility to export style dictionary into different platforms')
  .option('-p, --platform <type>', 'Platform type, e.g., json/flat')
  .option('-o, --output <directory>', 'Output directory, e.g., dir/lol')
  .option('-c, --config <config>', 'Config for specified platform')
  .arguments('<dirs...>')
  .action(async (args, options) => {
    exportTokens(args, options.platform, {
      output: options.output,
      config: options.config != null ? fs.readJSONSync(options.config) : null
    });
  });
