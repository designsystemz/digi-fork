import { Command } from 'commander';
import * as fs from 'fs-extra';
import * as glob from 'glob';

function extractCSSVariables(cssString: string): string[] {
  const variablePattern = /var\(--([a-zA-Z0-9\-]+)\)/g;
  const matches = cssString.matchAll(variablePattern);
  const variables = Array.from(matches, (match) => `--${match[1]}`);
  return variables;
}

function replaceCSSVariables(
  cssString: string,
  cb: (name: string) => string
): string {
  return cssString.replace(/var\((--[a-zA-Z0-9\-]+)\)/g, (_, name) => cb(name));
}

function cssToSDName(name: string) {
  return name.replace(/--digi--/g, '');
}

function cssToSDValue(value: string) {
  return replaceCSSVariables(
    value,
    (variableName) => `{${cssToSDName(variableName)}}`
  );
}

export default new Command()
  .description('Transform token definitions to different formats')
  .arguments('<dirs...>')
  .option('-a, --action <cssToDigiformat>')
  .action(async (dirs, options) => {
    const result = {};
    for (let dir of dirs) {
      const files = glob.sync(dir);
      for (const file of files) {
        const content = await fs.readJSON(file);
        for (let entry of Object.entries(content)) {
          const key = cssToSDName(entry[0]);
          const value = cssToSDValue(entry[1] as string);
          result[key] = value;
        }
      }
    }
    console.log(JSON.stringify(result, null, 2));
  });
