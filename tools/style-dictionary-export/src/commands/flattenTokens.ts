import { Command } from 'commander';
import * as fs from 'fs-extra';
import * as glob from 'glob';
import tinycolor from 'tinycolor2'; // library used by style dictionary

interface ResolvedValue {
  r: number;
  g: number;
  b: number;
  a: number;
}

interface ModeValue {
  type: string;
  id: string;
}

interface ResolvedValueByMode {
  resolvedValue: ResolvedValue;
  alias: string;
  aliasName: string;
}

interface CodeSyntax {}

interface FigmaToken {
  id: string;
  name: string;
  description: string;
  type: string;
  valuesByMode: {
    [key: string]: ModeValue;
  };
  resolvedValuesByMode: {
    [key: string]: ResolvedValueByMode;
  };
  scopes: string[];
  hiddenFromPublishing: boolean;
  codeSyntax: CodeSyntax;
}

function parseFigmaColor(color: ResolvedValue) {
  const colors255 = Object.entries(color).map(([channel, value]) => [
    channel,
    Number(value) * 255
  ]);
  const color255Obj = Object.fromEntries(colors255);
  const tc = tinycolor(color255Obj);
  return tc.toHexString();
}

function digiName(name: string): string {
  return name.replace(/\//g, '--');
}

function digiReference(name: string): string {
  return `{${name}}`;
}

function flattenFigmaToken(
  obj: Partial<FigmaToken>,
  mode: string
): [string, string] {
  const id = digiName(obj.name as string);
  const resolvedValue = obj?.resolvedValuesByMode?.[mode];

  if (!resolvedValue) {
    return [id, "UNRESOLVED VALUE"]
  }

  let value = resolvedValue.resolvedValue.toString();
  if (resolvedValue.alias != null) {
    // reference to another variable
    value = digiReference(digiName(resolvedValue.aliasName));
  } else {
    if (obj.type === 'COLOR') {
      value = parseFigmaColor(resolvedValue.resolvedValue);
    }
  }
  return [id, value];
}

export default new Command()
  .description('Flatten Token Definitions from different formats')
  .arguments('<dirs...>')
  .option('-a, --action <figmaToDigi>')
  .action(async (dirs, options) => {
    let results = {};
    for (const dir of dirs) {
      const files = glob.sync(dir);
      for (const file of files) {
        const content = await fs.readJSON(file);
        const mode = Object.keys(content.modes).at(0);
        if (!mode) {
          console.warn('⚠️ No modes in token definitions, skip file ' + file)
          continue
        }
        const tokens = content.variables
          .map((token) => {
            return flattenFigmaToken(token, mode);
          })
          .filter(([id]) => !id.includes('#'));
        const tokenObj = Object.fromEntries(tokens);
        results = {
          ...results,
          ...tokenObj
        };
      }
    }
    console.log(JSON.stringify(results, null, 2));
  });
