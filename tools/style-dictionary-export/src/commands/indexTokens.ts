import { Command } from 'commander';
import * as fs from 'fs-extra';
import * as glob from 'glob';

export default new Command()
  .description('Count occurence of design token usage per file')
  .option(
    '-t, --tokens <tokenFile>',
    'Token file in dash format, separated by (--)'
  )
  .option('-f, --format [json|csv]', 'Output format of the results')
  .arguments('<dirs...>')
  .action(async (dirs, options) => {
    let index = {};
    let contentCache = new Map();
    const tokenContent = await fs.readFile(options.tokens);
    const json = JSON.parse(tokenContent.toString());
    const tokenEntries = Object.entries(json.tokens);
    for (let [name, value] of tokenEntries) {
      if (index[name] == null) {
        index[name] = 0;
      }
      for (let dir of dirs) {
        const files = glob
          .sync(dir)
          .filter((file) => !file.includes('src/design-tokens'));
        for (const file of files) {
          try {
            if (!contentCache.has(file)) {
              const content = await fs.readFile(file, 'utf8');
              contentCache.set(file, content);
            }
            const content = contentCache.get(file);
            if (content.indexOf(name) > -1) {
              index[name] = index[name] + 1;
            }
          } catch (error) {
            console.error(`Error processing ${file}: ${error}`);
            console.log(error);
          }
        }
      }
    }

    if (options.format === 'csv') {
      let csv = 'token,value\n';
      for (let [token, value] of Object.entries(index)) {
        csv += `${token},${value}\n`;
      }
      console.log(csv);
    } else if (options.format === 'json') {
      console.log(JSON.stringify(index, null, 2));
    }
  });
