# Token Tailor 👗👔

Verktyg för att hantera, analysera och transformera Design Tokens i Digi

## Compile to JS

`nx build style-dictionary-export`

Hjälpkommando

`node dist/tools/style-dictionary-export --help`

## Exempel

### Exportera CSS-variabler till JSON-lista

```bash
node dist/tools/style-dictionary-export extract-css libs/arbetsformedlingen/package/**/*.scss > css-variables.json
```

### Ta bort prefix (--digi--) från JSON-lista med CSS-variabler
```bash
node dist/tools/style-dictionary-export transform css-variables.json > css-variables-no-digi.json
```

### Konvertera JSON-lista med variabler till Style Dictionary-format

```bash
node dist/tools/style-dictionary-export nest consumers/arbetsformedlingen/theme.json > style-dictionary.json
```

### Exportera Style Dictionary till Figma-variabler

```bash
node dist/tools/style-dictionary-export export -p figma --config consumers/arbetsformedlingen/figma.json sd-tokens.json
```

### Konvertera Style Dictionary-format till JSON-lista
```bash
node dist/tools/style-dictionary-export export -o dist/tokens-merged-flat-dash -p json/dash libs/arbetsformedlingen/design-tokens/src/styles/**/*.json
```

### Lägg till "default" på variabler som har undervariabler

Exempel

* typography--color--font--h1
* typography--color--font--h1--hover
* typography--color--font--h1--active
* typography--color--font--h2
* typography--color--font--h2--hover
* typography--color--font--h2--active

Blir till

* typography--color--font--h1--default
* typography--color--font--h1--hover
* typography--color--font--h1--active
* typography--color--font--h2--default
* typography--color--font--h2--hover
* typography--color--font--h2--active

Verktyg skapat för att unvikda kollisioner vid export till Figma

```bash
node dist/tools/style-dictionary-export validate --fix consumers/arbetsformedlingen/theme.json > flat-fix.json
```