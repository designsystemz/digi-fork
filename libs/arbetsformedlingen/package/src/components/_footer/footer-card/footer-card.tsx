import { Component, h, Host, Prop, Element } from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { FooterCardVariation } from './footer-card-variation.enum';

/**
 * @private internal use only
 *
 * @slot mySlot - Slot description, if any
 *
 * @enums myEnum - footer-card-myEnum.enum.ts
 *
 * @swedishName Sidfotkort
 */
@Component({
  tag: 'digi-footer-card',
  styleUrl: 'footer-card.scss',
  scoped: true
})
export class FooterFooterCard {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Sätter innehållstyp för footerkortet, så som länklista med, utan ikon eller om kortet ska ha en kantlinje
   * @en Sets content typ for the footercard, such as linklist with and without icon and if the card should have a border or not.
   */
  @Prop() afType: FooterCardVariation = FooterCardVariation.NONE;

  get cssModifiers() {
    return {
      'digi-footer-card--icon': this.afType == FooterCardVariation.ICON,
      'digi-footer-card--border': this.afType == FooterCardVariation.BORDER
    };
  }

  render() {
    return (
      <Host>
        <div
          class={{
            'digi-footer-card': true,
            ...this.cssModifiers
          }}>
          <div class="digi-footer-card__content">
            <slot></slot>
          </div>
        </div>
      </Host>
    );
  }
}
