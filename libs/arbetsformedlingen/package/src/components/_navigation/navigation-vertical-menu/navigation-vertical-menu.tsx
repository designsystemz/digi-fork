/* eslint-disable no-inner-declarations */
/* eslint-disable @typescript-eslint/no-explicit-any */

import {
  Component,
  Element,
  Prop,
  Listen,
  h,
  Event,
  EventEmitter,
  Method,
  State
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { NavigationVerticalMenuVariation } from './navigation-vertical-menu-variation.enum';
import { NavigationVerticalMenuActiveIndicatorSize } from './navigation-vertical-menu-active-indicator-size.enum';

/**
 * @slot default - Måste innehålla en eller flera digi-navigation-vertical-menu-item
 *
 * @enums NavigationVerticalMenuVariation - navigation-vertical-menu-variation.enum.ts
 * @enums NavigationVerticalMenuActiveIndicatorSize = navigation-vertical-menu-active-indicator-size.enum.ts
 *@swedishName Vertikal meny
 */
@Component({
  tag: 'digi-navigation-vertical-menu',
  styleUrls: ['navigation-vertical-menu.scss'],
  scoped: true
})
export class NavigationVerticalMenu {
  @Element() hostElement: HTMLStencilElement;

  @State() listItems: any = [];

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-navigation-vertical-menu');

  /**
   * Sätter variant. Kan vara 'primary' eller 'secondary'.
   * @en Set variation of vertical menu. Can be 'primary' or 'secondary'.
   * @ignore
   */
  @Prop() afVariation: NavigationVerticalMenuVariation =
    NavigationVerticalMenuVariation.PRIMARY;

  /**
   * Sätter variant. Kan vara 'primary' eller 'secondary'.
   * @en Set variation of vertical menu. Can be 'primary' or 'secondary'.
   */
  @Prop() afActiveIndicatorSize: NavigationVerticalMenuActiveIndicatorSize =
    NavigationVerticalMenuActiveIndicatorSize.PRIMARY;

  /**
   * Sätt attributet 'aria-label'.
   * @en Set button `aria-label` attribute.
   */
  @Prop() afAriaLabel: string;

  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet.
   * @en When the component and slots are loaded and initialized this event will trigger.
   */
  @Event({
    bubbles: false,
    cancelable: true
  })
  afOnReady: EventEmitter;

  @Listen('afOnClick')
  linkClickHandler(e: any): void {
    this.setCurrentActiveLevel(e.target);
    this.setClasses(e.target);
  }

  setClasses(el: HTMLElement) {
    const activeDeepClass = 'digi-navigation-vertical-menu__item--deep-active';
    const activeClass = 'digi-navigation-vertical-menu__item--active';

    const navList = this.hostElement.querySelector(
      '.digi-navigation-vertical-menu__list'
    );
    const activeNavItems = Array.from(navList.children).filter(
      (item: HTMLElement) => item.classList.contains(activeClass)
    );

    activeNavItems.forEach((listItem: HTMLElement) => {
      listItem.classList.add(activeDeepClass);
      setClassRecursive(listItem);
    });

    function setClassRecursive(listItem: HTMLElement) {
      const arr = Array.from(listItem.children).filter(
        (item) => item.tagName === 'UL'
      );
      const list = arr[0];
      const activeListItems = Array.from(list.children).filter((item) =>
        item.classList.contains(activeClass)
      );

      if (activeListItems.length == 0) {
        removeClassRecursive(list as HTMLElement);
        return;
      }

      activeListItems.forEach((item: HTMLElement) => {
        item.classList.add(activeDeepClass);
        setClassRecursive(item);
      });
    }

    function removeClassRecursive(item: HTMLElement, deepElement = true) {
      const parentEl = item.closest('li');
      if (!parentEl) {
        return;
      }

      if (!deepElement) {
        parentEl.classList.remove(activeDeepClass);
      }

      const deepParent = parentEl.closest('ul');
      if (!deepParent) {
        return;
      }
      removeClassRecursive(deepParent, false);
    }

    const removeUnselected = this.hostElement.querySelectorAll(
      `.${activeDeepClass}:not(.${activeClass})`
    );
    removeUnselected.forEach((item: HTMLElement) => {
      item.classList.remove(activeDeepClass);
    });

    if (!el.matches('.digi-navigation-vertical-menu-item--has-subnav')) {
      removeClassRecursive(el);
    }
  }

  componentWillLoad() {
    this.getListItems();
    this.initNav();
    this.setLevelClass();
  }

  componentDidLoad() {
    this.initCurrentActiveLevel();
    this.afOnReady.emit();
  }

  componentWillUpdate() {
    this.getListItems();
    this.initNav();
    this.setLevelClass();
  }

  initCurrentActiveLevel() {
    let elActiveSubnav = this.hostElement.querySelector(
      'digi-navigation-vertical-menu-item[af-active-subnav="true"]'
    );

    if (elActiveSubnav) {
      function getDeep(elActiveSubnav) {
        const deepEl = elActiveSubnav
          .closest('li')
          .querySelector('ul')
          .querySelector(
            'digi-navigation-vertical-menu-item[af-active-subnav="true"]'
          );
        return deepEl ? deepEl : null;
      }

      function checkRecursive(deepEl) {
        const elToCheck = getDeep(deepEl);
        if (elToCheck) {
          elActiveSubnav = elToCheck;
          checkRecursive(elToCheck);
        } else {
          elActiveSubnav = deepEl;
        }
      }
      checkRecursive(elActiveSubnav);

      this.setCurrentActiveLevel(elActiveSubnav);
    }
  }

  setCurrentActiveLevel(targetElement) {
    const currentLevelClass =
      'digi-navigation-vertical-menu__item--current-level';
    const target = targetElement;
    const oldTarget = this.hostElement.querySelectorAll(
      '.digi-navigation-vertical-menu__item--current-level'
    );
    oldTarget.forEach((el) => {
      el.classList.remove(currentLevelClass);
    });

    let subnav = target.closest('li').querySelector('ul');

    if (target.matches('.digi-navigation-vertical-menu-item--has-subnav')) {
      subnav = target.closest('ul');
      subnav.classList.add(currentLevelClass);
      return;
    }

    if (subnav) {
      subnav.classList.add(currentLevelClass);
    } else {
      subnav = target.closest('ul');
      subnav.classList.add(currentLevelClass);
    }
  }

  setLevelClass() {
    this.hostElement
      .querySelector('ul')
      .classList.remove('primary', 'secondary');
    this.hostElement
      .querySelector('ul')
      .classList.add(
        'digi-navigation-vertical-menu__list--root',
        this.afVariation
      );
  }

  initNav() {
    const $ul = this.hostElement.querySelectorAll('ul');
    const $li = this.hostElement.querySelectorAll('li');

    $ul.forEach((item) =>
      item.classList.add('digi-navigation-vertical-menu__list')
    );
    $li.forEach((item) =>
      item.classList.add('digi-navigation-vertical-menu__item')
    );
  }

  @Method()
  async afMSetCurrentActiveLevel(e: any) {
    this.setCurrentActiveLevel(e);
    this.initNav();
    this.setClasses(e);
  }

  getListItems() {
    const menuItems = this.hostElement.children;

    if (!menuItems || menuItems.length <= 0) {
      logger.warn(`The slot contains no children elements.`, this.hostElement);
      return;
    }

    this.listItems = [...Array.from(menuItems)].map((filter) => {
      return {
        outerHTML: filter.outerHTML
      };
    });
  }

  get cssModifiers() {
    return {
      'digi-navigation-vertical-menu': false,
      'digi-navigation-vertical-menu--primary':
        this.afVariation === NavigationVerticalMenuVariation.PRIMARY,
      'digi-navigation-vertical-menu--secondary':
        this.afVariation === NavigationVerticalMenuVariation.SECONDARY,
      'digi-navigation-vertical-menu--indicator--secondary':
        this.afActiveIndicatorSize ===
        NavigationVerticalMenuActiveIndicatorSize.SECONDARY
    };
  }

  render() {
    return (
      <nav
        class={{
          'digi-navigation-vertical-menu': true,
          ...this.cssModifiers
        }}
        id={this.afId}
        aria-label={this.afAriaLabel}>
        <slot></slot>
      </nav>
    );
  }
}
