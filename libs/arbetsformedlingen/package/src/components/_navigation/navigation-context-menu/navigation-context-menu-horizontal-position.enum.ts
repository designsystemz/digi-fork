export enum NavigationContextMenuHorizontalPosition {
  START = 'start',
  END = 'end'
}
