export enum NavigationSidebarCloseButtonPosition {
  START = 'start',
  END = 'end'
}
