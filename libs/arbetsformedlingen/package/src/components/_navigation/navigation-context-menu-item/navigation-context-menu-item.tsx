import { Component, Prop, Element } from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { logger } from '../../../global/utils/logger';
import { NavigationContextMenuItemType } from './navigation-context-menu-item-type.enum';

/**
 * @private deprecated, but are being used in language-picker. Remove in Issue-334
 * @deprecated  Denna komponent kommer tas bort från Designsystemet i slutet av augusti 2024. Använd <digi-context-menu/> istället.
 * @enums NavigationContextMenuItemType - navigation-context-menu-item-type.enum.ts
 * @swedishName Rullgardinsmenyval
 */
@Component({
  tag: 'digi-navigation-context-menu-item',
  scoped: true
})
export class NavigationContextMenuItem {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Sätter texten i komponenten
   * @en Sets the item text
   */
  @Prop() afText!: string;

  /**
   * Sätter attributet 'href'.
   * @en Set href attribute
   */
  @Prop() afHref: string;

  /**
   * Sätter typ. Kan vara 'link' eller 'button'.
   * @en Set type. Can be 'link' or 'button'
   */
  @Prop() afType!: NavigationContextMenuItemType;

  /**
   * Sätter attributet 'lang'.
   * @en Set lang attribute
   */
  @Prop() afLang: string;

  /**
   * Sätter attributet 'dir'.
   * @en Set dir attribute
   */
  @Prop() afDir: string;

  componentWillLoad() {
    if (
      this.afType !== NavigationContextMenuItemType.BUTTON &&
      this.afType !== NavigationContextMenuItemType.LINK
    ) {
      logger.warn(
        `The navigation-context-menu-item is of unallowed afType. Only button and link is allowed.`,
        this.hostElement
      );
      return;
    }
  }
}
