export enum NavigationContextMenuItemType {
  LINK = 'link',
  BUTTON = 'button'
}
