import {
  Component,
  h,
  Prop,
  State,
  Event,
  EventEmitter,
  Listen,
  Watch
} from '@stencil/core';
import { isAfter, isBefore, isValid } from 'date-fns';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';
import { logger } from '../../../global/utils/logger';
import { _t } from '../../../text';

/**
 * @slot calendar-footer - displayed beneath the calendar. Preferable buttons.
 *
 */

export interface YearAndMonth {
  year: number;
  month: number;
}
/**
 * @swedishName Kalender
 */
@Component({
  tag: 'digi-calendar',
  styleUrls: ['calendar.scss'],
  scoped: true
})
export class Calendar {
  @State() selectedDates: Date[] = [];
  @State() focusedDate: Date;
  @State() dirty = false;

  private _tbody: HTMLElement;
  private _nextButton: HTMLElement;
  private _yearSelect?;
  private _yearSelectInputElement?;

  /**
   * Prefix för komponentens alla olika interna 'id'-attribut på kalendern etc. Ex. 'my-cool-id' genererar 'my-cool-id-calendar' osv. Ges inget värde genereras ett slumpmässigt.
   * @en Prefix for all id attributes inside the component. Example: 'my-cool-id' generates 'my-cool-id-calendar' and so on. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-calendar');

  /**
   * Vald månad att visa från start. Förvalt är den månad som dagens datum har. (Januari = 0, December = 11)
   * @en Selected month to display from start. Defaults to the month of todays month. (Januray = 0, December = 11)
   */
  @Prop() afInitSelectedMonth: number = new Date().getMonth();

  /**
   * Valt måndad och år från start. Förvalt är dagens månad och år.
   * @en Selected month and year to display from start. Defaults to todays month and year.
   */
  @Prop() afInitSelectedDate: Date = new Date();

  /**
   * Visa veckonummer i kalender. Förvalt är false
   * @en Show week number in the calander. Default is false
   */
  @Prop() afShowWeekNumber = false;

  /**
   * Valt datum i kalendern
   * @en Selected date in calendar
   */
  @Prop() afSelectedDate: Date;
  @Watch('afSelectedDate')
  afSelectedDateChangeHandler() {
    if (isValid(this.afSelectedDate)) {
      this.selectedDates = [this.afSelectedDate];
    } else {
      this.selectedDates = [];
    }
  }

  /**
   * Valda datum i kalendern
   * @en Selected dates in calendar
   */
  @Prop() afSelectedDates: Date[] = [];
  @Watch('afSelectedDates')
  afSelectedDatesChangeHandler() {
    if (this.afSelectedDates.length > 0) {
      if (this.afMultipleDates) {
        const invalidDate = this.afSelectedDates.find((date) => !isValid(date));
        if (invalidDate === undefined) {
          this.selectedDates = this.afSelectedDates;
        }
      } else if (isValid(this.afSelectedDates[0])) {
        this.selectedDates = [this.afSelectedDates[0]];
      }
    } else {
      this.selectedDates = [];
    }
  }

  /**
   * Visar döljer kalendern. Visas som förvalt
   * @en Toggle calendar hide/show. Defaults to true
   */
  @Prop() afActive = true;

  /**
   * Tidigaste valbara datumet
   * @en Earliest selectable date
   */
  @Prop() afMinDate: Date;

  /**
   * Senaste valbara datumet
   * @en Latest selectable date
   */
  @Prop() afMaxDate: Date;

  /**
   * Sätt denna till true för att kunna markera mer en ett datum i kalendern. Förvalt är false
   * @en Set this to true to be able to select multiple dates in the calander. Default is false
   */
  @Prop() afMultipleDates = false;

  /**
   * Sätt denna till true för att kunna välja år genom en väljare. Förvalt är false
   * @en Set this to true to be able to select year through a select component. Default is false
   */
  @Prop() afYearSelect = false;

  /**
   * Sker när ett datum har valts eller avvalts. Returnerar datumen i en array.
   * @en When a date is selected. Return the dates in an array.
   */
  @Event() afOnDateSelectedChange: EventEmitter;

  /**
   * Sker när fokus flyttas utanför kalendern
   * @en When focus moves out from the calendar
   */
  @Event() afOnFocusOutside: EventEmitter;

  /**
   * Sker vid klick utanför kalendern
   * @en When click outside the calendar
   */
  @Event() afOnClickOutside: EventEmitter;

  /**
   * Sker när kalenderdatumen har rörts första gången, när värdet på focusedDate har ändrats första gången.
   * @en When the calendar dates are touched the first time, when focusedDate is changed the first time.
   */
  @Event() afOnDirty: EventEmitter;

  activeCalendar = [];
  lastWeekOfPreviousMonth: Date[];
  firstWeekOfNextMonth: Date[];
  weeksBetween: number;
  weekDaysbetween: Date[];
  months = [];

  @Listen('focusin', { target: 'document' })
  focusoutHandler(e: FocusEvent): void {
    const target = e.target as HTMLElement;
    const selector = `#${this.afId}-calendar`;

    if (detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    }
  }

  @Listen('click', { target: 'window' })
  clickHandler(e: MouseEvent): void {
    const target = e.target as HTMLElement;
    const selector = `#${this.afId}-calendar`;

    if (detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    }
  }

  drawCalendar(focusedDate: Date) {
    const date: YearAndMonth = {
      year: focusedDate.getFullYear(),
      month: focusedDate.getMonth()
    };

    this.lastWeekOfPreviousMonth = this.getWeekDays(
      this.getLastDayOfPrevMonth(date)
    );
    this.firstWeekOfNextMonth = this.getWeekDays(
      this.getFirstDayOfNextMonth(date)
    );
    this.weeksBetween = this.getWeeksBetween(
      this.getLastDayOfPrevMonth(date),
      this.getFirstDayOfNextMonth(date)
    );
    this.weekDaysbetween = this.getWeekdaysOfMonth(this.weeksBetween);
    this.months = [
      'januari',
      'februari',
      'mars',
      'april',
      'maj',
      'juni',
      'juli',
      'augusti',
      'september',
      'oktober',
      'november',
      'december'
    ];

    this.activeCalendar = this.appendFirstWeekOfNextMonth()
      ? [
          this.lastWeekOfPreviousMonth,
          ...this.weekDaysbetween,
          this.appendFirstWeekOfNextMonth()
        ]
      : [this.lastWeekOfPreviousMonth, ...this.weekDaysbetween];

    this.appendFirstWeekOfNextMonth();

    if (this.afShowWeekNumber) {
      this.activeCalendar.forEach((el, i) => {
        this.activeCalendar[i].unshift(this.getWeekNumbers(el[0]));
      });
    }
  }

  appendFirstWeekOfNextMonth() {
    const lastArray = JSON.stringify(this.weekDaysbetween.slice(-1)[0]);
    const stringFirstweek = JSON.stringify(this.firstWeekOfNextMonth);

    if (
      !this.firstWeekOfNextMonth.some((date: Date) =>
        this.isSameMonth(date, this.focusedDate)
      )
    ) {
      return;
    } else if (lastArray !== stringFirstweek) {
      return this.firstWeekOfNextMonth;
    }
  }

  getLastDayOfPrevMonth(date: YearAndMonth): Date {
    return new Date(date.year, date.month, 0);
  }

  getWeekDays(date?: Date): Date[] {
    const today = date || new Date();
    const week = [];

    for (let i = 1; i <= 7; i++) {
      const first = today.getDate() - today.getDay() + i;
      const day = new Date(today.setDate(first));
      week.push(day);
    }
    return week;
  }

  getFirstDayOfNextMonth(date: YearAndMonth): Date {
    return new Date(date.year, date.month + 1, 1);
  }

  getWeeksBetween(lastDayOfPrevMonth: Date, firstDayOnNextMonth: Date) {
    const week = 7 * 24 * 60 * 60 * 1000;
    const d1ms = lastDayOfPrevMonth.getTime();
    const d2ms = firstDayOnNextMonth.getTime();
    const diff = Math.abs(d2ms - d1ms);
    return Math.floor(diff / week);
  }

  getWeekdaysOfMonth(weeksBetween: number) {
    let weeks = [];
    for (let i = 1; i <= weeksBetween; i++) {
      weeks = [...weeks, this.getWeekDays(this.getUpcoming(i))];
    }
    return weeks;
  }

  getUpcoming(weeks: number) {
    return new Date(
      this.focusedDate.getFullYear(),
      this.focusedDate.getMonth(),
      7 * weeks
    );
  }

  onDirty() {
    if (!this.dirty) {
      this.dirty = true;
      this.afOnDirty.emit(this.dirty);
    }
  }

  selectDateHandler(dateSelected) {
    this.onDirty();

    if (!this.isDateSelectable(dateSelected)) return;

    if (this.isDateSelected(dateSelected)) {
      this.selectedDates = this.afMultipleDates
        ? this.selectedDates.filter((d) => {
            return !this.isSameDate(d, dateSelected);
          })
        : [];
    } else {
      this.selectedDates = this.afMultipleDates
        ? [...this.selectedDates, dateSelected]
        : [dateSelected];
    }
    this.focusedDate = new Date(dateSelected);
    this.afOnDateSelectedChange.emit(this.selectedDates);
  }

  getMonthName(monthNumber: number) {
    return new Date(1992, monthNumber, 1)
      .toLocaleDateString('default', {
        month: 'short'
      })
      .replace('.', '');
  }

  getFullMonthName(monthNumber: number) {
    return new Date(
      this.focusedDate.getFullYear(),
      monthNumber,
      1
    ).toLocaleDateString('default', {
      month: 'long',
      year: 'numeric'
    });
  }

  // Get the week number of a specific date. Taken from https://weeknumber.com/how-to/javascript
  getWeekNumbers(dateInput: Date) {
    const date = new Date(dateInput);
    date.getTime();
    date.setHours(0, 0, 0, 0);
    date.setDate(date.getDate() + 3 - ((date.getDay() + 6) % 7));
    const week1 = new Date(date.getFullYear(), 0, 4);
    return (
      1 +
      Math.round(
        ((date.getTime() - week1.getTime()) / 86400000 -
          3 +
          ((week1.getDay() + 6) % 7)) /
          7
      )
    );
  }

  isSameMonth(date: Date, currentDate: Date) {
    if (date instanceof Date && date instanceof Date) {
      return (
        `${date.getFullYear()}${date.getMonth()}` ===
        `${currentDate.getFullYear()}${currentDate.getMonth()}`
      );
    }
    return false;
  }

  isSameDate(date: Date, newDate: Date): boolean {
    return this.getDateString(date) === this.getDateString(newDate);
  }
  getDateString(date: Date): string {
    return `${date.getDate()}${date.getMonth()}${date.getFullYear()}`;
  }
  isDateSelected(date: Date): boolean {
    return !!this.selectedDates.find((d) => this.isSameDate(date, d));
  }
  isDateSelectable(date: Date): boolean {
    if (this.afMinDate && this.isBefore(date, this.afMinDate)) return false;
    if (this.afMaxDate && this.isAfter(date, this.afMaxDate)) return false;
    return true;
  }
  isBefore(a: Date, b: Date): boolean {
    return isBefore(this.dateWithoutTime(a), this.dateWithoutTime(b));
  }
  isAfter(a: Date, b: Date): boolean {
    return isAfter(this.dateWithoutTime(a), this.dateWithoutTime(b));
  }
  dateWithoutTime(date: Date): Date {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate());
  }
  isDisabledDate(date: Date) {
    return !this.isSameMonth(date, this.focusedDate);
  }

  subtractMonth() {
    const month = new Date(this.focusedDate);
    const currentDay = month.getDate();
    month.setMonth(month.getMonth() - 1, 1);
    const lastDayOfPrevMonth = new Date(
      month.getFullYear(),
      month.getMonth() + 1,
      0
    ).getDate();
    month.setDate(Math.min(currentDay, lastDayOfPrevMonth));
    this.afInitSelectedMonth = month.getMonth();
    this.focusedDate = month;
  }

  addMonth() {
    const month = new Date(this.focusedDate);
    const currentDay = month.getDate();
    month.setMonth(month.getMonth() + 1, 1);
    const lastDayOfNextMonth = new Date(
      month.getFullYear(),
      month.getMonth() + 1,
      0
    ).getDate();
    month.setDate(Math.min(currentDay, lastDayOfNextMonth));

    this.afInitSelectedMonth = month.getMonth();
    this.focusedDate = month;
  }

  getCurrent(date: Date) {
    return this.isSameDate(date, new Date()) ? 'date' : null;
  }

  setTabIndex(date: Date) {
    if (this.dirty) {
      return this.isSameDate(date, this.focusedDate) ? 0 : -1;
    } else {
      return this.isSameMonth(date, new Date())
        ? this.isSameDate(date, new Date()) && !this.isDisabledDate(date)
          ? 0
          : -1
        : this.isSameMonth(date, this.focusedDate) && date.getDate() === 1
          ? 0
          : -1;
    }
  }

  navigateHandler(e: CustomEvent, daysToAdd: number) {
    !!e.detail && e.detail.preventDefault();

    if (e.detail.key === 'Tab') {
      return;
    }

    this.onDirty();
    const date = new Date(e.detail.target.dataset.date);
    this.focusedDate = new Date(date.setDate(date.getDate() + daysToAdd));
    this.afInitSelectedMonth = this.focusedDate.getMonth();
    this.resetFocus();
  }

  shiftTabHandler(): void {
    this.afYearSelect
      ? this._yearSelectInputElement.focus()
      : this._nextButton.focus();
  }

  resetFocus() {
    setTimeout(() => {
      const dates = this._tbody.querySelectorAll('.digi-calendar__date');

      if (dates.length <= 0) {
        logger.warn(
          `Could not find an element with data-date attribute matching navigated date.`,
          this._tbody
        );
        return;
      }

      Array.from(dates).map((date) => {
        const d = date as HTMLElement;
        d.tabIndex = -1;
        d.classList.remove('digi-calendar__date--focused');

        if (d.dataset.date === this.getFullDate(this.focusedDate)) {
          d.focus();
          d.tabIndex = 0;
          d.classList.add('digi-calendar__date--focused');
        }
      });
    }, 100);
  }

  getFullDate(date: Date) {
    return date.toLocaleString('default', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric'
    });
  }

  getDateName(date: Date) {
    return date.toLocaleString('default', {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    });
  }

  setFocused(date: Date) {
    if (this.dirty) {
      return this.isSameDate(date, this.focusedDate);
    } else {
      return this.isSameMonth(date, new Date())
        ? this.isSameDate(date, new Date()) && !this.isDisabledDate(date)
        : this.isSameMonth(date, this.focusedDate) && date.getDate() === 1;
    }
  }

  ariaPressed(date: Date) {
    return this.selectedDates.length > 0 &&
      !!this.selectedDates.find((d) => this.isSameDate(d, date))
      ? 'true'
      : 'false';
  }

  selectMonth(evt) {
    const tempDate = new Date(this.focusedDate);
    const selectedMonth = parseInt(evt.target.value, 10);
    const currentDay = tempDate.getDate();
    tempDate.setMonth(selectedMonth, 1);
    const lastDayOfSelectedMonth = new Date(
      tempDate.getFullYear(),
      selectedMonth + 1,
      0
    ).getDate();
    tempDate.setDate(Math.min(currentDay, lastDayOfSelectedMonth));

    this.afInitSelectedMonth = selectedMonth;
    this.focusedDate = tempDate;
    this.drawCalendar(this.focusedDate);
  }
  selectYear(evt) {
    const tempDate = this.focusedDate;
    tempDate.setFullYear(evt.target.value);
    this.afInitSelectedDate = evt.target.value;
    this.focusedDate = tempDate;
    this.drawCalendar(this.focusedDate);
  }
  earliestSelectableYear() {
    if (this.afMinDate) return this.afMinDate.getFullYear();
    return 1900;
  }
  latestSelectableYear() {
    if (this.afMaxDate) return this.afMaxDate.getFullYear();
    return new Date(Date.now()).getFullYear() + 10;
  }
  allSelectableYears() {
    const allYears: Array<number> = [];
    let currentYear: number = this.earliestSelectableYear();
    while (currentYear <= this.latestSelectableYear()) {
      allYears.push(currentYear++);
    }
    return allYears;
  }

  componentWillLoad() {
    if (this.afInitSelectedDate) {
      this.afInitSelectedMonth = this.afInitSelectedDate.getMonth();
      this.focusedDate = this.afInitSelectedDate;
      this.drawCalendar(this.afInitSelectedDate);
    } else {
      const startSelectedMonth = new Date(
        new Date().getFullYear(),
        this.afInitSelectedMonth
      );
      this.focusedDate = startSelectedMonth;
      this.drawCalendar(startSelectedMonth);
    }
  }

  componentWillUpdate() {
    this.drawCalendar(this.focusedDate);
  }

  async componentDidLoad() {
    if (isValid(this.afSelectedDate))
      this.selectedDates = [this.afSelectedDate];
    else this.afSelectedDatesChangeHandler();

    await customElements.whenDefined('digi-form-select');
    if (this.afYearSelect)
      this._yearSelectInputElement =
        await this?._yearSelect?.afMGetFormControlElement();
  }

  cssDateModifiers(date: Date) {
    return {
      'digi-calendar__date--today':
        this.isSameDate(date, new Date()) && !this.isDisabledDate(date),
      'digi-calendar__date--disabled': this.isDisabledDate(date),
      'digi-calendar__date--focused': this.setFocused(date),
      'digi-calendar__date--selected': this.isDateSelected(date),
      'digi-calendar__date--not-selectable':
        !this.isDisabledDate(date) && !this.isDateSelectable(date)
    };
  }
  render() {
    return (
      <div
        class={{
          'digi-calendar': true,
          'digi-calendar--hide': !this.afActive
        }}
        id={`${this.afId}-calendar`}>
        {this.afYearSelect && (
          <div class="digi-calendar__header">
            <div class="digi-calendar__header--year-select">
              <digi-form-select
                afLabel="Månad"
                af-variation="small"
                afValue={this.focusedDate.getMonth().toString()}
                onAfOnSelect={(evt) => {
                  this.selectMonth(evt);
                }}>
                {this.months.map((month, index) => {
                  return <option value={index}>{month}</option>;
                })}
              </digi-form-select>

              <digi-form-select
                afLabel="År"
                af-variation="small"
                afValue={this.focusedDate.getFullYear().toString()}
                onAfOnSelect={(evt) => {
                  this.selectYear(evt);
                }}
                ref={(el) => (this._yearSelect = el)}>
                {this.allSelectableYears().map((year) => (
                  <option value={year.toString()}>{year}</option>
                ))}
              </digi-form-select>
            </div>
          </div>
        )}

        {!this.afYearSelect && (
          <div class="digi-calendar__header">
            <div class="digi-calendar__header--month-step">
              <button
                class="digi-calendar__month-select-button"
                type="button"
                aria-label={
                  this.getFullMonthName(this.afInitSelectedMonth - 1) +
                  ', föregående månad'
                }
                onClick={() => this.subtractMonth()}>
                <digi-icon
                  afName={`chevron-left`}
                  slot="icon"
                  aria-hidden="true"></digi-icon>
                <span class="digi-calendar__month-select-name">
                  {this.getMonthName(this.afInitSelectedMonth - 1)}
                </span>
              </button>
              <div class="digi-calendar__month">
                {this.getFullMonthName(this.afInitSelectedMonth)}
              </div>
              <button
                class="digi-calendar__month-select-button"
                type="button"
                aria-label={
                  this.getFullMonthName(this.afInitSelectedMonth + 1) +
                  ', nästa månad'
                }
                onClick={() => this.addMonth()}
                ref={(el) => (this._nextButton = el)}>
                <span class="digi-calendar__month-select-name">
                  {this.getMonthName(this.afInitSelectedMonth + 1)}
                </span>
                <digi-icon
                  afName={`chevron-right`}
                  slot="icon"
                  aria-hidden="true"></digi-icon>
              </button>
            </div>
          </div>
        )}

        <digi-util-keydown-handler
          onAfOnDown={(e) => this.navigateHandler(e, 7)}
          onAfOnUp={(e) => this.navigateHandler(e, -7)}
          onAfOnLeft={(e) => this.navigateHandler(e, -1)}
          onAfOnRight={(e) => this.navigateHandler(e, 1)}
          onAfOnShiftTab={() => this.shiftTabHandler()}>
          <table role="grid" class="digi-calendar__table">
            <thead>
              <tr>
                {this.afShowWeekNumber && (
                  <th
                    role="gridcell"
                    abbr={_t.calendar.week}
                    class="digi-calendar__table-header-week">
                    {_t.calendar.week.toLowerCase()}
                  </th>
                )}
                {Object.keys(_t.calendar.weekdays).map((day) => (
                  <th
                    role="gridcell"
                    abbr={_t.calendar.weekdays[day]}
                    class="digi-calendar__table-header">
                    {_t.calendar.weekdays_short[day].toLowerCase()}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody ref={(el) => (this._tbody = el)}>
              {this.activeCalendar.map((week) => {
                return (
                  <tr>
                    {week.map((day, i) => {
                      if (this.afShowWeekNumber && i === 0) {
                        return (
                          <td
                            role="gridcell"
                            class="digi-calendar__td-week digi-calendar__date--week-number"
                            id={'digi-calendar__week-' + day}>
                            {day}
                          </td>
                        );
                      } else {
                        return (
                          <td role="gridcell" class="digi-calendar__td">
                            <button
                              type="button"
                              class={{
                                ...this.cssDateModifiers(day),
                                'digi-calendar__date': true
                              }}
                              onClick={() => this.selectDateHandler(day)}
                              // aria-current={this.getCurrent(day)}
                              aria-label={this.getDateName(day)}
                              aria-disabled={
                                this.isDisabledDate(day) ||
                                !this.isDateSelectable(day)
                              }
                              aria-describedby={
                                this.afShowWeekNumber && i === 1
                                  ? 'digi-calendar__week-' + week[0]
                                  : null
                              }
                              tabindex={this.setTabIndex(day)}
                              data-date={this.getFullDate(day)}
                              aria-pressed={this.ariaPressed(day)}>
                              <span aria-hidden="true">{day.getDate()}</span>
                            </button>
                          </td>
                        );
                      }
                    })}
                  </tr>
                );
              })}
            </tbody>
          </table>
        </digi-util-keydown-handler>
        <slot name="calendar-footer"></slot>
      </div>
    );
  }
}
