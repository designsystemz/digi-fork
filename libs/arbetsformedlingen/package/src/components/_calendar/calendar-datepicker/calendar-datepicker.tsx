import {
  Component,
  h,
  Host,
  State,
  Event,
  EventEmitter,
  Element,
  Prop,
  Listen,
  Watch
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { isAfter, isBefore, isValid } from 'date-fns';
import { FormInputValidation } from '../../../enums';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { CalendarDatepickerValidation } from './calendar-datepicker-validation.enum';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';

/**
 * @swedishName Datumväljare
 */
@Component({
  tag: 'digi-calendar-datepicker',
  styleUrl: 'calendar-datepicker.scss',
  scoped: true
})
export class CalendarDatepicker {
  @Element() hostElement: HTMLStencilElement;

  @State() showWrongFormatError = false;
  @State() showDisabledDateError = false;
  @State() showCalendar = false;

  private futureYears = 10;

  @State() userInput = '';

  /**
   * Sätter ett id attribut.
   * @en Sets an id attribute. Standard är slumpmässig sträng.
   */
  @Prop() afId: string = randomIdGenerator('digi-calendar-datepicker');

  /**
   * Texten till labelelementet
   * @en The label text
   */
  @Prop() afLabel = 'Skriv in eller välj datum (åååå-mm-dd)';

  /**
   * Valfri beskrivande text
   * @en Optional description text
   */
  @Prop() afLabelDescription = 'Exempel: 1992-06-26';

  /**
   * Sätter valideringsstatus. Kan vara 'success', 'error', 'warning', 'neutral' eller ingenting.
   * @en Set input validation status
   */
  @Prop() afValidation: CalendarDatepickerValidation =
    CalendarDatepickerValidation.NEUTRAL;

  /**
   * Sätter valideringsmeddelandet
   * @en Set input validation message
   */
  @Prop() afValidationText: string;

  /**
   * Sätter valideringsmeddelandet när du använder afInvalid
   * @en Set input validation message when using afInvalid property
   */
  @Prop() afInvalid = false;

  /**
   * Sätter valideringsmeddelandet när du använder afInvalid
   * @en Set input validation message when using afInvalid property
   */
  @Prop() afValidationMessage = 'Eget meddelande';

  /**
   * Sätter valideringsmeddelandet
   * @en Set input validation message
   */
  @Prop() afValidationWrongFormat = 'Fel format';

  /**
   * Sätter valideringsmeddelandet
   * @en Set input validation message
   */
  @Prop() afValidationDisabledDate = 'Ej valbart datum';

  /**
   * Sätter attributet 'required'.
   * @en Input required attribute
   */
  @Prop() afRequired: boolean;

  /**
   * Sätter text för afRequired.
   * @en Set text for afRequired.
   */

  @Prop() afRequiredText: string;

  /**
   * Valda datum i kalendern
   * @en Selected dates in calendar
   */
  @Prop() afSelectedDates: Array<Date> = [];

  /**
   * Sätter attributet 'value'
   * @en Input value attribute
   * @ignore
   */
  @Prop() value: Array<Date> = [];
  @Watch('value')
  valueChangeHandler() {
    this.afSelectedDates = this.value;
  }

  /**
   * Tidigaste valbara datumet
   * @en Earliest selectable date
   */
  @Prop() afMinDate: Date;

  /**
   * Senaste valbara datumet
   * @en Latest selectable date
   */
  @Prop() afMaxDate: Date;

  /**
   * Visa veckonummer i kalender. Förvalt är false
   * @en Show week number in the calander. Default is false
   */
  @Prop() afShowWeekNumber = false;

  /**
   * Sätt denna till true för att kunna markera mer en ett datum i kalendern. Förvalt är false
   * @en Set this to true to be able to select multiple dates in the calander. Default is false
   */
  @Prop() afMultipleDates = false;
  @Watch('afMultipleDates')
  afMultipleDatesChangeHandler() {
    if (!this.afMultipleDates && this.afSelectedDates.length > 1) {
      this.afSelectedDates = this.afSelectedDates.slice(-1);
      this.value = this.afSelectedDates ?? [];
    }
  }

  /**
   * Sätt denna till true om kalendern ska stängas när användaren väljer ett datum. Förvalt är false
   * @en Set this to true if the calendar should close when a date is selected. Default is false
   */
  @Prop() afCloseOnSelect = false;

  /**
   * Sker vid datum uppdatering
   * @en When date is updated
   */
  @Event() afOnDateChange: EventEmitter<[] | [''] | Date[]>;

  @Listen('click', { target: 'window' })
  clickHandler(e: MouseEvent): void {
    const target = e.target as HTMLElement;
    const selector = `#${this.afId}-datepicker`;

    if (detectClickOutside(target, selector)) {
      this.showCalendar = false;
    }
  }

  connectedCallback() {
    this.hostElement.addEventListener('keyup', this.keyUpHandler.bind(this));
  }

  disconnectedCallback() {
    this.hostElement.removeEventListener('keyup', this.keyUpHandler.bind(this));
  }

  componentDidLoad() {
    if (this.value?.length) {
      this.setValue(this.value);
    }
  }

  get cssModifiers() {
    return {
      'digi-calendar-datepicker__show-week-numbers': this.afShowWeekNumber,
      'digi-calendar-datepicker__show-error': this.getShowError(),
      'digi-calendar-datepicker__show-calendar': this.showCalendar
    };
  }

  keyUpHandler(e: KeyboardEvent) {
    if (e.key === 'Escape' && this.showCalendar) {
      this.showCalendar = false;
      this.focusOnCalendarButton();
      e.stopPropagation();
    }
  }

  focusOnCalendarButton() {
    const calendarButton = this.hostElement.querySelector(
      `#${this.afId}-button`
    ) as HTMLButtonElement;
    calendarButton.focus();
  }

  toggleCalendar() {
    this.showCalendar = !this.showCalendar;
  }

  parseInputString(value: string): Date[] {
    return value
      .split(/[&,]|och/g)
      .map((text) => this.parseDateString(text.trim()));
  }

  resetValueAndState() {
    this.showWrongFormatError = false;
    this.showDisabledDateError = false;
    this.setValue([]);
    this.afOnDateChange.emit(['']);
  }

  validateInputAndShowErrors(dates: Date[]) {
    this.showWrongFormatError = !!dates.find((date) => !isValid(date));
    this.showDisabledDateError = !!dates.find(
      (date) => !this.isDateSelectable(date)
    );
  }

  setValue(value: Date[]) {
    this.afSelectedDates = value;
    this.value = value;
  }

  parseInput(evt) {
    const newValue = evt.target.value;
    this.userInput = newValue;
    if (newValue == '') {
      this.resetValueAndState();
    } else {
      const dates = this.parseInputString(newValue);
      this.validateInputAndShowErrors(dates);
      this.setValue(dates);

      if (this.getShowError()) {
        this.afOnDateChange.emit([]);
      } else {
        this.afOnDateChange.emit(this.afSelectedDates);
      }
    }
  }

  parseCalendar(evt) {
    if (evt && evt.detail) {
      this.setValue(evt.detail);
      this.showWrongFormatError = false;
      this.showDisabledDateError = false;

      if (this.afCloseOnSelect) {
        this.toggleCalendar();
        this.focusOnCalendarButton();
      }
    }
    this.afOnDateChange.emit(this.afSelectedDates ?? []);
  }

  parseDateString(s: string): Date {
    let date: Date = new Date(s);
    const future: Date = new Date();
    future.setFullYear(future.getFullYear() + this.futureYears);
    if (!isValid(date) || date > future)
      date = new Date(
        this.getFullYear(s.slice(0, -4)) +
          '-' +
          s.slice(-4, -2) +
          '-' +
          s.slice(-2)
      );
    return date;
  }

  getFullYear(s: string): string {
    if (s.length === 4) return s;
    const y = new Date().getFullYear().toString();
    return parseInt(s) > parseInt(y.slice(-2)) + this.futureYears
      ? parseInt(y.slice(0, -2)) - 1 + s
      : y.slice(0, -2) + s;
  }

  isDateSelectable(date: Date): boolean {
    if (this.afMinDate && this.isBefore(date, this.afMinDate)) return false;
    if (this.afMaxDate && this.isAfter(date, this.afMaxDate)) return false;
    return true;
  }

  isBefore(a: Date, b: Date): boolean {
    return isBefore(this.dateWithoutTime(a), this.dateWithoutTime(b));
  }

  isAfter(a: Date, b: Date): boolean {
    return isAfter(this.dateWithoutTime(a), this.dateWithoutTime(b));
  }

  dateWithoutTime(date: Date): Date {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate());
  }

  getShowError(): boolean {
    return (
      this.afInvalid || this.showWrongFormatError || this.showDisabledDateError
    );
  }

  getErrorText(): string {
    if (this.afInvalid) {
      return this.afValidationText ?? this.afValidationMessage;
    }
    return this.showWrongFormatError
      ? this.afValidationWrongFormat
      : this.afValidationDisabledDate;
  }

  get inputValue(): string {
    if (this.getShowError()) {
      return this.userInput;
    }

    const sortedDates = this.datesToInputString(
      this.afSelectedDates.sort((a: Date, b: Date) =>
        this.isBefore(a, b) ? -1 : 1
      )
    );
    return sortedDates;
  }

  datesToInputString(dates: Date[]): string {
    return dates.every((date) => date instanceof Date && !isNaN(date.getTime()))
      ? dates
          .map((date) => date.toLocaleDateString('sv-SE'))
          .flatMap((date: string, index, all) => {
            const isLast = index == all.length - 1;
            const isFirst = index === 0;
            if (isFirst) {
              return date;
            } else if (isLast) {
              return [' och ', date];
            } else {
              return [', ', date];
            }
          })
          .join('')
      : '';
  }

  render() {
    return (
      <Host>
        <div
          class={{
            'digi-calendar-datepicker': true,
            ...this.cssModifiers
          }}
          id={`${this.afId}-datepicker`}>
          <div class="digi-calendar-datepicker__input-field">
            <digi-form-input
              class={'digi-calendar-datepicker__input-component'}
              afLabel={this.afLabel}
              afLabelDescription={this.afLabelDescription}
              afValue={this.inputValue}
              afValidation={
                this.getShowError()
                  ? FormInputValidation.ERROR
                  : FormInputValidation.NEUTRAL
              }
              afValidationText={this.getErrorText()}
              afId={this.afId}
              afAriaDescribedby={`${this.afId}--validation-message`}
              afRequired={this.afRequired}
              afRequiredText={this.afRequiredText}
              onAfOnChange={(evt) => {
                evt.stopImmediatePropagation();
                this.parseInput(evt);
              }}>
              <button
                id={this.afId + '-button'}
                class="digi-calendar-datepicker__input-button"
                slot="button"
                aria-label="Visa eller dölj kalender"
                aria-expanded={this.showCalendar ? 'true' : 'false'}
                aria-haspopup={true}
                onClick={() => this.toggleCalendar()}
                type="button">
                <digi-icon
                  class="digi-calendar-datepicker__button-icon"
                  afName={'calendar-alt'}
                  slot="icon"
                />
              </button>
            </digi-form-input>
          </div>
          <div class="digi-calendar-datepicker__calendar">
            <digi-calendar
              afActive={this.showCalendar}
              afShowWeekNumber={this.afShowWeekNumber}
              afMultipleDates={this.afMultipleDates}
              afSelectedDates={this.getShowError() ? [] : this.afSelectedDates}
              afMinDate={this.afMinDate}
              afMaxDate={this.afMaxDate}
              aria-hidden={!this.showCalendar}
              onAfOnDateSelectedChange={(evt) => {
                evt.stopImmediatePropagation();
                this.parseCalendar(evt);
              }}
            />
          </div>
        </div>
      </Host>
    );
  }
}
