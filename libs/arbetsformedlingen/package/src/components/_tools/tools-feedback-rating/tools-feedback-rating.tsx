/* eslint-disable max-len */

import {
  Component,
  Event,
  EventEmitter,
  h,
  State,
  Element,
  Prop,
  Method,
  Watch
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { LayoutBlockVariation } from '../../../components/_layout/layout-block/layout-block-variation.enum';
import { logger } from '../../../global/utils/logger';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FeedbackRatingVariation } from './tools-feedback-rating-variation.enum';
import { FeedbackRatingType } from './tools-feedback-rating-type.enum';
import { FeedbackRatingHeadingLevel } from './tools-feedback-rating-heading-level.enum';

/**
 *
 * @enums FeedbackRatingVariation - tools-feedback-rating-variation.enum
 * @enums FeedbackRatingType - tools-feedback-rating-type.enum
 * @enums FeedbackRatingHeadingLevel - tools-feedback-rating-heading-level.enum
 *
 * @swedishName Betygskomponenten
 */
@Component({
  tag: 'digi-tools-feedback-rating',
  styleUrl: 'tools-feedback-rating.scss',
  scoped: true
})
export class ToolsFeedbackRating {
  @Element() hostElement: HTMLStencilElement;

  private _thankYouHeading;

  /**
   * Sätter ett id-attribut. Om inget anges används en slumpmässig sträng.
   * @en Sets an id attribute.
   */
  @Prop() afId: string = randomIdGenerator('digi-tools-feedback-rating');

  /**
   * Gör det möjligt att visa ett tack-meddelande som visas när användaren har delat med sig av feedback.
   * @en If true, a response message  will show thanking the user for sharing that the page was helpful
   */
  @Prop() afIsDone: boolean;

  /**
   * Sätter variant, kan vara primär, sekundär eller tertiär
   * @en Sets variation. Can be 'primary', 'secondary' or 'tertiary'
   */
  @Prop() afVariation: FeedbackRatingVariation;

  /**
   * Sätter typ som kan vara 'fullwidth', 'grid' eller 'minimal'.
   * @en Sets type. Can be 'fullwidth', 'grid' or 'minimal'.
   */
  @Prop() afType: FeedbackRatingType = FeedbackRatingType.FULLWIDTH;

  /**
   * Sätt rubrikens vikt. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  @Prop() afHeadingLevel: FeedbackRatingHeadingLevel =
    FeedbackRatingHeadingLevel.H2;

  /**
   * Anger frågan på det som ska betygsättas.
   * @en Sets the question
   */
  @Prop() afQuestion = 'Hur nöjd är du med tjänsten?';

  /**
   * Anger rubriken efter inskickat betyg.
   * @en Sets the heading after the rating is sent
   */
  @Prop() afThankYouHeading = 'Tack för din feedback!';

  /**
   * Anger svarsalternativen som visas i komponenten och spåras med Piwik. Behöver separeras med semikolon.
   * @en Sets the answer options
   */
  @Prop() afRatings =
    'Inte alls nöjd;Inte särskilt nöjd;Ganska nöjd;Nöjd;Mycket nöjd';

  /**
   * Sätter attributet 'dir'.
   * @en Set button `dir` attribute.
   */
  @Prop() afDir: string;

  /**
   * Sätter attributet 'lang'.
   * @en Set button `dir` attribute.
   */
  @Prop() afLang: string;

  /**
   * Frivillig hyperlänk
   * @en Sets optional hyperlink
   */
  @Prop() afLink: string;

  /**
   * Sätter hyperlänktext
   * @en Sets hyperlink text
   */
  @Prop() afLinkText: string;

  /**
   * Sätter produktnamn, användbart tex vid spårning
   * @en Sets a product name, usable for tracking
   */
  @Prop() afProduct: string;

  /**
   * Sätter applikationsnamn, användbart tex vid spårning
   * @en Sets an application name, usable for tracking
   */
  @Prop() afApplication: string;

  /**
   * Sätter produktområdesnamn, användbart tex vid spårning
   * @en Sets an application name, usable for tracking
   */
  @Prop() afProductArea: string;

  /**
   * Sätter beskrivning för målgrupp, skärning, videonamn (t.ex. Blivit föräldraledig, Fått arbete, Börjat studera), användbart tex vid spårning
   * @en Sets an application name, usable for tracking
   */
  @Prop() afName: string;

  /**
   * Definierar typ av fråga såsom Innehåll, Måluppfyllnad eller Kundnöjdhet för korrekt redovisning i webbanalysverktyg, användbart tex vid spårning
   * @en Defines type of question
   */
  @Prop() afQuestionType: string;

  /**
   * Om afLink används, kombineras den med URL-variabler för inkludering av betyg och URL:en som användaren kommer ifrån
   * @en Adds custom link for sending rating to Webropol
   */
  @Prop() afUseCustomLink = false;

  /**
   * Sätter nuvarande steg
   * @en Sets current steps
   */
  @State() currentStep = 1;

  @Watch('afIsDone')
  afIsDoneHandler() {
    this.currentStep = 2;
  }

  /**
   * Kontrollerar att frågan inte är en tom sträng
   * @en Checks if mandatory content are included
   */
  @State() validContent = true;

  @Watch('afQuestion')
  validateQuestion() {
    if (this.afQuestion == '') {
      logger.warn(
        `digi-tools-feedback-rating must have a question - please add af-question to the component`,
        this.hostElement
      );
      this.validContent = false;
      return false;
    }
  }

  @State() hoveredRating: number;
  @State() selectedRating: number;
  @State() selectedText: string;

  componentWillLoad() {
    this.validateQuestion();
  }

  componentDidRender() {
    if (this._thankYouHeading) {
      this._thankYouHeading.focus();
    }
  }

  getCurrentPageUrl() {
    return window.location.href;
  }

  renderLink() {
    if (this.afUseCustomLink) {
      return `${this.afLink}?q1o${this.selectedRating}=1&q2o1=${this.getCurrentPageUrl()}`;
    } else {
      return this.afLink;
    }
  }

  /**
   * Används för att ändra steg mellan 1-2.
   * @en Use to change step between 1-2
   * @ignore
   */
  @Method()
  async afMSetStep(step: 1 | 2) {
    this.currentStep = step;
  }

  /**
   * Används för att ändra till steg 1 och återställa betyget.
   * @en Use to change to step 1 and reset star rating
   */
  @Method()
  async afMReset() {
    this.afMSetStep(1);
    this.selectedRating = null;
  }

  /**
   * Emit av eventet sker vid klick på stjärna
   * @en When click on star
   */
  @Event() afOnClickFeedback: EventEmitter;

  /**
   * Emit av eventet sker vid klick på länk
   * @en When click on hyperlink
   */
  @Event() afOnSubmitFeedbackLink: EventEmitter;

  submitFeedback() {
    this.afOnClickFeedback.emit(this.getFeedbackData());
  }

  submitFeedbackLink() {
    this.afOnSubmitFeedbackLink.emit(this.getFeedbackLinkData());
  }

  ratingsData() {
    let ratings = [];
    if (this.afRatings) {
      ratings = this.afRatings.split(';');
    }
    const ratingItems = [];
    for (let i = 0; i < 5; i++) {
      ratingItems.push({
        value: i + 1,
        text: ratings[i] || ''
      });
    }

    return ratingItems;
  }

  handleRatingChange(rating: number) {
    this.selectedRating = rating;
    this.selectedText =
      (
        this.ratingsData().find((item) => item.value === this.selectedRating) ||
        {}
      ).text || null;
    this.submitFeedback();
    this.currentStep = 2;
  }

  handleHover(rating: number) {
    this.hoveredRating = rating;
  }

  getFeedbackData() {
    return {
      productarea: this.afProductArea,
      product: this.afProduct,
      application: this.afApplication,
      id: this.afId,
      name: this.afName,
      questionType: this.afQuestionType,
      question: this.afQuestion,
      rating: this.selectedRating,
      ratingDescription: this.selectedText,
      variation: this.afVariation,
      version: '2'
    };
  }

  getFeedbackLinkData() {
    return {
      productarea: this.afProductArea,
      product: this.afProduct,
      application: this.afApplication,
      id: this.afId,
      name: this.afName,
      questionType: this.afQuestionType,
      question: this.afQuestion,
      linkText: this.afLinkText,
      linkHref: this.afLink,
      version: '2'
    };
  }

  get cssModifiers() {
    return {
      'digi-tools-feedback-rating--fullwidth':
        this.afType === FeedbackRatingType.FULLWIDTH,
      'digi-tools-feedback-rating--grid':
        this.afType === FeedbackRatingType.GRID,
      'digi-tools-feedback-rating--minimal':
        this.afType === FeedbackRatingType.MINIMAL
    };
  }

  get layoutBlockVariation() {
    if (this.afType === FeedbackRatingType.MINIMAL)
      return LayoutBlockVariation.TRANSPARENT;

    switch (this.afVariation) {
      case FeedbackRatingVariation.PRIMARY:
        return LayoutBlockVariation.SYMBOL;
      case FeedbackRatingVariation.SECONDARY:
        return LayoutBlockVariation.PRIMARY;
      case FeedbackRatingVariation.TERTIARY:
        return LayoutBlockVariation.SECONDARY;
    }
  }

  get content() {
    return (
      <digi-typography>
        {this.validContent && (
          <div
            dir={this.afDir}
            lang={this.afLang}
            class={`digi-tools-feedback-rating__wrapper ${
              this.currentStep === 2 ? 'step2' : ''
            }`}>
            {this.currentStep === 1 && (
              <div class="digi-tools-feedback-rating__star-rating">
                <this.afHeadingLevel
                  id={`${this.afId}-rating-question`}
                  class={`digi-tools-feedback-rating__heading ${
                    this.afType === FeedbackRatingType.MINIMAL ? 'hidden' : ''
                  }`}>
                  {this.afQuestion}
                </this.afHeadingLevel>
                <div class="digi-tools-feedback-rating__step1-wrapper">
                  {this.afRatings && (
                    <span class="digi-tools-feedback-rating__step1-description">
                      Välj från{' '}
                      {`${
                        (
                          this.ratingsData().find((item) => item.value === 1) ||
                          {}
                        ).text.toLowerCase() || null
                      } till ${
                        (
                          this.ratingsData().find((item) => item.value === 5) ||
                          {}
                        ).text.toLowerCase() || null
                      }`}
                    </span>
                  )}
                  <div
                    aria-labelledby={`${this.afId}-rating-question`}
                    class="digi-tools-feedback-rating__star-rating-stars"
                    role="group">
                    {this.ratingsData().map(({ text, value }) => (
                      <div class="digi-tools-feedback-rating__star-rating-star">
                        <button
                          class="digi-tools-feedback-rating__star-rating-star-button"
                          onClick={() => this.handleRatingChange(value)}
                          aria-label={`${value} ${
                            value === 1 ? 'stjärna' : 'stjärnor'
                          }${text !== '' ? ', ' + text : ''}`}
                          onMouseEnter={() => this.handleHover(value)}
                          onMouseLeave={() => this.handleHover(0)}
                          onFocus={() => this.handleHover(value)}
                          onFocusout={() => this.handleHover(0)}>
                          <svg
                            aria-hidden="true"
                            width="29px"
                            height="29px"
                            viewBox="0 0 32 32"
                            version="1.1"
                            xmlns="http://www.w3.org/2000/svg"
                            class={`digi-tools-feedback-rating-star-icon ${
                              this.hoveredRating >= value ? 'filled' : ''
                            }`}>
                            <g
                              stroke="none"
                              stroke-width="1"
                              stroke-linecap="round"
                              stroke-linejoin="round">
                              <g
                                transform="translate(-1852, -2564)"
                                stroke-width="2"
                                class="digi-tools-feedback-rating-star-icon-border">
                                <g transform="translate(1663, 2565)">
                                  <g transform="translate(190, 0)">
                                    <path
                                      d="M13.0369701,1.00988613 L9.47212213,8.51643297 L1.4958694,9.72422429 C0.0655277509,9.93973122 -0.507709524,11.7709448 0.529620648,12.8199039 L6.30005635,18.6595463 L4.93529305,26.9090563 C4.6897182,28.3999832 6.20191813,29.5170473 7.46854321,28.8195669 L14.6037422,24.9244875 L21.7391705,28.8195669 C23.0057956,29.5113322 24.5179955,28.3999832 24.2721914,26.9090563 L22.9074281,18.6595463 L28.6780931,12.8199039 C29.715194,11.7709448 29.1419567,9.93973122 27.711615,9.72422429 L19.7355916,8.51643297 L16.1707436,1.00988613 C15.531928,-0.328161856 13.6810595,-0.345069029 13.0369701,1.00988613 L13.0369701,1.00988613 Z"
                                      id="Stroke-3"></path>
                                  </g>
                                </g>
                              </g>
                            </g>
                          </svg>
                        </button>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            )}
            {this.currentStep === 2 && (
              <div class="digi-tools-feedback-rating__thankyou">
                <div class="digi-tools-feedback-rating__thankyou-text">
                  <svg
                    width="32"
                    height="26"
                    xmlns="http://www.w3.org/2000/svg"
                    class="digi-tools-feedback-rating__thankyou-check-icon">
                    <g fill="none" fill-rule="evenodd">
                      <path
                        d="M12.431 25.438C5.577 25.438 0 19.848 0 12.978 0 6.107 5.577.518 12.43.518c2.874 0 5.677 1.006 7.893 2.833l-1.441 1.755a10.163 10.163 0 0 0-6.452-2.315c-5.604 0-10.163 4.57-10.163 10.187 0 5.616 4.56 10.186 10.163 10.186 4.346 0 8.21-2.766 9.617-6.883l2.146.737a12.435 12.435 0 0 1-11.763 8.42"
                        fill="#333"
                      />
                      <path
                        d="M13.073 20.935a.725.725 0 0 1-1.022 0l-8.042-8.013a.72.72 0 0 1 0-1.019l1.704-1.699a.725.725 0 0 1 1.023 0l5.826 5.805L28.417.211a.724.724 0 0 1 1.023 0l1.704 1.699a.72.72 0 0 1 0 1.02l-18.07 18.005Z"
                        fill="#95C13E"
                      />
                    </g>
                  </svg>
                  <h2
                    class="digi-tools-feedback-rating__thankyou-text"
                    tabindex="-1"
                    ref={(el) => (this._thankYouHeading = el)}>
                    {this.afThankYouHeading}
                  </h2>
                </div>
                {this.afRatings && (
                  <p class="digi-tools-feedback-rating__thankyou-text-with-rating">
                    Du har gett betyget "{this.selectedText.toLowerCase()}" med{' '}
                    {this.selectedRating}
                    {`${this.selectedRating === 1 ? ' stjärna' : ' stjärnor'}`}.
                  </p>
                )}
              </div>
            )}
            {this.currentStep === 2 && this.afLinkText && (
              <div class="digi-tools-feedback-rating__link">
                <digi-link-external
                  afHref={this.renderLink()}
                  af-target="_blank"
                  onAfOnClick={() => this.submitFeedbackLink()}
                  af-variation="small">
                  {this.afLinkText}
                </digi-link-external>
              </div>
            )}
          </div>
        )}
      </digi-typography>
    );
  }

  render() {
    return (
      <div
        class={{
          'digi-tools-feedback-rating': true,
          ...this.cssModifiers
        }}>
        <digi-layout-block
          afVariation={this.layoutBlockVariation}
          afMarginTop
          af-container="none">
          {this.content}
        </digi-layout-block>
      </div>
    );
  }
}
