export enum ToolsFeedbackType {
  FULLWIDTH = 'fullwidth',
  GRID = 'grid'
}
