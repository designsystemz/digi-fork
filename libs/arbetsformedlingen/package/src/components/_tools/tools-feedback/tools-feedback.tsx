/* eslint-disable max-len */

import {
  Component,
  Prop,
  State,
  Element,
  Method,
  Event,
  EventEmitter,
  Watch,
  h
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FeedbackVariation } from './tools-feedback-variation.enum';
import { ToolsFeedbackHeadingLevel } from './tools-feedback-heading-level.enum';
import { ToolsFeedbackType } from './tools-feedback-type.enum';
import { LayoutBlockVariation } from '../../../components/_layout/layout-block/layout-block-variation.enum';
import { ButtonSize, ButtonVariation } from '../../../enums';

/**
 * @slot form - kan innehålla vad som helst
 *
 * @enums ToolsFeedbackHeadingLevel - tools-feedback-heading-level.enum.ts
 * @enums FeedbackVariation - tools-feedback-variation.enum.ts
 * @enums ToolsFeedbackType - tools-feedback-type.enum.ts
 *
 * @swedishName Feedback
 */
@Component({
  tag: 'digi-tools-feedback',
  styleUrl: 'tools-feedback.scss',
  scoped: true
})
export class ToolsFeedback {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Sätter variant. Kan vara 'primär' eller 'sekundär'.
   * @en Sets the variation of the feedback.
   */
  @Prop() afVariation: FeedbackVariation = FeedbackVariation.PRIMARY;

  /**
   * Sätter typ på kortet. Kan vara 'fullwidth' eller 'grid'.
   * @en Set card type. Can be 'fullwidth' or 'grid'.
   */

  @Prop() afType: ToolsFeedbackType = ToolsFeedbackType.FULLWIDTH;

  /**
   * Texten på frågan
   * @en The label on the question
   */
  @Prop() afText: string;

  /**
   * Visa stäng-knapp
   * @en Show close button
   */
  @Prop() afCloseButtonText = 'Stäng';

  /**
   * Sätter textens vikt (h1-h6)
   * @en Set text level (h1-h6)
   */
  @Prop() afHeadingLevel: ToolsFeedbackHeadingLevel =
    ToolsFeedbackHeadingLevel.H4;

  /**
   * Texten på feedback
   * @en The label on the feedback
   */
  @Prop() afFeedback: string;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-tools-feedback');

  /**
   * Gör det möjligt att visa ett tack-meddelande som visas när användaren har delat med sig av feedback.
   * @en If true, a response message  will show thanking the user for sharing that the page was helpful
   */
  @Prop() afIsDone = false;

  @Watch('afIsDone')
  afIsDoneHandler() {
    this.currentStep = 3;
  }

  /**
   * Sätter nuvarande steg
   * @en Set current steps
   */
  @State() currentStep = 1;

  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet.
   * @en When the component and slots are loaded and initialized this event will trigger.
   */
  @Event({
    bubbles: false,
    cancelable: true
  })
  afOnReady: EventEmitter;

  /**
   * Används för att nollställa.
   * @en Use to reset interaction.
   */
  @Method()
  async afMReset() {
    this.currentStep = 1;
  }

  /**
   * Används för att ändra steg mellan 1-3.
   * @en Use to change step between 1-3
   * @ignore
   */
  @Method()
  async afMSetStep(step: 1 | 2 | 3) {
    this.currentStep = step;
  }

  get cssModifiers() {
    return {
      'digi-tools-feedback--primary':
        this.afVariation === FeedbackVariation.PRIMARY,
      'digi-tools-feedback--secondary':
        this.afVariation === FeedbackVariation.SECONDARY,
      'digi-tools-feedback--step-one': this.currentStep === 1,
      'digi-tools-feedback--step-two': this.currentStep === 2,
      'digi-tools-feedback--step-three': this.currentStep === 3,
      'digi-tools-feedback--fullwidth':
        this.afType === ToolsFeedbackType.FULLWIDTH,
      'digi-tools-feedback--grid': this.afType === ToolsFeedbackType.GRID
    };
  }

  get icon() {
    return (
      <svg
        class="digi-tools-feedback__container-header-icon"
        aria-hidden="true"
        viewBox="0 0 60 48"
        version="1.1"
        width="58"
        height="48">
        <title>Chat</title>
        <g
          id="Mainpage"
          stroke="none"
          stroke-width="1"
          fill="none"
          fill-rule="evenodd">
          <g
            id="Översikt-moduler-feedback-design-v1"
            transform="translate(-1121.000000, -1074.000000)">
            <g
              id="Group-27-Copy-2"
              transform="translate(1122.000000, 1075.000000)">
              <path
                d="M31.3351987,35.0796454 C31.3351987,37.3388621 33.1498711,39.1874113 35.3679732,39.1874113 L43.2325677,39.1874113 L49.7962813,44.9840831 L49.7962813,39.1874113 L52.6602616,39.1874113 C54.8781464,39.1874113 56.6928188,37.3388621 56.6928188,35.0796454 L56.6928188,17.9415266 C56.6928188,15.6820918 54.8781464,13.8335426 52.6602616,13.8335426 L37.4687612,13.8335426 C37.4687612,13.8335426 37.5319804,25.5217561 37.5319804,27.0499279 C37.5319804,28.5778817 36.1843911,32.189488 31.3351987,32.189488 C31.3043495,34.4120652 31.3351987,35.0796454 31.3351987,35.0796454"
                id="Fill-1"
                fill="#DEE9B7"></path>
              <path
                d="M38.1172465,13.8645553 L53.9081341,13.8645553 C56.158606,13.8645553 58,15.7131045 58,17.9723213 L58,36.0955623 C58,38.3547791 56.158606,40.2033283 53.9081341,40.2033283 L51.0022249,40.2033283 L51.0022249,46 L44.341836,40.2033283 L36.3618827,40.2033283 C34.1111935,40.2033283 32.2697995,38.3547791 32.2697995,36.0955623 L32.2697995,32.521904"
                id="Stroke-3"
                stroke="#333333"
                stroke-width="1.296"
                stroke-linejoin="round"></path>
              <g id="Group-8">
                <path
                  d="M38.1172465,13.8645553 L38.1172465,5.18012517 C38.1172465,2.35604969 35.815504,0.0453631709 33.0023598,0.0453631709 L33.0251708,0 L5.11488673,0 C2.30174248,0 0,2.31068652 0,5.134762 L0,27.387142 C0,30.2112175 2.30174248,32.521904 5.11488673,32.521904 L9.88217668,32.521904 L9.88217668,43.3905272 L20.0617659,32.521904 L33.0023598,32.521904 C35.815504,32.521904 38.1172465,30.2112175 38.1172465,27.387142 L38.1172465,13.8143941"
                  id="Fill-5"
                  fill="#FFFFFF"></path>
                <path
                  d="M38.1172465,13.8645553 L38.1172465,5.18012517 C38.1172465,2.35604969 35.815504,0.0453631709 33.0023598,0.0453631709 L33.0251708,0 L5.11488673,0 C2.30174248,0 0,2.31068652 0,5.134762 L0,27.387142 C0,30.2112175 2.30174248,32.521904 5.11488673,32.521904 L9.88217668,32.521904 L9.88217668,43.3905272 L20.0617659,32.521904 L33.0023598,32.521904 C35.815504,32.521904 38.1172465,30.2112175 38.1172465,27.387142 L38.1172465,13.8143941"
                  id="Stroke-7"
                  stroke="#333333"
                  stroke-width="1.296"
                  stroke-linejoin="round"></path>
              </g>
              <line
                x1="28.4671558"
                y1="9.19912763"
                x2="9.71648163"
                y2="9.19912763"
                id="Stroke-9"
                stroke="#333333"
                stroke-width="1.296"
                stroke-linejoin="round"></line>
              <line
                x1="51.0653789"
                y1="23.3170842"
                x2="47.8731369"
                y2="23.3170842"
                id="Stroke-11"
                stroke="#333333"
                stroke-width="1.296"
                stroke-linejoin="round"></line>
              <line
                x1="46.5029755"
                y1="23.3170842"
                x2="43.3107335"
                y2="23.3170842"
                id="Stroke-13"
                stroke="#333333"
                stroke-width="1.296"
                stroke-linejoin="round"></line>
              <line
                x1="41.9405939"
                y1="23.3170842"
                x2="38.7483519"
                y2="23.3170842"
                id="Stroke-15"
                stroke="#333333"
                stroke-width="1.296"
                stroke-linejoin="round"></line>
              <line
                x1="51.0653789"
                y1="27.7662735"
                x2="46.4946984"
                y2="27.7662735"
                id="Stroke-17"
                stroke="#333333"
                stroke-width="1.296"
                stroke-linejoin="round"></line>
              <line
                x1="43.2358916"
                y1="27.7662735"
                x2="38.4564358"
                y2="27.7662735"
                id="Stroke-19"
                stroke="#333333"
                stroke-width="1.296"
                stroke-linejoin="round"></line>
              <line
                x1="28.4671558"
                y1="13.9050986"
                x2="9.71648163"
                y2="13.9050986"
                id="Stroke-21"
                stroke="#333333"
                stroke-width="1.296"
                stroke-linejoin="round"></line>
              <line
                x1="28.4671558"
                y1="18.6110914"
                x2="9.71648163"
                y2="18.6110914"
                id="Stroke-23"
                stroke="#333333"
                stroke-width="1.296"
                stroke-linejoin="round"></line>
              <line
                x1="21.4671558"
                y1="23.3170842"
                x2="9.72"
                y2="23.3170842"
                id="Stroke-25"
                stroke="#333333"
                stroke-width="1.296"
                stroke-linejoin="round"></line>
            </g>
          </g>
        </g>
      </svg>
    );
  }
  get responseIcon() {
    return (
      <svg
        class="digi-tools-feedback__container-header-icon"
        aria-hidden="true"
        version="1.1"
        viewBox="0 0 35 29"
        width="31"
        height="25">
        <title>Check mark</title>
        <g fill="none" fill-rule="evenodd">
          <g transform="translate(-170 -32)">
            <g transform="translate(170 32.325)">
              <g transform="translate(0 .77495)">
                <mask id="b" fill="white">
                  <polygon points="0 0 26.685 0 26.685 27.689 0 27.689" />
                </mask>
                <path
                  d="m13.711 27.689c-7.5602 0-13.711-6.2103-13.711-13.844 0-7.6345 6.151-13.845 13.711-13.845 3.1693 0 6.2611 1.1179 8.7048 3.1478l-1.5894 1.9509c-1.9972-1.6592-4.5241-2.5724-7.1155-2.5724-6.181 0-11.209 5.0773-11.209 11.319 0 6.2406 5.0283 11.318 11.209 11.318 4.7924 0 9.0551-3.0733 10.607-7.6478l2.3668 0.81853c-1.8983 5.5958-7.1124 9.3556-12.974 9.3556"
                  fill="#333"
                  mask="url(#b)"
                />
              </g>
              <g transform="translate(4.2177)">
                <mask id="a" fill="white">
                  <polygon points="0 0 30.425 0 30.425 23.496 0 23.496" />
                </mask>
                <path
                  d="m30.191 3.2545-19.95 20.007c-0.31227 0.31253-0.81758 0.31253-1.1292 0l-8.878-8.9033c-0.31164-0.31316-0.31164-0.81992 0-1.1324l1.8818-1.8878c0.31164-0.31253 0.81758-0.31253 1.1292 0l6.4315 6.4499 17.504-17.553c0.31164-0.31316 0.81695-0.31316 1.1292 0l1.8818 1.8872c0.31164 0.31253 0.31164 0.81929 0 1.1324"
                  fill="#95C13E"
                  mask="url(#a)"
                />
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }

  get content() {
    return (
      <div>
        {this.currentStep === 1 && (
          <div class="digi-tools-feedback__container">
            <div class="digi-tools-feedback__container-header">
              {this.icon}
              <this.afHeadingLevel class="digi-tools-feedback__container-header-title">
                {this.afText}
              </this.afHeadingLevel>
            </div>
            <div class="digi-tools-feedback__container-btns">
              <digi-button
                af-size={ButtonSize.MEDIUM}
                af-variation={ButtonVariation.SECONDARY}
                af-full-width="false"
                onAfOnClick={() => {
                  this.currentStep = 3;
                }}>
                Ja
              </digi-button>
              <digi-button
                af-size={ButtonSize.MEDIUM}
                af-variation={ButtonVariation.SECONDARY}
                af-full-width="false"
                onAfOnClick={() => {
                  this.currentStep = 2;
                }}>
                Nej
              </digi-button>
            </div>
          </div>
        )}

        {this.currentStep === 2 && (
          <div class="digi-tools-feedback__container">
            {/* <div class="digi-tools-feedback__close-button">
							<digi-button
								afAriaLabel={this.afCloseButtonText}
								onAfOnClick={() => this.afMReset()}
								af-size={ButtonSize.MEDIUM}
								af-variation={ButtonVariation.FUNCTION}
								af-full-width="false"
							>
								{this.afCloseButtonText}
								<digi-icon
									class="digi--tools-feedback__close-icon"
									slot="icon-secondary"
									afName={`x`}
								/>
							</digi-button>
						</div> */}
            <div class="digi-tools-feedback__container-header">
              {' '}
              {this.icon}
              <div class="digi-tools-feedback__container-header-title">
                Hur kan vi bli bättre?
              </div>
            </div>
          </div>
        )}

        <div class="digi-tools-feedback__container-forms">
          <slot name="form"></slot>
        </div>

        {this.currentStep === 3 && (
          <div class="digi-tools-feedback__container-header">
            {this.responseIcon}
            <div class="digi-tools-feedback__container-header-title">
              Tack för din återkoppling!
            </div>
          </div>
        )}
      </div>
    );
  }

  componentDidLoad() {
    this.afOnReady.emit();
  }

  render() {
    return (
      <div
        class={{
          'digi-tools-feedback': true,
          ...this.cssModifiers
        }}>
        {this.afType === ToolsFeedbackType.FULLWIDTH && (
          <digi-layout-block afVariation={LayoutBlockVariation.TRANSPARENT}>
            {this.content}
          </digi-layout-block>
        )}
        {this.afType !== ToolsFeedbackType.FULLWIDTH && this.content}
      </div>
    );
  }
}
