import {
  Component,
  State,
  Event,
  EventEmitter,
  Watch,
  Prop,
  h
} from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { INavigationContextMenuItem } from '../../../interfaces';

/**
 * @swedishName Språkväljare
 */
@Component({
  tag: 'digi-tools-languagepicker',
  styleUrl: 'tools-languagepicker.scss',
  scoped: true,
  shadow: false
})
export class ToolsLanguagepicker {
  /**
   * Texten på språkväljaren
   * @en The label on the language picker
   */
  @Prop() afLanguagepickerText: string;
  @State() _afLanguagepickerText: string;

  /**
   * Texten på "Lyssna"-kanppen
   * @en The label on the "Listen"-button
   */
  @Prop() afListenText = 'Lyssna';
  @State() _afListenText: string;

  /**
   * Texten på "Teckenspråk"-kanppen
   * @en The label on the "Sign language"-button
   */
  @Prop() afSignlangText = 'Teckenspråk';
  @State() _afSignlangText: string;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-tools-languagepicker');
  @State() _afId: string | null;

  /**
   * Sätter ett förvalt värde.
   * @en Sets initial selected value.
   */
  @Prop() afLanguagepickerStartSelected = 1;
  @State() _afLanguagepickerStartSelected: number;

  /**
   * Sätter alla valbara språk.
   * @en Sets all selectable languages.
   */
  @Prop() afLanguagepickerItems: string | INavigationContextMenuItem[];
  @State() _afLanguagepickerItems: string | INavigationContextMenuItem[];

  /**
   * Dölj språkväljaren
   * @en Hide the language picker
   */
  @Prop() afLanguagepickerHide = false;
  @State() _afLanguagepickerHide: boolean;

  /**
   * Visa "Lyssna"-knappen
   * @en Show the "Listen"-button
   */
  @Prop() afListenShow = false;
  @State() _afListenShow: boolean;

  /**
   * Dölj "Teckenspråk"-knappen
   * @en Hide the "Sign language"-button
   */
  @Prop() afSignlangHide = false;
  @State() _afSignlangHide: boolean;

  @Watch('afLanguagepickerText')
  watchHandler_afLanguagepickerText() {
    this._afLanguagepickerText = this.afLanguagepickerText;
  }

  @Watch('afListenText')
  watchHandler_afListenText() {
    this._afListenText = this.afListenText;
  }

  @Watch('afSignlangText')
  watchHandler_afSignlangText() {
    this._afSignlangText = this.afSignlangText;
  }

  @Watch('afId')
  watchHandler_afId() {
    this._afId = this.afId;
  }

  @Watch('afLanguagepickerStartSelected')
  watchHandler_afLanguagepickerStartSelected() {
    this._afLanguagepickerStartSelected = this.afLanguagepickerStartSelected;
  }

  @Watch('afLanguagepickerItems')
  watchHandler_afLanguagepickerItems() {
    this._afLanguagepickerItems = this.afLanguagepickerItems;
  }

  @Watch('afLanguagepickerHide')
  watchHandler_afLanguagepickerHide() {
    this._afLanguagepickerHide = this.afLanguagepickerHide;
    this.updateHideButtonTextState();
  }

  @Watch('afListenShow')
  watchHandler_afListenShow() {
    this._afListenShow = this.afListenShow;
    this.updateHideButtonTextState();
  }

  @Watch('afSignlangHide')
  watchHandler_afSignlangHide() {
    this._afSignlangHide = this.afSignlangHide;
    this.updateHideButtonTextState();
  }

  @State() _hideButtonText: boolean;
  @State() _languagepickerIcon: string;
  /**
   * När komponenten stängs
   * @en When component gets inactive
   */
  @Event() afOnInactive: EventEmitter;

  /**
   * När komponenten öppnas
   * @en When component gets active
   */
  @Event() afOnActive: EventEmitter;

  /**
   * När fokus sätts utanför komponenten
   * @en When focus is move outside of component
   */
  @Event() afOnBlur: EventEmitter;

  /**
   * När fokus sätts på komponenten
   * @en When focus is on component
   */
  @Event() afOnFocus: EventEmitter;

  /**
   * Vid navigering till nytt listobjekt
   * @en When navigating to a new list item
   */
  @Event() afOnChange: EventEmitter;

  /**
   * Toggleknappens 'onclick'-event
   * @en The toggle button's 'onclick'-event
   */
  @Event() afOnToggle: EventEmitter;

  /**
   * 'onclick'-event på knappelementen i listan
   * @en List item buttons' 'onclick'-event
   */
  @Event() afOnSelect: EventEmitter;

  /**
   * 'onclick'-event på någon av knapparna
   * @en Button 'onclick'-event
   */
  @Event() afOnClick: EventEmitter;

  updateHideButtonTextState() {
    this._hideButtonText =
      (this._afLanguagepickerHide ||
        !this._afListenShow ||
        this._afSignlangHide) === false;
  }

  setLanguagePickerIcon() {
    this._languagepickerIcon = 'language-outline';
    return <digi-icon-language-outline></digi-icon-language-outline>;
  }

  componentWillLoad() {
    this.watchHandler_afLanguagepickerText();
    this.watchHandler_afId();
    this.watchHandler_afLanguagepickerStartSelected();
    this.watchHandler_afLanguagepickerItems();
    this.watchHandler_afListenText();
    this.watchHandler_afSignlangText();
    this.watchHandler_afLanguagepickerHide();
    this.watchHandler_afListenShow();
    this.watchHandler_afSignlangHide();
    this.setLanguagePickerIcon();
  }

  get cssModifiers() {
    return {};
  }
  render() {
    return (
      <div
        class={{
          'digi-tools-languagepicker': true,
          ...this.cssModifiers
        }}>
        {this._afLanguagepickerHide === false && (
          <digi-navigation-context-menu
            afText={this._afLanguagepickerText}
            afId={this._afId + '__languagepicker'}
            afStartSelected={this._afLanguagepickerStartSelected}
            afNavigationContextMenuItems={this._afLanguagepickerItems}
            afIcon={this._languagepickerIcon}
            onAfOnActive={(event) => this.afOnActive.emit(event.detail)}
            onAfOnBlur={(event) => this.afOnBlur.emit(event.detail)}
            onAfOnChange={(event) => this.afOnChange.emit(event.detail)}
            onAfOnInactive={(event) => this.afOnInactive.emit(event.detail)}
            onAfOnSelect={(event) => this.afOnSelect.emit(event.detail)}
            onAfOnToggle={(event) => this.afOnToggle.emit(event.detail)}
            class="digi-tools-languagepicker__context"></digi-navigation-context-menu>
        )}
        {this._afListenShow && (
          <digi-button
            af-size="medium"
            af-variation="primary"
            onAfOnClick={(event) => {
              this.afOnClick.emit(event);
            }}
            onAfOnBlur={(event) => {
              this.afOnBlur.emit(event);
            }}
            onAfOnFocus={(event) => {
              this.afOnFocus.emit(event);
            }}
            af-id={this._afId + '__listenButton'}
            class={`digi-tools-languagepicker__buttons ${
              this._hideButtonText ? 'hide-text' : ''
            }`}>
            <digi-icon-volume slot="icon"></digi-icon-volume>
            {this._afListenText}
          </digi-button>
        )}
        {this._afSignlangHide === false && (
          <digi-button
            af-size="medium"
            af-variation="primary"
            onAfOnClick={(event) => {
              this.afOnClick.emit(event);
            }}
            onAfOnBlur={(event) => {
              this.afOnBlur.emit(event);
            }}
            onAfOnFocus={(event) => {
              this.afOnFocus.emit(event);
            }}
            af-id={this._afId + '__signlangButton'}
            class={`digi-tools-languagepicker__buttons ${
              this._hideButtonText ? 'hide-text' : 'show-text'
            }`}>
            <digi-icon-sign slot="icon"></digi-icon-sign>
            {this._afSignlangText}
          </digi-button>
        )}
      </div>
    );
  }
}
