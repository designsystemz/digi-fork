import { Component, h, Host, Prop, Element } from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';

/**
 * @private internal use only
 * @swedishName Flikfält - enskild flikpanel
 */
@Component({
  tag: 'digi-tablist-panel',
  scoped: true
})
export class NavigationTabPanel {
  @Element() hostElement: HTMLStencilElement;

  @Prop() tab: string;
  @Prop() isVisible = false;

  render() {
    return (
      <Host>
        <div
          id={`tabpanel-${this.tab}`}
          role="tabpanel"
          aria-labelledby={`tab-${this.tab}`}
          style={{
            display: this.isVisible ? 'initial' : 'none',
            overflowX: 'hidden',
            overflowY: 'auto'
          }}>
          <slot></slot>
        </div>
      </Host>
    );
  }
}
