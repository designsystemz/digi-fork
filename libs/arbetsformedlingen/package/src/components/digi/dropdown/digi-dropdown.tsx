import {
  h,
  Component,
  Prop,
  Event,
  EventEmitter,
  Watch,
  State,
  Listen
} from '@stencil/core';
import {
  DropdownAfRefEvent,
  DropdownEventItem,
  RelativePosition
} from './digi-dropdown.types';
import {
  DropdownRole,
  ElementRef,
  DropdownItem,
  HTMLDir
} from './digi-dropdown.types';

function deserializeProp<T>(prop: string | T): T {
  if (typeof prop === 'string') {
    return JSON.parse(prop);
  }
  return prop;
}

function getItemRole(role: DropdownRole) {
  switch (role) {
    case 'listbox':
      return 'option';
    case 'menu':
      return 'menuitem';
  }
}

/**
 * @private internal use only
 * @swedishName Rullgardin
 */
@Component({
  tag: 'digi-dropdown',
  styleUrls: ['./digi-dropdown.scss'],
  scoped: true
})
export class DigiDropdown {
  private slotRef: HTMLElement;
  private dropdownRef: HTMLElement;
  private itemRefs: HTMLElement;

  @Prop() afId: string;
  @Prop() afItems: DropdownItem[] | string;
  @Prop() afSelectItem: number;
  @Prop() afActiveItem: number;
  @Prop() afVisible = true;
  @Prop() afAriaLabelledby: string;
  @Prop() afAriaLabel: string;
  @Prop() afDir: HTMLDir;
  @Prop() afLang: string;
  @Prop() afMenuPosition: RelativePosition = 'left-bottom';

  @Prop() afRenderItem: (item: DropdownItem, idx: number) => HTMLElement;

  @State() items: DropdownItem[] = [];

  @State() selectItemIdx = 0;
  @State() activeItemIdx;

  @State() posTop: number;
  @State() posLeft: number;

  @Event() afChangeItem: EventEmitter<DropdownEventItem>;
  @Event() afFocusItem: EventEmitter<DropdownEventItem>;
  @Event() afChangeVisibility: EventEmitter<boolean>;
  @Event() afRef: EventEmitter<DropdownAfRefEvent>;

  @Listen('keydown')
  handleKey(e: KeyboardEvent) {
    if (!this.focusCaptured()) return;
    let supportsKey = true;
    switch (e.key) {
      case 'ArrowUp':
        this.focusItem(this.nextIdx(this.selectItemIdx - 1));
        break;
      case 'ArrowDown':
        this.focusItem(this.nextIdx(this.selectItemIdx + 1));
        break;
      case 'Enter':
      case ' ':
        this.activateItem(this.selectItemIdx);
        break;
      default:
        supportsKey = false;
        break;
    }
    if (supportsKey) {
      e.preventDefault();
    }
  }

  @Watch('afVisible')
  handleChangeVisibility() {
    this.afChangeVisibility.emit(this.afVisible);
  }

  @Watch('afItems')
  @Watch('afSelectItem')
  @Watch('afActiveItem')
  syncItems(prop, _, propName) {
    switch (propName) {
      case 'afItems':
        this.items = deserializeProp<DropdownItem[]>(prop);
        break;
      case 'afSelectItem':
        this.selectItemIdx = prop ?? this.selectItemIdx;
        break;
      case 'afActiveItem':
        this.activeItemIdx = prop ?? this.activeItemIdx;
        this.selectItemIdx = prop ?? this.selectItemIdx;
        break;
    }
  }

  focusCaptured() {
    return this.items.some(
      (item) => this.itemRefs[item.id] === document.activeElement
    );
  }

  nextIdx(idx: number) {
    if (idx > this.items.length - 1) {
      return 0;
    } else if (idx < 0) {
      return this.items.length - 1;
    } else {
      return idx;
    }
  }

  activateItem(idx: number) {
    const item = this.items[idx];
    this.activeItemIdx = idx;
    this.focusItem(idx);
    this.afChangeItem.emit({ item, idx });
  }

  focusItem(idx: number) {
    const item = this.items[idx];
    this.selectItemIdx = idx;
    this.itemRefs[item.id]?.focus();
    this.afFocusItem.emit({ item, idx });
  }

  handleRef(ref, refId, payload?) {
    if (refId === ElementRef.Item) {
      const item: DropdownItem = payload;
      this.itemRefs = {
        ...this.itemRefs,
        [item.id]: ref
      };
    } else if (refId === ElementRef.Host) {
      this.dropdownRef = ref;
    }
    this.afRef.emit({
      id: refId,
      ref,
      payload
    });
  }

  handleChangeItem(_, idx: number) {
    this.activateItem(idx);
  }

  @Watch('afMenuPosition')
  syncPosition() {
    if (this.slotRef != null) {
      const anchorBox = this.slotRef.children[0].getBoundingClientRect();
      const elemBox = this.dropdownRef.getBoundingClientRect();
      if (this.afMenuPosition === 'right-bottom') {
        this.posLeft = anchorBox.width - elemBox.width;
      } else if (this.afMenuPosition === 'center-bottom') {
        this.posLeft = anchorBox.width / 2 - elemBox.width / 2;
      } else {
        this.posLeft = 0;
      }
    }
  }

  componentWillLoad() {
    this.syncItems(this.afItems, null, 'afItems');
    this.syncItems(this.afSelectItem, null, 'afSelectItem');
    this.syncItems(this.afActiveItem, null, 'afActiveItem');
  }

  componentDidLoad() {
    this.syncPosition();
  }

  render() {
    return (
      <div
        style={{
          position: 'relative',
          display: 'flex'
        }}>
        <div ref={(ref) => (this.slotRef = ref)}>
          <slot />
        </div>
        <div
          style={{
            position: 'absolute',
            top: 'calc(100% + 0.5rem)',
            left: `${this.posLeft}px`,
            zIndex: '10',
            visibility: this.afVisible ? 'visible' : 'hidden'
          }}>
          <div
            id={this.afId}
            class={{ dropdown: true }}
            ref={(ref) => this.handleRef?.(ref, ElementRef.Host)}>
            <div
              role="listbox"
              id={`${this.afId}-listbox`}
              class={{
                'dropdown-wrapper': true,
                'dropdown-wrapper--dir-rtl': this.afDir === 'rtl'
              }}
              aria-labelledby={this.afAriaLabelledby}
              aria-label={this.afAriaLabel}
              tabIndex={-1}>
              {this.items.map((item, idx) => {
                const active = this.activeItemIdx === idx;
                const select = this.selectItemIdx === idx;
                return (
                  <div
                    role={getItemRole('listbox')}
                    id={`${this.afId}-listbox-item-${item.id}`}
                    key={item.id}
                    class={{
                      'option-wrapper': true,
                      'option-wrapper--active': active
                    }}
                    dir={this.afDir}
                    lang={this.afLang}
                    aria-selected={active.toString()}
                    tabIndex={select ? 0 : -1}
                    onClick={() => this.handleChangeItem?.(item, idx)}
                    ref={(ref) => this.handleRef?.(ref, ElementRef.Item, item)}>
                    {this.afRenderItem?.(item, idx)}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
