import {
  DropdownItem,
  DropdownPosition
} from '../dropdown/digi-dropdown.types';

export interface ContextMenuItem extends DropdownItem {
  type?: ContextMenuItemType;
  href?: string;
}

export type ContextMenuItemType = 'link' | 'button';

export type ContextMenuPosition = DropdownPosition;

export interface ContextMenuEventItem {
  item: ContextMenuItem;
  idx: number;
}
