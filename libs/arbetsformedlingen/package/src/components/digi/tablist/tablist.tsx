import {
  Component,
  h,
  Host,
  Prop,
  State,
  Watch,
  Listen,
  Event,
  EventEmitter
} from '@stencil/core';
import {
  Direction,
  OverflowMode,
  TabItem
} from './presentation/tablist-presentation.interface';

/**
 * @slot default - <digi-tablist-panel /> för varje flik
 * @swedishName Flikfält
 */
@Component({
  tag: 'digi-tablist',
  scoped: true
})
export class NavigationTablist {
  private tablistRef: HTMLElement;
  private tabRefs: {
    [key: string]: HTMLElement;
  } = {};
  private panelsRef: Element;

  private resizeObserver: ResizeObserver;

  @State() activeTab = 0;
  @State() selectedTab = 0;

  @State() windowTab = 0;
  @State() tabsPerWindow;

  @State() tabs = [];

  @State() listOverflows = false;
  @State() showPrev = false;
  @State() showNext = false;

  /**
   * Bestämmer flikar
   * @en Set tabs
   */
  @Prop() afTabs: TabItem[] | string;

  /**
   * Bestämmer aria-label på listan av flikar
   * @en Set aria-labelledby for tablist
   */
  @Prop() afAriaLabelledby: string;

  /**
   * Bestämmer aria-label på listan av flikar
   * @en Set aria label for tablist
   */
  @Prop() afAriaLabel: string;

  /**
   * Flik som är aktiv vid start, använder flikens id
   * @en Active tab at start, using tab id
   */
  @Prop() afInitialTab?: string;

  /**
   * Flik som är aktiv, använder flikens id
   * @en Active tab, using tab id
   */
  @Prop() afActiveTab?: string;

  /**
   * Skickas när en flik aktiveras, dvs vid klick eller med space/enter-tangentklick på markerad flik. Fliken som aktiveras finns under event.detail
   * @en Set text for toggle button
   */
  @Event() afTabChange: EventEmitter<TabItem>;

  /**
   * Skickas när en flik fokusmarkeras samt vid klick. Fliken som markeras finns under event.detail
   * @en Set text for toggle button
   */
  @Event() afTabFocusChange: EventEmitter<TabItem>;

  @Listen('keydown')
  handleKey(e: KeyboardEvent) {
    if (this.focusCaptured()) {
      let captureKey = true;
      switch (e.key) {
        case ' ':
        case 'Enter':
          return this.setActiveTabAndFocus(this.selectedTab);
        case 'ArrowLeft':
          if (e.shiftKey) {
            this.handleTabControl(Direction.Previous);
          } else {
            this.focusTab(this.selectedTab - 1);
          }
          break;
        case 'ArrowRight':
          if (e.shiftKey) {
            this.handleTabControl(Direction.Next);
          } else {
            this.focusTab(this.selectedTab + 1);
          }
          break;
        case 'Home':
          if (e.shiftKey) {
            return this.focusTab(0);
          } else {
            return this.moveWindow(0);
          }
        case 'End':
          if (e.shiftKey) {
            return this.focusTab(this.tabs.length - 1);
          } else {
            return this.moveWindow(this.tabs.length - 1);
          }
        default:
          captureKey = false;
      }
      if (captureKey) {
        e.preventDefault();
      }
    }
  }

  @Watch('activeTab')
  syncPanels() {
    // side-effect
    if (this.panelsRef != null) {
      const panels = this.panelsRef.querySelectorAll('digi-tablist-panel');
      for (let i = 0; i < panels.length; i++) {
        const panel = panels[i];
        const showPanel = panel.tab === this.tabs[this.activeTab].id;
        panel.isVisible = showPanel;
      }
    }
  }

  @Watch('afActiveTab')
  handleTabPropChange (tabId) {
    this.setActiveTabById(tabId)
  }

  @Watch('windowTab')
  @Watch('listOverflows')
  syncControls() {
    const tabGroup = Math.floor(this.windowTab / this.tabsPerWindow);
    const numberOfGroups = Math.ceil(this.tabs.length / this.tabsPerWindow);
    this.showPrev = tabGroup > 0;
    this.showNext = tabGroup < numberOfGroups - 1;
  }

  @Watch('windowTab')
  syncWindow(tabIdx: number) {
    const tab = this.tabs[tabIdx];
    const tabRef = this.tabRefs[tab.id];

    const listBox = this.tablistRef.getBoundingClientRect();
    const tabBox = tabRef.getBoundingClientRect();

    const centeredOffset =
      tabRef.offsetLeft - listBox.width / 2 + tabBox.width / 2;
    const maxLeft = this.tablistRef.scrollWidth - listBox.width;
    const left = Math.max(Math.min(centeredOffset, maxLeft), 0);

    this.tablistRef.scrollTo({
      left: left,
      behavior: 'smooth'
    });
  }

  @Watch('selectedTab')
  syncFocus(tabIdx: number) {
    const tabRef = this.tabRefs[this.tabs[tabIdx].id];
    tabRef.focus({ preventScroll: true });

    this.windowTab = tabIdx;
  }

  @Watch('afTabs')
  setupTabs(tabs) {
    if (typeof tabs === 'string') {
      this.tabs = JSON.parse(tabs);
    } else if (Array.isArray(tabs)) {
      this.tabs = tabs;
    } else {
      throw new Error('Invalid format or no tabs supplied');
    }
  }

  setActiveTabById (id: string) {
    const tabIdx = this.tabs.findIndex((tab) => tab.id === id);
    if (tabIdx > -1) {
      this.activeTab = tabIdx;
    } 
  }

  setupInitialTab() {
    if (this.afActiveTab != null) {
      return this.setActiveTabById(this.afActiveTab)
    } else if (this.afInitialTab != null) {
      return this.setActiveTabById(this.afInitialTab);
    }
  }

  unobserveSize(elem) {
    if (elem != null) {
      this.resizeObserver.unobserve(elem);
    }
  }

  observeSize(elem) {
    if (elem != null) {
      this.resizeObserver.observe(elem);
    }
  }

  setupRefs(refs) {
    this.unobserveSize(this.tablistRef);
    this.observeSize(refs.tablist);

    this.tablistRef = refs.tablist;
    this.tabRefs = refs.tabs;
  }

  setupResizeHandler() {
    this.resizeObserver = new ResizeObserver(() => this.handleResize());
  }

  handleTabChange(tab: TabItem) {
    const idx = this.tabs.findIndex(({ id }) => id === tab.id);
    this.setActiveTabAndFocus(idx);

    this.afTabChange.emit(tab);
  }

  handleResize() {
    const tlRect = this.tablistRef.getBoundingClientRect();
    const contentWidth = this.tablistRef.scrollWidth;
    this.tabsPerWindow = Math.max(
      Math.floor(
        Math.min(
          (tlRect.width / contentWidth) * this.tabs.length,
          this.tabs.length
        )
      ),
      1
    );
    this.listOverflows =
      this.tablistRef.scrollWidth > this.tablistRef.clientWidth;
  }

  handleTabControl(direction: Direction) {
    this.moveWindow(this.windowTab + direction * this.tabsPerWindow);
  }

  setActiveTabAndFocus(tabIdx: number) {
    this.activeTab = tabIdx;
    this.focusTab(tabIdx);
  }

  focusTab(tabIdx: number) {
    this.selectedTab = this.nextTabIdx(tabIdx);

    const currentTab = this.tabs[this.selectedTab];
    this.afTabFocusChange.emit(currentTab);
  }

  moveWindow(tabIdx: number) {
    tabIdx = Math.max(0, Math.min(tabIdx, this.tabs.length - 1));
    this.windowTab = tabIdx;
  }

  nextTabIdx(tabIdx: number): number {
    if (tabIdx >= this.tabs.length) {
      return 0;
    } else if (tabIdx < 0) {
      return this.tabs.length - 1;
    }
    return tabIdx;
  }

  focusCaptured() {
    return this.tabs.some(
      (tab) => this.tabRefs[tab.id] === document.activeElement
    );
  }

  componentWillLoad() {
    this.setupTabs(this.afTabs);
    this.setupInitialTab();
    this.setupResizeHandler();
  }

  componentDidLoad() {
    this.syncPanels();
    this.syncControls();
  }

  render() {
    return (
      <Host>
        <digi-tablist-presentation
          htmlAriaLabelledby={this.afAriaLabelledby}
          htmlAriaLabel={this.afAriaLabel}
          overflowMode={OverflowMode.Controls}
          showPrevControl={this.listOverflows && this.showPrev}
          showNextControl={this.listOverflows && this.showNext}
          activeTab={this.tabs[this.activeTab].id}
          focusTab={this.tabs[this.selectedTab].id}
          tabs={this.tabs}
          onTabChange={(e) => this.handleTabChange(e.detail)}
          onTabControl={(e) => this.handleTabControl(e.detail)}
          onRefs={(e) => this.setupRefs(e.detail)}
        />
        <div ref={(ref) => (this.panelsRef = ref)} role="presentation">
          <slot />
        </div>
      </Host>
    );
  }
}
