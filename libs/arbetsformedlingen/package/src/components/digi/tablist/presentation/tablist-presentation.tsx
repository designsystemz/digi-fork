import { Component, h, Host, Prop, EventEmitter, Event } from '@stencil/core';

import {
  Direction,
  OverflowMode,
  TabItem
} from './tablist-presentation.interface';

/**
 * @ignore
 * @private internal use only
 */
@Component({
  tag: 'digi-tablist-presentation',
  styleUrl: 'tablist-presentation.scss',
  scoped: true
})
export class TablistPresentation {
  private tabRefs: { [key: string]: HTMLElement } = {};
  private tablistRef: HTMLElement;

  @Prop() tabs: TabItem[];

  @Prop() activeTab: string;
  @Prop() focusTab: string;

  @Prop() overflowMode: OverflowMode;
  @Prop() showPrevControl = false;
  @Prop() showNextControl = false;

  @Prop() htmlAriaLabelledby: string;
  @Prop() htmlAriaLabel: string;

  @Event() tabControl: EventEmitter<Direction>;
  @Event() tabChange: EventEmitter<TabItem>;
  @Event() refs: EventEmitter<{
    tabs: {
      [key: string]: HTMLElement;
    };
    tablist: HTMLElement;
  }>;

  handleChangeTab(tab: TabItem) {
    this.tabChange.emit(tab);
  }

  componentDidLoad() {
    this.refs.emit({
      tablist: this.tablistRef,
      tabs: this.tabRefs
    });
  }

  render() {
    return (
      <Host>
        <div
          class={{
            'digi-tablist': true,
            'digi-tablist--overflow-scroll':
              this.overflowMode === OverflowMode.Scroll,
            'digi-tablist--overflow-controls':
              this.overflowMode === OverflowMode.Controls
          }}>
          <div
            class="control-wrapper"
            style={{
              left: '0',
              visibility: this.showPrevControl ? 'visible' : 'hidden'
            }}>
            <button
              tabIndex={-1}
              aria-hidden="true"
              class={{ 'tab-control-button': true }}
              onClick={() => this.tabControl.emit(Direction.Previous)}>
              <digi-icon-chevron-left />
            </button>
          </div>
          <div
            role="tablist"
            ref={(ref) => (this.tablistRef = ref)}
            aria-labelledby={this.htmlAriaLabelledby}
            aria-label={this.htmlAriaLabel}>
            {this.tabs.map((tab) => {
              const focusable = tab.id === this.focusTab;
              const isActive = tab.id === this.activeTab;
              return (
                <button
                  ref={(ref) => (this.tabRefs[tab.id] = ref)}
                  class={{
                    'digi-tab': true,
                    'is-active': isActive
                  }}
                  id={`tab-${tab.id}`}
                  role="tab"
                  tabindex={focusable ? '0' : '-1'}
                  aria-selected={isActive.toString()}
                  aria-controls={`tabpanel-${tab.id}`}
                  onClick={() => this.handleChangeTab(tab)}>
                  <div class="content-wrapper">
                    <span class="title" data-text={tab.title}>
                      {tab.title}
                    </span>
                    <div class="indicator" />
                  </div>
                </button>
              );
            })}
          </div>
          <div
            class="control-wrapper"
            style={{
              right: '0',
              visibility: this.showNextControl ? 'visible' : 'hidden'
            }}>
            <button
              tabIndex={-1}
              aria-hidden="true"
              class={{ 'tab-control-button': true }}
              onClick={() => this.tabControl.emit(Direction.Next)}>
              <digi-icon-chevron-right />
            </button>
          </div>
        </div>
      </Host>
    );
  }
}
