export enum LogoVariation {
  SMALL = 'small',
  LARGE = 'large'
}
