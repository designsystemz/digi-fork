import { Component, h, Prop } from '@stencil/core';
/**
 *
 *
 * @swedishName Sidhuvud-menyval
 */
@Component({
  tag: 'digi-header-navigation-item',
  styleUrl: 'header-navigation-item.scss',
  scoped: true
})
export class HeaderNavigationItem {
  /**
   * Sätter vilken sida som är active
   * @en Sets what page is currently active
   */
  @Prop() afCurrentPage: boolean;

  get cssModifiers() {
    return {
      'digi-header-navigation-item--active': this.afCurrentPage,
      'digi-header-navigation-item-indicator--active': this.afCurrentPage
    };
  }

  render() {
    return (
      <div
        role="listitem"
        class={{
          'digi-header-navigation-item': true,
          ...this.cssModifiers
        }}>
        <slot />
        <div class={{
          'digi-header-navigation-item-indicator': true,
          ...this.cssModifiers
        }}/>
      </div>
    );
  }
}
