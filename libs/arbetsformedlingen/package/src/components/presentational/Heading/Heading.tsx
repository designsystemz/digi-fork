import { h, FunctionalComponent } from '@stencil/core';

interface a11yProps {
  ariaControls: string;
  ariaExpanded: string;
  ariaLabelledby: string;
  ariaLabel: string;
}

type HeadingLevel = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';

interface ExpandHeadingProps
  extends Pick<a11yProps, 'ariaControls' | 'ariaExpanded'> {
  title: string;
  headingLevel: HeadingLevel;
}

export const ExpandHeading = ({
  title,
  ariaControls,
  ariaExpanded
}): FunctionalComponent<ExpandHeadingProps> => {
  return (
    <div>
      <button aria-controls={ariaControls} aria-expanded={ariaExpanded}>
        <headingLevel>{title}</headingLevel>
      </button>
    </div>
  );
};
