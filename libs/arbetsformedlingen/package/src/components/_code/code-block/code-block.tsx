import { Component, Prop, h, State, Watch } from '@stencil/core';

import { CodeBlockVariation } from './code-block-variation.enum';
import { CodeBlockLanguage } from './code-block-language.enum';
import Prism from 'prismjs';
import 'prismjs/components/prism-typescript';
import 'prismjs/components/prism-bash';
import 'prismjs/components/prism-scss';
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-git';
import { ButtonVariation } from '../../../enums';
import { ButtonSize } from '../../../enums';
import { _t } from '../../../text';

/**
 * @enums CodeBlockLanguage - code-block-language.enum.ts
 * @enums CodeBlockVariation - code-block-variation.enum.ts
 * @swedishName Kodblock
 */
@Component({
  tag: 'digi-code-block',
  styleUrls: ['code-block.scss']
})
export class CodeBlock {
  @State() highlightedCode;

  /**
   * En sträng med den kod som du vill visa upp.
   * @en A string of code to use in the code block.
   */
  @Prop() afCode: string;

  /**
   * Sätt färgtemat. Kan vara ljust eller mörkt.
   * @en Set code block variation. This defines the syntax highlighting and can be either light or dark.
   */
  @Prop() afVariation: CodeBlockVariation = CodeBlockVariation.DARK;

  /**
   * Sätt programmeringsspråk. 'html' är förvalt.
   * @en Set code language. Defaults to html.
   */
  @Prop() afLanguage: CodeBlockLanguage = CodeBlockLanguage.HTML;

  /**
   * Sätt språk inuti code-elementet. 'en' (engelska) är förvalt.
   * @en Set language inside of code element. Defaults to 'en' (english).
   */
  @Prop() afLang = 'en';

  /**
   * Ta bort knappar för att till exempel kopiera kod
   * @en Remove toolbar for copying code etc
   */
  @Prop() afHideToolbar = false;

  componentWillLoad() {
    Prism.manual = true;
    this.formatCode();
  }

  @Watch('afCode')
  formatCode() {
    this.highlightedCode = Prism.highlight(
      this.afCode,
      Prism.languages[this.afLanguage],
      this.afLanguage
    );
  }

  copyButtonClickHandler() {
    navigator.clipboard.writeText(this.afCode).then(
      () => {
        // console.log('hurra!');
      },
      (err) => {
        // console.log('åh nej!', err);
        console.error(err);
      }
    );
  }

  get cssModifiers() {
    return {
      'digi-code-block--light': this.afVariation === CodeBlockVariation.LIGHT,
      'digi-code-block--dark': this.afVariation === CodeBlockVariation.DARK
    };
  }

  render() {
    return (
      <div
        class={{
          'digi-code-block': true,
          ...this.cssModifiers
        }}>
        <pre class={`digi-code-block__pre language-${this.afLanguage}`}>
          <code
            class="digi-code-block__code"
            lang={this.afLang}
            innerHTML={this.highlightedCode}></code>
        </pre>
        {!this.afHideToolbar && (
          <div class="digi-code-block__toolbar">
            <digi-button
              class="digi-code-block__button"
              onAfOnClick={() => this.copyButtonClickHandler()}
              afVariation={ButtonVariation.FUNCTION}
              afSize={ButtonSize.SMALL}>
              <digi-icon
                class="digi-code-block__icon"
                aria-hidden="true"
                afName={`copy`}
                slot="icon"></digi-icon>
              {_t.code.copy}
            </digi-button>
          </div>
        )}
      </div>
    );
  }
}
