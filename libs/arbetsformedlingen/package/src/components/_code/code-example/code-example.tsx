/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, Prop, h, State, Watch, Element } from '@stencil/core';

import { CodeExampleVariation } from './code-example-variation.enum';
import { CodeExampleLanguage } from './code-example-language.enum';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { ButtonSize } from '../../_button/button/button-size.enum';
import { ButtonVariation } from '../../_button/button/button-variation.enum';
import { CodeBlockVariation } from '../code-block/code-block-variation.enum';
import { _t } from '../../../text';

/**
 * @private internal use only
 * @enums CodeExampleLanguage - code-example-language.enum.ts
 * @enums CodeExampleVariation - code-example-variation.enum.ts
 * @swedishName Kodexempel
 */
@Component({
  tag: 'digi-code-example',
  styleUrls: ['code-example.scss'],
  scoped: true
})
export class CodeExample {
  @State() isActive = true;
  @State() codeLanguage: CodeExampleLanguage;
  @State() hasControls: boolean;
  @State() showMoreButton: boolean;
  @State() containerElement!: HTMLDivElement;

  @Element() hostElement!: HTMLStencilElement;

  private _codeExample:
    | string
    | {
        [codeLang in CodeExampleLanguage]: string;
      };
  private _codeHeightObserver;

  /**
   * Försöker parsa afCode som ett objekt.
   * Returnerar antingen codeExample som sträng - antingen direkt om afCode var en sträng, annars det valda kodspråket.
   */
  get codeExample(): string {
    let codeString: string;
    if (typeof this._codeExample === 'object') {
      this.codeLanguage ||
        (this.codeLanguage = this
          .codeExampleLanguages[0] as CodeExampleLanguage);
      codeString = this._codeExample[this.codeLanguage];
    } else codeString = this._codeExample;
    return codeString;
  }
  set codeExample(code: string) {
    try {
      this._codeExample = JSON.parse(code);
    } catch (e) {
      // code är antagligen en sträng och inte objekt.
      this._codeExample = code;
    }
  }

  /**
   * Returnerar tomt om inget kodspråk har skickats med i afCode.
   * Dock returnerar en lista av kodspråk (t.ex. ts eller js) om detta är angett.
   */
  get codeExampleLanguages(): string[] {
    return (
      typeof this._codeExample === 'object' && Object.keys(this._codeExample)
    );
  }

  /**
   * En sträng, eller objekt med strängar, med den kod som du vill visa upp.
   * @en A string, or object of strings, of code to use in the code block.
   */
  @Prop() afCode:
    | string
    | {
        [codeLang in CodeExampleLanguage]: string;
      };

  /**
   * Sätt färgtemat på kodblocket. Kan vara ljust eller mörkt.
   * @en Set code block variation. This defines the syntax highlighting and can be either light or dark.
   */
  @Prop() afCodeBlockVariation: CodeBlockVariation = CodeBlockVariation.DARK;

  /**
   * Sätt färgtemat på exemplets bakgrund. Kan vara ljust eller mörkt.
   * @en Set example background. This defines the syntax highlighting and can be either light or dark.
   */
  @Prop() afExampleVariation: CodeExampleVariation = CodeExampleVariation.LIGHT;

  /**
   * Sätt programmeringsspråk. 'html' är förvalt.
   * @en Set code language. Defaults to html.
   */
  @Prop() afLanguage: CodeExampleLanguage = CodeExampleLanguage.HTML;

  @Prop() afHideControls: boolean;

  @Prop() afHideCode: boolean;

  /**
   * Spara kodexemplet från afCode vid uppdatering av attributet.
   * @en Save the code example from afCode on update of the attribute.
   */
  @Watch('afCode')
  codeExampleUpdate() {
    this.codeExample = this.afCode as string;
  }

  componentWillLoad() {
    this.codeExampleUpdate();

    this.hasControls =
      !!this.hostElement.querySelector('[slot="controls"]') ||
      !!this.codeExampleLanguages;
  }

  componentWillUpdate() {
    this.hasControls =
      !!this.hostElement.querySelector('[slot="controls"]') ||
      !!this.codeExampleLanguages;
  }

  componentDidLoad() {
    this.updateShowMoreButton();
  }

  toggleButtonClickHandler() {
    this.isActive = !this.isActive;
  }

  copyButtonClickHandler() {
    navigator.clipboard.writeText(this.codeExample).then(
      () => {
        // console.log('hurra!');
      },
      (err) => {
        // console.log('åh nej!', err);
        console.error(err);
      }
    );
  }

  /**
   * Ändrar kodspråk vid val av radioknapp
   * @param e Event object of the change event.
   */
  codeLanguageChangeHandler(e) {
    this.codeLanguage = e.target.value;
    this.codeExampleUpdate();
  }

  /**
   * Visar "Visa mer" när kodblocket är högre än 180px.
   * @en Show "Visa mer" when code block is higher than 180px.
   */
  updateShowMoreButton() {
    const codeblock = this.containerElement.querySelector(
      '.digi-code-example__codeblock'
    );
    const controls = this.containerElement.querySelector(
      '.digi-code-example__controls > span'
    );
    const example = this.containerElement.querySelector(
      '.digi-code-example__example'
    );

    if (!this._codeHeightObserver && codeblock) {
      this._codeHeightObserver = new ResizeObserver((observable) => {
        const initSetActive = this.showMoreButton == undefined;
        this.showMoreButton =
          observable[0].target.scrollHeight >=
          (controls ? controls.scrollHeight - example.scrollHeight + 35 : 64);
        if (initSetActive) {
          this.isActive = this.showMoreButton ? false : true;
        }
      });
      this._codeHeightObserver.observe(codeblock);
    }
  }

  get cssModifiers() {
    return {
      'digi-code-example--active': this.isActive,
      'digi-code-example--inactive': !this.isActive,
      [`digi-code-example--hide-controls-${!!this.afHideControls}`]: true,
      'digi-code-example--light':
        this.afExampleVariation === CodeExampleVariation.LIGHT,
      'digi-code-example--dark':
        this.afExampleVariation === CodeExampleVariation.DARK,
      'digi-code-example--no-controls':
        !this.hasControls && !this.codeExampleLanguages
    };
  }
  capitalize(word) {
    return word
      .toLowerCase()
      .replace(/\w/, (firstLetter) => firstLetter.toUpperCase());
  }

  render() {
    return (
      <div
        class={{
          'digi-code-example': true,
          ...this.cssModifiers
        }}
        ref={(el) => (this.containerElement = el)}>
        <div
          class={{
            'digi-code-example__container': true
          }}>
          <div class="digi-code-example__example">
            <slot></slot>
          </div>

          {!this.afHideCode && (
            <div class="digi-code-example__code">
              {this.showMoreButton && (
                <button
                  class="digi-code-example__code-expand"
                  aria-label={
                    this.isActive ? _t.code.show_less : _t.code.show_more
                  }
                  aria-expanded={this.isActive ? `true` : `false`}
                  onClick={() => this.toggleButtonClickHandler()}>
                  {this.isActive ? _t.code.show_less : _t.code.show_more}{' '}
                  <digi-icon
                    class={{
                      'digi-code-example__code-expand-icon': true,
                      'digi-code-example__code-expand-icon--open': this.isActive
                    }}
                    aria-hidden="true"
                    afName={this.isActive ? `chevron-up` : `chevron-down`}
                  />
                </button>
              )}
              <div
                class={{
                  'digi-code-example__codeblock': true,
                  'digi-code-example__codeblock--light':
                    this.afCodeBlockVariation === 'light',
                  'digi-code-example__codeblock--dark':
                    this.afCodeBlockVariation === 'dark',
                  'digi-code-example__codeblock--fade':
                    !this.isActive && this.showMoreButton
                }}>
                <digi-code-block
                  afCode={this.codeExample || ''}
                  afLanguage={this.afLanguage.toLowerCase() as any}
                  afVariation={this.afCodeBlockVariation as any}
                  af-hide-toolbar
                />
                {!this.isActive ? (
                  <div
                    class={{
                      'digi-code-example__fade': true,
                      'digi-code-example__fade--dark':
                        this.afCodeBlockVariation === 'dark',
                      'digi-code-example__fade--light':
                        this.afCodeBlockVariation === 'light'
                    }}
                    onClick={() => this.toggleButtonClickHandler()}></div>
                ) : (
                  ''
                )}
              </div>
            </div>
          )}
        </div>
        {this.hasControls || !!this.codeExampleLanguages ? (
          <div
            class={{
              'digi-code-example__controls': true
            }}
            hidden={!!this.afHideControls}>
            <span>
              <slot name="controls"></slot>
              <div class="slot__controls">
                {this.codeExampleLanguages && (
                  <digi-form-fieldset
                    af-legend={_t.code.language}
                    af-name="Code Language"
                    onChange={(e) => this.codeLanguageChangeHandler(e)}>
                    {this.codeExampleLanguages.map((codeLang) => (
                      <digi-form-radiobutton
                        afName="Code Language"
                        afChecked={this.codeLanguage === codeLang}
                        afValue={codeLang}
                        afLabel={codeLang}
                      />
                    ))}
                  </digi-form-fieldset>
                )}
              </div>
            </span>
            <digi-button
              class="digi-code-example__copy-button"
              onAfOnClick={() => this.copyButtonClickHandler()}
              af-variation={ButtonVariation.FUNCTION}
              af-size={ButtonSize.MEDIUM}>
              <digi-icon
                class="digi-code-example__icon"
                aria-hidden="true"
                slot="icon"
                afName={`copy`}
              />
              {_t.code.copy}
            </digi-button>
          </div>
        ) : (
          !this.afHideControls && (
            <digi-button
              class={{
                'digi-code-example__copy-button': true,
                'digi-code-example__copy-button--dark':
                  this.afCodeBlockVariation === 'dark'
              }}
              onAfOnClick={() => this.copyButtonClickHandler()}
              af-variation={ButtonVariation.FUNCTION}
              af-size={ButtonSize.MEDIUM}>
              <digi-icon
                class="digi-code-example__icon"
                aria-hidden="true"
                slot="icon"
                afName={`copy`}
              />
              {_t.code.copy}
            </digi-button>
          )
        )}
      </div>
    );
  }
}
