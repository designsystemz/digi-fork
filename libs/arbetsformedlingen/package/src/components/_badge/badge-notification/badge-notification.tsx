import { Component, h, Host, Prop, State, Watch } from '@stencil/core';
import { BadgeNotificationShape } from './badge-notification-shape.enum';

/**
 *
 * @enums  BadgeNotificationShape - badge-notification-shape.enum.ts
 *
 * @swedishName Notifikationsindikator
 */
@Component({
  tag: 'digi-badge-notification',
  styleUrl: 'badge-notification.scss',
  scoped: true
})
export class BadgeNotification {
  @State() afShape: BadgeNotificationShape = BadgeNotificationShape.PILL;
  @State() afText = '';

  /**
   * Sätt attributet 'aria-label'.
   * @en Set button `aria-label` attribute.
   */
  @Prop() afAriaLabel: string;

  /**
   * Värdet som ska skrivas ut i notifieringen.
   * @en The value to be displayed in the badge.
   */
  @Prop() afValue: string | number = '';
  @Watch('afValue')
  afValueChangeHandler() {
    this.checkProps();
  }

  get cssModifiers() {
    return {
      'digi-badge-notification--circle':
        this.afShape === BadgeNotificationShape.CIRCLE,
      'digi-badge-notification--oval':
        this.afShape === BadgeNotificationShape.OVAL,
      'digi-badge-notification--pill':
        this.afShape === BadgeNotificationShape.PILL
    };
  }

  componentWillLoad() {
    this.checkProps();
  }

  checkProps() {
    let valueAsNumber;

    if (typeof this.afValue === 'string') {
      valueAsNumber = parseInt(this.afValue);
    }

    if (!isNaN(valueAsNumber)) {
      if (valueAsNumber >= 100) {
        this.afValue = '+99';
        this.afShape = BadgeNotificationShape.PILL;
      } else if (valueAsNumber < 100 && valueAsNumber >= 10) {
        this.afShape = BadgeNotificationShape.OVAL;
      } else {
        this.afShape = BadgeNotificationShape.CIRCLE;
      }
    } else {
      this.afShape = BadgeNotificationShape.PILL;
    }
  }

  render() {
    return (
      <Host>
        <div
          class={{
            'digi-badge-notification': true,
            ...this.cssModifiers
          }}
          aria-label={this.afAriaLabel}>
          <div class={`${this.afShape}`}>
            <span>{this.afValue}</span>
          </div>
        </div>
      </Host>
    );
  }
}
