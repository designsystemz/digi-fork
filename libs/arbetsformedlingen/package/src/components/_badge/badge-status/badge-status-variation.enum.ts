export enum BadgeStatusVariation {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
