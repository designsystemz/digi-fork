export enum BadgeStatusSize {
  SMALL = 'small',
  LARGE = 'large'
}
