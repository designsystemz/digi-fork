import { Component, Event, EventEmitter, Prop, Watch, h } from '@stencil/core';
import { setupId } from '../../../global/utils/randomIdGenerator';

import {
  FilterItemId,
  FocusElement,
  FormFilterItem,
  RefIndex,
  FormFilterState,
  FilterChange
} from './form-filter-presentational.interface';

/**
 * @private internal use only
 * @slot default - Ska vara formulärselement. Till exempel digi-form-checkbox.
 * @swedishName Multifilter
 */
@Component({
  tag: 'digi-form-filter-presentational',
  styleUrls: ['form-filter-presentational.scss'],
  scoped: true
})
export class FormFilterPresentational {
  private createId = setupId('digi-form-filter');

  private rootRef: HTMLElement;
  private menuRef: HTMLElement;
  private toggleRef: HTMLButtonElement;
  private submitRef: HTMLButtonElement;
  private resetRef: HTMLButtonElement;
  private listItemRefs: { [key: FilterItemId]: HTMLInputElement } = {};

  /**
   * Texten i filterknappen
   * @en Set the filter button text
   */
  @Prop() afFilterButtonText!: string;

  /**
   * Texten i submitknappen
   * @en Set the submit button text
   */
  @Prop() afSubmitButtonText!: string;

  /**
   * Skärmläsartext för submitknappen
   * @en Set the submit button aria-label text
   */
  @Prop() afSubmitButtonTextAriaLabel: string;

  /**
   * Texten i rensaknappen
   * @en Set the reset button text
   */
  @Prop() afResetButtonText!: string;

  /**
   * Skärmläsartext för resetknappen
   * @en Set the reset button aria-label text
   */
  @Prop() afResetButtonTextAriaLabel: string;

  /**
   * Döljer rensaknappen när värdet sätts till true
   * @en Hides the reset button when set to true
   */
  @Prop() afHideResetButton = false;

  /**
   * Justera dropdown från höger till vänster
   * @en Align dropdown from right to left
   */
  @Prop() afAlignRight: boolean;

  /**
   * Namnet på filterlistans valda filter vid submit. Används även av legendelementet inne i filterlistan.
   * @en Binding name of filterlist selected filters on submit. Also used by the legend element.
   */
  @Prop() afName: string;

  /**
   * Sätter attributet 'aria-label' på filterknappen
   * @en Input aria-label attribute on filter button
   */
  @Prop() afFilterButtonAriaLabel: string;

  /**
   * Sätter attributet 'aria-describedby' på filterknappen
   * @en Input aria-describedby attribute on filter button
   */
  @Prop() afFilterButtonAriaDescribedby: string;

  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  @Prop() autoFocus: boolean;

  /**
   * Lista med val
   * @en List with option
   */
  @Prop() listItems: FormFilterItem[];

  /**
   * Indikerar om list-alternativet är ikryssat eller inte. Måste vara id på list-alternativ.
   * @en Indicates if list item is checked. Must be id of list item.
   */
  @Prop() checked: FilterItemId[] = [];

  /**
   *
   */
  @Prop() tabbable: FilterItemId[] = [];

  /**
   * Indikerar om filter-menyn är synlig
   * @en Indicates if filter menu is visible
   */
  @Prop() showMenu = false;

  /**
   * När menyn öppnas / stängs. Skickar med information om meny visas eller inte.
   * @en On menu open / close along with information if menu is shown or not.
   */
  @Event() toggleMenu: EventEmitter<boolean>;

  /**
   * När filtervalet är godkänt. Skickar information med alla filter samt ikryssad status.
   * @en On filter submission, contains current filters and corresponding check status.
   */
  @Event() submitFilter: EventEmitter<FormFilterState>;

  /**
   * Vid reset
   * @en On reset
   */
  @Event() resetFilter: EventEmitter;

  /**
   * När ett filter-alternativ blir markerat / avmarkerat
   * @en When a filter-item is checked / unchecked
   */
  @Event() changeFilter: EventEmitter<FilterChange>;

  /**
   * När referenser till HTML-noder är redo
   * @en When references for HTML nodes are ready
   */
  @Event() refs: EventEmitter<RefIndex<FocusElement>>;

  componentDidLoad() {
    this.emitRefs();
  }

  @Watch('listItems')
  emitRefs() {
    this.refs.emit({
      root: this.rootRef,
      menu: this.menuRef,
      filter: this.listItemRefs,
      reset: this.resetRef,
      submit: this.submitRef,
      toggle: this.toggleRef
    });
  }

  handleSubmit() {
    this.submitFilter.emit({
      listItems: this.listItems,
      checked: this.checked
    });
  }

  handleReset() {
    this.resetFilter.emit();
  }

  handleCheckItem(item: FormFilterItem) {
    const eventData = {
      id: item.id,
      isChecked: this.checked.includes(item.id)
    };
    this.changeFilter.emit(eventData);
  }

  handleToggleMenu() {
    this.toggleMenu.emit(this.showMenu);
  }

  render() {
    return (
      <div
        id={this.createId('identifier')}
        ref={(ref) => (this.rootRef = ref)}
        class={{
          'digi-form-filter': true,
          'digi-form-filter--open': this.showMenu
        }}>
        <digi-button
          id={this.createId('toggle-button')}
          afInputRef={(ref) => (this.toggleRef = ref)}
          class="digi-form-filter__toggle-button"
          onAfOnClick={() => this.handleToggleMenu()}
          af-aria-haspopup="true"
          af-variation="secondary"
          af-aria-label={this.afFilterButtonAriaLabel}
          af-aria-labelledby={this.afFilterButtonAriaDescribedby}
          af-aria-controls={this.createId('dropdown-menu')}
          af-aria-expanded={this.showMenu}
          af-autofocus={this.autoFocus ? this.autoFocus : null}>
          <i
            class={{
              'digi-form-filter__toggle-button-indicator': true,
              'digi-form-filter__toggle-button-indicator--active':
                this.checked.length > 0
            }}></i>
          <span class="digi-form-filter__toggle-button-text">
            {this.afFilterButtonText}
          </span>
          <digi-icon
            class="digi-form-filter__toggle-icon"
            slot="icon-secondary"
            afName={`chevron-down`}></digi-icon>
        </digi-button>

        <div
          id={this.createId('dropdown-menu')}
          ref={(ref) => (this.menuRef = ref)}
          class={{
            'digi-form-filter__dropdown': true,
            'digi-form-filter__dropdown--right': this.afAlignRight,
            'digi-form-filter__dropdown--hidden': !this.showMenu
          }}>
          <div class="digi-form-filter__scroll">
            <fieldset>
              {this.afName ? (
                <legend class="digi-form-filter__legend">{this.afName}</legend>
              ) : (
                <legend class="legend-hidden">{this.afFilterButtonText}</legend>
              )}
              <ul
                class="digi-form-filter__list"
                aria-label={this.afFilterButtonAriaLabel}
                id={this.createId('filter-list')}>
                {this.listItems.map((item) => {
                  const isChecked = this.checked.includes(item.id);
                  const isTabbable = this.tabbable.includes(item.id);
                  return (
                    <li class="digi-form-filter__item" key={item.id}>
                      <digi-form-checkbox
                        afInputRef={(ref) => (this.listItemRefs[item.id] = ref)}
                        afTabIndex={isTabbable ? 0 : -1}
                        afLabel={item.label}
                        afValue={item.id}
                        afChecked={isChecked}
                        onClick={(e) => {
                          e.preventDefault(); // or internal state will decide check status
                          this.handleCheckItem(item);
                        }}
                      />
                    </li>
                  );
                })}
              </ul>
            </fieldset>
          </div>
          <div class="digi-form-filter__footer">
            <digi-button
              class="digi-form-filter__submit-button"
              af-variation="primary"
              af-aria-label={this.afSubmitButtonTextAriaLabel}
              onClick={() => this.handleSubmit()}
              afInputRef={(ref) => (this.submitRef = ref)}>
              {this.afSubmitButtonText}
            </digi-button>
            {!this.afHideResetButton && (
              <digi-button
                af-variation="secondary"
                af-aria-label={this.afResetButtonTextAriaLabel}
                onClick={() => this.handleReset()}
                class="digi-form-filter__reset-button"
                afInputRef={(ref) => (this.resetRef = ref)}>
                {this.afResetButtonText}
              </digi-button>
            )}
          </div>
        </div>
      </div>
    );
  }
}
