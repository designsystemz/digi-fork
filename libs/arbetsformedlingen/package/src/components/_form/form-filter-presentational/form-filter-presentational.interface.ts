export type FilterItemId = string;

export interface FormFilterItem {
  id: FilterItemId;
  label: string;
}

export enum FocusElement {
  Menu = 'menu',
  Root = 'root',
  Toggle = 'toggle',
  Filter = 'filter',
  Submit = 'submit',
  Reset = 'reset'
}

export type RefIndex<T extends keyof never> = {
  [key in T]:
    | {
        [id: string]: HTMLElement;
      }
    | HTMLElement;
};

export interface FormFilterState {
  listItems: FormFilterItem[];
  checked: FilterItemId[];
}

export interface FilterChange {
  id: FilterItemId;
  isChecked: boolean;
}
