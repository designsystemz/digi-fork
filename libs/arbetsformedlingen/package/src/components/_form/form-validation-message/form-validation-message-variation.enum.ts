export enum FormValidationMessageVariation {
  SUCCESS = 'success',
  ERROR = 'error',
  WARNING = 'warning'
}
