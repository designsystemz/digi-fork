import {
  Component,
  Element,
  Event,
  EventEmitter,
  Method,
  Prop,
  State,
  Watch,
  h
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormTextareaVariation } from './form-textarea-variation.enum';
import { FormTextareaValidation } from './form-textarea-validation.enum';

/**
 * @enums FormTextareaValidation - form-textarea-validation.enum.ts
 * @enums FormTextareaVariation - form-textarea-variation.enum.ts
 * @swedishName Textarea
 */
@Component({
  tag: 'digi-form-textarea',
  styleUrls: ['form-textarea.scss'],
  scoped: true
})
export class FormTextarea {
  @Element() hostElement: HTMLStencilElement;

  private _textarea?: HTMLTextAreaElement;

  @State() hasActiveValidationMessage = false;
  @State() dirty = false;
  @State() touched = false;

  /**
   * Texten till labelelementet
   * @en The label text
   */
  @Prop() afLabel!: string;

  /**
   * Valfri beskrivande text
   * @en A description text
   */
  @Prop() afLabelDescription: string;

  /**
   * Sätter variant. Kan vara 'small', 'medium', 'large' eller 'auto'
   * @en Set variation.
   */
  @Prop() afVariation: FormTextareaVariation = FormTextareaVariation.MEDIUM;

  /**
   * Sätter attributet 'name'.
   * @en set name attribute
   */
  @Prop() afName: string;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-form-textarea');

  /**
   * Sätter attributet 'maxlength'.
   * @en Set maxlength attribute
   */
  @Prop() afMaxlength: number;

  /**
   * Sätter attributet 'minlength'.
   * @en Set minlength attribute
   */
  @Prop() afMinlength: number;

  /**
   * Sätter attributet 'required'.
   * @en Set required attribute
   */
  @Prop() afRequired: boolean;

  /**
   * Sätter text för afRequired.
   * @en Set text for afRequired.
   */

  @Prop() afRequiredText: string;

  /**
   * Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.
   * @en Set this to true if the form contains more required fields than optional fields.
   */
  @Prop() afAnnounceIfOptional = false;

  /**
   * Sätter text för afAnnounceIfOptional.
   * @en Set text for afAnnounceIfOptional.
   */
  @Prop() afAnnounceIfOptionalText: string;

  /**
   * Sätter attributet 'value'.
   * @en Set value attribute
   * @ignore
   */
  @Prop() value: string;
  @Watch('value') onValueChanged(value) {
    this.afValue = value;
  }

  /**
   * Sätter attributet 'value'.
   * @en Input value attribute
   */
  @Prop() afValue: string;
  @Watch('afValue') onAfValueChanged(value) {
    this.value = value;
  }

  /**
   * Sätter valideringsstatus. Kan vara 'success', 'error', 'warning', 'neutral' eller ingenting.
   * @en Set validation status
   */
  @Prop() afValidation: FormTextareaValidation = FormTextareaValidation.NEUTRAL;

  /**
   * Sätter valideringsmeddelandet
   * @en Set input validation message
   */
  @Prop() afValidationText: string;

  /**
   * Sätter attributet 'role'.
   * @en Set role attribute
   */
  @Prop() afRole: string;

  /**
   * Sätter attributet 'aria-activedescendant'.
   * @en Set aria-activedescendant attribute
   */
  @Prop() afAriaActivedescendant: string;

  /**
   * Sätter attributet 'aria-labelledby'.
   * @en Set aria-labelledby attribute
   */
  @Prop() afAriaLabelledby: string;

  /**
   * Sätter attributet 'aria-describedby'.
   * @en Set aria-describedby attribute
   */
  @Prop() afAriaDescribedby: string;

  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  @Prop() afAutofocus: boolean;

  /**
   * Textareatelementets 'onchange'-event.
   * @en The textarea element's 'onchange' event.
   */
  @Event() afOnChange: EventEmitter;

  /**
   * Textareatelementets 'onblur'-event.
   * @en The textarea element's 'onblur' event.
   */
  @Event() afOnBlur: EventEmitter;

  /**
   * Textareatelementets 'onkeyup'-event.
   * @en The textarea element's 'onkeyup' event.
   */
  @Event() afOnKeyup: EventEmitter;

  /**
   * Textareatelementets 'onfocus'-event.
   * @en The textarea element's 'onfocus' event.
   */
  @Event() afOnFocus: EventEmitter;

  /**
   * Textareatelementets 'onfocusout'-event.
   * @en The textarea element's 'onfocusout' event.
   */
  @Event() afOnFocusout: EventEmitter;

  /**
   * Textareatelementets 'oninput'-event.
   * @en The textarea element's 'oninput' event.
   */
  @Event() afOnInput: EventEmitter;

  /**
   * Sker vid textareaelementets första 'oninput'
   * @en First time the textarea element receives an input
   */
  @Event() afOnDirty: EventEmitter;

  /**
   * Sker vid textareaelementets första 'onblur'
   * @en First time the textelement is blurred
   */
  @Event() afOnTouched: EventEmitter;

  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet.
   * @en When the component and slots are loaded and initialized this event will trigger.
   */
  @Event({
    bubbles: false,
    cancelable: true
  })
  afOnReady: EventEmitter;

  @Watch('afValidationText')
  afValidationTextWatch() {
    this.setActiveValidationMessage();
  }

  /**
   * Hämtar en referens till textareaelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the textarea element. Handy for setting focus programmatically.
   */
  @Method()
  async afMGetFormControlElement() {
    return this._textarea;
  }

  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
    this.setActiveValidationMessage();
  }

  componentDidLoad() {
    this.afOnReady.emit();
  }

  setActiveValidationMessage() {
    this.hasActiveValidationMessage = !!this.afValidationText;
  }

  get cssModifiers() {
    return {
      'digi-form-textarea--small':
        this.afVariation === FormTextareaVariation.SMALL,
      'digi-form-textarea--medium':
        this.afVariation === FormTextareaVariation.MEDIUM,
      'digi-form-textarea--large':
        this.afVariation === FormTextareaVariation.LARGE,
      'digi-form-textarea--auto':
        this.afVariation === FormTextareaVariation.AUTO,
      'digi-form-textarea--neutral':
        this.afValidation === FormTextareaValidation.NEUTRAL,
      'digi-form-textarea--success':
        this.afValidation === FormTextareaValidation.SUCCESS,
      'digi-form-textarea--error':
        this.afValidation === FormTextareaValidation.ERROR,
      'digi-form-textarea--warning':
        this.afValidation === FormTextareaValidation.WARNING,
      'digi-form-textarea--empty':
        !this.afValue &&
        (!this.afValidation ||
          this.afValidation === FormTextareaValidation.NEUTRAL)
    };
  }

  blurHandler(e) {
    if (!this.touched) {
      this.afOnTouched.emit(e);
      this.touched = true;
    }
    this.setActiveValidationMessage();
    this.afOnBlur.emit(e);
  }

  changeHandler(e) {
    this.afOnChange.emit(e);
  }

  focusHandler(e) {
    this.afOnFocus.emit(e);
  }

  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }

  keyupHandler(e) {
    this.afOnKeyup.emit(e);
  }

  inputHandler(e) {
    this.afValue = this.value = e.target.value;
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.setActiveValidationMessage();
    this.afOnInput.emit(e);
  }

  render() {
    return (
      <div
        class={{
          'digi-form-textarea': true,
          ...this.cssModifiers
        }}>
        <div class="digi-form-textarea__label">
          <digi-form-label
            afFor={this.afId}
            afLabel={this.afLabel}
            afDescription={this.afLabelDescription}
            afRequired={this.afRequired}
            afAnnounceIfOptional={this.afAnnounceIfOptional}
            afRequiredText={this.afRequiredText}
            afAnnounceIfOptionalText={
              this.afAnnounceIfOptionalText
            }></digi-form-label>
        </div>
        <div class="digi-form-textarea__content">
          <textarea
            class="digi-form-textarea__textarea"
            ref={(el) => (this._textarea = el as HTMLTextAreaElement)}
            onBlur={(e) => this.blurHandler(e)}
            onChange={(e) => this.changeHandler(e)}
            onFocus={(e) => this.focusHandler(e)}
            onFocusout={(e) => this.focusoutHandler(e)}
            onKeyUp={(e) => this.keyupHandler(e)}
            onInput={(e) => this.inputHandler(e)}
            aria-activedescendant={this.afAriaActivedescendant}
            aria-describedby={this.afAriaDescribedby}
            aria-labelledby={this.afAriaLabelledby}
            aria-invalid={
              this.afValidation === FormTextareaValidation.ERROR
                ? 'true'
                : 'false'
            }
            maxLength={this.afMaxlength}
            minLength={this.afMinlength}
            role={this.afRole}
            required={this.afRequired}
            id={this.afId}
            name={this.afName}
            value={this.afValue}
            autofocus={this.afAutofocus ? this.afAutofocus : null}></textarea>
        </div>
        <div class="digi-form-textarea__footer">
          <div
            class="digi-form-textarea__validation"
            aria-atomic="true"
            role="alert"
            id={`${this.afId}--validation-message`}>
            {this.hasActiveValidationMessage &&
              this.afValidation != FormTextareaValidation.NEUTRAL && (
                <digi-form-validation-message
                  class="digi-form-textarea__validation-message"
                  af-variation={this.afValidation}>
                  {this.afValidationText}
                </digi-form-validation-message>
              )}
          </div>
        </div>
      </div>
    );
  }
}
