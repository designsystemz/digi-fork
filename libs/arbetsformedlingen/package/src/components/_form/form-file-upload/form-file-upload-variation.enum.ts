export enum FormFileUploadVariation {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large'
}
