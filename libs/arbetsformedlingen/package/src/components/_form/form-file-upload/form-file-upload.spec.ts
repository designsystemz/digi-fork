import { expect, it } from '@jest/globals';

describe('FormFileUpload', () => {
  it('dummytest - should render the component', () => {
    expect(1).toBe(1);
  });
  /*
   * Vilka fel kan uppstå
   * - filen överskrider en viss storlek
   * - filen är av fel typ
   * - fler filer än vad som är tillåtet
   * - filen är obligatorisk
   * - filen valideras utifrån och skickas tillbaks via afMValidateFile med status OK eller ERROR
   * är statusen Error så kan meddelande sättas på returnerat objekt
   *
   *
   * Olika sätt som komponenten tar emot filer -
   *
   * 1. Om en fil laddas upp via komponenten:
   * - Validera
   * --dublett
   * --storlek
   * --filtyp
   * --antal
   *
   * - Sedan
   * -- ge filen en status (pending/OK) beroende på afValidation
   * -- emitta filen
   *
   * 2. Om en fil skickas in via afMValidateFile - då förväntas filen redan finnas i komponenten med status "pending"
   * - Validera
   * -- status på filen
   *
   * - Sedan
   * -- Om status = error -> Visa felmeddelanden och ta bort filen från listan
   * -- Om status = OK -> Visa fil i listan på filer
   *
   * 3. Om filer importeras
   * - Validera
   * --dublett
   * --storlek
   * --filtyp
   * --antal
   *
   * - Sedan
   * -- Visa resultatet från valideringen
   * -- Filen ska inte emittas
   *
   *
   * metoden afMGetAllFiles ska bara ge OK filer
   *
   * komponenten ska emitta ett uppladdningsevent när en fil laddats upp
   *
   * - Valideringen ska funka
   * -- inga dubbletter av namn
   * -- inte överskrida storlek
   * -- inte överskrida antal
   * -- inte acceptera fel filtyp
   *
   * om afValidation true så ska validering av filerna också göras utanför komponenten
   *
   *
   * */
});
