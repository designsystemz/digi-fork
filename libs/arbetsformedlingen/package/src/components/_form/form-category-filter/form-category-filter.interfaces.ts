export interface FormCategory {
  name: string;
  hits?: string | number;
  selected?: boolean;
}
