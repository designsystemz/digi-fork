import {
  Component,
  h,
  Host,
  Prop,
  Event,
  EventEmitter,
  State,
  Watch,
  Element
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { FormCategory } from './form-category-filter.interfaces';

/**
 * @swedishName Kategorifilter
 */
@Component({
  tag: 'digi-form-category-filter',
  styleUrl: 'form-category-filter.scss',
  scoped: true
})
export class FormCategoryFilter {
  @Element() hostElement: HTMLStencilElement;

  /**
   * En lista av kategorier
   * @en A list of categories
   */
  @Prop() afCategories: Array<FormCategory> = [];

  @Watch('afCategories')
  afCategoriesChangeHandler() {
    this.selectedCategories = this.afCategories
      .filter((category) => category.selected)
      .map((category) => category.name);
    if (
      this.selectedCategories.length === 0 &&
      this.afAllCategories &&
      this.afAllCategoriesSelected
    )
      this.selectedCategories = [this.afAllCategoriesText];
  }

  /**
   * Ifall listan ska börja med en 'Alla kategorier' kategori
   * @en If the list should start with a 'All categories' category
   */
  @Prop() afAllCategories = true;

  /**
   * Ifall 'Alla kategorier' kategorin ska börja vald
   * @en If the 'All categories' category should start selected
   */
  @Prop() afAllCategoriesSelected = true;

  /**
   * Namnet på 'Alla kategorier' kategorin
   * @en The name of the 'All categories' category
   */
  @Prop() afAllCategoriesText = 'Alla kategorier';

  /**
   * Antal synliga kategorier när listan är kollapsad (ifall det finns färre valbara kategorier än detta värde så syns inte 'Visa fler/färre'-knappen)
   * @en Number of visible categories when the list is collapsed (the 'Show more/less'-button is not shown if there are fewer selectable categories than this number)
   */
  @Prop() afVisibleCollapsed = 5;

  /**
   * Ifall listan ska var kollapsad från början
   * @en If the list should start out collapsed
   */
  @Prop() afStartCollapsed = true;

  /**
   * Välj om man ska kunna filtrera på flera kategorier åt gången.
   * @en Set if filtering should use multiselect.
   */
  @Prop() afMultiselect = true;

  /**
   * Visa alla kategorier och dölj knappen för att "visa fler/visa färre
   * @en Show all categories and hide button to "visa fler"/"visa färre"
   */
  @Prop() afHideToggle = false;

  /**
   * Sker vid ändrade valda kategorier
   * @en When selected categories is changed
   */
  @Event() afOnSelectedCategoryChange: EventEmitter;

  @State() selectedCategories: Array<string> = [];
  @State() collapsed = false;

  componentDidLoad() {
    this.afCategoriesChangeHandler();
    this.collapsed = this.afStartCollapsed;
  }

  displayCategories(): Array<FormCategory> {
    if (this.afHideToggle) {
      return [{ name: this.afAllCategoriesText }, ...this.afCategories];
    }
    if (this.afAllCategories) {
      if (
        this.collapsed &&
        this.afCategories.length + 1 > this.afVisibleCollapsed
      )
        return [
          { name: this.afAllCategoriesText },
          ...this.afCategories.slice(0, this.afVisibleCollapsed - 1)
        ];
      return [{ name: this.afAllCategoriesText }, ...this.afCategories];
    }
    if (this.collapsed && this.afCategories.length > this.afVisibleCollapsed)
      return this.afCategories.slice(0, this.afVisibleCollapsed);
    return this.afCategories;
  }

  toggleCategory(category: FormCategory) {
    if (!this.afMultiselect) {
      if (this.afAllCategories && category.name === this.afAllCategoriesText) {
        if (this.selectedCategories.includes(this.afAllCategoriesText)) {
          this.selectedCategories = [];
          this.afOnSelectedCategoryChange.emit([]);
        } else {
          this.selectedCategories = [this.afAllCategoriesText];
          this.afOnSelectedCategoryChange.emit(
            this.afCategories.map((category) => category.name)
          );
        }
      }

      // Not allCategories

      this.selectedCategories = [category.name];
      this.afOnSelectedCategoryChange.emit(this.selectedCategories);
      return;
    }

    if (this.afAllCategories && category.name === this.afAllCategoriesText) {
      if (this.selectedCategories.includes(this.afAllCategoriesText)) {
        this.selectedCategories = [];
        this.afOnSelectedCategoryChange.emit([]);
      } else {
        this.selectedCategories = [this.afAllCategoriesText];
        this.afOnSelectedCategoryChange.emit(
          this.afCategories.map((category) => category.name)
        );
      }
    } else {
      if (
        this.selectedCategories.findIndex((name) => name === category.name) >= 0
      )
        this.selectedCategories = this.selectedCategories.filter(
          (name) => name !== category.name
        );
      else if (this.afAllCategories)
        this.selectedCategories = [
          ...this.selectedCategories.filter(
            (name) => name !== this.afAllCategoriesText
          ),
          category.name
        ];
      else
        this.selectedCategories = [...this.selectedCategories, category.name];
      this.afOnSelectedCategoryChange.emit(this.selectedCategories);
    }
  }

  toggleCollapsed() {
    this.collapsed = !this.collapsed;
  }

  toggleHidden() {
    this.afHideToggle = !this.toggleHidden;
  }

  render() {
    return (
      <Host>
        <div
          class={{
            'digi-form-category-filter': true
          }}>
          {this.displayCategories().map((category) => (
            <digi-button
              class={{
                'digi-form-category-filter--category': true,
                'digi-form-category-filter--category--not-selected':
                  !this.selectedCategories.includes(category.name)
              }}
              afVariation="primary"
              afAriaPressed={this.selectedCategories.includes(category.name)}
              onAfOnClick={() => {
                this.toggleCategory(category);
              }}>
              {typeof category.hits === 'undefined' && category.name}
              {typeof category.hits !== 'undefined' && (
                <span>
                  {category.name}{' '}
                  <span class="digi-form-category-filter--category--thin-text">
                    ({category.hits})
                  </span>
                </span>
              )}
            </digi-button>
          ))}
          {this.afHideToggle === false &&
            this.afCategories.length > this.afVisibleCollapsed && (
              <div>
                <digi-button
                  afVariation="function"
                  afAriaExpanded={!this.collapsed}
                  onClick={() => {
                    this.toggleCollapsed();
                  }}>
                  {this.collapsed ? 'Visa alla' : 'Visa färre'}
                  {this.collapsed ? (
                    <digi-icon-plus
                      slot="icon"
                      aria-hidden="true"></digi-icon-plus>
                  ) : (
                    <digi-icon-minus
                      slot="icon"
                      aria-hidden="true"></digi-icon-minus>
                  )}
                </digi-button>
              </div>
            )}
        </div>
      </Host>
    );
  }
}
