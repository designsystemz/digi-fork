/* eslint-disable @typescript-eslint/no-explicit-any */

import {
  Component,
  Element,
  Event,
  EventEmitter,
  Prop,
  State,
  Listen,
  Watch,
  h,
  Method
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';
import { _t } from '../../../text';
import { IListItem } from './form-select-filter.interface';
import {
  DigiFormInputCustomEvent,
  DigiUtilKeydownHandlerCustomEvent
} from '../../../components';
import {
  FormInputType,
  FormInputValidation,
  FormInputVariation
} from '../../../enums';
import { FormSelectFilterValidation } from './form-select-validation.enum';

/**
 * @swedishName Väljare med sök
 */

@Component({
  tag: 'digi-form-select-filter',
  styleUrls: ['form-select-filter.scss'],
  scoped: true
})
export class FormSelectFilter {
  @Element() hostElement: HTMLStencilElement;

  @State() isOpen = false;
  @State() focusListItemIndex = 0;

  @State() focusInList = false;

  private _toggleButton;
  private _submitButton;
  private _searchQuery = '';

  /**
   * En valfri beskrivningstext.
   * @en An optional description text.
   */
  @Prop() afDescription: string;

  /**
   * Inaktiverar valideringen
   * @en Disables the validation.
   */
  @Prop() afDisableValidation: boolean;

  /**
   * Sätter väljarens validering
   * @en Set select validation
   */
  @Prop() afValidation: FormSelectFilterValidation =
    FormSelectFilterValidation.NEUTRAL;

  /**
   * Sätter valideringstexten.
   * @en Sets the validation text.
   */
  @Prop() afValidationText: string;

  /**
   * Sätt attributet till "true" om fältet är obligatoriskt.
   * @en Set it to "true" if field is required.
   */
  @Prop() afRequired: boolean;

  /**
   * Sätter text för afRequired.
   * @en Set text for afRequired.
   */

  @Prop() afRequiredText: string;

  /**
   * Normalt betonas ett obligatoriskt fält visuellt och semantiskt.
   * Detta vänder på det och betonar fältet om det inte krävs.
   * Ställ in detta till sant om ditt formulär innehåller fler obligatoriska fält än inte.
   * @en Normally a required field is visually and semantically emphasized.
   * This flips that and emphasizes the field if it is not required.
   * Set this to true if your form contains more required fields than not.
   */
  @Prop() afAnnounceIfOptional = false;

  /**
   * Sätter text för afAnnounceIfOptional.
   * @en Set text for afAnnounceIfOptional.
   */
  @Prop() afAnnounceIfOptionalText: string;

  /**
   * Sorterar listan i alfabetisk ordning.
   * @en Sort the list in alphabetical order.
   */
  @Prop() sortAlphabetically = true;

  /**
   * @ignore
   */
  @Prop({ mutable: true }) value: IListItem[] = [];
  @Watch('value') onValueChanged(value: IListItem[]) {
    const joinedArray = [...value, ...this._selectedItems];
    const noDuplicates = joinedArray.filter(
      (item, pos) => joinedArray.indexOf(item) === pos
    );
    if (noDuplicates && noDuplicates.length > 0) {
      if (this.afMultipleItems) {
        this._selectedItems = [...noDuplicates];
      } else {
        this._selectedItems = [noDuplicates[0]];
      }
    }
  }

  /**
   * Sätts till true om det ska gå att välja flera val från listan.
   * @en Set to true if you should be able to choose multiple items from the list.
   */
  @Prop({ mutable: false }) afMultipleItems = false;

  /**
   * Lista med val.
   * @en List with option.
   */
  @Prop() afListItems: string | IListItem[];
  @State() _afListItems: IListItem[];
  @State() _filteredResult: IListItem[] = [];
  @State() _selectedItems: IListItem[] = [];

  @Watch('afListItems')
  handleListItems() {
    if (this.afListItems) {
      try {
        if (this.afListItems.constructor === Array) {
          this._afListItems = this.afListItems;
        } else if (typeof this.afListItems === 'string') {
          this._afListItems = JSON.parse(this.afListItems);
        } else {
          throw `Invalid type in "af-list-items" attribute`;
        }

        this.giveItemsValueIfNoneExist();

        this.sortListAlphabetically();

        const selectedListItems = this._afListItems.filter(
          (item) => item.selected
        );
        if (selectedListItems && selectedListItems.length > 0) {
          this._selectedItems = [...selectedListItems];
          this.afOnSelect.emit(this._selectedItems);
          this.value = this._selectedItems;
        } else if (this.value && this.value.length > 0) {
          this._selectedItems = [...this.value];
        }
        this.filterResults();
      } catch (e) {
        logger.warn(
          `Invalid JSON in "af-list-items" attribute`,
          this.hostElement,
          e
        );
        return;
      }
    }
  }

  /**
   * Prefixet för alla komponentens olika interna 'id'-attribut på dropdowns, filterlistor etc. Ex. 'my-cool-id' genererar 'my-cool-id-filter-list', 'my-cool-id-dropdown-menu' och så vidare. Ges inget värde så genereras ett slumpmässigt.
   * @en Prefix for all id attributes inside the component. Example: 'my-cool-id' generates 'my-cool-id-filter-list', 'my-cool-id-dropdown-menu' and so on. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-form-select-filter');

  /**
   * Texten till label för knappen
   * @en Set the label text for filter button text
   */
  @Prop() afFilterButtonTextLabel!: string;

  /**
   * Texten i filterknappen
   * @en Set the filter button text
   */
  @Prop() afFilterButtonText!: string;

  /**
   * Texten som visas när man gjort fler än ett val. Använd {count} för att läsa ut antalet val
   * @en Text that is shown in button when user has selected multiple choices. Use {count} to display number of items.
   */
  @Prop() afFilterButtonTextMultipleValues: string;

  /**
   * Texten i submitknappen
   * @en Set the submit button text
   */
  @Prop() afSubmitButtonText: string;

  /**
   * Skärmläsartext för submitknappen
   * @en Set the submit button aria-label text
   */
  @Prop() afSubmitButtonTextAriaLabel: string;

  /**
   * Texten i rensaknappen
   * @en Set the reset button text
   */
  @Prop() afResetButtonText: string = _t.clear;

  /**
   * Skärmläsartext för resetknappen
   * @en Set the reset button aria-label text
   */
  @Prop() afResetButtonTextAriaLabel: string;

  /**
   * Döljer rensaknappen när värdet sätts till true
   * @en Hides the reset button when set to true
   */
  @Prop() afHideResetButton = false;

  /**
   * Justera dropdown från höger till vänster
   * @en Align dropdown from right to left
   */
  @Prop() afAlignRight: boolean;

  /**
   * Label till sökfältet i filterlistan.
   * @en Label for the search-field in filter-list.
   */
  @Prop() afName!: string;

  /**
   * Sätter attributet 'aria-describedby' på filterknappen
   * @en Input aria-describedby attribute on filter button
   */
  @Prop() afFilterButtonAriaDescribedby: string;

  @Event() afOnFocusout: EventEmitter;

  /**
   * Vid uppdatering av filtret
   * @en At filter submit
   */
  @Event() afOnSubmitFilters: EventEmitter;

  /**
   * När filtret stängs utan att valda alternativ bekräftats
   * @en When the filter is closed without confirming the selected options
   */
  @Event() afOnFilterClosed: EventEmitter;

  /**
   * Vid reset av filtret
   * @en At filter reset
   */
  @Event() afOnResetFilters: EventEmitter;

  /**
   * När ett eller flera filter väljs
   * @en When a filter is changed
   */
  @Event() afOnSelect: EventEmitter<any[]>;

  /**
   * När en söksträng ändras
   * @en When a search query is changed.
   */
  @Event() afOnQueryChanged: EventEmitter<string>;

  /**
   *Funktion för att rensa valda värden från komponenten
   *@en.Function for clearing selected values from component
   */
  @Method()
  async afMReset() {
    this.resetSelectItems();
  }

  @Listen('click', { target: 'window' })
  clickHandler(e: MouseEvent): void {
    const target = e.target as HTMLElement;
    const selector = `#${this.afId}-identifier`;

    if (detectClickOutside(target, selector) && this.isOpen) {
      this.emitFilterClosedEvent();
    }
  }

  @Listen('focusin', { target: 'document' })
  focusoutHandler(e: FocusEvent): void {
    const target = e.target as HTMLElement;
    const selector = `#${this.afId}-identifier`;
    const focusOutsideFilter = detectFocusOutside(target, selector);

    if (focusOutsideFilter && this.isOpen) {
      this.emitFilterClosedEvent();
    } else {
      this.focusInList = !detectFocusOutside(
        target,
        `#${this.afId}-select-filter-list`
      );

      //Update tabindex on focused input element
      if (this.isOpen && this.focusInList && target.tagName === 'LI') {
        this.setTabIndex();
        this.updateTabIndex(target);
      }
    }
  }

  debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func.apply(this, args);
      }, timeout);
    };
  }

  setClosed(focusToggleButton = false) {
    this.isOpen = false;

    if (focusToggleButton) {
      document.getElementById(`${this.afId}`).focus();
    }

    this.setScroll(0);
    this._searchQuery = '';
    this.afOnQueryChanged.emit('');
    this.filterResults();
  }
  setScroll(scrollY: number) {
    document.querySelector(
      `#${this.afId}-dropdown-menu .digi-form-select-filter__scroll`
    ).scrollTop = scrollY;
  }

  setOpen() {
    this.isOpen = true;
    this.focusListItemIndex = 0;
    setTimeout(() => {
      this.setFocusSearch();
    }, 50);
  }

  giveItemsValueIfNoneExist() {
    this._afListItems
      .filter((item) => {
        return item.value === undefined;
      })
      .forEach((item) => {
        item.value = item.label;
      });
  }

  sortListAlphabetically() {
    if (!this.sortAlphabetically) {
      return;
    }

    this._afListItems.sort((a, b) => {
      if (a.label < b.label) {
        return -1;
      }
      if (a.label > b.label) {
        return 1;
      }
      return 0;
    });
  }

  toggleDropdown() {
    return this.isOpen ? this.setClosed() : this.setOpen();
  }

  inputItem(identifier, index) {
    const item: HTMLInputElement = this.hostElement
      .querySelectorAll<HTMLInputElement>(
        `#${identifier} .digi-form-select-filter__item`
      )
      .item(index);
    return item;
  }

  emitFilterClosedEvent() {
    this.setClosed(true);
    this.afOnFilterClosed.emit();
  }
  componentWillLoad() {
    this.handleListItems();
  }

  setFocusItem() {
    const element = this.inputItem(
      `${this.afId}-select-filter-list`,
      this.focusListItemIndex
    );
    if (element) {
      element.focus();
    }
  }
  setFocusSearch() {
    const element = this.hostElement.querySelector('input');

    if (element) {
      element.focus();
    }
  }

  setTabIndex() {
    const tabindexes = document.querySelectorAll(`#${this.afId}-dropdown-menu`);

    if (!tabindexes) {
      logger.warn(
        `No input elements was found to set tabindex on.`,
        this.hostElement
      );
      return;
    }

    Array.from(tabindexes).map((tabindex) => {
      tabindex.setAttribute('tabindex', '-1');
    });
  }

  updateTabIndex(element: HTMLElement) {
    if (!element) {
      logger.warn(
        `No input element was found to update tabindex on.`,
        this.hostElement
      );
      return;
    }
    element.setAttribute('tabindex', '0');
  }

  incrementFocusItem() {
    const listLength = this.showNeutralValue
      ? this._filteredResult.length
      : this._filteredResult.length - 1;
    if (this.focusListItemIndex < listLength) {
      this.focusListItemIndex = this.focusListItemIndex + 1;

      this.setFocusItem();
      return;
    } else {
      if (!this.afMultipleItems) {
        this.focusListItemIndex = 0;
        this.setFocusItem();
        return;
      }
    }
    this.tabHandler();
  }

  decrementFocusItem() {
    if (this.focusListItemIndex === 0) {
      this.setFocusSearch();
      return;
    }

    if (this.focusListItemIndex > 0) {
      this.focusListItemIndex = this.focusListItemIndex - 1;
    }
    this.setFocusItem();
  }

  downHandler(e: DigiUtilKeydownHandlerCustomEvent<KeyboardEvent>) {
    e.detail.preventDefault();
    if (e.detail.code === 'ArrowDown' || e.detail.code === 'ArrowRight') {
      this.incrementFocusItem();
    }
  }

  upHandler(e: DigiUtilKeydownHandlerCustomEvent<KeyboardEvent>) {
    e.detail.preventDefault();

    if (e.detail.code === 'ArrowUp' || e.detail.code === 'ArrowLeft') {
      this.decrementFocusItem();
    }
  }

  homeHandler(e: DigiUtilKeydownHandlerCustomEvent<KeyboardEvent>) {
    e.detail.preventDefault();

    this.focusListItemIndex = 0;
    this.setFocusItem();
  }

  endHandler(e: DigiUtilKeydownHandlerCustomEvent<KeyboardEvent>) {
    e.detail.preventDefault();

    this.focusListItemIndex = this.showNeutralValue
      ? this._filteredResult.length
      : this._filteredResult.length - 1;
    this.setFocusItem();
  }

  enterHandler(e: DigiUtilKeydownHandlerCustomEvent<KeyboardEvent>) {
    e.detail.preventDefault();
    (e.detail.target as HTMLElement).click();
  }

  spaceHandler(e: DigiUtilKeydownHandlerCustomEvent<KeyboardEvent>) {
    e.detail.preventDefault();
    (e.detail.target as HTMLElement).click();
  }

  escHandler() {
    if (this.isOpen) {
      this.setClosed(true);
    }
  }

  tabHandler() {
    if (this.isOpen && this.focusInList) {
      if (this.afMultipleItems && this._submitButton) {
        setTimeout(() => {
          if (this._submitButton) {
            this._submitButton.querySelector('button')?.focus();
          }
        }, 10);
      } else {
        this.setClosed(true);
      }
    }
  }

  shiftHandler() {
    if (this.isOpen && this.focusInList) {
      if (this.afMultipleItems && this._toggleButton) {
        this._toggleButton.querySelector('button').focus();
      } else {
        this.setFocusSearch();
      }
    }
  }

  filterResults() {
    const searchstatus = this.hostElement.querySelector(
      `#${this.afId}-searchstatus`
    );

    if (this._searchQuery && this._searchQuery.length > 0) {
      this._filteredResult = [
        ...this._afListItems.filter((item) => {
          return item.label
            .toLowerCase()
            .includes(this._searchQuery.toLowerCase());
        })
      ];
    } else {
      this._filteredResult = [...this._afListItems];
    }
    if (searchstatus) {
      if (this._filteredResult && this._filteredResult.length > 0) {
        searchstatus.classList.add('digi--util--sr-only');
        if (this._searchQuery !== '') {
          searchstatus.innerHTML = `${this._filteredResult.length} sökträff${this._filteredResult.length > 1 ? 'ar' : ''}`;
        } else {
          searchstatus.innerHTML = '';
        }
      } else {
        searchstatus.classList.remove('digi--util--sr-only');
        searchstatus.innerHTML = `Inga sökträffar för ${this._searchQuery}`;
      }
    }
  }

  inputSearchKeyboardHandler(e: KeyboardEvent) {
    if (e.code === 'ArrowDown') {
      e.preventDefault();
      this.setFocusItem();
    }
    if (e.code === 'Enter' && this._searchQuery !== '') {
      e.preventDefault();
      this.setFocusItem();
    }
  }

  inputSearchInputHandler(e: InputEvent) {
    this._searchQuery = (e.target as HTMLInputElement).value;
    this.afOnQueryChanged.emit(this._searchQuery);
    this.filterResults();
  }
  resetSelectItems(close = false) {
    this._selectedItems = [];
    this.value = [];
    this.afOnSelect.emit([]);
    this._searchQuery = '';
    this.filterResults();
    if (close) {
      this.emitFilterClosedEvent();
    }
  }

  selectItemHandler(item: IListItem) {
    if (this.afMultipleItems) {
      item.selected = !item.selected;
      if (
        this._selectedItems.filter((x) => {
          return x.value === item.value;
        }).length > 0
      ) {
        //ITEM VALUE ALREADY EXISTS IN LIST, DELETE FROM LIST
        this._selectedItems.splice(
          this._selectedItems.findIndex((e) => e.value === item.value),
          1
        );
      } else {
        this._selectedItems = [...this._selectedItems, item];
      }
    } else {
      this._selectedItems.forEach((item) => {
        item.selected = false;
      });

      item.selected = true;
      this._selectedItems = [item];

      this.emitFilterClosedEvent();
    }
    this.value = this._selectedItems;
    this.afOnSelect.emit(this._selectedItems);
    this.filterResults();
  }

  isSelected(textValue: string): boolean {
    if (textValue && this._selectedItems && this._selectedItems.length > 0) {
      return (
        this._selectedItems.filter((item) => {
          return item.value === textValue;
        }).length > 0
      );
    }
    return false;
  }

  get cssModifiers() {
    return {
      'digi-form-select-filter--open': this.isOpen,
      'digi-form-select-filter--neutral':
        this.afValidation === FormSelectFilterValidation.NEUTRAL,
      'digi-form-select-filter--success':
        this.afValidation === FormSelectFilterValidation.SUCCESS,
      'digi-form-select-filter--warning':
        this.afValidation === FormSelectFilterValidation.WARNING,
      'digi-form-select-filter--error':
        this.afValidation === FormSelectFilterValidation.ERROR
    };
  }

  get cssModifiersToggleButtonIndicator() {
    return {
      'digi-form-select-filter__toggle-button-indicator--active':
        this._selectedItems.length > 0
    };
  }

  get cssModifiersToggleButtonWrapper() {
    return {
      'digi-form-select-filter__toggle-button-wrapper--activated':
        this._selectedItems.length > 0
    };
  }

  buttonText(isAriaLabel: boolean) {
    const ariaLabelText = (text = '') => {
      return isAriaLabel ? text : '';
    };

    if (this._selectedItems && this._selectedItems.length === 1) {
      return (
        ariaLabelText(this.afFilterButtonTextLabel + ': ') +
        this._selectedItems[0].label
      );
    } else if (
      this._selectedItems &&
      this._selectedItems.length > 0 &&
      this.afMultipleItems
    ) {
      if (this.afFilterButtonTextMultipleValues) {
        return (
          ariaLabelText(this.afFilterButtonTextLabel + ': ') +
          this.afFilterButtonTextMultipleValues.replace(
            '{count}',
            this._selectedItems.length.toString()
          )
        );
      } else {
        return (
          ariaLabelText(this.afFilterButtonTextLabel + ': ') +
          `${this._selectedItems.length} valda`
        );
      }
    } else {
      return (
        ariaLabelText(
          this.afFilterButtonTextLabel
            ? this.afFilterButtonTextLabel + ': '
            : ''
        ) + this.afFilterButtonText
      );
    }
  }

  get renderButtonLabel() {
    return this.buttonText(false);
  }

  get ariaLabel() {
    return this.buttonText(true);
  }

  get showNeutralValue() {
    return (
      !this.afMultipleItems &&
      this.afFilterButtonText &&
      this._searchQuery.length === 0
    );
  }

  showValidation() {
    return !this.afDisableValidation &&
      this.afValidation !== FormSelectFilterValidation.NEUTRAL &&
      this.afValidationText
      ? true
      : false;
  }

  render() {
    return (
      <div
        class={{
          'digi-form-select-filter': true,
          ...this.cssModifiers
        }}
        role="application"
        id={`${this.afId}-identifier`}>
        <digi-form-label
          afLabel={this.afFilterButtonTextLabel}
          afFor={`${this.afId}`}
          afId={`${this.afId}-label`}
          class="digi-form-select-filter__label"
          afDescription={this.afDescription}
          afRequired={this.afRequired}
          afAnnounceIfOptional={this.afAnnounceIfOptional}
          afRequiredText={this.afRequiredText}
          afAnnounceIfOptionalText={
            this.afAnnounceIfOptionalText
          }></digi-form-label>

        <digi-util-keydown-handler onAfOnEsc={() => this.escHandler()}>
          <button
            type="button"
            id={`${this.afId}`}
            class="digi-form-select-filter__toggle-button"
            onClick={() => this.toggleDropdown()}
            ref={(el) => (this._toggleButton = el)}
            aria-haspopup="true"
            aria-label={this.ariaLabel}
            aria-labelledby={this.afFilterButtonAriaDescribedby}
            aria-controls={`${this.afId}-dropdown-menu`}
            aria-expanded={this.isOpen}>
            <div
              class={{
                'digi-form-filter__toggle-button-wrapper': true,
                ...this.cssModifiersToggleButtonWrapper
              }}>
              <digi-icon
                class={{
                  'digi-form-select-filter__toggle-button-indicator': true,
                  ...this.cssModifiersToggleButtonIndicator
                }}
                slot="icon"
                afName={`check`}></digi-icon>
              <span class="digi-form-select-filter__toggle-button-text">
                {this.renderButtonLabel}
              </span>
            </div>
            <digi-icon
              class="digi-form-select-filter__toggle-icon"
              afName={`chevron-down`}></digi-icon>
          </button>

          <div
            id={`${this.afId}-dropdown-menu`}
            class={{
              'digi-form-select-filter__dropdown': true,
              'digi-form-select-filter__dropdown--right': this.afAlignRight,
              'digi-form-select-filter__dropdown--hidden': !this.isOpen
            }}>
            <fieldset>
              <div class="digi-form-select-filter__header">
                <legend class="digi-form-select-filter__legend">
                  {this.afFilterButtonTextLabel}
                </legend>
                <digi-form-input
                  afLabel={this.afName}
                  afId={`${this.afId}-search`}
                  afVariation={FormInputVariation.MEDIUM}
                  afType={FormInputType.SEARCH}
                  afValidation={FormInputValidation.NEUTRAL}
                  onAfOnFocus={() => {
                    this.focusListItemIndex = 0;
                    99;
                  }}
                  onAfOnKeyup={(e: DigiFormInputCustomEvent<KeyboardEvent>) => {
                    this.inputSearchKeyboardHandler(e.detail);
                  }}
                  onAfOnInput={(e: DigiFormInputCustomEvent<InputEvent>) =>
                    this.inputSearchInputHandler(e.detail)
                  }
                  afValue={this._searchQuery}
                  class="digi-form-select-filter__search-input"
                  afAriaControls={`${this.afId}-select-filter-list`}
                  afAutocomplete="off"
                  afAriaAutocomplete="none"></digi-form-input>
                <div
                  role="status"
                  id={`${this.afId}-searchstatus`}
                  class="digi-form-select-filter__search-status"></div>
              </div>
              <div class="digi-form-select-filter__scroll">
                <digi-util-keydown-handler
                  onAfOnDown={(e) => this.downHandler(e)}
                  onAfOnRight={(e) => this.downHandler(e)}
                  onAfOnUp={(e) => this.upHandler(e)}
                  onAfOnLeft={(e) => this.upHandler(e)}
                  onAfOnHome={(e) => this.homeHandler(e)}
                  onAfOnEnd={(e) => this.endHandler(e)}
                  onAfOnEnter={(e) => this.enterHandler(e)}
                  onAfOnSpace={(e) => this.spaceHandler(e)}
                  onAfOnTab={() => this.tabHandler()}
                  onAfOnShiftTab={() => this.shiftHandler()}>
                  <ul
                    class="digi-form-select-filter__list"
                    aria-labelledby={`${this.afId}`}
                    id={`${this.afId}-select-filter-list`}
                    role={this.afMultipleItems ? '' : 'radiogroup'}
                    aria-invalid={
                      this.afMultipleItems
                        ? 'false'
                        : this.afValidation === FormSelectFilterValidation.ERROR
                          ? 'true'
                          : 'false'
                    }
                    aria-errormessage={`${this.afId}--validation-message`}
                    aria-required={
                      this.afMultipleItems
                        ? false
                        : this.afRequired
                          ? 'true'
                          : 'false'
                    }>
                    {this.showNeutralValue && (
                      <li
                        class="digi-form-select-filter__item"
                        role="radio"
                        aria-required={
                          this.afMultipleItems
                            ? 'false'
                            : this.afRequired
                              ? 'true'
                              : 'false'
                        }
                        aria-checked={
                          this._selectedItems.length === 0 ? 'true' : 'false'
                        }
                        onClick={() => this.resetSelectItems(true)}
                        tabIndex={0}
                        aria-label={this.afFilterButtonText}>
                        {this._selectedItems.length === 0 && (
                          <digi-icon
                            class="digi-form-select-filter__item__icon"
                            afName={`check`}
                            aria-hidden={'true'}></digi-icon>
                        )}
                        {this.afFilterButtonText}
                      </li>
                    )}
                    {this._filteredResult &&
                      this._filteredResult.map((item, index) => {
                        return (
                          <li
                            class="digi-form-select-filter__item"
                            key={index}
                            dir={item.dir}
                            lang={item.lang}
                            aria-checked={
                              this.isSelected(item.value) ? 'true' : 'false'
                            }
                            role={this.afMultipleItems ? 'checkbox' : 'radio'}
                            aria-required={
                              this.afMultipleItems
                                ? 'false'
                                : this.afRequired
                                  ? 'true'
                                  : 'false'
                            }
                            aria-invalid={
                              this.afMultipleItems
                                ? this.afValidation ===
                                  FormSelectFilterValidation.ERROR
                                  ? 'true'
                                  : 'false'
                                : 'false'
                            }
                            aria-errormessage={`${this.afId}--validation-message`}
                            onClick={() => this.selectItemHandler(item)}
                            tabIndex={
                              this.showNeutralValue ? -1 : index === 0 ? 0 : -1
                            }>
                            {this.isSelected(item.value) && (
                              <digi-icon
                                class="digi-form-select-filter__item__icon"
                                afName={`check`}
                                aria-hidden={'true'}></digi-icon>
                            )}
                            {item.label}
                          </li>
                        );
                      })}
                  </ul>
                </digi-util-keydown-handler>
              </div>
            </fieldset>
            {this.afMultipleItems && (
              <div class="digi-form-select-filter__footer">
                {!this.afHideResetButton && (
                  <digi-button
                    af-variation="secondary"
                    af-aria-label={this.afResetButtonTextAriaLabel}
                    onClick={() => this.resetSelectItems()}
                    class="digi-form-select-filter__reset-button">
                    {this.afResetButtonText}
                  </digi-button>
                )}
                <digi-util-keydown-handler onAfOnUp={() => this.setFocusItem()}>
                  <digi-button
                    ref={(el) => (this._submitButton = el)}
                    af-variation="primary"
                    af-aria-label={this.afSubmitButtonTextAriaLabel}
                    onClick={() => {
                      this.setClosed(true);
                    }}
                    class="digi-form-select-filter__submit-button">
                    {this.afSubmitButtonText}
                  </digi-button>
                </digi-util-keydown-handler>
              </div>
            )}
          </div>
        </digi-util-keydown-handler>
        <div class="digi-form-select__footer">
          <div
            aria-atomic="true"
            role="alert"
            id={`${this.afId}--validation-message`}>
            {this.showValidation() && (
              <digi-form-validation-message af-variation={this.afValidation}>
                {this.afValidationText}
              </digi-form-validation-message>
            )}
          </div>
        </div>
      </div>
    );
  }
}
