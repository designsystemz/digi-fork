export interface IListItem {
  selected?: boolean; //Är markerad som (för)vald
  label: string; //Namn på  texten som visas i dropdownen.
  value?: string; // value sets type Button
  lang?: string; // Vilket språk är ovan text (2-letter isokod)
  dir?: string; // "LTR" | "RTL"
}
