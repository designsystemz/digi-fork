export enum FormCheckboxVariation {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
