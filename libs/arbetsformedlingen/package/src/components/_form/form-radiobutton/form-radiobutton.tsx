import {
  Component,
  Element,
  Event,
  EventEmitter,
  h,
  Method,
  Prop,
  State,
  Watch
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormRadiobuttonVariation } from './form-radiobutton-variation.enum';
import { FormRadiobuttonValidation } from './form-radiobutton-validation.enum';
import { FormRadiobuttonLayout } from './form-radiobutton-layout.enum';

/**
 * @enums FormRadiobuttonValidation - form-radiobutton-validation.enum.ts
 * @enums FormRadiobuttonVariation - form-radiobutton-variation.enum.ts
 * @swedishName Radioknapp
 */
@Component({
  tag: 'digi-form-radiobutton',
  styleUrls: ['form-radiobutton.scss'],
  scoped: true
})
export class FormRadiobutton {
  @Element() hostElement: HTMLStencilElement;

  private _input?: HTMLInputElement;

  @State() dirty = false;
  @State() touched = false;

  /**
   * Texten till labelelementet
   * @en The label text
   */
  @Prop() afLabel!: string;

  /**
   * Sätter variant. Kan vara 'primary' eller 'secondary'
   * @en Set radiobutton variation.
   */
  @Prop() afVariation: FormRadiobuttonVariation =
    FormRadiobuttonVariation.PRIMARY;

  /**
   * Sätter layouten.
   * @en Set radiobutton layout.
   */
  @Prop() afLayout: FormRadiobuttonLayout = FormRadiobuttonLayout.INLINE;

  /**
   * Sätter attributet 'name'.
   * @en Input name attribute
   */
  @Prop() afName: string;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-form-radiobutton');

  /**
   * Sätter attributet 'required'
   * @en Input required attribute
   */
  @Prop() afRequired: boolean;

  /**
   * Sätter attributet 'value'
   * @en Input value attribute
   * @ignore
   */
  @Prop() value: string;
  @Watch('value') onValueChanged(value) {
    this.afValue = value;
  }

  /**
   * Sätter initiala attributet 'value'
   * @en Input initial value attribute
   */
  @Prop() afValue: string;
  @Watch('afValue') onAfValueChanged(value) {
    this.value = value;
  }

  /**
   * Sätter om radioknappen ska vara ikryssad.
   * @en Sets if radiobutton should be checked.
   */
  @Prop() checked: boolean;
  @Watch('checked') onCheckedChanged(checked) {
    this.afChecked = checked;
  }

  /**
   * Sätter om radioknappen ska vara ikryssad.
   * @en Sets if radiobutton should be checked.
   */
  @Prop() afChecked: boolean;
  @Watch('afChecked') onAfCheckedChanged(checked) {
    this.checked = checked;
  }

  /**
   * Sätter valideringsstatus. Kan vara 'error' eller ingenting.
   * @en Set input validation
   */
  @Prop() afValidation: FormRadiobuttonValidation;

  /**
   * Sätter attributet 'aria-labelledby'
   * @en Input aria-labelledby attribute
   */
  @Prop() afAriaLabelledby: string;

  /**
   * Sätter attributet 'aria-describedby'
   * @en Input aria-describedby attribute
   */
  @Prop() afAriaDescribedby: string;

  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  @Prop() afAutofocus: boolean;

  /**
   * Inputelementets 'onchange'-event.
   * @en The input element's 'onchange' event.
   */
  @Event() afOnChange: EventEmitter;

  /**
   * Inputelementets 'onblur'-event.
   * @en The input element's 'onblur' event.
   */
  @Event() afOnBlur: EventEmitter;

  /**
   * Inputelementets 'onfocus'-event.
   * @en The input element's 'onfocus' event.
   */
  @Event() afOnFocus: EventEmitter;

  /**
   * Inputelementets 'onfocusout'-event.
   * @en The input element's 'onfocusout' event.
   */
  @Event() afOnFocusout: EventEmitter;

  /**
   * Inputelementets 'oninput'-event.
   * @en The input element's 'oninput' event.
   */
  @Event() afOnInput: EventEmitter;

  /**
   * Sker vid inputfältets första 'oninput'
   * @en First time the input element receives an input
   */
  @Event() afOnDirty: EventEmitter;

  /**
   * Sker vid inputfältets första 'onblur'
   * @en First time the input element is blurred
   */
  @Event() afOnTouched: EventEmitter;

  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet.
   * @en When the component and slots are loaded and initialized this event will trigger.
   */
  @Event({
    bubbles: false,
    cancelable: true
  })
  afOnReady: EventEmitter;

  /**
   * Hämta en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  @Method()
  async afMGetFormControlElement() {
    return this._input;
  }

  componentDidLoad() {
    if (this.value) {
      this.afValue = this.value;
    }

    this.afOnReady.emit();
  }

  get cssModifiers() {
    return {
      'digi-form-radiobutton--error':
        this.afValidation === FormRadiobuttonValidation.ERROR,
      'digi-form-radiobutton--primary':
        this.afVariation === FormRadiobuttonVariation.PRIMARY,
      'digi-form-radiobutton--secondary':
        this.afVariation === FormRadiobuttonVariation.SECONDARY,
      [`digi-form-radiobutton--layout-${this.afLayout}`]: !!this.afLayout
    };
  }

  blurHandler(e) {
    if (!this.touched) {
      this.afOnTouched.emit(e);
      this.touched = true;
    }
    this.afOnBlur.emit(e);
  }

  changeHandler(e) {
    this.afOnChange.emit(e);
  }

  focusHandler(e) {
    this.afOnFocus.emit(e);
  }

  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }

  inputHandler(e) {
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }

    this.afOnInput.emit(e);
  }

  render() {
    return (
      <div
        class={{
          'digi-form-radiobutton': true,
          ...this.cssModifiers
        }}>
        <input
          class="digi-form-radiobutton__input"
          ref={(el) => (this._input = el as HTMLInputElement)}
          onInput={(e) => this.inputHandler(e)}
          onBlur={(e) => this.blurHandler(e)}
          onChange={(e) => this.changeHandler(e)}
          onFocus={(e) => this.focusHandler(e)}
          onFocusout={(e) => this.focusoutHandler(e)}
          aria-describedby={this.afAriaDescribedby}
          aria-labelledby={this.afAriaLabelledby}
          aria-invalid={
            this.afValidation === FormRadiobuttonValidation.ERROR
              ? 'true'
              : 'false'
          }
          required={this.afRequired}
          id={this.afId}
          name={this.afName}
          type="radio"
          checked={this.afChecked}
          value={this.afValue}
          autofocus={this.afAutofocus ? this.afAutofocus : null}
        />
        <label htmlFor={this.afId} class="digi-form-radiobutton__label">
          <span class="digi-form-radiobutton__circle"></span>
          {this.afLabel}
        </label>
      </div>
    );
  }
}
