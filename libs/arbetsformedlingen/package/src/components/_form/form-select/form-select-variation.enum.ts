export enum FormSelectVariation {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large'
}
