import { Component, Prop, h, Listen } from '@stencil/core';
import { TypographyHeadingJumboLevel } from './typography-heading-jumbo-level.enum';

/**
 * @enums TypographyHeadingJumboLevel - typography-heading-jumbo-level.enum.ts
 * @swedishName Jumborubrik
 */
@Component({
  tag: 'digi-typography-heading-jumbo',
  styleUrls: ['typography-heading-jumbo.scss'],
  scoped: true
})
export class TypographyHeadingJumbo {
  headingElement!: HTMLHeadingElement;
  /**
   * Rubrikens text
   * @en The heading text
   */
  @Prop() afText = '';

  /**
   * Sätt rubrikens vikt. 'h1' är förvalt.
   * @en Set heading level. Default is 'h1'
   */
  @Prop() afLevel: TypographyHeadingJumboLevel = TypographyHeadingJumboLevel.H1;

  /**
   * Sätt språk inuti rubrik-elementet. 'sv' (svenska) är förvalt.
   * @en Set language inside of title element. Defaults to 'sv' (swedish).
   */
  @Prop() afLang = 'sv';

  @Listen('focus')
  focusOnHeading() {
    this.headingElement.focus();
  }

  render() {
    return (
      <this.afLevel
        ref={(el) => (this.headingElement = el as HTMLHeadingElement)}
        class="digi-typography-heading-jumbo"
        lang={this.afLang}
        tabindex={-1}>
        {this.afText}
      </this.afLevel>
    );
  }
}
