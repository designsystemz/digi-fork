import { Component, h } from '@stencil/core';

/**
 * @slot default - Should be a text node
 * @swedishName Ingress
 */
@Component({
  tag: 'digi-typography-preamble',
  styleUrls: ['typography-preamble.scss'],
  scoped: true
})
export class TypographyPreamble {
  render() {
    return (
      <p class="digi-typography-preamble">
        <slot></slot>
      </p>
    );
  }
}
