import { ChartLineSeries } from './chart-line-series.interface';

export interface ChartLineData {
  data: {
    xValues: number[];
    series: ChartLineSeries[];
    xValueNames?: string[];
  };
  x: string;
  y: string;
  title: string;
  subTitle?: string;
  infoText?: string;
  meta?: {
    numberOfReferenceLines?: number;
    percentage?: boolean;
    hideXAxis?: boolean;
    valueLabels?: boolean;
    labelProperties?: {
      significantDigits?: number;
      shortHand?: boolean;
    };
  };
}
