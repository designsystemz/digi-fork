export enum BarChartVariation {
  Vertical = 'vertical',
  Horizontal = 'horizontal'
}
