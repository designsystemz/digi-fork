/* eslint-disable @typescript-eslint/no-explicit-any */

import { Component, h, Host, Prop, Element, Watch } from '@stencil/core';
import * as d3 from 'd3';
import { _t } from '../../../text';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { ChartLineData } from '../chart-line/chart-line-data.interface';
import { ChartLineSeries } from '../chart-line/chart-line-series.interface';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { BarChartVariation } from './bar-chart-variation.enum';

/**
 * @slot mySlot - Slot description, if any
 *
 * @enums Orientation - BarChartVariation.enum.ts
 *
 * @swedishName Stapeldiagram
 */
@Component({
  tag: 'digi-bar-chart',
  styleUrl: 'bar-chart.scss',
  scoped: true
})
export class barChart {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Sätter orienteringen på diagrammet, kan vara vertikalt eller horisontellt, vertikalt är förvalt.
   */
  @Prop() afVariation: BarChartVariation = BarChartVariation.Vertical;

  /**
   * En sträng med rubriknivån du vill ha i diagrammet, default är 'h3'
   */
  @Prop() afHeadingLevel = 'h3';

  /**
   * (Frivillig) Om du vill att komponenten ska ha ett unikt ID, kan du skicka in detta här, annars genereras ett automatiskt
   */
  @Prop() afId: string = randomIdGenerator('tooltip');

  /**
   * Datan som ska visas upp i stapeldiagrammet, kan vara av antingen typen ChartLineData, eller strängifierad ChartLineData
   */
  @Prop({ mutable: true }) afChartData: ChartLineData | string;

  private _chartData: ChartLineData;

  // Axis
  private _yAxis: d3.Axis<d3.AxisDomain>;
  private _yScale: any;
  private _yAxisHandle: d3.Selection<SVGGElement, unknown, null, undefined>;
  private _xScale: any;
  private _xAxis: d3.Axis<d3.NumberValue>;
  private _namedTickSizes: number[];

  // Handles to commonly used selections
  private _barSelection: any;
  private _textSelection: any;
  private _barChartContainer: d3.Selection<
    SVGGElement,
    unknown,
    null,
    undefined
  >;
  private _xAxisHandle: d3.Selection<SVGGElement, unknown, null, undefined>;
  private _rem: number;
  private _ellipsisSize: number;
  private _status: HTMLDivElement;

  // States
  private _loading = false;
  private _placeholderTicks = ['0', '1', '2', '3', '4', '5', '6', '7'];
  private _prevHeight: string;
  // Tempscale is used to get exact margins on y-axis
  private _tempScale: number[];

  @Watch('afChartData')
  afChartDataUpdate(data: ChartLineData | string) {
    if (typeof data != 'string') this._chartData = data;
    else this._chartData = JSON.parse(data);
  }

  // Handles to html components
  private _chartDiv: any;
  private _host: any;
  private _legendDiv: HTMLDivElement;
  private _titleDiv: HTMLDivElement;
  private _tableDiv: any;
  private _svg: d3.Selection<SVGSVGElement, unknown, null, undefined>;

  // Tooltip specific
  private _tooltip: any;
  private _tooltipHeading: string;
  private _tooltipData: any;

  // Dimensions and sizes
  private _maxHeight: number;
  private _pixelSize: number;
  private _dims: DOMRect = {} as DOMRect;
  private _margin = {
    bottom: 0,
    top: 6,
    left: 5,
    right: 15,
    ellipsis: false
  };
  private tickPadding = 8;
  private yTickSize = 0;
  private xTickSize = 5;
  private _maxVal: number;
  private _minVal: number;

  // Width of the largets X-tick-value
  private _tickWidth = 1;

  // Transition
  private defaultDuration = 400;

  // Text and formats
  fontSize = '0.875rem';
  private _numberFormat: any;

  // Observer for watching size changes when in line chart mode
  barChartObserver = new ResizeObserver((entries) => {
    entries.forEach(() => {
      if (this._loading) return this.loadingResize();
      this.resize();
    });
  });

  // Observer for watching size changes when in table mode
  tableObserver = new ResizeObserver((entries) => {
    entries.forEach(() => {
      d3.select(this._legendDiv).style('width', this._dims.width + 'px');
      this.resizeTable();
    });
  });

  initSvg() {
    // Fetch number formatting
    this._numberFormat = {
      maximumSignificantDigits:
        this._chartData.meta?.labelProperties?.significantDigits,
      notation: this._chartData.meta?.labelProperties?.shortHand
        ? 'compact'
        : 'standard'
    };
    d3.select(this._chartDiv).select('svg').remove();

    this._maxHeight = d3
      .select(this._host)
      .node()
      .parentNode.getBoundingClientRect().height;

    this.barChartObserver.observe(this._chartDiv);

    // Getting rem-size
    this._rem = parseFloat(getComputedStyle(document.documentElement).fontSize);
    this.setMinMax();
    this.setDims();
    this.setMargins();

    // Appending svg
    this._svg = d3
      .select(this._chartDiv)
      .append('svg')
      .attr('aria-hidden', true)
      .attr('role', 'img')
      .attr(
        'aria-label',
        this._chartData && this._chartData.title
          ? 'Stapeldiagram om ' + this._chartData.title.toLowerCase()
          : 'Stapeldiagram'
      );

    this._svg.attr('width', this._dims.width).attr('height', this._dims.height);
  }

  initToolTip() {
    // Getting handle for tooltip
    this._tooltip = d3.select('#' + this.afId);
    this._tooltip.select('.close').on('click', this.closeTooltip);
    this._tooltipHeading =
      'h' + (parseInt(this.afHeadingLevel.replace(/\D/g, '')) + 1); // increment heading level by one
    // Add heading placeholder for heading
    this._tooltip
      .selectAll('.tooltipBody')
      .append(this._tooltipHeading)
      .attr('class', 'tooltipHeading') // Adding class to try to dodge global css classes
      .style('font-size', '1rem')
      .style('margin', '0 0.75rem')
      .style('font-weight', '500');
  }

  initEmpty() {
    d3.select(this._chartDiv).select('svg').remove();

    this._maxHeight = d3
      .select(this._host)
      .node()
      .parentNode.getBoundingClientRect().height;

    this.barChartObserver.observe(this._chartDiv);

    // Getting rem-size
    this._rem = parseFloat(getComputedStyle(document.documentElement).fontSize);
    //this.setMinMax();
    this.setDims();
    this.setMargins();

    // Appending svg
    this._svg = d3
      .select(this._chartDiv)
      .append('svg')
      .attr('aria-hidden', true)
      .attr('role', 'img')
      .attr(
        'aria-label',
        this._chartData && this._chartData.title
          ? 'Stapeldiagram om ' + this._chartData.title.toLowerCase()
          : 'Stapeldiagram'
      );

    this._svg.attr('width', this._dims.width).attr('height', this._dims.height);

    // Hide unusable components
    d3.select(this._titleDiv)
      .select(this.afHeadingLevel)
      .html(`${_t.loading}...`);

    this.initYAxis(true);
    this.initXAxis(true);

    this.loadingState(true);
  }

  // Resizing chart while loading
  private loadingResize = () => {
    const newHeight = this.setDims();
    this.setMargins(true);
    this._svg.attr('width', this._dims.width).attr('height', this._dims.height);
    this._barChartContainer.attr(
      'transform',
      `translate(${this._margin.left + this.tickPadding + this.yTickSize},0)`
    );

    this._margin.right =
      this.getTextSize(this._xScale.domain()[1].toLocaleString()) / 2 +
      this.yTickSize +
      this.tickPadding;

    // If there is a new height
    if (newHeight) {
      // Update y-axis with potentially new height (might want to do a check here)
      this.afVariation === BarChartVariation.Horizontal
        ? this._yScale.range([
            this._margin.top,
            this._dims.height - this._margin.bottom
          ])
        : this._yScale.range([
            this._dims.height - this._margin.bottom,
            this._margin.top
          ]);

      this._xAxisHandle.attr(
        'transform',
        `translate(0,${this._dims.height - this._margin.bottom})`
      );
    }

    // Update x-axis with new width
    this._xScale.range([
      0,
      this._dims.width - this._margin.right - this._margin.left
    ]);
    this._barSelection
      .attr('x', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? 2
          : this._xScale(d.key)
      )
      .attr('width', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? this._xScale(d.value)
          : this._xScale.bandwidth()
      );

    this.setValueTicks();
  };

  loadingState(empty?: boolean) {
    this._loading = true;

    // close potentially open tooltip
    if (this._tooltip) this.closeTooltip();

    d3.select(this._host).select('.tableButton').style('display', 'none');

    const newHeight = this.setDims();

    this.transitionTitle();

    this._xAxisHandle.selectAll('.tick').remove();
    this._xAxisHandle.select('path').remove();

    this._svg
      .transition()
      .duration(400)
      .attr('width', this._dims.width)
      .attr('height', this._dims.height)
      .attr(
        'aria-label',
        this._chartData && this._chartData.title
          ? 'Stapeldiagram om ' + this._chartData.title.toLowerCase()
          : 'Stapeldiagram'
      );
    this.setMargins(true);

    d3.select(this._titleDiv)
      .select(this.afHeadingLevel)
      .html(`${_t.loading}...`);

    // Waiting with observing for changes until transition to regular bars is complete
    this._barChartContainer
      .transition('position')
      .duration(this.defaultDuration)
      .ease(d3.easePolyInOut)
      .attr(
        'transform',
        `translate(${this._margin.left + this.tickPadding + this.yTickSize},0)`
      );

    // If there is a new height
    if (newHeight) {
      // Update y-axis with potentially new height (might want to do a check here)
      this.afVariation === BarChartVariation.Horizontal
        ? this._yScale.range([
            this._margin.top,
            this._dims.height - this._margin.bottom
          ])
        : this._yScale.range([
            this._dims.height - this._margin.bottom,
            this._margin.top
          ]);

      this._xAxisHandle.attr(
        'transform',
        `translate(0,${this._dims.height - this._margin.bottom})`
      );
    }

    const animateBars = () => {
      this._barChartContainer.selectAll('.barGroup').remove();
      this._barSelection = this._barChartContainer
        .selectAll('barGroup')
        .data(this.reshapeData)
        .join('g')
        .attr('class', 'barGroup')
        .attr('id', (d: any) => 'barGroup' + d.key.replace(/[^A-Z0-9]+/gi, '_'))
        .append('rect')
        .attr('class', 'bar')
        .style('cursor', 'pointer');

      this._barSelection
        .attr('y', (d: any) =>
          this.afVariation === BarChartVariation.Horizontal
            ? this._yScale(d.key)
            : this._dims.height - this._margin.bottom
        )
        .attr('x', (d: any) =>
          this.afVariation === BarChartVariation.Horizontal
            ? 2
            : this._xScale(d.key)
        )
        .attr('ry', 4)
        .attr('rx', 4)
        .attr('width', (d: any) =>
          this.afVariation === BarChartVariation.Horizontal
            ? this._xScale(d.value)
            : this._xScale.bandwidth()
        )
        .attr('height', () => 0)
        //.attr('height', (d: any) => this.afVariation === BarChartVariation.Horizontal ? this._yScale.bandwidth() : (this._dims.height - this._margin.bottom) - this._yScale(d.value))
        .attr('fill', 'gray')
        .transition()
        .duration(400)
        .delay((...[, i]: any[]) => i * 150)
        .attr('y', (d: any) =>
          this.afVariation === BarChartVariation.Horizontal
            ? this._yScale(d.key)
            : this._yScale(d.value)
        )
        .attr('height', (d: any) =>
          this.afVariation === BarChartVariation.Horizontal
            ? this._yScale.bandwidth()
            : this._dims.height - this._margin.bottom - this._yScale(d.value)
        );

      this._barSelection
        .transition()
        .duration(400)
        .ease(d3.easeLinear)
        .delay((...[, i]: any[]) => i * 150 + 400)
        .attr('y', (d: any) =>
          this.afVariation === BarChartVariation.Horizontal
            ? this._yScale(d.key)
            : this._dims.height - this._margin.bottom
        )
        .attr('height', () => 0)
        .on('end', (...[, i]: any[]) => {
          if (i === this._placeholderTicks.length - 1) animateBars();
        });
    };

    const flattenBars = () => {
      this._barChartContainer
        .selectAll('rect')
        .transition()
        .duration(200)
        .ease(d3.easeLinear)
        .attr('y', (d: any) =>
          this.afVariation === BarChartVariation.Horizontal
            ? this._yScale(d.key)
            : this._dims.height - this._margin.bottom
        )
        .attr('height', () => 0)
        .on('end', (...[, i]: any[]) => {
          if (i != 0) return;
          this.setYScale(true);
          this.setValueTicks();
          this.setXScale(true);
          this.barChartObserver.observe(this._chartDiv);
          animateBars();
        });
    };
    if (empty) animateBars();
    else flattenBars();
  }

  transitionTitle() {
    const height = d3.select(this._titleDiv).style('height');
    d3.select(this._titleDiv)
      .style('height', this._prevHeight)
      .transition('adjust')
      .duration(400)
      .style('height', height)
      .on('end', () => d3.select(this._titleDiv).style('height', null));
  }

  initYAxis(empty?: boolean) {
    this.setYScale(empty);

    this._barChartContainer = this._svg
      .append('g')
      .attr(
        'transform',
        `translate(${this._margin.left + this.tickPadding + this.yTickSize},0)`
      );

    this._yAxisHandle = this._barChartContainer.append('g');

    if (this.afVariation === BarChartVariation.Horizontal) {
      this.tickSizes();
      this.setNamedTicks();
      this.setTickInteraction();
    } else this.setValueTicks();
  }

  // Adding click listener for tool tip text
  private setTickInteraction = () => {
    if (this.afVariation === BarChartVariation.Horizontal) {
      this._yAxisHandle
        .selectAll('text')
        .style('cursor', 'pointer')
        .on('click', this.openTooltip)
        .on('mouseenter', this.hover)
        .on('mouseleave', this.hoverOut);
    } else {
      this._xAxisHandle
        .selectAll('text')
        .style('cursor', 'pointer')
        .on('click', this.openTooltip)
        .on('mouseenter', this.hover)
        .on('mouseleave', this.hoverOut);
    }
  };

  // Setting Y-scale
  private setYScale = (empty?: boolean) => {
    if (this.afVariation == BarChartVariation.Horizontal) {
      this._yScale = d3
        .scaleBand(
          empty
            ? ['0', '1', '2', '3', '4', '5', '6', '7']
            : this._chartData.data.xValueNames,
          [this._margin.top, this._dims.height - this._margin.bottom]
        )
        .paddingOuter(0.1)
        .paddingInner(0.1);

      this._yAxis = d3.axisLeft(this._yScale);
    } else {
      this._yScale = d3
        .scaleLinear()
        .domain(empty ? [0, 10] : [this._minVal, this._maxVal])
        .nice(
          this._chartData.meta?.numberOfReferenceLines
            ? this._chartData.meta.numberOfReferenceLines
            : 3
        );

      this._yScale.range([
        this._dims.height - this._margin.bottom,
        this._margin.top
      ]);

      this._yAxis = d3.axisLeft(this._yScale);
    }
  };

  // Sets the largest and smallest value from series 0, Should only be used when not loading
  private setMinMax(): void {
    this._maxVal = d3.max(this._chartData.data.series[0].yValues);
    this._minVal = d3.min([0, d3.min(this._chartData.data.series[0].yValues)]);
    this._tempScale = d3
      .scaleLinear()
      .domain([this._minVal, this._maxVal])
      .nice(
        this._chartData.meta?.numberOfReferenceLines
          ? this._chartData.meta.numberOfReferenceLines
          : 3
      )
      .domain();
  }

  // Setting X-Scale
  private setXScale = (empty?: boolean) => {
    // Horizontal
    if (this.afVariation == BarChartVariation.Horizontal) {
      this._xScale = d3
        .scaleLinear()
        .domain(empty ? [0, 10] : [this._minVal, this._maxVal])
        .nice(3);

      this._margin.right =
        this.getTextSize(this._xScale.domain()[1].toLocaleString()) / 2 +
        this.yTickSize +
        this.tickPadding;

      this._xScale.range([
        0,
        this._dims.width - this._margin.right - this._margin.left
      ]);

      // Initializing actual axis
      this._xAxis = d3.axisBottom(this._xScale);

      this._tickWidth = this.getLargestText(
        this._xScale.ticks().map((tick: number) => tick.toLocaleString())
      );
    } else {
      // Vertical
      this._xScale = d3
        .scaleBand()
        .domain(
          empty ? this._placeholderTicks : this._chartData.data.xValueNames
        )
        .range([0, this._dims.width - this._margin.right - this._margin.left])
        .paddingOuter(0.1)
        .paddingInner(0.1);

      this._xAxis = d3.axisBottom(this._xScale);
    }
  };

  // Stores all tick text sizes in an indexable array
  private tickSizes = () => {
    // Initializing tick size array
    this._namedTickSizes = [];
    this._ellipsisSize = this.getTextSize('...');
    for (let i = 0; i < this._chartData.data.xValueNames.length; i++) {
      this._namedTickSizes.push(
        Math.ceil(this.getTextSize(this._chartData.data.xValueNames[i]))
      );
    }
  };

  // Setting up x-axis, getting margin.right in this function
  initXAxis(empty?: boolean) {
    this.setXScale(empty);

    this._xAxisHandle = this._barChartContainer
      .append('g')
      .attr(
        'transform',
        `translate(0,${this._dims.height - this._margin.bottom})`
      );

    if (this.afVariation === BarChartVariation.Horizontal) {
      this.setValueTicks();
      // Removing first tick, because it aligns with y-axis domain
      this._xAxisHandle.select('.tick').select('line').remove();
    } else {
      if (!empty) {
        this.tickSizes();
        this.setNamedTicks();
        this.setTickInteraction();
      }
    }
  }

  // Initializing bars, can only handle one series of bars for now
  initBars() {
    this._barSelection = this._barChartContainer
      .selectAll('barGroup')
      .data(this.reshapeData)
      .join('g')
      .attr('class', 'barGroup')
      .attr('id', (d: any) => 'barGroup' + d.key.replace(/[^A-Z0-9]+/gi, '_'))
      .append('rect')
      .attr('class', 'bar')
      .style('cursor', 'pointer');

    this._barSelection
      .attr('y', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? this._yScale(d.key)
          : this._yScale(d.value)
      )
      .attr('x', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? 2
          : this._xScale(d.key)
      )
      .attr('ry', 4)
      .attr('rx', 4)
      .attr('width', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? this._xScale(d.value)
          : this._xScale.bandwidth()
      )
      .attr('height', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? this._yScale.bandwidth()
          : this._dims.height - this._margin.bottom - this._yScale(d.value)
      )
      .attr('fill', '#00005a')
      .on('mouseenter', this.hover)
      .on('mouseleave', this.hoverOut)
      .on('click', this.openTooltip);
  }

  private updateBars = (fromLoading?: boolean) => {
    this._barChartContainer
      .selectAll('.barGroup')
      .data(this.reshapeData)
      .join(
        (enter) => {
          const tempSelection = enter
            .append('g')
            .attr('class', 'barGroup')
            .attr(
              'id',
              (d: any) => 'barGroup' + d.key.replace(/[^A-Z0-9]+/gi, '_')
            );

          tempSelection
            .append('rect')
            .attr('class', 'bar')
            .style('cursor', 'pointer')
            .attr('ry', 4)
            .attr('rx', 4)
            .attr('y', (d: any) =>
              this.afVariation === BarChartVariation.Horizontal
                ? this._yScale(d.key)
                : this._dims.height - this._margin.bottom
            )
            .attr('x', (d: any) =>
              this.afVariation === BarChartVariation.Horizontal
                ? 2
                : this._xScale(d.key)
            )
            .attr('width', () =>
              this.afVariation === BarChartVariation.Horizontal
                ? 0
                : this._xScale.bandwidth()
            )
            .attr('height', () =>
              this.afVariation === BarChartVariation.Horizontal
                ? this._yScale.bandwidth()
                : 0
            )
            .on('mouseenter', this.hover)
            .on('mouseleave', this.hoverOut)
            .on('click', this.openTooltip)
            .transition()
            .duration(this.defaultDuration)
            .ease(d3.easePolyInOut)
            .attr('width', (d: any) =>
              this.afVariation === BarChartVariation.Horizontal
                ? this._xScale(d.value)
                : this._xScale.bandwidth()
            )
            .attr('y', (d: any) =>
              this.afVariation === BarChartVariation.Horizontal
                ? this._yScale(d.key)
                : this._yScale(d.value)
            )
            .attr('height', (d: any) =>
              this.afVariation === BarChartVariation.Horizontal
                ? this._yScale.bandwidth()
                : this._dims.height -
                  this._margin.bottom -
                  this._yScale(d.value)
            )
            .attr('fill', '#00005a');

          if (!this._chartData.meta?.valueLabels) return tempSelection;

          tempSelection
            .append('text')
            .text((d: any) => d.value.toLocaleString())
            .attr('class', 'barText')
            .attr('font-size', this.fontSize)
            .attr('font-weight', '600')
            .attr('text-anchor', 'end')
            .style('pointer-events', 'none')
            .attr('y', (d: any) =>
              this.afVariation === BarChartVariation.Horizontal
                ? this._yScale(d.key) + this._yScale.bandwidth() / 2
                : this._dims.height - this._margin.bottom
            )
            .attr('x', (d: any) =>
              this.afVariation === BarChartVariation.Horizontal
                ? 2
                : this._xScale(d.key) + this._xScale.bandwidth() / 2
            )
            .transition()
            .duration(this.defaultDuration)
            .ease(d3.easePolyInOut)
            .attr('font-size', this.fontSize)
            .attr('font-weight', '600')
            .attr('x', (d: any) =>
              this.afVariation === BarChartVariation.Horizontal
                ? this._xScale(d.value)
                : this._xScale(d.key) + this._xScale.bandwidth() / 2
            )
            .attr('y', (d: any) =>
              this.afVariation === BarChartVariation.Horizontal
                ? this._yScale(d.key) + this._yScale.bandwidth() / 2
                : this._yScale(d.value)
            )
            .attr('text-anchor', (d: any) => {
              if (this.afVariation === BarChartVariation.Vertical)
                return 'middle';
              return this._xScale(d.value) < d.len + this.tickPadding * 2
                ? 'start'
                : 'end';
            })
            .attr('dx', (d: any) => {
              if (this.afVariation === BarChartVariation.Horizontal)
                return this._xScale(d.value) < d.len + this.tickPadding * 2
                  ? this.tickPadding
                  : -this.tickPadding;

              return 0;
            })
            .attr('fill', (d: any) => {
              if (this.afVariation === BarChartVariation.Vertical)
                return 'black';
              return this._xScale(d.value) < d.len + this.tickPadding * 2
                ? 'black'
                : 'white';
            })
            .attr('dy', () =>
              this.afVariation == BarChartVariation.Horizontal
                ? '0.35em'
                : '-0.25em'
            )
            .style('pointer-events', 'none')
            .attrTween('text', (d: any, i: number, nodelist: any) => {
              const oldnum = parseFloat(
                nodelist[i].textContent.replace(/\s/g, '')
              )
                ? parseFloat(nodelist[i].textContent.replace(/\s/g, ''))
                : 0;
              const interpolatedValue: any = d3.interpolate(oldnum, d.value);
              return (t: any) =>
                (nodelist[i].textContent = parseInt(
                  interpolatedValue(t)
                ).toLocaleString(undefined, this._numberFormat));
            });

          return tempSelection;
        },
        (update) => {
          // Update ids to bars
          update.attr(
            'id',
            (d: any) => 'barGroup' + d.key.replace(/[^A-Z0-9]+/gi, '_')
          );

          // Mouse interaction to updated loading bars
          if (fromLoading) {
            update
              .select('rect')
              .on('mouseenter', this.hover)
              .on('mouseleave', this.hoverOut)
              .on('click', this.openTooltip)
              .attr('x', (d: any) =>
                this.afVariation === BarChartVariation.Horizontal
                  ? 2
                  : this._xScale(d.key)
              )
              .attr('y', (d: any) =>
                this.afVariation === BarChartVariation.Horizontal
                  ? this._yScale(d.key)
                  : this._dims.height - this._margin.bottom
              )
              .attr('width', (d: any) =>
                this.afVariation === BarChartVariation.Horizontal
                  ? this._xScale(d.value)
                  : this._xScale.bandwidth()
              )
              .attr('height', () =>
                this.afVariation === BarChartVariation.Horizontal
                  ? this._yScale.bandwidth()
                  : 0
              )
              .attr('fill', '#00005a')
              .transition()
              .duration(this.defaultDuration)
              .ease(d3.easePolyInOut)
              .attr('y', (d: any) =>
                this.afVariation === BarChartVariation.Horizontal
                  ? this._yScale(d.key)
                  : this._yScale(d.value)
              )
              .attr('height', (d: any) =>
                this.afVariation === BarChartVariation.Horizontal
                  ? this._yScale.bandwidth()
                  : this._dims.height -
                    this._margin.bottom -
                    this._yScale(d.value)
              )
              .on('end', () => this.barChartObserver.observe(this._chartDiv)); // Reset barchartObserver
          } else {
            update
              .select('rect')
              .transition()
              .duration(this.defaultDuration)
              .ease(d3.easePolyInOut)
              .attr('y', (d: any) =>
                this.afVariation === BarChartVariation.Horizontal
                  ? this._yScale(d.key)
                  : this._yScale(d.value)
              )
              .attr('x', (d: any) =>
                this.afVariation === BarChartVariation.Horizontal
                  ? 2
                  : this._xScale(d.key)
              )
              .attr('width', (d: any) =>
                this.afVariation === BarChartVariation.Horizontal
                  ? this._xScale(d.value)
                  : this._xScale.bandwidth()
              )
              .attr('height', (d: any) =>
                this.afVariation === BarChartVariation.Horizontal
                  ? this._yScale.bandwidth()
                  : this._dims.height -
                    this._margin.bottom -
                    this._yScale(d.value)
              )
              .attr('fill', '#00005a');
          }

          return update
            .select('text')
            .style('display', () =>
              this._chartData.meta?.valueLabels ? null : 'none'
            )
            .transition()
            .duration(this.defaultDuration)
            .ease(d3.easePolyInOut)
            .attr('font-size', this.fontSize)
            .attr('font-weight', '600')
            .attr('y', (d: any) =>
              this.afVariation === BarChartVariation.Horizontal
                ? this._yScale(d.key) + this._yScale.bandwidth() / 2
                : this._yScale(d.value)
            )
            .attr('x', (d: any) =>
              this.afVariation === BarChartVariation.Horizontal
                ? this._xScale(d.value)
                : this._xScale(d.key) + this._xScale.bandwidth() / 2
            )
            .attr('text-anchor', (d: any) => {
              if (this.afVariation === BarChartVariation.Vertical)
                return 'middle';
              return this._xScale(d.value) < d.len + this.tickPadding * 2
                ? 'start'
                : 'end';
            })
            .attr('dx', (d: any) => {
              if (this.afVariation === BarChartVariation.Horizontal)
                return this._xScale(d.value) < d.len + this.tickPadding * 2
                  ? this.tickPadding
                  : -this.tickPadding;

              return 0;
            })
            .attr('fill', (d: any) => {
              if (this.afVariation === BarChartVariation.Vertical)
                return 'black';
              return this._xScale(d.value) < d.len + this.tickPadding * 2
                ? 'black'
                : 'white';
            })
            .attr('dy', () =>
              this.afVariation == BarChartVariation.Horizontal
                ? '0.35em'
                : '-0.25em'
            )
            .style('pointer-events', 'none')
            .attrTween('text', (d: any, i: number, nodelist: any) => {
              const oldnum = parseFloat(
                nodelist[i].textContent.replace(/\s/g, '')
              )
                ? parseFloat(nodelist[i].textContent.replace(/\s/g, ''))
                : 0;
              const interpolatedValue: any = d3.interpolate(oldnum, d.value);
              return (t: any) =>
                (nodelist[i].textContent = parseInt(
                  interpolatedValue(t)
                ).toLocaleString(undefined, this._numberFormat));
            });
        },
        (exit) => exit.remove()
      );
    this._barSelection = this._barChartContainer.selectAll('rect');
    this._textSelection = this._barChartContainer.selectAll('.barText');
  };

  initBarText() {
    this._textSelection = this._barChartContainer
      .selectAll('.barGroup')
      .append('text');
    this._textSelection
      .text((d: any) => d.value.toLocaleString(undefined, this._numberFormat))
      .attr('class', 'barText')
      .attr('font-size', this.fontSize)
      .attr('font-weight', '600')
      .attr('text-anchor', 'end')
      .attr('y', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? this._yScale(d.key) + this._yScale.bandwidth() / 2
          : this._yScale(d.value)
      )
      .attr('x', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? this._xScale(d)
          : this._xScale(d.key) + this._xScale.bandwidth()
      )
      .attr('dy', () =>
        this.afVariation == BarChartVariation.Horizontal ? '0.35em' : '-0.25em'
      )
      .style('pointer-events', 'none')
      .style('display', () =>
        this._chartData.meta?.valueLabels ? null : 'none'
      );
    this.adjustBarText();
  }

  initChain() {
    this.initSvg();
    this.initToolTip();
    this.initYAxis();
    this.initXAxis();
    this.initBars();
    this.initBarText();
  }

  async updateChain() {
    // Fetch number formatting
    this._numberFormat = {
      maximumSignificantDigits:
        this._chartData.meta?.labelProperties?.significantDigits,
      notation: this._chartData.meta?.labelProperties?.shortHand
        ? 'compact'
        : 'standard'
    };

    if (this._tooltip) this.closeTooltip();

    // Check whether we are in table mode or not
    if (this._tableDiv.style['display'] == 'block') {
      if (!this._chartData.title) {
        d3.select(this._titleDiv)
          .select(this.afHeadingLevel)
          .html(`${_t.loading}...`);
      } else {
        this.setTableContent();
        this.setTableDimensions();
      }
      this.transitionTitle();
    } else {
      // displaying legend again
      if (this._loading) {
        d3.select(this._host).select('.tableButton').style('display', null);
        d3.select(this._legendDiv).style('display', null);
        this.initToolTip();
      }
      // Disconnect resize observer while adjusting things
      this.barChartObserver.disconnect();
      this.setMinMax();
      const newHeight = this.setDims();

      this.transitionTitle();

      this._svg
        .transition()
        .duration(400)
        .attr('width', this._dims.width)
        .attr('height', this._dims.height)
        .attr(
          'aria-label',
          this._chartData && this._chartData.title
            ? 'Stapeldiagram om ' + this._chartData.title.toLowerCase()
            : 'Stapeldiagram'
        );
      this.setMargins();

      // Waiting with observing for changes until transition to regular bars is complete
      this._barChartContainer
        .transition('position')
        .duration(this.defaultDuration)
        .ease(d3.easePolyInOut)
        .attr(
          'transform',
          `translate(${this._margin.left + this.tickPadding + this.yTickSize},0)`
        );
      if (!this._loading)
        this._barChartContainer.on('end', () =>
          this.barChartObserver.observe(this._chartDiv)
        );

      // If there is a new height
      if (newHeight) {
        // Update y-axis with potentially new height (might want to do a check here)
        this.afVariation === BarChartVariation.Horizontal
          ? this._yScale.range([
              this._margin.top,
              this._dims.height - this._margin.bottom
            ])
          : this._yScale.range([
              this._dims.height - this._margin.bottom,
              this._margin.top
            ]);

        this._xAxisHandle.attr(
          'transform',
          `translate(0,${this._dims.height - this._margin.bottom})`
        );
      }
      this.tickSizes();
      this.setYScale();
      this.setXScale();
      this.setTickInteraction();
      this._tickWidth = this.getLargestText(
        this.afVariation == BarChartVariation.Horizontal
          ? this._xScale.ticks().map((tick: number) => tick.toLocaleString())
          : this._yScale.ticks().map((tick: number) => tick.toLocaleString())
      );
      this.setValueTicks(true);

      if (this._loading) {
        d3.select(this._legendDiv)
          .style('opacity', 0)
          .transition()
          .duration(400)
          .ease(d3.easePolyInOut)
          .style('opacity', 1);

        this._margin.right =
          this.getTextSize(this._xScale.domain()[1].toLocaleString()) / 2 +
          this.yTickSize +
          this.tickPadding;

        // Update x-axis with new width
        this._xScale.range([
          0,
          this._dims.width - this._margin.right - this._margin.left
        ]);

        this.setNamedTicks(true);

        // Stop animation
        this._barSelection.selectAll('.barGroup').on('end', null);
        this._loading = false;

        this._barSelection
          .transition()
          .duration(this.defaultDuration)
          .attr('y', (d: any) =>
            this.afVariation === BarChartVariation.Horizontal
              ? this._yScale(d.key)
              : this._dims.height - this._margin.bottom
          )
          .attr('height', () => 0)
          .on('end', (...[, i]: any[]) =>
            i === this._placeholderTicks.length - 1
              ? this.updateBars(true)
              : null
          );
        this.setTickInteraction();
      } else {
        this.setNamedTicks(true);
        // Removing first tick, because it aligns with y-axis domain
        this.updateBars();
      }
    }
  }

  // Setting position of text according to bar size
  private adjustBarText = () => {
    if (!this._chartData.meta?.valueLabels) return;

    this._textSelection
      .attr('y', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? this._yScale(d.key) + this._yScale.bandwidth() / 2
          : this._yScale(d.value)
      )
      .attr('x', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? this._xScale(d.value)
          : this._xScale(d.key) + this._xScale.bandwidth() / 2
      )
      .attr('dx', (d: any) => {
        if (this.afVariation === BarChartVariation.Horizontal)
          return this._xScale(d.value) < d.len + this.tickPadding * 2
            ? this.tickPadding
            : -this.tickPadding;

        return 0;
      })
      .attr('fill', (d: any) => {
        if (this.afVariation === BarChartVariation.Vertical) return 'black';
        return this._xScale(d.value) < d.len + this.tickPadding * 2
          ? 'black'
          : 'white';
      })
      .attr('text-anchor', (d: any) => {
        if (this.afVariation === BarChartVariation.Vertical) return 'middle';
        return this._xScale(d.value) < d.len + this.tickPadding * 2
          ? 'start'
          : 'end';
      });
  };

  private reshapeData = (): any[] => {
    const toReturn = [];
    if (this._loading) {
      this._placeholderTicks.forEach((key: string) => {
        toReturn.push({
          key: key,
          value: Math.floor(Math.random() * (9 - 2 + 1) + 2),
          len: '1'
        });
      });
      return toReturn;
    }
    this._chartData.data.xValueNames.forEach((key: string, index: number) => {
      toReturn.push({
        key: key,
        value: this._chartData.data.series[0].yValues[index],
        len: this.getTextSize(
          this._chartData.data.series[0].yValues[index].toLocaleString()
        )
      });
    });
    return toReturn;
  };

  private adjustBarSize = () => {
    this._barSelection
      .attr('y', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? this._yScale(d.key)
          : this._yScale(d.value)
      )
      .attr('x', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? 2
          : this._xScale(d.key)
      )
      .attr('width', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? this._xScale(d.value)
          : this._xScale.bandwidth()
      )
      .attr('height', (d: any) =>
        this.afVariation === BarChartVariation.Horizontal
          ? this._yScale.bandwidth()
          : this._dims.height - this._margin.bottom - this._yScale(d.value)
      );
  };

  // Resizing bar chart
  resize() {
    const newHeight = this.setDims();
    this.setMargins();
    this._svg.attr('width', this._dims.width).attr('height', this._dims.height);
    this._barChartContainer.attr(
      'transform',
      `translate(${this._margin.left + this.tickPadding + this.yTickSize},0)`
    );

    this._margin.right =
      this.getTextSize(this._xScale.domain()[1].toLocaleString()) / 2 +
      this.yTickSize +
      this.tickPadding;

    // If there is a new height
    if (newHeight) {
      // Update y-axis with potentially new height (might want to do a check here)
      this.afVariation === BarChartVariation.Horizontal
        ? this._yScale.range([
            this._margin.top,
            this._dims.height - this._margin.bottom
          ])
        : this._yScale.range([
            this._dims.height - this._margin.bottom,
            this._margin.top
          ]);

      this._xAxisHandle.attr(
        'transform',
        `translate(0,${this._dims.height - this._margin.bottom})`
      );
    }

    // Update x-axis with new width
    this._xScale.range([
      0,
      this._dims.width - this._margin.right - this._margin.left
    ]);
    this.setNamedTicks();
    this.setValueTicks();

    this.adjustBarSize();
    this.adjustBarText();

    if (this._tooltipData) this.positionTooltip(this._tooltipData);
  }

  // Resize table
  private resizeTable = () => {
    this.setDims();
    this.setTableDimensions();
  };

  // Returns the longest text from an array of strings
  private getLargestText(array: string[]): number {
    let width = 0;
    array.forEach((elem: string) => {
      const temp = this.getTextSize(elem);
      if (temp > width) {
        width = temp;
      }
    });
    return width;
  }

  // Setting dimension variable
  private setDims = (onlyHeight = false): boolean => {
    // Get title dimensions
    const tempTitle = d3.select(this._titleDiv).node().getBoundingClientRect(),
      tempLegend = d3.select(this._legendDiv).node().getBoundingClientRect();
    const prevHeight = this._dims.height;

    if (!onlyHeight)
      this._dims = d3.select(this._chartDiv).node().getBoundingClientRect();
    this._dims.height =
      this._maxHeight - tempLegend.height - tempTitle.height - 1.75 * this._rem;
    return prevHeight != this._dims.height;
  };

  // Setting margins given text size
  private setMargins = (fromLoading?: boolean) => {
    this._pixelSize =
      Number(
        window
          .getComputedStyle(document.body)
          .getPropertyValue('font-size')
          .match(/\d+/)[0]
      ) * Number(this.fontSize.replace(/[^\d.]|\.(?=.*\.)/g, ''));
    // Getting the longest word, assuming it will also be the widest in terms of pixels
    if (this.afVariation === BarChartVariation.Horizontal) {
      this._margin.left = this.getLargestText(this._chartData.data.xValueNames);
    } else
      this._margin.left = this.getLargestText(
        fromLoading
          ? ['0', '10']
          : [
              this._minVal ? this._tempScale[0].toLocaleString() : '0',
              this._maxVal ? this._tempScale[1].toLocaleString() : '10'
            ]
      );

    if (this._margin.left > this._dims.width / 4) {
      this._margin.left = Math.floor(this._dims.width / 4);
      this._margin.ellipsis = true;
    } else this._margin.ellipsis = false;

    this._margin.bottom = this._pixelSize + this.tickPadding + this.xTickSize;
  };

  // Adjusting y-ticks
  private setNamedTicks = (transition = false) => {
    if (this.afVariation == BarChartVariation.Horizontal) {
      if (!transition) {
        this._yAxisHandle
          .style('font-size', this.fontSize)
          .style('font-family', 'Open Sans')
          .call(
            this._yAxis
              .tickPadding(this.tickPadding)
              .tickSize(this.yTickSize)
              .tickSizeOuter(0)
              .tickFormat((d: string, i: number) => {
                if (!this._margin.ellipsis) return d.toLocaleString();

                // Adding ellipsis to text
                const size =
                  this._namedTickSizes[i] + this.tickPadding + this.yTickSize;
                const mSize = this._margin.left;
                if (size < mSize) return d.toLocaleString();

                const newSize = this.getTextSize(
                  d.slice(
                    0,
                    Math.max(
                      Math.floor(
                        (mSize / (size + this._ellipsisSize)) * d.length
                      ),
                      3
                    )
                  ) + '...'
                );
                let adjustment = 0;
                if (newSize > mSize) {
                  adjustment = Math.ceil((newSize - mSize) / (size / d.length));
                }

                // Slicing the text up, saving atleast 3 characters
                return (
                  d.slice(
                    0,
                    Math.max(
                      Math.floor(
                        (mSize / (size + this._ellipsisSize)) * d.length
                      ) - adjustment,
                      3
                    )
                  ) + '...'
                );
              })
          );
      } else {
        this._yAxisHandle
          .style('font-size', this.fontSize)
          .style('font-family', 'Open Sans')
          .transition()
          .duration(this.defaultDuration)
          .call(
            this._yAxis
              .tickPadding(this.tickPadding)
              .tickSize(this.yTickSize)
              .tickSizeOuter(0)
              .tickFormat((d: string, i: number) => {
                if (!this._margin.ellipsis) return d;

                // Adding ellipsis to text
                const size =
                  this._namedTickSizes[i] + this.tickPadding + this.yTickSize;
                const mSize = this._margin.left;
                if (size < mSize) return d;

                const newSize = this.getTextSize(
                  d.slice(
                    0,
                    Math.max(
                      Math.floor(
                        (mSize / (size + this._ellipsisSize)) * d.length
                      ),
                      3
                    )
                  ) + '...'
                );
                let adjustment = 0;
                if (newSize > mSize) {
                  adjustment = Math.ceil((newSize - mSize) / (size / d.length));
                }

                // Slicing the text up, saving atleast 3 characters
                return (
                  d.slice(
                    0,
                    Math.max(
                      Math.floor(
                        (mSize / (size + this._ellipsisSize)) * d.length
                      ) - adjustment,
                      3
                    )
                  ) + '...'
                );
              })
          );
      }
    } else {
      //Calculating every n:th text to show
      let nTh = Math.round(
        this._chartData.data.xValues.length /
          ((this._dims.width - this._margin.left - this._margin.right) / 90 - 1)
      );
      if (Math.floor(this._chartData.data.xValues.length / nTh) < 4) {
        nTh = Math.round(this._chartData.data.xValues.length / 4);
      }
      const allowedWidth =
        (this._dims.width - this._margin.left - this._margin.right) /
        Math.floor(this._chartData.data.xValues.length / nTh + 1);

      if (!transition) {
        this._xAxisHandle
          .style('font-size', this.fontSize)
          .style('font-family', 'Open Sans')
          .call(
            this._xAxis
              .tickValues(
                this._xScale.domain().filter((...[, i]: any[]) => {
                  if (i == 0) return true;
                  else {
                    return i % nTh == 0;
                  }
                })
              )
              .tickPadding(this.tickPadding)
              .tickSize(this.xTickSize)
              .tickSizeOuter(0)
              .tickFormat((d: any, i: number) => {
                const index = i * nTh;
                if (this._namedTickSizes[index] < allowedWidth) return d;
                return (
                  d.slice(
                    0,
                    Math.max(
                      Math.floor(
                        (allowedWidth /
                          (this._namedTickSizes[index] +
                            this._ellipsisSize +
                            5)) *
                          d.length
                      ),
                      3
                    )
                  ) + '...'
                );
              })
          );
      } else {
        this._xAxisHandle
          .style('font-size', this.fontSize)
          .style('font-family', 'Open Sans')
          .transition()
          .duration(this.defaultDuration)
          .call(
            this._xAxis
              .tickValues(
                this._xScale.domain().filter((...[, i]: any[]) => {
                  if (i == 0) return true;
                  else {
                    return i % nTh == 0;
                  }
                })
              )
              .tickPadding(this.tickPadding)
              .tickSize(this.xTickSize)
              .tickSizeOuter(0)
              .tickFormat((d: any, i: number) => {
                const index = i * nTh;
                if (this._namedTickSizes[index] < allowedWidth) return d;
                return (
                  d.slice(
                    0,
                    Math.max(
                      Math.floor(
                        (allowedWidth /
                          (this._namedTickSizes[index] +
                            this._ellipsisSize +
                            5)) *
                          d.length
                      ),
                      3
                    )
                  ) + '...'
                );
              })
          );
      }
    }
  };

  // Positioning the ticks of the value bearing axis
  private setValueTicks = (transition = false) => {
    if (this._chartData.meta && this._chartData.meta.hideXAxis) {
      this.afVariation === BarChartVariation.Horizontal
        ? this._xAxisHandle.attr('display', 'none')
        : this._yAxisHandle.attr('display', 'none');
      return;
    }

    let toCall: any, tickSize: number, handle: any;
    if (this.afVariation === BarChartVariation.Horizontal) {
      this._xAxisHandle.attr('display', null);
      toCall = this._xAxis;
      tickSize = -(this._dims.height - this._margin.top - this._margin.bottom);
      handle = this._xAxisHandle;
    } else {
      this._yAxisHandle.attr('display', null);
      toCall = this._yAxis;
      tickSize = -(this._dims.width - this._margin.left - this._margin.right);
      handle = this._yAxisHandle;
    }

    // Computing the amount of ticks to show
    const ticks = Math.min(
      Math.floor(
        (this._dims.width - this._margin.left - this._margin.right) /
          (this._tickWidth + this.xTickSize)
      ) - 2,
      this.afVariation == BarChartVariation.Horizontal
        ? this._xScale.ticks().length
        : this._yScale.ticks().length
    );

    if (!transition) {
      handle
        .style('font-size', this.fontSize)
        .style('font-family', 'Open Sans')
        .call(
          toCall
            .ticks(
              this.afVariation === BarChartVariation.Horizontal
                ? Math.max(2, Math.min(ticks, 9))
                : 5
            ) // Clamping value between 2 and 10
            .tickPadding(this.tickPadding)
            .tickSize(tickSize)
            .tickSizeOuter(0)
            .tickFormat((d: any) => d.toLocaleString())
        );
    } else {
      handle
        .style('font-size', this.fontSize)
        .style('font-family', 'Open Sans')
        .transition('newScale')
        .duration(this.defaultDuration)
        .call(
          toCall
            .ticks(
              this.afVariation === BarChartVariation.Horizontal
                ? Math.max(2, Math.min(ticks, 9))
                : 5
            ) // Clamping value between 2 and 10
            .tickPadding(this.tickPadding)
            .tickSize(tickSize)
            .tickSizeOuter(0)
            .tickFormat((d: any) => d.toLocaleString())
        );
    }

    handle.selectAll('line').attr('stroke', 'lightgray');

    if (this.afVariation === BarChartVariation.Vertical)
      this._yAxisHandle.select('.domain').attr('display', 'none');
  };

  // Highlight bar
  private hover = (event: any) => {
    d3.select(event.target)
      .transition('hover')
      .duration(20)
      .attr(
        'fill',
        event.target.tagName === 'rect'
          ? '#4C4C8B'
          : 'var(--digi--color--text--link-hover)'
      );
  };

  // Removing highlight from bar
  private hoverOut = (event: any) => {
    d3.select(event.target)
      .transition('hover')
      .duration(this.defaultDuration)
      .attr('fill', event.target.tagName === 'rect' ? '#00005a' : '#333333');
  };

  private openTooltip = (...[, d]: any) => {
    // If type is string, the user has clicked on a y-axis tick
    if (typeof d === 'string') {
      this._tooltipData = this._barChartContainer
        .select(`#barGroup${d.replace(/[^A-Z0-9]+/gi, '_')}`)
        .datum();
    } else this._tooltipData = d;

    const tooltipBody = this._tooltip.select('.tooltipBody');
    tooltipBody
      .select(this._tooltipHeading)
      .style('margin', '0 0.75rem 0 0 ')
      .style('font-weight', 500)
      .html(this._tooltipData.key);

    tooltipBody
      .selectAll('div')
      .data([this._tooltipData])
      .join('div')
      .style('display', 'flex')
      .style('flex-direction', 'row')
      .style('flex-wrap', 'nowrap')
      .style('white-space', 'nowrap')
      .style('justify-content', 'space-between')
      .html((d: any) => d.value.toLocaleString());

    this._tooltip.style('display', 'flex');
    this.positionTooltip(this._tooltipData);
  };

  private closeTooltip = () => {
    this._tooltip.style('display', 'none');
  };

  // Positions tooltip in horizontal mode
  private positionTooltip = (data: any) => {
    const width: any = this._tooltip.node().getBoundingClientRect().width;
    const height: any = this._tooltip.node().getBoundingClientRect().height + 8;
    const diff = this._xScale(data.value) / 2 - width / 2;
    if (this.afVariation === BarChartVariation.Horizontal) {
      this._tooltip
        .style(
          'transform',
          `translate(${this._margin.left + this.tickPadding + Math.max(diff, -4)}px, ${this._yScale(data.key) + this._yScale.bandwidth() + this._yScale.paddingInner() * this._yScale.bandwidth()}px )`
        )
        .style('max-width', this._xScale.range()[1] + 'px');
      this._tooltip
        .select('digi-icon-caret-up')
        .style('position', 'absolute')
        .style('--digi--icon--color', 'white')
        .style('top', '-11px')
        .style('left', '0')
        .style(
          'transform',
          `translate(${diff > 0 ? width / 2 - 10 : this._xScale(data.value) / 2}px,0)`
        );
    } else {
      this._tooltip
        .style(
          'transform',
          `translate(${this._margin.left + this.tickPadding + this._xScale(data.key) + this._xScale.bandwidth() / 2 - width / 2}px, ${this._yScale(data.value) - height}px )`
        )
        .style('max-width', this._xScale.range()[1] + 'px');
      this._tooltip
        .select('digi-icon-caret-down')
        .style('position', 'absolute')
        .style('--digi--icon--color', 'white')
        .style('left', '0')
        .style('bottom', '-11px')
        .style('transform', `translate(${width / 2 - 6}px,0)`);
    }
  };

  // Once texts has fully loaded, we can recompute some sizes
  private recomputeTextSize = () => {
    if (this.afVariation == BarChartVariation.Horizontal)
      this._tickWidth = this.getLargestText(
        this._xScale.ticks().map((tick: number) => tick.toLocaleString())
      );
    this.tickSizes();
    this._barChartContainer.selectAll('.barGroup').data(this.reshapeData);
    if (this._chartData.meta?.valueLabels) this._textSelection.remove();
    this.initBarText();
  };

  componentWillLoad() {
    this.afChartDataUpdate(this.afChartData);
  }

  componentDidLoad() {
    if (Object.keys(this._chartData).length === 0) {
      this._loading = true;
      return this.initEmpty();
    }

    this.initChain();

    document.fonts.ready.then(() => {
      this.recomputeTextSize();
      this.setNamedTicks();
      this._barChartContainer.attr(
        'transform',
        `translate(${this._margin.left + this.tickPadding + this.yTickSize},0)`
      );
    });
  }

  disconnectedCallback() {
    this.barChartObserver.disconnect();
  }

  componentWillUpdate() {
    this._prevHeight = d3.select(this._titleDiv).style('height');
  }

  componentDidUpdate() {
    if (
      this._tableDiv.style['display'] != 'block' &&
      Object.keys(this._chartData).length === 0
    ) {
      // Disconnect old barchartobserver while doing resizing
      this.barChartObserver.disconnect();
      return this.loadingState();
    }
    this.updateChain();
  }

  // Not currently used
  /*get cssModifiers() {
		return {
			'digi-bar-chart--vertical': this.afVariation == 'vertical',
			'digi-bar-chart--horizontal': this.afVariation == 'horizontal'
		};
	}*/

  // Returns pixelwidth of text given the current fontSize (costly function using .each)
  private getTextSize(text: string): number {
    const textWidth: number[] = [];
    const svg = d3.select(this._chartDiv).append('svg');
    svg
      .selectAll('dummyText')
      .data([text])
      .join('text')
      .attr('font-size', this.fontSize) // <- using 1rem here whereas default is 0.875rem
      .text((d: any) => d)
      .each((...[, i, nodelist]: any[]) => {
        const thisWidth = nodelist[i].getComputedTextLength();
        textWidth.push(thisWidth);
        nodelist[i].remove();
      });

    svg.remove().exit();

    return textWidth[0];
  }

  // Removes resize observer for chart and inits table
  private activateTable = (event: any) => {
    this.closeTooltip();
    this.barChartObserver.disconnect();
    this.tableObserver.observe(this._chartDiv);

    this._status.innerHTML = '<p>Visas som tabell</p>';

    d3.select(this._host).select('.tableButton').style('display', 'none');
    d3.select(this._host).select('.chartButton').style('display', null);
    event.target.nextSibling.lastChild.focus();
    const duration = 150;
    this._svg
      .attr('opacity', 1)
      .transition('fadeout')
      .duration(duration)
      .attr('opacity', 0)
      .on('end', () => {
        this._svg.remove();
        this._legendDiv.style['display'] = 'none';
        this._tableDiv.style['display'] = 'block';
        this.setTableContent();
        this.setTableDimensions();
        d3.select(this._tableDiv)
          .style('opacity', 0)
          .transition('fadein')
          .duration(duration)
          .style('opacity', 1);
      });
    d3.select(this._legendDiv)
      .style('opacity', 1)
      .transition('fadeout')
      .duration(duration)
      .style('opacity', 0);
  };

  // Setting table content from this._chartData
  private setTableContent(): void {
    this._tableDiv.innerHTML = `<digi-table af-size="small" af-variation="primary"> <table><caption>${this._chartData.title}</caption><thead><tr>${this.headings()}</tr></thead><tbody>${this.tableBody()}</tbody></table></digi-table>`;
  }

  private setTableDimensions(): void {
    const table = d3.select(this._tableDiv);
    table
      // The last value is for compensating the padding set on the table element, changing padding in css, will need change in these two lines
      .style('height', this._dims.height + 'px');
    table.style('width', this._dims.width - this._rem * 0.65 + 'px');
  }

  // Extracting all headings from data and returning a string with <th>-elements with headings
  private headings(): string {
    let headingsString = `<th scope="col">${this._chartData.x}</th>`;
    this._chartData.data.series.forEach((elem: ChartLineSeries) => {
      headingsString = headingsString.concat(
        `<th scope="col">${elem.title}</th>`
      );
    });
    return headingsString;
  }

  // Building the table body
  private tableBody(): string {
    let body = '';
    const loopable = this._chartData.data.xValueNames
      ? this._chartData.data.xValueNames
      : this._chartData.data.xValues;

    loopable.forEach((elem: any, index: number) => {
      let cells = '';
      this._chartData.data.series.forEach((serie: ChartLineSeries) => {
        cells = cells.concat(
          `<td>${serie.yValues[index] != null ? serie.yValues[index].toLocaleString() : '-'}</td>`
        );
      });
      body = body.concat(`<tr><th scope='row'>${elem}${cells}</th></tr>`);
    });

    return body;
  }

  // Removes observer and calls init functions for line chart
  private reInitChart = (event: any) => {
    this.tableObserver.disconnect();
    this._status.innerHTML = '<p>Visas som diagram</p>';

    d3.select(this._host).select('.tableButton').style('display', null);
    d3.select(this._host).select('.chartButton').style('display', 'none');
    event.target.previousSibling.lastChild.focus();
    const table = d3.select(this._tableDiv);
    table
      .style('opacity', 1)
      .transition()
      .duration(150)
      .style('opacity', 0)
      .on('end', () => {
        table.style('display', 'none');
        this.initChain();
        this._svg
          .attr('opacity', 0)
          .transition()
          .duration(150)
          .attr('opacity', 1);
      });
  };

  render() {
    // Setting header
    let heading = '';
    if (!this._chartData.subTitle)
      heading = `<${this.afHeadingLevel}>${this._chartData.title}</${this.afHeadingLevel}>`;
    else {
      if (!this._chartData.infoText)
        heading = `<${this.afHeadingLevel}>${this._chartData.title}</${this.afHeadingLevel}><p>${this._chartData.subTitle}</p>`;
      else
        heading = `<${this.afHeadingLevel}>${this._chartData.title}</${this.afHeadingLevel}><p>${this._chartData.subTitle}
				<span style="display:block;margin-top:0.25rem;font-size:0.875rem;color:var(--digi--global--color--neutral--grayscale--darkest-3)">${this._chartData.infoText}</span></p>`;
    }

    return (
      <Host ref={(el: any) => (this._host = el)}>
        <div class="topWrapper" ref={(el) => (this._titleDiv = el)}>
          <digi-typography
            class="chartTitle"
            innerHTML={heading}></digi-typography>

          {/* Toggle table/chart button */}
          <digi-button
            class="buttonWrapper tableButton"
            af-size="medium"
            af-variation="function"
            af-full-width="false"
            onAfOnClick={this.activateTable}
            afAriaLabel={`Visa tabell ${this._chartData.title ? this._chartData.title.toLowerCase() : ''}`}>
            <digi-icon-table slot="icon" />
            Visa tabell
          </digi-button>
          <digi-button
            style={{ display: 'none' }}
            class="buttonWrapper chartButton"
            af-size="medium"
            af-variation="function"
            af-full-width="false"
            onAfOnClick={this.reInitChart}
            afAriaLabel={`Visa diagram ${this._chartData.title ? this._chartData.title.toLowerCase() : ''}`}>
            <digi-icon-chart slot="icon" />
            Visa diagram
          </digi-button>
          <div
            class="scChartStatus"
            role="status"
            ref={(el) => (this._status = el)}></div>
        </div>
        <div class="chartTooltip" id={this.afId}>
          <div class="tooltipBody"></div>
          <digi-icon-x class="close"></digi-icon-x>
          {this.afVariation === BarChartVariation.Horizontal ? (
            <digi-icon-caret-up></digi-icon-caret-up>
          ) : (
            <digi-icon-caret-down></digi-icon-caret-down>
          )}
        </div>
        <div ref={(el) => (this._chartDiv = el)}> </div>
        <div class="legend" ref={(el) => (this._legendDiv = el)}></div>
        <div class="table" ref={(el) => (this._tableDiv = el)}>
          {' '}
        </div>
      </Host>
    );
  }
}
