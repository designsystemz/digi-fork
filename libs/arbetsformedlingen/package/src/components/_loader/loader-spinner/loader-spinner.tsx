import { Component, h, Host, Prop, Element } from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { LoaderSpinnerSize } from './loader-spinner-size.enum';

/**
 * @enums LoaderSpinnerSize - loader-spinner-size.enum.ts
 *
 * @swedishName Spinner
 */
@Component({
  tag: 'digi-loader-spinner',
  styleUrl: 'loader-spinner.scss',
  scoped: true
})
export class LoaderSpinner {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Sätter spinnerns storlek. 'medium' är förvalt.
   * @en Set spinner icon size. Defaults to medium size.
   */
  @Prop() afSize: LoaderSpinnerSize = LoaderSpinnerSize.MEDIUM;

  /**
   * Sätter spinnerns text.
   * @en Set spinners text.
   */
  @Prop() afText: string;

  get cssModifiers() {
    return {
      'digi-loader-spinner--small': this.afSize == 'small',
      'digi-loader-spinner--medium': this.afSize == 'medium',
      'digi-loader-spinner--large': this.afSize == 'large'
    };
  }

  render() {
    return (
      <Host>
        <div
          aria-label={this.afText}
          aria-live="assertive"
          class={{
            'digi-loader-spinner': true,
            ...this.cssModifiers
          }}>
          <div class="digi-loader-spinner__container" aria-hidden="true">
            <digi-icon
              class="digi-loader-spinner__spinner"
              afName={`spinner`}></digi-icon>
          </div>
          {this.afText && (
            <div class="digi-loader-spinner__label">{this.afText}</div>
          )}
        </div>
      </Host>
    );
  }
}
