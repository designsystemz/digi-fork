import {
  Component,
  h,
  Prop,
  State,
  Element,
  Method,
  Event,
  EventEmitter
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { ProgressListVariation } from './progress-list-variation.enum';
import { ProgressListHeadingLevel } from './progress-list-heading-level.enum';
import { ProgressListType } from './progress-list-type.enum';
import { ProgressListStatus } from './progress-list-status.enum';
import { IconName } from '../../../global/icon/icon';
import { logger } from '../../../global/utils/logger';

/**
 * @enums ProgressListHeadingLevel - progress-list-heading-level.enum
 * @enums ProgressListVariation - progress-list-variation.enum
 * @enums ProgressListStatus - progress-list-status.enum
 * @enums ProgressListType - progress-list-type.enum
 * @swedishName Förloppslista
 */
@Component({
  tag: 'digi-progress-list',
  styleUrl: 'progress-list.scss',
  scoped: true
})
export class ProgressList {
  @Element() hostElement: HTMLStencilElement;
  /**
   * Rubrikens text
   * @en The heading of text
   */
  @Prop() afHeading: string;

  /**
   * Sätter attributet 'aria-label'
   * @en Set aria-label attribute
   */
  @Prop() afAriaLabel: string;

  /**
   * Sätter rubriknivå på varje förloppsteg. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  @Prop() afHeadingLevel: ProgressListHeadingLevel =
    ProgressListHeadingLevel.H2;

  /**
   * Sätter variant. Kan vara 'Primary', 'Secondary' eller 'Tertiary'
   * @en Set variation. Can be 'Primary', 'Secondary' or 'Tertiary'
   */
  @Prop() afVariation: ProgressListVariation = ProgressListVariation.PRIMARY;

  /**
   * Sätter typ. Kan vara 'Circle' eller 'Icon'
   * @en Set type. Can be 'Circle' or 'Icon'
   */
  @Prop() afType: ProgressListType = ProgressListType.CIRCLE;

  /**
   * Sätter läge på Rubriken om den ska vara expanderad eller ej. Förvalt är false.
   * @en Sets the state of the Header item to expanded or not. Default is false.
   */
  @Prop() afExpandable = false;

  /**
   * Sätter texten som visas på knappen som expanderar komponenten, t.ex: 'Visa mer'.
   * Förvalt är endast ikon och detta fält är tomt.
   * @en Sets the text displayed on the button that expands the component, eg. 'Show more'.
   * Default is icon only and this field is empty.
   */
  @Prop() afExpandText = '';

  /**
   * Sätter texten som visas på knappen som minimerar komponenten, t.ex: 'Visa mindre'.
   * Förvalt är endast ikon och detta fält är tomt.
   * @en Sets the text displayed on the button that expands the component, eg: 'Show less'.
   * Default is icon only and this field is empty.
   */
  @Prop() afCollapseText = '';

  /**
   * Sätter ikon som visas på knappen som expanderar komponenten. Förvalt är 'chevron-down'.
   * @en Sets the icon displayed on the button that expands the component. Default is 'chevron-down'.
   */
  @Prop() afExpandIcon: IconName = 'chevron-down';

  /**
   * Sätter ikon som visas på knappen som kollapsar komponenten. Förvalt är 'chevron-up'.
   * @en Sets the icon displayed on the button that collapses the component. Default is 'chevron-up'.
   */
  @Prop() afCollapseIcon: IconName = 'chevron-up';

  /**
   * Sätter nuvarande steg
   * @en Set current steps
   */
  @Prop() afCurrentStep = 1;
  @State() steps = [];

  @Method()
  async afMNext() {
    this.afCurrentStep += 1;
  }

  @Method()
  async afMPrevious() {
    this.afCurrentStep -= 1;
  }

  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet.
   * @en When the component and slots are loaded and initialized this event will trigger.
   */
  @Event({
    bubbles: false,
    cancelable: true
  })
  afOnReady: EventEmitter;

  componentWillLoad() {
    this.getSteps();
    this.setStepAndTypeProps();
  }

  componentWillUpdate() {
    this.getSteps();
    this.setStepAndTypeProps();
  }

  componentDidLoad() {
    this.afOnReady.emit();
  }

  getSteps() {
    const steps = this.hostElement.querySelectorAll('digi-progress-list-item');
    if (!steps || steps.length <= 0) {
      logger.warn(`The slot contains no children elements.`, this.hostElement);
      return;
    }

    this.steps = [...Array.from(steps)].map((step) => {
      return {
        outerHTML: step.outerHTML,
        ref: step
      };
    });
  }

  setStepAndTypeProps() {
    if (this.steps && this.afCurrentStep < 1) {
      logger.warn(
        `Current step is set to ${this.afCurrentStep} which is not allowed.`,
        this.hostElement
      );
      this.afCurrentStep = 1;
      return;
    }

    if (this.steps && this.afCurrentStep > this.steps.length + 1) {
      logger.warn(
        `Current step is set to ${this.afCurrentStep} which is more than the amount of available steps (${this.steps.length}).`,
        this.hostElement
      );
      this.afCurrentStep = this.steps.length + 1;
      return;
    }

    this.steps &&
      this.steps.forEach((step, index) => {
        step.ref.afVariation = this.afVariation;

        try {
          const topLevelN =
            Number.parseInt(this.afHeadingLevel.split('h')[1]) + 1;
          const topLevel = Math.max(Math.min(topLevelN, 6), 1);
          const afHeadingLevel = 'h' + topLevel;

          const isLast = index == this.steps.length - 1;
          step.ref.afIsLast = isLast;

          let status: ProgressListStatus;
          if (index < this.afCurrentStep - 1) {
            status = ProgressListStatus.DONE;
          } else if (index == this.afCurrentStep - 1) {
            status = ProgressListStatus.CURRENT;
          } else {
            status = ProgressListStatus.UPCOMING;
          }
          step.ref.afStepStatus = status;

          step.ref.afType = this.afType;
          step.ref.afHeadingLevel = afHeadingLevel;

          // If expandable, check if expanded
          step.ref.afExpandable = this.afExpandable;
          step.ref.afExpanded = status === ProgressListStatus.CURRENT;
          step.ref.afExpandText = this.afExpandText;
          step.ref.afCollapseText = this.afCollapseText;

          step.ref.afExpandIcon = this.afExpandIcon;
          step.ref.afCollapseIcon = this.afCollapseIcon;
        } catch (e) {
          logger.warn(`Error in setStepAndTypeProps, `, e);
        }
      });
  }

  renderHeading() {
    if (this.afHeading?.trim().length > 1) {
      return (
        <div>
          <this.afHeadingLevel
            class="digi-progress-list__header"
            aria-label={this.afAriaLabel}>
            {this.afHeading}
          </this.afHeadingLevel>
        </div>
      );
    }
    return null;
  }

  render() {
    return (
      <div class="digi-progress-list">
        <digi-typography>
          {this.renderHeading()}
          <ol class="digi-progress-list__content">
            <slot></slot>
          </ol>
        </digi-typography>
      </div>
    );
  }
}
