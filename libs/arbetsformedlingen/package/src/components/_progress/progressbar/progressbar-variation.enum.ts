export enum ProgressbarVariation {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
