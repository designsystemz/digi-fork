import {
  Component,
  Element,
  Event,
  EventEmitter,
  Fragment,
  h,
  Host,
  Prop
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { _t } from '../../../text';
import { IconName } from '../../../global/icon/icon';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ProgressListItemHeadingLevel } from './progress-list-item-heading-level.enum';
import { ProgressListItemVariation } from './progress-list-item-variation.enum';
import { ProgressListItemStatus } from './progress-list-item-status.enum';
import { ProgressListItemType } from './progress-list-item-type.enum';

/**
 * @enums ProgressListItemStatus - progress-list-item-status.enum.ts
 * @enums ProgressListItemHeadingLevel - progress-list-item-heading-level.enum.ts
 * @enums ProgressbarVariation - progressbar-variation.enum.ts
 * @swedishName Förloppslista - enskilt steg
 */
@Component({
  tag: 'digi-progress-list-item',
  styleUrl: 'progress-list-item.scss',
  scoped: true
})
export class ProgressListItem {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Rubrikens text
   * @en The heading of text
   */
  @Prop() afHeading!: string;

  /**
   * Sätter rubriknivå. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  @Prop() afHeadingLevel: ProgressListItemHeadingLevel =
    ProgressListItemHeadingLevel.H2;

  /**
   * Sätter attributet 'afStepStatus'. Förvalt är 'upcoming'.
   * @en Set afStepStatus attribute. Defaults to 'upcoming'.
   */
  @Prop() afStepStatus: ProgressListItemStatus = ProgressListItemStatus.DONE;

  /**
   * Sätter attributet 'afStepStatusAriaLabel'. Blir till aria-label på status-ikonen/cirkeln. Default är "status klar",
   * "status aktuell" eller "status kommande" beroende på stegets status.
   * @en Set afStepStatusAriaLabel attribute. Defaults are "status klar", "status aktuell", "status kommande"'.
   */
  @Prop() afStepStatusAriaLabel: string;

  /**
   * Sätter variant. Kan vara 'Primary', 'Secondary' eller 'Tertiary'
   * @en Set variation. Can be 'Primary', 'Secondary' or 'Tertiary'
   */
  @Prop() afVariation: ProgressListItemVariation =
    ProgressListItemVariation.PRIMARY;

  /**
   * Sätter attributet 'aria-label'
   * @en Set aria-label attribute
   */
  @Prop() afAriaLabel: string;

  /**
   * Sätter typ. Kan vara 'Circle' eller 'Icon'
   * @en Set type. Can be 'Circle' or 'Icon'
   */
  @Prop() afType: ProgressListItemType = ProgressListItemType.CIRCLE;

  /**
   * Sätter läge på Rubriken om den tillåts vara expanderad eller ej. Förvalt är false.
   * @en Sets the state of the Header item to expanded or not. Default is false.
   */
  @Prop() afExpandable = false;

  /**
   * Om Rubriken tillåts att vara expanderarbar så sätts läge på Rubriken om den är expanderad eller ej. Förvalt är false.
   * @en If the Header is allowed to be expandable, the mode is set to the Header whether it is expanded or not. Default is false.
   */
  @Prop() afExpanded = false;

  /**
   * Sätter texten som visas på knappen som expanderar komponenten. Förvalt är endast ikon och detta fält tomt.
   * @en Sets text on the expand button. Default is icon only and this field is empty.
   */
  @Prop() afExpandText = '';

  /**
   * Sätter texten som visas på knappen som kollapsar komponenten. Förvalt är endast ikon och detta fält tomt.
   * @en Sets the text on the button that collapses the component. Default is icon only and this field is empty.
   */
  @Prop() afCollapseText = '';

  /**
   * Sätter ikon som visas på knappen som expanderar komponenten. Förvalt är 'chevron-down'.
   * @en Sets the icon displayed on the button that expands the component. Default is 'chevron-down'.
   */
  @Prop() afExpandIcon: IconName = 'chevron-down';

  /**
   * Sätter ikon som visas på knappen som kollapsar komponenten. Förvalt är 'chevron-up'.
   * @en Sets the icon displayed on the button that collapses the component. Default is 'chevron-up'.
   */
  @Prop() afCollapseIcon: IconName = 'chevron-up';

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-progress-list-item');

  /**
   * Kollar om det är sista steget
   * @en checks if it is the last step
   */
  @Prop() afIsLast = false;

  /**
   * Buttonelementets 'onclick'-event.
   * @en The button element's 'onclick' event.
   */
  @Event() afOnClick: EventEmitter<MouseEvent>;

  renderExpandAndCollapseText(): HTMLStencilElement {
    if (this.afExpandText.trim().length > 0) {
      return (
        <span class="digi-progress-list-item__header--toggle--text">
          {this.afExpanded ? this.afCollapseText : this.afExpandText}
        </span>
      );
    }
    return null;
  }

  renderHeading(): HTMLStencilElement {
    if (this.afExpandable) {
      return (
        <button
          id={`${this.afId}-btn`}
          type="button"
          class="digi-progress-list-item__header--button"
          onClick={(e) => {
            e.preventDefault();
            this.afExpanded = !this.afExpanded;
            this.afOnClick.emit(e);
          }}
          aria-expanded={this.afExpanded ? 'true' : 'false'}
          aria-controls={`${this.afId}-content`}>
          <span class="visually-hidden">
            {this.afAriaLabel ? this.afAriaLabel : this.afHeading}
            {this.getAriaLabelForStep()}
          </span>
          <span
            aria-hidden="true"
            class={{
              'digi-progress-list-item__header--heading--title': true,
              'digi-progress-list-item__header--heading--title--expandable':
                true
            }}>
            {this.afHeading}
          </span>
          <div
            class={{
              'digi-progress-list-item__header--toggle': true,
              'digi-progress-list-item__header--toggle--expanded':
                this.afExpanded
            }}>
            <digi-icon afName={this.afExpandIcon}></digi-icon>
            {this.renderExpandAndCollapseText()}
          </div>
        </button>
      );
    }
    return (
      <Fragment>
        <span class="visually-hidden">
          {this.afAriaLabel ? this.afAriaLabel : this.afHeading}
          {this.getAriaLabelForStep()}
        </span>
        <span
          class="digi-progress-list-item__header--heading--title"
          aria-hidden="true">
          {this.afHeading}
        </span>
      </Fragment>
    );
  }

  renderStatusIcon() {
    if (this.afType === ProgressListItemType.ICON) {
      if (this.afStepStatus === ProgressListItemStatus.CURRENT) {
        return (
          <digi-icon-arrow-right class="digi-progress-list-item__indicator--icon--digi"></digi-icon-arrow-right>
        );
      } else if (this.afStepStatus == ProgressListItemStatus.DONE) {
        return (
          <digi-icon-check class="digi-progress-list-item__indicator--icon--digi"></digi-icon-check>
        );
      }
    }
    return null;
  }

  getAriaLabelForStep() {
    const preText = ', ';
    if (this.afStepStatusAriaLabel && this.afStepStatusAriaLabel.length > 0) {
      return preText + this.afStepStatusAriaLabel;
    }

    switch (this.afStepStatus) {
      case ProgressListItemStatus.DONE:
        return preText + _t.progressList.progressListItemDone;
      case ProgressListItemStatus.CURRENT:
        return preText + _t.progressList.progressListItemCurrent;
      case ProgressListItemStatus.UPCOMING:
        return preText + _t.progressList.progressListItemUpcoming;
      default:
        return '';
    }
  }

  render() {
    const ariaCurrent =
      this.afStepStatus == ProgressListItemStatus.CURRENT ? 'step' : 'false';

    return (
      <Host role="listitem" aria-current={ariaCurrent}>
        <div
          class={{
            'digi-progress-list-item': true,
            'digi-progress-list-item--expanded':
              this.afExpandable && this.afExpanded,
            [`digi-progress-list-item--${this.afStepStatus}`]: true,
            'digi-progress-list-item--last': this.afIsLast,
            'digi-progress-list-item--primary':
              this.afVariation == ProgressListItemVariation.PRIMARY,
            'digi-progress-list-item--secondary':
              this.afVariation == ProgressListItemVariation.SECONDARY,
            'digi-progress-list-item--tertiary':
              this.afVariation == ProgressListItemVariation.TERTIARY
          }}>
          <span class="digi-progress-list-item__indicator">
            <span
              class={{
                'digi-progress-list-item__indicator--circle':
                  this.afType == ProgressListItemType.CIRCLE,
                'digi-progress-list-item__indicator--icon':
                  this.afType == ProgressListItemType.ICON
              }}>
              {this.renderStatusIcon()}
            </span>
            <span class="digi-progress-list-item__indicator--line"></span>
          </span>
          <div
            class={{
              'digi-progress-list-item__wrapper': true,
              'digi-progress-list-item__wrapper--last': this.afIsLast
            }}>
            <div class="digi-progress-list-item__header">
              <this.afHeadingLevel
                class="digi-progress-list-item__header--heading"
                id={`${this.afId}--heading`}>
                {this.renderHeading()}
              </this.afHeadingLevel>
            </div>
            <div
              class={{
                'digi-progress-list-item__content': true,
                'digi-progress-list-item__content--expandable':
                  this.afExpandable,
                'digi-progress-list-item__content--hidden':
                  this.afExpandable && !this.afExpanded,
                'digi-progress-list-item__content--visible':
                  !this.afExpandable || (this.afExpandable && this.afExpanded)
              }}
              id={`${this.afId}-content`}>
              <div>
                <slot></slot>
              </div>
            </div>
          </div>
        </div>
      </Host>
    );
  }
}
