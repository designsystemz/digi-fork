import { Component, Element, Event, EventEmitter, h } from '@stencil/core';

/**
 * @componentType utility
 * @slot default - Kan innehålla vad som helst
 * @swedishName Resize Observer
 */
@Component({
  tag: 'digi-util-resize-observer',
  styleUrls: ['util-resize-observer.scss'],
  scoped: true
})
export class UtilResizeObserver {
  @Element() hostElement: HTMLElement;

  private _observer: ResizeObserver = null;

  /**
   * När komponenten ändrar storlek
   * @en When the component changes size
   */
  @Event() afOnChange: EventEmitter<ResizeObserverEntry>;

  componentWillLoad() {
    this.initObserver();
  }

  disconnectedCallback() {
    this.removeObserver();
  }

  initObserver() {
    this._observer = new ResizeObserver(([entry]) => {
      this.afOnChange.emit(entry);
    });

    this._observer.observe(this.hostElement);
  }

  removeObserver() {
    if (this._observer) this._observer.disconnect();
  }

  changeHandler(e) {
    this.afOnChange.emit(e);
  }

  render() {
    return <slot></slot>;
  }
}
