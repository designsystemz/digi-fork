import {
  Component,
  Element,
  Event,
  EventEmitter,
  Listen,
  Prop,
  h
} from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';

/**
 * @componentType utility
 * @slot default - Kan innehålla vad som helst
 * @swedishName Detect Focus Outside
 *
 */
@Component({
  tag: 'digi-util-detect-focus-outside',
  scoped: true
})
export class UtilDetectFocusOutside {
  @Element() $el: HTMLElement;

  /**
   * Sätter en unik identifierare som behövs för att kontrollera fokuseventen. Måste vara prefixad med 'data-'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set unique identifier needed for focus control. Defaults to random string. Should be prefixed with 'data-'
   */
  @Prop() afDataIdentifier: string = randomIdGenerator(
    'data-digi-util-detect-focus-outside'
  );

  /**
   * Vid fokus utanför komponenten
   * @en When focus detected outside of component
   */
  @Event() afOnFocusOutside: EventEmitter<FocusEvent>;

  /**
   * Vid fokus inuti komponenten
   * @en When focus detected inside of component
   */
  @Event() afOnFocusInside: EventEmitter<FocusEvent>;

  componentWillLoad() {
    this.$el.setAttribute(this.afDataIdentifier, '');
  }

  @Listen('focusin', { target: 'document' })
  focusinHandler(e: FocusEvent): void {
    const target = e.target as HTMLElement;
    const selector = `[${this.afDataIdentifier}]`;
    if (detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    } else {
      this.afOnFocusInside.emit(e);
    }
  }

  render() {
    return <slot></slot>;
  }
}
