import { Component, Element, h, Host, Prop, State, Watch } from '@stencil/core';
import { ErrorPageStatusCodes } from './notification-error-page-httpstatus.enum';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import {
  NotificationIllustrationName,
  notificationIllustrations
} from './illustrations/illustrations';
import { UtilBreakpointObserverBreakpoints } from '../../../enums';

/**
 * @slot default - Html-text, använd för att skriva över komponentens default brödtext
 * @slot search - Skicka in en komponent eller kod som har stöd för att skicka vidare besökaren till en söksida
 * @slot links - Ska vara en lista (<ul> eller <ol>) med listobjekt med länkar (<li><a href="/">Start</a></li>)
 *
 * @enums ErrorPageStatusCodes - notification-error-page-httpstatus.enum.ts
 *
 * @swedishName Felsida
 */
@Component({
  tag: 'digi-notification-error-page',
  styleUrl: 'notification-error-page.scss',
  scoped: true
})
export class NotificationErrorPage {
  @Element() hostElement: HTMLStencilElement;

  @State() hasSearch: boolean;
  @State() hasLinks: boolean;
  @State() hasBodyText: boolean;
  @State() currentIllustration;
  @State() isMobile: boolean;

  setIllustration(name: NotificationIllustrationName) {
    const {
      NotificationIllustrationComponent: NotificationIllustrationComponent
    } = notificationIllustrations[name];
    this.currentIllustration = NotificationIllustrationComponent;
  }

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-notification-error-page');

  /**
   * Http status kod som supportas av komponenten
   * @en Http status code that are supported by our component
   */
  @Prop() afHttpStatusCode: ErrorPageStatusCodes;

  /**
   * Använd för egen rubrik
   * @en Use custom heading
   */
  @Prop({ mutable: true }) afCustomHeading: string;

  setHasSearch() {
    const searchSlot = !!this.hostElement.querySelector('[slot="search"]');
    if (searchSlot) {
      this.hasSearch = searchSlot;
    }
  }

  setHasLinks() {
    const linksSlot = !!this.hostElement.querySelector('[slot="links"]');
    if (linksSlot) {
      this.hasLinks = linksSlot;
    }
  }

  setHasBodyText() {
    const bodyTextSlot = !!this.hostElement.querySelector('[slot="bodytext"]');
    this.hasBodyText = bodyTextSlot;
  }

  componentWillLoad() {
    this.setHasSearch();
    this.setHasLinks();
    this.setHasBodyText();
    this.getImage();
  }

  componentWillUpdate() {
    this.setHasSearch();
    this.setHasLinks();
    this.setHasBodyText();
    this.getImage();
  }

  @Watch('afCustomHeading')
  private getText(): { heading: string; body: string } {
    const customHeading =
      this.afCustomHeading && this.afCustomHeading !== ''
        ? this.afCustomHeading
        : undefined;
    switch (this.afHttpStatusCode) {
      case ErrorPageStatusCodes.FORBIDDEN:
        return {
          heading: customHeading ?? 'Sidan är låst',
          body: '<p>Du behöver vara inloggad för att kunna se den här sidan. Kontrollera även att du har aktiverat VPN.</p>'
        };
      case ErrorPageStatusCodes.GATEWAY_TIMEOUT:
        return {
          heading: customHeading ?? 'Sidan kan inte visas just nu',
          body: '<p>Prova igen om en stund. Om felet upprepas kan du göra en felanmälan.</p>'
        };
      case ErrorPageStatusCodes.GONE:
        return {
          heading: customHeading ?? 'Sidan har tagits bort',
          body: '<p>Du kan söka på webbplatsen eller använda länkarna för att komma vidare.</p>'
        };
      case ErrorPageStatusCodes.INTERNAL_SERVER_ERRROR:
        return {
          heading: customHeading ?? 'Sidan kan inte visas just nu',
          body: '<p>Prova igen om en stund. Om felet upprepas kan du göra en felanmälan.</p>'
        };
      case ErrorPageStatusCodes.NOT_FOUND:
        return {
          heading: customHeading ?? 'Sidan kan inte hittas',
          body: '<p>Det kan bero på att länken är felaktig eller att sidan inte finns kvar. Du kan prova att ladda om sidan, söka på webbplatsen eller använda länkarna för att komma vidare.</p>'
        };
      case ErrorPageStatusCodes.UNAUTHORIZED:
        return {
          heading: customHeading ?? 'Sidan kan inte visas',
          body: '<p>Du behöver vara inloggad för att kunna se den här sidan. Har du hamnat fel kan du söka på webbplatsen.</p>'
        };
      case ErrorPageStatusCodes.SERVICE_UNAVAILABLE:
        return {
          heading:
            customHeading ??
            '[Namn på tjänst] är tillfälligt stängd på grund av underhållsarbete',
          body: '<p>Arbetet planeras att vara klart [datum, månad, år (exempelvis 1 oktober 2024)], [tid (ex kl.09.00)]. Använd länkarna nedan för att komma vidare.</p>'
        };
      default:
        return {
          heading: '',
          body: ''
        };
    }
  }

  private getImage(): void {
    switch (this.afHttpStatusCode) {
      case ErrorPageStatusCodes.UNAUTHORIZED:
        return this.setIllustration('ill-401');
      case ErrorPageStatusCodes.FORBIDDEN:
        return this.setIllustration('ill-403');
      case ErrorPageStatusCodes.NOT_FOUND:
        return this.setIllustration('ill-404');
      case ErrorPageStatusCodes.GONE:
        return this.setIllustration('ill-410');
      case ErrorPageStatusCodes.INTERNAL_SERVER_ERRROR:
        return this.setIllustration('ill-500');
      case ErrorPageStatusCodes.SERVICE_UNAVAILABLE:
        return this.setIllustration('ill-503');
      case ErrorPageStatusCodes.GATEWAY_TIMEOUT:
        return this.setIllustration('ill-504');
    }
  }

  render() {
    return (
      <Host>
        <digi-layout-container
          class={{
            'digi-notification-error-page': true,
            'digi-notification-error-page--mobile': this.isMobile
          }}
          id={this.afId}>
          <digi-typography>
            <digi-util-breakpoint-observer
              onAfOnChange={(e) => {
                const val = e.detail.value;
                this.isMobile =
                  val === UtilBreakpointObserverBreakpoints.SMALL ||
                  val === UtilBreakpointObserverBreakpoints.MEDIUM;
              }}></digi-util-breakpoint-observer>
            <div class="digi-notification-error-page__grid">
              <div class="digi-notification-error-page__picture">
                {this.currentIllustration ? (
                  <this.currentIllustration></this.currentIllustration>
                ) : null}
              </div>
              <div class="digi-notification-error-page__content">
                <h1
                  class="digi-notification-error-page__content__heading"
                  tabIndex={-1}>
                  {this.getText().heading}
                </h1>
                {this.hasBodyText ? (
                  <div class="digi-notification-error-page__content__bodytext">
                    <slot name="bodytext"></slot>
                  </div>
                ) : (
                  <div class="digi-notification-error-page__content__bodytext">
                    <div innerHTML={this.getText().body}></div>
                  </div>
                )}
                {this.hasSearch && (
                  <div class="digi-notification-error-page__content__search">
                    <slot name="search"></slot>
                  </div>
                )}
                {this.hasLinks && (
                  <div class="digi-notification-error-page__content__links">
                    <slot name="links"></slot>
                  </div>
                )}
              </div>
            </div>
          </digi-typography>
        </digi-layout-container>
      </Host>
    );
  }
}
