import {
  Component,
  h,
  Prop,
  Watch,
  Element,
  Event,
  EventEmitter
} from '@stencil/core';
import { ExpandableFaqItemHeadingLevel } from './expandable-faq-item-heading-level.enum';
import { ExpandableFaqItemVariation } from './expandable-faq-item-variation.enum';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import type { HTMLStencilElement } from '@stencil/core/internal';

/**
 * @enums ExpandableFaqItemHeadingLevel - expandable-faq-item-heading-level.enum.ts
 * @enums ExpandableFaqItemVariation - expandable-faq-item-variation.enum.ts
 * @swedishName FAQ - Enskild fråga
 */

@Component({
  tag: 'digi-expandable-faq-item',
  styleUrls: ['expandable-faq-item.scss'],
  scoped: true
})
export class ExpandableFaqItem {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Rubrikens vanliga frågor
   * @en The heading of FAQ text
   */
  @Prop() afHeading: string;

  /**
   * Sätter rubriknivå. 'h3' är förvalt.
   * @en Set heading level. Default is 'h3'
   */
  @Prop() afHeadingLevel: ExpandableFaqItemHeadingLevel =
    ExpandableFaqItemHeadingLevel.H3;

  /**
   * Sätter läge på FAQ-frågan om den ska vara expanderad eller ej.
   * @en Sets the state of the FAQ item to expanded or not.
   */
  @Prop() afExpanded = false;

  /**
   * Sätter variant. Kan vara 'Primary' eller 'Secondary'
   * @en Set variation. Can be 'Primary' or 'Secondary'
   */
  @Prop() afVariation: ExpandableFaqItemVariation =
    ExpandableFaqItemVariation.PRIMARY;

  /**
   * Ställer in ifall komponenten ska ha en animation vid utfällning. Animation är påslagen som standard.
   * @en Sets the state of the animation for the accordion's expanded area. Animation is on as default.
   */
  @Prop() afAnimation = true;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-expandable-faq-item');

  /**
   * Buttonelementets 'onclick'-event.
   * @en The button element's 'onclick' event.
   */
  @Event() afOnClick: EventEmitter<MouseEvent>;

  get cssModifiers() {
    return {
      'digi-expandable-faq-item--expanded': this.afExpanded,
      'digi-expandable-faq-item--primary':
        this.afVariation === ExpandableFaqItemVariation.PRIMARY,
      'digi-expandable-faq-item--secondary':
        this.afVariation === ExpandableFaqItemVariation.SECONDARY,
      'digi-expandable-faq-item--tertiary':
        this.afVariation === ExpandableFaqItemVariation.TERTIARY
    };
  }
  /**
   * Hämtar en höjden till innehållet i vanliga frågor och svar och animera innehållet.
   * @en Gets a height to the content for FAQ and animate the content.
   */

  componentDidLoad() {
    const content = this.hostElement.children[0].querySelector(
      `#${this.afId}-content`
    ) as HTMLElement;
    const sectionHeight = (content.firstElementChild as HTMLElement)
      .offsetHeight;
    if (this.afExpanded) {
      content.setAttribute('style', 'height: ' + sectionHeight + 'px');
    }

    if (!this.afExpanded) {
      content.setAttribute('style', 'visibility: hidden;');
    }

    content.addEventListener('transitionend', () => {
      if (!this.afExpanded) {
        content.setAttribute(
          'style',
          'visibility: hidden;  height: ' + 0 + 'px'
        );
      }
    });
  }

  @Watch('afExpanded')
  animateContentHeight() {
    const content = this.hostElement.children[0].querySelector(
      `#${this.afId}-content`
    ) as HTMLElement;
    const sectionHeight = (content.firstElementChild as HTMLElement)
      .offsetHeight;
    if (this.afExpanded) {
      content.setAttribute(
        'style',
        'visibility: visible; height:' + sectionHeight + 'px;'
      );
    } else {
      content.setAttribute('style', 'height: ' + 0 + 'px');
    }
  }

  render() {
    return (
      <article
        aria-expanded={this.afExpanded ? 'true' : 'false'}
        class={{
          'digi-expandable-faq-item': true,
          ...this.cssModifiers
        }}>
        <header class="digi-expandable-faq-item__header">
          <this.afHeadingLevel>
            <button
              type="button"
              class="digi-expandable-faq-item__button"
              onClick={(e) => {
                e.preventDefault();
                this.afExpanded = !this.afExpanded;
                this.afOnClick.emit(e);
              }}
              aria-pressed={this.afExpanded ? 'true' : 'false'}
              aria-expanded={this.afExpanded ? 'true' : 'false'}
              aria-controls={`${this.afId}-content`}>
              <digi-icon
                class={{
                  'digi-expandable-faq-item__header--toggle-icon': true,
                  'digi-expandable-faq-item__header--toggle-icon--expanded':
                    this.afExpanded
                }}
                afName={`chevron-down`}
              />
              {this.afHeading}
            </button>
          </this.afHeadingLevel>
        </header>
        <section
          class={{
            'digi-expandable-faq-item__content': true,
            'digi-expandable-faq-item__content--animation': this.afAnimation,
            'digi-expandable-faq-item__content--hidden': !this.afExpanded
          }}
          id={`${this.afId}-content`}>
          <div>
            <digi-typography>
              <slot></slot>
            </digi-typography>
          </div>
        </section>
      </article>
    );
  }
}
