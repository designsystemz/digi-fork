export enum LinkVariation {
  SMALL = 'small',
  LARGE = 'large'
}
