/* eslint-disable @typescript-eslint/no-explicit-any */

import {
  Component,
  Event,
  EventEmitter,
  Element,
  Prop,
  h,
  Host,
  Method,
  State,
  Watch
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { LinkVariation } from './link-variation.enum';

/**
 * @slot default - Ska vara en textnod
 * @slot icon - Ska vara en ikon (eller en ikonlik) komponent
 *
 * @enums LinkVariation - link-variation.enum.ts
 * @swedishName Länk
 */
@Component({
  tag: 'digi-link',
  styleUrls: ['link.scss'],
  scoped: true
})
export class Link {
  @Element() hostElement: HTMLStencilElement;

  private _link: any;

  @State() hasIcon: boolean;
  @State() relation: string;

  /**
   * Sätter attributet 'href'.
   * @en Set `href` attribute.
   */
  @Prop() afHref!: string;

  /**
   * Sätter attributet 'aria-describedby'.
   * @en Sets `aria-describedby` attribute.
   */
  @Prop() afDescribedby: string;

  /**
   * Sätter attributet 'aria-label'.
   * @en Sets `aria-label` attribute.
   */
  @Prop() afAriaLabel: string;

  /**
   * Sätter variant. Kan vara 'small' eller 'large'.
   * @en Sets the variation of the link.
   */
  @Prop() afVariation: `${LinkVariation}` = LinkVariation.SMALL;

  /**
   * Sätter attributet 'target'. Om värdet är '_blank' så lägger komponenten automatikt till ett ´ref´-attribut med 'noopener noreferrer'.
   * @en Set `target` attribute. If value is '_blank', the component automatically adds a 'ref' attribute with 'noopener noreferrer'.
   */
  @Prop() afTarget: string;

  /**
   * Kringgår länkens vanliga beteende.
   * Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande.
   * @en Override default link behavior. Should only be used if default link behaviour is a problem with e.g. routing
   */
  @Prop() afOverrideLink = false;

  /**
   * Sätt till true om du använder Angular, se exempelkod under 'Översikt'
   * @en Set to true if using Angular
   */
  @Prop() afLinkContainer = false;

  /**
   * Avgör om färgen inte ska ändras på besökta länkar
   * @en If link color should change when visited
   */
  @Prop() hideVisitedColor: boolean;

  /**
   * Ska användas när länken laddar ner en fil. Attributet blir namnet på filen som laddas ner.
   * @en Use when the link is used to download a file. The attribute becomes tha name of the downloaded file.
   */
  @Prop() afDownload: string;

  /**
   * Länkelementets 'onclick'-event.
   * @en The link element's 'onclick' event.
   */
  @Event() afOnClick: EventEmitter<MouseEvent>;

  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet.
   * @en When the component and slots are loaded and initialized this event will trigger.
   */
  @Event({
    bubbles: false,
    cancelable: true
  })
  afOnReady: EventEmitter;

  @Method()
  async afMGetLinkElement() {
    if (this.afLinkContainer) {
      return this.hostElement.children.length > 0
        ? this.hostElement.children.item(0)
        : undefined;
    }
    return this._link;
  }

  clickLinkHandler(e: MouseEvent) {
    if (this.afOverrideLink) {
      e.preventDefault();
    }
    this.afOnClick.emit(e);
  }

  @Watch('afTarget')
  setRel() {
    this.relation =
      this.afTarget?.toLowerCase() === '_blank' ? 'noopener noreferrer' : null;
  }

  setHasIcon() {
    this.hasIcon = !!this.hostElement.querySelector('[slot="icon"]');
  }

  componentWillLoad() {
    this.setHasIcon();
    this.setRel();
  }

  componentWillUpdate() {
    this.setHasIcon();
    this.setRel();
  }
  componentDidLoad() {
    this.afOnReady.emit();
  }

  get cssModifiers() {
    return {
      [`digi-link--variation-${this.afVariation}`]: !!this.afVariation,
      'digi-link--has-icon': this.hasIcon,
      'digi-link--hide-visited': this.hideVisitedColor
    };
  }

  render() {
    return (
      <Host
        class={{
          'digi-link': true,
          ...this.cssModifiers
        }}>
        {!this.afLinkContainer && (
          <a
            href={this.afHref}
            onClick={(e) => this.clickLinkHandler(e)}
            target={this.afTarget}
            rel={this.relation}
            ref={(el) => (this._link = el)}
            aria-describedby={this.afDescribedby}
            aria-label={this.afAriaLabel}
            download={this.afDownload}>
            {this.hasIcon && (
              <div class="digi-link__icon-wrapper" aria-hidden="true">
                <slot name="icon"></slot>
              </div>
            )}
            <slot></slot>
          </a>
        )}
        {this.afLinkContainer && <slot></slot>}
      </Host>
    );
  }
}
