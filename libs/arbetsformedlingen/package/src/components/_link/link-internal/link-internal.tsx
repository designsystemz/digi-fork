import {
  Component,
  Event,
  EventEmitter,
  Prop,
  h,
  Element,
  Host
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { LinkVariation } from '../link/link-variation.enum';
import { LinkInternalVariation } from './link-internal-variation.enum';

/**
 * @slot default - Ska vara en textnod
 *
 * @enums LinkInternalVariation - link-internal-variation.enum.ts
 *@swedishName Intern länk
 */
@Component({
  tag: 'digi-link-internal',
  styleUrls: ['link-internal.scss'],
  scoped: true
})
export class LinkInternal {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Sätter attributet 'href'.
   * @en Set `href` attribute.
   */
  @Prop() afHref!: string;

  /**
   * Sätter attributet 'aria-describedby'.
   * @en Sets `aria-describedby` attribute.
   */
  @Prop() afDescribedby: string;

  /**
   * Sätter attributet 'aria-label'.
   * @en Sets `aria-label` attribute.
   */
  @Prop() afAriaLabel: string;

  /**
   * Sätter variant. Kan vara 'small' eller 'large'.
   * @en Sets the variation of the link.
   */
  @Prop() afVariation: `${LinkInternalVariation}` = LinkInternalVariation.SMALL;

  /**
   * Kringgår länkens vanliga beteende.
   * Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande.
   * @en Override default link behavior. Should only be used if default link behaviour is a problem with e.g. routing
   */
  @Prop() afOverrideLink = false;

  /**
   * Sätt till true om du använder Angular, se exempelkod under 'Översikt'
   * @en Set to true if using Angular
   */
  @Prop() afLinkContainer = false;

  /**
   * Avgör om färgen ska ändras på besökta länkar
   * @en If link color should change when visited
   */
  @Prop() hideVisitedColor: boolean;

  /**
   * Länkelementets 'onclick'-event.
   * @en The link element's 'onclick' event.
   */
  @Event() afOnClick: EventEmitter<MouseEvent>;

  clickLinkHandler(e: CustomEvent) {
    (e as CustomEvent).stopImmediatePropagation();
    this.afOnClick.emit(e.detail);
  }

  get cssModifiers() {
    return {
      [`digi-link-internal--variation-${this.afVariation}`]: !!this.afVariation,
      'digi-link--hide-visited': this.hideVisitedColor
    };
  }

  render() {
    return (
      <Host>
        {!this.afLinkContainer && (
          <digi-link
            class={{
              'digi-link-internal': true,
              ...this.cssModifiers
            }}
            afVariation={this.afVariation as LinkVariation}
            afHref={this.afHref}
            afOverrideLink={this.afOverrideLink}
            afLinkContainer={this.afLinkContainer}
            onAfOnClick={(e) => this.clickLinkHandler(e)}
            afDescribedby={this.afDescribedby}
            afAriaLabel={this.afAriaLabel}>
            <digi-icon
              class="digi-link-internal__icon"
              aria-hidden="true"
              slot="icon"
              afName={`chevron-right`}></digi-icon>
            <slot></slot>
          </digi-link>
        )}
        {this.afLinkContainer && (
          <digi-link
            afVariation={this.afVariation as LinkVariation}
            afHref={this.afHref}
            afOverrideLink={true}
            afLinkContainer={this.afLinkContainer}
            onAfOnClick={(e) => this.clickLinkHandler(e)}
            afDescribedby={this.afDescribedby}
            afAriaLabel={this.afAriaLabel}>
            <slot></slot>
          </digi-link>
        )}
      </Host>
    );
  }
}
