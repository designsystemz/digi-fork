export enum LinkInternalVariation {
  SMALL = 'small',
  LARGE = 'large'
}
