import { Component, Prop, h } from '@stencil/core';
import { LayoutMediaObjectAlignment } from './layout-media-object-alignment.enum';

/**
 * @slot media - Bör innehålla någon sorts mediakompinent (t.ex. en bild eller en video).
 * @slot default - kan innehålla vad som helst. Vanligen är det textinnehåll.
 *
 * @enums LayoutMediaObjectAlignment - layout-media-object-alignment.enum.ts
 * @swedishName Medieobjekt
 */
@Component({
  tag: 'digi-layout-media-object',
  styleUrls: ['layout-media-object.scss'],
  scoped: true
})
export class LayoutMediaObject {
  /**
   * Sätter justeringen av innehållet. Kan vara 'center', 'start', 'end' eller 'stretch'.
   * @en Set alignment of the content. Can be 'center', 'start', 'end' or 'stretch'.
   */
  @Prop() afAlignment: `${LayoutMediaObjectAlignment}` =
    LayoutMediaObjectAlignment.START;

  get cssModifiers() {
    return {
      [`digi-layout-media-object--alignment-${this.afAlignment}`]:
        !!this.afAlignment
    };
  }

  render() {
    return (
      <div
        class={{
          'digi-layout-media-object': true,
          ...this.cssModifiers
        }}>
        <div class="digi-layout-media-object__first">
          <slot name="media"></slot>
        </div>
        <div class="digi-layout-media-object__last">
          <slot></slot>
        </div>
      </div>
    );
  }
}
