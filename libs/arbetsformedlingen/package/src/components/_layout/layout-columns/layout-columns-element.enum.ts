export enum LayoutColumnsElement {
  DIV = 'div',
  UL = 'ul',
  OL = 'ol'
}
