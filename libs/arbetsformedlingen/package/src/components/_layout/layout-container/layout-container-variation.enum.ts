export enum LayoutContainerVariation {
  STATIC = 'static',
  FLUID = 'fluid',
  NONE = 'none'
}
