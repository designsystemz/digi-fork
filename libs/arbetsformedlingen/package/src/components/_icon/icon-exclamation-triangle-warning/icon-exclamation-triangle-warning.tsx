import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-exclamation-triangle-warning',
  styleUrl: 'icon-exclamation-triangle-warning.scss',
  scoped: true
})
export class IconExclamationTriangleWarning {
  private _id: string = randomIdGenerator('digi-icon');

  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-exclamation-triangle-warning"
        width="22"
        height="20"
        viewBox="0 0 22 20"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <path
            d="M9.5 8.38l.252 4.207a.455.455 0 00.458.424h1.58a.455.455 0 00.458-.424l.251-4.207a.454.454 0 00-.457-.477H9.958a.454.454 0 00-.457.477zm3.104 6.734c0 .872-.718 1.578-1.604 1.578-.886 0-1.604-.706-1.604-1.578 0-.87.718-1.577 1.604-1.577.886 0 1.604.706 1.604 1.577zm-.016-13.522c-.704-1.2-2.47-1.202-3.176 0L.247 17.218c-.703 1.2.178 2.704 1.588 2.704h18.33c1.407 0 2.292-1.502 1.587-2.704L12.588 1.592zM2.032 17.782l8.77-14.95a.231.231 0 01.397 0l8.77 14.95a.225.225 0 01-.2.337H2.23a.225.225 0 01-.198-.338z"
            id={`${this._id}-iconExclamationTriangelWarningPath`}
          />
        </defs>
        <g fill="none" fill-rule="nonzero">
          <path
            d="M11.874 2.573l8.3 14.941A1 1 0 0119.3 19H2.7a1 1 0 01-.875-1.486l8.3-14.94a1 1 0 011.75 0z"
            fill="var(--digi--icon-exclamation-triangle-warning--background)"
          />
          <g>
            <mask
              id={`${this._id}-iconExclamationTriangelWarningPathB`}
              fill="#FFF">
              <use href={`#${this._id}-iconExclamationTriangelWarningPath`} />
            </mask>
            <use
              fill="none"
              fill-rule="nonzero"
              href={`#${this._id}-iconExclamationTriangelWarningPath`}
            />
            <g
              mask={`url(#${this._id}-iconExclamationTriangelWarningPathB)`}
              fill="var(--digi--icon-exclamation-triangle-warning--outline)">
              <path d="M-1.571-2.308h25.143v24.615H-1.571z" />
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
