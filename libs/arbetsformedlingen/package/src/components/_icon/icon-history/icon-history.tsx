import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-history',
  styleUrl: 'icon-history.scss',
  scoped: true
})
export class IconHistory {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-history"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-history__shape"
          d="M47.9999585,23.954655 C48.0244008,37.1779041 37.2405807,47.9906973 24.0173778,48 C18.3062586,48.0040521 13.0603322,46.0132099 8.9378555,42.6860105 C7.86588961,41.8208477 7.78576072,40.2142965 8.75988806,39.2401657 L9.85024102,38.149809 C10.6833686,37.3166785 12.0134308,37.225517 12.9364615,37.9578087 C15.9737143,40.3679742 19.8174818,41.8065251 23.9999585,41.8065251 C33.8423608,41.8065251 41.8063793,33.8409305 41.8063793,24.0000422 C41.8063793,14.1576055 33.8408124,6.19355927 23.9999585,6.19355927 C19.2760312,6.19355927 14.9855547,8.02927218 11.7998505,11.0256968 L16.7115195,15.9373829 C17.6870016,16.9128684 16.9961319,18.5806778 15.6167149,18.5806778 L1.54838442,18.5806778 C0.693192349,18.5806778 0,17.8874831 0,17.032288 L0,2.96390844 C0,1.58448666 1.66780357,0.893614474 2.64328575,1.86900329 L7.42121296,6.64694717 C11.7298829,2.52929477 17.569518,7.81597009e-14 23.9999585,7.81597009e-14 C37.239613,7.81597009e-14 47.9754331,10.720664 47.9999585,23.954655 Z M31.4429456,30.3566663 C32.2304926,29.3441161 32.0480736,27.8848555 31.0355269,27.0974025 L27.0967273,24.0338164 L27.0967273,13.9355084 C27.0967273,12.6527642 26.0568904,11.6129236 24.7741507,11.6129236 L23.2257663,11.6129236 C21.9430265,11.6129236 20.9031896,12.6527642 20.9031896,13.9355084 L20.9031896,27.0630476 L27.2330819,31.9863465 C28.2456285,32.7737995 29.7047873,32.5914766 30.4923343,31.5789265 L31.4429456,30.3566663 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
