import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-send-solid',
  styleUrls: ['icon-send-solid.scss'],
  scoped: true
})
export class IconSendSolid {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-send-solid"
        width="48px"
        height="48px"
        viewBox="0 0 62 62"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-send-solid"
          d="M56.9779638,23.3091569 L6.19168404,0.453309406 C4.55264079,-0.367657999 2.57674191,-0.0524908153 1.27551673,1.22829497 C-0.0266708927,2.50524894 -0.368338746,4.46618275 0.419903429,6.10428574 L10.7238359,26.6658311 L34.8561733,26.6658311 C35.2103529,26.6658311 38.939826,27.5845099 38.939826,31.0005773 C38.939826,34.6628392 35.1805172,35.3343656 34.8561733,35.3343656 L10.8268175,35.3343656 L1.2447185,54.1811716 L0.420865873,55.8930371 C-0.368338746,57.534014 -0.0266708927,59.4949478 1.27359184,60.7699858 C2.07723313,61.5631725 3.14458426,62 4.27641917,62 C4.93954354,62 5.6045928,61.8419374 6.1002518,61.5899953 L56.9683393,38.6958296 C60.0250636,37.3489449 62,34.3285129 62,31.0005773 C62,27.6764736 60.0289133,24.6598734 56.9779638,23.3091569"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
