import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-online-interview',
  styleUrls: ['icon-online-interview.scss'],
  scoped: true
})
export class IconOnlineInterview {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-online-interview"
        width="62px"
        height="63px"
        viewBox="0 0 62 63"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon id="path-1" points="0 0 62 0 62 9.21298912 0 9.21298912" />
        </defs>
        <g
          id="Ikonguide"
          stroke="none"
          stroke-width="1"
          fill="none"
          fill-rule="evenodd">
          <g transform="translate(-627, -890)" id="Online-interview">
            <g transform="translate(627, 896.4583)">
              <g id="Group" transform="translate(0, 0)">
                <path
                  d="M8.78313943,7.5511486 C8.78313943,6.84208031 9.3379027,6.26282418 10.0151303,6.26282418 L51.9848697,6.26282418 C52.6620973,6.26282418 53.2168606,6.84208031 53.2168606,7.5511486 L53.2168606,34.4369605 L58.8881822,34.4369605 L58.8881822,2.89872993 C58.8881822,1.30424477 57.6414956,0 56.1168151,0 L5.88318486,0 C4.35850436,0 3.1118178,1.30424477 3.1118178,2.89872993 L3.1118178,34.4369605 L8.78313943,34.4369605 L8.78313943,7.5511486 Z"
                  fill="currentColor"
                />
                <g transform="translate(0, 39.3695)">
                  <mask id="mask-2" fill="white">
                    <use href="#path-1" />
                  </mask>
                  <g />
                  <path
                    d="M43.6913426,0 C39.7639125,1.36670156 37.9232741,3.2171371 31.0003674,3.2171371 C24.0774606,3.2171371 22.2368222,1.36670156 18.3093922,0 L-0.000244928595,0 L-0.000244928595,4.37809864 C-0.000244928595,7.03802319 1.44115986,9.21298912 4.70850732,9.21298912 L57.2922275,9.21298912 C60.5595749,9.21298912 62.0009797,7.03802319 62.0009797,4.37809864 L62.0009797,0 L43.6913426,0 Z"
                    fill="currentColor"
                  />
                </g>
                <path
                  d="M42.3027199,35.1206787 C42.3027199,35.8223991 41.7602031,36.3918581 41.091548,36.3918581 L20.9082071,36.3918581 C20.239552,36.3918581 19.6982598,35.8223991 19.6982598,35.1206787 L19.6982598,30.7107393 C19.6982598,27.9014083 21.8658779,25.624797 24.5417228,25.624797 L27.2971695,25.624797 C29.6901219,26.7759614 32.3978075,26.7343236 34.7025856,25.624797 L37.4592569,25.624797 C40.1338772,25.624797 42.3027199,27.9014083 42.3027199,30.7107393 L42.3027199,35.1206787 Z M31.0004899,10.3657456 C34.5666502,10.3657456 37.4592569,13.4016355 37.4592569,17.1478184 C37.4592569,20.8927766 34.5666502,23.9286665 31.0004899,23.9286665 C27.4331049,23.9286665 24.5417228,20.8927766 24.5417228,17.1478184 C24.5417228,13.4016355 27.4331049,10.3657456 31.0004899,10.3657456 L31.0004899,10.3657456 Z"
                  fill="currentColor"
                />
                <path
                  d="M31.0002449,23.9289115 C34.5676299,23.9289115 37.459012,20.8930215 37.459012,17.1468386 C37.459012,13.4018804 34.5676299,10.3659905 31.0002449,10.3659905 C27.4328599,10.3659905 24.5414779,13.4018804 24.5414779,17.1468386 C24.5414779,20.8930215 27.4328599,23.9289115 31.0002449,23.9289115"
                  fill="currentColor"
                />
                <path
                  d="M37.4587671,25.6243072 L34.7020957,25.6243072 C32.3985423,26.7338337 29.6908567,26.7766962 27.2979043,25.6243072 L24.5412329,25.6243072 C21.8666127,25.6243072 19.69777,27.9009185 19.69777,30.7102495 L19.69777,35.1201888 C19.69777,35.8231339 20.2402868,36.3925929 20.9089419,36.3925929 L41.0922828,36.3925929 C41.7609378,36.3925929 42.30223,35.8231339 42.30223,35.1201888 L42.30223,30.7102495 C42.30223,27.9009185 40.134612,25.6243072 37.4587671,25.6243072"
                  fill="currentColor"
                />
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
