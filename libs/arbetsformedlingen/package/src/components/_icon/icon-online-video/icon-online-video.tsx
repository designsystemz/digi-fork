import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-online-video',
  styleUrls: ['icon-online-video.scss'],
  scoped: true
})
export class IconOnlineVideo {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-online-video"
        width="62px"
        height="63px"
        viewBox="0 0 62 63"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}

        <defs>
          <polygon
            id="path-1"
            points="0 0 57.8288208 0 57.8288208 31.6225833 0 31.6225833"
          />
          <polygon
            id="path-3"
            points="0 0 10.1235667 0 10.1235667 4.72491667 0 4.72491667"
          />
          <polygon
            id="path-5"
            points="0 0 31.6055333 0 31.6055333 4.72491667 0 4.72491667"
          />
          <polygon
            id="path-7"
            points="0 0 8.24083333 0 8.24083333 8.24070417 0 8.24070417"
          />
        </defs>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-41, -890)">
            <g transform="translate(43.5833, 897.75)">
              <g transform="translate(0, 0)">
                <g>
                  <mask fill="white">
                    <use href="#path-1" />
                  </mask>
                  <g />
                  <path
                    d="M35.4494042,16.844625 L23.6435708,23.66075 C23.4472375,23.7744167 23.2302375,23.8299583 23.0119458,23.8299583 C22.7936542,23.8299583 22.5753625,23.7744167 22.3803208,23.66075 C21.9889458,23.4347083 21.7486958,23.0187917 21.7486958,22.5667083 L21.7486958,8.93445833 C21.7486958,8.482375 21.9889458,8.06516667 22.3803208,7.84041667 C22.7704042,7.614375 23.2521958,7.614375 23.6435708,7.84041667 L35.4494042,14.6565417 C35.8407792,14.8812917 36.0810292,15.2997917 36.0810292,15.7505833 C36.0810292,16.201375 35.8407792,16.6185833 35.4494042,16.844625 M55.2971542,0 L2.53257083,0 C1.14015417,0 -0.0003875,1.13925 -0.0003875,2.53166667 L-0.0003875,29.089625 C-0.0003875,30.4820417 1.14015417,31.6225833 2.53257083,31.6225833 L55.2971542,31.6225833 C56.6895708,31.6225833 57.8288208,30.4820417 57.8288208,29.089625 L57.8288208,2.53166667 C57.8288208,1.13925 56.6895708,0 55.2971542,0"
                    fill="currentColor"
                  />
                </g>
                <g transform="translate(0, 40.7418)">
                  <mask fill="white">
                    <use href="#path-3" />
                  </mask>
                  <g />
                  <path
                    d="M10.1235667,0 L2.36194167,0 C1.062525,0 -0.000516666667,1.06304167 -0.000516666667,2.36245833 C-0.000516666667,3.661875 1.062525,4.72491667 2.36194167,4.72491667 L10.1235667,4.72491667 C9.87298333,3.97833333 9.70506667,3.193 9.70506667,2.36245833 C9.70506667,1.53191667 9.87298333,0.746583333 10.1235667,0"
                    fill="currentColor"
                  />
                </g>
                <g transform="translate(26.2237, 40.7418)">
                  <mask fill="white">
                    <use href="#path-5" />
                  </mask>
                  <g />
                  <path
                    d="M29.2433333,0 L0,0 C0.250583333,0.746583333 0.4185,1.53191667 0.4185,2.36245833 C0.4185,3.193 0.250583333,3.97833333 0,4.72491667 L29.2433333,4.72491667 C30.54275,4.72491667 31.6057917,3.661875 31.6057917,2.36245833 C31.6057917,1.06304167 30.54275,0 29.2433333,0"
                    fill="currentColor"
                  />
                </g>
                <g transform="translate(14.0529, 38.9837)">
                  <mask fill="white">
                    <use href="#path-7" />
                  </mask>
                  <g />
                  <path
                    d="M8.24083333,4.12041667 C8.24083333,6.39633333 6.39633333,8.24083333 4.12041667,8.24083333 C1.8445,8.24083333 0,6.39633333 0,4.12041667 C0,1.8445 1.8445,0 4.12041667,0 C6.39633333,0 8.24083333,1.8445 8.24083333,4.12041667"
                    fill="currentColor"
                  />
                </g>
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
