import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-media-course',
  styleUrl: 'icon-media-course.scss',
  scoped: true
})
export class IconMediaCourse {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-media-course"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-media-course__shape"
          d="M40.7878788,0 C43.465623,0 45.6363636,2.17074061 45.6363636,4.84848485 L45.6363636,43.1515152 C45.6363636,45.8292594 43.465623,48 40.7878788,48 L11.6969697,48 C6.34148121,48 2,43.6585188 2,38.3030303 L2,9.6969697 C2,4.66876107 5.82706929,0.534412361 10.7272502,0.0478812611 L10.7272727,0 L40.7878788,0 Z M40.7878788,4.84848485 L26,4.848 L26,17.8909296 C26,19.1616284 24.9698951,20.1917635 23.6991963,20.1917635 C23.1408408,20.1917635 22.601537,19.9886909 22.1818182,19.6204558 L19.9624223,17.6732967 C19.0476445,16.8707279 17.6796282,16.8707279 16.7648504,17.6732967 L14.5454545,19.6204558 C13.5902636,20.4584806 12.136575,20.3634987 11.2985501,19.4083077 C10.9303151,18.9885889 10.7272727,18.4492852 10.7272727,17.8909296 L10.7271785,4.94549123 C8.62897951,5.37144669 7.02282264,7.1523487 6.8617836,9.33512159 L6.84848485,9.6969697 L6.84848485,38.3030303 C6.84848485,40.8590589 8.8263704,42.9531276 11.3351216,43.1382164 L11.6969697,43.1515152 L40.7878788,43.1515152 L40.7878788,4.84848485 Z M21.151,4.848 L15.575,4.848 L15.5757576,12.7757576 L15.9443377,12.6359275 C17.5076218,12.0852045 19.2196509,12.0852045 20.782935,12.6359275 L21.1515152,12.7781818 L21.151,4.848 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
