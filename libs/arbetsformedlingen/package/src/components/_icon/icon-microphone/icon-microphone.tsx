import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-microphone',
  styleUrls: ['icon-microphone.scss'],
  scoped: true
})
export class IconMicrophone {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-microphone"
        width="48"
        height="48"
        viewBox="0 0 32 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        fill="currentColor"
        fill-rule="nonzero"
        id="Icon">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g id="Pågående" stroke="none" stroke-width="1" fill-rule="evenodd">
          <g
            id="Artboard"
            transform="translate(-1591.000000, -853.000000)"
            fill-rule="nonzero">
            <g
              id="mic-bred-on-v2"
              transform="translate(1591.000000, 853.000000)">
              <path
                d="M19.457,32.556 L12.542,32.556 C9.224,32.556 6.526,29.838 6.526,26.497 L6.526,6.059 C6.526,2.718 9.225,0 12.543,0 L19.458,0 C22.775,0 25.474,2.718 25.474,6.059 L25.474,26.498 C25.473,29.839 22.774,32.556 19.457,32.556 Z M12.543,3.921 C11.373,3.921 10.421,4.88 10.421,6.059 L10.421,26.498 C10.421,27.677 11.373,28.636 12.543,28.636 L19.458,28.636 C20.628,28.636 21.58,27.677 21.58,26.498 L21.58,6.059 C21.58,4.88 20.628,3.921 19.458,3.921 L12.543,3.921 Z"
                id="Shape"></path>
              <path
                d="M30.053,16.158 C28.978,16.158 28.106,17.036 28.106,18.119 L28.106,28.889 C28.106,31.927 25.652,34.399 22.636,34.399 L9.364,34.399 C6.348,34.399 3.894,31.927 3.894,28.889 L3.894,18.119 C3.894,17.036 3.022,16.158 1.947,16.158 C0.872,16.158 0,17.035 0,18.118 L0,28.888 C0,34.089 4.201,38.319 9.364,38.319 L14.053,38.319 L14.053,46.039 C14.053,47.122 14.925,48 16,48 C17.075,48 17.947,47.122 17.947,46.039 L17.947,38.319 L22.636,38.319 C27.799,38.319 32,34.089 32,28.888 L32,18.118 C32,17.035 31.128,16.158 30.053,16.158 Z"
                id="Path"></path>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
