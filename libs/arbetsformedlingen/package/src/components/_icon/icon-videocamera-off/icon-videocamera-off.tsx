import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-videocamera-off',
  styleUrls: ['icon-videocamera-off.scss'],
  scoped: true
})
export class IconVideocameraOff {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-videocamera-off"
        width="48"
        height="48"
        viewBox="0 0 48 47.999"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        fill="currentColor"
        id="Icon">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon
            id="path-1"
            points="-4.4408921e-16 0 45.2679002 0 45.2679002 47.9989489 -4.4408921e-16 47.9989489"></polygon>
        </defs>
        <g id="Pågående" stroke="none" stroke-width="1" fill-rule="evenodd">
          <g id="Artboard" transform="translate(-1062.000000, -776.000000)">
            <g id="cam-off" transform="translate(1062.000000, 776.000000)">
              <path
                class="digi-icon-videocamera-off__shape"
                d="M3.964,36.0208 L3.964,11.9778 L3.999,11.9778 L1.396,8.2628 C0.574,8.6518 4.4408921e-16,9.4808 4.4408921e-16,10.4468 L4.4408921e-16,37.5528 C4.4408921e-16,38.8888 1.09,39.9758 2.429,39.9758 L23.618,39.9758 L20.847,36.0218 L3.964,36.0218 L3.964,36.0208 Z"
                id="Fill-1"></path>
              <g id="Group-5" transform="translate(2.732000, 0.000000)">
                <g id="Clip-4"></g>
                <path
                  class="digi-icon-videocamera-off__shape"
                  d="M41.2609002,34.2247 L31.4049002,27.5537 L31.4049002,20.4447 L41.2609002,13.7737 L41.2609002,34.2247 Z M11.3019002,11.9777 L27.4419002,11.9777 L27.4419002,35.0027 L11.3019002,11.9777 Z M44.2029002,8.2317 C43.5499002,7.8857 42.7559002,7.9277 42.1409002,8.3437 L31.4059002,15.6107 L31.4059002,10.4467 C31.4059002,9.1107 30.3159002,8.0237 28.9769002,8.0237 L8.52990024,8.0237 L3.47890024,0.8187 C2.87290024,-0.0463 1.68190024,-0.2593 0.815900243,0.3397 C0.481900243,0.5697 0.245900243,0.8877 0.115900243,1.2417 C-0.0900997574,1.8047 -0.0260997574,2.4557 0.344900243,2.9867 L31.3239002,47.1807 C31.5579002,47.5147 31.8789002,47.7507 32.2349002,47.8807 C32.8009002,48.0887 33.4559002,48.0267 33.9869002,47.6597 C34.3209002,47.4287 34.5569002,47.1107 34.6869002,46.7567 C34.8929002,46.1947 34.8289002,45.5427 34.4579002,45.0117 L30.5259002,39.4017 C31.0589002,38.9567 31.4059002,38.2977 31.4059002,37.5507 L31.4059002,32.3857 L42.1409002,39.6527 C42.4789002,39.8817 42.8709002,39.9977 43.2649002,39.9977 C43.5869002,39.9977 43.9089002,39.9207 44.2029002,39.7647 C44.8579002,39.4167 45.2679002,38.7367 45.2679002,37.9967 L45.2679002,9.9987 C45.2679002,9.2597 44.8579002,8.5797 44.2029002,8.2317 L44.2029002,8.2317 Z"
                  id="Fill-3"></path>
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
