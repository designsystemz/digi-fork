import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-headphones',
  styleUrls: ['icon-headphones.scss'],
  scoped: true
})
export class IconHeadphones {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-headphones"
        width="26"
        height="22"
        viewBox="0 0 26 22"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-headphones__shape"
          d="M13 0C5.815 0 0 5.623 0 12.571v2.358c0 .595.348 1.139.898 1.405l.73.353C1.732 19.638 4.237 22 7.313 22h1.22c.672 0 1.218-.528 1.218-1.179V12.18c0-.651-.546-1.179-1.219-1.179H7.312c-1.591 0-3.03.632-4.062 1.651v-.08c0-5.198 4.374-9.428 9.75-9.428s9.75 4.23 9.75 9.428v.08A5.766 5.766 0 0018.687 11H17.47c-.673 0-1.219.528-1.219 1.179v8.642c0 .651.546 1.179 1.219 1.179h1.218c3.077 0 5.582-2.362 5.684-5.313l.73-.353c.551-.266.899-.81.899-1.405V12.57C26 5.624 20.186 0 13 0z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
