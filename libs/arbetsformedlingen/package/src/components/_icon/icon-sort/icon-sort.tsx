import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-sort',
  styleUrls: ['icon-sort.scss'],
  scoped: true
})
export class IconSort {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-sort"
        width="12"
        height="16"
        viewBox="0 0 12 16"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-sort__shape"
          d="M11.777 9.67a.689.689 0 01.223.512.689.689 0 01-.223.511l-5.25 5.091A.732.732 0 016 16a.732.732 0 01-.527-.216l-5.25-5.09A.689.689 0 010 10.181c0-.197.074-.368.223-.512a.732.732 0 01.527-.215h10.5c.203 0 .379.072.527.215zm0-4.363a.689.689 0 01.223.511.689.689 0 01-.223.512.732.732 0 01-.527.215H.75a.732.732 0 01-.527-.215A.689.689 0 010 5.818c0-.197.074-.367.223-.511L5.473.216A.732.732 0 016 0c.203 0 .379.072.527.216l5.25 5.09z"
          fill="currentColor"
          fill-rule="evenodd"
        />
      </svg>
    );
  }
}
