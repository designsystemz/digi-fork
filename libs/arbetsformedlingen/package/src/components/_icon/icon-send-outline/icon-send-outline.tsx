import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-send-outline',
  styleUrls: ['icon-send-outline.scss'],
  scoped: true
})
export class IconSendOutline {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-send-outline"
        xmlns="http://www.w3.org/2000/svg"
        width="48px"
        height="48px"
        viewBox="0 0 62 62"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-send-outline"
          d="M15.2425136,37.4224461 L5.74992195,56.3058551 L54.9327689,34.1672805 C56.2051207,33.6068709 57.0154991,32.366306 57.0154991,31.0002477 C57.0154991,29.6351474 56.2041583,28.3945825 54.9491304,27.8399207 L54.9327689,27.8332149 L5.74992195,5.69655625 L15.2425136,24.5780494 L34.8561733,24.5780494 C37.9725691,24.5780494 41.3084023,27.1578495 41.3084023,31.0002477 C41.3084023,34.842646 37.9725691,37.4224461 34.8561733,37.4224461 L15.2425136,37.4224461 Z M4.27641917,62 C3.14458426,62 2.07723313,61.5641259 1.27359184,60.7709307 C-0.0266708927,59.4949211 -0.368338746,57.5339665 0.420865873,55.8929722 L1.2447185,54.1810884 L12.1646156,32.4611446 L34.8561733,32.4611446 C35.1805172,32.4611446 36.3239014,32.1105293 36.3239014,31.0002477 C36.3239014,29.9790569 35.2103529,29.5393509 34.8561733,29.5393509 L12.1646156,29.5393509 L0.419903429,6.10369144 C-0.368338746,4.466529 -0.0266708927,2.50557434 1.27551673,1.22764883 C2.57674191,-0.0531505779 4.55264079,-0.367363147 6.19168404,0.453612987 L56.9779638,23.3087455 C60.0289133,24.6594764 62,27.6770666 62,31.0002477 C62,34.3282187 60.0250636,37.3496408 56.9683393,38.6965398 L6.1002518,61.5909489 C5.60363036,61.8428937 4.93954354,62 4.27641917,62 L4.27641917,62 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
