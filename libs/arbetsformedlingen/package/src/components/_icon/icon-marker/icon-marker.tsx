import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-marker',
  styleUrl: 'icon-marker.scss',
  scoped: true
})
export class IconMarker {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-marker"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-marker__shape"
          d="M24,0 C14.0534063,0 6,8.06381641 6,18 C6,24.6672307 8.24915625,26.7507317 20.1816562,45.8835222 C21.94275,48.7071798 26.0592187,48.7038048 27.81825,45.8835222 C39.7570312,26.7407005 42,24.6659182 42,18 C42,8.05341015 33.9361875,0 24,0 Z M24,43.5000211 C12.054,24.3455743 10.5,23.1363862 10.5,18.0000022 C10.5,10.5441614 16.5441563,4.50000218 24,4.50000218 C31.4558437,4.50000218 37.5,10.5441614 37.5,18.0000022 C37.5,23.1143549 36.0781875,24.1336054 24,43.5000211 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
