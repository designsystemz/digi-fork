import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-comunication-play',
  styleUrl: 'icon-comunication-play.scss',
  scoped: true
})
export class IconComunicationPlay {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-comunication-play"
        width="64"
        height="64"
        viewBox="0 0 64 64"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon points="0 0 64 0 64 64 0 64" />
        </defs>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-1383, -386)">
            <g transform="translate(1383, 386)">
              <path
                d="M24.117493,45.0566427 C23.7650141,45.0566427 23.4125352,44.9661414 23.0965744,44.783551 C22.4646529,44.4183702 22.0756559,43.7451673 22.0756559,43.0148057 L22.0756559,20.9848768 C22.0756559,20.2561029 22.4646529,19.5813123 23.0965744,19.2177192 C23.728496,18.8525384 24.5064899,18.8525384 25.1384115,19.2177192 L44.2167269,30.2318898 C44.8486484,30.5970706 45.2376454,31.2718612 45.2376454,32.0006351 C45.2376454,32.729409 44.8486484,33.4041996 44.2167269,33.7677926 L25.1384115,44.783551 C24.8224507,44.9661414 24.4699718,45.0566427 24.117493,45.0566427"
                fill="currentColor"
              />
              <g>
                <mask id="mask-2" fill="white">
                  <use href="#path-1" />
                </mask>
                <g />
                <path
                  d="M32.0006351,6.12519351 C17.7331799,6.12519351 6.12519351,17.7331799 6.12519351,32.0006351 C6.12519351,46.2680903 17.7331799,57.8744889 32.0006351,57.8744889 C46.2680903,57.8744889 57.8760767,46.2680903 57.8760767,32.0006351 C57.8760767,17.7331799 46.2680903,6.12519351 32.0006351,6.12519351 M32.0006351,64 C14.3560513,64 -0.000317548525,49.6452189 -0.000317548525,32.0006351 C-0.000317548525,14.3560513 14.3560513,-0.000317548525 32.0006351,-0.000317548525 C49.6452189,-0.000317548525 64,14.3560513 64,32.0006351 C64,49.6452189 49.6452189,64 32.0006351,64"
                  fill="currentColor"
                />
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
