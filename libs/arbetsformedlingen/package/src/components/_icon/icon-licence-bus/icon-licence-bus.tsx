import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-licence-bus',
  styleUrl: 'icon-licence-bus.scss',
  scoped: true
})
export class IconLicenceBus {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-licence-bus"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-licence-bus__shape"
          d="M45.75,12 L45,12 L45,7.5 C45,3.3 35.7,0 24,0 C12.3,0 3,3.3 3,7.5 L3,12 L2.25,12 C1.0078125,12 0,13.006875 0,14.25 L0,21.75 C0,22.9921875 1.0078125,24 2.25,24 L3,24 L3,39 C3,40.6565625 4.3434375,42 6,42 L6,45 C6,46.6565625 7.3434375,48 9,48 L12,48 C13.6565625,48 15,46.6565625 15,45 L15,42 L33,42 L33,45 C33,46.6565625 34.3434375,48 36,48 L39,48 C40.6565625,48 42,46.6565625 42,45 L42,42 L42.6,42 C44.1,42 45,40.8 45,39.6 L45,24 L45.75,24 C46.9921875,24 48,22.9921875 48,21.75 L48,14.25 C48,13.006875 46.9921875,12 45.75,12 Z M10.5,37.5 C8.8434375,37.5 7.5,36.1565625 7.5,34.5 C7.5,32.8434375 8.8434375,31.5 10.5,31.5 C12.1565625,31.5 13.5,32.8434375 13.5,34.5 C13.5,36.1565625 12.1565625,37.5 10.5,37.5 Z M12,27 C10.3434375,27 9,25.6565625 9,24 L9,12 C9,10.3434375 10.3434375,9 12,9 L36,9 C37.6565625,9 39,10.3434375 39,12 L39,24 C39,25.6565625 37.6565625,27 36,27 L12,27 Z M37.5,37.5 C35.8434375,37.5 34.5,36.1565625 34.5,34.5 C34.5,32.8434375 35.8434375,31.5 37.5,31.5 C39.1565625,31.5 40.5,32.8434375 40.5,34.5 C40.5,36.1565625 39.1565625,37.5 37.5,37.5 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
