import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-licence-car',
  styleUrl: 'icon-licence-car.scss',
  scoped: true
})
export class IconLicenceCar {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-licence-car"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-licence-car__shape"
          d="M40.8,19.244898 L39.6,19.244898 L31.4415,8.83826531 C30.5303537,7.67651295 29.1512212,7 27.693,7 L11.64975,7 C9.687,7 7.92225,8.21913265 7.19325,10.0788265 L3.6,19.4178571 C1.533,19.9642857 0,21.8630102 0,24.1428571 L0,32.7142857 C0,33.3908163 0.537,33.9387755 1.2,33.9387755 L4.8,33.9387755 C4.8,37.9964286 8.0235,41.2857143 12,41.2857143 C15.9765,41.2857143 19.2,37.9964286 19.2,33.9387755 L28.8,33.9387755 C28.8,37.9964286 32.0235,41.2857143 36,41.2857143 C39.9765,41.2857143 43.2,37.9964286 43.2,33.9387755 L46.8,33.9387755 C47.463,33.9387755 48,33.3908163 48,32.7142857 L48,26.5918367 C48,22.5341837 44.7765,19.244898 40.8,19.244898 Z M12,37.6122449 C10.01475,37.6122449 8.4,35.9645408 8.4,33.9387755 C8.4,31.9130102 10.01475,30.2653061 12,30.2653061 C13.98525,30.2653061 15.6,31.9130102 15.6,33.9387755 C15.6,35.9645408 13.98525,37.6122449 12,37.6122449 Z M17.4,19.244898 L8.76975,19.244898 L11.64975,11.8979592 L17.4,11.8979592 L17.4,19.244898 Z M21,19.244898 L21,11.8979592 L27.693,11.8979592 L33.453,19.244898 L21,19.244898 Z M36,37.6122449 C34.01475,37.6122449 32.4,35.9645408 32.4,33.9387755 C32.4,31.9130102 34.01475,30.2653061 36,30.2653061 C37.98525,30.2653061 39.6,31.9130102 39.6,33.9387755 C39.6,35.9645408 37.98525,37.6122449 36,37.6122449 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
