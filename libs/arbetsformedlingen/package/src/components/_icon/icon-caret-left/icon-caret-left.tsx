import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-caret-left',
  styleUrls: ['icon-caret-left.scss'],
  scoped: true
})
export class IconCaretLeft {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-caret-left"
        width="7"
        height="13"
        viewBox="0 0 7 13"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-caret-left__shape"
          d="M6.786 1.607v10.5a.768.768 0 01-.198.527.608.608 0 01-.469.223.608.608 0 01-.469-.223L.984 7.384a.768.768 0 01-.198-.527c0-.203.066-.379.198-.527L5.65 1.08A.608.608 0 016.12.857c.18 0 .337.074.469.223a.768.768 0 01.198.527z"
          fill="currentColor"
          fill-rule="evenodd"
        />
      </svg>
    );
  }
}
