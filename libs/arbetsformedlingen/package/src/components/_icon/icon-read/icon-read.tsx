import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-read',
  styleUrls: ['icon-read.scss'],
  scoped: true
})
export class IconRead {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-read"
        width="62px"
        height="63px"
        viewBox="0 0 62 63"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon
            id="path-1"
            points="0 0 6.45833333 0 6.45833333 12.9166667 0 12.9166667"
          />
          <polygon
            id="path-3"
            points="0 0 6.45833333 0 6.45833333 12.9166667 0 12.9166667"
          />
          <polygon
            id="path-5"
            points="0 0 21.9583333 0 21.9583333 20.6666667 0 20.6666667"
          />
        </defs>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-293, -890)" id="Read">
            <g transform="translate(293, 893.875)" id="Group">
              <path
                d="M28.31239,24.8305377 L9.14594332,19.4030327 C8.47212293,19.2144231 7.75,20.0122066 7.75,20.7572731 L7.75,26.2878691 C7.84298238,26.2773257 7.93596477,26.2632678 8.03136228,26.2632678 C10.5346413,26.2632678 10.1494285,29.7554743 10.1494285,34.0653795 C10.1494285,38.3741132 10.5346413,41.8674911 8.03136228,41.8674911 C7.93596477,41.8674911 7.84298238,41.8522618 7.75,41.8428899 L7.75,47.4238598 C7.75,48.1689263 8.31393212,48.5110009 9.14594332,48.7781002 L28.31239,54.2056052 C29.2023643,54.4563037 29.7083333,53.5964313 29.7083333,52.8513648 L29.7083333,26.1847781 C29.7083333,25.4397116 29.1069667,25.0730358 28.31239,24.8305377"
                id="Fill-1"
                fill="currentColor"
              />
              <g transform="translate(0, 27.125)">
                <mask id="mask-2" fill="white">
                  <use href="#path-1" />
                </mask>
                <g />
                <path
                  d="M6.45833333,6.45833333 C6.45833333,10.0251462 5.93688554,12.9166667 3.82490292,12.9166667 C1.71172977,12.9166667 -0.000238104016,10.0251462 -0.000238104016,6.45833333 C-0.000238104016,2.89152047 1.71172977,0 3.82490292,0 C5.93688554,0 6.45833333,2.89152047 6.45833333,6.45833333"
                  fill="currentColor"
                />
              </g>
              <g transform="translate(55.5417, 27.125)">
                <mask id="mask-4" fill="white">
                  <use href="#path-3" />
                </mask>
                <g />
                <path
                  d="M6.45880956,6.45833333 C6.45880956,10.0251462 4.74677856,12.9166667 2.63471807,12.9166667 C0.521467021,12.9166667 0,10.0251462 0,6.45833333 C0,2.89152047 0.521467021,0 2.63471807,0 C4.74677856,0 6.45880956,2.89152047 6.45880956,6.45833333"
                  id="Fill-6"
                  fill="currentColor"
                />
              </g>
              <path
                d="M33.6819522,24.8305377 L51.5680478,19.4030327 C52.2391372,19.2144231 52.9583333,20.0122066 52.9583333,20.7572731 L52.9583333,26.2878691 C52.8657278,26.2773257 52.7731223,26.2632678 52.6781114,26.2632678 C50.1849783,26.2632678 50.5686297,29.7554743 50.5686297,34.0653795 C50.5686297,38.3741132 50.1849783,41.8674911 52.6781114,41.8674911 C52.7731223,41.8674911 52.8657278,41.8522618 52.9583333,41.8428899 L52.9583333,47.4238598 C52.9583333,48.1689263 52.3966868,48.5110009 51.5680478,48.7781002 L33.6819522,54.2056052 C32.795585,54.4563037 32.2916667,53.5964313 32.2916667,52.8513648 L32.2916667,26.1847781 C32.2916667,25.4397116 32.8905959,25.0730358 33.6819522,24.8305377"
                fill="currentColor"
              />
              <g transform="translate(19.375, 0)">
                <mask id="mask-6" fill="white">
                  <use href="#path-5" />
                </mask>
                <g />
                <path
                  d="M10.9791667,20.6666667 C17.0425217,20.6666667 21.9583333,16.0397408 21.9583333,10.3332757 C21.9583333,4.62681064 17.0425217,-0.000115212297 10.9791667,-0.000115212297 C4.91581163,-0.000115212297 0,4.62681064 0,10.3332757 C0,16.0397408 4.91581163,20.6666667 10.9791667,20.6666667"
                  fill="currentColor"
                />
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
