import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-toggle-on',
  styleUrls: ['icon-toggle-on.scss'],
  scoped: true
})
export class IconToggleOn {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-toggle-on"
        width="29"
        height="19"
        viewBox="0 0 29 19"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-toggle-on__shape"
          d="M29 9.5c0 5.247-4.328 9.5-9.667 9.5H9.667C4.327 19 0 14.747 0 9.5S4.328 0 9.667 0h9.666C24.673 0 29 4.253 29 9.5zm-9.667-6.333c-3.561 0-6.444 2.832-6.444 6.333 0 3.5 2.882 6.333 6.444 6.333s6.445-2.832 6.445-6.333c0-3.5-2.883-6.333-6.445-6.333z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
