import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-bell-filled',
  styleUrl: 'icon-bell-filled.scss',
  scoped: true
})
export class IconBellFilled {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-bell-filled"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-bell-filled__shape"
          d="M23.555833,48 C26.8859842,48 29.5872534,45.3140625 29.5872534,42 L17.5244126,42 C17.5244126,45.3140625 20.2256818,48 23.555833,48 Z M43.8639093,33.9646875 C42.0423204,32.0184375 38.6339126,29.090625 38.6339126,19.5 C38.6339126,12.215625 33.4972582,6.384375 26.5710718,4.95375 L26.5710718,3 C26.5710718,1.3434375 25.2209086,0 23.555833,0 C21.8907574,0 20.5405942,1.3434375 20.5405942,3 L20.5405942,4.95375 C13.6144078,6.384375 8.47775338,12.215625 8.47775338,19.5 C8.47775338,29.090625 5.06934557,32.0184375 3.24775666,33.9646875 C2.68204582,34.569375 2.43124734,35.2921875 2.43589641,36 C2.44633297,37.5375 3.65978272,39 5.4625146,39 L41.6491514,39 C43.4518833,39 44.6662759,37.5375 44.6757696,36 C44.6804187,35.2921875 44.4296202,34.5684375 43.8639093,33.9646875 L43.8639093,33.9646875 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
