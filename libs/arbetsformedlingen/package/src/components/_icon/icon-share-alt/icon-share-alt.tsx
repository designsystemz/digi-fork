import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-share-alt',
  styleUrls: ['icon-share-alt.scss'],
  scoped: true
})
export class IconShareAlt {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-share-alt"
        width="22"
        height="26"
        viewBox="0 0 22 26"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-share-alt__shape"
          d="M17.286 16.25c-1.11 0-2.13.397-2.936 1.061l-5.033-3.253a5.066 5.066 0 000-2.116l5.033-3.253a4.598 4.598 0 002.936 1.061C19.889 9.75 22 7.567 22 4.875S19.89 0 17.286 0c-2.604 0-4.715 2.183-4.715 4.875 0 .363.04.718.112 1.058L7.65 9.186a4.598 4.598 0 00-2.936-1.061C2.111 8.125 0 10.308 0 13s2.11 4.875 4.714 4.875c1.11 0 2.13-.397 2.936-1.061l5.033 3.253a5.05 5.05 0 00-.112 1.058c0 2.692 2.111 4.875 4.715 4.875C19.889 26 22 23.817 22 21.125s-2.11-4.875-4.714-4.875z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
