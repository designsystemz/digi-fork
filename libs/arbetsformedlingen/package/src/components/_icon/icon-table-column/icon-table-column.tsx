import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-table-column',
  styleUrl: 'icon-table-column.scss',
  scoped: true
})
export class IconTableColumn {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-table-column"
        width="62px"
        height="62px"
        viewBox="0 0 62 62"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon points="0 62 62 62 62 0 0 0"></polygon>
        </defs>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-125, -975)">
            <g transform="translate(125, 975)">
              <polygon
                fill="currentColor"
                points="6.64198453 55.6082341 9.25119781 55.6082341 9.25119781 27.0498293 6.64198453 27.0498293"></polygon>
              <g>
                <path
                  d="M12.1952751,20.6603863 L3.69765001,20.6603863 C1.80600253,20.6603863 0.27313795,22.1999334 0.27313795,24.0995254 L0.27313795,58.5606028 C0.27313795,60.4601948 1.80600253,61.9997419 3.69765001,61.9997419 L12.1952751,61.9997419 C14.0856367,61.9997419 15.6197872,60.4601948 15.6197872,58.5606028 L15.6197872,24.0995254 C15.6197872,22.1999334 14.0856367,20.6603863 12.1952751,20.6603863"
                  fill="currentColor"></path>
                <g></g>
                <polygon
                  fill="currentColor"
                  points="53.0214258 55.6082341 55.630639 55.6082341 55.630639 27.0498293 53.0214258 27.0498293"></polygon>
                <path
                  d="M58.5756165,20.6603863 L50.0779914,20.6603863 C48.1863439,20.6603863 46.6534794,22.1999334 46.6534794,24.0995254 L46.6534794,58.5606028 C46.6534794,60.4601948 48.1863439,61.9997419 50.0779914,61.9997419 L58.5756165,61.9997419 C60.4659781,61.9997419 62.0001286,60.4601948 62.0001286,58.5606028 L62.0001286,24.0995254 C62.0001286,22.1999334 60.4659781,20.6603863 58.5756165,20.6603863"
                  id="Fill-6"
                  fill="currentColor"></path>
                <polygon
                  fill="currentColor"
                  points="29.8317051 55.6082341 32.4409184 55.6082341 32.4409184 27.0498293 29.8317051 27.0498293"></polygon>
                <path
                  d="M35.3852529,20.6603863 L26.8876278,20.6603863 C24.9959803,20.6603863 23.4631158,22.1999334 23.4631158,24.0995254 L23.4631158,58.5606028 C23.4631158,60.4601948 24.9959803,61.9997419 26.8876278,61.9997419 L35.3852529,61.9997419 C37.2769004,61.9997419 38.809765,60.4601948 38.809765,58.5606028 L38.809765,24.0995254 C38.809765,22.1999334 37.2769004,20.6603863 35.3852529,20.6603863"
                  id="Fill-8"
                  fill="currentColor"></path>
                <path
                  d="M0.000771576131,7.75077429 L0.000771576131,7.70689784 C0.000771576131,7.70560736 -0.000514384087,7.70431688 -0.000514384087,7.70302639 L0.000771576131,7.75077429 Z"
                  fill="currentColor"></path>
                <path
                  d="M61.7253189,7.75077429 L61.7266049,7.70302639 C61.7266049,7.70431688 61.7253189,7.70560736 61.7253189,7.70689784 L61.7253189,7.75077429 Z"
                  fill="currentColor"></path>
                <path
                  d="M61.101371,6.10398801 L61.101371,6.09753559 L50.9487151,0.225834652 L50.9422853,0.229706103 C50.6876652,0.0877528932 50.3983241,9.16943835e-15 50.0858358,9.16943835e-15 C49.1110779,9.16943835e-15 48.3214984,0.789776039 48.3163545,1.7679627 L48.3150685,1.7679627 L48.3150685,4.62509366 L13.4115363,4.62509366 L13.4102503,1.7679627 C13.4051065,0.789776039 12.6155269,9.16943835e-15 11.6407691,9.16943835e-15 C11.3282808,9.16943835e-15 11.0389397,0.0877528932 10.7830336,0.229706103 L10.7766038,0.225834652 L0.625233858,6.09753559 L0.625233858,6.10398801 C0.254877315,6.2949796 0.000257192044,6.67696278 0.000257192044,7.12217967 L0.000257192044,7.7067688 C0.00282911248,8.1506952 0.256163275,8.53138789 0.625233858,8.721089 L0.625233858,8.72754142 L10.7766038,14.5992424 L10.7830336,14.5953709 C11.0389397,14.7373241 11.3282808,14.8263675 11.6407691,14.8263675 C12.6155269,14.8263675 13.4051065,14.035301 13.4102503,13.0584048 L13.4115363,13.0571143 L13.4115363,13.0506619 L13.4115363,10.2012738 L48.3150685,10.2012738 L48.3163545,13.0584048 C48.3214984,14.035301 49.1110779,14.8263675 50.0858358,14.8263675 C50.3983241,14.8263675 50.6876652,14.7373241 50.9422853,14.5953709 L50.9487151,14.5992424 L61.101371,8.72754142 L61.101371,8.721089 C61.4691556,8.53138789 61.7237757,8.1506952 61.7250617,7.7067688 L61.7263477,7.12217967 C61.7263477,6.67696278 61.4704416,6.2949796 61.101371,6.10398801"
                  id="Fill-11"
                  fill="currentColor"></path>
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
