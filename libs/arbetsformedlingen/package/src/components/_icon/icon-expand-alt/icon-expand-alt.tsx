import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-expand-alt',
  styleUrls: ['icon-expand-alt.scss'],
  scoped: true
})
export class IconExpandAlt {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-expand-alt"
        width="22"
        height="22"
        viewBox="0 0 22 22"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-expand-alt__shape"
          d="M10.444 13.913l-4.551 4.551 1.617 1.524c.742.743.216 2.012-.834 2.012h-5.5C.526 22 0 21.472 0 20.821v-5.5c0-1.05 1.267-1.575 2.01-.833l1.526 1.62 4.551-4.552a.786.786 0 011.111 0l1.246 1.246a.786.786 0 010 1.11zm1.112-5.826l4.551-4.551-1.617-1.524C13.748 1.269 14.274 0 15.324 0h5.5C21.474 0 22 .528 22 1.179v5.5c0 1.05-1.267 1.575-2.01.833l-1.526-1.62-4.551 4.552a.786.786 0 01-1.111 0l-1.246-1.246a.786.786 0 010-1.11z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
