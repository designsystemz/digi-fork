import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-wifi-signal1',
  styleUrls: ['icon-wifi-signal1.scss'],
  scoped: true
})
export class IconWifiSignal1 {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-wifi-signal1"
        width="62px"
        height="62px"
        viewBox="0 0 62 62"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g transform="translate(0, 0)">
          <path
            d="M7.83654167,47.1665 C3.514625,47.1665 0,43.6505833 0,39.3286667 L0,27.787625 C0,23.4657083 3.514625,19.9510833 7.83654167,19.9510833 C12.1584583,19.9510833 15.674375,23.4657083 15.674375,27.787625 L15.674375,39.3286667 C15.674375,43.6505833 12.1584583,47.1665 7.83654167,47.1665"
            fill="currentColor"></path>
          <path
            d="M29.1496875,12.7752292 C26.2524792,12.7752292 23.8951875,15.1325208 23.8951875,18.0297292 L23.8951875,39.3293125 C23.8951875,42.2265208 26.2524792,44.5825208 29.1496875,44.5825208 C32.0468958,44.5825208 34.4028958,42.2265208 34.4028958,39.3293125 L34.4028958,18.0297292 C34.4028958,15.1325208 32.0468958,12.7752292 29.1496875,12.7752292 M29.1496875,47.1658542 C24.8277708,47.1658542 21.3118542,43.6499375 21.3118542,39.3293125 L21.3118542,18.0297292 C21.3118542,13.7078125 24.8277708,10.1918958 29.1496875,10.1918958 C33.4716042,10.1918958 36.9862292,13.7078125 36.9862292,18.0297292 L36.9862292,39.3293125 C36.9862292,43.6499375 33.4716042,47.1658542 29.1496875,47.1658542"
            fill="currentColor"></path>
          <g transform="translate(42.8066, 0)">
            <path
              d="M7.83654167,2.58320417 C4.93933333,2.58320417 2.58333333,4.94049583 2.58333333,7.8364125 L2.58333333,39.3285375 C2.58333333,42.2257458 4.93933333,44.5830375 7.83654167,44.5830375 C10.73375,44.5830375 13.0910417,42.2257458 13.0910417,39.3285375 L13.0910417,7.8364125 C13.0910417,4.94049583 10.73375,2.58320417 7.83654167,2.58320417 M7.83654167,47.1663708 C3.514625,47.1663708 0,43.6504542 0,39.3285375 L0,7.8364125 C0,3.5157875 3.514625,-0.000129166667 7.83654167,-0.000129166667 C12.1584583,-0.000129166667 15.674375,3.5157875 15.674375,7.8364125 L15.674375,39.3285375 C15.674375,43.6504542 12.1584583,47.1663708 7.83654167,47.1663708"
              fill="currentColor"></path>
          </g>
        </g>
      </svg>
    );
  }
}
