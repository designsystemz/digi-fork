import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-upload',
  styleUrl: 'icon-upload.scss',
  scoped: true
})
export class IconUpload {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-upload"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-upload__shape"
          d="M15.66675,33 L15.66675,34.0725 C15.66675,37.25025 17.8515,38.208 19.01925,38.208 L28.971,38.208 C30.8475,38.14575 32.25,37.2645 32.25,34.0725 L32.25,33 L45.75,33 C46.9965,33 48,34.00275 48,35.25 L48,45.75 C48,46.9965 46.9965,48 45.75,48 L2.25,48 C1.00275,48 0,46.9965 0,45.75 L0,35.25 C0,34.00275 1.00275,33 2.25,33 L15.66675,33 Z M41,41 C39.9,41 39,41.9 39,43 C39,44.1 39.9,45 41,45 C42.1,45 43,44.1 43,43 C43,41.9 42.1,41 41,41 Z M35,41 C33.9,41 33,41.9 33,43 C33,44.1 33.9,45 35,45 C36.1,45 37,44.1 37,43 C37,41.9 36.1,41 35,41 Z M22.7062855,0.527625824 C23.4090366,-0.175875275 24.5625384,-0.175875275 25.2652895,0.527625824 L39.5438118,14.7956481 C40.7250636,15.9769 39.8903123,17.9929031 38.2223097,17.9929031 L29.9902969,17.9929031 L29.9902969,33.7429277 C29.9902969,34.9901797 28.9875453,35.9929312 27.7402933,35.9929312 L20.2402816,35.9929312 C18.9937797,35.9929312 17.9902781,34.9901797 17.9902781,33.7429277 L17.9902781,17.9929031 L9.76876526,17.9929031 C8.10001266,17.9929031 7.26526135,15.9769 8.4465132,14.7956481 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
