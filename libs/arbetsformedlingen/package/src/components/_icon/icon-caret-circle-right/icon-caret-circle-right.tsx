import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-caret-circle-right',
  styleUrls: ['icon-caret-circle-right.scss'],
  scoped: true
})
export class IconCaretCircleRight {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-caret-circle-right"
        width="25"
        height="25"
        viewBox="0 0 25 25"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-caret-circle-right__shape"
          d="M10.484 17.485v-9.97c0-.54.655-.811 1.033-.428l4.955 4.985a.6.6 0 010 .851l-4.955 4.985c-.378.388-1.033.116-1.033-.423zM12.5 0C19.405 0 25 5.595 25 12.5S19.405 25 12.5 25 0 19.405 0 12.5 5.595 0 12.5 0zm0 2.42C6.93 2.42 2.42 6.93 2.42 12.5S6.93 22.58 12.5 22.58s10.08-4.51 10.08-10.08S18.07 2.42 12.5 2.42z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
