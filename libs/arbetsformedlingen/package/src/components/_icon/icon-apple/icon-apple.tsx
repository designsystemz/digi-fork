import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-apple',
  styleUrl: 'icon-apple.scss',
  scoped: true
})
export class IconApple {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-apple"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        width="74px"
        height="88px"
        viewBox="0 0 74 88"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        <path d="M44.4925393,10.5591508 C48.5967007,5.84030272 48.2208617,1.54382067 48.1005932,0 C44.4775057,0.203900843 40.2831429,2.38855273 37.8928072,5.08295673 C35.2619344,7.96669722 33.7134779,11.534962 34.0442162,15.5547215 C37.9679749,15.8460084 41.5459618,13.894386 44.4925393,10.5591508 L44.4925393,10.5591508 Z M53.0444563,35.5183715 C53.0143464,29.8745684 55.5134609,25.6148042 60.5719093,22.4776493 C57.741587,18.3409107 53.4659936,16.0649356 47.8204038,15.6189675 C42.4759122,15.1883777 36.6346086,18.8022571 34.4968119,18.8022571 C32.238576,18.8022571 27.0596883,15.7727496 22.9948636,15.7727496 C14.594226,15.9111535 5.66666667,22.6160532 5.66666667,36.2565256 C5.66666667,40.2856167 6.38930216,44.4480369 7.83457315,48.7436323 C9.76160113,54.3874354 16.7169678,68.2278246 23.9734325,67.9971515 C27.7672689,67.9048822 30.4470422,65.2444518 35.3850514,65.2444518 C40.1725115,65.2444518 42.656571,67.9971515 46.8869996,67.9971515 C54.203684,67.889504 60.4966348,55.3101281 62.3333333,49.6509467 C52.5175345,44.9298362 53.0444563,35.8105575 53.0444563,35.5183715 Z"></path>
      </svg>
    );
  }
}
