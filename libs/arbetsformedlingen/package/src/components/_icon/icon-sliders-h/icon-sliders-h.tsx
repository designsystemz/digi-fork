import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-sliders-h',
  styleUrls: ['icon-sliders-h.scss'],
  scoped: true
})
export class IconSlidersH {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-sliders-h"
        width="29"
        height="22"
        viewBox="0 0 29 22"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-sliders-h__shape"
          d="M29 2.16v1.965c0 .324-.272.59-.604.59H8.056v.392c0 .653-.54 1.179-1.209 1.179h-.805c-.67 0-1.209-.526-1.209-1.179v-.393H.604A.599.599 0 010 4.125V2.161c0-.324.272-.59.604-.59h4.23V1.18C4.833.525 5.371 0 6.041 0h.805c.67 0 1.209.525 1.209 1.179v.392h20.34c.332 0 .604.266.604.59zm-.604 7.269h-4.23v-.393c0-.653-.538-1.179-1.208-1.179h-.805c-.67 0-1.209.526-1.209 1.179v.393H.604a.599.599 0 00-.604.589v1.964c0 .324.272.59.604.59h20.34v.392c0 .653.54 1.179 1.209 1.179h.805c.67 0 1.209-.526 1.209-1.179v-.393h4.229a.599.599 0 00.604-.589v-1.964a.599.599 0 00-.604-.59zm0 7.857H14.5v-.393c0-.653-.539-1.179-1.208-1.179h-.806c-.67 0-1.208.526-1.208 1.179v.393H.604a.599.599 0 00-.604.589v1.964c0 .324.272.59.604.59h10.674v.392c0 .654.538 1.179 1.208 1.179h.806c.67 0 1.208-.525 1.208-1.179v-.392h13.896a.599.599 0 00.604-.59v-1.964a.599.599 0 00-.604-.59z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
