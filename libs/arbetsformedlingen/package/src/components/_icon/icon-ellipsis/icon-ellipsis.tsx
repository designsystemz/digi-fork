import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-ellipsis',
  styleUrl: 'icon-ellipsis.scss',
  scoped: true
})
export class IconEllipsis {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-ellipsis"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-ellipsis__shape"
          d="M28.9655172,23.9655172 C28.9655172,26.7068966 26.5560345,28.9310345 23.5862069,28.9310345 C20.6163793,28.9310345 18.2068966,26.7068966 18.2068966,23.9655172 C18.2068966,21.2241379 20.6163793,19 23.5862069,19 C26.5560345,19 28.9655172,21.2241379 28.9655172,23.9655172 Z M42.6206897,19 C39.6508621,19 37.2413793,21.2241379 37.2413793,23.9655172 C37.2413793,26.7068966 39.6508621,28.9310345 42.6206897,28.9310345 C45.5905172,28.9310345 48,26.7068966 48,23.9655172 C48,21.2241379 45.5905172,19 42.6206897,19 Z M5.37931034,19 C2.40948276,19 0,21.2241379 0,23.9655172 C0,26.7068966 2.40948276,28.9310345 5.37931034,28.9310345 C8.34913793,28.9310345 10.7586207,26.7068966 10.7586207,23.9655172 C10.7586207,21.2241379 8.34913793,19 5.37931034,19 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
