import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-file-powerpoint',
  styleUrl: 'icon-file-powerpoint.scss',
  scoped: true
})
export class IconFilePowerpoint {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-file-powerpoint"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-file-powerpoint__shape"
          d="M24.4015,25.425 C25.2375,25.425 25.874,25.678125 26.33,26.184375 C27.242,27.20625 27.261,29.25 26.311,30.31875 C25.8455,30.84375 25.1805,31.115625 24.3065,31.115625 L21.751,31.115625 L21.751,25.425 L24.4015,25.425 L24.4015,25.425 Z M41.815,9.84375 L32.505,0.65625 C32.0775,0.234375 31.498,0 30.89,0 L30.32,0 L30.32,12 L42.48,12 L42.48,11.428125 C42.48,10.8375 42.2425,10.265625 41.815,9.84375 Z M27.28,12.75 L27.28,0 L8.28,0 C7.0165,0 6,1.003125 6,2.25 L6,45.75 C6,46.996875 7.0165,48 8.28,48 L40.2,48 C41.4635,48 42.48,46.996875 42.48,45.75 L42.48,15 L29.56,15 C28.306,15 27.28,13.9875 27.28,12.75 Z M32.315,28.2375 C32.315,36.703125 23.879,35.5125 21.7605,35.5125 L21.7605,40.875 C21.7605,41.49375 21.2475,42 20.6205,42 L17.6945,42 C17.0675,42 16.5545,41.49375 16.5545,40.875 L16.5545,22.14375 C16.5545,21.525 17.0675,21.01875 17.6945,21.01875 L25.3895,21.01875 C29.617,21.01875 32.315,24.09375 32.315,28.2375 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
