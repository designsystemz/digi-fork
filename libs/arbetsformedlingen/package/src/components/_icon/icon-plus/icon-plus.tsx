import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-plus',
  styleUrls: ['icon-plus.scss'],
  scoped: true
})
export class IconPlus {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-plus"
        width="22"
        height="26"
        viewBox="0 0 22 26"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-plus__shape"
          d="M21.6,13c0,0.7-0.5,1.2-1.2,1.2h-7.9V22c0,0.7-0.5,1.2-1.2,1.2s-1.2-0.5-1.2-1.2v-7.9H2.3c-0.7,0-1.2-0.5-1.2-1.2 c0-0.6,0.5-1.2,1.2-1.2h7.9V4c0-0.7,0.5-1.2,1.2-1.2s1.2,0.5,1.2,1.2v7.9h7.9C21.1,11.8,21.6,12.4,21.6,13z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
