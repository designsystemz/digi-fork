import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-online-computervideo',
  styleUrls: ['icon-online-computervideo.scss'],
  scoped: true
})
export class IconOnlineComputerVideo {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-online-computervideo"
        width="62px"
        height="63px"
        viewBox="0 0 62 63"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon points="0 0 62 0 62 9.21298912 0 9.21298912" />
        </defs>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-712, -890)">
            <g transform="translate(712, 897.1169)">
              <g transform="translate(0, 0)">
                <path
                  d="M8.78313943,7.5511486 C8.78313943,6.84208031 9.3379027,6.26282418 10.0151303,6.26282418 L51.9848697,6.26282418 C52.6620973,6.26282418 53.2168606,6.84208031 53.2168606,7.5511486 L53.2168606,34.4369605 L58.8881822,34.4369605 L58.8881822,2.89872993 C58.8881822,1.30424477 57.6414956,0 56.1168151,0 L5.88318486,0 C4.35850436,0 3.1118178,1.30424477 3.1118178,2.89872993 L3.1118178,34.4369605 L8.78313943,34.4369605 L8.78313943,7.5511486 Z"
                  fill="currentColor"
                />
                <g transform="translate(0, 39.3695)">
                  <mask fill="white">
                    <use href="#path-1" />
                  </mask>
                  <g />
                  <path
                    d="M43.6913426,0 C39.7639125,1.36670156 37.9232741,3.2171371 31.0003674,3.2171371 C24.0774606,3.2171371 22.2368222,1.36670156 18.3093922,0 L-0.000244928595,0 L-0.000244928595,4.37809864 C-0.000244928595,7.03802319 1.44115986,9.21298912 4.70850732,9.21298912 L57.2922275,9.21298912 C60.5595749,9.21298912 62.0009797,7.03802319 62.0009797,4.37809864 L62.0009797,0 L43.6913426,0 Z"
                    fill="currentColor"
                  />
                </g>
                <path
                  d="M36.5641654,23.7988544 L27.4993581,29.0329785 C27.348727,29.1199281 27.1809509,29.1627906 27.0131748,29.1627906 C26.8466233,29.1627906 26.6788473,29.1199281 26.5282162,29.0329785 C26.2281786,28.8590791 26.0432576,28.5394473 26.0432576,28.1928734 L26.0432576,17.7246252 C26.0432576,17.3780512 26.2281786,17.0571948 26.5282162,16.8845201 C26.8294783,16.7106208 27.1980959,16.7106208 27.4993581,16.8845201 L36.5641654,22.1186442 C36.8642029,22.2913189 37.049124,22.6121753 37.049124,22.9587493 C37.049124,23.3053232 36.8642029,23.6249551 36.5641654,23.7988544 M44.6002726,11.75694 L17.3997274,11.75694 C16.3306141,11.75694 15.4549944,12.6325597 15.4549944,13.701673 L15.4549944,32.3088984 C15.4549944,33.3780117 16.3306141,34.2524068 17.3997274,34.2524068 L44.6002726,34.2524068 C45.6706105,34.2524068 46.5450056,33.3780117 46.5450056,32.3088984 L46.5450056,13.701673 C46.5450056,12.6325597 45.6706105,11.75694 44.6002726,11.75694"
                  fill="currentColor"
                />
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
