import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-caret-right',
  styleUrls: ['icon-caret-right.scss'],
  scoped: true
})
export class IconCaretRight {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-caret-right"
        width="6"
        height="13"
        viewBox="0 0 6 13"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-caret-right__shape"
          d="M5.802 6.33A.768.768 0 016 6.857a.768.768 0 01-.198.527l-4.667 5.25a.608.608 0 01-.468.223.608.608 0 01-.47-.223.768.768 0 01-.197-.527v-10.5c0-.203.066-.379.198-.527A.608.608 0 01.667.857c.18 0 .336.074.468.223l4.667 5.25z"
          fill="currentColor"
          fill-rule="evenodd"
        />
      </svg>
    );
  }
}
