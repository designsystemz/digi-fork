import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-file-governing',
  styleUrl: 'icon-file-governing.scss',
  scoped: true
})
export class IconFileGoverning {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-file-governing"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g stroke="none" stroke-width="1" fill-rule="evenodd">
          <g transform="translate(0.000000, -0.000000)">
            <g transform="translate(14.832626, 0.000000)">
              <g />
              <path
                class="digi-icon-file-governing__shape"
                d="M32.9062069,20.1667043 L28.9253056,16.185803 C28.5601783,15.8206758 27.9425047,15.8409606 27.5540499,16.2294154 L26.8461087,16.9373566 L16.2300338,6.32128176 L16.937975,5.61334058 C17.3264298,5.22488576 17.3467147,4.60721215 16.9815874,4.2420849 L13.0006861,0.261183607 C12.6355589,-0.103943645 12.0188995,-0.0836587979 11.6294305,0.305810271 L0.305414418,11.6288121 C-0.0840546513,12.0182811 -0.103325256,12.6359547 0.260787754,13.001082 L4.24270329,16.9819833 C4.6068163,17.3471105 5.2244899,17.3268257 5.61395897,16.9373566 L6.3208859,16.2294154 L16.937975,26.8465046 L16.2300338,27.5534315 C15.8405648,27.9429006 15.8202799,28.5605742 16.1854072,28.9246872 L20.1663085,32.9066027 C20.5314357,33.2707157 21.1491093,33.2514451 21.5385784,32.861976 L32.8615802,21.53796 C33.2510493,21.1484909 33.2713341,20.5318316 32.9062069,20.1667043"
              />
            </g>
            <path
              class="digi-icon-file-governing__shape"
              d="M25.4475096,26.7459931 L27.0997104,25.0065675 L26.7974661,24.7043233 L23.2963015,21.2031586 L22.9940573,20.9009144 L21.2536174,22.5521009 C18.3691121,25.3301108 13.9936705,25.5603438 12.3505979,27.1780604 L1.88361662,37.4766774 C-0.43595568,39.7587227 -0.641846881,43.3501549 1.42517907,45.4587648 L2.54084567,46.5744314 C4.64945555,48.6414574 8.24088778,48.4355662 10.5229331,46.1170081 L20.8225644,35.6490126 C22.4392667,34.00594 22.6705139,29.6304984 25.4475096,26.7459931"
            />
          </g>
        </g>
      </svg>
    );
  }
}
