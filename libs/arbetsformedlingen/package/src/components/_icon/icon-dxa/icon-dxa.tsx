import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-dxa',
  styleUrls: ['icon-dxa.scss'],
  scoped: true
})
export class IconDxa {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-dxa"
        width="24"
        height="24"
        viewBox="0 0 24 24"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g class="digi-icon-dxa__shape" fill="currentColor" fill-rule="evenodd">
          <path d="M11.882 22.032c-.538.013-1.239.022-2.152.022-3.892 0-9.081-.649-9.081-.649S0 13.622 0 9.73C0 5.838.649.649.649.649S4.54 0 9.729 0c5.19 0 9.082.649 9.082.649s.648 5.189.648 9.08c0 1.064-.02 2.032-.05 2.877a6.486 6.486 0 00-1.895-.282 6.46 6.46 0 00-2.831.649H3.892v1.297h8.99c-.381.388-.713.824-.987 1.298H3.892v1.297h7.432a6.484 6.484 0 00-.297 1.946c0 1.172.311 2.272.855 3.221zm-1.504-14.41c0-.504.973-.811.973-2.086 0-.995-.648-1.644-1.621-1.644s-1.622.649-1.622 1.644c0 1.275.973 1.58.973 2.086 0 .162-.324.324-.649.486-1.297.649-1.621.973-1.621 1.654 0 .308 1.721.616 2.919.616 1.185 0 2.919-.308 2.919-.616 0-.68-.325-1.005-1.622-1.654-.324-.162-.649-.324-.649-.486z" />
          <path d="M21.867 17.082a4.54 4.54 0 11-.716-1.23l1.67-1.67.973.973-1.927 1.927zm0 0a4.536 4.536 0 00-.717-1.23l-3.843 3.843-1.946-1.946-.973.973 2.92 2.92 4.559-4.56z" />
        </g>
      </svg>
    );
  }
}
