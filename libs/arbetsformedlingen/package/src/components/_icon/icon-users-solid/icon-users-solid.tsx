import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-users-solid',
  styleUrls: ['icon-users-solid.scss'],
  scoped: true
})
export class IconUsersSolid {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-users-solid"
        width="32"
        height="19"
        viewBox="0 0 32 19"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-users-solid__shape"
          d="M11 4.948C11 2.215 13.239 0 16 0s5 2.215 5 4.948c0 2.733-2.239 4.948-5 4.948s-5-2.215-5-4.948zM2.4 7.125c0-2.186 1.79-3.958 4-3.958s4 1.772 4 3.958-1.79 3.958-4 3.958-4-1.772-4-3.958zm19.2 0c0-2.186 1.79-3.958 4-3.958s4 1.772 4 3.958-1.79 3.958-4 3.958-4-1.772-4-3.958zm-1.1 3.76c1.933 0 3.5 1.551 3.5 3.464v3.463c0 .656-.537 1.188-1.2 1.188H9.2c-.663 0-1.2-.532-1.2-1.188V14.35c0-1.913 1.567-3.464 3.5-3.464h.892a7.065 7.065 0 007.216 0h.892zM6.4 14.35c0-.593.104-1.161.294-1.69a5.643 5.643 0 01-3.18-.784H2.8c-1.546 0-2.8 1.24-2.8 2.77v1.584c0 .656.537 1.188 1.2 1.188h5.2v-3.068zm22.8-2.474h-.714c-.975.58-2.084.84-3.18.784.195.542.294 1.114.294 1.69v3.068h5.2c.663 0 1.2-.532 1.2-1.188v-1.583c0-1.53-1.254-2.771-2.8-2.771z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
