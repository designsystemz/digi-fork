import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-file-pdf',
  styleUrls: ['icon-file-pdf.scss'],
  scoped: true
})
export class IconFilePdf {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-file-pdf"
        width="19"
        height="26"
        viewBox="0 0 19 26"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-file-pdf__shape"
          d="M9 13.005c-.247-.812-.242-2.382-.099-2.382.416 0 .376 1.874.1 2.382zm-.084 2.397c-.38 1.026-.856 2.199-1.405 3.184.905-.356 1.93-.873 3.112-1.112-.628-.488-1.232-1.188-1.707-2.072zM4.26 21.739c0 .041.653-.274 1.727-2.041-.332.32-1.44 1.244-1.727 2.041zm8.01-13.614H19v16.656c0 .676-.53 1.219-1.188 1.219H1.188C.53 26 0 25.457 0 24.781V1.22C0 .543.53 0 1.188 0h9.895v6.906c0 .67.535 1.219 1.188 1.219zm-.395 8.724c-.99-.62-1.648-1.472-2.113-2.732.223-.94.574-2.366.307-3.26-.233-1.493-2.098-1.346-2.365-.345-.247.929-.02 2.24.4 3.91-.573 1.401-1.42 3.28-2.018 4.357-.005 0-.005.005-.01.005-1.34.706-3.642 2.26-2.697 3.453.278.35.792.508 1.064.508.886 0 1.767-.914 3.023-3.138 1.277-.432 2.677-.97 3.91-1.178 1.073.599 2.33.99 3.166.99 1.445 0 1.544-1.625.975-2.204-.688-.69-2.687-.493-3.642-.366zm6.779-11.517L13.804.355a1.171 1.171 0 00-.84-.355h-.297v6.5H19v-.31c0-.32-.124-.63-.346-.858zm-3.667 12.964c.203-.137-.123-.604-2.117-.457 1.835.803 2.117.457 2.117.457z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
