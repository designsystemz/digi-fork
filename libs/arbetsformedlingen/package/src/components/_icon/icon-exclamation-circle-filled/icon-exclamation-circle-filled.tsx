import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-exclamation-circle-filled',
  styleUrls: ['icon-exclamation-circle-filled.scss'],
  scoped: true
})
export class IconExclamationCircleFilled {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-exclamation-circle-filled"
        width="25"
        height="25"
        viewBox="0 0 25 25"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-exclamation-circle-filled__shape"
          d="M25 12.5C25 19.405 19.403 25 12.5 25S0 19.405 0 12.5C0 5.599 5.597 0 12.5 0S25 5.599 25 12.5zm-12.5 2.52a2.319 2.319 0 100 4.637 2.319 2.319 0 000-4.637zm-2.201-8.334l.374 6.855c.017.32.282.572.604.572h2.446a.605.605 0 00.604-.572l.374-6.855a.605.605 0 00-.604-.638h-3.194a.605.605 0 00-.604.638z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
