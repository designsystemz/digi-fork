import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-sort-up',
  styleUrl: 'icon-sort-up.scss',
  scoped: true
})
export class IconSortUp {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-sort-up"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-sort-up__shape"
          d="M27.5652174,39.5652174 L22,39.5652174 C21.2316038,39.5652174 20.6086957,40.2659891 20.6086957,41.1304348 L20.6086957,44.2608696 C20.6086957,45.1253153 21.2316038,45.826087 22,45.826087 L27.5652174,45.826087 C28.3336136,45.826087 28.9565217,45.1253153 28.9565217,44.2608696 L28.9565217,41.1304348 C28.9565217,40.2659891 28.3336136,39.5652174 27.5652174,39.5652174 Z M1.3048094,14.5212209 L5.99991802,14.5212209 L5.99991802,44.2608437 C5.99991802,45.1253037 6.70060859,45.826087 7.56495422,45.826087 L10.6950266,45.826087 C11.5593723,45.826087 12.2600628,45.1253037 12.2600628,44.2608437 L12.2600628,14.5212209 L16.9551715,14.5212209 C18.3451192,14.5212209 19.0464511,12.8346713 18.0614564,11.8495463 L10.2362754,2.45808647 C9.62517381,1.84730451 8.63480704,1.84730451 8.02370546,2.45808647 L0.198524432,11.8495463 C-0.783535786,12.8327147 -0.087094675,14.5212209 1.3048094,14.5212209 Z M39.9875776,14.5217391 L22.0993789,14.5217391 C21.2760973,14.5217391 20.6086957,15.2225108 20.6086957,16.0869565 L20.6086957,19.2173913 C20.6086957,20.081837 21.2760973,20.7826087 22.0993789,20.7826087 L39.9875776,20.7826087 C40.8108593,20.7826087 41.4782609,20.081837 41.4782609,19.2173913 L41.4782609,16.0869565 C41.4782609,15.2225108 40.8108593,14.5217391 39.9875776,14.5217391 Z M33.7565217,27.0434783 L22.0695652,27.0434783 C21.2627492,27.0434783 20.6086957,27.74425 20.6086957,28.6086957 L20.6086957,31.7391304 C20.6086957,32.6035761 21.2627492,33.3043478 22.0695652,33.3043478 L33.7565217,33.3043478 C34.5633377,33.3043478 35.2173913,32.6035761 35.2173913,31.7391304 L35.2173913,28.6086957 C35.2173913,27.74425 34.5633377,27.0434783 33.7565217,27.0434783 Z M46.2318841,2 L22.115942,2 C21.2835128,2 20.6086957,2.7007717 20.6086957,3.56521739 L20.6086957,6.69565217 C20.6086957,7.56009787 21.2835128,8.26086957 22.115942,8.26086957 L46.2318841,8.26086957 C47.0643132,8.26086957 47.7391304,7.56009787 47.7391304,6.69565217 L47.7391304,3.56521739 C47.7391304,2.7007717 47.0643132,2 46.2318841,2 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
