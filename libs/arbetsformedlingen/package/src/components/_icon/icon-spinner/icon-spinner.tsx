import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-spinner',
  styleUrls: ['icon-spinner.scss'],
  scoped: true
})
export class IconSpinner {
  _path =
    'M25 42c9.389 0 17-7.611 17-17S34.389 8 25 8 8 15.611 8 25s7.611 17 17 17zm0 8C11.193 50 0 38.807 0 25S11.193 0 25 0s25 11.193 25 25-11.193 25-25 25z';

  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-spinner"
        height="50"
        viewBox="0 0 50 50"
        width="50"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g class="digi-icon-spinner__shape" fill="none" fill-rule="evenodd">
          <mask fill="#fff" id="iconSpinnerPath">
            <path d={this._path} />
          </mask>
          <path d={this._path} fill="#D0CFE1" fill-rule="nonzero" />
          <path
            d="M19 26L54-5v62z"
            fill="currentColor"
            mask="url(#iconSpinnerPath)"
          />
        </g>
      </svg>
    );
  }
}
