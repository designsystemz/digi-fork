import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-archive-outline',
  styleUrl: 'icon-archive-outline.scss',
  scoped: true
})
export class IconArchiveOutline {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-archive-outline"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-archive-outline__shape"
          d="M43.5,3 L4.5,3 C2.015625,3 0,5.015625 0,7.5 L0,15 C0,15.825 0.675,16.5 1.5,16.5 L3,16.5 L3,42 C3,43.659375 4.340625,45 6,45 L42,45 C43.659375,45 45,43.659375 45,42 L45,16.5 L46.5,16.5 C47.325,16.5 48,15.825 48,15 L48,7.5 C48,5.015625 45.984375,3 43.5,3 Z M40.5,40.5 L7.5,40.5 L7.5,16.5 L40.5,16.5 L40.5,40.5 Z M43.5,12 L4.5,12 L4.5,7.5 L43.5,7.5 L43.5,12 Z M19.125,25.5 L28.875,25.5 C29.49375,25.5 30,24.99375 30,24.375 L30,22.125 C30,21.50625 29.49375,21 28.875,21 L19.125,21 C18.50625,21 18,21.50625 18,22.125 L18,24.375 C18,24.99375 18.50625,25.5 19.125,25.5 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
