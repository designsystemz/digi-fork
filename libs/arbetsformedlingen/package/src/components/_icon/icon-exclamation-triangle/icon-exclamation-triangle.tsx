import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-exclamation-triangle',
  styleUrls: ['icon-exclamation-triangle.scss'],
  scoped: true
})
export class IconExclamationTriangle {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-exclamation-triangle"
        width="44"
        height="38"
        viewBox="0 0 44 38"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g class="digi-icon-exclamation-triangle__shape" fill="currentColor">
          <path
            d="M20.417 3.941L3.35 31.235s-.787 1.777 0 2.771S4.985 35 4.985 35h33.93s.846 0 1.718-.994c.826-.941 0-2.77 0-2.77L23.583 3.94C23.27 3.418 22.792 3 22 3c-.792 0-1.27.418-1.583.941zM22 0c1.853 0 3.283.943 4.128 2.352l17.156 27.465.083.184c.097.214.212.513.323.881A6.84 6.84 0 0144 32.92c-.01 1.113-.333 2.178-1.113 3.065C41.62 37.43 40.151 38 38.914 38H4.977c-.17-.005-.17-.005-.243-.01a4.78 4.78 0 01-1.237-.273 5.57 5.57 0 01-2.201-1.499 6.453 6.453 0 01-.298-.35c-.982-1.24-1.154-2.662-.9-4.132a7.384 7.384 0 01.509-1.716l.086-.195L17.861 2.37C18.735.93 20.159 0 22 0z"
            fill-rule="nonzero"
          />
          <path d="M19 13l.75 11h4.5L25 13zM25 28c0-1.645-1.133-3-3-3s-3 3-3 3 1.133 3 3 3 3-1.355 3-3z" />
        </g>
      </svg>
    );
  }
}
