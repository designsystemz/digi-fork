import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-heart',
  styleUrl: 'icon-heart.scss',
  scoped: true
})
export class IconHeart {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-heart"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-heart__shape"
          d="M21.2090909,43.4703308 L6.22727273,28.9641289 C5.58181818,28.372405 0,23.0917172 0,16.5199953 C0,8.388274 5.16363636,3 13.8181818,3 C17.5818182,3 21.1454545,5.04413722 24,7.46482604 C26.8454545,5.04413722 30.4181818,3 34.1818182,3 C42.5181818,3 48,8.06551549 48,16.5199953 C48,21.1820627 45.1090909,25.7993024 41.8090909,28.9551634 L41.7727273,28.9910255 L26.7909091,43.4703308 C25.2437253,44.9765564 22.7562747,44.9765564 21.2090909,43.4703308 Z M9.26363636,25.9473339 L24,40.2210526 L38.7090909,25.9921635 C41.1909091,23.5713695 43.6363636,20.0477693 43.6363636,16.5958964 C43.6363636,10.5977068 40.1636364,7.37894737 34.1818182,7.37894737 C29.8909091,7.37894737 25.7454545,11.7991379 24,13.5116255 C22.4545455,11.9874219 18.1818182,7.37894737 13.8181818,7.37894737 C7.82727273,7.37894737 4.36363636,10.5977068 4.36363636,16.5958964 C4.36363636,19.9401785 6.79090909,23.6699944 9.26363636,25.9473339 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
