import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-wifi-signal0',
  styleUrls: ['icon-wifi-signal0.scss'],
  scoped: true
})
export class IconWifiSignal0 {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-wifi-signal0"
        width="62px"
        height="62px"
        viewBox="0 0 62 62"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g transform="translate(0, 0)">
          <path
            d="M7.83654167,22.5340292 C4.93933333,22.5340292 2.58333333,24.8913208 2.58333333,27.7872375 L2.58333333,39.3282792 C2.58333333,42.2267792 4.93933333,44.5827792 7.83654167,44.5827792 C10.73375,44.5827792 13.0910417,42.2267792 13.0910417,39.3282792 L13.0910417,27.7872375 C13.0910417,24.8913208 10.73375,22.5340292 7.83654167,22.5340292 M7.83654167,47.1661125 C3.514625,47.1661125 0,43.6501958 0,39.3282792 L0,27.7872375 C0,23.4666125 3.514625,19.9506958 7.83654167,19.9506958 C12.1584583,19.9506958 15.674375,23.4666125 15.674375,27.7872375 L15.674375,39.3282792 C15.674375,43.6501958 12.1584583,47.1661125 7.83654167,47.1661125"
            fill="currentColor"></path>
          <path
            d="M29.1496875,12.7752292 C26.2524792,12.7752292 23.8951875,15.1325208 23.8951875,18.0297292 L23.8951875,39.3293125 C23.8951875,42.2265208 26.2524792,44.5825208 29.1496875,44.5825208 C32.0468958,44.5825208 34.4028958,42.2265208 34.4028958,39.3293125 L34.4028958,18.0297292 C34.4028958,15.1325208 32.0468958,12.7752292 29.1496875,12.7752292 M29.1496875,47.1658542 C24.8277708,47.1658542 21.3118542,43.6499375 21.3118542,39.3293125 L21.3118542,18.0297292 C21.3118542,13.7078125 24.8277708,10.1918958 29.1496875,10.1918958 C33.4716042,10.1918958 36.9862292,13.7078125 36.9862292,18.0297292 L36.9862292,39.3293125 C36.9862292,43.6499375 33.4716042,47.1658542 29.1496875,47.1658542"
            fill="currentColor"></path>
          <g transform="translate(42.8066, 0)">
            <path
              d="M7.83654167,2.58320417 C4.93933333,2.58320417 2.58333333,4.94049583 2.58333333,7.8364125 L2.58333333,39.3285375 C2.58333333,42.2257458 4.93933333,44.5830375 7.83654167,44.5830375 C10.73375,44.5830375 13.0910417,42.2257458 13.0910417,39.3285375 L13.0910417,7.8364125 C13.0910417,4.94049583 10.73375,2.58320417 7.83654167,2.58320417 M7.83654167,47.1663708 C3.514625,47.1663708 0,43.6504542 0,39.3285375 L0,7.8364125 C0,3.5157875 3.514625,-0.000129166667 7.83654167,-0.000129166667 C12.1584583,-0.000129166667 15.674375,3.5157875 15.674375,7.8364125 L15.674375,39.3285375 C15.674375,43.6504542 12.1584583,47.1663708 7.83654167,47.1663708"
              fill="currentColor"></path>
          </g>
        </g>
      </svg>
    );
  }
}
