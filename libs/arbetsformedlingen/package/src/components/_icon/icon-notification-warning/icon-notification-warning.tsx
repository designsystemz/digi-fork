import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-notification-warning',
  styleUrls: ['icon-notification-warning.scss'],
  scoped: true
})
export class IconExclamationCircle {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-notification-warning"
        width="42"
        height="42"
        viewBox="0 0 42 42"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g fill="currentColor" class="digi-icon-notification-warning__shape">
          <path
            d="M21 3C11.059 3 3 11.059 3 21s8.059 18 18 18 18-8.059 18-18S30.941 3 21 3zm0-3c11.598 0 21 9.402 21 21s-9.402 21-21 21S0 32.598 0 21 9.402 0 21 0z"
            fill-rule="nonzero"
          />
          <path d="M25 10l-.875 11h-5.25L18 10zM21.5 31c-2.178 0-3.5-1.58-3.5-3.5s1.322-3.5 3.5-3.5 3.5 1.58 3.5 3.5-1.322 3.5-3.5 3.5z" />
        </g>
      </svg>
    );
  }
}
