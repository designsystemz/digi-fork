import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-exclamation-triangle-filled',
  styleUrls: ['icon-exclamation-triangle-filled.scss'],
  scoped: true
})
export class IconExclamationTriangleFilled {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-exclamation-triangle-filled"
        width="29"
        height="26"
        viewBox="0 0 29 26"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-exclamation-triangle-filled__shape"
          d="M28.674 22.344C29.603 23.97 28.436 26 26.58 26H2.42C.56 26-.602 23.966.325 22.344l12.08-21.126c.93-1.625 3.26-1.623 4.187 0l12.08 21.126zM14.5 17.977a2.326 2.326 0 00-2.316 2.335 2.326 2.326 0 002.316 2.336 2.326 2.326 0 002.316-2.335 2.326 2.326 0 00-2.316-2.336zM12.301 9.58l.374 6.906a.606.606 0 00.603.576h2.444c.32 0 .586-.252.603-.576L16.7 9.58a.607.607 0 00-.603-.643h-3.192a.607.607 0 00-.603.643z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
