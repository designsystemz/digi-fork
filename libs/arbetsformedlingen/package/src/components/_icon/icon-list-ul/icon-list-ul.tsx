import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-list-ul',
  styleUrls: ['icon-list-ul.scss'],
  scoped: true
})
export class IconListUl {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-list-ul"
        width="48px"
        height="48px"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <path d="M0 5.333a5.333 5.333 0 1 0 10.666 0A5.333 5.333 0 0 0 0 5.333" />
          <path d="M0 5.334a5.333 5.333 0 0 0 5.334 5.332A5.333 5.333 0 0 0 5.334 0 5.335 5.335 0 0 0 0 5.334" />
          <path d="M0 5.333a5.333 5.333 0 1 0 10.666 0A5.333 5.333 0 0 0 0 5.333" />
        </defs>
        <g fill="none" fill-rule="evenodd">
          <path
            d="M44.952 13.762H18.286a3.048 3.048 0 1 1 0-6.096h26.666a3.048 3.048 0 1 1 0 6.096M5.334 15.666A5.333 5.333 0 1 0 5.334 5a5.333 5.333 0 0 0 0 10.666"
            fill="currentColor"
          />
          <path
            d="M44.952 27.476H18.286a3.048 3.048 0 1 1 0-6.096h26.666a3.048 3.048 0 1 1 0 6.096M5.334 29.38a5.333 5.333 0 1 0 0-10.666 5.333 5.333 0 0 0 0 10.666"
            fill="currentColor"
          />
          <path
            d="M44.952 41.19H18.286a3.048 3.048 0 1 1 0-6.096h26.666a3.048 3.048 0 1 1 0 6.096M5.334 43.094a5.333 5.333 0 1 0 0-10.666 5.333 5.333 0 0 0 0 10.666"
            fill="currentColor"
          />
        </g>
      </svg>
    );
  }
}
