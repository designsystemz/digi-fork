import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-table',
  styleUrl: 'icon-table.scss',
  scoped: true
})
export class IconTable {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-table"
        width="62px"
        height="62px"
        viewBox="0 0 62 62"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon points="0 62 62 62 62 0 0 0"></polygon>
        </defs>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-302, -975)">
            <g transform="translate(302, 975)">
              <polygon
                fill="currentColor"
                points="29.6502083 9.05264249 32.3497917 9.05264249 32.3497917 6.36704663 29.6502083 6.36704663"></polygon>
              <g>
                <path
                  d="M35.3073208,0 L26.6931958,0 C24.7918625,0 23.2496125,1.53297409 23.2496125,3.42574093 L23.2496125,11.9939482 C23.2496125,13.886715 24.7918625,15.4196891 26.6931958,15.4196891 L35.3073208,15.4196891 C37.2086542,15.4196891 38.7496125,13.886715 38.7496125,11.9939482 L38.7496125,3.42574093 C38.7496125,1.53297409 37.2086542,0 35.3073208,0"
                  fill="currentColor"></path>

                <polygon
                  fill="currentColor"
                  points="6.40020833 9.05264249 9.09979167 9.05264249 9.09979167 6.36704663 6.40020833 6.36704663"></polygon>
                <path
                  d="M12.0573208,0 L3.44319583,0 C1.5418625,0 -0.0003875,1.53297409 -0.0003875,3.42574093 L-0.0003875,11.9939482 C-0.0003875,13.886715 1.5418625,15.4196891 3.44319583,15.4196891 L12.0573208,15.4196891 C13.9586542,15.4196891 15.4996125,13.886715 15.4996125,11.9939482 L15.4996125,3.42574093 C15.4996125,1.53297409 13.9586542,0 12.0573208,0"
                  fill="currentColor"></path>
                <polygon
                  fill="currentColor"
                  points="52.9002083 9.05264249 55.5997917 9.05264249 55.5997917 6.36704663 52.9002083 6.36704663"></polygon>
                <path
                  d="M58.5573208,0 L49.9431958,0 C48.0418625,0 46.4996125,1.53297409 46.4996125,3.42574093 L46.4996125,11.9939482 C46.4996125,13.886715 48.0418625,15.4196891 49.9431958,15.4196891 L58.5573208,15.4196891 C60.4586542,15.4196891 61.9996125,13.886715 61.9996125,11.9939482 L61.9996125,3.42574093 C61.9996125,1.53297409 60.4586542,0 58.5573208,0"
                  fill="currentColor"></path>
                <polygon
                  fill="currentColor"
                  points="29.6502083 55.6329534 32.3497917 55.6329534 32.3497917 52.9473575 29.6502083 52.9473575"></polygon>
                <path
                  d="M35.3073208,46.5803109 L26.6931958,46.5803109 C24.7918625,46.5803109 23.2496125,48.113285 23.2496125,50.0060518 L23.2496125,58.5742591 C23.2496125,60.4670259 24.7918625,62 26.6931958,62 L35.3073208,62 C37.2086542,62 38.7496125,60.4670259 38.7496125,58.5742591 L38.7496125,50.0060518 C38.7496125,48.113285 37.2086542,46.5803109 35.3073208,46.5803109"
                  fill="currentColor"></path>
                <polygon
                  fill="currentColor"
                  points="6.40020833 55.6329534 9.09979167 55.6329534 9.09979167 52.9473575 6.40020833 52.9473575"></polygon>
                <path
                  d="M12.0573208,46.5803109 L3.44319583,46.5803109 C1.5418625,46.5803109 -0.0003875,48.113285 -0.0003875,50.0060518 L-0.0003875,58.5742591 C-0.0003875,60.4670259 1.5418625,62 3.44319583,62 L12.0573208,62 C13.9586542,62 15.4996125,60.4670259 15.4996125,58.5742591 L15.4996125,50.0060518 C15.4996125,48.113285 13.9586542,46.5803109 12.0573208,46.5803109"
                  fill="currentColor"></path>
                <polygon
                  fill="currentColor"
                  points="52.9002083 55.6329534 55.5997917 55.6329534 55.5997917 52.9473575 52.9002083 52.9473575"></polygon>
                <path
                  d="M58.5573208,46.5803109 L49.9431958,46.5803109 C48.0418625,46.5803109 46.4996125,48.113285 46.4996125,50.0060518 L46.4996125,58.5742591 C46.4996125,60.4670259 48.0418625,62 49.9431958,62 L58.5573208,62 C60.4586542,62 61.9996125,60.4670259 61.9996125,58.5742591 L61.9996125,50.0060518 C61.9996125,48.113285 60.4586542,46.5803109 58.5573208,46.5803109"
                  fill="currentColor"></path>
                <polygon
                  fill="currentColor"
                  points="29.6502083 32.3427979 32.3497917 32.3427979 32.3497917 29.6572021 29.6502083 29.6572021"></polygon>
                <path
                  d="M35.3073208,23.2901554 L26.6931958,23.2901554 C24.7918625,23.2901554 23.2496125,24.8231295 23.2496125,26.7158964 L23.2496125,35.2841036 C23.2496125,37.1768705 24.7918625,38.7098446 26.6931958,38.7098446 L35.3073208,38.7098446 C37.2086542,38.7098446 38.7496125,37.1768705 38.7496125,35.2841036 L38.7496125,26.7158964 C38.7496125,24.8231295 37.2086542,23.2901554 35.3073208,23.2901554"
                  fill="currentColor"></path>
                <polygon
                  fill="currentColor"
                  points="6.40020833 32.3427979 9.09979167 32.3427979 9.09979167 29.6572021 6.40020833 29.6572021"></polygon>
                <path
                  d="M12.0573208,23.2901554 L3.44319583,23.2901554 C1.5418625,23.2901554 -0.0003875,24.8231295 -0.0003875,26.7158964 L-0.0003875,35.2841036 C-0.0003875,37.1768705 1.5418625,38.7098446 3.44319583,38.7098446 L12.0573208,38.7098446 C13.9586542,38.7098446 15.4996125,37.1768705 15.4996125,35.2841036 L15.4996125,26.7158964 C15.4996125,24.8231295 13.9586542,23.2901554 12.0573208,23.2901554"
                  fill="currentColor"></path>
                <polygon
                  fill="currentColor"
                  points="52.9002083 32.3427979 55.5997917 32.3427979 55.5997917 29.6572021 52.9002083 29.6572021"></polygon>
                <path
                  d="M58.5573208,23.2901554 L49.9431958,23.2901554 C48.0418625,23.2901554 46.4996125,24.8231295 46.4996125,26.7158964 L46.4996125,35.2841036 C46.4996125,37.1768705 48.0418625,38.7098446 49.9431958,38.7098446 L58.5573208,38.7098446 C60.4586542,38.7098446 61.9996125,37.1768705 61.9996125,35.2841036 L61.9996125,26.7158964 C61.9996125,24.8231295 60.4586542,23.2901554 58.5573208,23.2901554"
                  fill="currentColor"></path>
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
