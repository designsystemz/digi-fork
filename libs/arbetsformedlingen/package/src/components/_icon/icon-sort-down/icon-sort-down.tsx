import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-sort-down',
  styleUrl: 'icon-sort-down.scss',
  scoped: true
})
export class IconSortDown {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-sort-down"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-sort-down__shape"
          d="M22.0952381,6.85714286 L28.1904762,6.85714286 C29.032053,6.85714286 29.7142857,6.089631 29.7142857,5.14285714 L29.7142857,1.71428571 C29.7142857,0.767511857 29.032053,0 28.1904762,0 L22.0952381,0 C21.2536613,0 20.5714286,0.767511857 20.5714286,1.71428571 L20.5714286,5.14285714 C20.5714286,6.089631 21.2536613,6.85714286 22.0952381,6.85714286 Z M22.1714286,20.5714286 L34.9714286,20.5714286 C35.8550842,20.5714286 36.5714286,19.8039167 36.5714286,18.8571429 L36.5714286,15.4285714 C36.5714286,14.4817976 35.8550842,13.7142857 34.9714286,13.7142857 L22.1714286,13.7142857 C21.287773,13.7142857 20.5714286,14.4817976 20.5714286,15.4285714 L20.5714286,18.8571429 C20.5714286,19.8039167 21.287773,20.5714286 22.1714286,20.5714286 Z M46.4761905,41.1428571 L22.0952381,41.1428571 C21.2536613,41.1428571 20.5714286,41.910369 20.5714286,42.8571429 L20.5714286,46.2857143 C20.5714286,47.2324881 21.2536613,48 22.0952381,48 L46.4761905,48 C47.3177672,48 48,47.2324881 48,46.2857143 L48,42.8571429 C48,41.910369 47.3177672,41.1428571 46.4761905,41.1428571 Z M22.2040816,34.2857143 L41.7959184,34.2857143 C42.6976078,34.2857143 43.4285714,33.5182024 43.4285714,32.5714286 L43.4285714,29.1428571 C43.4285714,28.1960833 42.6976078,27.4285714 41.7959184,27.4285714 L22.2040816,27.4285714 C21.3023922,27.4285714 20.5714286,28.1960833 20.5714286,29.1428571 L20.5714286,32.5714286 C20.5714286,33.5182024 21.3023922,34.2857143 22.2040816,34.2857143 Z M16.7609801,34.2862818 L12.1898238,34.2862818 L12.1898238,1.71431409 C12.1898238,0.767524562 11.5076316,0 10.666105,0 L7.61866741,0 C6.77714076,0 6.09494862,0.767524562 6.09494862,1.71431409 L6.09494862,34.2862818 L1.52379227,34.2862818 C0.172444172,34.2862818 -0.511324632,36.1334553 0.448618202,37.2124017 L8.06721212,47.4982862 C8.66218044,48.1672379 9.62640124,48.1672379 10.2213696,47.4982862 L17.8399635,37.2124017 C18.7941924,36.1355982 18.1151852,34.2862818 16.7609801,34.2862818 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
