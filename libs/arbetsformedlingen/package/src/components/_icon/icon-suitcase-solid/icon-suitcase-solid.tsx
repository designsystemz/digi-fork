import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-suitcase-solid',
  styleUrl: 'icon-suitcase-solid.scss',
  scoped: true
})
export class IconSuitcaseSolid {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-suitcase-solid"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-suitcase-solid__shape"
          d="M12,45.4615385 L36,45.4615385 L36,7.54945055 C36,5.03777473 33.984375,3 31.5,3 L16.5,3 C14.015625,3 12,5.03777473 12,7.54945055 L12,45.4615385 Z M18,9.06593407 L30,9.06593407 L30,12.0989011 L18,12.0989011 L18,9.06593407 Z M48,16.6483516 L48,40.9120879 C48,43.4237637 45.984375,45.4615385 43.5,45.4615385 L39,45.4615385 L39,12.0989011 L43.5,12.0989011 C45.984375,12.0989011 48,14.1366758 48,16.6483516 Z M9,45.4615385 L4.5,45.4615385 C2.015625,45.4615385 0,43.4237637 0,40.9120879 L0,16.6483516 C0,14.1366758 2.015625,12.0989011 4.5,12.0989011 L9,12.0989011 L9,45.4615385 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
