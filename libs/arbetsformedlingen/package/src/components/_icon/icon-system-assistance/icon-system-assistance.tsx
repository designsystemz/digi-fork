import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-system-assistance',
  styleUrl: 'icon-system-assistance.scss',
  scoped: true
})
export class IconSystemAssistance {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-system-assistance"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g stroke="none" stroke-width="1" fill-rule="evenodd">
          <g transform="translate(0.000000, 2.000000)">
            <g transform="translate(0.000000, 6.999600)">
              <g />
              <path
                class="digi-icon-system-assistance__shape"
                d="M40.4458,22.306 L40.4458,26.44 L3.5538,26.44 L3.5538,3.561 L20.2908,3.561 L19.2438,1.468 C18.9738,0.928 18.8208,0.443 18.7488,0 L2.1928,0 C0.9838,0 -0.0002,0.986 -0.0002,2.196 L-0.0002,27.804 C-0.0002,29.015 0.9838,30 2.1928,30 L19.9998,30 L19.9998,33 L8.4998,33 C7.3958,33 6.4998,33.896 6.4998,35 C6.4998,36.105 7.3958,37 8.4998,37 L35.4998,37 C36.6048,37 37.4998,36.105 37.4998,35 C37.4998,33.896 36.6048,33 35.4998,33 L23.9998,33 L23.9998,30 L41.8078,30 C43.0168,30 43.9998,29.015 43.9998,27.804 L43.9998,21.75 C43.4248,22.213 42.7428,22.579 41.8238,22.579 C41.3828,22.579 40.9328,22.488 40.4458,22.306"
              />
            </g>
            <path
              class="digi-icon-system-assistance__shape"
              d="M37.4907,17.455 C36.3667,20.168 33.2577,21.456 30.5447,20.332 C27.8327,19.209 26.5437,16.099 27.6677,13.386 C28.7917,10.674 31.9007,9.386 34.6137,10.509 C37.3257,11.633 38.6147,14.743 37.4907,17.455 M46.5077,12.934 L43.7997,12.031 C42.6587,11.232 42.9287,9.993 43.1817,9.341 L44.1867,7.33 C44.8907,5.924 44.4507,5.484 43.4837,4.517 C42.5157,3.549 42.0767,3.11 40.6697,3.813 L38.1167,5.09 C36.7447,5.331 36.0597,4.264 35.7767,3.625 L35.0667,1.492 C34.5687,0 33.9477,0 32.5797,0 C31.2107,0 30.5897,0 30.0917,1.492 L29.1897,4.2 C28.3907,5.341 27.1517,5.071 26.4997,4.818 L24.4887,3.813 C23.0817,3.11 22.6427,3.549 21.6747,4.517 C20.7077,5.484 20.2677,5.924 20.9717,7.33 L22.2477,9.883 C22.4897,11.255 21.4227,11.94 20.7827,12.223 L18.6507,12.934 C17.1587,13.431 17.1587,14.053 17.1587,15.421 C17.1587,16.789 17.1587,17.411 18.6507,17.908 L21.3587,18.81 C22.4997,19.609 22.2297,20.849 21.9767,21.5 L20.9717,23.511 C20.2677,24.918 20.7077,25.357 21.6747,26.325 C22.6427,27.292 23.0817,27.732 24.4887,27.028 L27.0417,25.752 C28.4137,25.51 29.0987,26.577 29.3817,27.217 L30.0917,29.349 C30.5897,30.842 31.2107,30.842 32.5797,30.842 C33.9477,30.842 34.5687,30.842 35.0667,29.349 L35.9687,26.642 C36.7677,25.5 38.0067,25.77 38.6587,26.023 L40.6697,27.028 C42.0767,27.732 42.5157,27.292 43.4837,26.325 C44.4507,25.357 44.8907,24.918 44.1867,23.511 L42.9107,20.958 C42.6687,19.587 43.7357,18.901 44.3757,18.619 L46.5077,17.908 C47.9997,17.411 47.9997,16.789 47.9997,15.421 C47.9997,14.053 47.9997,13.431 46.5077,12.934"
            />
          </g>
        </g>
      </svg>
    );
  }
}
