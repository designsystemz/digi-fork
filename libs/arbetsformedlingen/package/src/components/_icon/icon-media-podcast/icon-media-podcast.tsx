import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-media-podcast',
  styleUrl: 'icon-media-podcast.scss',
  scoped: true
})
export class IconMediaPodcast {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-media-podcast"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-media-podcast__shape"
          d="M24,3 C10.73625,3 0,13.5150306 0,26.5102041 L0,31.0757755 C0,31.8752729 0.441953143,32.6120853 1.15490625,33.0012245 L3.00196875,34.0093163 C3.085125,39.6197143 7.7529375,44.1428571 13.5,44.1428571 L15.75,44.1428571 C16.9926563,44.1428571 18,43.1560714 18,41.9387755 L18,25.7755102 C18,24.5582143 16.9926563,23.5714286 15.75,23.5714286 L13.5,23.5714286 C9.68025,23.5714286 6.3373125,25.5696122 4.5,28.5569694 L4.5,26.5102041 C4.5,15.9773571 13.2477188,7.40816327 24,7.40816327 C34.7522813,7.40816327 43.5,15.9773571 43.5,26.5102041 L43.5,28.5569694 C41.6626875,25.5696122 38.31975,23.5714286 34.5,23.5714286 L32.25,23.5714286 C31.0073438,23.5714286 30,24.5582143 30,25.7755102 L30,41.9387755 C30,43.1560714 31.0073438,44.1428571 32.25,44.1428571 L34.5,44.1428571 C40.2470625,44.1428571 44.914875,39.6197143 44.9980313,34.0092245 L46.8450938,33.0011327 C47.5580469,32.6119935 48,31.8751811 48,31.0756837 L48,26.5102041 C48,13.5171429 37.2659063,3 24,3 L24,3 Z M13.5,27.9795918 L13.5,39.7346939 C10.1915625,39.7346939 7.5,37.0980612 7.5,33.8571429 C7.5,30.6162245 10.1915625,27.9795918 13.5,27.9795918 Z M34.5,39.7346939 L34.5,27.9795918 C37.8084375,27.9795918 40.5,30.6162245 40.5,33.8571429 C40.5,37.0980612 37.8084375,39.7346939 34.5,39.7346939 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
