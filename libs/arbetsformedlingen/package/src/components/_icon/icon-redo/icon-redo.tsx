import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-redo',
  styleUrls: ['icon-redo.scss'],
  scoped: true
})
export class IconRedo {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-redo"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-redo__shape"
          d="M46.0175809,1.98241935 L40.9693558,7.03064516 C36.6266144,2.68712903 30.627583,0 24.0001,0 C10.7748438,0 -0.00967091015,10.7927419 0,24.0179032 C0.00968392594,37.2645484 10.7511341,48 24,48 C30.1863895,48 35.8260017,45.6593226 40.081743,41.8153548 C40.5765494,41.3684516 40.5997752,40.5997742 40.1283882,40.1282903 L38.2182594,38.2181613 C37.7829691,37.782871 37.0808724,37.7550968 36.6213886,38.1647419 C33.2697117,41.1528387 28.8488736,42.9677419 24,42.9677419 C13.5173273,42.9677419 5.03226389,34.4843226 5.03226389,24 C5.03226389,13.5173226 13.5156821,5.03225806 24,5.03225806 C29.2415832,5.03225806 33.9829374,7.15383871 37.4142595,10.5857419 L32.1760667,15.8240323 C31.4444539,16.5556452 31.9625828,17.8064516 32.9971956,17.8064516 L46.8387098,17.8064516 C47.4800323,17.8064516 48,17.2864839 48,16.6451613 L48,2.80364516 C48,1.76903226 46.7490969,1.25090323 46.0175809,1.98241935 Z"
          fill="currentColor"
          fill-rule="nonzero"
          id="Icon"
          transform="translate(24.000000, 24.000000) scale(-1, 1) translate(-24.000000, -24.000000) "
        />
      </svg>
    );
  }
}
