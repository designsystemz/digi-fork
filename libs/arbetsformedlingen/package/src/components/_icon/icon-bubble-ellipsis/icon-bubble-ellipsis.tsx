import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-bubble-ellipsis',
  styleUrl: 'icon-bubble-ellipsis.scss',
  scoped: true
})
export class IconBubbleEllipsis {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-bubble-ellipsis"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-bubble-ellipsis__shape"
          d="M13.5044715,19.956044 C11.8453116,19.956044 10.5048603,21.2524725 10.5048603,22.8571429 C10.5048603,24.4618132 11.8453116,25.7582418 13.5044715,25.7582418 C15.1636314,25.7582418 16.5040827,24.4618132 16.5040827,22.8571429 C16.5040827,21.2524725 15.1636314,19.956044 13.5044715,19.956044 Z M24.0031106,19.956044 C22.3439507,19.956044 21.0034994,21.2524725 21.0034994,22.8571429 C21.0034994,24.4618132 22.3439507,25.7582418 24.0031106,25.7582418 C25.6622705,25.7582418 27.0027218,24.4618132 27.0027218,22.8571429 C27.0027218,21.2524725 25.6622705,19.956044 24.0031106,19.956044 Z M34.5017497,19.956044 C32.8425898,19.956044 31.5021385,21.2524725 31.5021385,22.8571429 C31.5021385,24.4618132 32.8425898,25.7582418 34.5017497,25.7582418 C36.1609096,25.7582418 37.5013609,24.4618132 37.5013609,22.8571429 C37.5013609,21.2524725 36.1609096,19.956044 34.5017497,19.956044 Z M24.0031106,4 C10.7485787,4 0.00622121476,12.4403846 0.00622121476,22.8571429 C0.00622121476,27.1725275 1.87160441,31.1252747 4.96495344,34.3074176 C3.56825948,37.8793956 0.662386159,40.9074176 0.615517234,40.9436813 C-0.00315257016,41.5782967 -0.171880699,42.503022 0.184323128,43.3008242 C0.540526955,44.0986264 1.35604624,44.6153846 2.2559296,44.6153846 C8.02080732,44.6153846 12.567093,42.2854396 15.2948644,40.4178571 C18.0038883,41.2428571 20.9285092,41.7142857 24.0031106,41.7142857 C37.2576425,41.7142857 48,33.2739011 48,22.8571429 C48,12.4403846 37.2576425,4 24.0031106,4 Z M24.0031106,37.3626374 C21.50031,37.3626374 19.0256308,36.9909341 16.6540632,36.2656593 L14.5262141,35.6129121 L12.698326,36.864011 C11.3578748,37.7796703 9.52061291,38.8041209 7.30839967,39.4931319 C7.99268597,38.3961538 8.65822469,37.1631868 9.17378287,35.8486264 L10.1674041,33.3010989 L8.23640437,31.3247253 C6.5397493,29.575 4.50563798,26.682967 4.50563798,22.8571429 C4.50563798,14.860989 13.2513793,8.35164835 24.0031106,8.35164835 C34.7548419,8.35164835 43.5005832,14.860989 43.5005832,22.8571429 C43.5005832,30.8532967 34.7548419,37.3626374 24.0031106,37.3626374 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
