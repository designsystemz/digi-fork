import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-envelope',
  styleUrls: ['icon-envelope.scss'],
  scoped: true
})
export class IconEnvelope {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-envelope"
        width="26"
        height="19"
        viewBox="0 0 26 19"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-envelope__shape"
          d="M23.563 0H2.438C1.09 0 0 1.063 0 2.375v14.25C0 17.937 1.091 19 2.438 19h21.125C24.909 19 26 17.937 26 16.625V2.375C26 1.063 24.909 0 23.562 0zm0 2.375v2.019c-1.14.903-2.954 2.308-6.835 5.269-.855.655-2.55 2.23-3.728 2.212-1.179.018-2.873-1.557-3.728-2.212-3.88-2.96-5.696-4.365-6.835-5.269V2.375h21.125zM2.438 16.625V7.442c1.163.903 2.813 2.17 5.328 4.089 1.11.851 3.054 2.73 5.234 2.719 2.17.011 4.088-1.84 5.233-2.719a859.794 859.794 0 005.33-4.09v9.184H2.438z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
