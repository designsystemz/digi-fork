import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-compress-alt',
  styleUrls: ['icon-compress-alt.scss'],
  scoped: true
})
export class IconCompressAlt {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-compress-alt"
        width="22"
        height="22"
        viewBox="0 0 22 22"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-compress-alt__shape"
          d="M.23 19.413l4.877-4.877-1.617-1.524C2.748 12.269 3.274 11 4.324 11h5.5c.65 0 1.176.528 1.176 1.179v5.5c0 1.05-1.267 1.575-2.01.833l-1.526-1.62-4.877 4.878a.786.786 0 01-1.11 0L.23 20.524a.786.786 0 010-1.111zM21.77 2.587l-4.877 4.877 1.617 1.524c.742.743.216 2.012-.834 2.012h-5.5C11.526 11 11 10.472 11 9.821v-5.5c0-1.05 1.267-1.575 2.01-.833l1.526 1.62L19.413.23a.786.786 0 011.11 0l1.247 1.246a.786.786 0 010 1.111z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
