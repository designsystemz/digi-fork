import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-notification-succes',
  styleUrls: ['icon-notification-succes.scss'],
  scoped: true
})
export class IconCheckCircleReg {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-notification-succes"
        width="25"
        height="25"
        viewBox="0 0 25 25"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-notification-succes__shape"
          d="M12.5 0C5.596 0 0 5.596 0 12.5S5.596 25 12.5 25 25 19.404 25 12.5 19.404 0 12.5 0zm0 2.42c5.571 0 10.08 4.508 10.08 10.08 0 5.571-4.508 10.08-10.08 10.08A10.075 10.075 0 012.42 12.5c0-5.571 4.508-10.08 10.08-10.08zm7.067 6.565L18.43 7.84a.605.605 0 00-.855-.003l-7.125 7.067-3.014-3.038a.605.605 0 00-.855-.004l-1.145 1.136a.605.605 0 00-.004.856l4.576 4.612a.605.605 0 00.855.004l8.7-8.63a.605.605 0 00.003-.855z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
