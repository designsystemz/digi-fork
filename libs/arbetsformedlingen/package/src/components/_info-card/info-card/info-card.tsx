import {
  Component,
  Element,
  Event,
  EventEmitter,
  Prop,
  Watch,
  h,
  State
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { InfoCardVariation } from './info-card-variation.enum';
import { InfoCardType } from './info-card-type.enum';
import { InfoCardHeadingLevel } from './info-card-heading-level.enum';
import { InfoCardSize } from './info-card-size.enum';
import { InfoCardBorderPosition } from './info-card-border-position.enum';
import { logger } from '../../../global/utils/logger';

/**
 * @slot default - kan innehålla vad som helst
 *
 * @enums InfoCardType - info-card-type.enum.ts
 * @enums InfoCardVariation - info-card-variation.enum.ts
 * @enums InfoCardHeadingLevel - info-card-heading-level.enum.ts
 * @swedishName Infokort
 */
@Component({
  tag: 'digi-info-card',
  styleUrls: ['info-card.scss'],
  scoped: true
})
export class InfoCard {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Rubrikens text
   * @en The heading text
   */
  @Prop() afHeading!: string;

  /**
   * Sätter rubrikens vikt (h1-h6)
   * @en Set heading level (h1-h6)
   */
  @Prop() afHeadingLevel!: InfoCardHeadingLevel;

  /**
   * Sätter typ. Kan vara 'tip' or 'related'.
   * @en Set info card variation. Can be 'tip' or 'related'.
   */
  @Prop() afType: InfoCardType = InfoCardType.TIP;

  /**
   * Sätter storlek på kortet. Kan vara 'Standar'.
   * @en Set info card variation. Can be 'Standar'.
   */
  @Prop() afSize: InfoCardSize = InfoCardSize.STANDARD;

  /**
   * Sätter storlek på kortet. Kan vara 'Standard'.
   * @en Set info card variation. Can be 'Standard'.
   */
  @Prop() afBorderPosition: InfoCardBorderPosition = InfoCardBorderPosition.TOP;

  /**
   * Sätter variant. Kan vara 'primary' eller 'secondary'. Detta har endast genomslag om 'af-type' är 'tip'.
   * @en Set info card variation. Can be 'primary' or 'secondary'. This only applies if 'af-type' is 'tip'.
   */
  @Prop() afVariation: InfoCardVariation = InfoCardVariation.PRIMARY;

  /**
   * Text till en valfri länk. NOTERA: Länken är obligatorisk om 'af-type' är 'single'.
   * @en Text for optional link (required if afType is set to "single")
   */
  @Prop() afLinkText: string;

  /**
   * Sätter attributet 'href' på länken
   * @en Link href attribute
   */
  @Prop() afLinkHref: string;

  /**
   * Avgör om färgen på länken inte ska ändras på besökta länkar
   * @en If link color should change when visited
   */
    @Prop() hideVisitedColor: boolean;


  /**
   * Länkelementets 'onclick'-event
   * @en Link element's 'onclick' event
   */
  @Event() afOnClickLink: EventEmitter;

  @State() activeLink = false;

  /**
   * För att dölja ikon för skärmläsare.
   * @en Hides the icon for screen readers.
   */
  @Prop() afSvgAriaHidden: boolean;

  @State() hasListIcon = false;
  // get shouldRenderListIcon() {

  // 	return true
  // }

  setHasListIcon() {
    const List = this.hostElement.querySelector('digi-list');
    if (List) {
      this.hasListIcon = true;
    }
  }
  componentWillLoad() {
    this.validateHeading();
    this.setHasListIcon();
  }

  @Watch('afHeading')
  @Watch('afHeadingLevel')
  validateHeading() {
    if (!this.afHeading || !this.afHeadingLevel) {
      logger.warn(
        `digi-info-card must have a heading and a heading level. Please add af-heading and af-heading-level`,
        this.hostElement
      );
    }
  }

  clickLinkHandler(e) {
    this.afOnClickLink.emit(e);
  }

  get shouldRenderInternalLink() {
    return this.afType !== this.afLinkText && this.afLinkHref;
  }

  get cssModifiers() {
    return {
      'digi-info-card--primary':
        this.afVariation === InfoCardVariation.PRIMARY &&
        this.afType === InfoCardType.TIP,
      'digi-info-card--secondary':
        this.afVariation === InfoCardVariation.SECONDARY,
      'digi-info-card--tip': this.afType === InfoCardType.TIP,
      'digi-info-card--related': this.afType === InfoCardType.RELATED,
      'digi-info-card--standard': this.afSize === InfoCardSize.STANDARD,
      'digi-info-card--border-top':
        this.afBorderPosition === InfoCardBorderPosition.TOP &&
        this.afType === InfoCardType.RELATED,
      'digi-info-card--border-left':
        this.afBorderPosition === InfoCardBorderPosition.LEFT &&
        this.afType === InfoCardType.RELATED,
      'digi-info-card--active-link': this.activeLink === true
    };
  }

  render() {
    return (
      <div
        class={{
          'digi-info-card': true,
          ...this.cssModifiers
        }}>
        <div class="digi-info-card__content">
          {/* {this.shouldRenderListIcon && ( */}
          <div class="digi-info-card__icon">
            {this.hasListIcon && (
              <span class="digi-info-card__icon">
                <slot name="icon"></slot>
              </span>
            )}
          </div>
          {/* )} */}
          <this.afHeadingLevel class="digi-info-card__heading">
            {this.afHeading}
          </this.afHeadingLevel>
          <digi-typography>
            <slot></slot>
          </digi-typography>
          {this.shouldRenderInternalLink && (
            <digi-link-internal
              class="digi-info-card__link"
              afHref={this.afLinkHref}
              onAfOnClick={(e) => this.clickLinkHandler(e)}
              hideVisitedColor={this.hideVisitedColor}>
              {this.afLinkText}
            </digi-link-internal>
          )}
        </div>
      </div>
    );
  }
}
