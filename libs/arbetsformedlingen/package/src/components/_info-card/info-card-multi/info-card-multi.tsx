import {
  Component,
  h,
  Host,
  Element,
  Event,
  EventEmitter,
  Prop,
  Watch,
  State
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { InfoCardMultiType } from './info-card-multi-type.enum';
import { InfoCardMultiHeadingLevel } from './info-card-multi-heading-level.enum';
import { logger } from '../../../global/utils/logger';
import { TagMediaIcon } from '../../_tag/tag-media/tag-media-icon.enum';

/**
 * @slot mySlot - Slot description, if any
 *
 * @enums myEnum - info-card-multi-myEnum.enum.ts
 *
 * @swedishName Multikort
 */
@Component({
  tag: 'digi-info-card-multi',
  styleUrl: 'info-card-multi.scss',
  scoped: true
})
export class InfoCardMulti {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Sätter attributet 'src'.
   * @en Set `src` attribute.
   */
  @Prop() afSrc: string;

  /**
   * Sätter attributet 'alt'.
   * @en Set `alt` attribute.
   */
  @Prop() afAlt: string;

  /**
   * Sätter Taggtext på bild
   * @en set tag for image
   */
  @Prop() afTagText: string;

  /**
   * Sätter ikon för taggen
   * @en set icon for tag
   */
  @Prop() afTagIcon: TagMediaIcon = TagMediaIcon.NEWS;

  /**
   * Rubrikens text
   * @en The heading text
   */
  @Prop() afHeading!: string;

  /**
   * Sätter rubrikens vikt (h1-h6)
   * @en Set heading level (h1-h6)
   */
  @Prop() afHeadingLevel!: InfoCardMultiHeadingLevel;

  /**
   * Sätter typ. Kan vara 'media' eller 'text'.
   * @en Set info card variation. Can be 'media' or 'text'.
   */
  @Prop() afType: InfoCardMultiType = InfoCardMultiType.MEDIA;

  /**
   * Sätter attributet 'href' på länken
   * @en Link href attribute
   */
  @Prop() afLinkHref: string;

  /**
   * Avgör om färgen på länken inte ska ändras på besökta länkar
   * @en If link color should change when visited
   */
  @Prop() hideVisitedColor: boolean;

  /**
   * Länkelementets 'onclick'-event
   * @en Link element's 'onclick' event
   */
  @Event() afOnClickLink: EventEmitter;

  @State() activeLink = false;

  componentWillLoad() {
    this.validateHeading();
  }

  @Watch('afHeading')
  @Watch('afHeadingLevel')
  validateHeading() {
    if (!this.afHeading || !this.afHeadingLevel) {
      logger.warn(
        `digi-info-card must have a heading and a heading level. Please add af-heading and af-heading-level`,
        this.hostElement
      );
    }
  }

  clickLinkHandler(e) {
    this.afOnClickLink.emit(e);
  }

  get cssModifiers() {
    return {
      'digi-info-card-multi--media': this.afType === InfoCardMultiType.MEDIA,
      'digi-info-card-multi--related':
        this.afType === InfoCardMultiType.RELATED,
      'digi-info-card-multi--entry': this.afType === InfoCardMultiType.ENTRY,
      'digi-info-card-multi--hide-visited': this.hideVisitedColor
    };
  }

  render() {
    return (
      <Host>
        <div
          class={{
            'digi-info-card-multi': true,
            ...this.cssModifiers
          }}>
          <div class="digi-info-card-multi__content">
            {this.afType == InfoCardMultiType.MEDIA && (
              <div class="digi-info-card-multi__image">
                <digi-media-image
                  afUnlazy
                  afFullwidth
                  af-src={this.afSrc}
                  afAlt={this.afAlt}>
                  <div class="digi-info-card-multi__tag">
                    <digi-tag-media
                      af-icon={this.afTagIcon}
                      afText={this.afTagText}></digi-tag-media>
                  </div>
                </digi-media-image>
              </div>
            )}
            <div class="digi-info-card-multi__text">
              <this.afHeadingLevel class="digi-info-card-multi__heading">
                <a
                  class="digi-info-card-multi__link"
                  href={this.afLinkHref}
                  onMouseEnter={() => (this.activeLink = true)}
                  onFocus={() => (this.activeLink = true)}
                  onMouseLeave={() => (this.activeLink = false)}
                  onBlur={() => (this.activeLink = false)}
                  onClick={(e) => this.clickLinkHandler(e)}>
                  {this.afHeading}
                </a>
              </this.afHeadingLevel>

              <digi-typography>
                <slot></slot>
              </digi-typography>
            </div>
          </div>
        </div>
      </Host>
    );
  }
}
