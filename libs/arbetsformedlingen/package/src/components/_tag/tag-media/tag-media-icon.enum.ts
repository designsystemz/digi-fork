export enum TagMediaIcon {
  NEWS = 'news',
  PLAYLIST = 'playlist',
  PODCAST = 'podcast',
  FILM = 'film',
  WEBTV = 'webtv',
  WEBINAR = 'webinar'
}
