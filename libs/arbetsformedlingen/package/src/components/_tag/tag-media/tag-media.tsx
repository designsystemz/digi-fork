import { Component, h, Host, Prop, Element } from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { TagMediaIcon } from './tag-media-icon.enum';

/**
 * @slot mySlot - Slot description, if any
 *
 * @enums myEnum - tag-media-myEnum.enum.ts
 *
 * @swedishName Mediatagg
 */
@Component({
  tag: 'digi-tag-media',
  styleUrl: 'tag-media.scss',
  scoped: true
})
export class TagMedia {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Sätter taggens text.
   * @en Set the tag text.
   */
  @Prop() afText!: string;

  /**
   * Sätter en specifik icon för taggen.
   * @en Sets a specific icon for the tag.
   */
  @Prop() afIcon: TagMediaIcon = TagMediaIcon.NEWS;

  get tagIcon() {
    if (this.afIcon === TagMediaIcon.NEWS) {
      return <digi-icon-news class="digi-tag-media__icon"></digi-icon-news>;
    }

    if (this.afIcon === TagMediaIcon.PLAYLIST) {
      return (
        <digi-icon-list-ul class="digi-tag-media__icon"></digi-icon-list-ul>
      );
    }

    if (this.afIcon === TagMediaIcon.PODCAST) {
      return <digi-icon-pod class="digi-tag-media__icon"></digi-icon-pod>;
    }

    if (this.afIcon === TagMediaIcon.FILM) {
      return (
        <digi-icon-online-video class="digi-tag-media__icon"></digi-icon-online-video>
      );
    }

    if (this.afIcon === TagMediaIcon.WEBTV) {
      return (
        <digi-icon-online-computervideo class="digi-tag-media__icon"></digi-icon-online-computervideo>
      );
    }

    if (this.afIcon === TagMediaIcon.WEBINAR) {
      return (
        <digi-icon-webinar class="digi-tag-media__icon"></digi-icon-webinar>
      );
    }

    return <digi-icon-news class="digi-tag-media__icon"></digi-icon-news>;
  }

  render() {
    return (
      <Host>
        <div
          class={{
            'digi-tag-media': true
          }}>
          {this.tagIcon}
          <span class="digi-tag-media__text">{this.afText}</span>
        </div>
      </Host>
    );
  }
}
