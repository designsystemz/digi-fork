import { detectClosest } from '../detectClosest';

export function detectClickOutside(
  target: HTMLElement,
  selector: string
): boolean {
  return !detectClosest(target, selector);
}
