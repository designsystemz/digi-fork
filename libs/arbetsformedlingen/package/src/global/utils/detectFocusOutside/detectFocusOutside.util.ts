import { detectClosest } from '../detectClosest';

export function detectFocusOutside(
  target: HTMLElement,
  selector: string
): boolean {
  return !detectClosest(target, selector);
}
