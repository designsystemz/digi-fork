export enum KEY_CODE {
  LEFT_ARROW = 37,
  UP_ARROW = 38,
  RIGHT_ARROW = 39,
  DOWN_ARROW = 40,
  ENTER = 13,
  SPACE = 32,
  ESCAPE = 27,
  TAB = 9,
  SHIFT = 16,
  END = 35,
  HOME = 36,
  SHIFT_TAB = 'shift_tab',
  ANY = 'any'
}
