import { KEY_CODE } from './keyCode.enum';

export function keyboardHandler(event: KeyboardEvent): KEY_CODE {
  const key = event?.key || event?.keyCode;
  if (event.shiftKey && (key === 'Tab' || key === KEY_CODE.TAB)) {
    return KEY_CODE.SHIFT_TAB;
  }

  switch (key) {
    case 'Down':
    case 'ArrowDown':
    case KEY_CODE.DOWN_ARROW:
      return KEY_CODE.DOWN_ARROW;
    case 'Up':
    case 'ArrowUp':
    case KEY_CODE.UP_ARROW:
      return KEY_CODE.UP_ARROW;
    case 'Left':
    case 'ArrowLeft':
    case KEY_CODE.LEFT_ARROW:
      return KEY_CODE.LEFT_ARROW;
    case 'Right':
    case 'ArrowRight':
    case KEY_CODE.RIGHT_ARROW:
      return KEY_CODE.RIGHT_ARROW;
    case 'Enter':
    case KEY_CODE.ENTER:
      return KEY_CODE.ENTER;
    case 'Esc':
    case 'Escape':
    case KEY_CODE.ESCAPE:
      return KEY_CODE.ESCAPE;
    case 'Tab':
    case KEY_CODE.TAB:
      return KEY_CODE.TAB;
    case ' ':
    case 'Spacebar':
    case KEY_CODE.SPACE:
      return KEY_CODE.SPACE;
    case 'Home':
    case KEY_CODE.HOME:
      return KEY_CODE.HOME;
    case 'End':
    case KEY_CODE.END:
      return KEY_CODE.END;
    default:
      return KEY_CODE.ANY;
  }
}
