const ids: Array<string> = [];
export function randomIdGenerator(prefix = 'digi') {
  let id = `${prefix}-${Math.random().toString(36).substring(2, 7)}`;
  while (ids.includes(id))
    id = `${prefix}-${Math.random().toString(36).substring(2, 7)}`;
  return id;
}

export function setupId(
  prefix: string,
  userSeed?: string
): (scope: string) => string {
  const seed = userSeed || Math.random().toString(36).substring(2, 7);
  return (scope) => {
    return `${prefix}-${seed}-${scope}`;
  };
}
