export function detectClosest(target: HTMLElement, selector: string): boolean {
  return !!target.closest(selector);
}
