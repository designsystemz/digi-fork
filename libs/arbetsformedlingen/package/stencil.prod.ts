import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

import { angularOutputTarget } from '@stencil/angular-output-target';
import angularValueAccessorBindings from './stencil.valueAccessors';

import { reactOutputTarget } from '@stencil/react-output-target';


console.log('Transpiling with Stencil to Web Components..');

export const config: Config = {
  srcDir: './src',
  tsconfig: './tsconfig.json',
  globalStyle: './src/global/styles/index.scss',
  namespace: 'digi-arbetsformedlingen',
  taskQueue: 'async',
  autoprefixCss: false,
  sourceMap: false,
  plugins: [sass()],
  enableCache: false,
  outputTargets: [
    angularOutputTarget({
      componentCorePackage: '@digi/arbetsformedlingen',
      directivesArrayFile:
        '../../../libs/arbetsformedlingen/angular/src/lib/stencil-generated/proxies-list.ts', // relative to output dir
      directivesProxyFile:
        '../../../libs/arbetsformedlingen/angular/src/lib/stencil-generated/components.ts', // relative to output dir
      valueAccessorConfigs: angularValueAccessorBindings,
      outputType: 'standalone'
    }),
    reactOutputTarget({
      componentCorePackage: '@digi/arbetsformedlingen',
      proxiesFile:
        '../../../libs/arbetsformedlingen/react/src/lib/stencil-generated/components.ts', // relative to output dir
      includeDefineCustomElements: true,
      loaderDir: 'loader'
    }),
    {
      type: 'dist',
      dir: 'dist',
      // Example: esmLoaderPath: dist/libs/arbetsformedlingen/loader
      esmLoaderPath: '../loader',
      copy: [
        // `src` is relative to `srcDir`
        // `dest` is relative to output dir and defined `dir`
        // Example: src: 'libs/arbetsformedlingen/package/src/design-tokens', dest: 'dist/libs/arbetsformedlingen/dist/design-tokens'
        { src: 'design-tokens', dest: 'design-tokens' },
        { src: '../../fonts', dest: 'fonts' },
        { src: '../../styles', dest: 'styles' },
        {
          src: 'components/**/styles/*.variables.scss',
          dest: 'design-tokens/components',
          warn: true
        }
      ]
    },
    {
      type: 'dist-custom-elements',
      externalRuntime: false, // Behövdes för Issue 136
      dir: 'components',
      customElementsExportBehavior: 'single-export-module',
      generateTypeDeclarations: true
    },
    {
      type: 'docs-vscode',
      file: './custom-elements.json'
    },
    {
      type: 'dist-hydrate-script',
      dir: 'hydrate'
    },
    {
      type: 'www',
      serviceWorker: null,
      dir: 'www'
    }
  ]
};
