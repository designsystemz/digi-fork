import { Config } from '@stencil/core';

/**
 * Stencil config for documentation assets
 */
export const config: Config = {
  srcDir: './src',
  namespace: 'digi-arbetsformedlingen',
  taskQueue: 'async',
  autoprefixCss: false,
  sourceMap: false,
  enableCache: false,
  outputTargets: [
    {
      type: 'docs-json',
      file: '../../../dist/libs/arbetsformedlingen/docs.json'
    }
  ]
};
