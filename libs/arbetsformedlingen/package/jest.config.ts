// jest.config.ts
import { createJestPreset, JestConfigWithTsJest } from 'ts-jest';

const jestConfig: JestConfigWithTsJest = {
  ...createJestPreset(),
  automock: false
};

export default jestConfig;
