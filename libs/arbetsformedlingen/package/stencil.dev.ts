import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

/**
 * Config for running stencil locally
 */
export const config: Config = {
  srcDir: './src',
  tsconfig: './tsconfig.json',
  globalStyle: './src/global/styles/index.scss',
  namespace: 'digi-arbetsformedlingen',
  taskQueue: 'async',
  autoprefixCss: false,
  sourceMap: false,
  plugins: [sass()],
  enableCache: false,
  devServer: {
    reloadStrategy: 'pageReload',
    port: 3333
  },
  outputTargets: [
    {
      type: 'docs-vscode',
      file: 'custom-elements.json'
    },
    {
      type: 'dist-hydrate-script',
      dir: 'hydrate'
    },
    {
      type: 'www',
      serviceWorker: null,
      dir: 'www'
    },
    {
      type: 'dist',
      dir: 'dist',
      esmLoaderPath: '../loader',
      copy: [{ src: '../../fonts', dest: 'fonts' }]
    },
    {
      type: 'dist-custom-elements',
      dir: 'components',
      customElementsExportBehavior: 'single-export-module',
      generateTypeDeclarations: true,
      copy: [{ src: '../../fonts', dest: 'fonts' }]
    }
  ]
};
