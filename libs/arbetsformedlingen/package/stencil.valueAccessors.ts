import { FormInputType } from './src/enums';
import { ValueAccessorConfig } from '@stencil/angular-output-target';

// This is used primarily by Angular Reactive Forms
const angularValueAccessorBindings: ValueAccessorConfig[] = [
  {
    elementSelectors: [
      'digi-form-input',
      `digi-form-input[afType=${FormInputType.TEXT}]`,
      `digi-form-input[afType=${FormInputType.EMAIL}]`,
      `digi-form-input[afType=${FormInputType.COLOR}]`,
      `digi-form-input[afType=${FormInputType.DATE}]`,
      `digi-form-input[afType=${FormInputType.DATETIME_LOCAL}]`,
      `digi-form-input[afType=${FormInputType.MONTH}]`,
      `digi-form-input[afType=${FormInputType.PASSWORD}]`,
      `digi-form-input[afType=${FormInputType.SEARCH}]`,
      `digi-form-input[afType=${FormInputType.TEL}]`,
      `digi-form-input[afType=${FormInputType.TIME}]`,
      `digi-form-input[afType=${FormInputType.URL}]`,
      `digi-form-input[afType=${FormInputType.WEEK}]`,
      'digi-form-textarea',
      'digi-form-input-search'
    ],
    event: 'afOnInput',
    targetAttr: 'value',
    type: 'text'
  },
  {
    elementSelectors: [`digi-form-input[afType=${FormInputType.NUMBER}]`],
    event: 'afOnInput',
    targetAttr: 'value',
    type: 'number'
  },
  {
    elementSelectors: ['digi-calendar'],
    event: 'afOnDateSelectedChange',
    targetAttr: 'afSelectedDate',
    type: 'text'
  },
  {
    elementSelectors: ['digi-calendar-datepicker'],
    event: 'afOnDateChange',
    targetAttr: 'value',
    type: 'text'
  },
  {
    elementSelectors: ['digi-form-checkbox'],
    event: 'afOnChange',
    targetAttr: 'checked',
    type: 'boolean'
  },
  {
    elementSelectors: ['digi-form-radiogroup'],
    event: 'afOnGroupChange',
    targetAttr: 'value',
    type: 'text'
  },
  {
    elementSelectors: ['digi-form-radiobutton'],
    event: 'afOnChange',
    targetAttr: 'value',
    type: 'radio'
  },
  {
    elementSelectors: ['digi-form-select', 'digi-form-select-filter'],
    event: 'afOnSelect',
    targetAttr: 'value',
    type: 'select'
  }
];

export default angularValueAccessorBindings;
