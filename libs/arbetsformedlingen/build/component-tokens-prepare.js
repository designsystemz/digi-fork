/**
 * Detta är ett node skript som kopierar över komponent-tokens från mappen med
 * kompilerade tokens-filer till komponentens mapp
 * @en This is a node script that copies component-tokens from folder with
 * compiled token files to the component folder
 */

'use strict';
const glob = require('glob');
const fs = require('fs-extra');
const path = require('path');
const { readdir } = require('fs').promises;

const getFlags = () => {
  const args = process.argv.map((val) => val.replace(new RegExp('-', 'g'), ''));
  return args;
};
const args = getFlags();

const packageName = args
  .filter((val) => val.includes('packagename='))[0]
  .replace('packagename=', '');
const packagePath = `../../${packageName}/package/src`;

console.log(`----- Prepare component theme files for ${packageName} \n`);

/**
 * Sökväg till mapp med kompilerade Design Tokens filer
 * @en Path to folder with compiled Design Token files
 */
const __localComponentTokens = path.resolve(
  __dirname,
  packagePath + '/design-tokens/components'
);

/**
 * Sökväg till mapp med Core-komponenterna
 * @en Path to folder with Core components
 */
let __localCoreComponents = path.resolve(
  __dirname,
  packagePath + '/components'
);

const themeFiles = glob.sync(__localComponentTokens + '/**/*.scss');

const getFilePath = async (dirName, themeFile) => {
  let files = [];
  const items = await readdir(dirName, { withFileTypes: true });

  for (const item of items) {
    if (item.isDirectory()) {
      files = [
        ...files,
        ...(await getFilePath(`${dirName}/${item.name}`, themeFile))
      ];
    } else {
      if (item.name == themeFile) {
        files.push(`${dirName}/${item.name}`);
        return files;
      }
    }
  }

  return files;
};

themeFiles.forEach((file) => {
  const themeFile = path.basename(file);

  getFilePath(__localCoreComponents, themeFile).then((themeFile) => {
    fs.copyFileSync(file, themeFile[0]);
  });
});
