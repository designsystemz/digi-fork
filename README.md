![En byggkloss av ospecificerat märke. Illustration av typsnitt, färgstickor, datorer och mobiler. Skärmarna innehåller ikoner och inmatningsfält.](/assets/readme-banner.png)

<div align="center">
  <p>Robust Designsystem som säkerställer hög tillgänglighet, stabil drift och effektiv livscykelhantering för digitala tjänster.</p>
  <p>Stöd för React, Angular, Web Components och Figma.</p>
</div>

<div align="center">
  <a href="https://designsystem.arbetsformedlingen.se">Dokumentation ↗</a>
  &nbsp;
  <a href="https://designsystem.arbetsformedlingen.se/komponenter/om-komponenter">Komponenter ↗</a>
  &nbsp;
  <a href="#exempel">Exempel</a>
</div>

## Kom igång

Skapa `.npmrc` i roten av ditt projekt och lägg till

```
@digi:registry=https://nexus.jobtechdev.se/repository/arbetsformedlingen-npm/
```

Installera designsystemet genom att köra en av följande rader

```sh
# React
npm install @digi/arbetsformedlingen-react @digi/arbetsformedlingen

# Angular
npm install @digi/arbetsformedlingen-angular @digi/arbetsformedlingen

# Inget ramverk
npm install @digi/arbetsformedlingen
```

Lägg till följande rader i projektets globala css-fil

```css
@import '@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css';
@import '@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/fonts.css';
```

Det var allt! 🚀

## Exempel

### React

```jsx
// App.tsx
import { DigiButton } from '@digi/arbetsformedlingen-react';
import { ButtonVariation } from '@digi/arbetsformedlingen';

export function App() {
  const handleClick = () => console.log('👋 Hallå Världen');
  return (
    <DigiButton afVariation={ButtonVariation.PRIMARY} onAfOnClick={handleClick}>
      Hälsa
    </DigiButton>
  );
}
```

### Angular

```ts
// app.component.ts
import { Component } from '@angular/core';
import { DigiArbetsformedlingenAngularModule } from '@digi/arbetsformedlingen-angular';
import { ButtonVariation } from '@digi/arbetsformedlingen';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [DigiArbetsformedlingenAngularModule],
  templateUrl: './app.component.html'
})
export class AppComponent {
  buttonVariation = ButtonVariation.PRIMARY;
  handleClick() {
    console.log('👋 Hallå Världen');
  }
}
```

```html
<!--app.component.html-->
<digi-button afVariation="buttonVariation" (afOnClick)="handleClick"
  >Hälsa</digi-button
>
```

### Old School

```html
<!--index.html-->
<!doctype html>
<html>
  <head>
    <link
      rel="stylesheet"
      href="node_modules/@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css" />
    <link
      rel="stylesheet"
      href="node_modules/@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/fonts.css" />
    <script
      type="module"
      src="node_modules/@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.esm.js"></script>
  </head>
  <body>
    <digi-button af-variation="primary">Hälsa</digi-button>
    <script>
      let btn = document.querySelector('digi-button');
      btn.addEventListener('afOnClick', () => {
        console.log('👋 Hallå Världen');
      });
    </script>
  </body>
</html>
```

## Bidra till Designsystemet

Är du nyfiken på att hjälpa till med att bygga ett av sveriges mest tillgängliga och flexibla designsystem? Say no more. ✨

Vi behöver hjälp med allt ifrån kod, design, tillgänglighetsarbete, copywriting och marknadsföring.

- Läs [Contribution Guidelines](/CONTRIBUTING.md)
- Kolla igenom [issue tracker](https://gitlab.com/arbetsformedlingen/designsystem/digi/-/issues). 💡 Nybörjartips, [filtrera efter "Mindre uppgift"](https://gitlab.com/arbetsformedlingen/designsystem/digi/-/issues/?sort=created_date&state=opened&label_name[]=Mindre%20uppgift)

## Kommunikation & Forum

⚠️ Observera: Dela inte känslig information eller personuppgifter. Allting i GitLab är tillgängligt för alla besökare. Vid övriga ärenden var god använd kontaktuppgifter som finns tillgängliga på [dokumentationswebben](https://designsystem.arbetsformedlingen.se).

- Har du upptäckt ett problem med designsystemet? [Skapa issue 🐛](https://gitlab.com/arbetsformedlingen/designsystem/digi/-/issues/new?issuable_template=bug)

- Tycker du att något saknas i designsystemet? [Skicka in önskemål 🎁](https://gitlab.com/arbetsformedlingen/designsystem/digi/-/issues/new?issuable_template=feature)

- Har du fastnat på något som inte känns helt rätt eller är oklart? [Ställ fråga 🙋](https://gitlab.com/arbetsformedlingen/designsystem/digi/-/issues/new?issuable_template=question)

## Om designsystemet

Designsystemet är ett väl beprövat designsystem i myndighetsvärlden sen flera år tillbaka. Komponenterna visas och används över 100 000 gånger per månad till tusentals olika besökare med diverse funktionsvariationer.

---

Team Designsystem @ Arbetsförmedlingen 2024 🦝
