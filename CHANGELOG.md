## [28.8.0] - 2025-03-06
- `digi-tablist`
  - Ny prop `afActiveTab` för att bestämma vilken flik som är aktiv baserat på flikens `id`
- `digi-form-category-filter`
  - Ny property `afHideToggle` som gömmer "visa alla/visa färre"-knappen.
- `digi-progress-indicator`, `digi-typography-heading-jumbo`
  - Lade till funktionalitet så det går att fokusera på komponentens rubrik programmatiskt.

## [28.7.0] - 2025-02-27
- `digi-form-category-filter`
  - Ny property `afHideToggle` som gömmer "visa alla/visa färre"-knappen.
- `digi-tools-feedback`
  - Tog bort hårdkodad aria-label på svarsknapparna.
- `digi-progress-list-item` 
  - Aria-label för statusen på steget läses nu upp på rätt sätt även när steget är expanderbart.

- `Dokumentation`
  - Arbetat bort ordet "kund" från dokumentationswebben.
  - Allmänt underhåll.

## [28.6.0] - 2025-02-19
- `digi-info-card`, `digi-info-card-multi`
  - Funktionalitet för att gömma besökt länkfärg med prop `hideVisitedColor`
- `digi-navigation-breadcrumbs`
  - Tagit bort besökt länkfärg
  - Uppdaterad hover-effekt
- `digi-form-file-upload`
  - Fixat problem med att knappar försvann vid specifik brytpunkt
- `digi-calendar-datepicker`
  - Visuell justering av dropdown

- `Dokumentation`
  - Uppdaterat innehåll för `digi-navigation-breadcrumbs`, `digi-form-select`, `digi-form-input-search`

## [28.5.0] - 2025-02-14
- `digi-table`
  - Definierade egen typografi för table-header-element (th).
- `digi-calendar-datepicker`
  - Datumväljaren har nu en egen ValueAccessor som gör den enklare att använda i formulär i Angular. Även mindre buggfix.
- `digi-calendar`
   - Buggfix av funktion som tolkade tom sträng i array som ett värde. 
   - Fixat en bugg som gjorde att komponenten hoppade över en månad om användaren valde ett datum som inte finns i nästkommande månad.

## [28.4.0] - 2025-02-10

- `Fokusmarkering`
  - Vi har tagit fram och implementerat en gemensam fokusmarkering för alla våra komponenter. 
- `Dokumentation`
  - Uppdaterat innehåll och utseende på startsidan. 

## [28.3.1] - 2025-02-05

- `digi-layout-container`
  - Åtgärdat felaktig export
- `Dokumentation`
  - Allmänt underhåll
  - Förtydligat `Grid och brytpunkter` designmönstret

## [28.3.0] - 2025-01-23

- `digi-form-filter`
  - Legend-elementet får nu sitt värde från `afFilterButton` när `afName` är odefinierad
  - Mindre visuella justeringar.
  - Åtgärdat så att horisontell scroll inte syns i Safari vid långa texter.
- `digi-layout-container` 
  - Ny property `afMaxWidth` som styr containers max-bredd. Default är 1400px, men det går nu att välja 1200px också, även om det inte är rekommenderat. 

- `Dokumentation`
  - Allmänt underhåll
  - `digi-expandable-accordion`
    - Förtydligat hur komponenten är tänkt att användas
  - `digi-navigation-tablist`
    - Utökad dokumentation med exempel och hur komponenten är tänkt att användas

## [28.2.2] - 2025-01-14

### Ändrat
- `digi-progress-list-item`
  - `afAriaLabel` är inte längre obligatorisk 
  - Det finns en default aria-label för dom olika statusarna på stegen i Förloppslistan. Denna går att skriva över genom propertyn `afStepStatusAriaLabel`

- `Dokumentation`
  - Förtydligat dokumentation och uppdaterat brutna länkar. 

## [28.2.1] - 2024-12-18

### Ändrat
- `digi-dialog`
  - Åtgärdat bugg som inte satte fokus på Modalen när den visas direkt vid sidinläsning

- `digi-context-menu`
  - Ändrade font-storlek från 18px på desktop till 16px 
  - Åtgärdat bugg som gjorde så att knappen inte initialt beskrivits som expanderbar av hjälpmedel

- `digi-tools-languagepicker`
  - Åtgärdat bugg som gjorde så att knappen inte initialt beskrivits som expanderbar av hjälpmedel

- `digi-form-receipt`
  - Åtgärdat bugg som gjorde så att texten inte centrerades när konfigurationen var satt till "af-type="center"

- `digi-header-avatar`
  - Åtgärdat bugg där defualt-ikonen haft felaktigt utseende

## [28.2.0] - 2024-12-11

### Ändrat

- `digi-form-file-upload`

  - Förtydligat felhanteringen på dokumentationswebben
  - Åtgärdat bugg som gjorde så att samma fil inte kunde laddas in 2 gånger
  - Åtgärdat bugg som gjorde så att komponenten inte kunde starta med en valideringstext
  - Åtgärdat bugg som gjorde så att felmeddelande inte läses upp om ett felmeddelande redan visades i komponenten
  - Ändrat "fil-hover" färg

- `digi-context-menu`
  - Ny property `afAriaLabel` som lägger sig på knappen som expanderar menyn
  - Genererar ett automatiskt id till propertyn `afId` om den inte är satt. Löser bugg som skapat problem med att klick utanför komponenten inte alltid stängt den expanderade menyn

## [28.1.0] - 2024-12-05

### Ändrat

- `digi-form-file-upload`
  - Förbättrat fokus-hanteringen.
  - Visuella småjusteringar har gjorts.

- `digi-typography`
- Ny funktionalitet i digi-typography. Vill du att en rubrik på nivå H2 ska se ut som en H3 så kan du göra det nu genom att lägga till CSS-klassen "digi-h3". Alla headings finns nu också som CSS-klasser.

- `digi-tools-feedback-rating`
- Ny prop för att ändra tack-rubrik efter inskickat betyg, `afThankYouHeading`.

- `digi-notification-error-page`
- Förbättrade default-texter på sidorna.

- `Dokumentation`
  - Förtydligat dokumentation och exempel för `digi-form-file-upload`
  - Förtydligat dokumentation och exempel för intervall av datum för `digi-calendar-datepicker`.
  - Förtydligat dokumentation och exempel för `digi-notification-error-page`.

## [28.0.0] - 2024-11-27

### Ändrat

- Komponenter som har avvecklats <breaking />
  - `digi-progress-step`, ersätt med `digi-progress-list`
  - `digi-progress-steps`, ersätt med `digi-progress-list-item`

- `digi-form-file-upload` <breaking />

  - Breaking change - Event `afOnRetryFile` har tagits bort.
  - Ny property `af-validation-text` för att provocera fram ett felmeddelande i komponenten. Kan exempelvis användas för att informera om att en fil är obligatorisk.
  - Vi har ändrat utseende och beteende för felhantering i komponenten. Den liknar nu våra andra formulär-komponenter.
  - Filer som innehåller fel, eller inte passerar valideringen kommer inte längre att visas i listan på filer.
  - Förbättringar av tillgängligheten
    - Alla felmeddelanden har nu `role=alert` och kommer notifiera hjälpmedel om sitt innehåll
    - Komponenten har nu aktiv fokus-hantering så hjälpmedel inte tappar fokus när en fil tas bort. Fokus hamnar antingen på listan med filer, eller på knappen som väljer fil.

- `Dokumentation`
  - Förtydligat dokumentation och exempel för `digi-form-select-filter`, `digi-form-filter`, `digi-form-file-upload`

## [Underhåll] - 2024-11-07

### Ändrat

- Dokumentatationswebben
  - Lagt till länk till UI-kittet på startsidan
  - Förtydligat exempel för `digi-calendar-datepicker`

### Komponenter under avveckling

- `digi-progress-step` - utgår i slutet av november, använd `digi-progress-list`
- `digi-progress-steps` - utgår i slutet av november, använd `digi-progress-list-item`

## [27.2.0] - 2024-10-23

### Nytt

- `digi-notification-error-page` - Ny felsida, 503 Underhåll

- Dokumentatationswebben
  - Ny sida för Forskning och samarbeten

### Ändrat

- Dokumentationswebben
  - Ändrat information på sidan `tillgänglighetsredogörelsen`

### Komponenter under avveckling

- `digi-progress-step` - utgår i slutet av oktober, använd `digi-progress-list`
- `digi-progress-steps` - utgår i slutet av oktober, använd `digi-progress-list-item`

## [27.1.0] - 2024-10-16

### Ändrat

- `digi-tools-feedback-rating` - buggfix av medskick av betyg och URL till Webropol.
- `digi-form-select-filter` - ny prop, `sortAlphabetically` som default är satt till true. Även buggfix för list-items som ska ha texten från höger till vänster har gjorts.

- Dokumentationswebben
  - Ändrat information på sidan `Jobba med Digi UI Kit` om nya UI-kitet i Figma.

### Komponenter under avveckling

- `digi-progress-step` - utgår i slutet av oktober, använd `digi-progress-list`
- `digi-progress-steps` - utgår i slutet av oktober, använd `digi-progress-list-item`

## [Underhåll] - 2024-10-09

### Ändrat

- Dokumentationswebben
  - Uppdaterat exempel för `Väljare med sök` med obligatoriska attribut
  - Radiogrupp finns nu tillgänglig i listan över komponenter

### Komponenter under avveckling

- `digi-progress-step` - utgår i slutet av oktober, använd `digi-progress-list`
- `digi-progress-steps` - utgår i slutet av oktober, använd `digi-progress-list-item`

## [27.0.2] - 2024-10-02

### Ändrat

- `digi-expandable-accordion`

  - `aria-pressed` är borttaget så att komponenten inte läses upp som växlingsknapp av skärmläsare.

  `digi-form-select-filter`

  - Åtgärdat höjden i listan, som inte anpassade sig efter innehållet.

  `digi-calendar-datepicker`

  - Åtgärdat bugg som gjorde att valideringsfel kvarstod om man försökte välja samma datum en gång till.

  ### Komponenter under avveckling

  - `digi-progress-step` - utgår i slutet av oktober, använd `digi-progress-list`
  - `digi-progress-steps` - utgår i slutet av oktober, använd `digi-progress-list-item`

## [27.0.1] - 2024-09-25

### Ändrat

- `digi-progress-list`, `digi-progress-list-item`
  - Höjden på expanderat innehåll görs inte längre via Javascript i komponenten, utan via display-propertyn i CSS. Det medför att:
    - Om innehåll i expanderbar yta inte hinner renderas innan javascript-funktionen körs så doldes den, det gör den inte längre
    - Om innehållet bestod av expanderbara komponenter så doldes dessa ibland eftersom overflow-y var satt som hidden
  - `aria-current` ändrades nu dynamiskt även efter rendering
  - Ny property `afCollapseText` för text på knapp som minimerar förlopps steget
  - Tog bort felaktig WAI-ARIA role och förenklade semantiken som potentiellt kan störa användare med hjälpmedel

## [27.0.0] - 2024-09-18

### Ändrat

- Komponenter som har avvecklats <breaking />

  - `digi-navigation-tabs`, ersätt med `digi-tablist`
  - `digi-navigation-context-menu`, ersätt med `digi-context-menu`
  - `digi-progressbar`, ersätt med `digi-progress-indicator`

- `digi-calendar-datepicker` <breaking />

  - Skickar ut change-event med värdet `['']` när datumväljaren inte har ett värde.

- `Dokumentation`
  - Underhåll av tillgänglighetslistan. Åtgärdade problem med skräptecken vid återimport av tillgänglighetslista.

### Komponenter under avveckling

- `digi-progress-step` - utgår i slutet av oktober, använd `digi-progress-list`
- `digi-progress-steps` - utgår i slutet av oktober, använd `digi-progress-list-item`

## [26.6.6] - 2024-09-05

### Ändrat

- `digi-calendar-datepicker`
  - Fixat är nu att ikonen i datumväljaren syns även om återställningsfil har används.
- `digi-form-select-filter`

  - Lagt till ValueAccessor till Väljare med sök för att förenkla användandet av komponenten i Angular.

- `Dokumentation`
  - Rättning i designmönstret för Grid och brytpunkter med korrekta värden för extra breda skärmar.

## [26.6.5] - 2024-08-21

### Ändrat

- `digi-calendar-datepicker`

  - afOnDateChange triggas även när ett ogiltig datum är inmatat.
  - Använd `event.detail` istället för `event.target.value` för att få ett tomt värde returnerat vid ogiltigt datum.

- `Dokumentation`
  - Lagt till indikatorer i listan över komponenter som visar om komponenten är ny / utgående.

### Komponenter under avveckling

- `digi-navigation-tabs` - utgår i slutet av augusti, använd `digi-tablist`
- `digi-navigation-context-menu` - utgår i slutet av augusti, använd `digi-context-menu`
- `digi-progressbar` - utgår i slutet av augusti, använd `digi-progress-indicator`
- `digi-progress-step` - utgår i slutet av oktober, använd `digi-progress-list`
- `digi-progress-steps` - utgår i slutet av oktober, använd `digi-progress-list-item`

## [Underhåll] - 2024-08-15

### Ändrat

- `Dokumentation`
  - Förtydligat exempel för `digi-tag-media`.
  - Förtydligat instruktioner om hur man kan bidra till designsystemet. Instruktioner hittas i källkoden (CONTRIBUTING.md).
  - Förtydligat instruktioner om hur man kan konsumera designsystemet för olika miljöer. Instruktioner hittas i källkoden (README.md).

### Komponenter under avveckling

- `digi-navigation-tabs` - utgår i slutet av augusti, använd `digi-tablist`
- `digi-navigation-context-menu` - utgår i slutet av augusti, använd `digi-context-menu`
- `digi-progressbar` - utgår i slutet av augusti, använd `digi-progress-indicator`
- `digi-progress-step` - utgår i slutet av oktober, använd `digi-progress-list`
- `digi-progress-steps` - utgår i slutet av oktober, använd `digi-progress-list-item`

## [26.6.4] - 2024-07-23

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-progress-indicator` - Fix på visuell bug, samt lagt mindre funktionaliteter, detaljer finns på dokumentationssidan.

## [26.6.3] - 2024-07-12

### Ändrat

- `@digi/core`

  - Löst problem med att vissa komponenter inte renderats korrekt i Angular

- `@digi/arbetsformedlingen`
  - `digi-tools-feedback-rating` har fått bättre stöd med olika läsriktningar, specifikt RTL (Right to Left)

## [Underhåll] - 2024-07-10

- Uppdaterat pipelines för att förhindra mindre buggar.
- Lagt till verktyget `style-dictionery-export` för bl.a. import/export av variabler till Figma.

## [26.6.2] - 2024-07-04

### Ändrat

- `@digi/core`, `@digi/arbetsformedlingen`

  - Löst problem där användare importerat Stencil som ett beroende i sitt projekt för att använda `digi-core`

- `Dokumentation`
  - Förenklat instruktioner för att komma igång med digi-core för `React` och `Angular`
  - Sett över och rensat informationssidor

## [26.6.1] - 2024-06-21

### Ändrat

- `@digi/core`
  - `digi-layout-columns` - Fixade felaktig import

## [26.6.0] - 2024-06-20

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-navigation-context-menu`, `digi-tools-languagepicker` - stöd för att ändra `afStartSelected` efter första renderingen. Detta möjliggör för att om hen ändrar attributet `af-language-picker-start-selected` i Språkväljaren efter första rendering så kommer värdet propageras ner till underliggande komponent.
  - `digi-tools-languagepicker` - skriver nu inte längre ut logg-meddelanden i consolen
  - `digi-tools-feedback-rating` - har fått attribut för högerjusterad text samt val av språk. Det svenska namnet på komponenten har blivit ändrat till Betygskomponenten.
  - `digi-tag-media` - har fått justerad hörnrundning
  - `digi-status-badge` - av typen varning primär har fått justerad textfärg.

## [26.5.0] - 2024-06-12

### Nytt

- `@digi/core`

  - `digi-navigation-vertical-menu-item` har fått stöd för att implementera digi-badge-status.

- `Dokumentation`
  - Nytt designmönster för länkar

### Ändrat

- `@digi/core`

  - `--digi--global--color--function--warning--darkest` har blivit uppdaterad
  - `digi-link` - Fixade bugg i länk-komponenten. Den hade underline som default-utseende, vilket var fel.
  - `digi-layout-columns` - Lade till stöd för 32px column-gap

- `@digi/arbetsformedlingen`

  - `digi-badge-status` av sekundär typ har uppdaterad textfärg och borttagen ram
  - `digi-form-checkbox`, `digi-form-input`, `digi-form-select`, `digi-form-select-filter`,`digi-form-textarea` har fått uppdaterad ramfärg

- `Dokumentation`
  - Uppdaterat länkar för grafiskt material
  - Förtydligat om en komponent utgår eller är ny i meny för komponenterna

### Komponenter under avveckling

- `digi-navigation-tabs` - utgår i slutet av augusti, använd `digi-tablist`
- `digi-navigation-context-menu` - utgår i slutet av augusti, använd `digi-context-menu`
- `digi-progressbar` - utgår i slutet av augusti, använd `digi-progress-indicator`
- `digi-progress-step` - utgår i slutet av oktober, använd `digi-progress-list`
- `digi-progress-steps` - utgår i slutet av oktober, använd `digi-progress-list-item`

## [26.4.0] - 2024-05-30

### Nytt

- `@digi/core`
  - `digi-icon-send-outline` `digi-icon-send-solid` - nya ikoner
  - `digi-link`, `digi-link-external`, `digi-link-internal`, `digi-typography` - med "hide-visited-color" döljs nu besökt färg på länkar.

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-tools-feedback-rating` - justering av förvald betygsskala

- `@digi/core`

  - `digi-tablist` - Förbättrat upplevelsen för användare som använder skärmläsare
  - `digi-link`, `digi-link-external`, `digi-link-internal`, `digi-typography`
    - diverse utseendejusteringar samt att UI-kitet och digi-core nu är i synk.
    - åtgärdat fel med att ikon inte fick samma färg som texten vid besökt länk

- `Dokumentation`
  - Justering av betygsskala för kodexempel för digi-tools-feedback-rating

### Komponenter under avveckling

- `digi-navigation-tabs` - utgår i slutet av augusti, använd `digi-tablist`
- `digi-navigation-context-menu` - utgår i slutet av augusti, använd `digi-context-menu`
- `digi-progressbar` - utgår i slutet av augusti, använd `digi-progress-indicator`

## [26.3.1] - 2024-05-27

### Ändrat

- `@digi/core`
  - `digi-tablist` - Åtgärdat problem med responsivt läge i Angular

## [26.3.0] - 2024-05-22

### Nytt

- `@digi/core`

  - `digi-link` `digi-link-internal` `digi-link-external` - Lagt till stöd för aria label

- `@digi/arbetsformedlingen`
  - `digi-badge-status` - Lagt till stöd för olika storlekar och justeringar i utseendet
  - `digi-form-file-upload` - Lagt till event för när filväljaren visas.

### Ändrat

-`@digi/arbetsformedlingen` - `digi-form-receipt` `digi-notification-error-page` - Lagt till stöd för att fokusera rubriken - Uppdaterat funktionsfärger för varning, fel och godkänt

### Komponenter under avveckling

- `digi-navigation-tabs` - utgår i slutet av augusti, använd `digi-tablist`
- `digi-navigation-context-menu` - utgår i slutet av augusti, använd `digi-context-menu`
- `digi-progressbar` - utgår i slutet av augusti, använd `digi-progress-indicator`

## [26.2.0] - 2024-05-15

### Nytt

- `@digi/core`
  - `digi-tablist` - Lade till funktionalitet för att kunna välja vilken flik som är förvald

### Ändrat

- `Dokumentation`
  - Förloppsregistratorn har bytt namn till Förloppslista

## [26.1.0] - 2024-05-08

### Nytt

- `@digi/arbetsformedlingen`

  - `digi-progress-list` - Ny komponent

- `Dokumentation`
  - Tillägg till `digi-progressbar` och att den blir deprecated
  - Förtydligande i dokumentationen om digi-form-input

### Ändrat

- `@digi/core`
  - `digi-tablist` - Fixade ett typfel

### Komponenter under avveckling

- `digi-progressbar` - utgår i slutet av augusti, använd `digi-progress-indicator`

## [26.0.0] - 2024-04-25

### Nytt

- `@digi/core`

  - `digi-tablist` - Ny komponent för flikar
  - `digi-context-menu` - Ny komponent för rullgardiner

- `Dokumentation`
  - Ny sida om Samarbeten
  - Ny sida om testlabbet

### Ändrat

- `@digi/core`
  - `digi-form-label` - Stöd för tom text i frivillig och obligatorisk placeholder

### Komponenter under avveckling

- `digi-navigation-tabs` - utgår i slutet av augusti, använd `digi-tablist`
- `digi-navigation-context-menu` - utgår i slutet av augusti, använd `digi-context-menu`

## [25.0.0] - 2024-04-17

### Ändrat

- `@digi/core`
  - `digi-button`, `digi-link-button` <breaking /> - Knappinnehållet radbryts inte och förblir på samma rad. Kan påverka visuell design.
  - `digi-tag` - Standardvärde för aria-label när taggen är stängbar
- `@digi/arbetsformedlingen`

  - `digi-form-file-upload` - Möjlighet att specificera felmeddelande per fil

- `Dokumentationswebb`
  - Designmönster `Knappar` är omarbetad
  - Komponentsida för `digi-button` har fått tillägg om knappar i grupp

## [24.0.1] - 2024-04-12

- `@digi/arbetsformedlingen`
  - Fixade ett problem som uppstod med specifika bundlers

## [24.0.0] - 2024-04-11

### Nytt

- `@digi/arbetsformedlingen`
  - Nya ikoner
    - 🆕 `digi-icon-move`
    - 🆕 `digi-icon-share-chain`
    - 🆕 `digi-icon-table-column`
    - 🆕 `digi-icon-table-row`

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-tools-feedback-rating` <breaking />
    - Ny design för komponenten
    - `afRatings` separeras nu med semikolon istället för kommatecken
    - Nya props för Piwik och Webropol (`afName`, `afQuestionType`, `afUseCustomLink`)
    - `afOnSubmitFeedback` är borttaget eftersom Skicka-in-knappen är borttagen
    - `afMReset` för att återställa komponenten till ursprungsläge
  - `digi-icon-table` - Nytt utseende
- `@digi/core`
  - `digi-form-filter` <breaking />
    - 🆕 Prop `afListItems` för att definiera filter
    - 🆕 Prop `afCheckItems` (valfri), ids på filter som är valda
    - Event `afOnChange` är omdöpt till `afChangeFilter`
    - Event `afOnFilterClosed` är omdöpt till `afCloseFilter`
    - Event `afOnResetFilters` är omdöpt till `afResetFilter`
    - Event `afOnSubmitFilters` är omdöpt till `afSubmitFilter`
    - Event `afOnFocusout` är omdöpt till `afBlurFilter`
    - Alla event innehåller nu `filterName` i eventet som baseras på filtrets namn. Eventen innehåller mer information. För detaljer besök dokumentationswebben.
  - `digi-expandable-accordion`
    - Höjd på innehåll i komponent beräknas nu korrekt
- `Dokumentation`
  - `digi-tools-feedback-rating` är uppdaterad med information
  - Designmönster `Enkäter och feedback` är justerat
  - Kontaktsidan är uppdaterad med tydligare information

## [23.2.1] - 2024-03-20

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-info-card-multi`, `digi-info-card` - Uppdaterade riktlinjer för info-kort och dess länkar med förtydligande
    om vad som är obligatoriska attribut. Tog bort varning.
  - `digi-form-select-filter` - Lagt till funktionalitet för att nollställa val i "Väljare med sök".

## [23.2.0] - 2024-03-13

### Ändrat

- `@digi/core`

  - `digi-bar-chart`, `digi-chart-line` - rensat konsolen från onödigt felmeddelande
  - `digi-form-select` - lagt till afAriaLabel

- `@digi/arbetsformedlingen`

  - `digi-tag-media` - har uppdaterats med nya ikoner
  - `digi-icon-media-webinar` - har tagits bort och istället ersatt `digi-icon-webinar`

- `Dokumentation`
  - Utforska-fältet har tagits bort från Designsystemets startsida och kommer att ersättas av en ny lösning

## [23.1.1] - 2024-03-06

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-calendar-datepicker` Utökad funktionalitet för tangentbord
  - `digi-form-select-filter` Fix av visuell bug

## [23.1.0] - 2024-02-28

### Nytt

- `@digi/core`

  - `digi-icon-check-circle-solid` - ny ikon

- `@digi/arbetsformedlingen`
  - `digi-icon-apple`,`digi-icon-bullseye`, `digi-icon-checklist`,`digi-icon-communication-list`,`digi-icon-communication-play-solid`,`digi-icon-laptop-phone`,`digi-icon-media-webinar`,`digi-icon-online-computervideo`,`digi-icon-online-interview`,`digi-icon-online-meeting`,`digi-icon-online-task`,`digi-icon-online-video`,`digi-icon-play-solid`,`digi-icon-pod`,`digi-icon-read`,`digi-icon-task-done`,`digi-icon-wifi-signal0`,`digi-icon-wifi-signal1`,`digi-icon-wifi-signal2`,`digi-icon-wifi-signal3`,`digi-Icon-window` -
    nya ikoner

### Ändrat

- `@digi/core`

  - `digi-icon-check-circle-outline` - uppdaterad design
  - `digi-expandable-accordion` - mikrocopy går nu att skriva över med egna värden, default är fortfarande på svenska

- `@digi/arbetsformedlingen`

  - `digi-icon-comunication-graduate`,`digi-icon-communication-play` - uppdaterad design
  - `digi-form-category-filter` - lagt till default bakgrund på knappar

- `Dokumentation`
  - Justeringar med bilder som inte visades i visa designmönster

## [23.0.1] - 2024-02-21

### Nytt

- `Dokumentation`
  - Ny sida: `designmönster/indikatorer` - notifikationsindikator, statusindikator, mediaindikator.
  - Ny sida: `tillgänglighet/kognitiv genomgång`.

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-link`, `digi-link-internal`, `digi-link-external`; Justerat typsnitt till `semibold`.

- `Dokumentation`
  - Justerat layout på sidan `komponenter/om vårt komponentbibliotekt`.

## [23.0.0] - 2024-02-14

### Nytt

- `@digi/arbetsformedlingen-angular`; Stödjer Angular version 17

### Ändrat

- `@digi/arbetsformedlingen`;
  - `digi-form-file-upload`; <breaking /> `afVariation` ändras till att justera storlekar. Support av tumnagelbilder
    och förhandsvisning av bilder.

## [22.1.0] - 2024-01-31

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-tools-feedback-rating`; Ändrat färg på stjärnan. Lagt in nytt attribut, `af-product-area`, används med
    Piwik. Attributet `af-variation` ändras så den används till att sätta bakgrundsfärgen och samtidigt tar vi bort
    möjligheten att använda siffror. Även lagt till en ny typ, `af-type`. Layout har justerats.

- `Dokumentation`
  - `digi-form-label`; Justerat kodexempel så man inte kan använda ikon på etiketten.
  - Sidan `Grafisk profil/Bilder` har justerats.
  - Sidan `Tillgänglighet/Lagkrav och riktlinjer` har justerats.

## [22.0.0] - 2024-01-19

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-tools-feedback-rating`; Ny komponent

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-dialog`; Har justerat färg på stäng-knappen
  - `digi-languagepicker`; Ändrat så att lyssna-knappen är dold som förvalt

## [21.2.1] - 2023-12-20

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-bar-chart`; Har fått en loading state i väntan på att datat ska läsas in, gäller för vertikala varianten.
  - `digi-chart-line`, `digi-bar-chart`; Justerat marginaler så de matchar varandra bättre.

- `Dokumentation`
  - Ny layout på sidorna Typografi och Digital tillgänglighet
  - Tillgänglighetsmatrisen har fått en egen sida
  - Tillgänglighetsåtgärder på sidan ”Om vår grafiska profil”

## [21.2.0] - 2023-12-13

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-footer`; Ny komponent

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-bar-chart`, `digi-chart-line`; Lagt till `aria-label` på knappen som växlar mellan diagram- och tabellvy.
  - `digi-icon-table`; Buggfix med `path` som kan peka på fel element på sida.
  - `digi-navigation-tabs`; `:hover` på flikar sätter `cursor` till `pointer`.

## [21.1.1] - 2023-12-08

### Ändrat

- `Dokumentation`
  - Bugfix i kodexempel i `digi-header`
  - Uppdaterad dokumentation `digi-tools-languagepicker`

## [21.1.1] - 2023-12-06

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-icon-bookmark-outline`, `digi-icon-bookmark-solid`, `digi-icon-file-governing`, `digi-icon-system-assistance` ;
    Nya ikoner

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-badge-status`; Ändrad border och border-box
  - `digi-quote-single`; Tog bort en height
  - `digi-icon-twitter`; Ändrad till den nya loggan
  - `digi-navigation-menu-item`, `digi-typography-heading-jumbo`; Man kan sätta `lang` kod på dessa nu, standard är
    svenska

## [21.0.0] - 2023-11-29

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-header-avatar`; <breaking /> Attributet `afIsLoggedIn` ändras till `true` som förvalt.
  - `digi-navigation-context-menu`; Lagt in nytt attribut, `af-horizontal-position`, så man kan välja på vilken sida
    menyn placeras.
  - `digi-tools-languagepicker`; <breaking /> Tagit bort attributet `af-languagepicker-variation` då
    valet `ReadSpeaker` tas bort. Attributet `af-hide-listen` ersätt med `af-show-listen`, vilket innebär att som
    standard så döljs lyssna-knappen.
  - `digi-button`, `digi-navigation-context-menu`; Funktionsknappen får ny textfärg. Den använder sig av länkfärg nu
    istället.

## [20.5.0] - 2023-11-22

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-progress-indicator`; Ny komponent

### Ändrat

- `Dokumentation`
  - Bugfix i tillgänglighetslistan.

## [20.4.1] - 2023-11-15

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-logo`; Justerat textstorlek på systemnamn.
  - `--digi--typography--title-logo--font-size--desktop`; använder sig nu av följande
    token `--digi--global--typography--font-size--largest-5`.
  - `--digi--typography--heading-jumbo--font-size--desktop-large`; använder sig nu av följande
    token `--digi--global--typography--font-size--largest-6`.

- `Dokumentation`
  - Nu finns det en möjligheten att bara rensa sina filterval men inte det man fyllt i i tillgänglighetslistan.

## [20.4.0] - 2023-11-09

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-header`, `digi-header-avatar`, `digi-header-navigation`, `digi-header-navigation-item`, `digi-header-notification`;
    Nya komponenter
  - `digi-form-select-filter`; Ny komponent.

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-calendar-datepicker`; Lagt in stöd för valideringsstatus och valideringsmeddelande. Lagt in bättre stöd för
    valueaccessor.
  - `digi-tools-languagepicker`, `digi-navigation-context-menu`, `digi-navigation-context-menu-item`; Åtgärdat färg
    när man trycker ned knappen.
  - `digi-calendar`; Lagt in koll på att datumet är av rätt typ.
  - `digi-logo`; Justerat storlek.

## [20.3.1] - 2023-11-01

### Nytt

- `Dokumentation`
  - Feedbackkomponenten används nu i dokumentationswebben på komponentsidorna.

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-tools-languagepicker`; Korrigeringar i färger i digi-tools-languagepicker vid tab och hover samt justeringar
    i marginaler.

## [20.3.0] - 2023-10-25

### Nytt

- `@digi/arbetsformedlingen`

  - `digi-loader-skeleton`; Ny komponent.

- `Dokumentation`
  - Nytt designmönster: laddningsindikatorer: skeleton loader och spinner

## [20.2.1] - 2023-10-20

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-form-input-search`; Åtgärdat bugg där komponenten försökte läsa in `DigiFormInputCustomEvent` från fel
    sökväg.

## [20.2.0] - 2023-10-19

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-badge-status`; Ny komponent.

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-typography-time`; Lagt till möjlighet att visa datum i olika internationella datumformat
  - `digi-calendar`; lagt till ny navigering för att välja år/månad.

- `Dokumentation`
  - `digi-badge-notification`; Förtydligat kodexempel

## [20.1.1] - 2023-10-11

### Nytt

- `@digi/arbetsformedlingen`
  - `--digi--color--text--danger`; Ny designtoken.

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-form-file-upload`; Felmeddelandetext pekar mot ny designtoken `--digi--color--text--danger`
  - `digi-form-input-search`; Nytt event, `afOnSubmitSearch`. Komponent skickar detta Event när sökfältets knapp är
    klickat eller enter-knapp är tryckt när man har fokus på sökfältet. Returnerar fältets value

## [20.1.0] - 2023-10-04

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-badge-notification`; Ny komponent.
  - Lagt till bättre stöd för att bygga kod i Vite.

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-form-select`; Fixat så emit endast skickas event från select-elementet

## [20.0.4] - 2023-09-27

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-navigation-pagination`; Ändrat tabbordning så knapparna föregående och nästa går att navigera till först.

## [20.0.3] - 2023-09-21

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-util-click-outside`; Lagt till stöd så den går att använda i Angular.

## [20.0.2] - 2023-09-20

### Ändrat

- `@digi/arbetsformedlingen`

  - Uppdaterat mörka funktionsfärger (info, danger, warning, success) för bättre matchning mot profilfärgerna.
    `digi-link-internal`; korrigerat storlek på ikonen.

- `Dokumentation`
  - Korrigerat kodexempel på sidan "Design tokens/beskrivning".

## [20.0.1] - 2023-09-08

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-form-checkbox`; Korrigerat layout för kryssrutor när man sätter den som `indeterminate`.

## [20.0.0] - 2023-09-08

### Nytt

- `@digi/arbetsformedlingen-angular`
  - <breaking /> Uppdaterat till Angular 16
  - Om man använder Angular version lägre än 16, kan man behöva sätta `"skipLibCheck": true` i angular-applikationen,
    för att de har beroende till olika versioner av typescript, där lägsta version man kan använda i Angular 16 är
    Typescript 4.9.3.

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-tools-languagepicker`; <breaking /> Ersatt Google translate till Readspeaker.
  - `digi-icon-google-translate`, `digi-icon-translate`; <breaking /> Tagit bort från ikonbiblioteket. Byt ut
    mot `digi-icon-globe`.
  - `digi-bar-chart`; Åtgärdat så typsnittsstorlekar beräknas om korrekt.

## [19.5.1] - 2023-08-30

### Nytt

- `@digi/arbetsformedlingen`

  - `digi-icon`; nya ikoner: `digi-icon-screensharing` och `digi-icon-screensharing-off`

- `Dokumentation`
  - Nytt Designmönster: Enkäter och feedbackfunktioner

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-form-file-upload`; rättat felstavning
  - `digi-icon-camera-off`, `digi-icon-sign`; tagit bort en mask
  - Förbättrat mobilupplevelse på introduktionsidan för designmönsterna

## [19.5.0] - 2023-08-23

### Nytt

- `@digi/arbetsformedlingen`

  - `digi-expandable-faq`, `digi-expandable-faq-item`; Ny variant av komponenten
  - `digi-tools-feedback-banner`; Ny komponenten

- `Dokumentation`
  - `digi-list` - byt ut all list i dokumentationn webben mot komponent

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-bar-chart` - nytt attribut `af-variation`, så man kan välja mellan horisontell och vertikal stapeldiagram
  - `digi-info-card` - tagit bort en förvirrande felmeddelande på info-kort
  - `digi-link-external` - tagit bort router link från komponenten
  - `digi-link-external`, `digi-link-internal`, `digi-link` - la in ny attribut `af-describedby` är ny prop i
    digi-link
  - `digi-error-list`, ändring i komponenten, la in attribut `af-enable-heading-focus` som kontrollera Heading focus

## [19.4.0] - 2023-07-05

### Nytt

- `@digi/arbetsformedlingen`

  - `digi-notification-error-page`; Ny komponent
  - `digi-bar-chart`; Ny komponent
  - Ikonerna - man kan ange `aria-labelledby`

- `Dokumentation`
  - `Felmeddelandesidor` - nytt designmönster
  - `Sök och sökfilter` - nytt designmönster

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-typography-heading-jumbo`; Liten justering i marginalen mellan rubrik och linje

## [19.3.1] - 2023-06-28

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-util-mutation-observer`, `afOptions` kan nu ta emot `characterData` utöver `childList` & `attributes`

- `Dokumentation`
  - Ny sida Rörligt och ljud - Grafisk profil.
  - Uppdaterat startsida med ny tillgänglighetsbanner.

## [19.3.0] - 2023-06-21

### Nytt

- `@digi/arbetsformedlingen`
  - Nya
    videoikoner; `digi-icon-videocamera`, `digi-icon-videocamera-off`, `digi-icon-microphone`, `digi-icon-microphone-off`, `digi-icon-phone-hangup`
- `Dokumentation`
  - Nytt innehåll på startsidan som länkar till tillgänglighetsmatrisen

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-form-label`; Flyttat beskrviningstext innanför etikett
- `Dokumentation`
  - Ny Piktogram-sida/Whiteboardverktyg

## [19.2.7] - 2023-06-14

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-quote-single`, `digi-quote-multi-container`; Nya komponenter för citat

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-list`; Ändrat hur komponenten läser in HTML-taggar

## [19.2.6] - 2023-06-13

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-form-input`, `digi-form-input-search`; Åtgärdat bugg med padding
  - `digi-tools-languagepicker`; Åtgärdat bugg med fel färger på knappar

## [19.2.5] - 2023-06-07

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-calendar-datepicker`;

    - Fixat bugg med valideringen

  - `digi-form-select`;
    - Lagt till `afOnSelect`

- `Dokumentation`
  - Textkorrektur i sidan för Instruktioner för lista med tillgänglighetsbrister.

## [19.2.4] - 2023-05-31

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-calendar`;
    - La till `afInitSelectedDate` för att kunna ställa in förvalt år och månad

## [19.2.3] - 2023-05-31

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-calendar`;
    - La till `afInvalid` och `afValidationMessage` så att man manuellt kan styra valideringen.

- `Dokumentation`
  - Textkorrektur

## [19.2.2] - 2023-05-24

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-calendar-datepicker`;
    - Ändrade så att man kan välja upp till 10 år fram i tiden.
    - Fixade så att kalendern lägger sig över allt under, istället för att putta ner det.
    - Lagt till ett property för att automatiskt stänga kalendern vid valt datum.

- `Dokumentation`
  - Ändrat innehåll `Bilder` under `Grafisk profil`

## [19.2.1] - 2023-05-17

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-navigation-pagination`; Ändrat responsiv funktion och ny property afLimit.
  - `digi-layout-container`; Justerat så att grid storlek och paddings stämmer överens med designmönstret.
  - `digi-button`; Ny token: --digi--button--align-items.

## [19.2.0] - 2023-05-12

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-list`; En ny komponent för listor med färdiga layouter

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-tools-feedback`; Korrigerat designen så den är i synk med UI-kit.
  - `digi-form-file-upload`; Justerat så kontroll av tillåtna filtyper fungerar likadant för input-element och "
    dra-och-släpp"-funktionen.

## [19.1.1] - 2023-05-03

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-navigation-context-menu`; Förbättrad tillgänglighet i Context menu.
  - `digi-button`; Lagt till attributen 'afRole' och 'afAriaChecked'.
  - `digi-layout-columns`; Lagt till möjligheten att ange en kolumn.

## [19.1.0] - 2023-04-27

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-tools-feedback`; En komponent som möjliggör insamling av användarfeedback
  - `digi-form-category-filter`; Kategorifilter tar in en lista av kategorier och skickar ut en filtrerad lista på
    kategorier baserat på vilka kategorier användaren väljer.
- `Dokumentation`
  - Nytt designmönster om agentiva tjänster och AI

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-calendar`; En fix för att uppdatera datumen korrekt när man sätter en start månad
  - Alla komponenter som har en publik metod har fått en nytt event som heter `afOnReady`

## [19.0.4] - 2023-04-19

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-navigation-tabs`; Vi sätter textfärgen på flikarna enligt design tokens, annars kan det bli olika beteende
    beroende på enhet och webbläsare.
  - `custom-elements.json`; Vi inkluderar denna fil i npm-paketet så kan man aktivera intellisense. Exempel för hur
    man gör i VS Code finns på sidan <a href="/kom-i-gang/jobba-med-digi-core">Jobba med Digi Core</a>

- `Dokumentation`
  - Korrigerat kodexempel för `digi-form-select` och attributet `af-description`
  - Förbättrat layout i mobilen på tillgänglighetsmatrisen som finns på sidan `Om digital tillgänglighet`
  - Lyft upp `Grafisk profil` i menystrukturen

## [19.0.3] - 2023-04-05

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-navigation-pagination`; Ändrat så aria-current och aria-label hamnar på korrekt html-element
  - `digi-icon-exclamation-triangle-warning`; Lagt in så komponentens svg-kod får unika id:n

- `Dokumentation`
  - Korrigerat kodexempel för digi-datepicker, digi-form-error-list

## [19.0.2] - 2023-03-29

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-calendar`; Fixade en bug där valda datum inte initierades korrekt
  - `digi-navigation-breadcrumbs`; Ändrat standardtext i aria-label till svenska

## [19.0.1] - 2023-03-22

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-logo`; Korrigerat färg för varianten med systemnamn med inverterad textfärg
  - `digi-calendar`; Fixade en bug där valda datum inte initierades korrekt

## [19.0.0] - 2023-03-22

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-tools-languagepicker`; Ny komponent. Språkväljaren tillåter dig ge användaren möjlighet att ändra språk.
  - `digi-info-multi-card`; Ny komponent. Multikort är kort som är tänkta att användas ihop i layouter med samma sorts
    kort på en rad, med kurerat innehåll och längder på texter

### Ändrat

- `@digi/arbetsformedlingen`;

  - `digi-info-card`; <breaking /> Vi har justerat namnstruktur för att spegla bättre hur och när respektive variant
    ska användas. För hjälp med att gå över till denna version, se migreringsguide
    på <a href="/komponenter/digi-info-card/oversikt">komponentsidan</a>

- `@digi/arbetsformedlingen-angular`
  - `digi-link`, `digi-link-internal`, `digi-link-external`, `digi-link-button`; För bättre stöd med routerlinks i
    Angular så har vi lagt in stöd för att kunna använda komponenten som en container för ett vanligt länkelement.
    Detta behövs för att kunna stödja tangentbordsnavigation på korrekt vis.

## [18.5.1] - 2023-03-21

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-loader-spinner`; Åtgärdat bugg som uppstod vid behov av laddningsindikatorer i `digi-navigation-tabs`

## [18.5.0] - 2023-03-15

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-calendar-datepicker`; Ny komponent

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-chart-line`; Åtgärdat en bugg som ritade ut streckade linjen fel om diagrammet ändrade storlek. Ändrat
    muspekaren när man hovrar över en legend

## [18.4.2] - 2023-03-09

- `@digi/arbetsformedlingen-angular`
  - Uppdaterat `rxjs` som peerDependency till "^7.8.0"

## [18.4.1] - 2023-03-08

- `@digi/arbetsformedlingen`

  - `digi-tag`; Lagt till möjlighet att sätta attributet aria-label på en tagg med `af-aria-label`
  - `digi-progressbar`; Korrigerat padding på textetiketten så den är i synk med UI-kittet
  - `digi-form-checkbox`; Åtgärdat bugg som syns när komponenten validerar till "Fel" eller Varning"

- `Dokumentation`
  - Uppdaterat länkar till vår interna sharepoint på sidorna under "Tillgänglighet/Process för
    tillgänglighetsredogörelse"
  - Uppdaterat dokumentation för `digi-tag` med riktlinjer för tillgänglighet med nya attributet `af-aria-label`
  - Förbättrat tillgängligheten på filtren i Tillgänglighetslistan

## [18.4.0] - 2023-03-03

- `@digi/arbetsformedlingen`

  - Uppdaterat till Stencil 3.0
  - `digi-typography`; Ändrat storlek på länk så den följer textstorleken den ligger i

- `@digi/arbetsformedlingen-angular`
  - Lagt till Angular 15 som peerDependency så byggen i DevOps-miljöer ska fungera om man uppdaterat till senaste
    Angular-versionen.

## [18.3.2] - 2023-03-01

### Ändrat

- `@digi/arbetsformedlingen`
  - `Digi-form-filter`; Fixat bugg med att sidan scrollar när man navigerar med tangentbordet
  - `Digi-form-checkbox`; Fixat ikonjustering, justerat fokusindikering. Fixat bugg där sidan hoppade till top vid
    markering/klick av fält
  - `Digi-form-radiobutton`; Justerat fokusindikering. Fixat bugg där sidan hoppade till top vid markering/klick av
    fält
  - `Digi-form-radiogroup`; Buggfix med att den triggar change på init.
  - `Digi-form-fieldset`; Justeringar padding runt fieldset
  - `Digi-navigation-vertical-menu`; Bugg i af-variation="secondary", blev fel bakgrundsfärg på öppnad sektion.
  - `Digi-form-input`; Lagt till nytt attribut inputmode `af-inputmode`.
- `Dokumentation`
  - `Digi-form-input-details`; Lagt till dokumentation för af-inputmode. Även lagt till en beskrivning för hur och när
    man ska använda den.

## [18.3.1] - 2023-02-24

### Ändrat

- `Dokumentation`
  - Uppdaterat dokumentation och designmönster för knappar och vissa formulärkomponenter

## [18.3.0] - 2023-02-22

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-icon-calender-alt-alert`; Lagt till en ny ikon

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-logo`; Justerat färger
  - `digi-link`; Åtgärdat bugg som gjorde att man inte kunde ändra textstorlek
  - `digi-form-input-search`; Anger man af-id så sätts id på input-elementet

- `Dokumentation`
  - Uppdaterat sidan om vår grafiska profil, inklusive Do:s and Don't:s

## [18.2.1] - 2023-02-16

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-chart`; Lagt till d3 som en peerdependency till paketet

## [18.2.0] - 2023-02-15

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-chart`; Ny komponent för diagram
  - `digi-icon-chart`, `digi-icon-table`; Nya ikoner

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-dialog`

    - Bytt placering på knapparna enligt designmönster.
    - Fixat bugg med fokushantering.

  - `digi-link-button`
    - Uppdaterat paddings och marginaler
    - Lagt till val för full bredd, extra storlek och möjligheter att gömma ikon .

- `Dokumentation`
  - Lagt ny sektion i startsida för öppen källkod

## [18.1.2] - 2023-02-08

### Ändrat

- `@digi/arbetsformedlingen`

  - `digi-expandable-accordion`,`digi-expandable-faq-item`; Lagt till så innehållet får visibility: hidden när den är
    stängd, så man inte kan navigera dit med tangetbord då.
  - `digi-button`: Knappar med ikoner får fel höjd på mac

- `@digi/arbetsformedlingen-angular`

  - Lagt till value accessor för digi-form-radiogroup

- `Dokumentation`
  - Tillgänglighetsredogörelsen, vi har lyckats släcka tre punkter
  - Lagt till "Validering" under Designmönster-landningssidan
  - Uppdaterat bilder under formulär-designmönster
  - Ändring av länkar till sharepoint i processen för tillgänglighetsredogörelse

## [18.1.1] - 2023-02-02

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-icon-validation-warning`; Ny ikon

## [18.1.0] - 2023-02-01

### Nytt

- `@digi/arbetsformedlingen`
  - `digi-icon-notification-error`, `digi-icon-notification-info`, `digi-icon-notification-warning`; `digi-icon-notification-success`, `digi-icon-validation-error`, `digi-icon-validation-success`;
    Nya ikoner

### Ändrat

- `@digi/arbetsformedlingen`
  - `digi-form-error-list`; Alla ikoner är uppdaterade och justerad design
  - `digi-form-validation-message`; Alla ikoner är uppdaterade
  - `digi-form-checkbox`; Lagt till varningsvalidering
  - `digi-navigation-vertical-menu-item`; Nytt attribut för att kunna ange aria-current
  - `digi-code-example`; aria-pressed ändrat till aria-expanded
  - `Ikoner`; Uppdaterat så att man ska kunna justera storleken på ikonerna

## [18.0.0] - 2023-01-25

### Nytt

- `@digi/arbetsformedlingen`

  - Ersätter `@digi/core`. Innehåller förutom komponenterna även de delar som tidigare låg i dessa
    npm-paket: `@digi/fonts`, `@digi/styles`, `@digi/design-tokens`

- `@digi/arbetsformedlingen-angular`

  - Ersätter `@digi/core-angular`<br />
    <breaking /> Kräver minst version 14 av Angular

- `@digi/arbetsformedlingen-react`

  - Ersätter `@digi/core-react`

- `Dokumentation`

  - Uppdaterat alla komponenter med kodexemplen för användning i React

- `digi-form-receipt`; Ny komponent för att visa en kvittens för att bekräfta ett korrekt slutfört formulär (eller
  flöde).

### Ändrat

- `Dokumentation`;
  - Guiderna under `Kom igång` är uppdaterade
- `digi-link`, `digi-link-internal`, `digi-link-external`; Justerat så länkar med ikon inte har linje under länken.
- `@digi/core`;
  - har ersatts av `@digi/arbetsformedlingen`
- `@digi/core-angular`;
  - har ersatts av `@digi/arbetsformedlingen-angular` och modulen för att läsa in komponenterna har bytt namn
    till `DigiArbetsformedlingenAngularModule`.
- `@digi/core-react`;
  - har ersatts av `@digi/arbetsformedlingen-react`
- `@digi/fonts`, `@digi/styles`, `@digi/design-tokens`;
  - Dessa paket hittas i `@digi/arbetsformedlingen` framöver
- <breaking /> Paketen hämtas från ett annat npm-register så man behöver lägga till denna rad i projektets `.npmrc`-fil: `@digi:registry=https://nexus.jobtechdev.se/repository/arbetsformedlingen-npm/`
- <breaking /> Css/Scss - vi distibuerar endast en css-fil framöver. Den går bra att läsa in från en scss-fil också. Sökvägen är ändrad till: `@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css`
- <breaking /> Typsnitten - sökvägen till css/scss-filen är ändrad till: `@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/fonts.css`. Typsnittsfilerna ligger under: `@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/assets/fonts`
- <breaking /> Enums - enums har flyttat till nya paketet, så man behöver peka mot: `@digi/arbetsformedlingen`, ex: `import { EnumName1, EnumName2 } from '@digi/arbetsformedlingen';`

## [17.3.3] - 2022-12-23

### Nytt

- `@digi/core`

  - `digi-button`; lagt till möjlighet att ange id-attributet på knappen med attributet `af-id`

  - `Dokumentation`
  - "Validering". Ny sida under Designmönster.

## [17.3.2] - 2022-12-14

### Ändrat

- `@digi/core`

  - `digi-form-file-upload`; Lämnar beta-stadiet.
  - `digi-expandable-accordion`, `digi-expandable-faq-item`; åtgärdat så att innehållet inte går att navigera till när
    ytan är stängd.
  - `digi-calendar`, `digi-form-file-upload`, `digi-navigation-vertical-menu-item`; knappar har fått
    attributet `type="button"` för att undvika att formulär postas.

- `Dokumentation`
  - Text- och bild-korr och förtydligande av användande av knappar i mobil.

## [17.3.1] - 2022-12-07

### Ändrat

- `@digi/core`

  - `digi-navigation-tabs`; Lagt till nytt attribut, `afPreventScrollOnFocus`. Nytt event `afOnTabsReady`.

- `Dokumentation`
  - Uppdaterat exempel för `digi-progress-steps` och `digi-progress-step`

## [17.3.0] - 2022-11-30

### Ändrat

- `@digi/core`
  - `digi-navigation-vertical-menu-item`; Ändrat så vald sida är markerad i font-weight bold istället för semibold och
    i svart färg istället för länkfärg
  - `digi-logo`; Justerat font-weight till 700 istället för 800

## [17.2.0] - 2022-11-23

### Nytt

- `@digi/core`

  - `digi-icon-solid-heart`; Ny ikon

- `Dokumentation`
  - Tillgänglighetsmatris - en ny funktion på sidan "Tillgänglighet/Om digital tillgänglighet"

## [17.1.0] - 2022-11-09

- `@digi/core`
  - `digi-form-file-upload`; Ny styling för mobilläge
- `Dokumentation`
  - Ny sortering för design tokens

## [17.0.0] - 2022-11-02

- `@digi/core`
  - <breaking/> `digi-form-filter`; Vi har lagt till eventet `afOnFilterClosed`. Eventet skickas när filtret stängs utan att valda alternativ bekräftats. Tidigare skickades eventet `afOnSubmitFilters` på både vid bekräftelse och när man stängde ned filtret utan att bekräfta.

## [16.8.2] - 2022-10-26

- `@digi/core`
  - `digi-logo`; Åtgärdat storlek på system-logotyp i mobila enheter
  - `digi-form-file-upload`; Ändrat validering enligt ny API-struktur. Man kan få ut alla filer som är uppladdade och
    validerade

## [16.8.1] - 2022-10-19

### Ändrat

- `@digi/core`

  - `digi-dialog`; Åtgärdat problem med att man inte kan sätta fokus på formulärelement i dialogen

- `Dokumentation`
  - Korrigerat texter på sidorna under "Kom igång/Jobba med Digi Core"
  - Uppdaterat texter på sidan "Process för tillgänglighetsredogörelse" under "Tillgänglighet"

## [16.8.0] - 2022-10-12

### Nytt

- `@digi/core`
  - `digi-icon-chat`, `digi-icon-upload`, `digi-icon-notification-error`, `digi-icon-validation-error`; Nya ikoner

### Ändrat

- `@digi/core`

  - `digi-expandable-accordion`,`digi-expandable-faq-item`; Löst bugg med att text på flera rader blir centrerat
  - `digi-button`; Lagt till möjlighet att välja fullbredd på knappen i mindre enheter
  - Vi har lagt till aria-hidden="true" på alla ikoner
  - `digi-navigation-vertical-menu`; Lagt till en metod för att sätta aktiv nivå vid dynamisk inladdning
  - `digi-link`, `digi-link-internal`, `digi-link-external`, `digi-link-button`; Förbättrat länkhantering vid dynamisk
    routing i t.ex. angular och react

- `Dokumentation`
  - Korrigerat text på sidan "Designmönster/Knappar" kring riktlinjerna om förhållandet mellan primär och sekundär
    knapp
  - Uppdaterat texter på sidan "Tillgänglighet/process för tillgänglighetsredogörelse"
  - Uppdaterat texter på sidan "Tillgänglighet/Nivåer i WCAG"
  - Åtgärdat tillgänglighetsrelaterade buggar

## [16.7.4] - 2022-09-28

### Ändrat

- `Dokumentation`

  - Uppdaterat sidan "Digi Tokens" under "Om designsystemet".

- `@digi/core`
  - `digi-form-error-list`; Löst bugg med fokusfunktion på rubrik.

## [16.7.3] - 2022-09-21

### Ändrat

- `@digi/core`

  - `digi-form-file-upload`; Lagt in så man kan ändra rubriken. Små justeringar i designen.

- `Dokumentation`
  - Ny design på tillgänglighetslistan.
  - Lagt in så man kan se en lista över alla komponenter samt filtrera bland dessa på sidan "Komponenter/Om vårt
    komponentbibliotek".

## [16.7.2] - 2022-09-20

### Ändrat

- `@digi/core`
  - `digi-form-filter`; Löst bugg om man sätter id på kryssruta. Felet kom efter vi lagt
    in `digi-util-mutation-observer` för att lyssna efter ändringar i lista

## [16.7.1] - 2022-09-14

- `@digi/core`

  - `digi-form-filter`; Tillgänglighetsförbättringar.
  - `digi-dialog`; Lagt till en slot ovanför rubrik.
  - `digi-form-label`; Gjort det möjligt att språkanpassa texter.

- `Dokumentation`
  - "Knappar". Ny sida under Designmönster.

## [16.7.0] - 2022-09-07

- `@digi/core`

  - `digi-expandable-accordion`, `digi-expandable-faq-item`; Åtgärdat bugg med typsnitt. Fixat så den inte triggar
    submit på formulär.

- `Dokumentation`

  - "Digi Core React". Ny sida för hur du kommer igång med React-paketet.
  - "Grid och brytpunkter". Ny sida under Designmönster.
  - "Grid Poc". Live-exempel för vår grid.
  - Nya sidor under "Processen för tillgänglighetsredogörelsen"

- `@digi/core-react`
  - Nytt bibliotek för att använda våra komponenter i React.

## [16.6.1] - 2022-09-02

- `@digi/dialog`
  - Bugfix with height in small devices

## [16.6.0] - 2022-08-30

- `@digi/core`
  - `Digi-loading-spinner`; Ny komponent.
  - `Digi-calendar`; Uppdateringar av layout samt stöd för att välja flera dagar
  - `Digi-dialog`; Buggfixar på fokusfällan.
  - `Digi-expandable-accordion`, `digi-expandable-faq-item`, `digi-form-validation-message`, `digi-navigation-pagination`, `digi-navigation-sidebar`, `digi-tag`;
    Korrigerat så aria-hidden får ett värde.
  - `Digi-form-checkbox`, `digi-form-input`, `digi-form-textarea`; Lagt till aria-invalid.
  - `Digi-form-input`, `digi-form-select`, `digi-form-textare`; Lagt till id-attribut på valideringsmeddelandet.
  - `Digi-form-filter`; Lagt till aria-describedby på knappen som öppnar listan.
  - `Digi-navigation-breadcrumbs`; Lagt in så den går att uppdatera dynamiskt.
- `Dokumentation`
  - Länk i sidfoten till Open Source koden i GitLab.
  - Tagit bort länkarna "Tips för testa" i tillgänglighetslistan. Dessa kommer inkluderas direkt på sidan.
  - Tagit bort länk "Testmetoder" i navigationen. Testmetoder kommer inkluderas i tillgänglighetslistan.

## [16.5.1] - 2022-08-08

- `@digi/core-angular`
  - Löser buggfix med saknade moduler. Föregående version hänvisar till fel version av `@digi/core`.

## [16.5.0] - 2022-07-08

- `@digi/core`

  - `digi-form-file-upload`; Ny komponent för filuppladdning.
  - `digi-dialog`; Ny komponent för modala fönster.

- `Dokumentation`
  - Uppdatering av lagtexter under tillgänglighet.
  - Förtydligande av vilka webbläsare vi stödjer.

## [16.4.1] - 2022-06-28

- `@digi/core`

  - `digi-calendar-week-view`; Buggfix vid hover fixad för Safari på mac.

- `Dokumentation`
  - Designmönster, ny sidstruktur
  - Introduktion; Formulär; Spacing; nya sidor under Designmönster
  - Aktiverat webbanalys med Matomo
  - Tillgänglighetslistan; små textjusteringar

## [16.4.0] - 2022-06-15

- `@digi/core`

  - `digi-form-radiogroup`; Komponent som håller värdet för radiobuttons.

- `Dokumentation`
  - Kom igång med Digi UI-kit; ny sida under Kom igång
  - Textuppdateringar i tillgänglighetschecklistan

## [16.3.0] - 2022-06-07

### Nytt

- `@digi/core`
  - `digi-calendar-week-view`; Komponent för att visa en veckoöversikt.

### Ändrat

- `Dokumentation`
  - Uppdaterade kodexempel
    - Navigationskomponenter
  - Riktlinjer för animationer

## [16.2.1] - 2022-06-03

### Ändrat

- `@digi/core`
  - `digi-progressbar`; Åtgärdade en bugg med aktiv indikering

## [16.2.0] - 2022-05-31

### Nytt

- `@digi/core`
  - `digi-icon-filter`; Ny ikon

### Ändrat

- `Dokumentation`
  - Uppdaterade kodexempel
    - Layoutkomponenter
  - Uppdateringar på startsidan. Snabbgenvägar till landningssidor

## [16.1.2] - 2022-05-18

### Ändrat

- `Dokumentation`
  - Färgsidan under grafisk profil är uppdaterad enligt varumärket

## [16.1.1] - 2022-05-17

### Ändrat

- `Dokumentation`
  - Uppdaterade kodexempel
    - Typografikomponenter
    - Mediakomponenter
    - Tabellkomponent
  - Tillgänglighetsförbättringar på logotyplänk
  - Tillgänglighetslistan har fått funktionalitet för att importera csv-fil

## [16.1.0] - 2022-05-03

### Ändrat

- `@digi/core`
  - Live-exempel täcker nu fullbredd
- `Dokumentation`
  - Uppdaterade kodexempel
    - Formulärkomponenter
    - Förloppsmätarkomponenter
    - Kalenderkomponenten
    - Kodkomponenter
    - Kortkomponenter
    - Logotypkomponenten
    - Länkkomponenter
    - Taggkomponenten
    - Utfällbartkomponenter
  - Title-texter i webbläsaren sätts nu korrekt

## [16.0.1] - 2022-04-29

### Ändrat

- `@digi/core`
  - `digi-expandable-accordion`, `digi-expandable-faq-item`; Lagt in en knapp i rubriken för att interaktion med
    skärmläsare ska fungera korrekt.
  - `digi-button`; Lagt till attributen `afAriaControls`, `afAriaPressed` och `afAriaExpanded`.

## [16.0.0] - 2022-04-26

### Nytt

- `@digi/design-tokens`;<br />_Designtokens används för att style:a applikationer enligt gemensam nomenklatur och
  struktur._

  - Library som innehåller designtokens i jsonformat och förmåga att exportera dessa i olika format (e.g. CSS, SCSS,
    JSON, JS, m.fl.).
  - Designtokens finns i 3 nivåer; (Component-tokens finns bara i `@digi/core`
    och `@digi/styles`, `@digi/design-tokens` innehåller bara global och brand)
    - **global** - skalor av färger, avstånd m.m.,
    - **brand** - global tokens applicerade i olika kontexter, t.ex. color text primary, padding medium,
    - **component** - komponenters specifika applicering av brand-tokens, t.ex. button color background primary.
  - Alla designtokens hanteras i grunden här och inte längre i `@digi/styles` (som nu endast konsumerar designtokens i
    css/scss-format och exporterar vidare).
  - Olika applikationer kan nyttja designtokens i olika format, dessa format går att konfigurera direkt
    i `@digi/design-tokens` för att kunna exporteras vid behov - synka med designsystemet för att få till er
    applikations format!

- `@digi/core`

  - Ny struktur på komponenter som bättre följer en gemensam struktur och arkitektur. i Styles-mappen i komponentens
    mapp finns två filer främst; `<komponent>.variables.scss` (innehåller designtokens för komponenten)
    och `<komponent>.variations.scss` (innehåller mixins som applicerar designtokens beroende på komponentens aktiva
    variation).
  - <breaking/> Designtokens för komponenter skrivs och hanteras nu direkt i `@digi/core` och inte längre som SCSS-filer i styles. Dessa variabler/tokens exporteras dock till `@digi/styles` under Components.
  - Nytt verktyg för att skapa komponenter, kör kommandot `npm run generate-component:core` och följ dialogen för att
    skapa en ny komponent i `@digi/core` enligt den nya strukturen, med exempelfiler.

- `@digi/styles`
  - <breaking/> Tagit bort filerna `_entry.scss` och `digi.scss`, och ersatt dessa med `digi-styles.custom-properties.scss` (för alla designtokens i css/scss-format), `digi-styles.utilities.scss` (för alla functions och mixins, e.g. a11y--sr-only) samt `digi-styles.scss`(för allt från `@digi/styles` samtidigt, ersätter i princip `digi.scss`).
  - CSS-fil som innehåller alla designtokens/custom-properties (global, brand och component) samt CSS-fil som
    innehåller alla utility-classes finns under dist-mappen.

### Ändrat

- `@digi/core`

  - <breaking/> Alla enums för storlekar (S, M och L) har nu döpts om till sina fullständiga namn (SMALL, MEDIUM och LARGE).<br />**Se till att uppdatera era applikationer där ni använder dessa!**
  - <breaking/> Alla komponenters custom properties/designtokens har fått uppdaterade namn. Dessa kan ni se under respektive komponent på dokumentations-sajten.<br />**Se till att uppdatera era applikationer där ni använder dessa!**
  - Använder nu nya filer i `@digi/styles` för variables och utilities. Laddar in css-variabler i roten/global.
  - <breaking/> Ikoner har fått uppdaterad struktur för sina designtokens. Nu behöver du override:a variabeln direkt på komponenten, precis som för alla andra komponenter. Om du har en dynamisk ikon så kan du hitta den i css via t.ex. `[slot^='icon']` (för både icon och icon-secondary).<br />**Se till att uppdatera era applikationer där ni använder dessa!**
  - <breaking/> Följande komponenter har fått uppdaterade enums: `digi-button`, `digi-calendar`, `digi-form-input`, `digi-form-input-search`, `digi-form-select`, `digi-form-textarea`, `digi-link`, `digi-link-button`, `digi-link-internal`, `digi-link-external`, `digi-logo`, `digi-notification-alert`, `digi-table`, `digi-tag`, `digi-typography`, `digi-breakpoint-observer`.

- `@digi/styles`

  - <breaking/> Innehåller numera inga SCSS-variabler för komponenterna utan dessa är ersatta med customproperties (css-variabler). Källan till dessa är flyttade till respektive komponent i `@digi/core` och exporteras sedan tillbaka till `@digi/styles`.
  - Värdet på flera variabler har justerats, t.ex. avstånd och vissa färger.

- `Dokumentationen`
  - Använder nya designtokens och uppdatera komponenter.

## [15.0.0] - 2022-04-12

### Nytt

- `@digi/core`
  - Nya ikoner!
- `Dokumentation`
  - Lagt till en sida för release notes.
  - Förbättrade kontaktuppgifter enligt tillgänglighetskrav.
  - Ny startsida och sida för introduktion av designsystemet.

### Ändrat

- `@digi/core`
  - `digi-icon-arrow-<up|down|left|right>`; ikonerna för `arrow` har bytt namn
    till `digi-icon-chevron-<up|down|left|right>` för att bättre följa namnpraxis. Nya ikoner för arrow införda i dess
    ställe.
    **Glöm inte att byta namn på ikonerna om du lagt till dom manuellt i din applikation.**
  - `digi-media-image`; Fixat fel som uppstod om man använder komponenten som `af-unlazy` utan att ange specifik höjd
    och bredd på bilden.
  - `digi-icon-spinner`; problem med ikonen fixat.
- `Dokumentation`
  - Förbättrat live-exempel för informationsmeddelanden.
  - Fixat problem med laddning av bilder på dokumentations-sajten.

## [14.0.1] - 2022-04-11

### Ändrat

- `@digi/core-angular`
  - `digi-progress-steps` och `digi-progress-step` saknades i core-angular. Problemet är patchat.

## [14.0.0] - 2022-03-29

### Nytt

- `@digi/core`
  - `digi-progress-steps`; Ny komponent för att visualisera ett användarflöde.
  - `digi-progress-step`; Ny komponent som används tillsammans med `digi-progress-steps`. Detta är varje steg i
    flödet.

### Ändrat

- `Dokumentation`

  - Rättat länk till mediabanken.
  - Navigationen på dokumentationssidan använder sig av uppdateringarna i `digi-navigation-vertical-menu`
    och `digi-navigation-vertical-menu-item`.
  - Tagit bort dubbla landmärken runt navigationen.
  - Fixat valideringsfel på navigationen.
  - Uppdaterat tillgänglighetsredogörelsen.

- `@digi/core`
  - `digi-navigation-vertical-menu`; Layout är uppdaterad.
  - `digi-navigation-vertical-menu-item`; Layout är uppdaterad.

## [13.2.3] - 2022-03-25

- `@digi/core-angular`
  - `digi-icon-accessibility-universal`, `digi-icon-download`, `digi-icon-redo`, `digi-icon-trash` saknades i
    core-angular. Problemet är patchat.

## [13.2.2] - 2022-03-24

- `@digi/core`
  - `Code` typen för Code exporterades inte korrekt och skapade problem vid användning av Core i vissa sammanhang.
    Problemet är patchat.

## [13.2.1] - 2022-03-23

- `@digi/core-angular`
  - Tillåter alla peer dependency versioner av angular-paketen för version 12 och 13 och undviker därmed Conflicting
    Peer Dependency-felet när du gör npm install.

## [13.2.0] - 2022-03-22

### Ändrat

- `Dokumentationen`

  - Ändrat till Stencil Router V2 för att lösa buggar med bl.a. lazy-loadade bilder i dokumentationsapplikationen. (
    Problem med bildladdning kvarstår för vissa användare och kommer åtgärdas till nästa release.)
  - Komponentdokumentationen för `digi-link-button`, `digi-button` och `digi-info-card` är uppdaterad med den nya
    versionen av kodexemepelkomponenten

- `@digi/core`

  - `digi-code-example`; förbättrad funktionalitet och möjlighet att kunna växla mellan olika varianter av
    exempelkomponenten i demoytan.
  - `digi-code`; ändrat bakgrundsfärg från gråsvart till mörkblå, samt gjort den ljusa varianten till standard.
  - `digi-code-block`; ändrat bakgrundsfärg från gråsvart till mörkblå.

- `@digi/core-angular`

  - n/a

- `@digi/styles`

  - n/a

- `@digi/fonts`
  - n/a

## [13.1.2] - 2022-03-18

### Ändrat

- `@digi/core`

  - n/a

- `@digi/core-angular`

  - La till komponenter under Utfällbart (expandable).

- `@digi/styles`

  - n/a

- `@digi/fonts`
  - n/a

## [13.1.1] - 2022-03-16

### Ändrat

- `@digi/core`

  - `digi-navigation-pagination`; aktiv sida markerades inte korrekt om man har väldigt många sidor.

- `@digi/core-angular`

  - n/a

- `@digi/styles`

  - n/a

- `@digi/fonts`
  - n/a

## [13.1.0] - 2022-03-10

### Nytt

- `Changelog`

  - Ny gemensam changelog för hela monorepot. All information om driftsättningar kommer skrivas i samma dokument för
    att hålla ihop versionsnummer och gemensamma ändringar.

- `Dokumentationen`

  - Testmetoder; ny sida under Tillgänglighet och design.
  - Ny förbättrad caching av sidan. Laddar resurser i bakgrunden och tillåter bl.a. offline-läge och snabbare
    sidladdning.

- `@digi/core`

  - `digi-icon-trash`; ny ikon som illustrerar en soptunna.
  - `digi-icon-accessibility-universal`; ny ikon som illustrerar tillgänglighet.

- `@digi/core-angular`

  - n/a

- `@digi/styles`

  - n/a

- `@digi/fonts`
  - n/a

### Ändrat

- `Dokumentationen`

  - Tillgänglighetslistan; ny förbättrad funktionalitet för att kunna följa upp status och kommentarer på olika krav
    samt bättre filtrering. Även ökad tillgänglighet.

- `@digi/core`

  - n/a

- `@digi/core-angular`

  - n/a

- `@digi/styles`

  - n/a

- `@digi/fonts`
  - n/a

## [13.0.0] - 2022-03-08

### Nytt

- `@digi/core-angular`

  - Nu helt releasad och har gått ur beta.

- `@digi/fonts`
  - Nytt bibliotek för hantering av Arbetsförmedlingens typsnitt.

### Ändrat

- `digi-button`
  - Lagt till saknade design tokens samt dokumenterat tokens.

## [12.6.1] - 2022-02-18

- `digi-form-select`
  - `afStartSelected` är deprecated. Använd `afValue` istället.

## [12.6.0] - 2022-02-22

### Nytt

- `digi-expandable-accordion`

  - Ny komponent

- `digi-expandable-faq`

  - Ny komponent

- `digi-expandable-faq-item`
  - Ny komponent

## [12.5.0] - 2022-02-22

### Ändrat

- `digi-link-(internal|external)`
  - Justerat så att digi-link-internal och digi-link-external använder sig av digi-link för att de ska ärva regler och
    fungera lika.

## [12.4.0] - 2022-02-15

- `digi-typography`
  - Lagt till maxbredd för rubriker och länkar.
  - Lagt en l-version för maxbredd.

## [12.3.1] - 2022-02-09

- `digi-button, digi-link-(internal|external)`
  - Justerat positionering av ikoner och dess storlek för knappar och länkar.

## [12.3.0] - 2022-02-09

- `digi-navigation-tabs`
  - Lagt till en publik metod för att ändra aktiv flik.

## [12.1.1-beta.5] - 2022-01-20

- `value`
  - Value och afValue sätts vid afOnInput så attributet korrekt speglas vid target.

## [11.2.1-beta.0] - 2022-01-19

- `digi-form-radiobutton`
  - Fixat bugg där radiogroup inte stöds. Fungerar nu på det sätt du önskar!

## [10.2.1] - 2022-01-19

### Ändrat

- `digi-media-image`
  - Fixat bugg med hur platshållaren fungerar
  - Ändrat bakgrunsfärg på platshållaren
  - Lagt till så attributet loading används på bilden. Sätts till `lazy` eller `eager` beroende på om man
    använder `afUnLazy` eller inte
  - Bilden går att ändra dynamiskt

## [12.0.0] - 2022-01-18

- `version`
  - Ökade versionsnumret till 12 för att undvika problem med paket i npm.

## [10.2.0] - 2022-01-18

- `digi-layout-block`

  - Lade till attributet af-vertical-padding för att kunna addera padding inuti container-elementet.
  - Lade till attributen af-margin-top och af-margin-bottom för att kunna addera marginal uppe och/eller nere på
    container-elementet.

- `digi-layout-container`
  - Lade till attributet af-vertical-padding för att kunna addera padding inuti elementet.
  - Lade till attributen af-margin-top och af-margin-bottom för att kunna addera marginal uppe och/eller nere på
    elementet.

## [11.1.0] - 2022-01-10

- `enums`

  - Enums finns numera under `@digi/core` och fungerar med bl.a. Angular.

- `types`

  - Ändrat types från `components.d.ts` till `index.d.ts` som både exporterar components samt enums. På components
    finns namespace Components som kan användas t.ex. som `Components.DigiButton`. Förberett för interfaces om/när det
    ska användas och exponeras.

- `output-target`
  - Använder nu `dist-custom-elements` istället för `dist-custom-elements-bundle` som blivit deprecated.

## [10.1.3] - 2022-01-17

### Ändrat

- `digi-form-input`, `digi-form-textarea`
  - Tagit bort kontroll av 'dirty' och 'touched' av formulärelement vilket gör det mer flexibelt att välja när
    felmeddelanden ska visas.
- `digi-form-select`
  - Använder sig av `digi-util-mutation-observer` för att se om options-lista ändras programmatiskt.
- `digi-form-fieldset`
  - Lagt in möjlighet att sätta id på komponenten. Väljer man inget sätts en slumpmässig id.
- `digi-media-image`
  - Löst bugg som gjorde att komponenten inte använde den bredd och höjd man angett.

## [11.0.0] - 2021-12-23

- `digi-form-*`

  - Justerat formulärkomponenterna så att de kan använda `value` istället för `afValue` (som nu är markerat
    deprecated). Även `checked` ersätter `afChecked`. Detta är gjort för att bättre fungera med Angular samt det nya
    biblioteket Digi Core Angular som är påväg ut.

- `digi-form-select`

  - Ny logik för att hitta valt värde och skicka rätt event enligt samma practice som de andra formulärelementen.

- `digi-form-radiobutton`

  - Tog bort `afChecked` och använder nu `afValue`för att initialt sätta ett värde. När `value` är samma som `afValue`
    så är radioknappen icheckad (följer bättre radiogroup).

- `övrigt`
  - Digi Core är nu förberett för att kunna exportera filer via output-target till Angular. Ett nytt
    bibliotek `@digi/core-angular` kommer snart som är till för angular-appar. Det här biblioteket wrappar Digi Core
    och gör det enklare att använda t.ex. Reactive Forms.

## [10.1.2] - 2021-12-15

### Ändrat

- `digi-typography`
  - Lagt till så ul- och dl-listor får samma textstorlek som stycken och även samma hantering för att inte få för
    långa textrader
- `digi-navigation-pagination`
  - Justerat så att primär och sekundär variant på knapparna används korrekt. För att ändra utseende på knapparna så
    behöver knapparnas original-variabler ändras direkt, t.ex. `--digi-button--background`, dock finns variabler för
    t.ex. width, padding, m.m.

## [10.1.1] - 2021-12-10

### Ändrat

- `digi-form-filter`
  - Löst en bugg där komponenten tidigare fungerade felaktigt om någon kryssruta var aktiv vid sidladdning.
- `digi-navigation-tabs`
  - Lagt in stöd så komponenten känner av om man lägger till eller tar bort `digi-navigation-tab`-komponenter för att
    kunna dynamiskt ändra antal flikar.
- `digi-info-card`
  - Tagit bort felaktig marginal på rubriken.
- `digi-navigation-tab`
  - Ändrat så fokusram på en panel endast markeras när man navigerar dit med skärmläsare.
- `digi-form-filter`
  - Fixat så att komponenten fungerar korrekt även om man använder den inuti `digi-navigation-tab`. Tidigare stängdes
    listan när man försökte klicka på en kryssruta.

## [10.1.0] - 2021-11-23

### Nytt

- `digi-icon-exclamation-triangle-warning`
  - Lagt till en ny ikon. Denna ikon används bl.a. för varningsmeddelanden i formulär. Tidigare behövdes den ikonen
    läsas in som en asset i projektet, nu kommer inte detta behövas.

### Ändrat

- `digi-link-button`
  - Uppdaterat komponenten så man kan välja mellan olika storlekar.
  - Uppdaterat så alla layouter som finns i UI-kit också går att använda i kod.
- `digi-form-input`, `digi-form-select`, `digi-form-textarea`
  - Korrigerat så layout av formulärelement när de indikerar "fel", "varning" och "korrekt" följer UI-kit.
- `digi-icon-arrow-down`
  - Korrigerat layout så den följer UI-kit
- `digi-navigation-breadcrumbs`
  - Ändrat uppdelaren mellan länkar till '/', istället för '>'.

## [10.0.0] - 2021-10-19

### Ändrat

- `digi-navigation-menu`
  - Allt innehåll i Navigation Menu ska ligga i en `<ul>`-tagg och listas med respektive `<li>`-taggar.
- `digi-navigation-menu-item`
  - En Navigation Menu Item som ska agera som expanderbart menyalternativ ska följas av en `<ul>`-tagg, t.ex.
  ```
    <digi-navigation-menu-item></digi-navigation-menu-item>
    <ul>...</ul>
  ```
  För att göra raden expanderbar måste attributet `af-active-subnav` användas med antingen `true` för för-expanderad
  eller `false` för stängd.
- `digi-layout-media`
  - Lagt till token `--digi-layout-media-object--flex-wrap`
- `digi-progressbar`
  - Lagt till möjlighet att tända valfria steg i komponenten, som följer ordningen på vilka formulär-element som är
    gjorda.
- `digi-navigation-pagination`
  - Lagt till en publik metod för att ändra aktivt steg i komponenten.
- `digi-form-error-list`
  - Lade till default värde på linkItems för att undvika JS fel vid tom lista

## [9.4.6] - 2021-09-30

### Ändrat

- `digi-code-example`
  - Ändrat bakgrundsfärg på vertygslistan

## [9.4.5] - 2021-09-28

### Ändrat

- `digi-form-input-search`
  - Lagt till möjlighet att dölja knappen
  - Buggfix, knappens text gick inte att ändra

## [9.4.4] - 2021-09-24

### Ändrat

- `digi-form-error-list`
  - Ändrat färgen på notifikationen från "info" till "danger"
- `digi-notification-alert`
  - Justerat padding för medium storlek
- `digi-form-validation-message`
  - Ändrat default varningsmeddelande till att vara tomt

## [9.4.3] - 2021-09-16

### Ändrat

- `design-tokens`
  - Ändrat färgen `$digi--ui--color--green`
  - Ändrat färgen `$digi--ui--color--pink`

## [9.4.2] - 2021-08-27

### Ändrat

- `digi-form-filter`
  - Lagt till afOnChange EventEmitter

## [9.4.1] - 2021-08-18

### Ändrat

- Lagt till en ny outputTarget `dist-custom-elements-bundle`

## [9.4.0] - 2021-07-02

### Nytt

- `digi-progressbar`
  - Ny komponent

### Ändrat

- `digi-form-radiobutton`
  - Korrigeringar av layout

## [9.3.0] - 2021-06-18

### Ändrat

- `digi-navigation-pagination`
  - Skriv inte ut pagineringen om det bara är en sida. Däremot visas fortfarande texten "Visar 1-15 av XXX".

## [9.2.1] - 2021-06-15

### Ändrat

- Lagt till alla enums under `@digi/core/dist/enum/`
- Korrigerat felaktigheter i alla readmefiler. Främst gällande felaktiga paths till enum-importer.
- Lagt till enum-mappen till disten.

## [9.2.0] - 2021-06-15

### Nytt

- `digi-calendar`
  - Ny komponent
- `digi-typography-meta`
  - Ny komponent
- `digi-logo`
  - Ny komponent
- `digi-util-mutation-observer`
  - Ny komponent
- `digi-form-radiobutton`
  - Ny komponent

### Ändrat

- `digi-form-fieldset`
  - Ändrade padding i fieldset till 0 för att linjera med komponenter utanför fieldset
- `digi-form-error-list`
  - Lagt till mutation observer för att se ändringar i komponenten

## [9.1.0] - 2021-05-25

### Nytt

- `digi-util-breakpoint-observer`
  - Ny komponent
- `digi-form-select`
  - Ny komponent
- `digi-form-fieldset`
  - Ny komponent
- `digi-tag`
  - Ny komponent

### Ändrat

- `digi-navigation-pagination`
  - Felplacering av ikon i knapparna föregående/nästa vid navigering av siffer-knapparna.
  - Centrering av text i siffer-knapparna efter css-reset tagits bort

## [9.0.0] - 2021-05-04

### Ändrat

- `digi-core`
  - Lagt till ny docs tag `@enums` i alla komponenter
- `digi-media-figure`
  - Ändrat enum `MediaFigureAlign` till `MediaFigureAlignment`
  - Ändrat prop `afAlign` till `afAlignment`
- `digi-form-error-list`
  - Ändrat enum `ErrorListHeadingLevel` till `FormErrorListHeadingLevel`

## [8.1.0] - 2021-05-03

### Nytt

- `digi-core`
  - Lagt till alla `enums` till paketet. Dessa hittas under `@digi/core/enum/mitt-enum-namn.emum.ts`

## [8.0.0] - 2021-04-30

### Changed

- `digi-core`
  - Ändrat all dokumentation inne i komponenterna till svenska
- `digi-form-filter`
  - Ändrat event `afOnSubmittedFilter` till `afOnSubmitFilter`
- `digi-form-input-search`
  - Ändrat prop `afButtonLabel` till `afButtonText`
- `digi-navigation-tabs`
  - Ändrat prop `afTablistAriaLabel` till `afAriaLabel`
  - Ändrat prop `afInitActiveTabIndex` till `afInitActiveTab`
- `digi-navigation-pagination`
  - Ändrat prop `afStartPage` till `afInitActivePage`
- `dgi-notification-alert`
  - Ändrat event `afOnCloseAlert` till `afOnClose`

## [7.2.0] - 2021-04-23

### Added

- `digi-notification-alert`
  - Created component
- `digi-form-error-list`
  - Created component
- `digi-link`
  - Created component

### Changed

- `digi-layout-media-object`
  - Included Stretch alignment

## [7.1.0] - 2021-04-19

### Added

- `digi-typography-time`
  - Created component

### Changed

- `Navigation-pagination` `Navigation-tabs` `Form-filter` `Form-textarea` `Form-input` `Form-input-search`
  - Solved style issues after removal of base-reset

## [7.0.0] - 2021-04-19

### Added

- `digi-navigation-context-menu`
  - Created component
- `digi-navigation-breadcrumbs`
  - Created component

### Breaking Changes

- **Removed reset css from all components**
  - Previously, all components included a reset css to prevent global styles from leaking in. This created a massive
    duplication of css and is now removed. Every consuming app is now responsible for handling css leakage. We are
    continuously correcting css problems that this may have caused inside the components themselves. If you encounter
    any problems, please report them to us.
- **Removed `global` folder**
  - All uncompiled scss is being moved to a new library called `@digi/styles`, which will very soon be available from
    the package manager. In the meantime, you can use the old `@af/digi-core`-package.

### Changed

- `digi-button`
  - Included hasIconSecondary within handleErrors function
  - Added outline and text-align variables
